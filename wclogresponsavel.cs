/*
               File: WCLogResponsavel
        Description: WCLog Responsavel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 5:19:6.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wclogresponsavel : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wclogresponsavel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wclogresponsavel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_LogResponsavel_DemandaCod )
      {
         this.AV7LogResponsavel_DemandaCod = aP0_LogResponsavel_DemandaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavEmissor = new GXCombobox();
         cmbLogResponsavel_Acao = new GXCombobox();
         cmbavDestino = new GXCombobox();
         cmbLogResponsavel_Status = new GXCombobox();
         cmbLogResponsavel_NovoStatus = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7LogResponsavel_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7LogResponsavel_DemandaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7LogResponsavel_DemandaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_9 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_9_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_9_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV23TFLogResponsavel_DataHora = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFLogResponsavel_DataHora", context.localUtil.TToC( AV23TFLogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
                  AV24TFLogResponsavel_DataHora_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFLogResponsavel_DataHora_To", context.localUtil.TToC( AV24TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
                  AV41TFLogResponsavel_Prazo = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFLogResponsavel_Prazo", context.localUtil.TToC( AV41TFLogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
                  AV42TFLogResponsavel_Prazo_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFLogResponsavel_Prazo_To", context.localUtil.TToC( AV42TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " "));
                  AV47TFLogResponsavel_Observacao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFLogResponsavel_Observacao", AV47TFLogResponsavel_Observacao);
                  AV48TFLogResponsavel_Observacao_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFLogResponsavel_Observacao_Sel", AV48TFLogResponsavel_Observacao_Sel);
                  AV7LogResponsavel_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7LogResponsavel_DemandaCod), 6, 0)));
                  AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace", AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace);
                  AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace", AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace);
                  AV35ddo_LogResponsavel_StatusTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_LogResponsavel_StatusTitleControlIdToReplace", AV35ddo_LogResponsavel_StatusTitleControlIdToReplace);
                  AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace", AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace);
                  AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace", AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace);
                  AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace", AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace);
                  AV57Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV30TFLogResponsavel_Acao_Sels);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV34TFLogResponsavel_Status_Sels);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV38TFLogResponsavel_NovoStatus_Sels);
                  A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                  A58Usuario_PessoaNom = GetNextPar( );
                  n58Usuario_PessoaNom = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
                  A896LogResponsavel_Owner = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
                  A891LogResponsavel_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n891LogResponsavel_UsuarioCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
                  A493ContagemResultado_DemandaFM = GetNextPar( );
                  n493ContagemResultado_DemandaFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
                  AV52UltimaAcao = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52UltimaAcao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52UltimaAcao), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  AV54String = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54String", AV54String);
                  A894LogResponsavel_Acao = GetNextPar( );
                  A1797LogResponsavel_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
                  A1228ContratadaUsuario_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1228ContratadaUsuario_AreaTrabalhoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1228ContratadaUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0)));
                  AV17Destino = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavDestino_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Destino), 6, 0)));
                  A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
                  AV16Emissor = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavEmissor_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Emissor), 6, 0)));
                  A2080AreaTrabalho_SelUsrPrestadora = (bool)(BooleanUtil.Val(GetNextPar( )));
                  n2080AreaTrabalho_SelUsrPrestadora = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2080AreaTrabalho_SelUsrPrestadora", A2080AreaTrabalho_SelUsrPrestadora);
                  A66ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
                  A438Contratada_Sigla = GetNextPar( );
                  n438Contratada_Sigla = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A438Contratada_Sigla", A438Contratada_Sigla);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV23TFLogResponsavel_DataHora, AV24TFLogResponsavel_DataHora_To, AV41TFLogResponsavel_Prazo, AV42TFLogResponsavel_Prazo_To, AV47TFLogResponsavel_Observacao, AV48TFLogResponsavel_Observacao_Sel, AV7LogResponsavel_DemandaCod, AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV35ddo_LogResponsavel_StatusTitleControlIdToReplace, AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace, AV57Pgmname, AV30TFLogResponsavel_Acao_Sels, AV34TFLogResponsavel_Status_Sels, AV38TFLogResponsavel_NovoStatus_Sels, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV52UltimaAcao, AV6WWPContext, AV54String, A894LogResponsavel_Acao, A1797LogResponsavel_Codigo, A1228ContratadaUsuario_AreaTrabalhoCod, AV17Destino, A69ContratadaUsuario_UsuarioCod, AV16Emissor, A2080AreaTrabalho_SelUsrPrestadora, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAFY2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV57Pgmname = "WCLogResponsavel";
               context.Gx_err = 0;
               cmbavEmissor.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavEmissor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavEmissor.Enabled), 5, 0)));
               cmbavDestino.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDestino_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDestino.Enabled), 5, 0)));
               WSFY2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "WCLog Responsavel") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202062519691");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wclogresponsavel.aspx") + "?" + UrlEncode("" +AV7LogResponsavel_DemandaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFLOGRESPONSAVEL_DATAHORA", context.localUtil.TToC( AV23TFLogResponsavel_DataHora, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFLOGRESPONSAVEL_DATAHORA_TO", context.localUtil.TToC( AV24TFLogResponsavel_DataHora_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFLOGRESPONSAVEL_PRAZO", context.localUtil.TToC( AV41TFLogResponsavel_Prazo, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFLOGRESPONSAVEL_PRAZO_TO", context.localUtil.TToC( AV42TFLogResponsavel_Prazo_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFLOGRESPONSAVEL_OBSERVACAO", AV47TFLogResponsavel_Observacao);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFLOGRESPONSAVEL_OBSERVACAO_SEL", AV48TFLogResponsavel_Observacao_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_9", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_9), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV50DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV50DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA", AV22LogResponsavel_DataHoraTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA", AV22LogResponsavel_DataHoraTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vLOGRESPONSAVEL_ACAOTITLEFILTERDATA", AV28LogResponsavel_AcaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vLOGRESPONSAVEL_ACAOTITLEFILTERDATA", AV28LogResponsavel_AcaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vLOGRESPONSAVEL_STATUSTITLEFILTERDATA", AV32LogResponsavel_StatusTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vLOGRESPONSAVEL_STATUSTITLEFILTERDATA", AV32LogResponsavel_StatusTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA", AV36LogResponsavel_NovoStatusTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA", AV36LogResponsavel_NovoStatusTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA", AV40LogResponsavel_PrazoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA", AV40LogResponsavel_PrazoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA", AV46LogResponsavel_ObservacaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA", AV46LogResponsavel_ObservacaoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7LogResponsavel_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vLOGRESPONSAVEL_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7LogResponsavel_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV57Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFLOGRESPONSAVEL_ACAO_SELS", AV30TFLogResponsavel_Acao_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFLOGRESPONSAVEL_ACAO_SELS", AV30TFLogResponsavel_Acao_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFLOGRESPONSAVEL_STATUS_SELS", AV34TFLogResponsavel_Status_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFLOGRESPONSAVEL_STATUS_SELS", AV34TFLogResponsavel_Status_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFLOGRESPONSAVEL_NOVOSTATUS_SELS", AV38TFLogResponsavel_NovoStatus_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFLOGRESPONSAVEL_NOVOSTATUS_SELS", AV38TFLogResponsavel_NovoStatus_Sels);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIO_PESSOANOM", StringUtil.RTrim( A58Usuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOGRESPONSAVEL_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A896LogResponsavel_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOGRESPONSAVEL_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, sPrefix+"vULTIMAACAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52UltimaAcao), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vSTRING", StringUtil.RTrim( AV54String));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOGRESPONSAVEL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1797LogResponsavel_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"AREATRABALHO_SELUSRPRESTADORA", A2080AreaTrabalho_SelUsrPrestadora);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_SIGLA", StringUtil.RTrim( A438Contratada_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"vOS", AV51OS);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Caption", StringUtil.RTrim( Ddo_logresponsavel_datahora_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Tooltip", StringUtil.RTrim( Ddo_logresponsavel_datahora_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Cls", StringUtil.RTrim( Ddo_logresponsavel_datahora_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Filteredtext_set", StringUtil.RTrim( Ddo_logresponsavel_datahora_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Filteredtextto_set", StringUtil.RTrim( Ddo_logresponsavel_datahora_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Dropdownoptionstype", StringUtil.RTrim( Ddo_logresponsavel_datahora_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_logresponsavel_datahora_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Includesortasc", StringUtil.BoolToStr( Ddo_logresponsavel_datahora_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Includesortdsc", StringUtil.BoolToStr( Ddo_logresponsavel_datahora_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Includefilter", StringUtil.BoolToStr( Ddo_logresponsavel_datahora_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Filtertype", StringUtil.RTrim( Ddo_logresponsavel_datahora_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Filterisrange", StringUtil.BoolToStr( Ddo_logresponsavel_datahora_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Includedatalist", StringUtil.BoolToStr( Ddo_logresponsavel_datahora_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Cleanfilter", StringUtil.RTrim( Ddo_logresponsavel_datahora_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Rangefilterfrom", StringUtil.RTrim( Ddo_logresponsavel_datahora_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Rangefilterto", StringUtil.RTrim( Ddo_logresponsavel_datahora_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Searchbuttontext", StringUtil.RTrim( Ddo_logresponsavel_datahora_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Caption", StringUtil.RTrim( Ddo_logresponsavel_acao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Tooltip", StringUtil.RTrim( Ddo_logresponsavel_acao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Cls", StringUtil.RTrim( Ddo_logresponsavel_acao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Selectedvalue_set", StringUtil.RTrim( Ddo_logresponsavel_acao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_logresponsavel_acao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_logresponsavel_acao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Includesortasc", StringUtil.BoolToStr( Ddo_logresponsavel_acao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Includesortdsc", StringUtil.BoolToStr( Ddo_logresponsavel_acao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Includefilter", StringUtil.BoolToStr( Ddo_logresponsavel_acao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Includedatalist", StringUtil.BoolToStr( Ddo_logresponsavel_acao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Datalisttype", StringUtil.RTrim( Ddo_logresponsavel_acao_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_logresponsavel_acao_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_logresponsavel_acao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Cleanfilter", StringUtil.RTrim( Ddo_logresponsavel_acao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Searchbuttontext", StringUtil.RTrim( Ddo_logresponsavel_acao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Caption", StringUtil.RTrim( Ddo_logresponsavel_status_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Tooltip", StringUtil.RTrim( Ddo_logresponsavel_status_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Cls", StringUtil.RTrim( Ddo_logresponsavel_status_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Selectedvalue_set", StringUtil.RTrim( Ddo_logresponsavel_status_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Dropdownoptionstype", StringUtil.RTrim( Ddo_logresponsavel_status_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_logresponsavel_status_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Includesortasc", StringUtil.BoolToStr( Ddo_logresponsavel_status_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Includesortdsc", StringUtil.BoolToStr( Ddo_logresponsavel_status_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Includefilter", StringUtil.BoolToStr( Ddo_logresponsavel_status_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Includedatalist", StringUtil.BoolToStr( Ddo_logresponsavel_status_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Datalisttype", StringUtil.RTrim( Ddo_logresponsavel_status_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Allowmultipleselection", StringUtil.BoolToStr( Ddo_logresponsavel_status_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Datalistfixedvalues", StringUtil.RTrim( Ddo_logresponsavel_status_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Cleanfilter", StringUtil.RTrim( Ddo_logresponsavel_status_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Searchbuttontext", StringUtil.RTrim( Ddo_logresponsavel_status_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Caption", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Tooltip", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Cls", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Selectedvalue_set", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Dropdownoptionstype", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Includesortasc", StringUtil.BoolToStr( Ddo_logresponsavel_novostatus_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Includesortdsc", StringUtil.BoolToStr( Ddo_logresponsavel_novostatus_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Includefilter", StringUtil.BoolToStr( Ddo_logresponsavel_novostatus_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Includedatalist", StringUtil.BoolToStr( Ddo_logresponsavel_novostatus_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Datalisttype", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Allowmultipleselection", StringUtil.BoolToStr( Ddo_logresponsavel_novostatus_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Datalistfixedvalues", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Cleanfilter", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Searchbuttontext", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Caption", StringUtil.RTrim( Ddo_logresponsavel_prazo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Tooltip", StringUtil.RTrim( Ddo_logresponsavel_prazo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Cls", StringUtil.RTrim( Ddo_logresponsavel_prazo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Filteredtext_set", StringUtil.RTrim( Ddo_logresponsavel_prazo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Filteredtextto_set", StringUtil.RTrim( Ddo_logresponsavel_prazo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Dropdownoptionstype", StringUtil.RTrim( Ddo_logresponsavel_prazo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_logresponsavel_prazo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Includesortasc", StringUtil.BoolToStr( Ddo_logresponsavel_prazo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Includesortdsc", StringUtil.BoolToStr( Ddo_logresponsavel_prazo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Includefilter", StringUtil.BoolToStr( Ddo_logresponsavel_prazo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Filtertype", StringUtil.RTrim( Ddo_logresponsavel_prazo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Filterisrange", StringUtil.BoolToStr( Ddo_logresponsavel_prazo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Includedatalist", StringUtil.BoolToStr( Ddo_logresponsavel_prazo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Cleanfilter", StringUtil.RTrim( Ddo_logresponsavel_prazo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Rangefilterfrom", StringUtil.RTrim( Ddo_logresponsavel_prazo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Rangefilterto", StringUtil.RTrim( Ddo_logresponsavel_prazo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Searchbuttontext", StringUtil.RTrim( Ddo_logresponsavel_prazo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Caption", StringUtil.RTrim( Ddo_logresponsavel_observacao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Tooltip", StringUtil.RTrim( Ddo_logresponsavel_observacao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Cls", StringUtil.RTrim( Ddo_logresponsavel_observacao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Filteredtext_set", StringUtil.RTrim( Ddo_logresponsavel_observacao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Selectedvalue_set", StringUtil.RTrim( Ddo_logresponsavel_observacao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_logresponsavel_observacao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_logresponsavel_observacao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Includesortasc", StringUtil.BoolToStr( Ddo_logresponsavel_observacao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Includesortdsc", StringUtil.BoolToStr( Ddo_logresponsavel_observacao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Includefilter", StringUtil.BoolToStr( Ddo_logresponsavel_observacao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Filtertype", StringUtil.RTrim( Ddo_logresponsavel_observacao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Filterisrange", StringUtil.BoolToStr( Ddo_logresponsavel_observacao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Includedatalist", StringUtil.BoolToStr( Ddo_logresponsavel_observacao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Datalisttype", StringUtil.RTrim( Ddo_logresponsavel_observacao_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Datalistproc", StringUtil.RTrim( Ddo_logresponsavel_observacao_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_logresponsavel_observacao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Loadingdata", StringUtil.RTrim( Ddo_logresponsavel_observacao_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Cleanfilter", StringUtil.RTrim( Ddo_logresponsavel_observacao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Noresultsfound", StringUtil.RTrim( Ddo_logresponsavel_observacao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Searchbuttontext", StringUtil.RTrim( Ddo_logresponsavel_observacao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Activeeventkey", StringUtil.RTrim( Ddo_logresponsavel_datahora_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Filteredtext_get", StringUtil.RTrim( Ddo_logresponsavel_datahora_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Filteredtextto_get", StringUtil.RTrim( Ddo_logresponsavel_datahora_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Activeeventkey", StringUtil.RTrim( Ddo_logresponsavel_acao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Selectedvalue_get", StringUtil.RTrim( Ddo_logresponsavel_acao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Activeeventkey", StringUtil.RTrim( Ddo_logresponsavel_status_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Selectedvalue_get", StringUtil.RTrim( Ddo_logresponsavel_status_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Activeeventkey", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Selectedvalue_get", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Activeeventkey", StringUtil.RTrim( Ddo_logresponsavel_prazo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Filteredtext_get", StringUtil.RTrim( Ddo_logresponsavel_prazo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Filteredtextto_get", StringUtil.RTrim( Ddo_logresponsavel_prazo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Activeeventkey", StringUtil.RTrim( Ddo_logresponsavel_observacao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Filteredtext_get", StringUtil.RTrim( Ddo_logresponsavel_observacao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Selectedvalue_get", StringUtil.RTrim( Ddo_logresponsavel_observacao_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormFY2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wclogresponsavel.js", "?202062519829");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WCLogResponsavel" ;
      }

      public override String GetPgmdesc( )
      {
         return "WCLog Responsavel" ;
      }

      protected void WBFY0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wclogresponsavel.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_FY2( true) ;
         }
         else
         {
            wb_table1_2_FY2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_FY2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLogResponsavel_DemandaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A892LogResponsavel_DemandaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A892LogResponsavel_DemandaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogResponsavel_DemandaCod_Jsonclick, 0, "Attribute", "", "", "", edtLogResponsavel_DemandaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WCLogResponsavel.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflogresponsavel_datahora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflogresponsavel_datahora_Internalname, context.localUtil.TToC( AV23TFLogResponsavel_DataHora, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV23TFLogResponsavel_DataHora, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,24);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflogresponsavel_datahora_Jsonclick, 0, "Attribute", "", "", "", edtavTflogresponsavel_datahora_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WCLogResponsavel.htm");
            GxWebStd.gx_bitmap( context, edtavTflogresponsavel_datahora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflogresponsavel_datahora_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WCLogResponsavel.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflogresponsavel_datahora_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflogresponsavel_datahora_to_Internalname, context.localUtil.TToC( AV24TFLogResponsavel_DataHora_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV24TFLogResponsavel_DataHora_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,25);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflogresponsavel_datahora_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflogresponsavel_datahora_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WCLogResponsavel.htm");
            GxWebStd.gx_bitmap( context, edtavTflogresponsavel_datahora_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflogresponsavel_datahora_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WCLogResponsavel.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_logresponsavel_datahoraauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_logresponsavel_datahoraauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_logresponsavel_datahoraauxdate_Internalname, context.localUtil.Format(AV25DDO_LogResponsavel_DataHoraAuxDate, "99/99/99"), context.localUtil.Format( AV25DDO_LogResponsavel_DataHoraAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,27);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_logresponsavel_datahoraauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WCLogResponsavel.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_logresponsavel_datahoraauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WCLogResponsavel.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_logresponsavel_datahoraauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_logresponsavel_datahoraauxdateto_Internalname, context.localUtil.Format(AV26DDO_LogResponsavel_DataHoraAuxDateTo, "99/99/99"), context.localUtil.Format( AV26DDO_LogResponsavel_DataHoraAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,28);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_logresponsavel_datahoraauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WCLogResponsavel.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_logresponsavel_datahoraauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WCLogResponsavel.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflogresponsavel_prazo_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflogresponsavel_prazo_Internalname, context.localUtil.TToC( AV41TFLogResponsavel_Prazo, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV41TFLogResponsavel_Prazo, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,29);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflogresponsavel_prazo_Jsonclick, 0, "Attribute", "", "", "", edtavTflogresponsavel_prazo_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WCLogResponsavel.htm");
            GxWebStd.gx_bitmap( context, edtavTflogresponsavel_prazo_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflogresponsavel_prazo_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WCLogResponsavel.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflogresponsavel_prazo_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflogresponsavel_prazo_to_Internalname, context.localUtil.TToC( AV42TFLogResponsavel_Prazo_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV42TFLogResponsavel_Prazo_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,30);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflogresponsavel_prazo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflogresponsavel_prazo_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WCLogResponsavel.htm");
            GxWebStd.gx_bitmap( context, edtavTflogresponsavel_prazo_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflogresponsavel_prazo_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WCLogResponsavel.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_logresponsavel_prazoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_logresponsavel_prazoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_logresponsavel_prazoauxdate_Internalname, context.localUtil.Format(AV43DDO_LogResponsavel_PrazoAuxDate, "99/99/99"), context.localUtil.Format( AV43DDO_LogResponsavel_PrazoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,32);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_logresponsavel_prazoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WCLogResponsavel.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_logresponsavel_prazoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WCLogResponsavel.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_logresponsavel_prazoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_logresponsavel_prazoauxdateto_Internalname, context.localUtil.Format(AV44DDO_LogResponsavel_PrazoAuxDateTo, "99/99/99"), context.localUtil.Format( AV44DDO_LogResponsavel_PrazoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,33);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_logresponsavel_prazoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WCLogResponsavel.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_logresponsavel_prazoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WCLogResponsavel.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTflogresponsavel_observacao_Internalname, AV47TFLogResponsavel_Observacao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", 0, edtavTflogresponsavel_observacao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WCLogResponsavel.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTflogresponsavel_observacao_sel_Internalname, AV48TFLogResponsavel_Observacao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", 0, edtavTflogresponsavel_observacao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WCLogResponsavel.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_LOGRESPONSAVEL_DATAHORAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Internalname, AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", 0, edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WCLogResponsavel.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_LOGRESPONSAVEL_ACAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Internalname, AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", 0, edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WCLogResponsavel.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_LOGRESPONSAVEL_STATUSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Internalname, AV35ddo_LogResponsavel_StatusTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", 0, edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WCLogResponsavel.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Internalname, AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", 0, edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WCLogResponsavel.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_LOGRESPONSAVEL_PRAZOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Internalname, AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", 0, edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WCLogResponsavel.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_logresponsavel_observacaotitlecontrolidtoreplace_Internalname, AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", 0, edtavDdo_logresponsavel_observacaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WCLogResponsavel.htm");
         }
         wbLoad = true;
      }

      protected void STARTFY2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "WCLog Responsavel", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPFY0( ) ;
            }
         }
      }

      protected void WSFY2( )
      {
         STARTFY2( ) ;
         EVTFY2( ) ;
      }

      protected void EVTFY2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOGRESPONSAVEL_DATAHORA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11FY2 */
                                    E11FY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOGRESPONSAVEL_ACAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12FY2 */
                                    E12FY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOGRESPONSAVEL_STATUS.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13FY2 */
                                    E13FY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOGRESPONSAVEL_NOVOSTATUS.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14FY2 */
                                    E14FY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOGRESPONSAVEL_PRAZO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15FY2 */
                                    E15FY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOGRESPONSAVEL_OBSERVACAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16FY2 */
                                    E16FY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOPRAZOS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17FY2 */
                                    E17FY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCANCELARACAO'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18FY2 */
                                    E18FY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavTflogresponsavel_datahora_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFY0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFY0( ) ;
                              }
                              nGXsfl_9_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
                              SubsflControlProps_92( ) ;
                              A893LogResponsavel_DataHora = context.localUtil.CToT( cgiGet( edtLogResponsavel_DataHora_Internalname), 0);
                              cmbavEmissor.Name = cmbavEmissor_Internalname;
                              cmbavEmissor.CurrentValue = cgiGet( cmbavEmissor_Internalname);
                              AV16Emissor = (int)(NumberUtil.Val( cgiGet( cmbavEmissor_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavEmissor_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Emissor), 6, 0)));
                              cmbLogResponsavel_Acao.Name = cmbLogResponsavel_Acao_Internalname;
                              cmbLogResponsavel_Acao.CurrentValue = cgiGet( cmbLogResponsavel_Acao_Internalname);
                              A894LogResponsavel_Acao = cgiGet( cmbLogResponsavel_Acao_Internalname);
                              cmbavDestino.Name = cmbavDestino_Internalname;
                              cmbavDestino.CurrentValue = cgiGet( cmbavDestino_Internalname);
                              AV17Destino = (int)(NumberUtil.Val( cgiGet( cmbavDestino_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavDestino_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Destino), 6, 0)));
                              cmbLogResponsavel_Status.Name = cmbLogResponsavel_Status_Internalname;
                              cmbLogResponsavel_Status.CurrentValue = cgiGet( cmbLogResponsavel_Status_Internalname);
                              A1130LogResponsavel_Status = cgiGet( cmbLogResponsavel_Status_Internalname);
                              n1130LogResponsavel_Status = false;
                              cmbLogResponsavel_NovoStatus.Name = cmbLogResponsavel_NovoStatus_Internalname;
                              cmbLogResponsavel_NovoStatus.CurrentValue = cgiGet( cmbLogResponsavel_NovoStatus_Internalname);
                              A1234LogResponsavel_NovoStatus = cgiGet( cmbLogResponsavel_NovoStatus_Internalname);
                              n1234LogResponsavel_NovoStatus = false;
                              A1177LogResponsavel_Prazo = context.localUtil.CToT( cgiGet( edtLogResponsavel_Prazo_Internalname), 0);
                              n1177LogResponsavel_Prazo = false;
                              A1131LogResponsavel_Observacao = cgiGet( edtLogResponsavel_Observacao_Internalname);
                              n1131LogResponsavel_Observacao = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTflogresponsavel_datahora_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19FY2 */
                                          E19FY2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTflogresponsavel_datahora_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E20FY2 */
                                          E20FY2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTflogresponsavel_datahora_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E21FY2 */
                                          E21FY2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Tflogresponsavel_datahora Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFLOGRESPONSAVEL_DATAHORA"), 0) != AV23TFLogResponsavel_DataHora )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tflogresponsavel_datahora_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFLOGRESPONSAVEL_DATAHORA_TO"), 0) != AV24TFLogResponsavel_DataHora_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tflogresponsavel_prazo Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFLOGRESPONSAVEL_PRAZO"), 0) != AV41TFLogResponsavel_Prazo )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tflogresponsavel_prazo_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFLOGRESPONSAVEL_PRAZO_TO"), 0) != AV42TFLogResponsavel_Prazo_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tflogresponsavel_observacao Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFLOGRESPONSAVEL_OBSERVACAO"), AV47TFLogResponsavel_Observacao) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tflogresponsavel_observacao_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFLOGRESPONSAVEL_OBSERVACAO_SEL"), AV48TFLogResponsavel_Observacao_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTflogresponsavel_datahora_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPFY0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTflogresponsavel_datahora_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEFY2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormFY2( ) ;
            }
         }
      }

      protected void PAFY2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "vEMISSOR_" + sGXsfl_9_idx;
            cmbavEmissor.Name = GXCCtl;
            cmbavEmissor.WebTags = "";
            if ( cmbavEmissor.ItemCount > 0 )
            {
               AV16Emissor = (int)(NumberUtil.Val( cmbavEmissor.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16Emissor), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavEmissor_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Emissor), 6, 0)));
            }
            GXCCtl = "LOGRESPONSAVEL_ACAO_" + sGXsfl_9_idx;
            cmbLogResponsavel_Acao.Name = GXCCtl;
            cmbLogResponsavel_Acao.WebTags = "";
            cmbLogResponsavel_Acao.addItem("A", "Atribui", 0);
            cmbLogResponsavel_Acao.addItem("B", "StandBy", 0);
            cmbLogResponsavel_Acao.addItem("C", "Captura", 0);
            cmbLogResponsavel_Acao.addItem("R", "Rejeita", 0);
            cmbLogResponsavel_Acao.addItem("L", "Libera", 0);
            cmbLogResponsavel_Acao.addItem("E", "Encaminha", 0);
            cmbLogResponsavel_Acao.addItem("I", "Importa", 0);
            cmbLogResponsavel_Acao.addItem("S", "Solicita", 0);
            cmbLogResponsavel_Acao.addItem("D", "Diverg�ncia", 0);
            cmbLogResponsavel_Acao.addItem("V", "Resolvida", 0);
            cmbLogResponsavel_Acao.addItem("H", "Homologa", 0);
            cmbLogResponsavel_Acao.addItem("Q", "Liquida", 0);
            cmbLogResponsavel_Acao.addItem("P", "Fatura", 0);
            cmbLogResponsavel_Acao.addItem("O", "Aceite", 0);
            cmbLogResponsavel_Acao.addItem("N", "N�o Acata", 0);
            cmbLogResponsavel_Acao.addItem("M", "Autom�tica", 0);
            cmbLogResponsavel_Acao.addItem("F", "Cumprido", 0);
            cmbLogResponsavel_Acao.addItem("T", "Acata", 0);
            cmbLogResponsavel_Acao.addItem("X", "Cancela", 0);
            cmbLogResponsavel_Acao.addItem("NO", "Nota", 0);
            cmbLogResponsavel_Acao.addItem("U", "Altera", 0);
            cmbLogResponsavel_Acao.addItem("UN", "Rascunho", 0);
            cmbLogResponsavel_Acao.addItem("EA", "Em An�lise", 0);
            cmbLogResponsavel_Acao.addItem("RN", "Reuni�o", 0);
            cmbLogResponsavel_Acao.addItem("BK", "Desfaz", 0);
            cmbLogResponsavel_Acao.addItem("RE", "Reinicio", 0);
            cmbLogResponsavel_Acao.addItem("PR", "Prioridade", 0);
            if ( cmbLogResponsavel_Acao.ItemCount > 0 )
            {
               A894LogResponsavel_Acao = cmbLogResponsavel_Acao.getValidValue(A894LogResponsavel_Acao);
            }
            GXCCtl = "vDESTINO_" + sGXsfl_9_idx;
            cmbavDestino.Name = GXCCtl;
            cmbavDestino.WebTags = "";
            if ( cmbavDestino.ItemCount > 0 )
            {
               AV17Destino = (int)(NumberUtil.Val( cmbavDestino.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Destino), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavDestino_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Destino), 6, 0)));
            }
            GXCCtl = "LOGRESPONSAVEL_STATUS_" + sGXsfl_9_idx;
            cmbLogResponsavel_Status.Name = GXCCtl;
            cmbLogResponsavel_Status.WebTags = "";
            cmbLogResponsavel_Status.addItem("B", "Stand by", 0);
            cmbLogResponsavel_Status.addItem("S", "Solicitada", 0);
            cmbLogResponsavel_Status.addItem("E", "Em An�lise", 0);
            cmbLogResponsavel_Status.addItem("A", "Em execu��o", 0);
            cmbLogResponsavel_Status.addItem("R", "Resolvida", 0);
            cmbLogResponsavel_Status.addItem("C", "Conferida", 0);
            cmbLogResponsavel_Status.addItem("D", "Retornada", 0);
            cmbLogResponsavel_Status.addItem("H", "Homologada", 0);
            cmbLogResponsavel_Status.addItem("O", "Aceite", 0);
            cmbLogResponsavel_Status.addItem("P", "A Pagar", 0);
            cmbLogResponsavel_Status.addItem("L", "Liquidada", 0);
            cmbLogResponsavel_Status.addItem("X", "Cancelada", 0);
            cmbLogResponsavel_Status.addItem("N", "N�o Faturada", 0);
            cmbLogResponsavel_Status.addItem("J", "Planejamento", 0);
            cmbLogResponsavel_Status.addItem("I", "An�lise Planejamento", 0);
            cmbLogResponsavel_Status.addItem("T", "Validacao T�cnica", 0);
            cmbLogResponsavel_Status.addItem("Q", "Validacao Qualidade", 0);
            cmbLogResponsavel_Status.addItem("G", "Em Homologa��o", 0);
            cmbLogResponsavel_Status.addItem("M", "Valida��o Mensura��o", 0);
            cmbLogResponsavel_Status.addItem("U", "Rascunho", 0);
            if ( cmbLogResponsavel_Status.ItemCount > 0 )
            {
               A1130LogResponsavel_Status = cmbLogResponsavel_Status.getValidValue(A1130LogResponsavel_Status);
               n1130LogResponsavel_Status = false;
            }
            GXCCtl = "LOGRESPONSAVEL_NOVOSTATUS_" + sGXsfl_9_idx;
            cmbLogResponsavel_NovoStatus.Name = GXCCtl;
            cmbLogResponsavel_NovoStatus.WebTags = "";
            cmbLogResponsavel_NovoStatus.addItem("B", "Stand by", 0);
            cmbLogResponsavel_NovoStatus.addItem("S", "Solicitada", 0);
            cmbLogResponsavel_NovoStatus.addItem("E", "Em An�lise", 0);
            cmbLogResponsavel_NovoStatus.addItem("A", "Em execu��o", 0);
            cmbLogResponsavel_NovoStatus.addItem("R", "Resolvida", 0);
            cmbLogResponsavel_NovoStatus.addItem("C", "Conferida", 0);
            cmbLogResponsavel_NovoStatus.addItem("D", "Retornada", 0);
            cmbLogResponsavel_NovoStatus.addItem("H", "Homologada", 0);
            cmbLogResponsavel_NovoStatus.addItem("O", "Aceite", 0);
            cmbLogResponsavel_NovoStatus.addItem("P", "A Pagar", 0);
            cmbLogResponsavel_NovoStatus.addItem("L", "Liquidada", 0);
            cmbLogResponsavel_NovoStatus.addItem("X", "Cancelada", 0);
            cmbLogResponsavel_NovoStatus.addItem("N", "N�o Faturada", 0);
            cmbLogResponsavel_NovoStatus.addItem("J", "Planejamento", 0);
            cmbLogResponsavel_NovoStatus.addItem("I", "An�lise Planejamento", 0);
            cmbLogResponsavel_NovoStatus.addItem("T", "Validacao T�cnica", 0);
            cmbLogResponsavel_NovoStatus.addItem("Q", "Validacao Qualidade", 0);
            cmbLogResponsavel_NovoStatus.addItem("G", "Em Homologa��o", 0);
            cmbLogResponsavel_NovoStatus.addItem("M", "Valida��o Mensura��o", 0);
            cmbLogResponsavel_NovoStatus.addItem("U", "Rascunho", 0);
            if ( cmbLogResponsavel_NovoStatus.ItemCount > 0 )
            {
               A1234LogResponsavel_NovoStatus = cmbLogResponsavel_NovoStatus.getValidValue(A1234LogResponsavel_NovoStatus);
               n1234LogResponsavel_NovoStatus = false;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavTflogresponsavel_datahora_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_92( ) ;
         while ( nGXsfl_9_idx <= nRC_GXsfl_9 )
         {
            sendrow_92( ) ;
            nGXsfl_9_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_9_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_9_idx+1));
            sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
            SubsflControlProps_92( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       DateTime AV23TFLogResponsavel_DataHora ,
                                       DateTime AV24TFLogResponsavel_DataHora_To ,
                                       DateTime AV41TFLogResponsavel_Prazo ,
                                       DateTime AV42TFLogResponsavel_Prazo_To ,
                                       String AV47TFLogResponsavel_Observacao ,
                                       String AV48TFLogResponsavel_Observacao_Sel ,
                                       int AV7LogResponsavel_DemandaCod ,
                                       String AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace ,
                                       String AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace ,
                                       String AV35ddo_LogResponsavel_StatusTitleControlIdToReplace ,
                                       String AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace ,
                                       String AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace ,
                                       String AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace ,
                                       String AV57Pgmname ,
                                       IGxCollection AV30TFLogResponsavel_Acao_Sels ,
                                       IGxCollection AV34TFLogResponsavel_Status_Sels ,
                                       IGxCollection AV38TFLogResponsavel_NovoStatus_Sels ,
                                       int A1Usuario_Codigo ,
                                       String A58Usuario_PessoaNom ,
                                       int A896LogResponsavel_Owner ,
                                       int A891LogResponsavel_UsuarioCod ,
                                       String A493ContagemResultado_DemandaFM ,
                                       int AV52UltimaAcao ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV54String ,
                                       String A894LogResponsavel_Acao ,
                                       long A1797LogResponsavel_Codigo ,
                                       int A1228ContratadaUsuario_AreaTrabalhoCod ,
                                       int AV17Destino ,
                                       int A69ContratadaUsuario_UsuarioCod ,
                                       int AV16Emissor ,
                                       bool A2080AreaTrabalho_SelUsrPrestadora ,
                                       int A66ContratadaUsuario_ContratadaCod ,
                                       String A438Contratada_Sigla ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFFY2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOGRESPONSAVEL_DATAHORA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A893LogResponsavel_DataHora, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOGRESPONSAVEL_DATAHORA", context.localUtil.TToC( A893LogResponsavel_DataHora, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOGRESPONSAVEL_ACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A894LogResponsavel_Acao, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOGRESPONSAVEL_ACAO", StringUtil.RTrim( A894LogResponsavel_Acao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOGRESPONSAVEL_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1130LogResponsavel_Status, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOGRESPONSAVEL_STATUS", StringUtil.RTrim( A1130LogResponsavel_Status));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOGRESPONSAVEL_NOVOSTATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1234LogResponsavel_NovoStatus, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOGRESPONSAVEL_NOVOSTATUS", StringUtil.RTrim( A1234LogResponsavel_NovoStatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOGRESPONSAVEL_PRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1177LogResponsavel_Prazo, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOGRESPONSAVEL_PRAZO", context.localUtil.TToC( A1177LogResponsavel_Prazo, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOGRESPONSAVEL_OBSERVACAO", GetSecureSignedToken( sPrefix, A1131LogResponsavel_Observacao));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOGRESPONSAVEL_OBSERVACAO", A1131LogResponsavel_Observacao);
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFFY2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV57Pgmname = "WCLogResponsavel";
         context.Gx_err = 0;
         cmbavEmissor.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavEmissor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavEmissor.Enabled), 5, 0)));
         cmbavDestino.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDestino_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDestino.Enabled), 5, 0)));
      }

      protected void RFFY2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 9;
         /* Execute user event: E20FY2 */
         E20FY2 ();
         nGXsfl_9_idx = 1;
         sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
         SubsflControlProps_92( ) ;
         nGXsfl_9_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_92( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A894LogResponsavel_Acao ,
                                                 AV30TFLogResponsavel_Acao_Sels ,
                                                 A1130LogResponsavel_Status ,
                                                 AV34TFLogResponsavel_Status_Sels ,
                                                 A1234LogResponsavel_NovoStatus ,
                                                 AV38TFLogResponsavel_NovoStatus_Sels ,
                                                 AV23TFLogResponsavel_DataHora ,
                                                 AV24TFLogResponsavel_DataHora_To ,
                                                 AV30TFLogResponsavel_Acao_Sels.Count ,
                                                 AV34TFLogResponsavel_Status_Sels.Count ,
                                                 AV38TFLogResponsavel_NovoStatus_Sels.Count ,
                                                 AV41TFLogResponsavel_Prazo ,
                                                 AV42TFLogResponsavel_Prazo_To ,
                                                 AV48TFLogResponsavel_Observacao_Sel ,
                                                 AV47TFLogResponsavel_Observacao ,
                                                 A893LogResponsavel_DataHora ,
                                                 A1177LogResponsavel_Prazo ,
                                                 A1131LogResponsavel_Observacao ,
                                                 AV7LogResponsavel_DemandaCod ,
                                                 A892LogResponsavel_DemandaCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            lV47TFLogResponsavel_Observacao = StringUtil.Concat( StringUtil.RTrim( AV47TFLogResponsavel_Observacao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFLogResponsavel_Observacao", AV47TFLogResponsavel_Observacao);
            /* Using cursor H00FY2 */
            pr_default.execute(0, new Object[] {AV7LogResponsavel_DemandaCod, AV23TFLogResponsavel_DataHora, AV24TFLogResponsavel_DataHora_To, AV41TFLogResponsavel_Prazo, AV42TFLogResponsavel_Prazo_To, lV47TFLogResponsavel_Observacao, AV48TFLogResponsavel_Observacao_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_9_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A896LogResponsavel_Owner = H00FY2_A896LogResponsavel_Owner[0];
               A891LogResponsavel_UsuarioCod = H00FY2_A891LogResponsavel_UsuarioCod[0];
               n891LogResponsavel_UsuarioCod = H00FY2_n891LogResponsavel_UsuarioCod[0];
               A493ContagemResultado_DemandaFM = H00FY2_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00FY2_n493ContagemResultado_DemandaFM[0];
               A1797LogResponsavel_Codigo = H00FY2_A1797LogResponsavel_Codigo[0];
               A892LogResponsavel_DemandaCod = H00FY2_A892LogResponsavel_DemandaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
               n892LogResponsavel_DemandaCod = H00FY2_n892LogResponsavel_DemandaCod[0];
               A1131LogResponsavel_Observacao = H00FY2_A1131LogResponsavel_Observacao[0];
               n1131LogResponsavel_Observacao = H00FY2_n1131LogResponsavel_Observacao[0];
               A1177LogResponsavel_Prazo = H00FY2_A1177LogResponsavel_Prazo[0];
               n1177LogResponsavel_Prazo = H00FY2_n1177LogResponsavel_Prazo[0];
               A1234LogResponsavel_NovoStatus = H00FY2_A1234LogResponsavel_NovoStatus[0];
               n1234LogResponsavel_NovoStatus = H00FY2_n1234LogResponsavel_NovoStatus[0];
               A1130LogResponsavel_Status = H00FY2_A1130LogResponsavel_Status[0];
               n1130LogResponsavel_Status = H00FY2_n1130LogResponsavel_Status[0];
               A894LogResponsavel_Acao = H00FY2_A894LogResponsavel_Acao[0];
               A893LogResponsavel_DataHora = H00FY2_A893LogResponsavel_DataHora[0];
               A493ContagemResultado_DemandaFM = H00FY2_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00FY2_n493ContagemResultado_DemandaFM[0];
               /* Execute user event: E21FY2 */
               E21FY2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 9;
            WBFY0( ) ;
         }
         nGXsfl_9_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A894LogResponsavel_Acao ,
                                              AV30TFLogResponsavel_Acao_Sels ,
                                              A1130LogResponsavel_Status ,
                                              AV34TFLogResponsavel_Status_Sels ,
                                              A1234LogResponsavel_NovoStatus ,
                                              AV38TFLogResponsavel_NovoStatus_Sels ,
                                              AV23TFLogResponsavel_DataHora ,
                                              AV24TFLogResponsavel_DataHora_To ,
                                              AV30TFLogResponsavel_Acao_Sels.Count ,
                                              AV34TFLogResponsavel_Status_Sels.Count ,
                                              AV38TFLogResponsavel_NovoStatus_Sels.Count ,
                                              AV41TFLogResponsavel_Prazo ,
                                              AV42TFLogResponsavel_Prazo_To ,
                                              AV48TFLogResponsavel_Observacao_Sel ,
                                              AV47TFLogResponsavel_Observacao ,
                                              A893LogResponsavel_DataHora ,
                                              A1177LogResponsavel_Prazo ,
                                              A1131LogResponsavel_Observacao ,
                                              AV7LogResponsavel_DemandaCod ,
                                              A892LogResponsavel_DemandaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV47TFLogResponsavel_Observacao = StringUtil.Concat( StringUtil.RTrim( AV47TFLogResponsavel_Observacao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFLogResponsavel_Observacao", AV47TFLogResponsavel_Observacao);
         /* Using cursor H00FY3 */
         pr_default.execute(1, new Object[] {AV7LogResponsavel_DemandaCod, AV23TFLogResponsavel_DataHora, AV24TFLogResponsavel_DataHora_To, AV41TFLogResponsavel_Prazo, AV42TFLogResponsavel_Prazo_To, lV47TFLogResponsavel_Observacao, AV48TFLogResponsavel_Observacao_Sel});
         GRID_nRecordCount = H00FY3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV23TFLogResponsavel_DataHora, AV24TFLogResponsavel_DataHora_To, AV41TFLogResponsavel_Prazo, AV42TFLogResponsavel_Prazo_To, AV47TFLogResponsavel_Observacao, AV48TFLogResponsavel_Observacao_Sel, AV7LogResponsavel_DemandaCod, AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV35ddo_LogResponsavel_StatusTitleControlIdToReplace, AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace, AV57Pgmname, AV30TFLogResponsavel_Acao_Sels, AV34TFLogResponsavel_Status_Sels, AV38TFLogResponsavel_NovoStatus_Sels, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV52UltimaAcao, AV6WWPContext, AV54String, A894LogResponsavel_Acao, A1797LogResponsavel_Codigo, A1228ContratadaUsuario_AreaTrabalhoCod, AV17Destino, A69ContratadaUsuario_UsuarioCod, AV16Emissor, A2080AreaTrabalho_SelUsrPrestadora, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV23TFLogResponsavel_DataHora, AV24TFLogResponsavel_DataHora_To, AV41TFLogResponsavel_Prazo, AV42TFLogResponsavel_Prazo_To, AV47TFLogResponsavel_Observacao, AV48TFLogResponsavel_Observacao_Sel, AV7LogResponsavel_DemandaCod, AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV35ddo_LogResponsavel_StatusTitleControlIdToReplace, AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace, AV57Pgmname, AV30TFLogResponsavel_Acao_Sels, AV34TFLogResponsavel_Status_Sels, AV38TFLogResponsavel_NovoStatus_Sels, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV52UltimaAcao, AV6WWPContext, AV54String, A894LogResponsavel_Acao, A1797LogResponsavel_Codigo, A1228ContratadaUsuario_AreaTrabalhoCod, AV17Destino, A69ContratadaUsuario_UsuarioCod, AV16Emissor, A2080AreaTrabalho_SelUsrPrestadora, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV23TFLogResponsavel_DataHora, AV24TFLogResponsavel_DataHora_To, AV41TFLogResponsavel_Prazo, AV42TFLogResponsavel_Prazo_To, AV47TFLogResponsavel_Observacao, AV48TFLogResponsavel_Observacao_Sel, AV7LogResponsavel_DemandaCod, AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV35ddo_LogResponsavel_StatusTitleControlIdToReplace, AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace, AV57Pgmname, AV30TFLogResponsavel_Acao_Sels, AV34TFLogResponsavel_Status_Sels, AV38TFLogResponsavel_NovoStatus_Sels, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV52UltimaAcao, AV6WWPContext, AV54String, A894LogResponsavel_Acao, A1797LogResponsavel_Codigo, A1228ContratadaUsuario_AreaTrabalhoCod, AV17Destino, A69ContratadaUsuario_UsuarioCod, AV16Emissor, A2080AreaTrabalho_SelUsrPrestadora, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV23TFLogResponsavel_DataHora, AV24TFLogResponsavel_DataHora_To, AV41TFLogResponsavel_Prazo, AV42TFLogResponsavel_Prazo_To, AV47TFLogResponsavel_Observacao, AV48TFLogResponsavel_Observacao_Sel, AV7LogResponsavel_DemandaCod, AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV35ddo_LogResponsavel_StatusTitleControlIdToReplace, AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace, AV57Pgmname, AV30TFLogResponsavel_Acao_Sels, AV34TFLogResponsavel_Status_Sels, AV38TFLogResponsavel_NovoStatus_Sels, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV52UltimaAcao, AV6WWPContext, AV54String, A894LogResponsavel_Acao, A1797LogResponsavel_Codigo, A1228ContratadaUsuario_AreaTrabalhoCod, AV17Destino, A69ContratadaUsuario_UsuarioCod, AV16Emissor, A2080AreaTrabalho_SelUsrPrestadora, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV23TFLogResponsavel_DataHora, AV24TFLogResponsavel_DataHora_To, AV41TFLogResponsavel_Prazo, AV42TFLogResponsavel_Prazo_To, AV47TFLogResponsavel_Observacao, AV48TFLogResponsavel_Observacao_Sel, AV7LogResponsavel_DemandaCod, AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV35ddo_LogResponsavel_StatusTitleControlIdToReplace, AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace, AV57Pgmname, AV30TFLogResponsavel_Acao_Sels, AV34TFLogResponsavel_Status_Sels, AV38TFLogResponsavel_NovoStatus_Sels, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV52UltimaAcao, AV6WWPContext, AV54String, A894LogResponsavel_Acao, A1797LogResponsavel_Codigo, A1228ContratadaUsuario_AreaTrabalhoCod, AV17Destino, A69ContratadaUsuario_UsuarioCod, AV16Emissor, A2080AreaTrabalho_SelUsrPrestadora, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPFY0( )
      {
         /* Before Start, stand alone formulas. */
         AV57Pgmname = "WCLogResponsavel";
         context.Gx_err = 0;
         cmbavEmissor.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavEmissor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavEmissor.Enabled), 5, 0)));
         cmbavDestino.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDestino_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDestino.Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E19FY2 */
         E19FY2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV50DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA"), AV22LogResponsavel_DataHoraTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vLOGRESPONSAVEL_ACAOTITLEFILTERDATA"), AV28LogResponsavel_AcaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vLOGRESPONSAVEL_STATUSTITLEFILTERDATA"), AV32LogResponsavel_StatusTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA"), AV36LogResponsavel_NovoStatusTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA"), AV40LogResponsavel_PrazoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA"), AV46LogResponsavel_ObservacaoTitleFilterData);
            /* Read variables values. */
            A892LogResponsavel_DemandaCod = (int)(context.localUtil.CToN( cgiGet( edtLogResponsavel_DemandaCod_Internalname), ",", "."));
            n892LogResponsavel_DemandaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflogresponsavel_datahora_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLog Responsavel_Data Hora"}), 1, "vTFLOGRESPONSAVEL_DATAHORA");
               GX_FocusControl = edtavTflogresponsavel_datahora_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23TFLogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFLogResponsavel_DataHora", context.localUtil.TToC( AV23TFLogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV23TFLogResponsavel_DataHora = context.localUtil.CToT( cgiGet( edtavTflogresponsavel_datahora_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFLogResponsavel_DataHora", context.localUtil.TToC( AV23TFLogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflogresponsavel_datahora_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLog Responsavel_Data Hora_To"}), 1, "vTFLOGRESPONSAVEL_DATAHORA_TO");
               GX_FocusControl = edtavTflogresponsavel_datahora_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24TFLogResponsavel_DataHora_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFLogResponsavel_DataHora_To", context.localUtil.TToC( AV24TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV24TFLogResponsavel_DataHora_To = context.localUtil.CToT( cgiGet( edtavTflogresponsavel_datahora_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFLogResponsavel_DataHora_To", context.localUtil.TToC( AV24TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_logresponsavel_datahoraauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Log Responsavel_Data Hora Aux Date"}), 1, "vDDO_LOGRESPONSAVEL_DATAHORAAUXDATE");
               GX_FocusControl = edtavDdo_logresponsavel_datahoraauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25DDO_LogResponsavel_DataHoraAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DDO_LogResponsavel_DataHoraAuxDate", context.localUtil.Format(AV25DDO_LogResponsavel_DataHoraAuxDate, "99/99/99"));
            }
            else
            {
               AV25DDO_LogResponsavel_DataHoraAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_logresponsavel_datahoraauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DDO_LogResponsavel_DataHoraAuxDate", context.localUtil.Format(AV25DDO_LogResponsavel_DataHoraAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_logresponsavel_datahoraauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Log Responsavel_Data Hora Aux Date To"}), 1, "vDDO_LOGRESPONSAVEL_DATAHORAAUXDATETO");
               GX_FocusControl = edtavDdo_logresponsavel_datahoraauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26DDO_LogResponsavel_DataHoraAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DDO_LogResponsavel_DataHoraAuxDateTo", context.localUtil.Format(AV26DDO_LogResponsavel_DataHoraAuxDateTo, "99/99/99"));
            }
            else
            {
               AV26DDO_LogResponsavel_DataHoraAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_logresponsavel_datahoraauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DDO_LogResponsavel_DataHoraAuxDateTo", context.localUtil.Format(AV26DDO_LogResponsavel_DataHoraAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflogresponsavel_prazo_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLog Responsavel_Prazo"}), 1, "vTFLOGRESPONSAVEL_PRAZO");
               GX_FocusControl = edtavTflogresponsavel_prazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41TFLogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFLogResponsavel_Prazo", context.localUtil.TToC( AV41TFLogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV41TFLogResponsavel_Prazo = context.localUtil.CToT( cgiGet( edtavTflogresponsavel_prazo_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFLogResponsavel_Prazo", context.localUtil.TToC( AV41TFLogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflogresponsavel_prazo_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLog Responsavel_Prazo_To"}), 1, "vTFLOGRESPONSAVEL_PRAZO_TO");
               GX_FocusControl = edtavTflogresponsavel_prazo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42TFLogResponsavel_Prazo_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFLogResponsavel_Prazo_To", context.localUtil.TToC( AV42TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV42TFLogResponsavel_Prazo_To = context.localUtil.CToT( cgiGet( edtavTflogresponsavel_prazo_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFLogResponsavel_Prazo_To", context.localUtil.TToC( AV42TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_logresponsavel_prazoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Log Responsavel_Prazo Aux Date"}), 1, "vDDO_LOGRESPONSAVEL_PRAZOAUXDATE");
               GX_FocusControl = edtavDdo_logresponsavel_prazoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43DDO_LogResponsavel_PrazoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43DDO_LogResponsavel_PrazoAuxDate", context.localUtil.Format(AV43DDO_LogResponsavel_PrazoAuxDate, "99/99/99"));
            }
            else
            {
               AV43DDO_LogResponsavel_PrazoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_logresponsavel_prazoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43DDO_LogResponsavel_PrazoAuxDate", context.localUtil.Format(AV43DDO_LogResponsavel_PrazoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_logresponsavel_prazoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Log Responsavel_Prazo Aux Date To"}), 1, "vDDO_LOGRESPONSAVEL_PRAZOAUXDATETO");
               GX_FocusControl = edtavDdo_logresponsavel_prazoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44DDO_LogResponsavel_PrazoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44DDO_LogResponsavel_PrazoAuxDateTo", context.localUtil.Format(AV44DDO_LogResponsavel_PrazoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV44DDO_LogResponsavel_PrazoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_logresponsavel_prazoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44DDO_LogResponsavel_PrazoAuxDateTo", context.localUtil.Format(AV44DDO_LogResponsavel_PrazoAuxDateTo, "99/99/99"));
            }
            AV47TFLogResponsavel_Observacao = cgiGet( edtavTflogresponsavel_observacao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFLogResponsavel_Observacao", AV47TFLogResponsavel_Observacao);
            AV48TFLogResponsavel_Observacao_Sel = cgiGet( edtavTflogresponsavel_observacao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFLogResponsavel_Observacao_Sel", AV48TFLogResponsavel_Observacao_Sel);
            AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace = cgiGet( edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace", AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace);
            AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace = cgiGet( edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace", AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace);
            AV35ddo_LogResponsavel_StatusTitleControlIdToReplace = cgiGet( edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_LogResponsavel_StatusTitleControlIdToReplace", AV35ddo_LogResponsavel_StatusTitleControlIdToReplace);
            AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = cgiGet( edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace", AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace);
            AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace = cgiGet( edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace", AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace);
            AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace = cgiGet( edtavDdo_logresponsavel_observacaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace", AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_9 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_9"), ",", "."));
            wcpOAV7LogResponsavel_DemandaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7LogResponsavel_DemandaCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_logresponsavel_datahora_Caption = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Caption");
            Ddo_logresponsavel_datahora_Tooltip = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Tooltip");
            Ddo_logresponsavel_datahora_Cls = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Cls");
            Ddo_logresponsavel_datahora_Filteredtext_set = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Filteredtext_set");
            Ddo_logresponsavel_datahora_Filteredtextto_set = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Filteredtextto_set");
            Ddo_logresponsavel_datahora_Dropdownoptionstype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Dropdownoptionstype");
            Ddo_logresponsavel_datahora_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Titlecontrolidtoreplace");
            Ddo_logresponsavel_datahora_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Includesortasc"));
            Ddo_logresponsavel_datahora_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Includesortdsc"));
            Ddo_logresponsavel_datahora_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Includefilter"));
            Ddo_logresponsavel_datahora_Filtertype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Filtertype");
            Ddo_logresponsavel_datahora_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Filterisrange"));
            Ddo_logresponsavel_datahora_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Includedatalist"));
            Ddo_logresponsavel_datahora_Cleanfilter = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Cleanfilter");
            Ddo_logresponsavel_datahora_Rangefilterfrom = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Rangefilterfrom");
            Ddo_logresponsavel_datahora_Rangefilterto = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Rangefilterto");
            Ddo_logresponsavel_datahora_Searchbuttontext = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Searchbuttontext");
            Ddo_logresponsavel_acao_Caption = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Caption");
            Ddo_logresponsavel_acao_Tooltip = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Tooltip");
            Ddo_logresponsavel_acao_Cls = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Cls");
            Ddo_logresponsavel_acao_Selectedvalue_set = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Selectedvalue_set");
            Ddo_logresponsavel_acao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Dropdownoptionstype");
            Ddo_logresponsavel_acao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Titlecontrolidtoreplace");
            Ddo_logresponsavel_acao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Includesortasc"));
            Ddo_logresponsavel_acao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Includesortdsc"));
            Ddo_logresponsavel_acao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Includefilter"));
            Ddo_logresponsavel_acao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Includedatalist"));
            Ddo_logresponsavel_acao_Datalisttype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Datalisttype");
            Ddo_logresponsavel_acao_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Allowmultipleselection"));
            Ddo_logresponsavel_acao_Datalistfixedvalues = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Datalistfixedvalues");
            Ddo_logresponsavel_acao_Cleanfilter = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Cleanfilter");
            Ddo_logresponsavel_acao_Searchbuttontext = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Searchbuttontext");
            Ddo_logresponsavel_status_Caption = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Caption");
            Ddo_logresponsavel_status_Tooltip = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Tooltip");
            Ddo_logresponsavel_status_Cls = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Cls");
            Ddo_logresponsavel_status_Selectedvalue_set = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Selectedvalue_set");
            Ddo_logresponsavel_status_Dropdownoptionstype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Dropdownoptionstype");
            Ddo_logresponsavel_status_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Titlecontrolidtoreplace");
            Ddo_logresponsavel_status_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Includesortasc"));
            Ddo_logresponsavel_status_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Includesortdsc"));
            Ddo_logresponsavel_status_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Includefilter"));
            Ddo_logresponsavel_status_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Includedatalist"));
            Ddo_logresponsavel_status_Datalisttype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Datalisttype");
            Ddo_logresponsavel_status_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Allowmultipleselection"));
            Ddo_logresponsavel_status_Datalistfixedvalues = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Datalistfixedvalues");
            Ddo_logresponsavel_status_Cleanfilter = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Cleanfilter");
            Ddo_logresponsavel_status_Searchbuttontext = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Searchbuttontext");
            Ddo_logresponsavel_novostatus_Caption = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Caption");
            Ddo_logresponsavel_novostatus_Tooltip = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Tooltip");
            Ddo_logresponsavel_novostatus_Cls = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Cls");
            Ddo_logresponsavel_novostatus_Selectedvalue_set = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Selectedvalue_set");
            Ddo_logresponsavel_novostatus_Dropdownoptionstype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Dropdownoptionstype");
            Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Titlecontrolidtoreplace");
            Ddo_logresponsavel_novostatus_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Includesortasc"));
            Ddo_logresponsavel_novostatus_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Includesortdsc"));
            Ddo_logresponsavel_novostatus_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Includefilter"));
            Ddo_logresponsavel_novostatus_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Includedatalist"));
            Ddo_logresponsavel_novostatus_Datalisttype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Datalisttype");
            Ddo_logresponsavel_novostatus_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Allowmultipleselection"));
            Ddo_logresponsavel_novostatus_Datalistfixedvalues = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Datalistfixedvalues");
            Ddo_logresponsavel_novostatus_Cleanfilter = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Cleanfilter");
            Ddo_logresponsavel_novostatus_Searchbuttontext = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Searchbuttontext");
            Ddo_logresponsavel_prazo_Caption = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Caption");
            Ddo_logresponsavel_prazo_Tooltip = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Tooltip");
            Ddo_logresponsavel_prazo_Cls = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Cls");
            Ddo_logresponsavel_prazo_Filteredtext_set = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Filteredtext_set");
            Ddo_logresponsavel_prazo_Filteredtextto_set = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Filteredtextto_set");
            Ddo_logresponsavel_prazo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Dropdownoptionstype");
            Ddo_logresponsavel_prazo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Titlecontrolidtoreplace");
            Ddo_logresponsavel_prazo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Includesortasc"));
            Ddo_logresponsavel_prazo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Includesortdsc"));
            Ddo_logresponsavel_prazo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Includefilter"));
            Ddo_logresponsavel_prazo_Filtertype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Filtertype");
            Ddo_logresponsavel_prazo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Filterisrange"));
            Ddo_logresponsavel_prazo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Includedatalist"));
            Ddo_logresponsavel_prazo_Cleanfilter = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Cleanfilter");
            Ddo_logresponsavel_prazo_Rangefilterfrom = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Rangefilterfrom");
            Ddo_logresponsavel_prazo_Rangefilterto = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Rangefilterto");
            Ddo_logresponsavel_prazo_Searchbuttontext = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Searchbuttontext");
            Ddo_logresponsavel_observacao_Caption = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Caption");
            Ddo_logresponsavel_observacao_Tooltip = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Tooltip");
            Ddo_logresponsavel_observacao_Cls = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Cls");
            Ddo_logresponsavel_observacao_Filteredtext_set = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Filteredtext_set");
            Ddo_logresponsavel_observacao_Selectedvalue_set = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Selectedvalue_set");
            Ddo_logresponsavel_observacao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Dropdownoptionstype");
            Ddo_logresponsavel_observacao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Titlecontrolidtoreplace");
            Ddo_logresponsavel_observacao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Includesortasc"));
            Ddo_logresponsavel_observacao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Includesortdsc"));
            Ddo_logresponsavel_observacao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Includefilter"));
            Ddo_logresponsavel_observacao_Filtertype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Filtertype");
            Ddo_logresponsavel_observacao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Filterisrange"));
            Ddo_logresponsavel_observacao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Includedatalist"));
            Ddo_logresponsavel_observacao_Datalisttype = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Datalisttype");
            Ddo_logresponsavel_observacao_Datalistproc = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Datalistproc");
            Ddo_logresponsavel_observacao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_logresponsavel_observacao_Loadingdata = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Loadingdata");
            Ddo_logresponsavel_observacao_Cleanfilter = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Cleanfilter");
            Ddo_logresponsavel_observacao_Noresultsfound = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Noresultsfound");
            Ddo_logresponsavel_observacao_Searchbuttontext = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Searchbuttontext");
            Ddo_logresponsavel_datahora_Activeeventkey = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Activeeventkey");
            Ddo_logresponsavel_datahora_Filteredtext_get = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Filteredtext_get");
            Ddo_logresponsavel_datahora_Filteredtextto_get = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA_Filteredtextto_get");
            Ddo_logresponsavel_acao_Activeeventkey = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Activeeventkey");
            Ddo_logresponsavel_acao_Selectedvalue_get = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_ACAO_Selectedvalue_get");
            Ddo_logresponsavel_status_Activeeventkey = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Activeeventkey");
            Ddo_logresponsavel_status_Selectedvalue_get = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_STATUS_Selectedvalue_get");
            Ddo_logresponsavel_novostatus_Activeeventkey = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Activeeventkey");
            Ddo_logresponsavel_novostatus_Selectedvalue_get = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS_Selectedvalue_get");
            Ddo_logresponsavel_prazo_Activeeventkey = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Activeeventkey");
            Ddo_logresponsavel_prazo_Filteredtext_get = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Filteredtext_get");
            Ddo_logresponsavel_prazo_Filteredtextto_get = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_PRAZO_Filteredtextto_get");
            Ddo_logresponsavel_observacao_Activeeventkey = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Activeeventkey");
            Ddo_logresponsavel_observacao_Filteredtext_get = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Filteredtext_get");
            Ddo_logresponsavel_observacao_Selectedvalue_get = cgiGet( sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFLOGRESPONSAVEL_DATAHORA"), 0) != AV23TFLogResponsavel_DataHora )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFLOGRESPONSAVEL_DATAHORA_TO"), 0) != AV24TFLogResponsavel_DataHora_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFLOGRESPONSAVEL_PRAZO"), 0) != AV41TFLogResponsavel_Prazo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFLOGRESPONSAVEL_PRAZO_TO"), 0) != AV42TFLogResponsavel_Prazo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFLOGRESPONSAVEL_OBSERVACAO"), AV47TFLogResponsavel_Observacao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFLOGRESPONSAVEL_OBSERVACAO_SEL"), AV48TFLogResponsavel_Observacao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E19FY2 */
         E19FY2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19FY2( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTflogresponsavel_datahora_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTflogresponsavel_datahora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflogresponsavel_datahora_Visible), 5, 0)));
         edtavTflogresponsavel_datahora_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTflogresponsavel_datahora_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflogresponsavel_datahora_to_Visible), 5, 0)));
         edtavTflogresponsavel_prazo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTflogresponsavel_prazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflogresponsavel_prazo_Visible), 5, 0)));
         edtavTflogresponsavel_prazo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTflogresponsavel_prazo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflogresponsavel_prazo_to_Visible), 5, 0)));
         edtavTflogresponsavel_observacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTflogresponsavel_observacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflogresponsavel_observacao_Visible), 5, 0)));
         edtavTflogresponsavel_observacao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTflogresponsavel_observacao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflogresponsavel_observacao_sel_Visible), 5, 0)));
         Ddo_logresponsavel_datahora_Titlecontrolidtoreplace = subGrid_Internalname+"_LogResponsavel_DataHora";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_datahora_Internalname, "TitleControlIdToReplace", Ddo_logresponsavel_datahora_Titlecontrolidtoreplace);
         AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace = Ddo_logresponsavel_datahora_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace", AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace);
         edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_logresponsavel_acao_Titlecontrolidtoreplace = subGrid_Internalname+"_LogResponsavel_Acao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_acao_Internalname, "TitleControlIdToReplace", Ddo_logresponsavel_acao_Titlecontrolidtoreplace);
         AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace = Ddo_logresponsavel_acao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace", AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace);
         edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_logresponsavel_status_Titlecontrolidtoreplace = subGrid_Internalname+"_LogResponsavel_Status";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_status_Internalname, "TitleControlIdToReplace", Ddo_logresponsavel_status_Titlecontrolidtoreplace);
         AV35ddo_LogResponsavel_StatusTitleControlIdToReplace = Ddo_logresponsavel_status_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_LogResponsavel_StatusTitleControlIdToReplace", AV35ddo_LogResponsavel_StatusTitleControlIdToReplace);
         edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace = subGrid_Internalname+"_LogResponsavel_NovoStatus";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_novostatus_Internalname, "TitleControlIdToReplace", Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace);
         AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace", AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace);
         edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_logresponsavel_prazo_Titlecontrolidtoreplace = subGrid_Internalname+"_LogResponsavel_Prazo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_prazo_Internalname, "TitleControlIdToReplace", Ddo_logresponsavel_prazo_Titlecontrolidtoreplace);
         AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace = Ddo_logresponsavel_prazo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace", AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace);
         edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_logresponsavel_observacao_Titlecontrolidtoreplace = subGrid_Internalname+"_LogResponsavel_Observacao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_observacao_Internalname, "TitleControlIdToReplace", Ddo_logresponsavel_observacao_Titlecontrolidtoreplace);
         AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace = Ddo_logresponsavel_observacao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace", AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace);
         edtavDdo_logresponsavel_observacaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_logresponsavel_observacaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_logresponsavel_observacaotitlecontrolidtoreplace_Visible), 5, 0)));
         edtLogResponsavel_DemandaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLogResponsavel_DemandaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogResponsavel_DemandaCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV50DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV50DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E20FY2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV22LogResponsavel_DataHoraTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28LogResponsavel_AcaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32LogResponsavel_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36LogResponsavel_NovoStatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40LogResponsavel_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46LogResponsavel_ObservacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtLogResponsavel_DataHora_Titleformat = 2;
         edtLogResponsavel_DataHora_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLogResponsavel_DataHora_Internalname, "Title", edtLogResponsavel_DataHora_Title);
         cmbLogResponsavel_Acao_Titleformat = 2;
         cmbLogResponsavel_Acao.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "A��o", AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbLogResponsavel_Acao_Internalname, "Title", cmbLogResponsavel_Acao.Title.Text);
         cmbLogResponsavel_Status_Titleformat = 2;
         cmbLogResponsavel_Status.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status anterior", AV35ddo_LogResponsavel_StatusTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbLogResponsavel_Status_Internalname, "Title", cmbLogResponsavel_Status.Title.Text);
         cmbLogResponsavel_NovoStatus_Titleformat = 2;
         cmbLogResponsavel_NovoStatus.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Novo status", AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbLogResponsavel_NovoStatus_Internalname, "Title", cmbLogResponsavel_NovoStatus.Title.Text);
         edtLogResponsavel_Prazo_Titleformat = 2;
         edtLogResponsavel_Prazo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Prazo", AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLogResponsavel_Prazo_Internalname, "Title", edtLogResponsavel_Prazo_Title);
         edtLogResponsavel_Observacao_Titleformat = 2;
         edtLogResponsavel_Observacao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Observa��o", AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLogResponsavel_Observacao_Internalname, "Title", edtLogResponsavel_Observacao_Title);
         imgCancelaracao_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCancelaracao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgCancelaracao_Visible), 5, 0)));
         subGrid_Cellpadding = 5;
         subGrid_Cellspacing = 5;
         edtLogResponsavel_Observacao_Format = 2;
         tblUnnamedtable2_Height = 125;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblUnnamedtable2_Internalname, "Height", StringUtil.LTrim( StringUtil.Str( (decimal)(tblUnnamedtable2_Height), 9, 0)));
         AV54String = "BK S ";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54String", AV54String);
         /* Execute user subroutine: 'PRECARREGARCOMBOSDEUSUARIOS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV22LogResponsavel_DataHoraTitleFilterData", AV22LogResponsavel_DataHoraTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV28LogResponsavel_AcaoTitleFilterData", AV28LogResponsavel_AcaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV32LogResponsavel_StatusTitleFilterData", AV32LogResponsavel_StatusTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV36LogResponsavel_NovoStatusTitleFilterData", AV36LogResponsavel_NovoStatusTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV40LogResponsavel_PrazoTitleFilterData", AV40LogResponsavel_PrazoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV46LogResponsavel_ObservacaoTitleFilterData", AV46LogResponsavel_ObservacaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         cmbavEmissor.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16Emissor), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavEmissor_Internalname, "Values", cmbavEmissor.ToJavascriptSource());
         cmbavDestino.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Destino), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDestino_Internalname, "Values", cmbavDestino.ToJavascriptSource());
      }

      protected void E11FY2( )
      {
         /* Ddo_logresponsavel_datahora_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_logresponsavel_datahora_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV23TFLogResponsavel_DataHora = context.localUtil.CToT( Ddo_logresponsavel_datahora_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFLogResponsavel_DataHora", context.localUtil.TToC( AV23TFLogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
            AV24TFLogResponsavel_DataHora_To = context.localUtil.CToT( Ddo_logresponsavel_datahora_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFLogResponsavel_DataHora_To", context.localUtil.TToC( AV24TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV24TFLogResponsavel_DataHora_To) )
            {
               AV24TFLogResponsavel_DataHora_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV24TFLogResponsavel_DataHora_To)), (short)(DateTimeUtil.Month( AV24TFLogResponsavel_DataHora_To)), (short)(DateTimeUtil.Day( AV24TFLogResponsavel_DataHora_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFLogResponsavel_DataHora_To", context.localUtil.TToC( AV24TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E12FY2( )
      {
         /* Ddo_logresponsavel_acao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_logresponsavel_acao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV29TFLogResponsavel_Acao_SelsJson = Ddo_logresponsavel_acao_Selectedvalue_get;
            AV30TFLogResponsavel_Acao_Sels.FromJSonString(AV29TFLogResponsavel_Acao_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV30TFLogResponsavel_Acao_Sels", AV30TFLogResponsavel_Acao_Sels);
      }

      protected void E13FY2( )
      {
         /* Ddo_logresponsavel_status_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_logresponsavel_status_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV33TFLogResponsavel_Status_SelsJson = Ddo_logresponsavel_status_Selectedvalue_get;
            AV34TFLogResponsavel_Status_Sels.FromJSonString(AV33TFLogResponsavel_Status_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV34TFLogResponsavel_Status_Sels", AV34TFLogResponsavel_Status_Sels);
      }

      protected void E14FY2( )
      {
         /* Ddo_logresponsavel_novostatus_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_logresponsavel_novostatus_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFLogResponsavel_NovoStatus_SelsJson = Ddo_logresponsavel_novostatus_Selectedvalue_get;
            AV38TFLogResponsavel_NovoStatus_Sels.FromJSonString(AV37TFLogResponsavel_NovoStatus_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV38TFLogResponsavel_NovoStatus_Sels", AV38TFLogResponsavel_NovoStatus_Sels);
      }

      protected void E15FY2( )
      {
         /* Ddo_logresponsavel_prazo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_logresponsavel_prazo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV41TFLogResponsavel_Prazo = context.localUtil.CToT( Ddo_logresponsavel_prazo_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFLogResponsavel_Prazo", context.localUtil.TToC( AV41TFLogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
            AV42TFLogResponsavel_Prazo_To = context.localUtil.CToT( Ddo_logresponsavel_prazo_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFLogResponsavel_Prazo_To", context.localUtil.TToC( AV42TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV42TFLogResponsavel_Prazo_To) )
            {
               AV42TFLogResponsavel_Prazo_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV42TFLogResponsavel_Prazo_To)), (short)(DateTimeUtil.Month( AV42TFLogResponsavel_Prazo_To)), (short)(DateTimeUtil.Day( AV42TFLogResponsavel_Prazo_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFLogResponsavel_Prazo_To", context.localUtil.TToC( AV42TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E16FY2( )
      {
         /* Ddo_logresponsavel_observacao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_logresponsavel_observacao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFLogResponsavel_Observacao = Ddo_logresponsavel_observacao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFLogResponsavel_Observacao", AV47TFLogResponsavel_Observacao);
            AV48TFLogResponsavel_Observacao_Sel = Ddo_logresponsavel_observacao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFLogResponsavel_Observacao_Sel", AV48TFLogResponsavel_Observacao_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E21FY2( )
      {
         /* Grid_Load Routine */
         AV16Emissor = A896LogResponsavel_Owner;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavEmissor_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Emissor), 6, 0)));
         AV17Destino = A891LogResponsavel_UsuarioCod;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavDestino_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Destino), 6, 0)));
         AV51OS = A493ContagemResultado_DemandaFM;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51OS", AV51OS);
         if ( (0==AV52UltimaAcao) )
         {
            if ( ( AV16Emissor == AV6WWPContext.gxTpr_Userid ) && ( StringUtil.StringSearch( AV54String, StringUtil.Trim( A894LogResponsavel_Acao)+" ", 1) == 0 ) )
            {
               AV52UltimaAcao = (int)(A1797LogResponsavel_Codigo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52UltimaAcao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52UltimaAcao), 6, 0)));
            }
            else
            {
               AV52UltimaAcao = -1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52UltimaAcao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52UltimaAcao), 6, 0)));
               imgCancelaracao_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCancelaracao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgCancelaracao_Visible), 5, 0)));
            }
         }
         /* Execute user subroutine: 'CARREGAUSUARIOS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 9;
         }
         sendrow_92( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_9_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(9, GridRow);
         }
         cmbavEmissor.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16Emissor), 6, 0));
         cmbavDestino.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Destino), 6, 0));
      }

      protected void E17FY2( )
      {
         /* 'DoPrazos' Routine */
         context.PopUp(formatLink("wp_corretor.aspx") + "?" + UrlEncode("" +AV7LogResponsavel_DemandaCod), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E18FY2( )
      {
         /* 'DoCancelarAcao' Routine */
         context.PopUp(formatLink("wp_cancelaracao.aspx") + "?" + UrlEncode("" +AV52UltimaAcao) + "," + UrlEncode(StringUtil.RTrim(AV51OS)) + "," + UrlEncode("" +AV6WWPContext.gxTpr_Userid), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV57Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV57Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV57Pgmname+"GridState"), "");
         }
         AV58GXV1 = 1;
         while ( AV58GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV58GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_DATAHORA") == 0 )
            {
               AV23TFLogResponsavel_DataHora = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFLogResponsavel_DataHora", context.localUtil.TToC( AV23TFLogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
               AV24TFLogResponsavel_DataHora_To = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFLogResponsavel_DataHora_To", context.localUtil.TToC( AV24TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV23TFLogResponsavel_DataHora) )
               {
                  AV25DDO_LogResponsavel_DataHoraAuxDate = DateTimeUtil.ResetTime(AV23TFLogResponsavel_DataHora);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DDO_LogResponsavel_DataHoraAuxDate", context.localUtil.Format(AV25DDO_LogResponsavel_DataHoraAuxDate, "99/99/99"));
                  Ddo_logresponsavel_datahora_Filteredtext_set = context.localUtil.DToC( AV25DDO_LogResponsavel_DataHoraAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_datahora_Internalname, "FilteredText_set", Ddo_logresponsavel_datahora_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV24TFLogResponsavel_DataHora_To) )
               {
                  AV26DDO_LogResponsavel_DataHoraAuxDateTo = DateTimeUtil.ResetTime(AV24TFLogResponsavel_DataHora_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DDO_LogResponsavel_DataHoraAuxDateTo", context.localUtil.Format(AV26DDO_LogResponsavel_DataHoraAuxDateTo, "99/99/99"));
                  Ddo_logresponsavel_datahora_Filteredtextto_set = context.localUtil.DToC( AV26DDO_LogResponsavel_DataHoraAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_datahora_Internalname, "FilteredTextTo_set", Ddo_logresponsavel_datahora_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_ACAO_SEL") == 0 )
            {
               AV29TFLogResponsavel_Acao_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV30TFLogResponsavel_Acao_Sels.FromJSonString(AV29TFLogResponsavel_Acao_SelsJson);
               if ( ! ( AV30TFLogResponsavel_Acao_Sels.Count == 0 ) )
               {
                  Ddo_logresponsavel_acao_Selectedvalue_set = AV29TFLogResponsavel_Acao_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_acao_Internalname, "SelectedValue_set", Ddo_logresponsavel_acao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_STATUS_SEL") == 0 )
            {
               AV33TFLogResponsavel_Status_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV34TFLogResponsavel_Status_Sels.FromJSonString(AV33TFLogResponsavel_Status_SelsJson);
               if ( ! ( AV34TFLogResponsavel_Status_Sels.Count == 0 ) )
               {
                  Ddo_logresponsavel_status_Selectedvalue_set = AV33TFLogResponsavel_Status_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_status_Internalname, "SelectedValue_set", Ddo_logresponsavel_status_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_NOVOSTATUS_SEL") == 0 )
            {
               AV37TFLogResponsavel_NovoStatus_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV38TFLogResponsavel_NovoStatus_Sels.FromJSonString(AV37TFLogResponsavel_NovoStatus_SelsJson);
               if ( ! ( AV38TFLogResponsavel_NovoStatus_Sels.Count == 0 ) )
               {
                  Ddo_logresponsavel_novostatus_Selectedvalue_set = AV37TFLogResponsavel_NovoStatus_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_novostatus_Internalname, "SelectedValue_set", Ddo_logresponsavel_novostatus_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_PRAZO") == 0 )
            {
               AV41TFLogResponsavel_Prazo = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFLogResponsavel_Prazo", context.localUtil.TToC( AV41TFLogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
               AV42TFLogResponsavel_Prazo_To = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42TFLogResponsavel_Prazo_To", context.localUtil.TToC( AV42TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV41TFLogResponsavel_Prazo) )
               {
                  AV43DDO_LogResponsavel_PrazoAuxDate = DateTimeUtil.ResetTime(AV41TFLogResponsavel_Prazo);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43DDO_LogResponsavel_PrazoAuxDate", context.localUtil.Format(AV43DDO_LogResponsavel_PrazoAuxDate, "99/99/99"));
                  Ddo_logresponsavel_prazo_Filteredtext_set = context.localUtil.DToC( AV43DDO_LogResponsavel_PrazoAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_prazo_Internalname, "FilteredText_set", Ddo_logresponsavel_prazo_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV42TFLogResponsavel_Prazo_To) )
               {
                  AV44DDO_LogResponsavel_PrazoAuxDateTo = DateTimeUtil.ResetTime(AV42TFLogResponsavel_Prazo_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44DDO_LogResponsavel_PrazoAuxDateTo", context.localUtil.Format(AV44DDO_LogResponsavel_PrazoAuxDateTo, "99/99/99"));
                  Ddo_logresponsavel_prazo_Filteredtextto_set = context.localUtil.DToC( AV44DDO_LogResponsavel_PrazoAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_prazo_Internalname, "FilteredTextTo_set", Ddo_logresponsavel_prazo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_OBSERVACAO") == 0 )
            {
               AV47TFLogResponsavel_Observacao = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFLogResponsavel_Observacao", AV47TFLogResponsavel_Observacao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFLogResponsavel_Observacao)) )
               {
                  Ddo_logresponsavel_observacao_Filteredtext_set = AV47TFLogResponsavel_Observacao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_observacao_Internalname, "FilteredText_set", Ddo_logresponsavel_observacao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_OBSERVACAO_SEL") == 0 )
            {
               AV48TFLogResponsavel_Observacao_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFLogResponsavel_Observacao_Sel", AV48TFLogResponsavel_Observacao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFLogResponsavel_Observacao_Sel)) )
               {
                  Ddo_logresponsavel_observacao_Selectedvalue_set = AV48TFLogResponsavel_Observacao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_logresponsavel_observacao_Internalname, "SelectedValue_set", Ddo_logresponsavel_observacao_Selectedvalue_set);
               }
            }
            AV58GXV1 = (int)(AV58GXV1+1);
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV57Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV23TFLogResponsavel_DataHora) && (DateTime.MinValue==AV24TFLogResponsavel_DataHora_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOGRESPONSAVEL_DATAHORA";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV23TFLogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " ");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV24TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " ");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV30TFLogResponsavel_Acao_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOGRESPONSAVEL_ACAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV30TFLogResponsavel_Acao_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV34TFLogResponsavel_Status_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOGRESPONSAVEL_STATUS_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV34TFLogResponsavel_Status_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV38TFLogResponsavel_NovoStatus_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOGRESPONSAVEL_NOVOSTATUS_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV38TFLogResponsavel_NovoStatus_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV41TFLogResponsavel_Prazo) && (DateTime.MinValue==AV42TFLogResponsavel_Prazo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOGRESPONSAVEL_PRAZO";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV41TFLogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " ");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV42TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " ");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFLogResponsavel_Observacao)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOGRESPONSAVEL_OBSERVACAO";
            AV12GridStateFilterValue.gxTpr_Value = AV47TFLogResponsavel_Observacao;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFLogResponsavel_Observacao_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOGRESPONSAVEL_OBSERVACAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV48TFLogResponsavel_Observacao_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7LogResponsavel_DemandaCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&LOGRESPONSAVEL_DEMANDACOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7LogResponsavel_DemandaCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV57Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV57Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "LogResponsavel";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "LogResponsavel_DemandaCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7LogResponsavel_DemandaCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void S152( )
      {
         /* 'CARREGAUSUARIOS' Routine */
         /* Using cursor H00FY4 */
         pr_default.execute(2, new Object[] {AV17Destino, AV16Emissor, AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A69ContratadaUsuario_UsuarioCod = H00FY4_A69ContratadaUsuario_UsuarioCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = H00FY4_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = H00FY4_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A66ContratadaUsuario_ContratadaCod = H00FY4_A66ContratadaUsuario_ContratadaCod[0];
            A2080AreaTrabalho_SelUsrPrestadora = H00FY4_A2080AreaTrabalho_SelUsrPrestadora[0];
            n2080AreaTrabalho_SelUsrPrestadora = H00FY4_n2080AreaTrabalho_SelUsrPrestadora[0];
            A438Contratada_Sigla = H00FY4_A438Contratada_Sigla[0];
            n438Contratada_Sigla = H00FY4_n438Contratada_Sigla[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = H00FY4_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = H00FY4_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A438Contratada_Sigla = H00FY4_A438Contratada_Sigla[0];
            n438Contratada_Sigla = H00FY4_n438Contratada_Sigla[0];
            A2080AreaTrabalho_SelUsrPrestadora = H00FY4_A2080AreaTrabalho_SelUsrPrestadora[0];
            n2080AreaTrabalho_SelUsrPrestadora = H00FY4_n2080AreaTrabalho_SelUsrPrestadora[0];
            if ( A69ContratadaUsuario_UsuarioCod == AV16Emissor )
            {
               if ( ! A2080AreaTrabalho_SelUsrPrestadora && ! ( A66ContratadaUsuario_ContratadaCod == AV6WWPContext.gxTpr_Contratada_codigo ) && ! AV6WWPContext.gxTpr_Userehadministradorgam )
               {
                  cmbavEmissor.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod+900000), 6, 0)), A438Contratada_Sigla, 0);
                  AV16Emissor = (int)(A66ContratadaUsuario_ContratadaCod+900000);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavEmissor_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Emissor), 6, 0)));
               }
            }
            if ( A69ContratadaUsuario_UsuarioCod == AV17Destino )
            {
               if ( ! A2080AreaTrabalho_SelUsrPrestadora && ! ( A66ContratadaUsuario_ContratadaCod == AV6WWPContext.gxTpr_Contratada_codigo ) && ! AV6WWPContext.gxTpr_Userehadministradorgam )
               {
                  cmbavDestino.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod+900000), 6, 0)), A438Contratada_Sigla, 0);
                  AV17Destino = (int)(A66ContratadaUsuario_ContratadaCod+900000);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavDestino_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Destino), 6, 0)));
               }
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void S142( )
      {
         /* 'PRECARREGARCOMBOSDEUSUARIOS' Routine */
         cmbavEmissor.addItem("0", "(Nenhum)", 0);
         cmbavDestino.addItem("0", "(Nenhum)", 0);
         /* Using cursor H00FY5 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            A57Usuario_PessoaCod = H00FY5_A57Usuario_PessoaCod[0];
            A58Usuario_PessoaNom = H00FY5_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00FY5_n58Usuario_PessoaNom[0];
            A1Usuario_Codigo = H00FY5_A1Usuario_Codigo[0];
            A58Usuario_PessoaNom = H00FY5_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00FY5_n58Usuario_PessoaNom[0];
            cmbavEmissor.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)), A58Usuario_PessoaNom, 0);
            cmbavDestino.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)), A58Usuario_PessoaNom, 0);
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void wb_table1_2_FY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 5,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnprazos_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(9), 1, 0)+","+"null"+");", "Validar prazos", bttBtnprazos_Jsonclick, 5, "Valida os prazos das demandas na tela", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOPRAZOS\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WCLogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"9\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", subGrid_Cellpadding, subGrid_Cellspacing, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLogResponsavel_DataHora_Titleformat == 0 )
               {
                  context.SendWebValue( edtLogResponsavel_DataHora_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLogResponsavel_DataHora_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Emissor") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbLogResponsavel_Acao_Titleformat == 0 )
               {
                  context.SendWebValue( cmbLogResponsavel_Acao.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbLogResponsavel_Acao.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Destino") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbLogResponsavel_Status_Titleformat == 0 )
               {
                  context.SendWebValue( cmbLogResponsavel_Status.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbLogResponsavel_Status.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbLogResponsavel_NovoStatus_Titleformat == 0 )
               {
                  context.SendWebValue( cmbLogResponsavel_NovoStatus.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbLogResponsavel_NovoStatus.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLogResponsavel_Prazo_Titleformat == 0 )
               {
                  context.SendWebValue( edtLogResponsavel_Prazo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLogResponsavel_Prazo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLogResponsavel_Observacao_Titleformat == 0 )
               {
                  context.SendWebValue( edtLogResponsavel_Observacao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLogResponsavel_Observacao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A893LogResponsavel_DataHora, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLogResponsavel_DataHora_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLogResponsavel_DataHora_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Emissor), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavEmissor.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A894LogResponsavel_Acao));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbLogResponsavel_Acao.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbLogResponsavel_Acao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Destino), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavDestino.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1130LogResponsavel_Status));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbLogResponsavel_Status.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbLogResponsavel_Status_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1234LogResponsavel_NovoStatus));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbLogResponsavel_NovoStatus.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbLogResponsavel_NovoStatus_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1177LogResponsavel_Prazo, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLogResponsavel_Prazo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLogResponsavel_Prazo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1131LogResponsavel_Observacao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLogResponsavel_Observacao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLogResponsavel_Observacao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Format", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLogResponsavel_Observacao_Format), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 9 )
         {
            wbEnd = 0;
            nRC_GXsfl_9 = (short)(nGXsfl_9_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_19_FY2( true) ;
         }
         else
         {
            wb_table2_19_FY2( false) ;
         }
         return  ;
      }

      protected void wb_table2_19_FY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_FY2e( true) ;
         }
         else
         {
            wb_table1_2_FY2e( false) ;
         }
      }

      protected void wb_table2_19_FY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(tblUnnamedtable2_Height), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 5, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCancelaracao_Internalname, context.GetImagePath( "2fdb4136-545a-4ae2-9134-6a9035f31f51", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgCancelaracao_Visible, 1, "", "Cancelar �ltima a��o", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCancelaracao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCANCELARACAO\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WCLogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_19_FY2e( true) ;
         }
         else
         {
            wb_table2_19_FY2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7LogResponsavel_DemandaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7LogResponsavel_DemandaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAFY2( ) ;
         WSFY2( ) ;
         WEFY2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7LogResponsavel_DemandaCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAFY2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wclogresponsavel");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAFY2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7LogResponsavel_DemandaCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7LogResponsavel_DemandaCod), 6, 0)));
         }
         wcpOAV7LogResponsavel_DemandaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7LogResponsavel_DemandaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7LogResponsavel_DemandaCod != wcpOAV7LogResponsavel_DemandaCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7LogResponsavel_DemandaCod = AV7LogResponsavel_DemandaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7LogResponsavel_DemandaCod = cgiGet( sPrefix+"AV7LogResponsavel_DemandaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7LogResponsavel_DemandaCod) > 0 )
         {
            AV7LogResponsavel_DemandaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7LogResponsavel_DemandaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7LogResponsavel_DemandaCod), 6, 0)));
         }
         else
         {
            AV7LogResponsavel_DemandaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7LogResponsavel_DemandaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAFY2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSFY2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSFY2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7LogResponsavel_DemandaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7LogResponsavel_DemandaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7LogResponsavel_DemandaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7LogResponsavel_DemandaCod_CTRL", StringUtil.RTrim( sCtrlAV7LogResponsavel_DemandaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEFY2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020625191248");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("wclogresponsavel.js", "?2020625191248");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_92( )
      {
         edtLogResponsavel_DataHora_Internalname = sPrefix+"LOGRESPONSAVEL_DATAHORA_"+sGXsfl_9_idx;
         cmbavEmissor_Internalname = sPrefix+"vEMISSOR_"+sGXsfl_9_idx;
         cmbLogResponsavel_Acao_Internalname = sPrefix+"LOGRESPONSAVEL_ACAO_"+sGXsfl_9_idx;
         cmbavDestino_Internalname = sPrefix+"vDESTINO_"+sGXsfl_9_idx;
         cmbLogResponsavel_Status_Internalname = sPrefix+"LOGRESPONSAVEL_STATUS_"+sGXsfl_9_idx;
         cmbLogResponsavel_NovoStatus_Internalname = sPrefix+"LOGRESPONSAVEL_NOVOSTATUS_"+sGXsfl_9_idx;
         edtLogResponsavel_Prazo_Internalname = sPrefix+"LOGRESPONSAVEL_PRAZO_"+sGXsfl_9_idx;
         edtLogResponsavel_Observacao_Internalname = sPrefix+"LOGRESPONSAVEL_OBSERVACAO_"+sGXsfl_9_idx;
      }

      protected void SubsflControlProps_fel_92( )
      {
         edtLogResponsavel_DataHora_Internalname = sPrefix+"LOGRESPONSAVEL_DATAHORA_"+sGXsfl_9_fel_idx;
         cmbavEmissor_Internalname = sPrefix+"vEMISSOR_"+sGXsfl_9_fel_idx;
         cmbLogResponsavel_Acao_Internalname = sPrefix+"LOGRESPONSAVEL_ACAO_"+sGXsfl_9_fel_idx;
         cmbavDestino_Internalname = sPrefix+"vDESTINO_"+sGXsfl_9_fel_idx;
         cmbLogResponsavel_Status_Internalname = sPrefix+"LOGRESPONSAVEL_STATUS_"+sGXsfl_9_fel_idx;
         cmbLogResponsavel_NovoStatus_Internalname = sPrefix+"LOGRESPONSAVEL_NOVOSTATUS_"+sGXsfl_9_fel_idx;
         edtLogResponsavel_Prazo_Internalname = sPrefix+"LOGRESPONSAVEL_PRAZO_"+sGXsfl_9_fel_idx;
         edtLogResponsavel_Observacao_Internalname = sPrefix+"LOGRESPONSAVEL_OBSERVACAO_"+sGXsfl_9_fel_idx;
      }

      protected void sendrow_92( )
      {
         SubsflControlProps_92( ) ;
         WBFY0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_9_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_9_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_9_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLogResponsavel_DataHora_Internalname,context.localUtil.TToC( A893LogResponsavel_DataHora, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A893LogResponsavel_DataHora, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLogResponsavel_DataHora_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)9,(short)1,(short)0,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_9_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vEMISSOR_" + sGXsfl_9_idx;
               cmbavEmissor.Name = GXCCtl;
               cmbavEmissor.WebTags = "";
               if ( cmbavEmissor.ItemCount > 0 )
               {
                  AV16Emissor = (int)(NumberUtil.Val( cmbavEmissor.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16Emissor), 6, 0))), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavEmissor_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Emissor), 6, 0)));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavEmissor,(String)cmbavEmissor_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV16Emissor), 6, 0)),(short)1,(String)cmbavEmissor_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavEmissor.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbavEmissor.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16Emissor), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavEmissor_Internalname, "Values", (String)(cmbavEmissor.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_9_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "LOGRESPONSAVEL_ACAO_" + sGXsfl_9_idx;
               cmbLogResponsavel_Acao.Name = GXCCtl;
               cmbLogResponsavel_Acao.WebTags = "";
               cmbLogResponsavel_Acao.addItem("A", "Atribui", 0);
               cmbLogResponsavel_Acao.addItem("B", "StandBy", 0);
               cmbLogResponsavel_Acao.addItem("C", "Captura", 0);
               cmbLogResponsavel_Acao.addItem("R", "Rejeita", 0);
               cmbLogResponsavel_Acao.addItem("L", "Libera", 0);
               cmbLogResponsavel_Acao.addItem("E", "Encaminha", 0);
               cmbLogResponsavel_Acao.addItem("I", "Importa", 0);
               cmbLogResponsavel_Acao.addItem("S", "Solicita", 0);
               cmbLogResponsavel_Acao.addItem("D", "Diverg�ncia", 0);
               cmbLogResponsavel_Acao.addItem("V", "Resolvida", 0);
               cmbLogResponsavel_Acao.addItem("H", "Homologa", 0);
               cmbLogResponsavel_Acao.addItem("Q", "Liquida", 0);
               cmbLogResponsavel_Acao.addItem("P", "Fatura", 0);
               cmbLogResponsavel_Acao.addItem("O", "Aceite", 0);
               cmbLogResponsavel_Acao.addItem("N", "N�o Acata", 0);
               cmbLogResponsavel_Acao.addItem("M", "Autom�tica", 0);
               cmbLogResponsavel_Acao.addItem("F", "Cumprido", 0);
               cmbLogResponsavel_Acao.addItem("T", "Acata", 0);
               cmbLogResponsavel_Acao.addItem("X", "Cancela", 0);
               cmbLogResponsavel_Acao.addItem("NO", "Nota", 0);
               cmbLogResponsavel_Acao.addItem("U", "Altera", 0);
               cmbLogResponsavel_Acao.addItem("UN", "Rascunho", 0);
               cmbLogResponsavel_Acao.addItem("EA", "Em An�lise", 0);
               cmbLogResponsavel_Acao.addItem("RN", "Reuni�o", 0);
               cmbLogResponsavel_Acao.addItem("BK", "Desfaz", 0);
               cmbLogResponsavel_Acao.addItem("RE", "Reinicio", 0);
               cmbLogResponsavel_Acao.addItem("PR", "Prioridade", 0);
               if ( cmbLogResponsavel_Acao.ItemCount > 0 )
               {
                  A894LogResponsavel_Acao = cmbLogResponsavel_Acao.getValidValue(A894LogResponsavel_Acao);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbLogResponsavel_Acao,(String)cmbLogResponsavel_Acao_Internalname,StringUtil.RTrim( A894LogResponsavel_Acao),(short)1,(String)cmbLogResponsavel_Acao_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbLogResponsavel_Acao.CurrentValue = StringUtil.RTrim( A894LogResponsavel_Acao);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbLogResponsavel_Acao_Internalname, "Values", (String)(cmbLogResponsavel_Acao.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_9_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vDESTINO_" + sGXsfl_9_idx;
               cmbavDestino.Name = GXCCtl;
               cmbavDestino.WebTags = "";
               if ( cmbavDestino.ItemCount > 0 )
               {
                  AV17Destino = (int)(NumberUtil.Val( cmbavDestino.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Destino), 6, 0))), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavDestino_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Destino), 6, 0)));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavDestino,(String)cmbavDestino_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV17Destino), 6, 0)),(short)1,(String)cmbavDestino_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavDestino.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbavDestino.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Destino), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDestino_Internalname, "Values", (String)(cmbavDestino.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_9_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "LOGRESPONSAVEL_STATUS_" + sGXsfl_9_idx;
               cmbLogResponsavel_Status.Name = GXCCtl;
               cmbLogResponsavel_Status.WebTags = "";
               cmbLogResponsavel_Status.addItem("B", "Stand by", 0);
               cmbLogResponsavel_Status.addItem("S", "Solicitada", 0);
               cmbLogResponsavel_Status.addItem("E", "Em An�lise", 0);
               cmbLogResponsavel_Status.addItem("A", "Em execu��o", 0);
               cmbLogResponsavel_Status.addItem("R", "Resolvida", 0);
               cmbLogResponsavel_Status.addItem("C", "Conferida", 0);
               cmbLogResponsavel_Status.addItem("D", "Retornada", 0);
               cmbLogResponsavel_Status.addItem("H", "Homologada", 0);
               cmbLogResponsavel_Status.addItem("O", "Aceite", 0);
               cmbLogResponsavel_Status.addItem("P", "A Pagar", 0);
               cmbLogResponsavel_Status.addItem("L", "Liquidada", 0);
               cmbLogResponsavel_Status.addItem("X", "Cancelada", 0);
               cmbLogResponsavel_Status.addItem("N", "N�o Faturada", 0);
               cmbLogResponsavel_Status.addItem("J", "Planejamento", 0);
               cmbLogResponsavel_Status.addItem("I", "An�lise Planejamento", 0);
               cmbLogResponsavel_Status.addItem("T", "Validacao T�cnica", 0);
               cmbLogResponsavel_Status.addItem("Q", "Validacao Qualidade", 0);
               cmbLogResponsavel_Status.addItem("G", "Em Homologa��o", 0);
               cmbLogResponsavel_Status.addItem("M", "Valida��o Mensura��o", 0);
               cmbLogResponsavel_Status.addItem("U", "Rascunho", 0);
               if ( cmbLogResponsavel_Status.ItemCount > 0 )
               {
                  A1130LogResponsavel_Status = cmbLogResponsavel_Status.getValidValue(A1130LogResponsavel_Status);
                  n1130LogResponsavel_Status = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbLogResponsavel_Status,(String)cmbLogResponsavel_Status_Internalname,StringUtil.RTrim( A1130LogResponsavel_Status),(short)1,(String)cmbLogResponsavel_Status_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbLogResponsavel_Status.CurrentValue = StringUtil.RTrim( A1130LogResponsavel_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbLogResponsavel_Status_Internalname, "Values", (String)(cmbLogResponsavel_Status.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_9_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "LOGRESPONSAVEL_NOVOSTATUS_" + sGXsfl_9_idx;
               cmbLogResponsavel_NovoStatus.Name = GXCCtl;
               cmbLogResponsavel_NovoStatus.WebTags = "";
               cmbLogResponsavel_NovoStatus.addItem("B", "Stand by", 0);
               cmbLogResponsavel_NovoStatus.addItem("S", "Solicitada", 0);
               cmbLogResponsavel_NovoStatus.addItem("E", "Em An�lise", 0);
               cmbLogResponsavel_NovoStatus.addItem("A", "Em execu��o", 0);
               cmbLogResponsavel_NovoStatus.addItem("R", "Resolvida", 0);
               cmbLogResponsavel_NovoStatus.addItem("C", "Conferida", 0);
               cmbLogResponsavel_NovoStatus.addItem("D", "Retornada", 0);
               cmbLogResponsavel_NovoStatus.addItem("H", "Homologada", 0);
               cmbLogResponsavel_NovoStatus.addItem("O", "Aceite", 0);
               cmbLogResponsavel_NovoStatus.addItem("P", "A Pagar", 0);
               cmbLogResponsavel_NovoStatus.addItem("L", "Liquidada", 0);
               cmbLogResponsavel_NovoStatus.addItem("X", "Cancelada", 0);
               cmbLogResponsavel_NovoStatus.addItem("N", "N�o Faturada", 0);
               cmbLogResponsavel_NovoStatus.addItem("J", "Planejamento", 0);
               cmbLogResponsavel_NovoStatus.addItem("I", "An�lise Planejamento", 0);
               cmbLogResponsavel_NovoStatus.addItem("T", "Validacao T�cnica", 0);
               cmbLogResponsavel_NovoStatus.addItem("Q", "Validacao Qualidade", 0);
               cmbLogResponsavel_NovoStatus.addItem("G", "Em Homologa��o", 0);
               cmbLogResponsavel_NovoStatus.addItem("M", "Valida��o Mensura��o", 0);
               cmbLogResponsavel_NovoStatus.addItem("U", "Rascunho", 0);
               if ( cmbLogResponsavel_NovoStatus.ItemCount > 0 )
               {
                  A1234LogResponsavel_NovoStatus = cmbLogResponsavel_NovoStatus.getValidValue(A1234LogResponsavel_NovoStatus);
                  n1234LogResponsavel_NovoStatus = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbLogResponsavel_NovoStatus,(String)cmbLogResponsavel_NovoStatus_Internalname,StringUtil.RTrim( A1234LogResponsavel_NovoStatus),(short)1,(String)cmbLogResponsavel_NovoStatus_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbLogResponsavel_NovoStatus.CurrentValue = StringUtil.RTrim( A1234LogResponsavel_NovoStatus);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbLogResponsavel_NovoStatus_Internalname, "Values", (String)(cmbLogResponsavel_NovoStatus.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttributeNoWrap";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLogResponsavel_Prazo_Internalname,context.localUtil.TToC( A1177LogResponsavel_Prazo, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1177LogResponsavel_Prazo, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLogResponsavel_Prazo_Jsonclick,(short)0,(String)"BootstrapAttributeNoWrap",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)9,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLogResponsavel_Observacao_Internalname,(String)A1131LogResponsavel_Observacao,(String)A1131LogResponsavel_Observacao,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLogResponsavel_Observacao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)edtLogResponsavel_Observacao_Format,(short)9,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOGRESPONSAVEL_DATAHORA"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sPrefix+sGXsfl_9_idx, context.localUtil.Format( A893LogResponsavel_DataHora, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOGRESPONSAVEL_ACAO"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sPrefix+sGXsfl_9_idx, StringUtil.RTrim( context.localUtil.Format( A894LogResponsavel_Acao, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOGRESPONSAVEL_STATUS"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sPrefix+sGXsfl_9_idx, StringUtil.RTrim( context.localUtil.Format( A1130LogResponsavel_Status, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOGRESPONSAVEL_NOVOSTATUS"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sPrefix+sGXsfl_9_idx, StringUtil.RTrim( context.localUtil.Format( A1234LogResponsavel_NovoStatus, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOGRESPONSAVEL_PRAZO"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sPrefix+sGXsfl_9_idx, context.localUtil.Format( A1177LogResponsavel_Prazo, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOGRESPONSAVEL_OBSERVACAO"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sPrefix+sGXsfl_9_idx, A1131LogResponsavel_Observacao));
            GridContainer.AddRow(GridRow);
            nGXsfl_9_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_9_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_9_idx+1));
            sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
            SubsflControlProps_92( ) ;
         }
         /* End function sendrow_92 */
      }

      protected void init_default_properties( )
      {
         bttBtnprazos_Internalname = sPrefix+"BTNPRAZOS";
         edtLogResponsavel_DataHora_Internalname = sPrefix+"LOGRESPONSAVEL_DATAHORA";
         cmbavEmissor_Internalname = sPrefix+"vEMISSOR";
         cmbLogResponsavel_Acao_Internalname = sPrefix+"LOGRESPONSAVEL_ACAO";
         cmbavDestino_Internalname = sPrefix+"vDESTINO";
         cmbLogResponsavel_Status_Internalname = sPrefix+"LOGRESPONSAVEL_STATUS";
         cmbLogResponsavel_NovoStatus_Internalname = sPrefix+"LOGRESPONSAVEL_NOVOSTATUS";
         edtLogResponsavel_Prazo_Internalname = sPrefix+"LOGRESPONSAVEL_PRAZO";
         edtLogResponsavel_Observacao_Internalname = sPrefix+"LOGRESPONSAVEL_OBSERVACAO";
         imgCancelaracao_Internalname = sPrefix+"CANCELARACAO";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtLogResponsavel_DemandaCod_Internalname = sPrefix+"LOGRESPONSAVEL_DEMANDACOD";
         edtavTflogresponsavel_datahora_Internalname = sPrefix+"vTFLOGRESPONSAVEL_DATAHORA";
         edtavTflogresponsavel_datahora_to_Internalname = sPrefix+"vTFLOGRESPONSAVEL_DATAHORA_TO";
         edtavDdo_logresponsavel_datahoraauxdate_Internalname = sPrefix+"vDDO_LOGRESPONSAVEL_DATAHORAAUXDATE";
         edtavDdo_logresponsavel_datahoraauxdateto_Internalname = sPrefix+"vDDO_LOGRESPONSAVEL_DATAHORAAUXDATETO";
         divDdo_logresponsavel_datahoraauxdates_Internalname = sPrefix+"DDO_LOGRESPONSAVEL_DATAHORAAUXDATES";
         edtavTflogresponsavel_prazo_Internalname = sPrefix+"vTFLOGRESPONSAVEL_PRAZO";
         edtavTflogresponsavel_prazo_to_Internalname = sPrefix+"vTFLOGRESPONSAVEL_PRAZO_TO";
         edtavDdo_logresponsavel_prazoauxdate_Internalname = sPrefix+"vDDO_LOGRESPONSAVEL_PRAZOAUXDATE";
         edtavDdo_logresponsavel_prazoauxdateto_Internalname = sPrefix+"vDDO_LOGRESPONSAVEL_PRAZOAUXDATETO";
         divDdo_logresponsavel_prazoauxdates_Internalname = sPrefix+"DDO_LOGRESPONSAVEL_PRAZOAUXDATES";
         edtavTflogresponsavel_observacao_Internalname = sPrefix+"vTFLOGRESPONSAVEL_OBSERVACAO";
         edtavTflogresponsavel_observacao_sel_Internalname = sPrefix+"vTFLOGRESPONSAVEL_OBSERVACAO_SEL";
         Ddo_logresponsavel_datahora_Internalname = sPrefix+"DDO_LOGRESPONSAVEL_DATAHORA";
         edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE";
         Ddo_logresponsavel_acao_Internalname = sPrefix+"DDO_LOGRESPONSAVEL_ACAO";
         edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE";
         Ddo_logresponsavel_status_Internalname = sPrefix+"DDO_LOGRESPONSAVEL_STATUS";
         edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE";
         Ddo_logresponsavel_novostatus_Internalname = sPrefix+"DDO_LOGRESPONSAVEL_NOVOSTATUS";
         edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE";
         Ddo_logresponsavel_prazo_Internalname = sPrefix+"DDO_LOGRESPONSAVEL_PRAZO";
         edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE";
         Ddo_logresponsavel_observacao_Internalname = sPrefix+"DDO_LOGRESPONSAVEL_OBSERVACAO";
         edtavDdo_logresponsavel_observacaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtLogResponsavel_Observacao_Jsonclick = "";
         edtLogResponsavel_Prazo_Jsonclick = "";
         cmbLogResponsavel_NovoStatus_Jsonclick = "";
         cmbLogResponsavel_Status_Jsonclick = "";
         cmbavDestino_Jsonclick = "";
         cmbLogResponsavel_Acao_Jsonclick = "";
         cmbavEmissor_Jsonclick = "";
         edtLogResponsavel_DataHora_Jsonclick = "";
         imgCancelaracao_Visible = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtLogResponsavel_Observacao_Format = 0;
         cmbavDestino.Enabled = 0;
         cmbavEmissor.Enabled = 0;
         edtLogResponsavel_Observacao_Titleformat = 0;
         edtLogResponsavel_Prazo_Titleformat = 0;
         cmbLogResponsavel_NovoStatus_Titleformat = 0;
         cmbLogResponsavel_Status_Titleformat = 0;
         cmbLogResponsavel_Acao_Titleformat = 0;
         edtLogResponsavel_DataHora_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         subGrid_Cellspacing = 0;
         subGrid_Cellpadding = 0;
         tblUnnamedtable2_Height = 120;
         edtLogResponsavel_Observacao_Title = "Observa��o";
         edtLogResponsavel_Prazo_Title = "Prazo";
         cmbLogResponsavel_NovoStatus.Title.Text = "Novo status";
         cmbLogResponsavel_Status.Title.Text = "Status anterior";
         cmbLogResponsavel_Acao.Title.Text = "A��o";
         edtLogResponsavel_DataHora_Title = "Data";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_logresponsavel_observacaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Visible = 1;
         edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Visible = 1;
         edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Visible = 1;
         edtavTflogresponsavel_observacao_sel_Visible = 1;
         edtavTflogresponsavel_observacao_Visible = 1;
         edtavDdo_logresponsavel_prazoauxdateto_Jsonclick = "";
         edtavDdo_logresponsavel_prazoauxdate_Jsonclick = "";
         edtavTflogresponsavel_prazo_to_Jsonclick = "";
         edtavTflogresponsavel_prazo_to_Visible = 1;
         edtavTflogresponsavel_prazo_Jsonclick = "";
         edtavTflogresponsavel_prazo_Visible = 1;
         edtavDdo_logresponsavel_datahoraauxdateto_Jsonclick = "";
         edtavDdo_logresponsavel_datahoraauxdate_Jsonclick = "";
         edtavTflogresponsavel_datahora_to_Jsonclick = "";
         edtavTflogresponsavel_datahora_to_Visible = 1;
         edtavTflogresponsavel_datahora_Jsonclick = "";
         edtavTflogresponsavel_datahora_Visible = 1;
         edtLogResponsavel_DemandaCod_Jsonclick = "";
         edtLogResponsavel_DemandaCod_Visible = 1;
         Ddo_logresponsavel_observacao_Searchbuttontext = "Pesquisar";
         Ddo_logresponsavel_observacao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_logresponsavel_observacao_Cleanfilter = "Limpar pesquisa";
         Ddo_logresponsavel_observacao_Loadingdata = "Carregando dados...";
         Ddo_logresponsavel_observacao_Datalistupdateminimumcharacters = 0;
         Ddo_logresponsavel_observacao_Datalistproc = "GetWCLogResponsavelFilterData";
         Ddo_logresponsavel_observacao_Datalisttype = "Dynamic";
         Ddo_logresponsavel_observacao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_logresponsavel_observacao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_logresponsavel_observacao_Filtertype = "Character";
         Ddo_logresponsavel_observacao_Includefilter = Convert.ToBoolean( -1);
         Ddo_logresponsavel_observacao_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_observacao_Includesortasc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_observacao_Titlecontrolidtoreplace = "";
         Ddo_logresponsavel_observacao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_logresponsavel_observacao_Cls = "ColumnSettings";
         Ddo_logresponsavel_observacao_Tooltip = "Op��es";
         Ddo_logresponsavel_observacao_Caption = "";
         Ddo_logresponsavel_prazo_Searchbuttontext = "Pesquisar";
         Ddo_logresponsavel_prazo_Rangefilterto = "At�";
         Ddo_logresponsavel_prazo_Rangefilterfrom = "Desde";
         Ddo_logresponsavel_prazo_Cleanfilter = "Limpar pesquisa";
         Ddo_logresponsavel_prazo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_logresponsavel_prazo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_logresponsavel_prazo_Filtertype = "Date";
         Ddo_logresponsavel_prazo_Includefilter = Convert.ToBoolean( -1);
         Ddo_logresponsavel_prazo_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_prazo_Includesortasc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_prazo_Titlecontrolidtoreplace = "";
         Ddo_logresponsavel_prazo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_logresponsavel_prazo_Cls = "ColumnSettings";
         Ddo_logresponsavel_prazo_Tooltip = "Op��es";
         Ddo_logresponsavel_prazo_Caption = "";
         Ddo_logresponsavel_novostatus_Searchbuttontext = "Filtrar Selecionados";
         Ddo_logresponsavel_novostatus_Cleanfilter = "Limpar pesquisa";
         Ddo_logresponsavel_novostatus_Datalistfixedvalues = "B:Stand by,S:Solicitada,E:Em An�lise,A:Em execu��o,R:Resolvida,C:Conferida,D:Retornada,H:Homologada,O:Aceite,P:A Pagar,L:Liquidada,X:Cancelada,N:N�o Faturada,J:Planejamento,I:An�lise Planejamento,T:Validacao T�cnica,Q:Validacao Qualidade,G:Em Homologa��o,M:Valida��o Mensura��o,U:Rascunho";
         Ddo_logresponsavel_novostatus_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_logresponsavel_novostatus_Datalisttype = "FixedValues";
         Ddo_logresponsavel_novostatus_Includedatalist = Convert.ToBoolean( -1);
         Ddo_logresponsavel_novostatus_Includefilter = Convert.ToBoolean( 0);
         Ddo_logresponsavel_novostatus_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_novostatus_Includesortasc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace = "";
         Ddo_logresponsavel_novostatus_Dropdownoptionstype = "GridTitleSettings";
         Ddo_logresponsavel_novostatus_Cls = "ColumnSettings";
         Ddo_logresponsavel_novostatus_Tooltip = "Op��es";
         Ddo_logresponsavel_novostatus_Caption = "";
         Ddo_logresponsavel_status_Searchbuttontext = "Filtrar Selecionados";
         Ddo_logresponsavel_status_Cleanfilter = "Limpar pesquisa";
         Ddo_logresponsavel_status_Datalistfixedvalues = "B:Stand by,S:Solicitada,E:Em An�lise,A:Em execu��o,R:Resolvida,C:Conferida,D:Retornada,H:Homologada,O:Aceite,P:A Pagar,L:Liquidada,X:Cancelada,N:N�o Faturada,J:Planejamento,I:An�lise Planejamento,T:Validacao T�cnica,Q:Validacao Qualidade,G:Em Homologa��o,M:Valida��o Mensura��o,U:Rascunho";
         Ddo_logresponsavel_status_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_logresponsavel_status_Datalisttype = "FixedValues";
         Ddo_logresponsavel_status_Includedatalist = Convert.ToBoolean( -1);
         Ddo_logresponsavel_status_Includefilter = Convert.ToBoolean( 0);
         Ddo_logresponsavel_status_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_status_Includesortasc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_status_Titlecontrolidtoreplace = "";
         Ddo_logresponsavel_status_Dropdownoptionstype = "GridTitleSettings";
         Ddo_logresponsavel_status_Cls = "ColumnSettings";
         Ddo_logresponsavel_status_Tooltip = "Op��es";
         Ddo_logresponsavel_status_Caption = "";
         Ddo_logresponsavel_acao_Searchbuttontext = "Filtrar Selecionados";
         Ddo_logresponsavel_acao_Cleanfilter = "Limpar pesquisa";
         Ddo_logresponsavel_acao_Datalistfixedvalues = "A:Atribui,B:StandBy,C:Captura,R:Rejeita,L:Libera,E:Encaminha,I:Importa,S:Solicita,D:Diverg�ncia,V:Resolvida,H:Homologa,Q:Liquida,P:Fatura,O:Aceite,N:N�o Acata,M:Autom�tica,F:Cumprido,T:Acata,X:Cancela,NO:Nota,U:Altera,UN:Rascunho,EA:Em An�lise,RN:Reuni�o,BK:Desfaz,RE:Reinicio,PR:Prioridade";
         Ddo_logresponsavel_acao_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_logresponsavel_acao_Datalisttype = "FixedValues";
         Ddo_logresponsavel_acao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_logresponsavel_acao_Includefilter = Convert.ToBoolean( 0);
         Ddo_logresponsavel_acao_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_acao_Includesortasc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_acao_Titlecontrolidtoreplace = "";
         Ddo_logresponsavel_acao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_logresponsavel_acao_Cls = "ColumnSettings";
         Ddo_logresponsavel_acao_Tooltip = "Op��es";
         Ddo_logresponsavel_acao_Caption = "";
         Ddo_logresponsavel_datahora_Searchbuttontext = "Pesquisar";
         Ddo_logresponsavel_datahora_Rangefilterto = "At�";
         Ddo_logresponsavel_datahora_Rangefilterfrom = "Desde";
         Ddo_logresponsavel_datahora_Cleanfilter = "Limpar pesquisa";
         Ddo_logresponsavel_datahora_Includedatalist = Convert.ToBoolean( 0);
         Ddo_logresponsavel_datahora_Filterisrange = Convert.ToBoolean( -1);
         Ddo_logresponsavel_datahora_Filtertype = "Date";
         Ddo_logresponsavel_datahora_Includefilter = Convert.ToBoolean( -1);
         Ddo_logresponsavel_datahora_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_datahora_Includesortasc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_datahora_Titlecontrolidtoreplace = "";
         Ddo_logresponsavel_datahora_Dropdownoptionstype = "GridTitleSettings";
         Ddo_logresponsavel_datahora_Cls = "ColumnSettings";
         Ddo_logresponsavel_datahora_Tooltip = "Op��es";
         Ddo_logresponsavel_datahora_Caption = "";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV22LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV28LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV32LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV36LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV40LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46LogResponsavel_ObservacaoTitleFilterData',fld:'vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtLogResponsavel_DataHora_Titleformat',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat'},{av:'edtLogResponsavel_DataHora_Title',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{av:'cmbLogResponsavel_Acao'},{av:'cmbLogResponsavel_Status'},{av:'cmbLogResponsavel_NovoStatus'},{av:'edtLogResponsavel_Prazo_Titleformat',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat'},{av:'edtLogResponsavel_Prazo_Title',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'edtLogResponsavel_Observacao_Titleformat',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Titleformat'},{av:'edtLogResponsavel_Observacao_Title',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Title'},{av:'imgCancelaracao_Visible',ctrl:'CANCELARACAO',prop:'Visible'},{av:'subGrid_Cellpadding',ctrl:'GRID',prop:'Cellpadding'},{av:'subGrid_Cellspacing',ctrl:'GRID',prop:'Cellspacing'},{av:'edtLogResponsavel_Observacao_Format',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Format'},{av:'tblUnnamedtable2_Height',ctrl:'UNNAMEDTABLE2',prop:'Height'},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("DDO_LOGRESPONSAVEL_DATAHORA.ONOPTIONCLICKED","{handler:'E11FY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_logresponsavel_datahora_Activeeventkey',ctrl:'DDO_LOGRESPONSAVEL_DATAHORA',prop:'ActiveEventKey'},{av:'Ddo_logresponsavel_datahora_Filteredtext_get',ctrl:'DDO_LOGRESPONSAVEL_DATAHORA',prop:'FilteredText_get'},{av:'Ddo_logresponsavel_datahora_Filteredtextto_get',ctrl:'DDO_LOGRESPONSAVEL_DATAHORA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("DDO_LOGRESPONSAVEL_ACAO.ONOPTIONCLICKED","{handler:'E12FY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_logresponsavel_acao_Activeeventkey',ctrl:'DDO_LOGRESPONSAVEL_ACAO',prop:'ActiveEventKey'},{av:'Ddo_logresponsavel_acao_Selectedvalue_get',ctrl:'DDO_LOGRESPONSAVEL_ACAO',prop:'SelectedValue_get'}],oparms:[{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_LOGRESPONSAVEL_STATUS.ONOPTIONCLICKED","{handler:'E13FY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_logresponsavel_status_Activeeventkey',ctrl:'DDO_LOGRESPONSAVEL_STATUS',prop:'ActiveEventKey'},{av:'Ddo_logresponsavel_status_Selectedvalue_get',ctrl:'DDO_LOGRESPONSAVEL_STATUS',prop:'SelectedValue_get'}],oparms:[{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_LOGRESPONSAVEL_NOVOSTATUS.ONOPTIONCLICKED","{handler:'E14FY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_logresponsavel_novostatus_Activeeventkey',ctrl:'DDO_LOGRESPONSAVEL_NOVOSTATUS',prop:'ActiveEventKey'},{av:'Ddo_logresponsavel_novostatus_Selectedvalue_get',ctrl:'DDO_LOGRESPONSAVEL_NOVOSTATUS',prop:'SelectedValue_get'}],oparms:[{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_LOGRESPONSAVEL_PRAZO.ONOPTIONCLICKED","{handler:'E15FY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_logresponsavel_prazo_Activeeventkey',ctrl:'DDO_LOGRESPONSAVEL_PRAZO',prop:'ActiveEventKey'},{av:'Ddo_logresponsavel_prazo_Filteredtext_get',ctrl:'DDO_LOGRESPONSAVEL_PRAZO',prop:'FilteredText_get'},{av:'Ddo_logresponsavel_prazo_Filteredtextto_get',ctrl:'DDO_LOGRESPONSAVEL_PRAZO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("DDO_LOGRESPONSAVEL_OBSERVACAO.ONOPTIONCLICKED","{handler:'E16FY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_logresponsavel_observacao_Activeeventkey',ctrl:'DDO_LOGRESPONSAVEL_OBSERVACAO',prop:'ActiveEventKey'},{av:'Ddo_logresponsavel_observacao_Filteredtext_get',ctrl:'DDO_LOGRESPONSAVEL_OBSERVACAO',prop:'FilteredText_get'},{av:'Ddo_logresponsavel_observacao_Selectedvalue_get',ctrl:'DDO_LOGRESPONSAVEL_OBSERVACAO',prop:'SelectedValue_get'}],oparms:[{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E21FY2',iparms:[{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''}],oparms:[{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'AV51OS',fld:'vOS',pic:'',nv:''},{av:'imgCancelaracao_Visible',ctrl:'CANCELARACAO',prop:'Visible'},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOPRAZOS'","{handler:'E17FY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'DOCANCELARACAO'","{handler:'E18FY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV51OS',fld:'vOS',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV22LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV28LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV32LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV36LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV40LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46LogResponsavel_ObservacaoTitleFilterData',fld:'vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtLogResponsavel_DataHora_Titleformat',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat'},{av:'edtLogResponsavel_DataHora_Title',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{av:'cmbLogResponsavel_Acao'},{av:'cmbLogResponsavel_Status'},{av:'cmbLogResponsavel_NovoStatus'},{av:'edtLogResponsavel_Prazo_Titleformat',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat'},{av:'edtLogResponsavel_Prazo_Title',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'edtLogResponsavel_Observacao_Titleformat',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Titleformat'},{av:'edtLogResponsavel_Observacao_Title',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Title'},{av:'imgCancelaracao_Visible',ctrl:'CANCELARACAO',prop:'Visible'},{av:'subGrid_Cellpadding',ctrl:'GRID',prop:'Cellpadding'},{av:'subGrid_Cellspacing',ctrl:'GRID',prop:'Cellspacing'},{av:'edtLogResponsavel_Observacao_Format',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Format'},{av:'tblUnnamedtable2_Height',ctrl:'UNNAMEDTABLE2',prop:'Height'},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV22LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV28LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV32LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV36LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV40LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46LogResponsavel_ObservacaoTitleFilterData',fld:'vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtLogResponsavel_DataHora_Titleformat',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat'},{av:'edtLogResponsavel_DataHora_Title',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{av:'cmbLogResponsavel_Acao'},{av:'cmbLogResponsavel_Status'},{av:'cmbLogResponsavel_NovoStatus'},{av:'edtLogResponsavel_Prazo_Titleformat',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat'},{av:'edtLogResponsavel_Prazo_Title',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'edtLogResponsavel_Observacao_Titleformat',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Titleformat'},{av:'edtLogResponsavel_Observacao_Title',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Title'},{av:'imgCancelaracao_Visible',ctrl:'CANCELARACAO',prop:'Visible'},{av:'subGrid_Cellpadding',ctrl:'GRID',prop:'Cellpadding'},{av:'subGrid_Cellspacing',ctrl:'GRID',prop:'Cellspacing'},{av:'edtLogResponsavel_Observacao_Format',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Format'},{av:'tblUnnamedtable2_Height',ctrl:'UNNAMEDTABLE2',prop:'Height'},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV22LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV28LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV32LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV36LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV40LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46LogResponsavel_ObservacaoTitleFilterData',fld:'vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtLogResponsavel_DataHora_Titleformat',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat'},{av:'edtLogResponsavel_DataHora_Title',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{av:'cmbLogResponsavel_Acao'},{av:'cmbLogResponsavel_Status'},{av:'cmbLogResponsavel_NovoStatus'},{av:'edtLogResponsavel_Prazo_Titleformat',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat'},{av:'edtLogResponsavel_Prazo_Title',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'edtLogResponsavel_Observacao_Titleformat',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Titleformat'},{av:'edtLogResponsavel_Observacao_Title',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Title'},{av:'imgCancelaracao_Visible',ctrl:'CANCELARACAO',prop:'Visible'},{av:'subGrid_Cellpadding',ctrl:'GRID',prop:'Cellpadding'},{av:'subGrid_Cellspacing',ctrl:'GRID',prop:'Cellspacing'},{av:'edtLogResponsavel_Observacao_Format',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Format'},{av:'tblUnnamedtable2_Height',ctrl:'UNNAMEDTABLE2',prop:'Height'},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV22LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV28LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV32LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV36LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV40LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46LogResponsavel_ObservacaoTitleFilterData',fld:'vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtLogResponsavel_DataHora_Titleformat',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat'},{av:'edtLogResponsavel_DataHora_Title',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{av:'cmbLogResponsavel_Acao'},{av:'cmbLogResponsavel_Status'},{av:'cmbLogResponsavel_NovoStatus'},{av:'edtLogResponsavel_Prazo_Titleformat',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat'},{av:'edtLogResponsavel_Prazo_Title',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'edtLogResponsavel_Observacao_Titleformat',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Titleformat'},{av:'edtLogResponsavel_Observacao_Title',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Title'},{av:'imgCancelaracao_Visible',ctrl:'CANCELARACAO',prop:'Visible'},{av:'subGrid_Cellpadding',ctrl:'GRID',prop:'Cellpadding'},{av:'subGrid_Cellspacing',ctrl:'GRID',prop:'Cellspacing'},{av:'edtLogResponsavel_Observacao_Format',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Format'},{av:'tblUnnamedtable2_Height',ctrl:'UNNAMEDTABLE2',prop:'Height'},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Ddo_logresponsavel_datahora_Activeeventkey = "";
         Ddo_logresponsavel_datahora_Filteredtext_get = "";
         Ddo_logresponsavel_datahora_Filteredtextto_get = "";
         Ddo_logresponsavel_acao_Activeeventkey = "";
         Ddo_logresponsavel_acao_Selectedvalue_get = "";
         Ddo_logresponsavel_status_Activeeventkey = "";
         Ddo_logresponsavel_status_Selectedvalue_get = "";
         Ddo_logresponsavel_novostatus_Activeeventkey = "";
         Ddo_logresponsavel_novostatus_Selectedvalue_get = "";
         Ddo_logresponsavel_prazo_Activeeventkey = "";
         Ddo_logresponsavel_prazo_Filteredtext_get = "";
         Ddo_logresponsavel_prazo_Filteredtextto_get = "";
         Ddo_logresponsavel_observacao_Activeeventkey = "";
         Ddo_logresponsavel_observacao_Filteredtext_get = "";
         Ddo_logresponsavel_observacao_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV23TFLogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         AV24TFLogResponsavel_DataHora_To = (DateTime)(DateTime.MinValue);
         AV41TFLogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         AV42TFLogResponsavel_Prazo_To = (DateTime)(DateTime.MinValue);
         AV47TFLogResponsavel_Observacao = "";
         AV48TFLogResponsavel_Observacao_Sel = "";
         AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace = "";
         AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace = "";
         AV35ddo_LogResponsavel_StatusTitleControlIdToReplace = "";
         AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = "";
         AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace = "";
         AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace = "";
         AV57Pgmname = "";
         AV30TFLogResponsavel_Acao_Sels = new GxSimpleCollection();
         AV34TFLogResponsavel_Status_Sels = new GxSimpleCollection();
         AV38TFLogResponsavel_NovoStatus_Sels = new GxSimpleCollection();
         A58Usuario_PessoaNom = "";
         A493ContagemResultado_DemandaFM = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV54String = "";
         A894LogResponsavel_Acao = "";
         A438Contratada_Sigla = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV50DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV22LogResponsavel_DataHoraTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28LogResponsavel_AcaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32LogResponsavel_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36LogResponsavel_NovoStatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40LogResponsavel_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46LogResponsavel_ObservacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51OS = "";
         Ddo_logresponsavel_datahora_Filteredtext_set = "";
         Ddo_logresponsavel_datahora_Filteredtextto_set = "";
         Ddo_logresponsavel_acao_Selectedvalue_set = "";
         Ddo_logresponsavel_status_Selectedvalue_set = "";
         Ddo_logresponsavel_novostatus_Selectedvalue_set = "";
         Ddo_logresponsavel_prazo_Filteredtext_set = "";
         Ddo_logresponsavel_prazo_Filteredtextto_set = "";
         Ddo_logresponsavel_observacao_Filteredtext_set = "";
         Ddo_logresponsavel_observacao_Selectedvalue_set = "";
         GX_FocusControl = "";
         TempTags = "";
         AV25DDO_LogResponsavel_DataHoraAuxDate = DateTime.MinValue;
         AV26DDO_LogResponsavel_DataHoraAuxDateTo = DateTime.MinValue;
         AV43DDO_LogResponsavel_PrazoAuxDate = DateTime.MinValue;
         AV44DDO_LogResponsavel_PrazoAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         A1130LogResponsavel_Status = "";
         A1234LogResponsavel_NovoStatus = "";
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         A1131LogResponsavel_Observacao = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV47TFLogResponsavel_Observacao = "";
         H00FY2_A896LogResponsavel_Owner = new int[1] ;
         H00FY2_A891LogResponsavel_UsuarioCod = new int[1] ;
         H00FY2_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         H00FY2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00FY2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00FY2_A1797LogResponsavel_Codigo = new long[1] ;
         H00FY2_A892LogResponsavel_DemandaCod = new int[1] ;
         H00FY2_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00FY2_A1131LogResponsavel_Observacao = new String[] {""} ;
         H00FY2_n1131LogResponsavel_Observacao = new bool[] {false} ;
         H00FY2_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         H00FY2_n1177LogResponsavel_Prazo = new bool[] {false} ;
         H00FY2_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         H00FY2_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         H00FY2_A1130LogResponsavel_Status = new String[] {""} ;
         H00FY2_n1130LogResponsavel_Status = new bool[] {false} ;
         H00FY2_A894LogResponsavel_Acao = new String[] {""} ;
         H00FY2_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         H00FY3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV29TFLogResponsavel_Acao_SelsJson = "";
         AV33TFLogResponsavel_Status_SelsJson = "";
         AV37TFLogResponsavel_NovoStatus_SelsJson = "";
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         H00FY4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00FY4_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00FY4_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00FY4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00FY4_A2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         H00FY4_n2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         H00FY4_A438Contratada_Sigla = new String[] {""} ;
         H00FY4_n438Contratada_Sigla = new bool[] {false} ;
         H00FY5_A57Usuario_PessoaCod = new int[1] ;
         H00FY5_A58Usuario_PessoaNom = new String[] {""} ;
         H00FY5_n58Usuario_PessoaNom = new bool[] {false} ;
         H00FY5_A1Usuario_Codigo = new int[1] ;
         sStyleString = "";
         bttBtnprazos_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgCancelaracao_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7LogResponsavel_DemandaCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wclogresponsavel__default(),
            new Object[][] {
                new Object[] {
               H00FY2_A896LogResponsavel_Owner, H00FY2_A891LogResponsavel_UsuarioCod, H00FY2_n891LogResponsavel_UsuarioCod, H00FY2_A493ContagemResultado_DemandaFM, H00FY2_n493ContagemResultado_DemandaFM, H00FY2_A1797LogResponsavel_Codigo, H00FY2_A892LogResponsavel_DemandaCod, H00FY2_n892LogResponsavel_DemandaCod, H00FY2_A1131LogResponsavel_Observacao, H00FY2_n1131LogResponsavel_Observacao,
               H00FY2_A1177LogResponsavel_Prazo, H00FY2_n1177LogResponsavel_Prazo, H00FY2_A1234LogResponsavel_NovoStatus, H00FY2_n1234LogResponsavel_NovoStatus, H00FY2_A1130LogResponsavel_Status, H00FY2_n1130LogResponsavel_Status, H00FY2_A894LogResponsavel_Acao, H00FY2_A893LogResponsavel_DataHora
               }
               , new Object[] {
               H00FY3_AGRID_nRecordCount
               }
               , new Object[] {
               H00FY4_A69ContratadaUsuario_UsuarioCod, H00FY4_A1228ContratadaUsuario_AreaTrabalhoCod, H00FY4_n1228ContratadaUsuario_AreaTrabalhoCod, H00FY4_A66ContratadaUsuario_ContratadaCod, H00FY4_A2080AreaTrabalho_SelUsrPrestadora, H00FY4_n2080AreaTrabalho_SelUsrPrestadora, H00FY4_A438Contratada_Sigla, H00FY4_n438Contratada_Sigla
               }
               , new Object[] {
               H00FY5_A57Usuario_PessoaCod, H00FY5_A58Usuario_PessoaNom, H00FY5_n58Usuario_PessoaNom, H00FY5_A1Usuario_Codigo
               }
            }
         );
         AV57Pgmname = "WCLogResponsavel";
         /* GeneXus formulas. */
         AV57Pgmname = "WCLogResponsavel";
         context.Gx_err = 0;
         cmbavEmissor.Enabled = 0;
         cmbavDestino.Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_9 ;
      private short nGXsfl_9_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_9_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtLogResponsavel_DataHora_Titleformat ;
      private short cmbLogResponsavel_Acao_Titleformat ;
      private short cmbLogResponsavel_Status_Titleformat ;
      private short cmbLogResponsavel_NovoStatus_Titleformat ;
      private short edtLogResponsavel_Prazo_Titleformat ;
      private short edtLogResponsavel_Observacao_Titleformat ;
      private short subGrid_Cellpadding ;
      private short subGrid_Cellspacing ;
      private short edtLogResponsavel_Observacao_Format ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7LogResponsavel_DemandaCod ;
      private int wcpOAV7LogResponsavel_DemandaCod ;
      private int subGrid_Rows ;
      private int A1Usuario_Codigo ;
      private int A896LogResponsavel_Owner ;
      private int A891LogResponsavel_UsuarioCod ;
      private int AV52UltimaAcao ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int AV17Destino ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int AV16Emissor ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int Ddo_logresponsavel_observacao_Datalistupdateminimumcharacters ;
      private int A892LogResponsavel_DemandaCod ;
      private int edtLogResponsavel_DemandaCod_Visible ;
      private int edtavTflogresponsavel_datahora_Visible ;
      private int edtavTflogresponsavel_datahora_to_Visible ;
      private int edtavTflogresponsavel_prazo_Visible ;
      private int edtavTflogresponsavel_prazo_to_Visible ;
      private int edtavTflogresponsavel_observacao_Visible ;
      private int edtavTflogresponsavel_observacao_sel_Visible ;
      private int edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Visible ;
      private int edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Visible ;
      private int edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Visible ;
      private int edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_logresponsavel_observacaotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV30TFLogResponsavel_Acao_Sels_Count ;
      private int AV34TFLogResponsavel_Status_Sels_Count ;
      private int AV38TFLogResponsavel_NovoStatus_Sels_Count ;
      private int imgCancelaracao_Visible ;
      private int tblUnnamedtable2_Height ;
      private int AV58GXV1 ;
      private int A57Usuario_PessoaCod ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long A1797LogResponsavel_Codigo ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Ddo_logresponsavel_datahora_Activeeventkey ;
      private String Ddo_logresponsavel_datahora_Filteredtext_get ;
      private String Ddo_logresponsavel_datahora_Filteredtextto_get ;
      private String Ddo_logresponsavel_acao_Activeeventkey ;
      private String Ddo_logresponsavel_acao_Selectedvalue_get ;
      private String Ddo_logresponsavel_status_Activeeventkey ;
      private String Ddo_logresponsavel_status_Selectedvalue_get ;
      private String Ddo_logresponsavel_novostatus_Activeeventkey ;
      private String Ddo_logresponsavel_novostatus_Selectedvalue_get ;
      private String Ddo_logresponsavel_prazo_Activeeventkey ;
      private String Ddo_logresponsavel_prazo_Filteredtext_get ;
      private String Ddo_logresponsavel_prazo_Filteredtextto_get ;
      private String Ddo_logresponsavel_observacao_Activeeventkey ;
      private String Ddo_logresponsavel_observacao_Filteredtext_get ;
      private String Ddo_logresponsavel_observacao_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_9_idx="0001" ;
      private String AV57Pgmname ;
      private String A58Usuario_PessoaNom ;
      private String AV54String ;
      private String A894LogResponsavel_Acao ;
      private String cmbavDestino_Internalname ;
      private String cmbavEmissor_Internalname ;
      private String A438Contratada_Sigla ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_logresponsavel_datahora_Caption ;
      private String Ddo_logresponsavel_datahora_Tooltip ;
      private String Ddo_logresponsavel_datahora_Cls ;
      private String Ddo_logresponsavel_datahora_Filteredtext_set ;
      private String Ddo_logresponsavel_datahora_Filteredtextto_set ;
      private String Ddo_logresponsavel_datahora_Dropdownoptionstype ;
      private String Ddo_logresponsavel_datahora_Titlecontrolidtoreplace ;
      private String Ddo_logresponsavel_datahora_Filtertype ;
      private String Ddo_logresponsavel_datahora_Cleanfilter ;
      private String Ddo_logresponsavel_datahora_Rangefilterfrom ;
      private String Ddo_logresponsavel_datahora_Rangefilterto ;
      private String Ddo_logresponsavel_datahora_Searchbuttontext ;
      private String Ddo_logresponsavel_acao_Caption ;
      private String Ddo_logresponsavel_acao_Tooltip ;
      private String Ddo_logresponsavel_acao_Cls ;
      private String Ddo_logresponsavel_acao_Selectedvalue_set ;
      private String Ddo_logresponsavel_acao_Dropdownoptionstype ;
      private String Ddo_logresponsavel_acao_Titlecontrolidtoreplace ;
      private String Ddo_logresponsavel_acao_Datalisttype ;
      private String Ddo_logresponsavel_acao_Datalistfixedvalues ;
      private String Ddo_logresponsavel_acao_Cleanfilter ;
      private String Ddo_logresponsavel_acao_Searchbuttontext ;
      private String Ddo_logresponsavel_status_Caption ;
      private String Ddo_logresponsavel_status_Tooltip ;
      private String Ddo_logresponsavel_status_Cls ;
      private String Ddo_logresponsavel_status_Selectedvalue_set ;
      private String Ddo_logresponsavel_status_Dropdownoptionstype ;
      private String Ddo_logresponsavel_status_Titlecontrolidtoreplace ;
      private String Ddo_logresponsavel_status_Datalisttype ;
      private String Ddo_logresponsavel_status_Datalistfixedvalues ;
      private String Ddo_logresponsavel_status_Cleanfilter ;
      private String Ddo_logresponsavel_status_Searchbuttontext ;
      private String Ddo_logresponsavel_novostatus_Caption ;
      private String Ddo_logresponsavel_novostatus_Tooltip ;
      private String Ddo_logresponsavel_novostatus_Cls ;
      private String Ddo_logresponsavel_novostatus_Selectedvalue_set ;
      private String Ddo_logresponsavel_novostatus_Dropdownoptionstype ;
      private String Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace ;
      private String Ddo_logresponsavel_novostatus_Datalisttype ;
      private String Ddo_logresponsavel_novostatus_Datalistfixedvalues ;
      private String Ddo_logresponsavel_novostatus_Cleanfilter ;
      private String Ddo_logresponsavel_novostatus_Searchbuttontext ;
      private String Ddo_logresponsavel_prazo_Caption ;
      private String Ddo_logresponsavel_prazo_Tooltip ;
      private String Ddo_logresponsavel_prazo_Cls ;
      private String Ddo_logresponsavel_prazo_Filteredtext_set ;
      private String Ddo_logresponsavel_prazo_Filteredtextto_set ;
      private String Ddo_logresponsavel_prazo_Dropdownoptionstype ;
      private String Ddo_logresponsavel_prazo_Titlecontrolidtoreplace ;
      private String Ddo_logresponsavel_prazo_Filtertype ;
      private String Ddo_logresponsavel_prazo_Cleanfilter ;
      private String Ddo_logresponsavel_prazo_Rangefilterfrom ;
      private String Ddo_logresponsavel_prazo_Rangefilterto ;
      private String Ddo_logresponsavel_prazo_Searchbuttontext ;
      private String Ddo_logresponsavel_observacao_Caption ;
      private String Ddo_logresponsavel_observacao_Tooltip ;
      private String Ddo_logresponsavel_observacao_Cls ;
      private String Ddo_logresponsavel_observacao_Filteredtext_set ;
      private String Ddo_logresponsavel_observacao_Selectedvalue_set ;
      private String Ddo_logresponsavel_observacao_Dropdownoptionstype ;
      private String Ddo_logresponsavel_observacao_Titlecontrolidtoreplace ;
      private String Ddo_logresponsavel_observacao_Filtertype ;
      private String Ddo_logresponsavel_observacao_Datalisttype ;
      private String Ddo_logresponsavel_observacao_Datalistproc ;
      private String Ddo_logresponsavel_observacao_Loadingdata ;
      private String Ddo_logresponsavel_observacao_Cleanfilter ;
      private String Ddo_logresponsavel_observacao_Noresultsfound ;
      private String Ddo_logresponsavel_observacao_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtLogResponsavel_DemandaCod_Internalname ;
      private String edtLogResponsavel_DemandaCod_Jsonclick ;
      private String TempTags ;
      private String edtavTflogresponsavel_datahora_Internalname ;
      private String edtavTflogresponsavel_datahora_Jsonclick ;
      private String edtavTflogresponsavel_datahora_to_Internalname ;
      private String edtavTflogresponsavel_datahora_to_Jsonclick ;
      private String divDdo_logresponsavel_datahoraauxdates_Internalname ;
      private String edtavDdo_logresponsavel_datahoraauxdate_Internalname ;
      private String edtavDdo_logresponsavel_datahoraauxdate_Jsonclick ;
      private String edtavDdo_logresponsavel_datahoraauxdateto_Internalname ;
      private String edtavDdo_logresponsavel_datahoraauxdateto_Jsonclick ;
      private String edtavTflogresponsavel_prazo_Internalname ;
      private String edtavTflogresponsavel_prazo_Jsonclick ;
      private String edtavTflogresponsavel_prazo_to_Internalname ;
      private String edtavTflogresponsavel_prazo_to_Jsonclick ;
      private String divDdo_logresponsavel_prazoauxdates_Internalname ;
      private String edtavDdo_logresponsavel_prazoauxdate_Internalname ;
      private String edtavDdo_logresponsavel_prazoauxdate_Jsonclick ;
      private String edtavDdo_logresponsavel_prazoauxdateto_Internalname ;
      private String edtavDdo_logresponsavel_prazoauxdateto_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTflogresponsavel_observacao_Internalname ;
      private String edtavTflogresponsavel_observacao_sel_Internalname ;
      private String edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_logresponsavel_observacaotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtLogResponsavel_DataHora_Internalname ;
      private String cmbLogResponsavel_Acao_Internalname ;
      private String cmbLogResponsavel_Status_Internalname ;
      private String A1130LogResponsavel_Status ;
      private String cmbLogResponsavel_NovoStatus_Internalname ;
      private String A1234LogResponsavel_NovoStatus ;
      private String edtLogResponsavel_Prazo_Internalname ;
      private String edtLogResponsavel_Observacao_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String subGrid_Internalname ;
      private String Ddo_logresponsavel_datahora_Internalname ;
      private String Ddo_logresponsavel_acao_Internalname ;
      private String Ddo_logresponsavel_status_Internalname ;
      private String Ddo_logresponsavel_novostatus_Internalname ;
      private String Ddo_logresponsavel_prazo_Internalname ;
      private String Ddo_logresponsavel_observacao_Internalname ;
      private String edtLogResponsavel_DataHora_Title ;
      private String edtLogResponsavel_Prazo_Title ;
      private String edtLogResponsavel_Observacao_Title ;
      private String imgCancelaracao_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String bttBtnprazos_Internalname ;
      private String bttBtnprazos_Jsonclick ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String imgCancelaracao_Jsonclick ;
      private String sCtrlAV7LogResponsavel_DemandaCod ;
      private String sGXsfl_9_fel_idx="0001" ;
      private String ROClassString ;
      private String edtLogResponsavel_DataHora_Jsonclick ;
      private String cmbavEmissor_Jsonclick ;
      private String cmbLogResponsavel_Acao_Jsonclick ;
      private String cmbavDestino_Jsonclick ;
      private String cmbLogResponsavel_Status_Jsonclick ;
      private String cmbLogResponsavel_NovoStatus_Jsonclick ;
      private String edtLogResponsavel_Prazo_Jsonclick ;
      private String edtLogResponsavel_Observacao_Jsonclick ;
      private DateTime AV23TFLogResponsavel_DataHora ;
      private DateTime AV24TFLogResponsavel_DataHora_To ;
      private DateTime AV41TFLogResponsavel_Prazo ;
      private DateTime AV42TFLogResponsavel_Prazo_To ;
      private DateTime A893LogResponsavel_DataHora ;
      private DateTime A1177LogResponsavel_Prazo ;
      private DateTime AV25DDO_LogResponsavel_DataHoraAuxDate ;
      private DateTime AV26DDO_LogResponsavel_DataHoraAuxDateTo ;
      private DateTime AV43DDO_LogResponsavel_PrazoAuxDate ;
      private DateTime AV44DDO_LogResponsavel_PrazoAuxDateTo ;
      private bool entryPointCalled ;
      private bool n58Usuario_PessoaNom ;
      private bool n891LogResponsavel_UsuarioCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool A2080AreaTrabalho_SelUsrPrestadora ;
      private bool n2080AreaTrabalho_SelUsrPrestadora ;
      private bool n438Contratada_Sigla ;
      private bool toggleJsOutput ;
      private bool Ddo_logresponsavel_datahora_Includesortasc ;
      private bool Ddo_logresponsavel_datahora_Includesortdsc ;
      private bool Ddo_logresponsavel_datahora_Includefilter ;
      private bool Ddo_logresponsavel_datahora_Filterisrange ;
      private bool Ddo_logresponsavel_datahora_Includedatalist ;
      private bool Ddo_logresponsavel_acao_Includesortasc ;
      private bool Ddo_logresponsavel_acao_Includesortdsc ;
      private bool Ddo_logresponsavel_acao_Includefilter ;
      private bool Ddo_logresponsavel_acao_Includedatalist ;
      private bool Ddo_logresponsavel_acao_Allowmultipleselection ;
      private bool Ddo_logresponsavel_status_Includesortasc ;
      private bool Ddo_logresponsavel_status_Includesortdsc ;
      private bool Ddo_logresponsavel_status_Includefilter ;
      private bool Ddo_logresponsavel_status_Includedatalist ;
      private bool Ddo_logresponsavel_status_Allowmultipleselection ;
      private bool Ddo_logresponsavel_novostatus_Includesortasc ;
      private bool Ddo_logresponsavel_novostatus_Includesortdsc ;
      private bool Ddo_logresponsavel_novostatus_Includefilter ;
      private bool Ddo_logresponsavel_novostatus_Includedatalist ;
      private bool Ddo_logresponsavel_novostatus_Allowmultipleselection ;
      private bool Ddo_logresponsavel_prazo_Includesortasc ;
      private bool Ddo_logresponsavel_prazo_Includesortdsc ;
      private bool Ddo_logresponsavel_prazo_Includefilter ;
      private bool Ddo_logresponsavel_prazo_Filterisrange ;
      private bool Ddo_logresponsavel_prazo_Includedatalist ;
      private bool Ddo_logresponsavel_observacao_Includesortasc ;
      private bool Ddo_logresponsavel_observacao_Includesortdsc ;
      private bool Ddo_logresponsavel_observacao_Includefilter ;
      private bool Ddo_logresponsavel_observacao_Filterisrange ;
      private bool Ddo_logresponsavel_observacao_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1130LogResponsavel_Status ;
      private bool n1234LogResponsavel_NovoStatus ;
      private bool n1177LogResponsavel_Prazo ;
      private bool n1131LogResponsavel_Observacao ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String A1131LogResponsavel_Observacao ;
      private String AV29TFLogResponsavel_Acao_SelsJson ;
      private String AV33TFLogResponsavel_Status_SelsJson ;
      private String AV37TFLogResponsavel_NovoStatus_SelsJson ;
      private String AV47TFLogResponsavel_Observacao ;
      private String AV48TFLogResponsavel_Observacao_Sel ;
      private String AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace ;
      private String AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace ;
      private String AV35ddo_LogResponsavel_StatusTitleControlIdToReplace ;
      private String AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace ;
      private String AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace ;
      private String AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV51OS ;
      private String lV47TFLogResponsavel_Observacao ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavEmissor ;
      private GXCombobox cmbLogResponsavel_Acao ;
      private GXCombobox cmbavDestino ;
      private GXCombobox cmbLogResponsavel_Status ;
      private GXCombobox cmbLogResponsavel_NovoStatus ;
      private IDataStoreProvider pr_default ;
      private int[] H00FY2_A896LogResponsavel_Owner ;
      private int[] H00FY2_A891LogResponsavel_UsuarioCod ;
      private bool[] H00FY2_n891LogResponsavel_UsuarioCod ;
      private String[] H00FY2_A493ContagemResultado_DemandaFM ;
      private bool[] H00FY2_n493ContagemResultado_DemandaFM ;
      private long[] H00FY2_A1797LogResponsavel_Codigo ;
      private int[] H00FY2_A892LogResponsavel_DemandaCod ;
      private bool[] H00FY2_n892LogResponsavel_DemandaCod ;
      private String[] H00FY2_A1131LogResponsavel_Observacao ;
      private bool[] H00FY2_n1131LogResponsavel_Observacao ;
      private DateTime[] H00FY2_A1177LogResponsavel_Prazo ;
      private bool[] H00FY2_n1177LogResponsavel_Prazo ;
      private String[] H00FY2_A1234LogResponsavel_NovoStatus ;
      private bool[] H00FY2_n1234LogResponsavel_NovoStatus ;
      private String[] H00FY2_A1130LogResponsavel_Status ;
      private bool[] H00FY2_n1130LogResponsavel_Status ;
      private String[] H00FY2_A894LogResponsavel_Acao ;
      private DateTime[] H00FY2_A893LogResponsavel_DataHora ;
      private long[] H00FY3_AGRID_nRecordCount ;
      private int[] H00FY4_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00FY4_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00FY4_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H00FY4_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H00FY4_A2080AreaTrabalho_SelUsrPrestadora ;
      private bool[] H00FY4_n2080AreaTrabalho_SelUsrPrestadora ;
      private String[] H00FY4_A438Contratada_Sigla ;
      private bool[] H00FY4_n438Contratada_Sigla ;
      private int[] H00FY5_A57Usuario_PessoaCod ;
      private String[] H00FY5_A58Usuario_PessoaNom ;
      private bool[] H00FY5_n58Usuario_PessoaNom ;
      private int[] H00FY5_A1Usuario_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30TFLogResponsavel_Acao_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34TFLogResponsavel_Status_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV38TFLogResponsavel_NovoStatus_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV22LogResponsavel_DataHoraTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV28LogResponsavel_AcaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV32LogResponsavel_StatusTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36LogResponsavel_NovoStatusTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV40LogResponsavel_PrazoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46LogResponsavel_ObservacaoTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV50DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wclogresponsavel__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00FY2( IGxContext context ,
                                             String A894LogResponsavel_Acao ,
                                             IGxCollection AV30TFLogResponsavel_Acao_Sels ,
                                             String A1130LogResponsavel_Status ,
                                             IGxCollection AV34TFLogResponsavel_Status_Sels ,
                                             String A1234LogResponsavel_NovoStatus ,
                                             IGxCollection AV38TFLogResponsavel_NovoStatus_Sels ,
                                             DateTime AV23TFLogResponsavel_DataHora ,
                                             DateTime AV24TFLogResponsavel_DataHora_To ,
                                             int AV30TFLogResponsavel_Acao_Sels_Count ,
                                             int AV34TFLogResponsavel_Status_Sels_Count ,
                                             int AV38TFLogResponsavel_NovoStatus_Sels_Count ,
                                             DateTime AV41TFLogResponsavel_Prazo ,
                                             DateTime AV42TFLogResponsavel_Prazo_To ,
                                             String AV48TFLogResponsavel_Observacao_Sel ,
                                             String AV47TFLogResponsavel_Observacao ,
                                             DateTime A893LogResponsavel_DataHora ,
                                             DateTime A1177LogResponsavel_Prazo ,
                                             String A1131LogResponsavel_Observacao ,
                                             int AV7LogResponsavel_DemandaCod ,
                                             int A892LogResponsavel_DemandaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[LogResponsavel_Owner], T1.[LogResponsavel_UsuarioCod], T2.[ContagemResultado_DemandaFM], T1.[LogResponsavel_Codigo], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T1.[LogResponsavel_Observacao], T1.[LogResponsavel_Prazo], T1.[LogResponsavel_NovoStatus], T1.[LogResponsavel_Status], T1.[LogResponsavel_Acao], T1.[LogResponsavel_DataHora]";
         sFromString = " FROM ([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[LogResponsavel_DemandaCod] = @AV7LogResponsavel_DemandaCod)";
         if ( ! (DateTime.MinValue==AV23TFLogResponsavel_DataHora) )
         {
            sWhereString = sWhereString + " and (T1.[LogResponsavel_DataHora] >= @AV23TFLogResponsavel_DataHora)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFLogResponsavel_DataHora_To) )
         {
            sWhereString = sWhereString + " and (T1.[LogResponsavel_DataHora] <= @AV24TFLogResponsavel_DataHora_To)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV30TFLogResponsavel_Acao_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV30TFLogResponsavel_Acao_Sels, "T1.[LogResponsavel_Acao] IN (", ")") + ")";
         }
         if ( AV34TFLogResponsavel_Status_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV34TFLogResponsavel_Status_Sels, "T1.[LogResponsavel_Status] IN (", ")") + ")";
         }
         if ( AV38TFLogResponsavel_NovoStatus_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV38TFLogResponsavel_NovoStatus_Sels, "T1.[LogResponsavel_NovoStatus] IN (", ")") + ")";
         }
         if ( ! (DateTime.MinValue==AV41TFLogResponsavel_Prazo) )
         {
            sWhereString = sWhereString + " and (T1.[LogResponsavel_Prazo] >= @AV41TFLogResponsavel_Prazo)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV42TFLogResponsavel_Prazo_To) )
         {
            sWhereString = sWhereString + " and (T1.[LogResponsavel_Prazo] <= @AV42TFLogResponsavel_Prazo_To)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48TFLogResponsavel_Observacao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFLogResponsavel_Observacao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[LogResponsavel_Observacao] like @lV47TFLogResponsavel_Observacao)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFLogResponsavel_Observacao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[LogResponsavel_Observacao] = @AV48TFLogResponsavel_Observacao_Sel)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         sOrderString = sOrderString + " ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC";
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00FY3( IGxContext context ,
                                             String A894LogResponsavel_Acao ,
                                             IGxCollection AV30TFLogResponsavel_Acao_Sels ,
                                             String A1130LogResponsavel_Status ,
                                             IGxCollection AV34TFLogResponsavel_Status_Sels ,
                                             String A1234LogResponsavel_NovoStatus ,
                                             IGxCollection AV38TFLogResponsavel_NovoStatus_Sels ,
                                             DateTime AV23TFLogResponsavel_DataHora ,
                                             DateTime AV24TFLogResponsavel_DataHora_To ,
                                             int AV30TFLogResponsavel_Acao_Sels_Count ,
                                             int AV34TFLogResponsavel_Status_Sels_Count ,
                                             int AV38TFLogResponsavel_NovoStatus_Sels_Count ,
                                             DateTime AV41TFLogResponsavel_Prazo ,
                                             DateTime AV42TFLogResponsavel_Prazo_To ,
                                             String AV48TFLogResponsavel_Observacao_Sel ,
                                             String AV47TFLogResponsavel_Observacao ,
                                             DateTime A893LogResponsavel_DataHora ,
                                             DateTime A1177LogResponsavel_Prazo ,
                                             String A1131LogResponsavel_Observacao ,
                                             int AV7LogResponsavel_DemandaCod ,
                                             int A892LogResponsavel_DemandaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[LogResponsavel_DemandaCod] = @AV7LogResponsavel_DemandaCod)";
         if ( ! (DateTime.MinValue==AV23TFLogResponsavel_DataHora) )
         {
            sWhereString = sWhereString + " and (T1.[LogResponsavel_DataHora] >= @AV23TFLogResponsavel_DataHora)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFLogResponsavel_DataHora_To) )
         {
            sWhereString = sWhereString + " and (T1.[LogResponsavel_DataHora] <= @AV24TFLogResponsavel_DataHora_To)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV30TFLogResponsavel_Acao_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV30TFLogResponsavel_Acao_Sels, "T1.[LogResponsavel_Acao] IN (", ")") + ")";
         }
         if ( AV34TFLogResponsavel_Status_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV34TFLogResponsavel_Status_Sels, "T1.[LogResponsavel_Status] IN (", ")") + ")";
         }
         if ( AV38TFLogResponsavel_NovoStatus_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV38TFLogResponsavel_NovoStatus_Sels, "T1.[LogResponsavel_NovoStatus] IN (", ")") + ")";
         }
         if ( ! (DateTime.MinValue==AV41TFLogResponsavel_Prazo) )
         {
            sWhereString = sWhereString + " and (T1.[LogResponsavel_Prazo] >= @AV41TFLogResponsavel_Prazo)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV42TFLogResponsavel_Prazo_To) )
         {
            sWhereString = sWhereString + " and (T1.[LogResponsavel_Prazo] <= @AV42TFLogResponsavel_Prazo_To)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48TFLogResponsavel_Observacao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFLogResponsavel_Observacao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[LogResponsavel_Observacao] like @lV47TFLogResponsavel_Observacao)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFLogResponsavel_Observacao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[LogResponsavel_Observacao] = @AV48TFLogResponsavel_Observacao_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00FY2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
               case 1 :
                     return conditional_H00FY3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00FY4 ;
          prmH00FY4 = new Object[] {
          new Object[] {"@AV17Destino",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16Emissor",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00FY5 ;
          prmH00FY5 = new Object[] {
          } ;
          Object[] prmH00FY2 ;
          prmH00FY2 = new Object[] {
          new Object[] {"@AV7LogResponsavel_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23TFLogResponsavel_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24TFLogResponsavel_DataHora_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV41TFLogResponsavel_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV42TFLogResponsavel_Prazo_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV47TFLogResponsavel_Observacao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV48TFLogResponsavel_Observacao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00FY3 ;
          prmH00FY3 = new Object[] {
          new Object[] {"@AV7LogResponsavel_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23TFLogResponsavel_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24TFLogResponsavel_DataHora_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV41TFLogResponsavel_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV42TFLogResponsavel_Prazo_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV47TFLogResponsavel_Observacao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV48TFLogResponsavel_Observacao_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00FY2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FY2,11,0,true,false )
             ,new CursorDef("H00FY3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FY3,1,0,true,false )
             ,new CursorDef("H00FY4", "SELECT T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T3.[AreaTrabalho_SelUsrPrestadora], T2.[Contratada_Sigla] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV17Destino or T1.[ContratadaUsuario_UsuarioCod] = @AV16Emissor) AND (T2.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FY4,100,0,false,false )
             ,new CursorDef("H00FY5", "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Codigo] FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FY5,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((long[]) buf[5])[0] = rslt.getLong(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 20) ;
                ((DateTime[]) buf[17])[0] = rslt.getGXDateTime(11) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
       }
    }

 }

}
