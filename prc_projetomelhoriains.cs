/*
               File: PRC_ProjetoMelhoriaINS
        Description: Projeto Melhoria Insert
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/3/2020 18:49:57.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_projetomelhoriains : GXProcedure
   {
      public prc_projetomelhoriains( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_projetomelhoriains( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ProjetoMelhoria_ProjetoCod ,
                           ref int aP1_ProjetoMelhoria_SistemaCod ,
                           int aP2_Sistema_Codigo )
      {
         this.A694ProjetoMelhoria_ProjetoCod = aP0_ProjetoMelhoria_ProjetoCod;
         this.AV17ProjetoMelhoria_SistemaCod = aP1_ProjetoMelhoria_SistemaCod;
         this.AV15Sistema_Codigo = aP2_Sistema_Codigo;
         initialize();
         executePrivate();
         aP0_ProjetoMelhoria_ProjetoCod=this.A694ProjetoMelhoria_ProjetoCod;
         aP1_ProjetoMelhoria_SistemaCod=this.AV17ProjetoMelhoria_SistemaCod;
      }

      public void executeSubmit( ref int aP0_ProjetoMelhoria_ProjetoCod ,
                                 ref int aP1_ProjetoMelhoria_SistemaCod ,
                                 int aP2_Sistema_Codigo )
      {
         prc_projetomelhoriains objprc_projetomelhoriains;
         objprc_projetomelhoriains = new prc_projetomelhoriains();
         objprc_projetomelhoriains.A694ProjetoMelhoria_ProjetoCod = aP0_ProjetoMelhoria_ProjetoCod;
         objprc_projetomelhoriains.AV17ProjetoMelhoria_SistemaCod = aP1_ProjetoMelhoria_SistemaCod;
         objprc_projetomelhoriains.AV15Sistema_Codigo = aP2_Sistema_Codigo;
         objprc_projetomelhoriains.context.SetSubmitInitialConfig(context);
         objprc_projetomelhoriains.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_projetomelhoriains);
         aP0_ProjetoMelhoria_ProjetoCod=this.A694ProjetoMelhoria_ProjetoCod;
         aP1_ProjetoMelhoria_SistemaCod=this.AV17ProjetoMelhoria_SistemaCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_projetomelhoriains)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9Sdt_ItnPrj.FromXml(AV10WebSession.Get("ItensProjeto"), "");
         AV10WebSession.Remove("ItensProjeto");
         AV65GXV1 = 1;
         while ( AV65GXV1 <= AV9Sdt_ItnPrj.Count )
         {
            AV62Sdt_FnAPF = ((SdtSDT_ItensConagem_Item)AV9Sdt_ItnPrj.Item(AV65GXV1));
            if ( StringUtil.StrCmp(AV62Sdt_FnAPF.gxTpr_Itemtipo, "T") == 0 )
            {
               /* Using cursor P005A2 */
               pr_default.execute(0, new Object[] {AV62Sdt_FnAPF.gxTpr_Itemcodigo});
               while ( (pr_default.getStatus(0) != 101) )
               {
                  A165FuncaoAPF_Codigo = P005A2_A165FuncaoAPF_Codigo[0];
                  A378FuncaoAPFAtributos_FuncaoDadosCod = P005A2_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
                  n378FuncaoAPFAtributos_FuncaoDadosCod = P005A2_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
                  A412FuncaoAPFAtributos_FuncaoDadosNom = P005A2_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
                  n412FuncaoAPFAtributos_FuncaoDadosNom = P005A2_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
                  A415FuncaoAPFAtributos_FuncaoDadosTip = P005A2_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
                  n415FuncaoAPFAtributos_FuncaoDadosTip = P005A2_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
                  A755FuncaoDados_Contar = P005A2_A755FuncaoDados_Contar[0];
                  n755FuncaoDados_Contar = P005A2_n755FuncaoDados_Contar[0];
                  A364FuncaoAPFAtributos_AtributosCod = P005A2_A364FuncaoAPFAtributos_AtributosCod[0];
                  A412FuncaoAPFAtributos_FuncaoDadosNom = P005A2_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
                  n412FuncaoAPFAtributos_FuncaoDadosNom = P005A2_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
                  A415FuncaoAPFAtributos_FuncaoDadosTip = P005A2_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
                  n415FuncaoAPFAtributos_FuncaoDadosTip = P005A2_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
                  A755FuncaoDados_Contar = P005A2_A755FuncaoDados_Contar[0];
                  n755FuncaoDados_Contar = P005A2_n755FuncaoDados_Contar[0];
                  AV12FuncaoDados_Codigo = A378FuncaoAPFAtributos_FuncaoDadosCod;
                  AV67GXV2 = 1;
                  while ( AV67GXV2 <= AV9Sdt_ItnPrj.Count )
                  {
                     AV57Sdt_FnDados = ((SdtSDT_ItensConagem_Item)AV9Sdt_ItnPrj.Item(AV67GXV2));
                     if ( ( StringUtil.StrCmp(AV57Sdt_FnDados.gxTpr_Itemtipo, "D") == 0 ) && ( AV57Sdt_FnDados.gxTpr_Itemcodigo == AV12FuncaoDados_Codigo ) )
                     {
                        AV58OnListFlag = true;
                        if (true) break;
                     }
                     AV67GXV2 = (int)(AV67GXV2+1);
                  }
                  if ( ! AV58OnListFlag )
                  {
                     AV8Sdt_Item.gxTpr_Itemcodigo = A378FuncaoAPFAtributos_FuncaoDadosCod;
                     AV8Sdt_Item.gxTpr_Itemnome = A412FuncaoAPFAtributos_FuncaoDadosNom;
                     AV8Sdt_Item.gxTpr_Itempfb = A377FuncaoDados_PF;
                     AV8Sdt_Item.gxTpr_Itemtipofn = A415FuncaoAPFAtributos_FuncaoDadosTip;
                     AV8Sdt_Item.gxTpr_Itemcontar = false;
                     AV8Sdt_Item.gxTpr_Itemtipo = "D";
                     AV9Sdt_ItnPrj.Add(AV8Sdt_Item, 0);
                     AV8Sdt_Item = new SdtSDT_ItensConagem_Item(context);
                  }
                  pr_default.readNext(0);
               }
               pr_default.close(0);
            }
            AV65GXV1 = (int)(AV65GXV1+1);
         }
         AV9Sdt_ItnPrj.Sort("ItemTipo");
         AV68GXV3 = 1;
         while ( AV68GXV3 <= AV9Sdt_ItnPrj.Count )
         {
            AV8Sdt_Item = ((SdtSDT_ItensConagem_Item)AV9Sdt_ItnPrj.Item(AV68GXV3));
            if ( StringUtil.StrCmp(AV8Sdt_Item.gxTpr_Itemtipo, "D") == 0 )
            {
               /*
                  INSERT RECORD ON TABLE ProjetoMelhoria

               */
               A695ProjetoMelhoria_SistemaCod = AV15Sistema_Codigo;
               A698ProjetoMelhoria_FnAPFCod = 0;
               n698ProjetoMelhoria_FnAPFCod = false;
               n698ProjetoMelhoria_FnAPFCod = true;
               A697ProjetoMelhoria_FnDadosCod = AV8Sdt_Item.gxTpr_Itemcodigo;
               n697ProjetoMelhoria_FnDadosCod = false;
               A704ProjetoMelhoria_Observacao = AV8Sdt_Item.gxTpr_Itemobservacao;
               n704ProjetoMelhoria_Observacao = false;
               A730ProjetoMelhoria_INM = AV8Sdt_Item.gxTpr_Itemnaomens;
               n730ProjetoMelhoria_INM = false;
               A732Projetomelhoria_Tipo = AV8Sdt_Item.gxTpr_Itemtipoinm;
               n732Projetomelhoria_Tipo = false;
               A731ProjetoMelhoria_Valor = AV8Sdt_Item.gxTpr_Itemvalor;
               n731ProjetoMelhoria_Valor = false;
               A733ProjetoMelhoria_PFB = AV8Sdt_Item.gxTpr_Itempfb;
               n733ProjetoMelhoria_PFB = false;
               A734ProjetoMelhoria_PFL = AV8Sdt_Item.gxTpr_Itempfl;
               n734ProjetoMelhoria_PFL = false;
               /* Using cursor P005A3 */
               pr_default.execute(1, new Object[] {A694ProjetoMelhoria_ProjetoCod, A695ProjetoMelhoria_SistemaCod, n697ProjetoMelhoria_FnDadosCod, A697ProjetoMelhoria_FnDadosCod, n698ProjetoMelhoria_FnAPFCod, A698ProjetoMelhoria_FnAPFCod, n704ProjetoMelhoria_Observacao, A704ProjetoMelhoria_Observacao, n730ProjetoMelhoria_INM, A730ProjetoMelhoria_INM, n731ProjetoMelhoria_Valor, A731ProjetoMelhoria_Valor, n732Projetomelhoria_Tipo, A732Projetomelhoria_Tipo, n733ProjetoMelhoria_PFB, A733ProjetoMelhoria_PFB, n734ProjetoMelhoria_PFL, A734ProjetoMelhoria_PFL});
               A736ProjetoMelhoria_Codigo = P005A3_A736ProjetoMelhoria_Codigo[0];
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("ProjetoMelhoria") ;
               if ( (pr_default.getStatus(1) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
               /* Using cursor P005A4 */
               pr_default.execute(2, new Object[] {AV8Sdt_Item.gxTpr_Itemcodigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A755FuncaoDados_Contar = P005A4_A755FuncaoDados_Contar[0];
                  n755FuncaoDados_Contar = P005A4_n755FuncaoDados_Contar[0];
                  A745FuncaoDados_MelhoraCod = P005A4_A745FuncaoDados_MelhoraCod[0];
                  n745FuncaoDados_MelhoraCod = P005A4_n745FuncaoDados_MelhoraCod[0];
                  A370FuncaoDados_SistemaCod = P005A4_A370FuncaoDados_SistemaCod[0];
                  A1267FuncaoDados_UpdAoImpBsln = P005A4_A1267FuncaoDados_UpdAoImpBsln[0];
                  n1267FuncaoDados_UpdAoImpBsln = P005A4_n1267FuncaoDados_UpdAoImpBsln[0];
                  A1258FuncaoDados_Importada = P005A4_A1258FuncaoDados_Importada[0];
                  n1258FuncaoDados_Importada = P005A4_n1258FuncaoDados_Importada[0];
                  A1245FuncaoDados_Observacao = P005A4_A1245FuncaoDados_Observacao[0];
                  n1245FuncaoDados_Observacao = P005A4_n1245FuncaoDados_Observacao[0];
                  A1147FuncaoDados_Tecnica = P005A4_A1147FuncaoDados_Tecnica[0];
                  n1147FuncaoDados_Tecnica = P005A4_n1147FuncaoDados_Tecnica[0];
                  A1025FuncaoDados_RAImp = P005A4_A1025FuncaoDados_RAImp[0];
                  n1025FuncaoDados_RAImp = P005A4_n1025FuncaoDados_RAImp[0];
                  A1024FuncaoDados_DERImp = P005A4_A1024FuncaoDados_DERImp[0];
                  n1024FuncaoDados_DERImp = P005A4_n1024FuncaoDados_DERImp[0];
                  A394FuncaoDados_Ativo = P005A4_A394FuncaoDados_Ativo[0];
                  A391FuncaoDados_FuncaoDadosCod = P005A4_A391FuncaoDados_FuncaoDadosCod[0];
                  n391FuncaoDados_FuncaoDadosCod = P005A4_n391FuncaoDados_FuncaoDadosCod[0];
                  A397FuncaoDados_Descricao = P005A4_A397FuncaoDados_Descricao[0];
                  n397FuncaoDados_Descricao = P005A4_n397FuncaoDados_Descricao[0];
                  A373FuncaoDados_Tipo = P005A4_A373FuncaoDados_Tipo[0];
                  A369FuncaoDados_Nome = P005A4_A369FuncaoDados_Nome[0];
                  A368FuncaoDados_Codigo = P005A4_A368FuncaoDados_Codigo[0];
                  AV41FuncaoDado.gxTpr_Oldid = A368FuncaoDados_Codigo;
                  /*
                     INSERT RECORD ON TABLE FuncaoDados

                  */
                  W370FuncaoDados_SistemaCod = A370FuncaoDados_SistemaCod;
                  W745FuncaoDados_MelhoraCod = A745FuncaoDados_MelhoraCod;
                  n745FuncaoDados_MelhoraCod = false;
                  W755FuncaoDados_Contar = A755FuncaoDados_Contar;
                  n755FuncaoDados_Contar = false;
                  A370FuncaoDados_SistemaCod = AV17ProjetoMelhoria_SistemaCod;
                  A745FuncaoDados_MelhoraCod = AV8Sdt_Item.gxTpr_Itemcodigo;
                  n745FuncaoDados_MelhoraCod = false;
                  A755FuncaoDados_Contar = AV8Sdt_Item.gxTpr_Itemcontar;
                  n755FuncaoDados_Contar = false;
                  /* Using cursor P005A5 */
                  pr_default.execute(3, new Object[] {A369FuncaoDados_Nome, A370FuncaoDados_SistemaCod, A373FuncaoDados_Tipo, n397FuncaoDados_Descricao, A397FuncaoDados_Descricao, n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, A394FuncaoDados_Ativo, n745FuncaoDados_MelhoraCod, A745FuncaoDados_MelhoraCod, n755FuncaoDados_Contar, A755FuncaoDados_Contar, n1024FuncaoDados_DERImp, A1024FuncaoDados_DERImp, n1025FuncaoDados_RAImp, A1025FuncaoDados_RAImp, n1147FuncaoDados_Tecnica, A1147FuncaoDados_Tecnica, n1245FuncaoDados_Observacao, A1245FuncaoDados_Observacao, n1258FuncaoDados_Importada, A1258FuncaoDados_Importada, n1267FuncaoDados_UpdAoImpBsln, A1267FuncaoDados_UpdAoImpBsln});
                  A368FuncaoDados_Codigo = P005A5_A368FuncaoDados_Codigo[0];
                  pr_default.close(3);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
                  if ( (pr_default.getStatus(3) == 1) )
                  {
                     context.Gx_err = 1;
                     Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                  }
                  else
                  {
                     context.Gx_err = 0;
                     Gx_emsg = "";
                  }
                  A370FuncaoDados_SistemaCod = W370FuncaoDados_SistemaCod;
                  A745FuncaoDados_MelhoraCod = W745FuncaoDados_MelhoraCod;
                  n745FuncaoDados_MelhoraCod = false;
                  A755FuncaoDados_Contar = W755FuncaoDados_Contar;
                  n755FuncaoDados_Contar = false;
                  /* End Insert */
                  AV12FuncaoDados_Codigo = A368FuncaoDados_Codigo;
                  AV41FuncaoDado.gxTpr_Newid = AV12FuncaoDados_Codigo;
                  AV40FuncoesDados.Add(AV41FuncaoDado, 0);
                  AV41FuncaoDado = new SdtSDT_CloneAtributo_Atributo(context);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               /* Execute user subroutine: 'TABELASATRIBUTOS' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
            else if ( StringUtil.StrCmp(AV8Sdt_Item.gxTpr_Itemtipo, "T") == 0 )
            {
               /*
                  INSERT RECORD ON TABLE ProjetoMelhoria

               */
               A695ProjetoMelhoria_SistemaCod = AV15Sistema_Codigo;
               A697ProjetoMelhoria_FnDadosCod = 0;
               n697ProjetoMelhoria_FnDadosCod = false;
               n697ProjetoMelhoria_FnDadosCod = true;
               A698ProjetoMelhoria_FnAPFCod = AV8Sdt_Item.gxTpr_Itemcodigo;
               n698ProjetoMelhoria_FnAPFCod = false;
               A704ProjetoMelhoria_Observacao = AV8Sdt_Item.gxTpr_Itemobservacao;
               n704ProjetoMelhoria_Observacao = false;
               A730ProjetoMelhoria_INM = AV8Sdt_Item.gxTpr_Itemnaomens;
               n730ProjetoMelhoria_INM = false;
               A732Projetomelhoria_Tipo = AV8Sdt_Item.gxTpr_Itemtipoinm;
               n732Projetomelhoria_Tipo = false;
               A731ProjetoMelhoria_Valor = AV8Sdt_Item.gxTpr_Itemvalor;
               n731ProjetoMelhoria_Valor = false;
               A733ProjetoMelhoria_PFB = AV8Sdt_Item.gxTpr_Itempfb;
               n733ProjetoMelhoria_PFB = false;
               A734ProjetoMelhoria_PFL = AV8Sdt_Item.gxTpr_Itempfl;
               n734ProjetoMelhoria_PFL = false;
               /* Using cursor P005A6 */
               pr_default.execute(4, new Object[] {A694ProjetoMelhoria_ProjetoCod, A695ProjetoMelhoria_SistemaCod, n697ProjetoMelhoria_FnDadosCod, A697ProjetoMelhoria_FnDadosCod, n698ProjetoMelhoria_FnAPFCod, A698ProjetoMelhoria_FnAPFCod, n704ProjetoMelhoria_Observacao, A704ProjetoMelhoria_Observacao, n730ProjetoMelhoria_INM, A730ProjetoMelhoria_INM, n731ProjetoMelhoria_Valor, A731ProjetoMelhoria_Valor, n732Projetomelhoria_Tipo, A732Projetomelhoria_Tipo, n733ProjetoMelhoria_PFB, A733ProjetoMelhoria_PFB, n734ProjetoMelhoria_PFL, A734ProjetoMelhoria_PFL});
               A736ProjetoMelhoria_Codigo = P005A6_A736ProjetoMelhoria_Codigo[0];
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ProjetoMelhoria") ;
               if ( (pr_default.getStatus(4) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
               /* Using cursor P005A7 */
               pr_default.execute(5, new Object[] {AV8Sdt_Item.gxTpr_Itemcodigo});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A744FuncaoAPF_MelhoraCod = P005A7_A744FuncaoAPF_MelhoraCod[0];
                  n744FuncaoAPF_MelhoraCod = P005A7_n744FuncaoAPF_MelhoraCod[0];
                  A360FuncaoAPF_SistemaCod = P005A7_A360FuncaoAPF_SistemaCod[0];
                  n360FuncaoAPF_SistemaCod = P005A7_n360FuncaoAPF_SistemaCod[0];
                  A1268FuncaoAPF_UpdAoImpBsln = P005A7_A1268FuncaoAPF_UpdAoImpBsln[0];
                  n1268FuncaoAPF_UpdAoImpBsln = P005A7_n1268FuncaoAPF_UpdAoImpBsln[0];
                  A1246FuncaoAPF_Importada = P005A7_A1246FuncaoAPF_Importada[0];
                  n1246FuncaoAPF_Importada = P005A7_n1246FuncaoAPF_Importada[0];
                  A1244FuncaoAPF_Observacao = P005A7_A1244FuncaoAPF_Observacao[0];
                  n1244FuncaoAPF_Observacao = P005A7_n1244FuncaoAPF_Observacao[0];
                  A1233FuncaoAPF_ParecerSE = P005A7_A1233FuncaoAPF_ParecerSE[0];
                  n1233FuncaoAPF_ParecerSE = P005A7_n1233FuncaoAPF_ParecerSE[0];
                  A1146FuncaoAPF_Tecnica = P005A7_A1146FuncaoAPF_Tecnica[0];
                  n1146FuncaoAPF_Tecnica = P005A7_n1146FuncaoAPF_Tecnica[0];
                  A1026FuncaoAPF_RAImp = P005A7_A1026FuncaoAPF_RAImp[0];
                  n1026FuncaoAPF_RAImp = P005A7_n1026FuncaoAPF_RAImp[0];
                  A1022FuncaoAPF_DERImp = P005A7_A1022FuncaoAPF_DERImp[0];
                  n1022FuncaoAPF_DERImp = P005A7_n1022FuncaoAPF_DERImp[0];
                  A432FuncaoAPF_Link = P005A7_A432FuncaoAPF_Link[0];
                  n432FuncaoAPF_Link = P005A7_n432FuncaoAPF_Link[0];
                  A414FuncaoAPF_Mensagem = P005A7_A414FuncaoAPF_Mensagem[0];
                  n414FuncaoAPF_Mensagem = P005A7_n414FuncaoAPF_Mensagem[0];
                  A413FuncaoAPF_Acao = P005A7_A413FuncaoAPF_Acao[0];
                  n413FuncaoAPF_Acao = P005A7_n413FuncaoAPF_Acao[0];
                  A358FuncaoAPF_FunAPFPaiCod = P005A7_A358FuncaoAPF_FunAPFPaiCod[0];
                  n358FuncaoAPF_FunAPFPaiCod = P005A7_n358FuncaoAPF_FunAPFPaiCod[0];
                  A359FuncaoAPF_ModuloCod = P005A7_A359FuncaoAPF_ModuloCod[0];
                  n359FuncaoAPF_ModuloCod = P005A7_n359FuncaoAPF_ModuloCod[0];
                  A184FuncaoAPF_Tipo = P005A7_A184FuncaoAPF_Tipo[0];
                  A183FuncaoAPF_Ativo = P005A7_A183FuncaoAPF_Ativo[0];
                  A167FuncaoAPF_Descricao = P005A7_A167FuncaoAPF_Descricao[0];
                  A166FuncaoAPF_Nome = P005A7_A166FuncaoAPF_Nome[0];
                  A165FuncaoAPF_Codigo = P005A7_A165FuncaoAPF_Codigo[0];
                  /*
                     INSERT RECORD ON TABLE FuncoesAPF

                  */
                  W360FuncaoAPF_SistemaCod = A360FuncaoAPF_SistemaCod;
                  n360FuncaoAPF_SistemaCod = false;
                  W744FuncaoAPF_MelhoraCod = A744FuncaoAPF_MelhoraCod;
                  n744FuncaoAPF_MelhoraCod = false;
                  A360FuncaoAPF_SistemaCod = AV17ProjetoMelhoria_SistemaCod;
                  n360FuncaoAPF_SistemaCod = false;
                  A744FuncaoAPF_MelhoraCod = AV8Sdt_Item.gxTpr_Itemcodigo;
                  n744FuncaoAPF_MelhoraCod = false;
                  /* Using cursor P005A8 */
                  pr_default.execute(6, new Object[] {A166FuncaoAPF_Nome, A167FuncaoAPF_Descricao, A183FuncaoAPF_Ativo, A184FuncaoAPF_Tipo, n360FuncaoAPF_SistemaCod, A360FuncaoAPF_SistemaCod, n359FuncaoAPF_ModuloCod, A359FuncaoAPF_ModuloCod, n358FuncaoAPF_FunAPFPaiCod, A358FuncaoAPF_FunAPFPaiCod, n413FuncaoAPF_Acao, A413FuncaoAPF_Acao, n414FuncaoAPF_Mensagem, A414FuncaoAPF_Mensagem, n432FuncaoAPF_Link, A432FuncaoAPF_Link, n744FuncaoAPF_MelhoraCod, A744FuncaoAPF_MelhoraCod, n1022FuncaoAPF_DERImp, A1022FuncaoAPF_DERImp, n1026FuncaoAPF_RAImp, A1026FuncaoAPF_RAImp, n1146FuncaoAPF_Tecnica, A1146FuncaoAPF_Tecnica, n1233FuncaoAPF_ParecerSE, A1233FuncaoAPF_ParecerSE, n1244FuncaoAPF_Observacao, A1244FuncaoAPF_Observacao, n1246FuncaoAPF_Importada, A1246FuncaoAPF_Importada, n1268FuncaoAPF_UpdAoImpBsln, A1268FuncaoAPF_UpdAoImpBsln});
                  A165FuncaoAPF_Codigo = P005A8_A165FuncaoAPF_Codigo[0];
                  pr_default.close(6);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
                  if ( (pr_default.getStatus(6) == 1) )
                  {
                     context.Gx_err = 1;
                     Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                  }
                  else
                  {
                     context.Gx_err = 0;
                     Gx_emsg = "";
                  }
                  A360FuncaoAPF_SistemaCod = W360FuncaoAPF_SistemaCod;
                  n360FuncaoAPF_SistemaCod = false;
                  A744FuncaoAPF_MelhoraCod = W744FuncaoAPF_MelhoraCod;
                  n744FuncaoAPF_MelhoraCod = false;
                  /* End Insert */
                  AV13FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(5);
               /* Execute user subroutine: 'ATRIBUTOSEVIDENCIAS' */
               S131 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
            AV68GXV3 = (int)(AV68GXV3+1);
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'TABELASATRIBUTOS' Routine */
         /* Using cursor P005A9 */
         pr_default.execute(7, new Object[] {AV8Sdt_Item.gxTpr_Itemcodigo});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A368FuncaoDados_Codigo = P005A9_A368FuncaoDados_Codigo[0];
            A172Tabela_Codigo = P005A9_A172Tabela_Codigo[0];
            AV16Tabela_Codigo = A172Tabela_Codigo;
            /* Execute user subroutine: 'NEWTABELAFND' */
            S129 ();
            if ( returnInSub )
            {
               pr_default.close(7);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(7);
         }
         pr_default.close(7);
      }

      protected void S129( )
      {
         /* 'NEWTABELAFND' Routine */
         /* Using cursor P005A10 */
         pr_default.execute(8, new Object[] {AV16Tabela_Codigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A746Tabela_MelhoraCod = P005A10_A746Tabela_MelhoraCod[0];
            n746Tabela_MelhoraCod = P005A10_n746Tabela_MelhoraCod[0];
            A190Tabela_SistemaCod = P005A10_A190Tabela_SistemaCod[0];
            A188Tabela_ModuloCod = P005A10_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = P005A10_n188Tabela_ModuloCod[0];
            A181Tabela_PaiCod = P005A10_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = P005A10_n181Tabela_PaiCod[0];
            A175Tabela_Descricao = P005A10_A175Tabela_Descricao[0];
            n175Tabela_Descricao = P005A10_n175Tabela_Descricao[0];
            A174Tabela_Ativo = P005A10_A174Tabela_Ativo[0];
            A173Tabela_Nome = P005A10_A173Tabela_Nome[0];
            A172Tabela_Codigo = P005A10_A172Tabela_Codigo[0];
            /*
               INSERT RECORD ON TABLE Tabela

            */
            W190Tabela_SistemaCod = A190Tabela_SistemaCod;
            W746Tabela_MelhoraCod = A746Tabela_MelhoraCod;
            n746Tabela_MelhoraCod = false;
            A190Tabela_SistemaCod = AV17ProjetoMelhoria_SistemaCod;
            A746Tabela_MelhoraCod = AV16Tabela_Codigo;
            n746Tabela_MelhoraCod = false;
            /* Using cursor P005A11 */
            pr_default.execute(9, new Object[] {A173Tabela_Nome, A174Tabela_Ativo, n175Tabela_Descricao, A175Tabela_Descricao, n181Tabela_PaiCod, A181Tabela_PaiCod, n188Tabela_ModuloCod, A188Tabela_ModuloCod, A190Tabela_SistemaCod, n746Tabela_MelhoraCod, A746Tabela_MelhoraCod});
            A172Tabela_Codigo = P005A11_A172Tabela_Codigo[0];
            pr_default.close(9);
            dsDefault.SmartCacheProvider.SetUpdated("Tabela") ;
            if ( (pr_default.getStatus(9) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            A190Tabela_SistemaCod = W190Tabela_SistemaCod;
            A746Tabela_MelhoraCod = W746Tabela_MelhoraCod;
            n746Tabela_MelhoraCod = false;
            /* End Insert */
            AV27NewTabela_Codigo = A172Tabela_Codigo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(8);
         /*
            INSERT RECORD ON TABLE FuncaoDadosTabela

         */
         A368FuncaoDados_Codigo = AV12FuncaoDados_Codigo;
         A172Tabela_Codigo = AV27NewTabela_Codigo;
         /* Using cursor P005A12 */
         pr_default.execute(10, new Object[] {A368FuncaoDados_Codigo, A172Tabela_Codigo});
         pr_default.close(10);
         dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosTabela") ;
         if ( (pr_default.getStatus(10) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         /* Using cursor P005A13 */
         pr_default.execute(11, new Object[] {AV16Tabela_Codigo});
         while ( (pr_default.getStatus(11) != 101) )
         {
            A747Atributos_MelhoraCod = P005A13_A747Atributos_MelhoraCod[0];
            n747Atributos_MelhoraCod = P005A13_n747Atributos_MelhoraCod[0];
            A356Atributos_TabelaCod = P005A13_A356Atributos_TabelaCod[0];
            A401Atributos_FK = P005A13_A401Atributos_FK[0];
            n401Atributos_FK = P005A13_n401Atributos_FK[0];
            A400Atributos_PK = P005A13_A400Atributos_PK[0];
            n400Atributos_PK = P005A13_n400Atributos_PK[0];
            A390Atributos_Detalhes = P005A13_A390Atributos_Detalhes[0];
            n390Atributos_Detalhes = P005A13_n390Atributos_Detalhes[0];
            A180Atributos_Ativo = P005A13_A180Atributos_Ativo[0];
            A179Atributos_Descricao = P005A13_A179Atributos_Descricao[0];
            n179Atributos_Descricao = P005A13_n179Atributos_Descricao[0];
            A178Atributos_TipoDados = P005A13_A178Atributos_TipoDados[0];
            n178Atributos_TipoDados = P005A13_n178Atributos_TipoDados[0];
            A177Atributos_Nome = P005A13_A177Atributos_Nome[0];
            A176Atributos_Codigo = P005A13_A176Atributos_Codigo[0];
            W356Atributos_TabelaCod = A356Atributos_TabelaCod;
            AV29Atributo.gxTpr_Oldid = A176Atributos_Codigo;
            /*
               INSERT RECORD ON TABLE Atributos

            */
            W356Atributos_TabelaCod = A356Atributos_TabelaCod;
            W747Atributos_MelhoraCod = A747Atributos_MelhoraCod;
            n747Atributos_MelhoraCod = false;
            A356Atributos_TabelaCod = AV27NewTabela_Codigo;
            A747Atributos_MelhoraCod = AV29Atributo.gxTpr_Oldid;
            n747Atributos_MelhoraCod = false;
            /* Using cursor P005A14 */
            pr_default.execute(12, new Object[] {A177Atributos_Nome, n178Atributos_TipoDados, A178Atributos_TipoDados, n179Atributos_Descricao, A179Atributos_Descricao, A180Atributos_Ativo, A356Atributos_TabelaCod, n390Atributos_Detalhes, A390Atributos_Detalhes, n400Atributos_PK, A400Atributos_PK, n401Atributos_FK, A401Atributos_FK, n747Atributos_MelhoraCod, A747Atributos_MelhoraCod});
            A176Atributos_Codigo = P005A14_A176Atributos_Codigo[0];
            pr_default.close(12);
            dsDefault.SmartCacheProvider.SetUpdated("Atributos") ;
            if ( (pr_default.getStatus(12) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            A356Atributos_TabelaCod = W356Atributos_TabelaCod;
            A747Atributos_MelhoraCod = W747Atributos_MelhoraCod;
            n747Atributos_MelhoraCod = false;
            /* End Insert */
            AV39Atributos_Codigo = A176Atributos_Codigo;
            AV29Atributo.gxTpr_Newid = AV39Atributos_Codigo;
            AV28Atributos.Add(AV29Atributo, 0);
            AV29Atributo = new SdtSDT_CloneAtributo_Atributo(context);
            A356Atributos_TabelaCod = W356Atributos_TabelaCod;
            pr_default.readNext(11);
         }
         pr_default.close(11);
      }

      protected void S131( )
      {
         /* 'ATRIBUTOSEVIDENCIAS' Routine */
         /* Using cursor P005A15 */
         pr_default.execute(13, new Object[] {AV8Sdt_Item.gxTpr_Itemcodigo});
         while ( (pr_default.getStatus(13) != 101) )
         {
            A165FuncaoAPF_Codigo = P005A15_A165FuncaoAPF_Codigo[0];
            A364FuncaoAPFAtributos_AtributosCod = P005A15_A364FuncaoAPFAtributos_AtributosCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = P005A15_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            n378FuncaoAPFAtributos_FuncaoDadosCod = P005A15_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A383FuncoesAPFAtributos_Code = P005A15_A383FuncoesAPFAtributos_Code[0];
            n383FuncoesAPFAtributos_Code = P005A15_n383FuncoesAPFAtributos_Code[0];
            A385FuncoesAPFAtributos_Descricao = P005A15_A385FuncoesAPFAtributos_Descricao[0];
            n385FuncoesAPFAtributos_Descricao = P005A15_n385FuncoesAPFAtributos_Descricao[0];
            A384FuncoesAPFAtributos_Nome = P005A15_A384FuncoesAPFAtributos_Nome[0];
            n384FuncoesAPFAtributos_Nome = P005A15_n384FuncoesAPFAtributos_Nome[0];
            A389FuncoesAPFAtributos_Regra = P005A15_A389FuncoesAPFAtributos_Regra[0];
            n389FuncoesAPFAtributos_Regra = P005A15_n389FuncoesAPFAtributos_Regra[0];
            AV75GXV4 = 1;
            while ( AV75GXV4 <= AV28Atributos.Count )
            {
               AV32AtributoId = ((SdtSDT_CloneAtributo_Atributo)AV28Atributos.Item(AV75GXV4));
               if ( AV32AtributoId.gxTpr_Oldid == A364FuncaoAPFAtributos_AtributosCod )
               {
                  AV31NewAtributos_Codigo = AV32AtributoId.gxTpr_Newid;
                  if (true) break;
               }
               AV75GXV4 = (int)(AV75GXV4+1);
            }
            AV76GXV5 = 1;
            while ( AV76GXV5 <= AV40FuncoesDados.Count )
            {
               AV47FuncaoDadoId = ((SdtSDT_CloneAtributo_Atributo)AV40FuncoesDados.Item(AV76GXV5));
               if ( AV47FuncaoDadoId.gxTpr_Oldid == A378FuncaoAPFAtributos_FuncaoDadosCod )
               {
                  AV37FuncaoAPFAtributos_FuncaoDadosCod = AV47FuncaoDadoId.gxTpr_Newid;
                  if (true) break;
               }
               AV76GXV5 = (int)(AV76GXV5+1);
            }
            AV33FuncoesAPFAtributos_Code = A383FuncoesAPFAtributos_Code;
            AV36FuncoesAPFAtributos_Descricao = A385FuncoesAPFAtributos_Descricao;
            AV34FuncoesAPFAtributos_Nome = A384FuncoesAPFAtributos_Nome;
            AV38FuncoesAPFAtributos_Regra = A389FuncoesAPFAtributos_Regra;
            AV53FuncaoAPFAtributos_MelhoraCod = A364FuncaoAPFAtributos_AtributosCod;
            /* Execute user subroutine: 'NEWATRIBUTO' */
            S1415 ();
            if ( returnInSub )
            {
               pr_default.close(13);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(13);
         }
         pr_default.close(13);
         /* Using cursor P005A16 */
         pr_default.execute(14, new Object[] {AV8Sdt_Item.gxTpr_Itemcodigo});
         while ( (pr_default.getStatus(14) != 101) )
         {
            A165FuncaoAPF_Codigo = P005A16_A165FuncaoAPF_Codigo[0];
            A407FuncaoAPFEvidencia_Descricao = P005A16_A407FuncaoAPFEvidencia_Descricao[0];
            n407FuncaoAPFEvidencia_Descricao = P005A16_n407FuncaoAPFEvidencia_Descricao[0];
            A409FuncaoAPFEvidencia_NomeArq = P005A16_A409FuncaoAPFEvidencia_NomeArq[0];
            n409FuncaoAPFEvidencia_NomeArq = P005A16_n409FuncaoAPFEvidencia_NomeArq[0];
            A410FuncaoAPFEvidencia_TipoArq = P005A16_A410FuncaoAPFEvidencia_TipoArq[0];
            n410FuncaoAPFEvidencia_TipoArq = P005A16_n410FuncaoAPFEvidencia_TipoArq[0];
            A411FuncaoAPFEvidencia_Data = P005A16_A411FuncaoAPFEvidencia_Data[0];
            A406FuncaoAPFEvidencia_Codigo = P005A16_A406FuncaoAPFEvidencia_Codigo[0];
            A408FuncaoAPFEvidencia_Arquivo = P005A16_A408FuncaoAPFEvidencia_Arquivo[0];
            n408FuncaoAPFEvidencia_Arquivo = P005A16_n408FuncaoAPFEvidencia_Arquivo[0];
            AV48FuncaoAPFEvidencia_Descricao = A407FuncaoAPFEvidencia_Descricao;
            AV49FuncaoAPFEvidencia_Arquivo = A408FuncaoAPFEvidencia_Arquivo;
            AV50FuncaoAPFEvidencia_NomeArq = A409FuncaoAPFEvidencia_NomeArq;
            AV51FuncaoAPFEvidencia_TipoArq = A410FuncaoAPFEvidencia_TipoArq;
            AV52FuncaoAPFEvidencia_Data = A411FuncaoAPFEvidencia_Data;
            AV54FuncaoAPFEvidencia_MelhoraCod = A406FuncaoAPFEvidencia_Codigo;
            /* Execute user subroutine: 'NEWEVIDENCIA' */
            S1516 ();
            if ( returnInSub )
            {
               pr_default.close(14);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(14);
         }
         pr_default.close(14);
      }

      protected void S1415( )
      {
         /* 'NEWATRIBUTO' Routine */
         /*
            INSERT RECORD ON TABLE FuncoesAPFAtributos

         */
         A165FuncaoAPF_Codigo = AV13FuncaoAPF_Codigo;
         A364FuncaoAPFAtributos_AtributosCod = AV31NewAtributos_Codigo;
         A378FuncaoAPFAtributos_FuncaoDadosCod = AV37FuncaoAPFAtributos_FuncaoDadosCod;
         n378FuncaoAPFAtributos_FuncaoDadosCod = false;
         A383FuncoesAPFAtributos_Code = AV33FuncoesAPFAtributos_Code;
         n383FuncoesAPFAtributos_Code = false;
         A385FuncoesAPFAtributos_Descricao = AV36FuncoesAPFAtributos_Descricao;
         n385FuncoesAPFAtributos_Descricao = false;
         A384FuncoesAPFAtributos_Nome = AV34FuncoesAPFAtributos_Nome;
         n384FuncoesAPFAtributos_Nome = false;
         A389FuncoesAPFAtributos_Regra = AV38FuncoesAPFAtributos_Regra;
         n389FuncoesAPFAtributos_Regra = false;
         A748FuncaoAPFAtributos_MelhoraCod = AV53FuncaoAPFAtributos_MelhoraCod;
         n748FuncaoAPFAtributos_MelhoraCod = false;
         /* Using cursor P005A17 */
         pr_default.execute(15, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod, n389FuncoesAPFAtributos_Regra, A389FuncoesAPFAtributos_Regra, n383FuncoesAPFAtributos_Code, A383FuncoesAPFAtributos_Code, n384FuncoesAPFAtributos_Nome, A384FuncoesAPFAtributos_Nome, n385FuncoesAPFAtributos_Descricao, A385FuncoesAPFAtributos_Descricao, n748FuncaoAPFAtributos_MelhoraCod, A748FuncaoAPFAtributos_MelhoraCod});
         pr_default.close(15);
         dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
         if ( (pr_default.getStatus(15) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
      }

      protected void S1516( )
      {
         /* 'NEWEVIDENCIA' Routine */
         /*
            INSERT RECORD ON TABLE FuncaoAPFEvidencia

         */
         A165FuncaoAPF_Codigo = AV13FuncaoAPF_Codigo;
         A407FuncaoAPFEvidencia_Descricao = AV48FuncaoAPFEvidencia_Descricao;
         n407FuncaoAPFEvidencia_Descricao = false;
         A408FuncaoAPFEvidencia_Arquivo = AV49FuncaoAPFEvidencia_Arquivo;
         n408FuncaoAPFEvidencia_Arquivo = false;
         A409FuncaoAPFEvidencia_NomeArq = AV50FuncaoAPFEvidencia_NomeArq;
         n409FuncaoAPFEvidencia_NomeArq = false;
         A410FuncaoAPFEvidencia_TipoArq = AV51FuncaoAPFEvidencia_TipoArq;
         n410FuncaoAPFEvidencia_TipoArq = false;
         A411FuncaoAPFEvidencia_Data = AV52FuncaoAPFEvidencia_Data;
         A749FuncaoAPFEvidencia_MelhoraCod = AV54FuncaoAPFEvidencia_MelhoraCod;
         n749FuncaoAPFEvidencia_MelhoraCod = false;
         A750FuncaoAPFEvidencia_Ativo = true;
         n750FuncaoAPFEvidencia_Ativo = false;
         /* Using cursor P005A18 */
         pr_default.execute(16, new Object[] {A165FuncaoAPF_Codigo, n407FuncaoAPFEvidencia_Descricao, A407FuncaoAPFEvidencia_Descricao, n408FuncaoAPFEvidencia_Arquivo, A408FuncaoAPFEvidencia_Arquivo, n409FuncaoAPFEvidencia_NomeArq, A409FuncaoAPFEvidencia_NomeArq, n410FuncaoAPFEvidencia_TipoArq, A410FuncaoAPFEvidencia_TipoArq, A411FuncaoAPFEvidencia_Data, n749FuncaoAPFEvidencia_MelhoraCod, A749FuncaoAPFEvidencia_MelhoraCod, n750FuncaoAPFEvidencia_Ativo, A750FuncaoAPFEvidencia_Ativo});
         A406FuncaoAPFEvidencia_Codigo = P005A18_A406FuncaoAPFEvidencia_Codigo[0];
         pr_default.close(16);
         dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFEvidencia") ;
         if ( (pr_default.getStatus(16) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ProjetoMelhoriaINS");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9Sdt_ItnPrj = new GxObjectCollection( context, "SDT_ItensConagem.Item", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ItensConagem_Item", "GeneXus.Programs");
         AV10WebSession = context.GetSession();
         AV62Sdt_FnAPF = new SdtSDT_ItensConagem_Item(context);
         scmdbuf = "";
         P005A2_A165FuncaoAPF_Codigo = new int[1] ;
         P005A2_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P005A2_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P005A2_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         P005A2_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         P005A2_A415FuncaoAPFAtributos_FuncaoDadosTip = new String[] {""} ;
         P005A2_n415FuncaoAPFAtributos_FuncaoDadosTip = new bool[] {false} ;
         P005A2_A755FuncaoDados_Contar = new bool[] {false} ;
         P005A2_n755FuncaoDados_Contar = new bool[] {false} ;
         P005A2_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         A412FuncaoAPFAtributos_FuncaoDadosNom = "";
         A415FuncaoAPFAtributos_FuncaoDadosTip = "";
         AV57Sdt_FnDados = new SdtSDT_ItensConagem_Item(context);
         AV8Sdt_Item = new SdtSDT_ItensConagem_Item(context);
         A704ProjetoMelhoria_Observacao = "";
         A730ProjetoMelhoria_INM = "";
         P005A3_A736ProjetoMelhoria_Codigo = new int[1] ;
         Gx_emsg = "";
         P005A4_A755FuncaoDados_Contar = new bool[] {false} ;
         P005A4_n755FuncaoDados_Contar = new bool[] {false} ;
         P005A4_A745FuncaoDados_MelhoraCod = new int[1] ;
         P005A4_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         P005A4_A370FuncaoDados_SistemaCod = new int[1] ;
         P005A4_A1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         P005A4_n1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         P005A4_A1258FuncaoDados_Importada = new DateTime[] {DateTime.MinValue} ;
         P005A4_n1258FuncaoDados_Importada = new bool[] {false} ;
         P005A4_A1245FuncaoDados_Observacao = new String[] {""} ;
         P005A4_n1245FuncaoDados_Observacao = new bool[] {false} ;
         P005A4_A1147FuncaoDados_Tecnica = new short[1] ;
         P005A4_n1147FuncaoDados_Tecnica = new bool[] {false} ;
         P005A4_A1025FuncaoDados_RAImp = new short[1] ;
         P005A4_n1025FuncaoDados_RAImp = new bool[] {false} ;
         P005A4_A1024FuncaoDados_DERImp = new short[1] ;
         P005A4_n1024FuncaoDados_DERImp = new bool[] {false} ;
         P005A4_A394FuncaoDados_Ativo = new String[] {""} ;
         P005A4_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         P005A4_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         P005A4_A397FuncaoDados_Descricao = new String[] {""} ;
         P005A4_n397FuncaoDados_Descricao = new bool[] {false} ;
         P005A4_A373FuncaoDados_Tipo = new String[] {""} ;
         P005A4_A369FuncaoDados_Nome = new String[] {""} ;
         P005A4_A368FuncaoDados_Codigo = new int[1] ;
         A1258FuncaoDados_Importada = DateTime.MinValue;
         A1245FuncaoDados_Observacao = "";
         A394FuncaoDados_Ativo = "";
         A397FuncaoDados_Descricao = "";
         A373FuncaoDados_Tipo = "";
         A369FuncaoDados_Nome = "";
         AV41FuncaoDado = new SdtSDT_CloneAtributo_Atributo(context);
         P005A5_A368FuncaoDados_Codigo = new int[1] ;
         AV40FuncoesDados = new GxObjectCollection( context, "SDT_CloneAtributo.Atributo", "GxEv3Up14_MeetrikaVs3", "SdtSDT_CloneAtributo_Atributo", "GeneXus.Programs");
         P005A6_A736ProjetoMelhoria_Codigo = new int[1] ;
         P005A7_A744FuncaoAPF_MelhoraCod = new int[1] ;
         P005A7_n744FuncaoAPF_MelhoraCod = new bool[] {false} ;
         P005A7_A360FuncaoAPF_SistemaCod = new int[1] ;
         P005A7_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         P005A7_A1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         P005A7_n1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         P005A7_A1246FuncaoAPF_Importada = new DateTime[] {DateTime.MinValue} ;
         P005A7_n1246FuncaoAPF_Importada = new bool[] {false} ;
         P005A7_A1244FuncaoAPF_Observacao = new String[] {""} ;
         P005A7_n1244FuncaoAPF_Observacao = new bool[] {false} ;
         P005A7_A1233FuncaoAPF_ParecerSE = new String[] {""} ;
         P005A7_n1233FuncaoAPF_ParecerSE = new bool[] {false} ;
         P005A7_A1146FuncaoAPF_Tecnica = new short[1] ;
         P005A7_n1146FuncaoAPF_Tecnica = new bool[] {false} ;
         P005A7_A1026FuncaoAPF_RAImp = new short[1] ;
         P005A7_n1026FuncaoAPF_RAImp = new bool[] {false} ;
         P005A7_A1022FuncaoAPF_DERImp = new short[1] ;
         P005A7_n1022FuncaoAPF_DERImp = new bool[] {false} ;
         P005A7_A432FuncaoAPF_Link = new String[] {""} ;
         P005A7_n432FuncaoAPF_Link = new bool[] {false} ;
         P005A7_A414FuncaoAPF_Mensagem = new bool[] {false} ;
         P005A7_n414FuncaoAPF_Mensagem = new bool[] {false} ;
         P005A7_A413FuncaoAPF_Acao = new bool[] {false} ;
         P005A7_n413FuncaoAPF_Acao = new bool[] {false} ;
         P005A7_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         P005A7_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         P005A7_A359FuncaoAPF_ModuloCod = new int[1] ;
         P005A7_n359FuncaoAPF_ModuloCod = new bool[] {false} ;
         P005A7_A184FuncaoAPF_Tipo = new String[] {""} ;
         P005A7_A183FuncaoAPF_Ativo = new String[] {""} ;
         P005A7_A167FuncaoAPF_Descricao = new String[] {""} ;
         P005A7_A166FuncaoAPF_Nome = new String[] {""} ;
         P005A7_A165FuncaoAPF_Codigo = new int[1] ;
         A1246FuncaoAPF_Importada = (DateTime)(DateTime.MinValue);
         A1244FuncaoAPF_Observacao = "";
         A1233FuncaoAPF_ParecerSE = "";
         A432FuncaoAPF_Link = "";
         A184FuncaoAPF_Tipo = "";
         A183FuncaoAPF_Ativo = "";
         A167FuncaoAPF_Descricao = "";
         A166FuncaoAPF_Nome = "";
         P005A8_A165FuncaoAPF_Codigo = new int[1] ;
         P005A9_A368FuncaoDados_Codigo = new int[1] ;
         P005A9_A172Tabela_Codigo = new int[1] ;
         P005A10_A746Tabela_MelhoraCod = new int[1] ;
         P005A10_n746Tabela_MelhoraCod = new bool[] {false} ;
         P005A10_A190Tabela_SistemaCod = new int[1] ;
         P005A10_A188Tabela_ModuloCod = new int[1] ;
         P005A10_n188Tabela_ModuloCod = new bool[] {false} ;
         P005A10_A181Tabela_PaiCod = new int[1] ;
         P005A10_n181Tabela_PaiCod = new bool[] {false} ;
         P005A10_A175Tabela_Descricao = new String[] {""} ;
         P005A10_n175Tabela_Descricao = new bool[] {false} ;
         P005A10_A174Tabela_Ativo = new bool[] {false} ;
         P005A10_A173Tabela_Nome = new String[] {""} ;
         P005A10_A172Tabela_Codigo = new int[1] ;
         A175Tabela_Descricao = "";
         A173Tabela_Nome = "";
         P005A11_A172Tabela_Codigo = new int[1] ;
         P005A13_A747Atributos_MelhoraCod = new int[1] ;
         P005A13_n747Atributos_MelhoraCod = new bool[] {false} ;
         P005A13_A356Atributos_TabelaCod = new int[1] ;
         P005A13_A401Atributos_FK = new bool[] {false} ;
         P005A13_n401Atributos_FK = new bool[] {false} ;
         P005A13_A400Atributos_PK = new bool[] {false} ;
         P005A13_n400Atributos_PK = new bool[] {false} ;
         P005A13_A390Atributos_Detalhes = new String[] {""} ;
         P005A13_n390Atributos_Detalhes = new bool[] {false} ;
         P005A13_A180Atributos_Ativo = new bool[] {false} ;
         P005A13_A179Atributos_Descricao = new String[] {""} ;
         P005A13_n179Atributos_Descricao = new bool[] {false} ;
         P005A13_A178Atributos_TipoDados = new String[] {""} ;
         P005A13_n178Atributos_TipoDados = new bool[] {false} ;
         P005A13_A177Atributos_Nome = new String[] {""} ;
         P005A13_A176Atributos_Codigo = new int[1] ;
         A390Atributos_Detalhes = "";
         A179Atributos_Descricao = "";
         A178Atributos_TipoDados = "";
         A177Atributos_Nome = "";
         AV29Atributo = new SdtSDT_CloneAtributo_Atributo(context);
         P005A14_A176Atributos_Codigo = new int[1] ;
         AV28Atributos = new GxObjectCollection( context, "SDT_CloneAtributo.Atributo", "GxEv3Up14_MeetrikaVs3", "SdtSDT_CloneAtributo_Atributo", "GeneXus.Programs");
         P005A15_A165FuncaoAPF_Codigo = new int[1] ;
         P005A15_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P005A15_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P005A15_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P005A15_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         P005A15_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         P005A15_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         P005A15_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         P005A15_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         P005A15_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         P005A15_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         P005A15_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         A383FuncoesAPFAtributos_Code = "";
         A385FuncoesAPFAtributos_Descricao = "";
         A384FuncoesAPFAtributos_Nome = "";
         AV32AtributoId = new SdtSDT_CloneAtributo_Atributo(context);
         AV47FuncaoDadoId = new SdtSDT_CloneAtributo_Atributo(context);
         AV33FuncoesAPFAtributos_Code = "";
         AV36FuncoesAPFAtributos_Descricao = "";
         AV34FuncoesAPFAtributos_Nome = "";
         P005A16_A165FuncaoAPF_Codigo = new int[1] ;
         P005A16_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         P005A16_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         P005A16_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         P005A16_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         P005A16_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         P005A16_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         P005A16_A411FuncaoAPFEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         P005A16_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         P005A16_A408FuncaoAPFEvidencia_Arquivo = new String[] {""} ;
         P005A16_n408FuncaoAPFEvidencia_Arquivo = new bool[] {false} ;
         A407FuncaoAPFEvidencia_Descricao = "";
         A409FuncaoAPFEvidencia_NomeArq = "";
         A410FuncaoAPFEvidencia_TipoArq = "";
         A411FuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         A408FuncaoAPFEvidencia_Arquivo = "";
         AV48FuncaoAPFEvidencia_Descricao = "";
         AV49FuncaoAPFEvidencia_Arquivo = "";
         AV50FuncaoAPFEvidencia_NomeArq = "";
         AV51FuncaoAPFEvidencia_TipoArq = "";
         AV52FuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         P005A18_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_projetomelhoriains__default(),
            new Object[][] {
                new Object[] {
               P005A2_A165FuncaoAPF_Codigo, P005A2_A378FuncaoAPFAtributos_FuncaoDadosCod, P005A2_n378FuncaoAPFAtributos_FuncaoDadosCod, P005A2_A412FuncaoAPFAtributos_FuncaoDadosNom, P005A2_n412FuncaoAPFAtributos_FuncaoDadosNom, P005A2_A415FuncaoAPFAtributos_FuncaoDadosTip, P005A2_n415FuncaoAPFAtributos_FuncaoDadosTip, P005A2_A755FuncaoDados_Contar, P005A2_n755FuncaoDados_Contar, P005A2_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               P005A3_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               P005A4_A755FuncaoDados_Contar, P005A4_n755FuncaoDados_Contar, P005A4_A745FuncaoDados_MelhoraCod, P005A4_n745FuncaoDados_MelhoraCod, P005A4_A370FuncaoDados_SistemaCod, P005A4_A1267FuncaoDados_UpdAoImpBsln, P005A4_n1267FuncaoDados_UpdAoImpBsln, P005A4_A1258FuncaoDados_Importada, P005A4_n1258FuncaoDados_Importada, P005A4_A1245FuncaoDados_Observacao,
               P005A4_n1245FuncaoDados_Observacao, P005A4_A1147FuncaoDados_Tecnica, P005A4_n1147FuncaoDados_Tecnica, P005A4_A1025FuncaoDados_RAImp, P005A4_n1025FuncaoDados_RAImp, P005A4_A1024FuncaoDados_DERImp, P005A4_n1024FuncaoDados_DERImp, P005A4_A394FuncaoDados_Ativo, P005A4_A391FuncaoDados_FuncaoDadosCod, P005A4_n391FuncaoDados_FuncaoDadosCod,
               P005A4_A397FuncaoDados_Descricao, P005A4_n397FuncaoDados_Descricao, P005A4_A373FuncaoDados_Tipo, P005A4_A369FuncaoDados_Nome, P005A4_A368FuncaoDados_Codigo
               }
               , new Object[] {
               P005A5_A368FuncaoDados_Codigo
               }
               , new Object[] {
               P005A6_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               P005A7_A744FuncaoAPF_MelhoraCod, P005A7_n744FuncaoAPF_MelhoraCod, P005A7_A360FuncaoAPF_SistemaCod, P005A7_n360FuncaoAPF_SistemaCod, P005A7_A1268FuncaoAPF_UpdAoImpBsln, P005A7_n1268FuncaoAPF_UpdAoImpBsln, P005A7_A1246FuncaoAPF_Importada, P005A7_n1246FuncaoAPF_Importada, P005A7_A1244FuncaoAPF_Observacao, P005A7_n1244FuncaoAPF_Observacao,
               P005A7_A1233FuncaoAPF_ParecerSE, P005A7_n1233FuncaoAPF_ParecerSE, P005A7_A1146FuncaoAPF_Tecnica, P005A7_n1146FuncaoAPF_Tecnica, P005A7_A1026FuncaoAPF_RAImp, P005A7_n1026FuncaoAPF_RAImp, P005A7_A1022FuncaoAPF_DERImp, P005A7_n1022FuncaoAPF_DERImp, P005A7_A432FuncaoAPF_Link, P005A7_n432FuncaoAPF_Link,
               P005A7_A414FuncaoAPF_Mensagem, P005A7_n414FuncaoAPF_Mensagem, P005A7_A413FuncaoAPF_Acao, P005A7_n413FuncaoAPF_Acao, P005A7_A358FuncaoAPF_FunAPFPaiCod, P005A7_n358FuncaoAPF_FunAPFPaiCod, P005A7_A359FuncaoAPF_ModuloCod, P005A7_n359FuncaoAPF_ModuloCod, P005A7_A184FuncaoAPF_Tipo, P005A7_A183FuncaoAPF_Ativo,
               P005A7_A167FuncaoAPF_Descricao, P005A7_A166FuncaoAPF_Nome, P005A7_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               P005A8_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               P005A9_A368FuncaoDados_Codigo, P005A9_A172Tabela_Codigo
               }
               , new Object[] {
               P005A10_A746Tabela_MelhoraCod, P005A10_n746Tabela_MelhoraCod, P005A10_A190Tabela_SistemaCod, P005A10_A188Tabela_ModuloCod, P005A10_n188Tabela_ModuloCod, P005A10_A181Tabela_PaiCod, P005A10_n181Tabela_PaiCod, P005A10_A175Tabela_Descricao, P005A10_n175Tabela_Descricao, P005A10_A174Tabela_Ativo,
               P005A10_A173Tabela_Nome, P005A10_A172Tabela_Codigo
               }
               , new Object[] {
               P005A11_A172Tabela_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P005A13_A747Atributos_MelhoraCod, P005A13_n747Atributos_MelhoraCod, P005A13_A356Atributos_TabelaCod, P005A13_A401Atributos_FK, P005A13_n401Atributos_FK, P005A13_A400Atributos_PK, P005A13_n400Atributos_PK, P005A13_A390Atributos_Detalhes, P005A13_n390Atributos_Detalhes, P005A13_A180Atributos_Ativo,
               P005A13_A179Atributos_Descricao, P005A13_n179Atributos_Descricao, P005A13_A178Atributos_TipoDados, P005A13_n178Atributos_TipoDados, P005A13_A177Atributos_Nome, P005A13_A176Atributos_Codigo
               }
               , new Object[] {
               P005A14_A176Atributos_Codigo
               }
               , new Object[] {
               P005A15_A165FuncaoAPF_Codigo, P005A15_A364FuncaoAPFAtributos_AtributosCod, P005A15_A378FuncaoAPFAtributos_FuncaoDadosCod, P005A15_n378FuncaoAPFAtributos_FuncaoDadosCod, P005A15_A383FuncoesAPFAtributos_Code, P005A15_n383FuncoesAPFAtributos_Code, P005A15_A385FuncoesAPFAtributos_Descricao, P005A15_n385FuncoesAPFAtributos_Descricao, P005A15_A384FuncoesAPFAtributos_Nome, P005A15_n384FuncoesAPFAtributos_Nome,
               P005A15_A389FuncoesAPFAtributos_Regra, P005A15_n389FuncoesAPFAtributos_Regra
               }
               , new Object[] {
               P005A16_A165FuncaoAPF_Codigo, P005A16_A407FuncaoAPFEvidencia_Descricao, P005A16_n407FuncaoAPFEvidencia_Descricao, P005A16_A409FuncaoAPFEvidencia_NomeArq, P005A16_n409FuncaoAPFEvidencia_NomeArq, P005A16_A410FuncaoAPFEvidencia_TipoArq, P005A16_n410FuncaoAPFEvidencia_TipoArq, P005A16_A411FuncaoAPFEvidencia_Data, P005A16_A406FuncaoAPFEvidencia_Codigo, P005A16_A408FuncaoAPFEvidencia_Arquivo,
               P005A16_n408FuncaoAPFEvidencia_Arquivo
               }
               , new Object[] {
               }
               , new Object[] {
               P005A18_A406FuncaoAPFEvidencia_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A732Projetomelhoria_Tipo ;
      private short A1147FuncaoDados_Tecnica ;
      private short A1025FuncaoDados_RAImp ;
      private short A1024FuncaoDados_DERImp ;
      private short A1146FuncaoAPF_Tecnica ;
      private short A1026FuncaoAPF_RAImp ;
      private short A1022FuncaoAPF_DERImp ;
      private int A694ProjetoMelhoria_ProjetoCod ;
      private int AV17ProjetoMelhoria_SistemaCod ;
      private int AV15Sistema_Codigo ;
      private int AV65GXV1 ;
      private int A165FuncaoAPF_Codigo ;
      private int A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int AV12FuncaoDados_Codigo ;
      private int AV67GXV2 ;
      private int AV68GXV3 ;
      private int GX_INS93 ;
      private int A695ProjetoMelhoria_SistemaCod ;
      private int A698ProjetoMelhoria_FnAPFCod ;
      private int A697ProjetoMelhoria_FnDadosCod ;
      private int A736ProjetoMelhoria_Codigo ;
      private int A745FuncaoDados_MelhoraCod ;
      private int A370FuncaoDados_SistemaCod ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int A368FuncaoDados_Codigo ;
      private int GX_INS59 ;
      private int W370FuncaoDados_SistemaCod ;
      private int W745FuncaoDados_MelhoraCod ;
      private int A744FuncaoAPF_MelhoraCod ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A358FuncaoAPF_FunAPFPaiCod ;
      private int A359FuncaoAPF_ModuloCod ;
      private int GX_INS36 ;
      private int W360FuncaoAPF_SistemaCod ;
      private int W744FuncaoAPF_MelhoraCod ;
      private int AV13FuncaoAPF_Codigo ;
      private int A172Tabela_Codigo ;
      private int AV16Tabela_Codigo ;
      private int A746Tabela_MelhoraCod ;
      private int A190Tabela_SistemaCod ;
      private int A188Tabela_ModuloCod ;
      private int A181Tabela_PaiCod ;
      private int GX_INS39 ;
      private int W190Tabela_SistemaCod ;
      private int W746Tabela_MelhoraCod ;
      private int AV27NewTabela_Codigo ;
      private int GX_INS60 ;
      private int A747Atributos_MelhoraCod ;
      private int A356Atributos_TabelaCod ;
      private int A176Atributos_Codigo ;
      private int W356Atributos_TabelaCod ;
      private int GX_INS40 ;
      private int W747Atributos_MelhoraCod ;
      private int AV39Atributos_Codigo ;
      private int AV75GXV4 ;
      private int AV31NewAtributos_Codigo ;
      private int AV76GXV5 ;
      private int AV37FuncaoAPFAtributos_FuncaoDadosCod ;
      private int AV53FuncaoAPFAtributos_MelhoraCod ;
      private int A406FuncaoAPFEvidencia_Codigo ;
      private int AV54FuncaoAPFEvidencia_MelhoraCod ;
      private int GX_INS58 ;
      private int A748FuncaoAPFAtributos_MelhoraCod ;
      private int GX_INS63 ;
      private int A749FuncaoAPFEvidencia_MelhoraCod ;
      private decimal A377FuncaoDados_PF ;
      private decimal A731ProjetoMelhoria_Valor ;
      private decimal A733ProjetoMelhoria_PFB ;
      private decimal A734ProjetoMelhoria_PFL ;
      private String scmdbuf ;
      private String A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private String A730ProjetoMelhoria_INM ;
      private String Gx_emsg ;
      private String A394FuncaoDados_Ativo ;
      private String A373FuncaoDados_Tipo ;
      private String A184FuncaoAPF_Tipo ;
      private String A183FuncaoAPF_Ativo ;
      private String A173Tabela_Nome ;
      private String A390Atributos_Detalhes ;
      private String A178Atributos_TipoDados ;
      private String A177Atributos_Nome ;
      private String A384FuncoesAPFAtributos_Nome ;
      private String AV34FuncoesAPFAtributos_Nome ;
      private String A409FuncaoAPFEvidencia_NomeArq ;
      private String A410FuncaoAPFEvidencia_TipoArq ;
      private String AV50FuncaoAPFEvidencia_NomeArq ;
      private String AV51FuncaoAPFEvidencia_TipoArq ;
      private DateTime A1246FuncaoAPF_Importada ;
      private DateTime A411FuncaoAPFEvidencia_Data ;
      private DateTime AV52FuncaoAPFEvidencia_Data ;
      private DateTime A1258FuncaoDados_Importada ;
      private bool n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool A755FuncaoDados_Contar ;
      private bool n755FuncaoDados_Contar ;
      private bool AV58OnListFlag ;
      private bool n698ProjetoMelhoria_FnAPFCod ;
      private bool n697ProjetoMelhoria_FnDadosCod ;
      private bool n704ProjetoMelhoria_Observacao ;
      private bool n730ProjetoMelhoria_INM ;
      private bool n732Projetomelhoria_Tipo ;
      private bool n731ProjetoMelhoria_Valor ;
      private bool n733ProjetoMelhoria_PFB ;
      private bool n734ProjetoMelhoria_PFL ;
      private bool n745FuncaoDados_MelhoraCod ;
      private bool A1267FuncaoDados_UpdAoImpBsln ;
      private bool n1267FuncaoDados_UpdAoImpBsln ;
      private bool n1258FuncaoDados_Importada ;
      private bool n1245FuncaoDados_Observacao ;
      private bool n1147FuncaoDados_Tecnica ;
      private bool n1025FuncaoDados_RAImp ;
      private bool n1024FuncaoDados_DERImp ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool n397FuncaoDados_Descricao ;
      private bool W755FuncaoDados_Contar ;
      private bool returnInSub ;
      private bool n744FuncaoAPF_MelhoraCod ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool A1268FuncaoAPF_UpdAoImpBsln ;
      private bool n1268FuncaoAPF_UpdAoImpBsln ;
      private bool n1246FuncaoAPF_Importada ;
      private bool n1244FuncaoAPF_Observacao ;
      private bool n1233FuncaoAPF_ParecerSE ;
      private bool n1146FuncaoAPF_Tecnica ;
      private bool n1026FuncaoAPF_RAImp ;
      private bool n1022FuncaoAPF_DERImp ;
      private bool n432FuncaoAPF_Link ;
      private bool A414FuncaoAPF_Mensagem ;
      private bool n414FuncaoAPF_Mensagem ;
      private bool A413FuncaoAPF_Acao ;
      private bool n413FuncaoAPF_Acao ;
      private bool n358FuncaoAPF_FunAPFPaiCod ;
      private bool n359FuncaoAPF_ModuloCod ;
      private bool n746Tabela_MelhoraCod ;
      private bool n188Tabela_ModuloCod ;
      private bool n181Tabela_PaiCod ;
      private bool n175Tabela_Descricao ;
      private bool A174Tabela_Ativo ;
      private bool n747Atributos_MelhoraCod ;
      private bool A401Atributos_FK ;
      private bool n401Atributos_FK ;
      private bool A400Atributos_PK ;
      private bool n400Atributos_PK ;
      private bool n390Atributos_Detalhes ;
      private bool A180Atributos_Ativo ;
      private bool n179Atributos_Descricao ;
      private bool n178Atributos_TipoDados ;
      private bool n383FuncoesAPFAtributos_Code ;
      private bool n385FuncoesAPFAtributos_Descricao ;
      private bool n384FuncoesAPFAtributos_Nome ;
      private bool A389FuncoesAPFAtributos_Regra ;
      private bool n389FuncoesAPFAtributos_Regra ;
      private bool AV38FuncoesAPFAtributos_Regra ;
      private bool n407FuncaoAPFEvidencia_Descricao ;
      private bool n409FuncaoAPFEvidencia_NomeArq ;
      private bool n410FuncaoAPFEvidencia_TipoArq ;
      private bool n408FuncaoAPFEvidencia_Arquivo ;
      private bool n748FuncaoAPFAtributos_MelhoraCod ;
      private bool n749FuncaoAPFEvidencia_MelhoraCod ;
      private bool A750FuncaoAPFEvidencia_Ativo ;
      private bool n750FuncaoAPFEvidencia_Ativo ;
      private String A704ProjetoMelhoria_Observacao ;
      private String A1245FuncaoDados_Observacao ;
      private String A397FuncaoDados_Descricao ;
      private String A1244FuncaoAPF_Observacao ;
      private String A1233FuncaoAPF_ParecerSE ;
      private String A167FuncaoAPF_Descricao ;
      private String A175Tabela_Descricao ;
      private String A179Atributos_Descricao ;
      private String A407FuncaoAPFEvidencia_Descricao ;
      private String AV48FuncaoAPFEvidencia_Descricao ;
      private String A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String A369FuncaoDados_Nome ;
      private String A432FuncaoAPF_Link ;
      private String A166FuncaoAPF_Nome ;
      private String A383FuncoesAPFAtributos_Code ;
      private String A385FuncoesAPFAtributos_Descricao ;
      private String AV33FuncoesAPFAtributos_Code ;
      private String AV36FuncoesAPFAtributos_Descricao ;
      private String A408FuncaoAPFEvidencia_Arquivo ;
      private String AV49FuncaoAPFEvidencia_Arquivo ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ProjetoMelhoria_ProjetoCod ;
      private int aP1_ProjetoMelhoria_SistemaCod ;
      private IDataStoreProvider pr_default ;
      private int[] P005A2_A165FuncaoAPF_Codigo ;
      private int[] P005A2_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P005A2_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private String[] P005A2_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] P005A2_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] P005A2_A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] P005A2_n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] P005A2_A755FuncaoDados_Contar ;
      private bool[] P005A2_n755FuncaoDados_Contar ;
      private int[] P005A2_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P005A3_A736ProjetoMelhoria_Codigo ;
      private bool[] P005A4_A755FuncaoDados_Contar ;
      private bool[] P005A4_n755FuncaoDados_Contar ;
      private int[] P005A4_A745FuncaoDados_MelhoraCod ;
      private bool[] P005A4_n745FuncaoDados_MelhoraCod ;
      private int[] P005A4_A370FuncaoDados_SistemaCod ;
      private bool[] P005A4_A1267FuncaoDados_UpdAoImpBsln ;
      private bool[] P005A4_n1267FuncaoDados_UpdAoImpBsln ;
      private DateTime[] P005A4_A1258FuncaoDados_Importada ;
      private bool[] P005A4_n1258FuncaoDados_Importada ;
      private String[] P005A4_A1245FuncaoDados_Observacao ;
      private bool[] P005A4_n1245FuncaoDados_Observacao ;
      private short[] P005A4_A1147FuncaoDados_Tecnica ;
      private bool[] P005A4_n1147FuncaoDados_Tecnica ;
      private short[] P005A4_A1025FuncaoDados_RAImp ;
      private bool[] P005A4_n1025FuncaoDados_RAImp ;
      private short[] P005A4_A1024FuncaoDados_DERImp ;
      private bool[] P005A4_n1024FuncaoDados_DERImp ;
      private String[] P005A4_A394FuncaoDados_Ativo ;
      private int[] P005A4_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] P005A4_n391FuncaoDados_FuncaoDadosCod ;
      private String[] P005A4_A397FuncaoDados_Descricao ;
      private bool[] P005A4_n397FuncaoDados_Descricao ;
      private String[] P005A4_A373FuncaoDados_Tipo ;
      private String[] P005A4_A369FuncaoDados_Nome ;
      private int[] P005A4_A368FuncaoDados_Codigo ;
      private int[] P005A5_A368FuncaoDados_Codigo ;
      private int[] P005A6_A736ProjetoMelhoria_Codigo ;
      private int[] P005A7_A744FuncaoAPF_MelhoraCod ;
      private bool[] P005A7_n744FuncaoAPF_MelhoraCod ;
      private int[] P005A7_A360FuncaoAPF_SistemaCod ;
      private bool[] P005A7_n360FuncaoAPF_SistemaCod ;
      private bool[] P005A7_A1268FuncaoAPF_UpdAoImpBsln ;
      private bool[] P005A7_n1268FuncaoAPF_UpdAoImpBsln ;
      private DateTime[] P005A7_A1246FuncaoAPF_Importada ;
      private bool[] P005A7_n1246FuncaoAPF_Importada ;
      private String[] P005A7_A1244FuncaoAPF_Observacao ;
      private bool[] P005A7_n1244FuncaoAPF_Observacao ;
      private String[] P005A7_A1233FuncaoAPF_ParecerSE ;
      private bool[] P005A7_n1233FuncaoAPF_ParecerSE ;
      private short[] P005A7_A1146FuncaoAPF_Tecnica ;
      private bool[] P005A7_n1146FuncaoAPF_Tecnica ;
      private short[] P005A7_A1026FuncaoAPF_RAImp ;
      private bool[] P005A7_n1026FuncaoAPF_RAImp ;
      private short[] P005A7_A1022FuncaoAPF_DERImp ;
      private bool[] P005A7_n1022FuncaoAPF_DERImp ;
      private String[] P005A7_A432FuncaoAPF_Link ;
      private bool[] P005A7_n432FuncaoAPF_Link ;
      private bool[] P005A7_A414FuncaoAPF_Mensagem ;
      private bool[] P005A7_n414FuncaoAPF_Mensagem ;
      private bool[] P005A7_A413FuncaoAPF_Acao ;
      private bool[] P005A7_n413FuncaoAPF_Acao ;
      private int[] P005A7_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] P005A7_n358FuncaoAPF_FunAPFPaiCod ;
      private int[] P005A7_A359FuncaoAPF_ModuloCod ;
      private bool[] P005A7_n359FuncaoAPF_ModuloCod ;
      private String[] P005A7_A184FuncaoAPF_Tipo ;
      private String[] P005A7_A183FuncaoAPF_Ativo ;
      private String[] P005A7_A167FuncaoAPF_Descricao ;
      private String[] P005A7_A166FuncaoAPF_Nome ;
      private int[] P005A7_A165FuncaoAPF_Codigo ;
      private int[] P005A8_A165FuncaoAPF_Codigo ;
      private int[] P005A9_A368FuncaoDados_Codigo ;
      private int[] P005A9_A172Tabela_Codigo ;
      private int[] P005A10_A746Tabela_MelhoraCod ;
      private bool[] P005A10_n746Tabela_MelhoraCod ;
      private int[] P005A10_A190Tabela_SistemaCod ;
      private int[] P005A10_A188Tabela_ModuloCod ;
      private bool[] P005A10_n188Tabela_ModuloCod ;
      private int[] P005A10_A181Tabela_PaiCod ;
      private bool[] P005A10_n181Tabela_PaiCod ;
      private String[] P005A10_A175Tabela_Descricao ;
      private bool[] P005A10_n175Tabela_Descricao ;
      private bool[] P005A10_A174Tabela_Ativo ;
      private String[] P005A10_A173Tabela_Nome ;
      private int[] P005A10_A172Tabela_Codigo ;
      private int[] P005A11_A172Tabela_Codigo ;
      private int[] P005A13_A747Atributos_MelhoraCod ;
      private bool[] P005A13_n747Atributos_MelhoraCod ;
      private int[] P005A13_A356Atributos_TabelaCod ;
      private bool[] P005A13_A401Atributos_FK ;
      private bool[] P005A13_n401Atributos_FK ;
      private bool[] P005A13_A400Atributos_PK ;
      private bool[] P005A13_n400Atributos_PK ;
      private String[] P005A13_A390Atributos_Detalhes ;
      private bool[] P005A13_n390Atributos_Detalhes ;
      private bool[] P005A13_A180Atributos_Ativo ;
      private String[] P005A13_A179Atributos_Descricao ;
      private bool[] P005A13_n179Atributos_Descricao ;
      private String[] P005A13_A178Atributos_TipoDados ;
      private bool[] P005A13_n178Atributos_TipoDados ;
      private String[] P005A13_A177Atributos_Nome ;
      private int[] P005A13_A176Atributos_Codigo ;
      private int[] P005A14_A176Atributos_Codigo ;
      private int[] P005A15_A165FuncaoAPF_Codigo ;
      private int[] P005A15_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P005A15_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P005A15_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private String[] P005A15_A383FuncoesAPFAtributos_Code ;
      private bool[] P005A15_n383FuncoesAPFAtributos_Code ;
      private String[] P005A15_A385FuncoesAPFAtributos_Descricao ;
      private bool[] P005A15_n385FuncoesAPFAtributos_Descricao ;
      private String[] P005A15_A384FuncoesAPFAtributos_Nome ;
      private bool[] P005A15_n384FuncoesAPFAtributos_Nome ;
      private bool[] P005A15_A389FuncoesAPFAtributos_Regra ;
      private bool[] P005A15_n389FuncoesAPFAtributos_Regra ;
      private int[] P005A16_A165FuncaoAPF_Codigo ;
      private String[] P005A16_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] P005A16_n407FuncaoAPFEvidencia_Descricao ;
      private String[] P005A16_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] P005A16_n409FuncaoAPFEvidencia_NomeArq ;
      private String[] P005A16_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] P005A16_n410FuncaoAPFEvidencia_TipoArq ;
      private DateTime[] P005A16_A411FuncaoAPFEvidencia_Data ;
      private int[] P005A16_A406FuncaoAPFEvidencia_Codigo ;
      private String[] P005A16_A408FuncaoAPFEvidencia_Arquivo ;
      private bool[] P005A16_n408FuncaoAPFEvidencia_Arquivo ;
      private int[] P005A18_A406FuncaoAPFEvidencia_Codigo ;
      [ObjectCollection(ItemType=typeof( SdtSDT_CloneAtributo_Atributo ))]
      private IGxCollection AV40FuncoesDados ;
      [ObjectCollection(ItemType=typeof( SdtSDT_CloneAtributo_Atributo ))]
      private IGxCollection AV28Atributos ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ItensConagem_Item ))]
      private IGxCollection AV9Sdt_ItnPrj ;
      private SdtSDT_CloneAtributo_Atributo AV41FuncaoDado ;
      private SdtSDT_CloneAtributo_Atributo AV29Atributo ;
      private SdtSDT_CloneAtributo_Atributo AV32AtributoId ;
      private SdtSDT_CloneAtributo_Atributo AV47FuncaoDadoId ;
      private SdtSDT_ItensConagem_Item AV62Sdt_FnAPF ;
      private SdtSDT_ItensConagem_Item AV57Sdt_FnDados ;
      private SdtSDT_ItensConagem_Item AV8Sdt_Item ;
   }

   public class prc_projetomelhoriains__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005A2 ;
          prmP005A2 = new Object[] {
          new Object[] {"@AV62Sdt_FnAPF__Itemcodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005A3 ;
          prmP005A3 = new Object[] {
          new Object[] {"@ProjetoMelhoria_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_FnDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_FnAPFCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_Observacao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ProjetoMelhoria_INM",SqlDbType.Char,10,0} ,
          new Object[] {"@ProjetoMelhoria_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Projetomelhoria_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ProjetoMelhoria_PFB",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ProjetoMelhoria_PFL",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmP005A4 ;
          prmP005A4 = new Object[] {
          new Object[] {"@AV8Sdt_Item__Itemcodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005A5 ;
          prmP005A5 = new Object[] {
          new Object[] {"@FuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDados_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_Ativo",SqlDbType.Char,1,0} ,
          new Object[] {"@FuncaoDados_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_Contar",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoDados_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_Tecnica",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@FuncaoDados_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDados_Importada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@FuncaoDados_UpdAoImpBsln",SqlDbType.Bit,4,0}
          } ;
          Object[] prmP005A6 ;
          prmP005A6 = new Object[] {
          new Object[] {"@ProjetoMelhoria_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_FnDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_FnAPFCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoMelhoria_Observacao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ProjetoMelhoria_INM",SqlDbType.Char,10,0} ,
          new Object[] {"@ProjetoMelhoria_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Projetomelhoria_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ProjetoMelhoria_PFB",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ProjetoMelhoria_PFL",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmP005A7 ;
          prmP005A7 = new Object[] {
          new Object[] {"@AV8Sdt_Item__Itemcodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005A8 ;
          prmP005A8 = new Object[] {
          new Object[] {"@FuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@FuncaoAPF_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoAPF_Ativo",SqlDbType.Char,1,0} ,
          new Object[] {"@FuncaoAPF_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPF_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_FunAPFPaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Acao",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPF_Mensagem",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPF_Link",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPF_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_Tecnica",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@FuncaoAPF_ParecerSE",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@FuncaoAPF_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPF_Importada",SqlDbType.DateTime,8,5} ,
          new Object[] {"@FuncaoAPF_UpdAoImpBsln",SqlDbType.Bit,4,0}
          } ;
          Object[] prmP005A9 ;
          prmP005A9 = new Object[] {
          new Object[] {"@AV8Sdt_Item__Itemcodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005A10 ;
          prmP005A10 = new Object[] {
          new Object[] {"@AV16Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005A11 ;
          prmP005A11 = new Object[] {
          new Object[] {"@Tabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Tabela_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Tabela_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005A12 ;
          prmP005A12 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005A13 ;
          prmP005A13 = new Object[] {
          new Object[] {"@AV16Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005A14 ;
          prmP005A14 = new Object[] {
          new Object[] {"@Atributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Atributos_TipoDados",SqlDbType.Char,4,0} ,
          new Object[] {"@Atributos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Atributos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_TabelaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Atributos_Detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@Atributos_PK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_FK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005A15 ;
          prmP005A15 = new Object[] {
          new Object[] {"@AV8Sdt_Item__Itemcodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005A16 ;
          prmP005A16 = new Object[] {
          new Object[] {"@AV8Sdt_Item__Itemcodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005A17 ;
          prmP005A17 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncoesAPFAtributos_Regra",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncoesAPFAtributos_Code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@FuncoesAPFAtributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@FuncoesAPFAtributos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@FuncaoAPFAtributos_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005A18 ;
          prmP005A18 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@FuncaoAPFEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@FuncaoAPFEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@FuncaoAPFEvidencia_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFEvidencia_Ativo",SqlDbType.Bit,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005A2", "SELECT T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod, T2.[FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, T2.[FuncaoDados_Tipo] AS FuncaoAPFAtributos_FuncaoDadosTip, T2.[FuncaoDados_Contar], T1.[FuncaoAPFAtributos_AtributosCod] FROM ([FuncoesAPFAtributos] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoAPFAtributos_FuncaoDadosCod]) WHERE T1.[FuncaoAPF_Codigo] = @AV62Sdt_FnAPF__Itemcodigo ORDER BY T1.[FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005A2,100,0,true,false )
             ,new CursorDef("P005A3", "INSERT INTO [ProjetoMelhoria]([ProjetoMelhoria_ProjetoCod], [ProjetoMelhoria_SistemaCod], [ProjetoMelhoria_FnDadosCod], [ProjetoMelhoria_FnAPFCod], [ProjetoMelhoria_Observacao], [ProjetoMelhoria_INM], [ProjetoMelhoria_Valor], [Projetomelhoria_Tipo], [ProjetoMelhoria_PFB], [ProjetoMelhoria_PFL]) VALUES(@ProjetoMelhoria_ProjetoCod, @ProjetoMelhoria_SistemaCod, @ProjetoMelhoria_FnDadosCod, @ProjetoMelhoria_FnAPFCod, @ProjetoMelhoria_Observacao, @ProjetoMelhoria_INM, @ProjetoMelhoria_Valor, @Projetomelhoria_Tipo, @ProjetoMelhoria_PFB, @ProjetoMelhoria_PFL); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP005A3)
             ,new CursorDef("P005A4", "SELECT TOP 1 [FuncaoDados_Contar], [FuncaoDados_MelhoraCod], [FuncaoDados_SistemaCod], [FuncaoDados_UpdAoImpBsln], [FuncaoDados_Importada], [FuncaoDados_Observacao], [FuncaoDados_Tecnica], [FuncaoDados_RAImp], [FuncaoDados_DERImp], [FuncaoDados_Ativo], [FuncaoDados_FuncaoDadosCod], [FuncaoDados_Descricao], [FuncaoDados_Tipo], [FuncaoDados_Nome], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV8Sdt_Item__Itemcodigo ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005A4,1,0,true,true )
             ,new CursorDef("P005A5", "INSERT INTO [FuncaoDados]([FuncaoDados_Nome], [FuncaoDados_SistemaCod], [FuncaoDados_Tipo], [FuncaoDados_Descricao], [FuncaoDados_FuncaoDadosCod], [FuncaoDados_Ativo], [FuncaoDados_MelhoraCod], [FuncaoDados_Contar], [FuncaoDados_DERImp], [FuncaoDados_RAImp], [FuncaoDados_Tecnica], [FuncaoDados_Observacao], [FuncaoDados_Importada], [FuncaoDados_UpdAoImpBsln]) VALUES(@FuncaoDados_Nome, @FuncaoDados_SistemaCod, @FuncaoDados_Tipo, @FuncaoDados_Descricao, @FuncaoDados_FuncaoDadosCod, @FuncaoDados_Ativo, @FuncaoDados_MelhoraCod, @FuncaoDados_Contar, @FuncaoDados_DERImp, @FuncaoDados_RAImp, @FuncaoDados_Tecnica, @FuncaoDados_Observacao, @FuncaoDados_Importada, @FuncaoDados_UpdAoImpBsln); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP005A5)
             ,new CursorDef("P005A6", "INSERT INTO [ProjetoMelhoria]([ProjetoMelhoria_ProjetoCod], [ProjetoMelhoria_SistemaCod], [ProjetoMelhoria_FnDadosCod], [ProjetoMelhoria_FnAPFCod], [ProjetoMelhoria_Observacao], [ProjetoMelhoria_INM], [ProjetoMelhoria_Valor], [Projetomelhoria_Tipo], [ProjetoMelhoria_PFB], [ProjetoMelhoria_PFL]) VALUES(@ProjetoMelhoria_ProjetoCod, @ProjetoMelhoria_SistemaCod, @ProjetoMelhoria_FnDadosCod, @ProjetoMelhoria_FnAPFCod, @ProjetoMelhoria_Observacao, @ProjetoMelhoria_INM, @ProjetoMelhoria_Valor, @Projetomelhoria_Tipo, @ProjetoMelhoria_PFB, @ProjetoMelhoria_PFL); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP005A6)
             ,new CursorDef("P005A7", "SELECT TOP 1 [FuncaoAPF_MelhoraCod], [FuncaoAPF_SistemaCod], [FuncaoAPF_UpdAoImpBsln], [FuncaoAPF_Importada], [FuncaoAPF_Observacao], [FuncaoAPF_ParecerSE], [FuncaoAPF_Tecnica], [FuncaoAPF_RAImp], [FuncaoAPF_DERImp], [FuncaoAPF_Link], [FuncaoAPF_Mensagem], [FuncaoAPF_Acao], [FuncaoAPF_FunAPFPaiCod], [FuncaoAPF_ModuloCod], [FuncaoAPF_Tipo], [FuncaoAPF_Ativo], [FuncaoAPF_Descricao], [FuncaoAPF_Nome], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @AV8Sdt_Item__Itemcodigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005A7,1,0,true,true )
             ,new CursorDef("P005A8", "INSERT INTO [FuncoesAPF]([FuncaoAPF_Nome], [FuncaoAPF_Descricao], [FuncaoAPF_Ativo], [FuncaoAPF_Tipo], [FuncaoAPF_SistemaCod], [FuncaoAPF_ModuloCod], [FuncaoAPF_FunAPFPaiCod], [FuncaoAPF_Acao], [FuncaoAPF_Mensagem], [FuncaoAPF_Link], [FuncaoAPF_MelhoraCod], [FuncaoAPF_DERImp], [FuncaoAPF_RAImp], [FuncaoAPF_Tecnica], [FuncaoAPF_ParecerSE], [FuncaoAPF_Observacao], [FuncaoAPF_Importada], [FuncaoAPF_UpdAoImpBsln]) VALUES(@FuncaoAPF_Nome, @FuncaoAPF_Descricao, @FuncaoAPF_Ativo, @FuncaoAPF_Tipo, @FuncaoAPF_SistemaCod, @FuncaoAPF_ModuloCod, @FuncaoAPF_FunAPFPaiCod, @FuncaoAPF_Acao, @FuncaoAPF_Mensagem, @FuncaoAPF_Link, @FuncaoAPF_MelhoraCod, @FuncaoAPF_DERImp, @FuncaoAPF_RAImp, @FuncaoAPF_Tecnica, @FuncaoAPF_ParecerSE, @FuncaoAPF_Observacao, @FuncaoAPF_Importada, @FuncaoAPF_UpdAoImpBsln); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP005A8)
             ,new CursorDef("P005A9", "SELECT [FuncaoDados_Codigo], [Tabela_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV8Sdt_Item__Itemcodigo ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005A9,100,0,true,false )
             ,new CursorDef("P005A10", "SELECT TOP 1 [Tabela_MelhoraCod], [Tabela_SistemaCod], [Tabela_ModuloCod], [Tabela_PaiCod], [Tabela_Descricao], [Tabela_Ativo], [Tabela_Nome], [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @AV16Tabela_Codigo ORDER BY [Tabela_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005A10,1,0,true,true )
             ,new CursorDef("P005A11", "INSERT INTO [Tabela]([Tabela_Nome], [Tabela_Ativo], [Tabela_Descricao], [Tabela_PaiCod], [Tabela_ModuloCod], [Tabela_SistemaCod], [Tabela_MelhoraCod]) VALUES(@Tabela_Nome, @Tabela_Ativo, @Tabela_Descricao, @Tabela_PaiCod, @Tabela_ModuloCod, @Tabela_SistemaCod, @Tabela_MelhoraCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP005A11)
             ,new CursorDef("P005A12", "INSERT INTO [FuncaoDadosTabela]([FuncaoDados_Codigo], [Tabela_Codigo]) VALUES(@FuncaoDados_Codigo, @Tabela_Codigo)", GxErrorMask.GX_NOMASK,prmP005A12)
             ,new CursorDef("P005A13", "SELECT [Atributos_MelhoraCod], [Atributos_TabelaCod], [Atributos_FK], [Atributos_PK], [Atributos_Detalhes], [Atributos_Ativo], [Atributos_Descricao], [Atributos_TipoDados], [Atributos_Nome], [Atributos_Codigo] FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_TabelaCod] = @AV16Tabela_Codigo ORDER BY [Atributos_TabelaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005A13,100,0,true,false )
             ,new CursorDef("P005A14", "INSERT INTO [Atributos]([Atributos_Nome], [Atributos_TipoDados], [Atributos_Descricao], [Atributos_Ativo], [Atributos_TabelaCod], [Atributos_Detalhes], [Atributos_PK], [Atributos_FK], [Atributos_MelhoraCod]) VALUES(@Atributos_Nome, @Atributos_TipoDados, @Atributos_Descricao, @Atributos_Ativo, @Atributos_TabelaCod, @Atributos_Detalhes, @Atributos_PK, @Atributos_FK, @Atributos_MelhoraCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP005A14)
             ,new CursorDef("P005A15", "SELECT [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod], [FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod, [FuncoesAPFAtributos_Code], [FuncoesAPFAtributos_Descricao], [FuncoesAPFAtributos_Nome], [FuncoesAPFAtributos_Regra] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @AV8Sdt_Item__Itemcodigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005A15,100,0,true,false )
             ,new CursorDef("P005A16", "SELECT [FuncaoAPF_Codigo], [FuncaoAPFEvidencia_Descricao], [FuncaoAPFEvidencia_NomeArq], [FuncaoAPFEvidencia_TipoArq], [FuncaoAPFEvidencia_Data], [FuncaoAPFEvidencia_Codigo], [FuncaoAPFEvidencia_Arquivo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @AV8Sdt_Item__Itemcodigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005A16,100,0,true,false )
             ,new CursorDef("P005A17", "INSERT INTO [FuncoesAPFAtributos]([FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod], [FuncaoAPFAtributos_FuncaoDadosCod], [FuncoesAPFAtributos_Regra], [FuncoesAPFAtributos_Code], [FuncoesAPFAtributos_Nome], [FuncoesAPFAtributos_Descricao], [FuncaoAPFAtributos_MelhoraCod], [FuncaoAPFAtributos_Ativo]) VALUES(@FuncaoAPF_Codigo, @FuncaoAPFAtributos_AtributosCod, @FuncaoAPFAtributos_FuncaoDadosCod, @FuncoesAPFAtributos_Regra, @FuncoesAPFAtributos_Code, @FuncoesAPFAtributos_Nome, @FuncoesAPFAtributos_Descricao, @FuncaoAPFAtributos_MelhoraCod, convert(bit, 0))", GxErrorMask.GX_NOMASK,prmP005A17)
             ,new CursorDef("P005A18", "INSERT INTO [FuncaoAPFEvidencia]([FuncaoAPF_Codigo], [FuncaoAPFEvidencia_Descricao], [FuncaoAPFEvidencia_Arquivo], [FuncaoAPFEvidencia_NomeArq], [FuncaoAPFEvidencia_TipoArq], [FuncaoAPFEvidencia_Data], [FuncaoAPFEvidencia_MelhoraCod], [FuncaoAPFEvidencia_Ativo]) VALUES(@FuncaoAPF_Codigo, @FuncaoAPFEvidencia_Descricao, @FuncaoAPFEvidencia_Arquivo, @FuncaoAPFEvidencia_NomeArq, @FuncaoAPFEvidencia_TipoArq, @FuncaoAPFEvidencia_Data, @FuncaoAPFEvidencia_MelhoraCod, @FuncaoAPFEvidencia_Ativo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP005A18)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getLongVarchar(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 3) ;
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((int[]) buf[24])[0] = rslt.getInt(15) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((short[]) buf[12])[0] = rslt.getShort(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((short[]) buf[14])[0] = rslt.getShort(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((short[]) buf[16])[0] = rslt.getShort(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((bool[]) buf[20])[0] = rslt.getBool(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((bool[]) buf[22])[0] = rslt.getBool(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((int[]) buf[24])[0] = rslt.getInt(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((int[]) buf[26])[0] = rslt.getInt(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((String[]) buf[28])[0] = rslt.getString(15, 3) ;
                ((String[]) buf[29])[0] = rslt.getString(16, 1) ;
                ((String[]) buf[30])[0] = rslt.getLongVarchar(17) ;
                ((String[]) buf[31])[0] = rslt.getVarchar(18) ;
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 4) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 50) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((String[]) buf[9])[0] = rslt.getBLOBFile(7, "tmp", "") ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[17]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[6]);
                }
                stmt.SetParameter(6, (String)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(13, (DateTime)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 14 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(14, (bool)parms[23]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[17]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(9, (bool)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 15 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(17, (DateTime)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(18, (bool)parms[31]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                stmt.SetParameter(6, (int)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[10]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                stmt.SetParameter(4, (bool)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[13]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                stmt.SetParameterDatetime(6, (DateTime)parms[9]);
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[13]);
                }
                return;
       }
    }

 }

}
