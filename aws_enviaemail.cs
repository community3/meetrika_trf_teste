/*
               File: ws_EnviaEmail
        Description: Envia Email
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/31/2020 1:13:47.96
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Mail;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aws_enviaemail : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( ! context.isAjaxRequest( ) )
         {
            GXSoapHTTPResponse.AppendHeader("Content-type", "text/xml;charset=utf-8");
         }
         if ( StringUtil.StrCmp(StringUtil.Lower( GXSoapHTTPRequest.Method), "get") == 0 )
         {
            if ( StringUtil.StrCmp(StringUtil.Lower( GXSoapHTTPRequest.QueryString), "wsdl") == 0 )
            {
               GXSoapXMLWriter.OpenResponse(GXSoapHTTPResponse);
               GXSoapXMLWriter.WriteStartDocument("utf-8", 0);
               GXSoapXMLWriter.WriteStartElement("definitions");
               GXSoapXMLWriter.WriteAttribute("name", "ws_EnviaEmail");
               GXSoapXMLWriter.WriteAttribute("targetNamespace", "GxEv3Up14_MeetrikaVs3");
               GXSoapXMLWriter.WriteAttribute("xmlns:wsdlns", "GxEv3Up14_MeetrikaVs3");
               GXSoapXMLWriter.WriteAttribute("xmlns:soap", "http://schemas.xmlsoap.org/wsdl/soap/");
               GXSoapXMLWriter.WriteAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
               GXSoapXMLWriter.WriteAttribute("xmlns", "http://schemas.xmlsoap.org/wsdl/");
               GXSoapXMLWriter.WriteAttribute("xmlns:tns", "GxEv3Up14_MeetrikaVs3");
               GXSoapXMLWriter.WriteStartElement("types");
               GXSoapXMLWriter.WriteStartElement("schema");
               GXSoapXMLWriter.WriteAttribute("targetNamespace", "GxEv3Up14_MeetrikaVs3");
               GXSoapXMLWriter.WriteAttribute("xmlns", "http://www.w3.org/2001/XMLSchema");
               GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");
               GXSoapXMLWriter.WriteAttribute("elementFormDefault", "qualified");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteAttribute("name", "ArrayOfint");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "0");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "unbounded");
               GXSoapXMLWriter.WriteAttribute("name", "item");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:int");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteAttribute("name", "ArrayOfvchar");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "0");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "unbounded");
               GXSoapXMLWriter.WriteAttribute("name", "item");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "ws_EnviaEmail.Execute");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Areatrabalho_codigo");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:int");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Usuarios");
               GXSoapXMLWriter.WriteAttribute("type", "tns:ArrayOfint");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Subject");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Emailtext");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Attachments");
               GXSoapXMLWriter.WriteAttribute("type", "tns:ArrayOfvchar");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Resultado");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "ws_EnviaEmail.ExecuteResponse");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Resultado");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "ws_EnviaEmail.ExecuteSoapIn");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:ws_EnviaEmail.Execute");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "ws_EnviaEmail.ExecuteSoapOut");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:ws_EnviaEmail.ExecuteResponse");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("portType");
               GXSoapXMLWriter.WriteAttribute("name", "ws_EnviaEmailSoapPort");
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "Execute");
               GXSoapXMLWriter.WriteElement("input", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"ws_EnviaEmail.ExecuteSoapIn");
               GXSoapXMLWriter.WriteElement("output", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"ws_EnviaEmail.ExecuteSoapOut");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("binding");
               GXSoapXMLWriter.WriteAttribute("name", "ws_EnviaEmailSoapBinding");
               GXSoapXMLWriter.WriteAttribute("type", "wsdlns:"+"ws_EnviaEmailSoapPort");
               GXSoapXMLWriter.WriteElement("soap:binding", "");
               GXSoapXMLWriter.WriteAttribute("style", "document");
               GXSoapXMLWriter.WriteAttribute("transport", "http://schemas.xmlsoap.org/soap/http");
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "Execute");
               GXSoapXMLWriter.WriteElement("soap:operation", "");
               GXSoapXMLWriter.WriteAttribute("soapAction", "GxEv3Up14_MeetrikaVs3action/"+"AWS_ENVIAEMAIL.Execute");
               GXSoapXMLWriter.WriteStartElement("input");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("output");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("service");
               GXSoapXMLWriter.WriteAttribute("name", "ws_EnviaEmail");
               GXSoapXMLWriter.WriteStartElement("port");
               GXSoapXMLWriter.WriteAttribute("name", "ws_EnviaEmailSoapPort");
               GXSoapXMLWriter.WriteAttribute("binding", "wsdlns:"+"ws_EnviaEmailSoapBinding");
               GXSoapXMLWriter.WriteElement("soap:address", "");
               GXSoapXMLWriter.WriteAttribute("location", "http://"+context.GetServerName( )+((context.GetServerPort( )>0)&&(context.GetServerPort( )!=80)&&(context.GetServerPort( )!=443) ? ":"+StringUtil.LTrim( StringUtil.Str( (decimal)(context.GetServerPort( )), 6, 0)) : "")+context.GetScriptPath( )+"aws_enviaemail.aspx");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.Close();
               return  ;
            }
            else
            {
               currSoapErr = (short)(-20000);
               currSoapErrmsg = "No SOAP request found. Call " + "http://" + context.GetServerName( ) + ((context.GetServerPort( )>0)&&(context.GetServerPort( )!=80)&&(context.GetServerPort( )!=443) ? ":"+StringUtil.LTrim( StringUtil.Str( (decimal)(context.GetServerPort( )), 6, 0)) : "") + context.GetScriptPath( ) + "aws_enviaemail.aspx" + "?wsdl to get the WSDL.";
            }
         }
         if ( currSoapErr == 0 )
         {
            GXSoapXMLReader.OpenRequest(GXSoapHTTPRequest);
            GXSoapXMLReader.IgnoreComments = 1;
            GXSoapError = GXSoapXMLReader.Read();
            while ( GXSoapError > 0 )
            {
               if ( StringUtil.StringSearch( GXSoapXMLReader.Name, "Body", 1) > 0 )
               {
                  if (true) break;
               }
               GXSoapError = GXSoapXMLReader.Read();
            }
            if ( GXSoapError > 0 )
            {
               GXSoapError = GXSoapXMLReader.Read();
               if ( GXSoapError > 0 )
               {
                  currMethod = GXSoapXMLReader.Name;
                  if ( StringUtil.StringSearch( currMethod+"&", "Execute&", 1) > 0 )
                  {
                     if ( currSoapErr == 0 )
                     {
                        AV22Usuarios = new GxSimpleCollection();
                        AV10Attachments = new GxSimpleCollection();
                        sTagName = GXSoapXMLReader.Name;
                        if ( GXSoapXMLReader.IsSimple == 0 )
                        {
                           GXSoapError = GXSoapXMLReader.Read();
                           nOutParmCount = 0;
                           while ( ( ( StringUtil.StrCmp(GXSoapXMLReader.Name, sTagName) != 0 ) || ( GXSoapXMLReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
                           {
                              readOk = 0;
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "Areatrabalho_codigo") && ( GXSoapXMLReader.NodeType != 2 ) && ( StringUtil.StrCmp(GXSoapXMLReader.NamespaceURI, "GxEv3Up14_MeetrikaVs3") == 0 ) )
                              {
                                 AV8AreaTrabalho_Codigo = (int)(NumberUtil.Val( GXSoapXMLReader.Value, "."));
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "Usuarios") && ( GXSoapXMLReader.NodeType != 2 ) && ( StringUtil.StrCmp(GXSoapXMLReader.NamespaceURI, "GxEv3Up14_MeetrikaVs3") == 0 ) )
                              {
                                 if ( AV22Usuarios == null )
                                 {
                                    AV22Usuarios = new GxSimpleCollection();
                                 }
                                 if ( GXSoapXMLReader.IsSimple == 0 )
                                 {
                                    GXSoapError = AV22Usuarios.readxmlcollection(GXSoapXMLReader, "Usuarios", "item");
                                 }
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "Subject") && ( GXSoapXMLReader.NodeType != 2 ) && ( StringUtil.StrCmp(GXSoapXMLReader.NamespaceURI, "GxEv3Up14_MeetrikaVs3") == 0 ) )
                              {
                                 AV20Subject = GXSoapXMLReader.Value;
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "Emailtext") && ( GXSoapXMLReader.NodeType != 2 ) && ( StringUtil.StrCmp(GXSoapXMLReader.NamespaceURI, "GxEv3Up14_MeetrikaVs3") == 0 ) )
                              {
                                 AV13EmailText = GXSoapXMLReader.Value;
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "Attachments") && ( GXSoapXMLReader.NodeType != 2 ) && ( StringUtil.StrCmp(GXSoapXMLReader.NamespaceURI, "GxEv3Up14_MeetrikaVs3") == 0 ) )
                              {
                                 if ( AV10Attachments == null )
                                 {
                                    AV10Attachments = new GxSimpleCollection();
                                 }
                                 if ( GXSoapXMLReader.IsSimple == 0 )
                                 {
                                    GXSoapError = AV10Attachments.readxmlcollection(GXSoapXMLReader, "Attachments", "item");
                                 }
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "Resultado") && ( GXSoapXMLReader.NodeType != 2 ) && ( StringUtil.StrCmp(GXSoapXMLReader.NamespaceURI, "GxEv3Up14_MeetrikaVs3") == 0 ) )
                              {
                                 AV18Resultado = GXSoapXMLReader.Value;
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              nOutParmCount = (short)(nOutParmCount+1);
                              if ( readOk == 0 )
                              {
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + GXSoapXMLReader.ReadRawXML();
                                 GXSoapError = (short)(nOutParmCount*-1);
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     currSoapErr = (short)(-20002);
                     currSoapErrmsg = "Wrong method called. Expected method: " + "Execute";
                  }
               }
            }
            GXSoapXMLReader.Close();
         }
         if ( currSoapErr == 0 )
         {
            if ( GXSoapError < 0 )
            {
               currSoapErr = (short)(GXSoapError*-1);
               currSoapErrmsg = context.sSOAPErrMsg;
            }
            else
            {
               if ( GXSoapXMLReader.ErrCode > 0 )
               {
                  currSoapErr = (short)(GXSoapXMLReader.ErrCode*-1);
                  currSoapErrmsg = GXSoapXMLReader.ErrDescription;
               }
               else
               {
                  if ( GXSoapError == 0 )
                  {
                     currSoapErr = (short)(-20001);
                     currSoapErrmsg = "Malformed SOAP message.";
                  }
                  else
                  {
                     currSoapErr = 0;
                     currSoapErrmsg = "No error.";
                  }
               }
            }
         }
         if ( currSoapErr == 0 )
         {
            executePrivate();
         }
         context.CloseConnections() ;
         GXSoapXMLWriter.OpenResponse(GXSoapHTTPResponse);
         GXSoapXMLWriter.WriteStartDocument("utf-8", 0);
         GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Envelope");
         GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
         GXSoapXMLWriter.WriteAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
         GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");
         GXSoapXMLWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
         if ( StringUtil.StringSearch( currMethod+"&", "Execute&", 1) > 0 )
         {
            GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Body");
            GXSoapXMLWriter.WriteStartElement("ws_EnviaEmail.ExecuteResponse");
            GXSoapXMLWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            if ( currSoapErr == 0 )
            {
               GXSoapXMLWriter.WriteElement("Resultado", StringUtil.RTrim( AV18Resultado));
               GXSoapXMLWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            else
            {
               GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Fault");
               GXSoapXMLWriter.WriteElement("faultcode", "SOAP-ENV:Client");
               GXSoapXMLWriter.WriteElement("faultstring", currSoapErrmsg);
               GXSoapXMLWriter.WriteElement("detail", StringUtil.Trim( StringUtil.Str( (decimal)(currSoapErr), 10, 0)));
               GXSoapXMLWriter.WriteEndElement();
            }
            GXSoapXMLWriter.WriteEndElement();
            GXSoapXMLWriter.WriteEndElement();
         }
         GXSoapXMLWriter.WriteEndElement();
         GXSoapXMLWriter.Close();
         cleanup();
      }

      public aws_enviaemail( )
      {
         this.DisconnectAtCleanup = true;
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aws_enviaemail( IGxContext context )
      {
         this.DisconnectAtCleanup = true;
         context = context.UtlClone();
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           IGxCollection aP1_Usuarios ,
                           String aP2_Subject ,
                           String aP3_EmailText ,
                           IGxCollection aP4_Attachments ,
                           ref String aP5_Resultado )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV22Usuarios = aP1_Usuarios;
         this.AV20Subject = aP2_Subject;
         this.AV13EmailText = aP3_EmailText;
         this.AV10Attachments = aP4_Attachments;
         this.AV18Resultado = aP5_Resultado;
         initialize();
         executePrivate();
         aP5_Resultado=this.AV18Resultado;
      }

      public String executeUdp( int aP0_AreaTrabalho_Codigo ,
                                IGxCollection aP1_Usuarios ,
                                String aP2_Subject ,
                                String aP3_EmailText ,
                                IGxCollection aP4_Attachments )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV22Usuarios = aP1_Usuarios;
         this.AV20Subject = aP2_Subject;
         this.AV13EmailText = aP3_EmailText;
         this.AV10Attachments = aP4_Attachments;
         this.AV18Resultado = aP5_Resultado;
         initialize();
         executePrivate();
         aP5_Resultado=this.AV18Resultado;
         return AV18Resultado ;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 IGxCollection aP1_Usuarios ,
                                 String aP2_Subject ,
                                 String aP3_EmailText ,
                                 IGxCollection aP4_Attachments ,
                                 ref String aP5_Resultado )
      {
         aws_enviaemail objaws_enviaemail;
         objaws_enviaemail = new aws_enviaemail();
         objaws_enviaemail.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objaws_enviaemail.AV22Usuarios = aP1_Usuarios;
         objaws_enviaemail.AV20Subject = aP2_Subject;
         objaws_enviaemail.AV13EmailText = aP3_EmailText;
         objaws_enviaemail.AV10Attachments = aP4_Attachments;
         objaws_enviaemail.AV18Resultado = aP5_Resultado;
         objaws_enviaemail.context.SetSubmitInitialConfig(context);
         objaws_enviaemail.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaws_enviaemail);
         aP5_Resultado=this.AV18Resultado;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aws_enviaemail)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25i = 1;
         if ( AV22Usuarios.Count == 0 )
         {
            /* Using cursor P00842 */
            pr_default.execute(0, new Object[] {AV8AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A66ContratadaUsuario_ContratadaCod = P00842_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = P00842_A69ContratadaUsuario_UsuarioCod[0];
               A538Usuario_EhGestor = P00842_A538Usuario_EhGestor[0];
               n538Usuario_EhGestor = P00842_n538Usuario_EhGestor[0];
               A516Contratada_TipoFabrica = P00842_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = P00842_n516Contratada_TipoFabrica[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = P00842_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = P00842_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A341Usuario_UserGamGuid = P00842_A341Usuario_UserGamGuid[0];
               n341Usuario_UserGamGuid = P00842_n341Usuario_UserGamGuid[0];
               A516Contratada_TipoFabrica = P00842_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = P00842_n516Contratada_TipoFabrica[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = P00842_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = P00842_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A538Usuario_EhGestor = P00842_A538Usuario_EhGestor[0];
               n538Usuario_EhGestor = P00842_n538Usuario_EhGestor[0];
               A341Usuario_UserGamGuid = P00842_A341Usuario_UserGamGuid[0];
               n341Usuario_UserGamGuid = P00842_n341Usuario_UserGamGuid[0];
               AV14GamUser.load( A341Usuario_UserGamGuid);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14GamUser.gxTpr_Email)) )
               {
                  AV15MailRecipient.Name = StringUtil.Trim( AV14GamUser.gxTpr_Email);
                  AV15MailRecipient.Address = StringUtil.Trim( AV14GamUser.gxTpr_Email);
                  if ( AV25i == 1 )
                  {
                     AV12Email.To.Add(AV15MailRecipient) ;
                     AV11Destinatarios = AV11Destinatarios + AV15MailRecipient.Address;
                  }
                  else
                  {
                     AV12Email.BCC.Add(AV15MailRecipient) ;
                  }
                  AV15MailRecipient = new GeneXus.Mail.GXMailRecipient();
                  AV25i = (short)(AV25i+1);
               }
               pr_default.readNext(0);
            }
            pr_default.close(0);
         }
         else
         {
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 A1Usuario_Codigo ,
                                                 AV22Usuarios },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00843 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1Usuario_Codigo = P00843_A1Usuario_Codigo[0];
               A341Usuario_UserGamGuid = P00843_A341Usuario_UserGamGuid[0];
               n341Usuario_UserGamGuid = P00843_n341Usuario_UserGamGuid[0];
               AV14GamUser.load( A341Usuario_UserGamGuid);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14GamUser.gxTpr_Email)) )
               {
                  AV15MailRecipient.Name = StringUtil.Trim( AV14GamUser.gxTpr_Email);
                  AV15MailRecipient.Address = StringUtil.Trim( AV14GamUser.gxTpr_Email);
                  if ( AV25i == 1 )
                  {
                     AV12Email.To.Add(AV15MailRecipient) ;
                     AV11Destinatarios = AV11Destinatarios + AV15MailRecipient.Address;
                  }
                  else
                  {
                     AV12Email.BCC.Add(AV15MailRecipient) ;
                  }
                  AV15MailRecipient = new GeneXus.Mail.GXMailRecipient();
                  AV25i = (short)(AV25i+1);
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
         if ( AV12Email.To.Count > 0 )
         {
            /* Using cursor P00844 */
            pr_default.execute(2, new Object[] {AV8AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A29Contratante_Codigo = P00844_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00844_n29Contratante_Codigo[0];
               A5AreaTrabalho_Codigo = P00844_A5AreaTrabalho_Codigo[0];
               A548Contratante_EmailSdaUser = P00844_A548Contratante_EmailSdaUser[0];
               n548Contratante_EmailSdaUser = P00844_n548Contratante_EmailSdaUser[0];
               A547Contratante_EmailSdaHost = P00844_A547Contratante_EmailSdaHost[0];
               n547Contratante_EmailSdaHost = P00844_n547Contratante_EmailSdaHost[0];
               A549Contratante_EmailSdaPass = P00844_A549Contratante_EmailSdaPass[0];
               n549Contratante_EmailSdaPass = P00844_n549Contratante_EmailSdaPass[0];
               A550Contratante_EmailSdaKey = P00844_A550Contratante_EmailSdaKey[0];
               n550Contratante_EmailSdaKey = P00844_n550Contratante_EmailSdaKey[0];
               A552Contratante_EmailSdaPort = P00844_A552Contratante_EmailSdaPort[0];
               n552Contratante_EmailSdaPort = P00844_n552Contratante_EmailSdaPort[0];
               A551Contratante_EmailSdaAut = P00844_A551Contratante_EmailSdaAut[0];
               n551Contratante_EmailSdaAut = P00844_n551Contratante_EmailSdaAut[0];
               A1048Contratante_EmailSdaSec = P00844_A1048Contratante_EmailSdaSec[0];
               n1048Contratante_EmailSdaSec = P00844_n1048Contratante_EmailSdaSec[0];
               A548Contratante_EmailSdaUser = P00844_A548Contratante_EmailSdaUser[0];
               n548Contratante_EmailSdaUser = P00844_n548Contratante_EmailSdaUser[0];
               A547Contratante_EmailSdaHost = P00844_A547Contratante_EmailSdaHost[0];
               n547Contratante_EmailSdaHost = P00844_n547Contratante_EmailSdaHost[0];
               A549Contratante_EmailSdaPass = P00844_A549Contratante_EmailSdaPass[0];
               n549Contratante_EmailSdaPass = P00844_n549Contratante_EmailSdaPass[0];
               A550Contratante_EmailSdaKey = P00844_A550Contratante_EmailSdaKey[0];
               n550Contratante_EmailSdaKey = P00844_n550Contratante_EmailSdaKey[0];
               A552Contratante_EmailSdaPort = P00844_A552Contratante_EmailSdaPort[0];
               n552Contratante_EmailSdaPort = P00844_n552Contratante_EmailSdaPort[0];
               A551Contratante_EmailSdaAut = P00844_A551Contratante_EmailSdaAut[0];
               n551Contratante_EmailSdaAut = P00844_n551Contratante_EmailSdaAut[0];
               A1048Contratante_EmailSdaSec = P00844_A1048Contratante_EmailSdaSec[0];
               n1048Contratante_EmailSdaSec = P00844_n1048Contratante_EmailSdaSec[0];
               AV19SMTPSession.AttachDir = "\\";
               AV19SMTPSession.Sender.Name = StringUtil.Trim( A548Contratante_EmailSdaUser);
               AV19SMTPSession.Sender.Address = StringUtil.Trim( A548Contratante_EmailSdaUser);
               AV19SMTPSession.Host = StringUtil.Trim( A547Contratante_EmailSdaHost);
               AV19SMTPSession.UserName = StringUtil.Trim( A548Contratante_EmailSdaUser);
               if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A549Contratante_EmailSdaPass)) || P00844_n549Contratante_EmailSdaPass[0] ) )
               {
                  AV19SMTPSession.Password = Crypto.Decrypt64( A549Contratante_EmailSdaPass, A550Contratante_EmailSdaKey);
               }
               AV19SMTPSession.Port = A552Contratante_EmailSdaPort;
               AV19SMTPSession.Authentication = (short)((A551Contratante_EmailSdaAut ? 1 : 0));
               AV19SMTPSession.Secure = A1048Contratante_EmailSdaSec;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19SMTPSession.Host)) || (0==AV19SMTPSession.Port) || String.IsNullOrEmpty(StringUtil.RTrim( AV19SMTPSession.UserName)) )
            {
               AV18Resultado = "Cadastro da Contratante sem dados configurados para envio de e-mails!" + StringUtil.NewLine( );
            }
            else
            {
               AV12Email.Subject = "[Meetrika] "+AV20Subject;
               AV12Email.Text = AV13EmailText;
               AV12Email.Text = AV12Email.Text+StringUtil.Chr( 13)+"Enviado automáticamente pelo aplicativo MEETRIKA"+StringUtil.Chr( 13);
               AV31GXV1 = 1;
               while ( AV31GXV1 <= AV10Attachments.Count )
               {
                  AV9Attachment = ((String)AV10Attachments.Item(AV31GXV1));
                  AV12Email.Attachments.Add(AV9Attachment) ;
                  AV31GXV1 = (int)(AV31GXV1+1);
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV18Resultado)) )
               {
                  AV18Resultado = "Envio de notificação";
               }
               AV19SMTPSession.Login();
               if ( (0==AV19SMTPSession.ErrCode) )
               {
                  AV19SMTPSession.Send(AV12Email);
                  if ( (0==AV19SMTPSession.ErrCode) )
                  {
                     AV18Resultado = AV18Resultado + " para " + AV11Destinatarios + " com sucesso";
                  }
                  else
                  {
                     AV18Resultado = AV18Resultado + " retornou: " + AV19SMTPSession.ErrDescription;
                     AV16Message.gxTpr_Description = AV19SMTPSession.ErrDescription;
                     AV17Messages.Add(AV16Message, 0);
                  }
                  AV19SMTPSession.Logout();
               }
               else
               {
                  AV18Resultado = "Login para " + StringUtil.Lower( AV18Resultado) + StringUtil.Trim( AV19SMTPSession.Sender.Address) + " retornou: " + AV19SMTPSession.ErrDescription;
               }
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXSoapHTTPRequest = new GxHttpRequest(context) ;
         GXSoapXMLReader = new GXXMLReader(context.GetPhysicalPath());
         GXSoapHTTPResponse = new GxHttpResponse(context) ;
         GXSoapXMLWriter = new GXXMLWriter(context.GetPhysicalPath());
         currSoapErrmsg = "";
         currMethod = "";
         sTagName = "";
         scmdbuf = "";
         P00842_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00842_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00842_A538Usuario_EhGestor = new bool[] {false} ;
         P00842_n538Usuario_EhGestor = new bool[] {false} ;
         P00842_A516Contratada_TipoFabrica = new String[] {""} ;
         P00842_n516Contratada_TipoFabrica = new bool[] {false} ;
         P00842_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P00842_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P00842_A341Usuario_UserGamGuid = new String[] {""} ;
         P00842_n341Usuario_UserGamGuid = new bool[] {false} ;
         A516Contratada_TipoFabrica = "";
         A341Usuario_UserGamGuid = "";
         AV14GamUser = new SdtGAMUser(context);
         AV15MailRecipient = new GeneXus.Mail.GXMailRecipient();
         AV12Email = new GeneXus.Mail.GXMailMessage();
         AV11Destinatarios = "";
         P00843_A1Usuario_Codigo = new int[1] ;
         P00843_A341Usuario_UserGamGuid = new String[] {""} ;
         P00843_n341Usuario_UserGamGuid = new bool[] {false} ;
         P00844_A29Contratante_Codigo = new int[1] ;
         P00844_n29Contratante_Codigo = new bool[] {false} ;
         P00844_A5AreaTrabalho_Codigo = new int[1] ;
         P00844_A548Contratante_EmailSdaUser = new String[] {""} ;
         P00844_n548Contratante_EmailSdaUser = new bool[] {false} ;
         P00844_A547Contratante_EmailSdaHost = new String[] {""} ;
         P00844_n547Contratante_EmailSdaHost = new bool[] {false} ;
         P00844_A549Contratante_EmailSdaPass = new String[] {""} ;
         P00844_n549Contratante_EmailSdaPass = new bool[] {false} ;
         P00844_A550Contratante_EmailSdaKey = new String[] {""} ;
         P00844_n550Contratante_EmailSdaKey = new bool[] {false} ;
         P00844_A552Contratante_EmailSdaPort = new short[1] ;
         P00844_n552Contratante_EmailSdaPort = new bool[] {false} ;
         P00844_A551Contratante_EmailSdaAut = new bool[] {false} ;
         P00844_n551Contratante_EmailSdaAut = new bool[] {false} ;
         P00844_A1048Contratante_EmailSdaSec = new short[1] ;
         P00844_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         A548Contratante_EmailSdaUser = "";
         A547Contratante_EmailSdaHost = "";
         A549Contratante_EmailSdaPass = "";
         A550Contratante_EmailSdaKey = "";
         AV19SMTPSession = new GeneXus.Mail.GXSMTPSession(context.GetPhysicalPath());
         AV9Attachment = "";
         AV16Message = new SdtMessages_Message(context);
         AV17Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aws_enviaemail__default(),
            new Object[][] {
                new Object[] {
               P00842_A66ContratadaUsuario_ContratadaCod, P00842_A69ContratadaUsuario_UsuarioCod, P00842_A538Usuario_EhGestor, P00842_n538Usuario_EhGestor, P00842_A516Contratada_TipoFabrica, P00842_n516Contratada_TipoFabrica, P00842_A1228ContratadaUsuario_AreaTrabalhoCod, P00842_n1228ContratadaUsuario_AreaTrabalhoCod, P00842_A341Usuario_UserGamGuid, P00842_n341Usuario_UserGamGuid
               }
               , new Object[] {
               P00843_A1Usuario_Codigo, P00843_A341Usuario_UserGamGuid
               }
               , new Object[] {
               P00844_A29Contratante_Codigo, P00844_n29Contratante_Codigo, P00844_A5AreaTrabalho_Codigo, P00844_A548Contratante_EmailSdaUser, P00844_n548Contratante_EmailSdaUser, P00844_A547Contratante_EmailSdaHost, P00844_n547Contratante_EmailSdaHost, P00844_A549Contratante_EmailSdaPass, P00844_n549Contratante_EmailSdaPass, P00844_A550Contratante_EmailSdaKey,
               P00844_n550Contratante_EmailSdaKey, P00844_A552Contratante_EmailSdaPort, P00844_n552Contratante_EmailSdaPort, P00844_A551Contratante_EmailSdaAut, P00844_n551Contratante_EmailSdaAut, P00844_A1048Contratante_EmailSdaSec, P00844_n1048Contratante_EmailSdaSec
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short GXSoapError ;
      private short currSoapErr ;
      private short readOk ;
      private short nOutParmCount ;
      private short AV25i ;
      private short A552Contratante_EmailSdaPort ;
      private short A1048Contratante_EmailSdaSec ;
      private int AV8AreaTrabalho_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A1Usuario_Codigo ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int AV31GXV1 ;
      private String currSoapErrmsg ;
      private String currMethod ;
      private String sTagName ;
      private String AV20Subject ;
      private String AV13EmailText ;
      private String AV18Resultado ;
      private String scmdbuf ;
      private String A516Contratada_TipoFabrica ;
      private String A341Usuario_UserGamGuid ;
      private String A550Contratante_EmailSdaKey ;
      private bool A538Usuario_EhGestor ;
      private bool n538Usuario_EhGestor ;
      private bool n516Contratada_TipoFabrica ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n341Usuario_UserGamGuid ;
      private bool n29Contratante_Codigo ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n549Contratante_EmailSdaPass ;
      private bool n550Contratante_EmailSdaKey ;
      private bool n552Contratante_EmailSdaPort ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n1048Contratante_EmailSdaSec ;
      private String AV9Attachment ;
      private String AV11Destinatarios ;
      private String A548Contratante_EmailSdaUser ;
      private String A547Contratante_EmailSdaHost ;
      private String A549Contratante_EmailSdaPass ;
      private GXXMLReader GXSoapXMLReader ;
      private GXXMLWriter GXSoapXMLWriter ;
      private GxHttpRequest GXSoapHTTPRequest ;
      private GxHttpResponse GXSoapHTTPResponse ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP5_Resultado ;
      private IDataStoreProvider pr_default ;
      private int[] P00842_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00842_A69ContratadaUsuario_UsuarioCod ;
      private bool[] P00842_A538Usuario_EhGestor ;
      private bool[] P00842_n538Usuario_EhGestor ;
      private String[] P00842_A516Contratada_TipoFabrica ;
      private bool[] P00842_n516Contratada_TipoFabrica ;
      private int[] P00842_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P00842_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String[] P00842_A341Usuario_UserGamGuid ;
      private bool[] P00842_n341Usuario_UserGamGuid ;
      private int[] P00843_A1Usuario_Codigo ;
      private String[] P00843_A341Usuario_UserGamGuid ;
      private bool[] P00843_n341Usuario_UserGamGuid ;
      private int[] P00844_A29Contratante_Codigo ;
      private bool[] P00844_n29Contratante_Codigo ;
      private int[] P00844_A5AreaTrabalho_Codigo ;
      private String[] P00844_A548Contratante_EmailSdaUser ;
      private bool[] P00844_n548Contratante_EmailSdaUser ;
      private String[] P00844_A547Contratante_EmailSdaHost ;
      private bool[] P00844_n547Contratante_EmailSdaHost ;
      private String[] P00844_A549Contratante_EmailSdaPass ;
      private bool[] P00844_n549Contratante_EmailSdaPass ;
      private String[] P00844_A550Contratante_EmailSdaKey ;
      private bool[] P00844_n550Contratante_EmailSdaKey ;
      private short[] P00844_A552Contratante_EmailSdaPort ;
      private bool[] P00844_n552Contratante_EmailSdaPort ;
      private bool[] P00844_A551Contratante_EmailSdaAut ;
      private bool[] P00844_n551Contratante_EmailSdaAut ;
      private short[] P00844_A1048Contratante_EmailSdaSec ;
      private bool[] P00844_n1048Contratante_EmailSdaSec ;
      private GeneXus.Mail.GXMailMessage AV12Email ;
      private GeneXus.Mail.GXMailRecipient AV15MailRecipient ;
      private GeneXus.Mail.GXSMTPSession AV19SMTPSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV22Usuarios ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV10Attachments ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV17Messages ;
      private SdtGAMUser AV14GamUser ;
      private SdtMessages_Message AV16Message ;
   }

   public class aws_enviaemail__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00843( IGxContext context ,
                                             int A1Usuario_Codigo ,
                                             IGxCollection AV22Usuarios )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT [Usuario_Codigo], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV22Usuarios, "[Usuario_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Usuario_Codigo]";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P00843(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00842 ;
          prmP00842 = new Object[] {
          new Object[] {"@AV8AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00844 ;
          prmP00844 = new Object[] {
          new Object[] {"@AV8AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00843 ;
          prmP00843 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00842", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Usuario_EhGestor], T2.[Contratada_TipoFabrica], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[Usuario_UserGamGuid] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T3.[Usuario_EhGestor] = 1) AND (T2.[Contratada_AreaTrabalhoCod] = @AV8AreaTrabalho_Codigo) AND (T2.[Contratada_TipoFabrica] = 'M') ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00842,100,0,true,false )
             ,new CursorDef("P00843", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00843,100,0,true,false )
             ,new CursorDef("P00844", "SELECT TOP 1 T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_EmailSdaUser], T2.[Contratante_EmailSdaHost], T2.[Contratante_EmailSdaPass], T2.[Contratante_EmailSdaKey], T2.[Contratante_EmailSdaPort], T2.[Contratante_EmailSdaAut], T2.[Contratante_EmailSdaSec] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AV8AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00844,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 40) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 32) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
