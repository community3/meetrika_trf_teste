/*
               File: PRC_GravaContratanteAreaTrabalho
        Description: PRC_Grava Contratante Area Trabalho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:47.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_gravacontratanteareatrabalho : GXProcedure
   {
      public prc_gravacontratanteareatrabalho( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_gravacontratanteareatrabalho( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratante_Codigo ,
                           int aP1_AreaTrabalho_Codigo )
      {
         this.AV8Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV9AreaTrabalho_Codigo = aP1_AreaTrabalho_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratante_Codigo ,
                                 int aP1_AreaTrabalho_Codigo )
      {
         prc_gravacontratanteareatrabalho objprc_gravacontratanteareatrabalho;
         objprc_gravacontratanteareatrabalho = new prc_gravacontratanteareatrabalho();
         objprc_gravacontratanteareatrabalho.AV8Contratante_Codigo = aP0_Contratante_Codigo;
         objprc_gravacontratanteareatrabalho.AV9AreaTrabalho_Codigo = aP1_AreaTrabalho_Codigo;
         objprc_gravacontratanteareatrabalho.context.SetSubmitInitialConfig(context);
         objprc_gravacontratanteareatrabalho.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_gravacontratanteareatrabalho);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_gravacontratanteareatrabalho)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P00162 */
         pr_default.execute(0, new Object[] {n29Contratante_Codigo, AV8Contratante_Codigo, AV9AreaTrabalho_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("AreaTrabalho") ;
         dsDefault.SmartCacheProvider.SetUpdated("AreaTrabalho") ;
         /* End optimized UPDATE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_GravaContratanteAreaTrabalho");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_gravacontratanteareatrabalho__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Contratante_Codigo ;
      private int AV9AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private bool n29Contratante_Codigo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_gravacontratanteareatrabalho__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00162 ;
          prmP00162 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00162", "UPDATE [AreaTrabalho] SET [Contratante_Codigo]=@Contratante_Codigo  WHERE [AreaTrabalho_Codigo] = @AV9AreaTrabalho_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00162)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
