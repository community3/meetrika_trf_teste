/*
               File: GetPromptGeral_FuncaoFilterData
        Description: Get Prompt Geral_Funcao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:47.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptgeral_funcaofilterdata : GXProcedure
   {
      public getpromptgeral_funcaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptgeral_funcaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptgeral_funcaofilterdata objgetpromptgeral_funcaofilterdata;
         objgetpromptgeral_funcaofilterdata = new getpromptgeral_funcaofilterdata();
         objgetpromptgeral_funcaofilterdata.AV20DDOName = aP0_DDOName;
         objgetpromptgeral_funcaofilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetpromptgeral_funcaofilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptgeral_funcaofilterdata.AV24OptionsJson = "" ;
         objgetpromptgeral_funcaofilterdata.AV27OptionsDescJson = "" ;
         objgetpromptgeral_funcaofilterdata.AV29OptionIndexesJson = "" ;
         objgetpromptgeral_funcaofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptgeral_funcaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptgeral_funcaofilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptgeral_funcaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_FUNCAO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_FUNCAO_UONOM") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAO_UONOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("PromptGeral_FuncaoGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptGeral_FuncaoGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("PromptGeral_FuncaoGridState"), "");
         }
         AV50GXV1 = 1;
         while ( AV50GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV50GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "FUNCAO_ATIVO") == 0 )
            {
               AV36Funcao_Ativo = BooleanUtil.Val( AV34GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAO_NOME") == 0 )
            {
               AV10TFFuncao_Nome = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAO_NOME_SEL") == 0 )
            {
               AV11TFFuncao_Nome_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFGRUPOFUNCAO_CODIGO") == 0 )
            {
               AV12TFGrupoFuncao_Codigo = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
               AV13TFGrupoFuncao_Codigo_To = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAO_GTO_SEL") == 0 )
            {
               AV14TFFuncao_GTO_Sel = (short)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAO_UONOM") == 0 )
            {
               AV15TFFuncao_UONom = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAO_UONOM_SEL") == 0 )
            {
               AV16TFFuncao_UONom_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFFUNCAO_ATIVO_SEL") == 0 )
            {
               AV17TFFuncao_Ativo_Sel = (short)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
            }
            AV50GXV1 = (int)(AV50GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV37DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "FUNCAO_NOME") == 0 )
            {
               AV38Funcao_Nome1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "FUNCAO_UONOM") == 0 )
            {
               AV39Funcao_UONom1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV40DynamicFiltersEnabled2 = true;
               AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(2));
               AV41DynamicFiltersSelector2 = AV35GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "FUNCAO_NOME") == 0 )
               {
                  AV42Funcao_Nome2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "FUNCAO_UONOM") == 0 )
               {
                  AV43Funcao_UONom2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV44DynamicFiltersEnabled3 = true;
                  AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(3));
                  AV45DynamicFiltersSelector3 = AV35GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "FUNCAO_NOME") == 0 )
                  {
                     AV46Funcao_Nome3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "FUNCAO_UONOM") == 0 )
                  {
                     AV47Funcao_UONom3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAO_NOMEOPTIONS' Routine */
         AV10TFFuncao_Nome = AV18SearchTxt;
         AV11TFFuncao_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV37DynamicFiltersSelector1 ,
                                              AV38Funcao_Nome1 ,
                                              AV39Funcao_UONom1 ,
                                              AV40DynamicFiltersEnabled2 ,
                                              AV41DynamicFiltersSelector2 ,
                                              AV42Funcao_Nome2 ,
                                              AV43Funcao_UONom2 ,
                                              AV44DynamicFiltersEnabled3 ,
                                              AV45DynamicFiltersSelector3 ,
                                              AV46Funcao_Nome3 ,
                                              AV47Funcao_UONom3 ,
                                              AV11TFFuncao_Nome_Sel ,
                                              AV10TFFuncao_Nome ,
                                              AV12TFGrupoFuncao_Codigo ,
                                              AV13TFGrupoFuncao_Codigo_To ,
                                              AV14TFFuncao_GTO_Sel ,
                                              AV16TFFuncao_UONom_Sel ,
                                              AV15TFFuncao_UONom ,
                                              AV17TFFuncao_Ativo_Sel ,
                                              A622Funcao_Nome ,
                                              A636Funcao_UONom ,
                                              A619GrupoFuncao_Codigo ,
                                              A623Funcao_GTO ,
                                              A630Funcao_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV38Funcao_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV38Funcao_Nome1), "%", "");
         lV39Funcao_UONom1 = StringUtil.PadR( StringUtil.RTrim( AV39Funcao_UONom1), 50, "%");
         lV42Funcao_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV42Funcao_Nome2), "%", "");
         lV43Funcao_UONom2 = StringUtil.PadR( StringUtil.RTrim( AV43Funcao_UONom2), 50, "%");
         lV46Funcao_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV46Funcao_Nome3), "%", "");
         lV47Funcao_UONom3 = StringUtil.PadR( StringUtil.RTrim( AV47Funcao_UONom3), 50, "%");
         lV10TFFuncao_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncao_Nome), "%", "");
         lV15TFFuncao_UONom = StringUtil.PadR( StringUtil.RTrim( AV15TFFuncao_UONom), 50, "%");
         /* Using cursor P00N02 */
         pr_default.execute(0, new Object[] {lV38Funcao_Nome1, lV39Funcao_UONom1, lV42Funcao_Nome2, lV43Funcao_UONom2, lV46Funcao_Nome3, lV47Funcao_UONom3, lV10TFFuncao_Nome, AV11TFFuncao_Nome_Sel, AV12TFGrupoFuncao_Codigo, AV13TFGrupoFuncao_Codigo_To, lV15TFFuncao_UONom, AV16TFFuncao_UONom_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKN02 = false;
            A635Funcao_UOCod = P00N02_A635Funcao_UOCod[0];
            n635Funcao_UOCod = P00N02_n635Funcao_UOCod[0];
            A630Funcao_Ativo = P00N02_A630Funcao_Ativo[0];
            A622Funcao_Nome = P00N02_A622Funcao_Nome[0];
            A623Funcao_GTO = P00N02_A623Funcao_GTO[0];
            A619GrupoFuncao_Codigo = P00N02_A619GrupoFuncao_Codigo[0];
            A636Funcao_UONom = P00N02_A636Funcao_UONom[0];
            n636Funcao_UONom = P00N02_n636Funcao_UONom[0];
            A621Funcao_Codigo = P00N02_A621Funcao_Codigo[0];
            A636Funcao_UONom = P00N02_A636Funcao_UONom[0];
            n636Funcao_UONom = P00N02_n636Funcao_UONom[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00N02_A622Funcao_Nome[0], A622Funcao_Nome) == 0 ) )
            {
               BRKN02 = false;
               A621Funcao_Codigo = P00N02_A621Funcao_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKN02 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A622Funcao_Nome)) )
            {
               AV22Option = A622Funcao_Nome;
               AV25OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A622Funcao_Nome, "@!")));
               AV23Options.Add(AV22Option, 0);
               AV26OptionsDesc.Add(AV25OptionDesc, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKN02 )
            {
               BRKN02 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADFUNCAO_UONOMOPTIONS' Routine */
         AV15TFFuncao_UONom = AV18SearchTxt;
         AV16TFFuncao_UONom_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV37DynamicFiltersSelector1 ,
                                              AV38Funcao_Nome1 ,
                                              AV39Funcao_UONom1 ,
                                              AV40DynamicFiltersEnabled2 ,
                                              AV41DynamicFiltersSelector2 ,
                                              AV42Funcao_Nome2 ,
                                              AV43Funcao_UONom2 ,
                                              AV44DynamicFiltersEnabled3 ,
                                              AV45DynamicFiltersSelector3 ,
                                              AV46Funcao_Nome3 ,
                                              AV47Funcao_UONom3 ,
                                              AV11TFFuncao_Nome_Sel ,
                                              AV10TFFuncao_Nome ,
                                              AV12TFGrupoFuncao_Codigo ,
                                              AV13TFGrupoFuncao_Codigo_To ,
                                              AV14TFFuncao_GTO_Sel ,
                                              AV16TFFuncao_UONom_Sel ,
                                              AV15TFFuncao_UONom ,
                                              AV17TFFuncao_Ativo_Sel ,
                                              A622Funcao_Nome ,
                                              A636Funcao_UONom ,
                                              A619GrupoFuncao_Codigo ,
                                              A623Funcao_GTO ,
                                              A630Funcao_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV38Funcao_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV38Funcao_Nome1), "%", "");
         lV39Funcao_UONom1 = StringUtil.PadR( StringUtil.RTrim( AV39Funcao_UONom1), 50, "%");
         lV42Funcao_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV42Funcao_Nome2), "%", "");
         lV43Funcao_UONom2 = StringUtil.PadR( StringUtil.RTrim( AV43Funcao_UONom2), 50, "%");
         lV46Funcao_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV46Funcao_Nome3), "%", "");
         lV47Funcao_UONom3 = StringUtil.PadR( StringUtil.RTrim( AV47Funcao_UONom3), 50, "%");
         lV10TFFuncao_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncao_Nome), "%", "");
         lV15TFFuncao_UONom = StringUtil.PadR( StringUtil.RTrim( AV15TFFuncao_UONom), 50, "%");
         /* Using cursor P00N03 */
         pr_default.execute(1, new Object[] {lV38Funcao_Nome1, lV39Funcao_UONom1, lV42Funcao_Nome2, lV43Funcao_UONom2, lV46Funcao_Nome3, lV47Funcao_UONom3, lV10TFFuncao_Nome, AV11TFFuncao_Nome_Sel, AV12TFGrupoFuncao_Codigo, AV13TFGrupoFuncao_Codigo_To, lV15TFFuncao_UONom, AV16TFFuncao_UONom_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKN04 = false;
            A635Funcao_UOCod = P00N03_A635Funcao_UOCod[0];
            n635Funcao_UOCod = P00N03_n635Funcao_UOCod[0];
            A623Funcao_GTO = P00N03_A623Funcao_GTO[0];
            A619GrupoFuncao_Codigo = P00N03_A619GrupoFuncao_Codigo[0];
            A636Funcao_UONom = P00N03_A636Funcao_UONom[0];
            n636Funcao_UONom = P00N03_n636Funcao_UONom[0];
            A622Funcao_Nome = P00N03_A622Funcao_Nome[0];
            A630Funcao_Ativo = P00N03_A630Funcao_Ativo[0];
            A621Funcao_Codigo = P00N03_A621Funcao_Codigo[0];
            A636Funcao_UONom = P00N03_A636Funcao_UONom[0];
            n636Funcao_UONom = P00N03_n636Funcao_UONom[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00N03_A635Funcao_UOCod[0] == A635Funcao_UOCod ) )
            {
               BRKN04 = false;
               A621Funcao_Codigo = P00N03_A621Funcao_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKN04 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A636Funcao_UONom)) )
            {
               AV22Option = A636Funcao_UONom;
               AV21InsertIndex = 1;
               while ( ( AV21InsertIndex <= AV23Options.Count ) && ( StringUtil.StrCmp(((String)AV23Options.Item(AV21InsertIndex)), AV22Option) < 0 ) )
               {
                  AV21InsertIndex = (int)(AV21InsertIndex+1);
               }
               AV23Options.Add(AV22Option, AV21InsertIndex);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), AV21InsertIndex);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKN04 )
            {
               BRKN04 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV36Funcao_Ativo = true;
         AV10TFFuncao_Nome = "";
         AV11TFFuncao_Nome_Sel = "";
         AV15TFFuncao_UONom = "";
         AV16TFFuncao_UONom_Sel = "";
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV37DynamicFiltersSelector1 = "";
         AV38Funcao_Nome1 = "";
         AV39Funcao_UONom1 = "";
         AV41DynamicFiltersSelector2 = "";
         AV42Funcao_Nome2 = "";
         AV43Funcao_UONom2 = "";
         AV45DynamicFiltersSelector3 = "";
         AV46Funcao_Nome3 = "";
         AV47Funcao_UONom3 = "";
         scmdbuf = "";
         lV38Funcao_Nome1 = "";
         lV39Funcao_UONom1 = "";
         lV42Funcao_Nome2 = "";
         lV43Funcao_UONom2 = "";
         lV46Funcao_Nome3 = "";
         lV47Funcao_UONom3 = "";
         lV10TFFuncao_Nome = "";
         lV15TFFuncao_UONom = "";
         A622Funcao_Nome = "";
         A636Funcao_UONom = "";
         P00N02_A635Funcao_UOCod = new int[1] ;
         P00N02_n635Funcao_UOCod = new bool[] {false} ;
         P00N02_A630Funcao_Ativo = new bool[] {false} ;
         P00N02_A622Funcao_Nome = new String[] {""} ;
         P00N02_A623Funcao_GTO = new bool[] {false} ;
         P00N02_A619GrupoFuncao_Codigo = new int[1] ;
         P00N02_A636Funcao_UONom = new String[] {""} ;
         P00N02_n636Funcao_UONom = new bool[] {false} ;
         P00N02_A621Funcao_Codigo = new int[1] ;
         AV22Option = "";
         AV25OptionDesc = "";
         P00N03_A635Funcao_UOCod = new int[1] ;
         P00N03_n635Funcao_UOCod = new bool[] {false} ;
         P00N03_A623Funcao_GTO = new bool[] {false} ;
         P00N03_A619GrupoFuncao_Codigo = new int[1] ;
         P00N03_A636Funcao_UONom = new String[] {""} ;
         P00N03_n636Funcao_UONom = new bool[] {false} ;
         P00N03_A622Funcao_Nome = new String[] {""} ;
         P00N03_A630Funcao_Ativo = new bool[] {false} ;
         P00N03_A621Funcao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptgeral_funcaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00N02_A635Funcao_UOCod, P00N02_n635Funcao_UOCod, P00N02_A630Funcao_Ativo, P00N02_A622Funcao_Nome, P00N02_A623Funcao_GTO, P00N02_A619GrupoFuncao_Codigo, P00N02_A636Funcao_UONom, P00N02_n636Funcao_UONom, P00N02_A621Funcao_Codigo
               }
               , new Object[] {
               P00N03_A635Funcao_UOCod, P00N03_n635Funcao_UOCod, P00N03_A623Funcao_GTO, P00N03_A619GrupoFuncao_Codigo, P00N03_A636Funcao_UONom, P00N03_n636Funcao_UONom, P00N03_A622Funcao_Nome, P00N03_A630Funcao_Ativo, P00N03_A621Funcao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFFuncao_GTO_Sel ;
      private short AV17TFFuncao_Ativo_Sel ;
      private int AV50GXV1 ;
      private int AV12TFGrupoFuncao_Codigo ;
      private int AV13TFGrupoFuncao_Codigo_To ;
      private int A619GrupoFuncao_Codigo ;
      private int A635Funcao_UOCod ;
      private int A621Funcao_Codigo ;
      private int AV21InsertIndex ;
      private long AV30count ;
      private String AV15TFFuncao_UONom ;
      private String AV16TFFuncao_UONom_Sel ;
      private String AV39Funcao_UONom1 ;
      private String AV43Funcao_UONom2 ;
      private String AV47Funcao_UONom3 ;
      private String scmdbuf ;
      private String lV39Funcao_UONom1 ;
      private String lV43Funcao_UONom2 ;
      private String lV47Funcao_UONom3 ;
      private String lV15TFFuncao_UONom ;
      private String A636Funcao_UONom ;
      private bool returnInSub ;
      private bool AV36Funcao_Ativo ;
      private bool AV40DynamicFiltersEnabled2 ;
      private bool AV44DynamicFiltersEnabled3 ;
      private bool A623Funcao_GTO ;
      private bool A630Funcao_Ativo ;
      private bool BRKN02 ;
      private bool n635Funcao_UOCod ;
      private bool n636Funcao_UONom ;
      private bool BRKN04 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV10TFFuncao_Nome ;
      private String AV11TFFuncao_Nome_Sel ;
      private String AV37DynamicFiltersSelector1 ;
      private String AV38Funcao_Nome1 ;
      private String AV41DynamicFiltersSelector2 ;
      private String AV42Funcao_Nome2 ;
      private String AV45DynamicFiltersSelector3 ;
      private String AV46Funcao_Nome3 ;
      private String lV38Funcao_Nome1 ;
      private String lV42Funcao_Nome2 ;
      private String lV46Funcao_Nome3 ;
      private String lV10TFFuncao_Nome ;
      private String A622Funcao_Nome ;
      private String AV22Option ;
      private String AV25OptionDesc ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00N02_A635Funcao_UOCod ;
      private bool[] P00N02_n635Funcao_UOCod ;
      private bool[] P00N02_A630Funcao_Ativo ;
      private String[] P00N02_A622Funcao_Nome ;
      private bool[] P00N02_A623Funcao_GTO ;
      private int[] P00N02_A619GrupoFuncao_Codigo ;
      private String[] P00N02_A636Funcao_UONom ;
      private bool[] P00N02_n636Funcao_UONom ;
      private int[] P00N02_A621Funcao_Codigo ;
      private int[] P00N03_A635Funcao_UOCod ;
      private bool[] P00N03_n635Funcao_UOCod ;
      private bool[] P00N03_A623Funcao_GTO ;
      private int[] P00N03_A619GrupoFuncao_Codigo ;
      private String[] P00N03_A636Funcao_UONom ;
      private bool[] P00N03_n636Funcao_UONom ;
      private String[] P00N03_A622Funcao_Nome ;
      private bool[] P00N03_A630Funcao_Ativo ;
      private int[] P00N03_A621Funcao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getpromptgeral_funcaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00N02( IGxContext context ,
                                             String AV37DynamicFiltersSelector1 ,
                                             String AV38Funcao_Nome1 ,
                                             String AV39Funcao_UONom1 ,
                                             bool AV40DynamicFiltersEnabled2 ,
                                             String AV41DynamicFiltersSelector2 ,
                                             String AV42Funcao_Nome2 ,
                                             String AV43Funcao_UONom2 ,
                                             bool AV44DynamicFiltersEnabled3 ,
                                             String AV45DynamicFiltersSelector3 ,
                                             String AV46Funcao_Nome3 ,
                                             String AV47Funcao_UONom3 ,
                                             String AV11TFFuncao_Nome_Sel ,
                                             String AV10TFFuncao_Nome ,
                                             int AV12TFGrupoFuncao_Codigo ,
                                             int AV13TFGrupoFuncao_Codigo_To ,
                                             short AV14TFFuncao_GTO_Sel ,
                                             String AV16TFFuncao_UONom_Sel ,
                                             String AV15TFFuncao_UONom ,
                                             short AV17TFFuncao_Ativo_Sel ,
                                             String A622Funcao_Nome ,
                                             String A636Funcao_UONom ,
                                             int A619GrupoFuncao_Codigo ,
                                             bool A623Funcao_GTO ,
                                             bool A630Funcao_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Funcao_UOCod] AS Funcao_UOCod, T1.[Funcao_Ativo], T1.[Funcao_Nome], T1.[Funcao_GTO], T1.[GrupoFuncao_Codigo], T2.[UnidadeOrganizacional_Nome] AS Funcao_UONom, T1.[Funcao_Codigo] FROM ([Geral_Funcao] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Funcao_UOCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Funcao_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Funcao_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV38Funcao_Nome1 + '%')";
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Funcao_UONom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV39Funcao_UONom1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV40DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Funcao_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV42Funcao_Nome2 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV40DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Funcao_UONom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV43Funcao_UONom2 + '%')";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Funcao_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV46Funcao_Nome3 + '%')";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Funcao_UONom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV47Funcao_UONom3 + '%')";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like @lV10TFFuncao_Nome)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] = @AV11TFFuncao_Nome_Sel)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV12TFGrupoFuncao_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoFuncao_Codigo] >= @AV12TFGrupoFuncao_Codigo)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV13TFGrupoFuncao_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoFuncao_Codigo] <= @AV13TFGrupoFuncao_Codigo_To)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV14TFFuncao_GTO_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_GTO] = 1)";
         }
         if ( AV14TFFuncao_GTO_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_GTO] = 0)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV16TFFuncao_UONom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncao_UONom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV15TFFuncao_UONom)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFFuncao_UONom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV16TFFuncao_UONom_Sel)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV17TFFuncao_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Ativo] = 1)";
         }
         if ( AV17TFFuncao_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Funcao_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00N03( IGxContext context ,
                                             String AV37DynamicFiltersSelector1 ,
                                             String AV38Funcao_Nome1 ,
                                             String AV39Funcao_UONom1 ,
                                             bool AV40DynamicFiltersEnabled2 ,
                                             String AV41DynamicFiltersSelector2 ,
                                             String AV42Funcao_Nome2 ,
                                             String AV43Funcao_UONom2 ,
                                             bool AV44DynamicFiltersEnabled3 ,
                                             String AV45DynamicFiltersSelector3 ,
                                             String AV46Funcao_Nome3 ,
                                             String AV47Funcao_UONom3 ,
                                             String AV11TFFuncao_Nome_Sel ,
                                             String AV10TFFuncao_Nome ,
                                             int AV12TFGrupoFuncao_Codigo ,
                                             int AV13TFGrupoFuncao_Codigo_To ,
                                             short AV14TFFuncao_GTO_Sel ,
                                             String AV16TFFuncao_UONom_Sel ,
                                             String AV15TFFuncao_UONom ,
                                             short AV17TFFuncao_Ativo_Sel ,
                                             String A622Funcao_Nome ,
                                             String A636Funcao_UONom ,
                                             int A619GrupoFuncao_Codigo ,
                                             bool A623Funcao_GTO ,
                                             bool A630Funcao_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Funcao_UOCod] AS Funcao_UOCod, T1.[Funcao_GTO], T1.[GrupoFuncao_Codigo], T2.[UnidadeOrganizacional_Nome] AS Funcao_UONom, T1.[Funcao_Nome], T1.[Funcao_Ativo], T1.[Funcao_Codigo] FROM ([Geral_Funcao] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Funcao_UOCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Funcao_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Funcao_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV38Funcao_Nome1 + '%')";
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Funcao_UONom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV39Funcao_UONom1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV40DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Funcao_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV42Funcao_Nome2 + '%')";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV40DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Funcao_UONom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV43Funcao_UONom2 + '%')";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Funcao_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV46Funcao_Nome3 + '%')";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Funcao_UONom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV47Funcao_UONom3 + '%')";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] like @lV10TFFuncao_Nome)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Nome] = @AV11TFFuncao_Nome_Sel)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (0==AV12TFGrupoFuncao_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoFuncao_Codigo] >= @AV12TFGrupoFuncao_Codigo)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV13TFGrupoFuncao_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[GrupoFuncao_Codigo] <= @AV13TFGrupoFuncao_Codigo_To)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV14TFFuncao_GTO_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_GTO] = 1)";
         }
         if ( AV14TFFuncao_GTO_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_GTO] = 0)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV16TFFuncao_UONom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncao_UONom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV15TFFuncao_UONom)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFFuncao_UONom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV16TFFuncao_UONom_Sel)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV17TFFuncao_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Ativo] = 1)";
         }
         if ( AV17TFFuncao_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Funcao_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Funcao_UOCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00N02(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (bool)dynConstraints[22] , (bool)dynConstraints[23] );
               case 1 :
                     return conditional_P00N03(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (bool)dynConstraints[22] , (bool)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00N02 ;
          prmP00N02 = new Object[] {
          new Object[] {"@lV38Funcao_Nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV39Funcao_UONom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Funcao_Nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV43Funcao_UONom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46Funcao_Nome3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV47Funcao_UONom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFFuncao_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV11TFFuncao_Nome_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV12TFGrupoFuncao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFGrupoFuncao_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV15TFFuncao_UONom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV16TFFuncao_UONom_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00N03 ;
          prmP00N03 = new Object[] {
          new Object[] {"@lV38Funcao_Nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV39Funcao_UONom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Funcao_Nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV43Funcao_UONom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46Funcao_Nome3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV47Funcao_UONom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFFuncao_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV11TFFuncao_Nome_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV12TFGrupoFuncao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFGrupoFuncao_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV15TFFuncao_UONom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV16TFFuncao_UONom_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00N02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00N02,100,0,true,false )
             ,new CursorDef("P00N03", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00N03,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptgeral_funcaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptgeral_funcaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptgeral_funcaofilterdata") )
          {
             return  ;
          }
          getpromptgeral_funcaofilterdata worker = new getpromptgeral_funcaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
