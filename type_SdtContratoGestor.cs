/*
               File: type_SdtContratoGestor
        Description: Gestor do Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:5.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContratoGestor" )]
   [XmlType(TypeName =  "ContratoGestor" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtContratoGestor : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoGestor( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratoGestor_Contratogestor_contratadasigla = "";
         gxTv_SdtContratoGestor_Contratogestor_contratadatipo = "";
         gxTv_SdtContratoGestor_Contratogestor_contratadaareades = "";
         gxTv_SdtContratoGestor_Contratogestor_usuariopesnom = "";
         gxTv_SdtContratoGestor_Mode = "";
         gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z = "";
         gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z = "";
         gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z = "";
         gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z = "";
      }

      public SdtContratoGestor( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1078ContratoGestor_ContratoCod ,
                        int AV1079ContratoGestor_UsuarioCod )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1078ContratoGestor_ContratoCod,(int)AV1079ContratoGestor_UsuarioCod});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContratoGestor_ContratoCod", typeof(int)}, new Object[]{"ContratoGestor_UsuarioCod", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContratoGestor");
         metadata.Set("BT", "ContratoGestor");
         metadata.Set("PK", "[ \"ContratoGestor_ContratoCod\",\"ContratoGestor_UsuarioCod\" ]");
         metadata.Set("PKAssigned", "[ \"ContratoGestor_ContratoCod\",\"ContratoGestor_UsuarioCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Contrato_Codigo\" ],\"FKMap\":[ \"ContratoGestor_ContratoCod-Contrato_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContratoGestor_UsuarioCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_contratocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_contratadacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_contratadasigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_contratadatipo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_contratadaareacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_contratadaareades_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_usuariocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_usuariopescod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_usuariopesnom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_usuarioehcontratante_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_usuarioatv_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_tipo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_contratadacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_contratadasigla_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_contratadatipo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_contratadaareacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_contratadaareades_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_usuariopescod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_usuariopesnom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_usuarioatv_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratogestor_tipo_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratoGestor deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratoGestor)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratoGestor obj ;
         obj = this;
         obj.gxTpr_Contratogestor_contratocod = deserialized.gxTpr_Contratogestor_contratocod;
         obj.gxTpr_Contratogestor_contratadacod = deserialized.gxTpr_Contratogestor_contratadacod;
         obj.gxTpr_Contratogestor_contratadasigla = deserialized.gxTpr_Contratogestor_contratadasigla;
         obj.gxTpr_Contratogestor_contratadatipo = deserialized.gxTpr_Contratogestor_contratadatipo;
         obj.gxTpr_Contratogestor_contratadaareacod = deserialized.gxTpr_Contratogestor_contratadaareacod;
         obj.gxTpr_Contratogestor_contratadaareades = deserialized.gxTpr_Contratogestor_contratadaareades;
         obj.gxTpr_Contratogestor_usuariocod = deserialized.gxTpr_Contratogestor_usuariocod;
         obj.gxTpr_Contratogestor_usuariopescod = deserialized.gxTpr_Contratogestor_usuariopescod;
         obj.gxTpr_Contratogestor_usuariopesnom = deserialized.gxTpr_Contratogestor_usuariopesnom;
         obj.gxTpr_Contratogestor_usuarioehcontratante = deserialized.gxTpr_Contratogestor_usuarioehcontratante;
         obj.gxTpr_Contratogestor_usuarioatv = deserialized.gxTpr_Contratogestor_usuarioatv;
         obj.gxTpr_Contratogestor_tipo = deserialized.gxTpr_Contratogestor_tipo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratogestor_contratocod_Z = deserialized.gxTpr_Contratogestor_contratocod_Z;
         obj.gxTpr_Contratogestor_contratadacod_Z = deserialized.gxTpr_Contratogestor_contratadacod_Z;
         obj.gxTpr_Contratogestor_contratadasigla_Z = deserialized.gxTpr_Contratogestor_contratadasigla_Z;
         obj.gxTpr_Contratogestor_contratadatipo_Z = deserialized.gxTpr_Contratogestor_contratadatipo_Z;
         obj.gxTpr_Contratogestor_contratadaareacod_Z = deserialized.gxTpr_Contratogestor_contratadaareacod_Z;
         obj.gxTpr_Contratogestor_contratadaareades_Z = deserialized.gxTpr_Contratogestor_contratadaareades_Z;
         obj.gxTpr_Contratogestor_usuariocod_Z = deserialized.gxTpr_Contratogestor_usuariocod_Z;
         obj.gxTpr_Contratogestor_usuariopescod_Z = deserialized.gxTpr_Contratogestor_usuariopescod_Z;
         obj.gxTpr_Contratogestor_usuariopesnom_Z = deserialized.gxTpr_Contratogestor_usuariopesnom_Z;
         obj.gxTpr_Contratogestor_usuarioehcontratante_Z = deserialized.gxTpr_Contratogestor_usuarioehcontratante_Z;
         obj.gxTpr_Contratogestor_usuarioatv_Z = deserialized.gxTpr_Contratogestor_usuarioatv_Z;
         obj.gxTpr_Contratogestor_tipo_Z = deserialized.gxTpr_Contratogestor_tipo_Z;
         obj.gxTpr_Contratogestor_contratadacod_N = deserialized.gxTpr_Contratogestor_contratadacod_N;
         obj.gxTpr_Contratogestor_contratadasigla_N = deserialized.gxTpr_Contratogestor_contratadasigla_N;
         obj.gxTpr_Contratogestor_contratadatipo_N = deserialized.gxTpr_Contratogestor_contratadatipo_N;
         obj.gxTpr_Contratogestor_contratadaareacod_N = deserialized.gxTpr_Contratogestor_contratadaareacod_N;
         obj.gxTpr_Contratogestor_contratadaareades_N = deserialized.gxTpr_Contratogestor_contratadaareades_N;
         obj.gxTpr_Contratogestor_usuariopescod_N = deserialized.gxTpr_Contratogestor_usuariopescod_N;
         obj.gxTpr_Contratogestor_usuariopesnom_N = deserialized.gxTpr_Contratogestor_usuariopesnom_N;
         obj.gxTpr_Contratogestor_usuarioatv_N = deserialized.gxTpr_Contratogestor_usuarioatv_N;
         obj.gxTpr_Contratogestor_tipo_N = deserialized.gxTpr_Contratogestor_tipo_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratoCod") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaCod") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaSigla") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadasigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaTipo") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadatipo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaAreaCod") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadaareacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaAreaDes") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadaareades = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioCod") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuariocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioPesCod") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuariopescod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioPesNom") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuariopesnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioEhContratante") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioAtv") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuarioatv = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_Tipo") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_tipo = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratoGestor_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratoGestor_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratoCod_Z") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaCod_Z") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaSigla_Z") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaTipo_Z") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaAreaCod_Z") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaAreaDes_Z") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioCod_Z") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuariocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioPesCod_Z") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuariopescod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioPesNom_Z") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioEhContratante_Z") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioAtv_Z") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuarioatv_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_Tipo_Z") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_tipo_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaCod_N") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaSigla_N") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadasigla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaTipo_N") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadatipo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaAreaCod_N") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_ContratadaAreaDes_N") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_contratadaareades_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioPesCod_N") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuariopescod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioPesNom_N") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_UsuarioAtv_N") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_usuarioatv_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoGestor_Tipo_N") )
               {
                  gxTv_SdtContratoGestor_Contratogestor_tipo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContratoGestor";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratoGestor_ContratoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_contratocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoGestor_ContratadaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_contratadacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoGestor_ContratadaSigla", StringUtil.RTrim( gxTv_SdtContratoGestor_Contratogestor_contratadasigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoGestor_ContratadaTipo", StringUtil.RTrim( gxTv_SdtContratoGestor_Contratogestor_contratadatipo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoGestor_ContratadaAreaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_contratadaareacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoGestor_ContratadaAreaDes", StringUtil.RTrim( gxTv_SdtContratoGestor_Contratogestor_contratadaareades));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoGestor_UsuarioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_usuariocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoGestor_UsuarioPesCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_usuariopescod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoGestor_UsuarioPesNom", StringUtil.RTrim( gxTv_SdtContratoGestor_Contratogestor_usuariopesnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoGestor_UsuarioEhContratante", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoGestor_UsuarioAtv", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoGestor_Contratogestor_usuarioatv)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratoGestor_Tipo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_tipo), 1, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratoGestor_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_ContratoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_contratocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_ContratadaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_contratadacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_ContratadaSigla_Z", StringUtil.RTrim( gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_ContratadaTipo_Z", StringUtil.RTrim( gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_ContratadaAreaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_ContratadaAreaDes_Z", StringUtil.RTrim( gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_UsuarioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_usuariocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_UsuarioPesCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_usuariopescod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_UsuarioPesNom_Z", StringUtil.RTrim( gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_UsuarioEhContratante_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_UsuarioAtv_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratoGestor_Contratogestor_usuarioatv_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_Tipo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_tipo_Z), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_ContratadaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_contratadacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_ContratadaSigla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_contratadasigla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_ContratadaTipo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_contratadatipo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_ContratadaAreaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_ContratadaAreaDes_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_contratadaareades_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_UsuarioPesCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_usuariopescod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_UsuarioPesNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_UsuarioAtv_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_usuarioatv_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratoGestor_Tipo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoGestor_Contratogestor_tipo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratoGestor_ContratoCod", gxTv_SdtContratoGestor_Contratogestor_contratocod, false);
         AddObjectProperty("ContratoGestor_ContratadaCod", gxTv_SdtContratoGestor_Contratogestor_contratadacod, false);
         AddObjectProperty("ContratoGestor_ContratadaSigla", gxTv_SdtContratoGestor_Contratogestor_contratadasigla, false);
         AddObjectProperty("ContratoGestor_ContratadaTipo", gxTv_SdtContratoGestor_Contratogestor_contratadatipo, false);
         AddObjectProperty("ContratoGestor_ContratadaAreaCod", gxTv_SdtContratoGestor_Contratogestor_contratadaareacod, false);
         AddObjectProperty("ContratoGestor_ContratadaAreaDes", gxTv_SdtContratoGestor_Contratogestor_contratadaareades, false);
         AddObjectProperty("ContratoGestor_UsuarioCod", gxTv_SdtContratoGestor_Contratogestor_usuariocod, false);
         AddObjectProperty("ContratoGestor_UsuarioPesCod", gxTv_SdtContratoGestor_Contratogestor_usuariopescod, false);
         AddObjectProperty("ContratoGestor_UsuarioPesNom", gxTv_SdtContratoGestor_Contratogestor_usuariopesnom, false);
         AddObjectProperty("ContratoGestor_UsuarioEhContratante", gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante, false);
         AddObjectProperty("ContratoGestor_UsuarioAtv", gxTv_SdtContratoGestor_Contratogestor_usuarioatv, false);
         AddObjectProperty("ContratoGestor_Tipo", gxTv_SdtContratoGestor_Contratogestor_tipo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratoGestor_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratoGestor_Initialized, false);
            AddObjectProperty("ContratoGestor_ContratoCod_Z", gxTv_SdtContratoGestor_Contratogestor_contratocod_Z, false);
            AddObjectProperty("ContratoGestor_ContratadaCod_Z", gxTv_SdtContratoGestor_Contratogestor_contratadacod_Z, false);
            AddObjectProperty("ContratoGestor_ContratadaSigla_Z", gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z, false);
            AddObjectProperty("ContratoGestor_ContratadaTipo_Z", gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z, false);
            AddObjectProperty("ContratoGestor_ContratadaAreaCod_Z", gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_Z, false);
            AddObjectProperty("ContratoGestor_ContratadaAreaDes_Z", gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z, false);
            AddObjectProperty("ContratoGestor_UsuarioCod_Z", gxTv_SdtContratoGestor_Contratogestor_usuariocod_Z, false);
            AddObjectProperty("ContratoGestor_UsuarioPesCod_Z", gxTv_SdtContratoGestor_Contratogestor_usuariopescod_Z, false);
            AddObjectProperty("ContratoGestor_UsuarioPesNom_Z", gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z, false);
            AddObjectProperty("ContratoGestor_UsuarioEhContratante_Z", gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_Z, false);
            AddObjectProperty("ContratoGestor_UsuarioAtv_Z", gxTv_SdtContratoGestor_Contratogestor_usuarioatv_Z, false);
            AddObjectProperty("ContratoGestor_Tipo_Z", gxTv_SdtContratoGestor_Contratogestor_tipo_Z, false);
            AddObjectProperty("ContratoGestor_ContratadaCod_N", gxTv_SdtContratoGestor_Contratogestor_contratadacod_N, false);
            AddObjectProperty("ContratoGestor_ContratadaSigla_N", gxTv_SdtContratoGestor_Contratogestor_contratadasigla_N, false);
            AddObjectProperty("ContratoGestor_ContratadaTipo_N", gxTv_SdtContratoGestor_Contratogestor_contratadatipo_N, false);
            AddObjectProperty("ContratoGestor_ContratadaAreaCod_N", gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_N, false);
            AddObjectProperty("ContratoGestor_ContratadaAreaDes_N", gxTv_SdtContratoGestor_Contratogestor_contratadaareades_N, false);
            AddObjectProperty("ContratoGestor_UsuarioPesCod_N", gxTv_SdtContratoGestor_Contratogestor_usuariopescod_N, false);
            AddObjectProperty("ContratoGestor_UsuarioPesNom_N", gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_N, false);
            AddObjectProperty("ContratoGestor_UsuarioAtv_N", gxTv_SdtContratoGestor_Contratogestor_usuarioatv_N, false);
            AddObjectProperty("ContratoGestor_Tipo_N", gxTv_SdtContratoGestor_Contratogestor_tipo_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratoCod" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratoCod"   )]
      public int gxTpr_Contratogestor_contratocod
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratocod ;
         }

         set {
            if ( gxTv_SdtContratoGestor_Contratogestor_contratocod != value )
            {
               gxTv_SdtContratoGestor_Mode = "INS";
               this.gxTv_SdtContratoGestor_Contratogestor_contratocod_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_contratadacod_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_usuariocod_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_usuariopescod_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_usuarioatv_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_tipo_Z_SetNull( );
            }
            gxTv_SdtContratoGestor_Contratogestor_contratocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaCod" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaCod"   )]
      public int gxTpr_Contratogestor_contratadacod
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadacod ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadacod_N = 0;
            gxTv_SdtContratoGestor_Contratogestor_contratadacod = (int)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadacod_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadacod_N = 1;
         gxTv_SdtContratoGestor_Contratogestor_contratadacod = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaSigla" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaSigla"   )]
      public String gxTpr_Contratogestor_contratadasigla
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadasigla ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadasigla_N = 0;
            gxTv_SdtContratoGestor_Contratogestor_contratadasigla = (String)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadasigla_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadasigla_N = 1;
         gxTv_SdtContratoGestor_Contratogestor_contratadasigla = "";
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadasigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaTipo" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaTipo"   )]
      public String gxTpr_Contratogestor_contratadatipo
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadatipo ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadatipo_N = 0;
            gxTv_SdtContratoGestor_Contratogestor_contratadatipo = (String)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadatipo_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadatipo_N = 1;
         gxTv_SdtContratoGestor_Contratogestor_contratadatipo = "";
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadatipo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaAreaCod" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaAreaCod"   )]
      public int gxTpr_Contratogestor_contratadaareacod
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadaareacod ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_N = 0;
            gxTv_SdtContratoGestor_Contratogestor_contratadaareacod = (int)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_N = 1;
         gxTv_SdtContratoGestor_Contratogestor_contratadaareacod = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaAreaDes" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaAreaDes"   )]
      public String gxTpr_Contratogestor_contratadaareades
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadaareades ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadaareades_N = 0;
            gxTv_SdtContratoGestor_Contratogestor_contratadaareades = (String)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadaareades_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadaareades_N = 1;
         gxTv_SdtContratoGestor_Contratogestor_contratadaareades = "";
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadaareades_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioCod" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioCod"   )]
      public int gxTpr_Contratogestor_usuariocod
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuariocod ;
         }

         set {
            if ( gxTv_SdtContratoGestor_Contratogestor_usuariocod != value )
            {
               gxTv_SdtContratoGestor_Mode = "INS";
               this.gxTv_SdtContratoGestor_Contratogestor_contratocod_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_contratadacod_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_usuariocod_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_usuariopescod_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_usuarioatv_Z_SetNull( );
               this.gxTv_SdtContratoGestor_Contratogestor_tipo_Z_SetNull( );
            }
            gxTv_SdtContratoGestor_Contratogestor_usuariocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioPesCod" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioPesCod"   )]
      public int gxTpr_Contratogestor_usuariopescod
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuariopescod ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_usuariopescod_N = 0;
            gxTv_SdtContratoGestor_Contratogestor_usuariopescod = (int)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_usuariopescod_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_usuariopescod_N = 1;
         gxTv_SdtContratoGestor_Contratogestor_usuariopescod = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_usuariopescod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioPesNom" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioPesNom"   )]
      public String gxTpr_Contratogestor_usuariopesnom
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuariopesnom ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_N = 0;
            gxTv_SdtContratoGestor_Contratogestor_usuariopesnom = (String)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_N = 1;
         gxTv_SdtContratoGestor_Contratogestor_usuariopesnom = "";
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioEhContratante" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioEhContratante"   )]
      public bool gxTpr_Contratogestor_usuarioehcontratante
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante = value;
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante = false;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioAtv" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioAtv"   )]
      public bool gxTpr_Contratogestor_usuarioatv
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuarioatv ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_usuarioatv_N = 0;
            gxTv_SdtContratoGestor_Contratogestor_usuarioatv = value;
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_usuarioatv_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_usuarioatv_N = 1;
         gxTv_SdtContratoGestor_Contratogestor_usuarioatv = false;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_usuarioatv_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_Tipo" )]
      [  XmlElement( ElementName = "ContratoGestor_Tipo"   )]
      public short gxTpr_Contratogestor_tipo
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_tipo ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_tipo_N = 0;
            gxTv_SdtContratoGestor_Contratogestor_tipo = (short)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_tipo_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_tipo_N = 1;
         gxTv_SdtContratoGestor_Contratogestor_tipo = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_tipo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratoGestor_Mode ;
         }

         set {
            gxTv_SdtContratoGestor_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Mode_SetNull( )
      {
         gxTv_SdtContratoGestor_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratoGestor_Initialized ;
         }

         set {
            gxTv_SdtContratoGestor_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Initialized_SetNull( )
      {
         gxTv_SdtContratoGestor_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratoCod_Z" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratoCod_Z"   )]
      public int gxTpr_Contratogestor_contratocod_Z
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratocod_Z ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratocod_Z_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaCod_Z" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaCod_Z"   )]
      public int gxTpr_Contratogestor_contratadacod_Z
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadacod_Z ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadacod_Z_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaSigla_Z" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaSigla_Z"   )]
      public String gxTpr_Contratogestor_contratadasigla_Z
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaTipo_Z" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaTipo_Z"   )]
      public String gxTpr_Contratogestor_contratadatipo_Z
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaAreaCod_Z" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaAreaCod_Z"   )]
      public int gxTpr_Contratogestor_contratadaareacod_Z
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_Z ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_Z_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaAreaDes_Z" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaAreaDes_Z"   )]
      public String gxTpr_Contratogestor_contratadaareades_Z
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioCod_Z" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioCod_Z"   )]
      public int gxTpr_Contratogestor_usuariocod_Z
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuariocod_Z ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_usuariocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_usuariocod_Z_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_usuariocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_usuariocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioPesCod_Z" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioPesCod_Z"   )]
      public int gxTpr_Contratogestor_usuariopescod_Z
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuariopescod_Z ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_usuariopescod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_usuariopescod_Z_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_usuariopescod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_usuariopescod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioPesNom_Z" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioPesNom_Z"   )]
      public String gxTpr_Contratogestor_usuariopesnom_Z
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioEhContratante_Z" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioEhContratante_Z"   )]
      public bool gxTpr_Contratogestor_usuarioehcontratante_Z
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_Z ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_Z = value;
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_Z_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioAtv_Z" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioAtv_Z"   )]
      public bool gxTpr_Contratogestor_usuarioatv_Z
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuarioatv_Z ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_usuarioatv_Z = value;
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_usuarioatv_Z_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_usuarioatv_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_usuarioatv_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_Tipo_Z" )]
      [  XmlElement( ElementName = "ContratoGestor_Tipo_Z"   )]
      public short gxTpr_Contratogestor_tipo_Z
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_tipo_Z ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_tipo_Z = (short)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_tipo_Z_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_tipo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_tipo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaCod_N" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaCod_N"   )]
      public short gxTpr_Contratogestor_contratadacod_N
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadacod_N ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadacod_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadacod_N_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaSigla_N" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaSigla_N"   )]
      public short gxTpr_Contratogestor_contratadasigla_N
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadasigla_N ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadasigla_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadasigla_N_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadasigla_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadasigla_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaTipo_N" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaTipo_N"   )]
      public short gxTpr_Contratogestor_contratadatipo_N
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadatipo_N ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadatipo_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadatipo_N_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadatipo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadatipo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaAreaCod_N" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaAreaCod_N"   )]
      public short gxTpr_Contratogestor_contratadaareacod_N
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_N ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_N_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_ContratadaAreaDes_N" )]
      [  XmlElement( ElementName = "ContratoGestor_ContratadaAreaDes_N"   )]
      public short gxTpr_Contratogestor_contratadaareades_N
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_contratadaareades_N ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_contratadaareades_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_contratadaareades_N_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadaareades_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_contratadaareades_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioPesCod_N" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioPesCod_N"   )]
      public short gxTpr_Contratogestor_usuariopescod_N
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuariopescod_N ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_usuariopescod_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_usuariopescod_N_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_usuariopescod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_usuariopescod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioPesNom_N" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioPesNom_N"   )]
      public short gxTpr_Contratogestor_usuariopesnom_N
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_N ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_N_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_UsuarioAtv_N" )]
      [  XmlElement( ElementName = "ContratoGestor_UsuarioAtv_N"   )]
      public short gxTpr_Contratogestor_usuarioatv_N
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_usuarioatv_N ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_usuarioatv_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_usuarioatv_N_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_usuarioatv_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_usuarioatv_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoGestor_Tipo_N" )]
      [  XmlElement( ElementName = "ContratoGestor_Tipo_N"   )]
      public short gxTpr_Contratogestor_tipo_N
      {
         get {
            return gxTv_SdtContratoGestor_Contratogestor_tipo_N ;
         }

         set {
            gxTv_SdtContratoGestor_Contratogestor_tipo_N = (short)(value);
         }

      }

      public void gxTv_SdtContratoGestor_Contratogestor_tipo_N_SetNull( )
      {
         gxTv_SdtContratoGestor_Contratogestor_tipo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratoGestor_Contratogestor_tipo_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratoGestor_Contratogestor_contratadasigla = "";
         gxTv_SdtContratoGestor_Contratogestor_contratadatipo = "S";
         gxTv_SdtContratoGestor_Contratogestor_contratadaareades = "";
         gxTv_SdtContratoGestor_Contratogestor_usuariopesnom = "";
         gxTv_SdtContratoGestor_Mode = "";
         gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z = "";
         gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z = "";
         gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z = "";
         gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z = "";
         gxTv_SdtContratoGestor_Contratogestor_usuarioatv = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratogestor", "GeneXus.Programs.contratogestor_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratoGestor_Contratogestor_tipo ;
      private short gxTv_SdtContratoGestor_Initialized ;
      private short gxTv_SdtContratoGestor_Contratogestor_tipo_Z ;
      private short gxTv_SdtContratoGestor_Contratogestor_contratadacod_N ;
      private short gxTv_SdtContratoGestor_Contratogestor_contratadasigla_N ;
      private short gxTv_SdtContratoGestor_Contratogestor_contratadatipo_N ;
      private short gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_N ;
      private short gxTv_SdtContratoGestor_Contratogestor_contratadaareades_N ;
      private short gxTv_SdtContratoGestor_Contratogestor_usuariopescod_N ;
      private short gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_N ;
      private short gxTv_SdtContratoGestor_Contratogestor_usuarioatv_N ;
      private short gxTv_SdtContratoGestor_Contratogestor_tipo_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratoGestor_Contratogestor_contratocod ;
      private int gxTv_SdtContratoGestor_Contratogestor_contratadacod ;
      private int gxTv_SdtContratoGestor_Contratogestor_contratadaareacod ;
      private int gxTv_SdtContratoGestor_Contratogestor_usuariocod ;
      private int gxTv_SdtContratoGestor_Contratogestor_usuariopescod ;
      private int gxTv_SdtContratoGestor_Contratogestor_contratocod_Z ;
      private int gxTv_SdtContratoGestor_Contratogestor_contratadacod_Z ;
      private int gxTv_SdtContratoGestor_Contratogestor_contratadaareacod_Z ;
      private int gxTv_SdtContratoGestor_Contratogestor_usuariocod_Z ;
      private int gxTv_SdtContratoGestor_Contratogestor_usuariopescod_Z ;
      private String gxTv_SdtContratoGestor_Contratogestor_contratadasigla ;
      private String gxTv_SdtContratoGestor_Contratogestor_contratadatipo ;
      private String gxTv_SdtContratoGestor_Contratogestor_usuariopesnom ;
      private String gxTv_SdtContratoGestor_Mode ;
      private String gxTv_SdtContratoGestor_Contratogestor_contratadasigla_Z ;
      private String gxTv_SdtContratoGestor_Contratogestor_contratadatipo_Z ;
      private String gxTv_SdtContratoGestor_Contratogestor_usuariopesnom_Z ;
      private String sTagName ;
      private bool gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante ;
      private bool gxTv_SdtContratoGestor_Contratogestor_usuarioatv ;
      private bool gxTv_SdtContratoGestor_Contratogestor_usuarioehcontratante_Z ;
      private bool gxTv_SdtContratoGestor_Contratogestor_usuarioatv_Z ;
      private String gxTv_SdtContratoGestor_Contratogestor_contratadaareades ;
      private String gxTv_SdtContratoGestor_Contratogestor_contratadaareades_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContratoGestor", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtContratoGestor_RESTInterface : GxGenericCollectionItem<SdtContratoGestor>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoGestor_RESTInterface( ) : base()
      {
      }

      public SdtContratoGestor_RESTInterface( SdtContratoGestor psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratoGestor_ContratoCod" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratogestor_contratocod
      {
         get {
            return sdt.gxTpr_Contratogestor_contratocod ;
         }

         set {
            sdt.gxTpr_Contratogestor_contratocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoGestor_ContratadaCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratogestor_contratadacod
      {
         get {
            return sdt.gxTpr_Contratogestor_contratadacod ;
         }

         set {
            sdt.gxTpr_Contratogestor_contratadacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoGestor_ContratadaSigla" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contratogestor_contratadasigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratogestor_contratadasigla) ;
         }

         set {
            sdt.gxTpr_Contratogestor_contratadasigla = (String)(value);
         }

      }

      [DataMember( Name = "ContratoGestor_ContratadaTipo" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Contratogestor_contratadatipo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratogestor_contratadatipo) ;
         }

         set {
            sdt.gxTpr_Contratogestor_contratadatipo = (String)(value);
         }

      }

      [DataMember( Name = "ContratoGestor_ContratadaAreaCod" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratogestor_contratadaareacod
      {
         get {
            return sdt.gxTpr_Contratogestor_contratadaareacod ;
         }

         set {
            sdt.gxTpr_Contratogestor_contratadaareacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoGestor_ContratadaAreaDes" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Contratogestor_contratadaareades
      {
         get {
            return sdt.gxTpr_Contratogestor_contratadaareades ;
         }

         set {
            sdt.gxTpr_Contratogestor_contratadaareades = (String)(value);
         }

      }

      [DataMember( Name = "ContratoGestor_UsuarioCod" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratogestor_usuariocod
      {
         get {
            return sdt.gxTpr_Contratogestor_usuariocod ;
         }

         set {
            sdt.gxTpr_Contratogestor_usuariocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoGestor_UsuarioPesCod" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratogestor_usuariopescod
      {
         get {
            return sdt.gxTpr_Contratogestor_usuariopescod ;
         }

         set {
            sdt.gxTpr_Contratogestor_usuariopescod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoGestor_UsuarioPesNom" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Contratogestor_usuariopesnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratogestor_usuariopesnom) ;
         }

         set {
            sdt.gxTpr_Contratogestor_usuariopesnom = (String)(value);
         }

      }

      [DataMember( Name = "ContratoGestor_UsuarioEhContratante" , Order = 9 )]
      [GxSeudo()]
      public bool gxTpr_Contratogestor_usuarioehcontratante
      {
         get {
            return sdt.gxTpr_Contratogestor_usuarioehcontratante ;
         }

         set {
            sdt.gxTpr_Contratogestor_usuarioehcontratante = value;
         }

      }

      [DataMember( Name = "ContratoGestor_UsuarioAtv" , Order = 10 )]
      [GxSeudo()]
      public bool gxTpr_Contratogestor_usuarioatv
      {
         get {
            return sdt.gxTpr_Contratogestor_usuarioatv ;
         }

         set {
            sdt.gxTpr_Contratogestor_usuarioatv = value;
         }

      }

      [DataMember( Name = "ContratoGestor_Tipo" , Order = 11 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratogestor_tipo
      {
         get {
            return sdt.gxTpr_Contratogestor_tipo ;
         }

         set {
            sdt.gxTpr_Contratogestor_tipo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtContratoGestor sdt
      {
         get {
            return (SdtContratoGestor)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratoGestor() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 35 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
