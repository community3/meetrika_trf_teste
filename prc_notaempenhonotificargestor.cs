/*
               File: PRC_NotaEmpenhoNotificarGestor
        Description: PRC_Nota Empenho Notificar Gestor
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:39.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_notaempenhonotificargestor : GXProcedure
   {
      public prc_notaempenhonotificargestor( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_notaempenhonotificargestor( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_NotaEmpenho_Codigo )
      {
         this.AV23NotaEmpenho_Codigo = aP0_NotaEmpenho_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_NotaEmpenho_Codigo )
      {
         prc_notaempenhonotificargestor objprc_notaempenhonotificargestor;
         objprc_notaempenhonotificargestor = new prc_notaempenhonotificargestor();
         objprc_notaempenhonotificargestor.AV23NotaEmpenho_Codigo = aP0_NotaEmpenho_Codigo;
         objprc_notaempenhonotificargestor.context.SetSubmitInitialConfig(context);
         objprc_notaempenhonotificargestor.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_notaempenhonotificargestor);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_notaempenhonotificargestor)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV18WWPContext) ;
         /* Using cursor P00BN2 */
         pr_default.execute(0, new Object[] {AV23NotaEmpenho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1561SaldoContrato_Codigo = P00BN2_A1561SaldoContrato_Codigo[0];
            A1560NotaEmpenho_Codigo = P00BN2_A1560NotaEmpenho_Codigo[0];
            A1564NotaEmpenho_Itentificador = P00BN2_A1564NotaEmpenho_Itentificador[0];
            n1564NotaEmpenho_Itentificador = P00BN2_n1564NotaEmpenho_Itentificador[0];
            A1566NotaEmpenho_Valor = P00BN2_A1566NotaEmpenho_Valor[0];
            n1566NotaEmpenho_Valor = P00BN2_n1566NotaEmpenho_Valor[0];
            A74Contrato_Codigo = P00BN2_A74Contrato_Codigo[0];
            A74Contrato_Codigo = P00BN2_A74Contrato_Codigo[0];
            AV14NotaEmpenho_Itentificador = A1564NotaEmpenho_Itentificador;
            AV15NotaEmpenho_Valor = A1566NotaEmpenho_Valor;
            AV8Contrato_Codigo = A74Contrato_Codigo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P00BN3 */
         pr_default.execute(1, new Object[] {AV8Contrato_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A74Contrato_Codigo = P00BN3_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00BN3_A77Contrato_Numero[0];
            AV13Contrato_Numero = A77Contrato_Numero;
            /* Using cursor P00BN4 */
            pr_default.execute(2, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1078ContratoGestor_ContratoCod = P00BN4_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = P00BN4_A1079ContratoGestor_UsuarioCod[0];
               AV16Usuario_Codigo = A1079ContratoGestor_UsuarioCod;
               /* Execute user subroutine: 'USUARIO' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  this.cleanup();
                  if (true) return;
               }
               pr_default.readNext(2);
            }
            pr_default.close(2);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         /* Using cursor P00BN6 */
         pr_default.execute(3, new Object[] {AV18WWPContext.gxTpr_Userid});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A40000Usuario_Nome = P00BN6_A40000Usuario_Nome[0];
         }
         else
         {
            A40000Usuario_Nome = "";
         }
         pr_default.close(3);
         AV17DataRegistro = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV24USuario_Nome = A40000Usuario_Nome;
         AV19EmailText = StringUtil.NewLine( ) + "Prezado(a)," + StringUtil.NewLine( );
         AV19EmailText = AV19EmailText + StringUtil.Format( "O contrato %1, foi alterado, conforme detalhe abaixo:", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Contrato_Codigo), 6, 0)), "", "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV19EmailText = AV19EmailText + StringUtil.Format( "N�mero da Nota de Empenho: %1", StringUtil.Trim( AV14NotaEmpenho_Itentificador), "", "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV19EmailText = AV19EmailText + StringUtil.Format( "Valor: %1", StringUtil.Trim( context.localUtil.Format( AV15NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV19EmailText = AV19EmailText + StringUtil.Format( "Alterado ou Registrado por: %1 - %2", StringUtil.Trim( StringUtil.Str( (decimal)(AV18WWPContext.gxTpr_Userid), 4, 0)), StringUtil.Trim( AV24USuario_Nome), "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV19EmailText = AV19EmailText + StringUtil.Format( "Data de Registro: %1", StringUtil.Trim( context.localUtil.TToC( AV17DataRegistro, 8, 5, 0, 3, "/", ":", " ")), "", "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV12AreaTrabalho_Codigo = AV18WWPContext.gxTpr_Areatrabalho_codigo;
         AV19EmailText = AV19EmailText + StringUtil.NewLine( ) + StringUtil.Trim( AV18WWPContext.gxTpr_Areatrabalho_descricao) + ", usu�rio " + StringUtil.Trim( AV18WWPContext.gxTpr_Username) + " - " + AV24USuario_Nome + StringUtil.NewLine( );
         AV19EmailText = AV19EmailText + StringUtil.NewLine( ) + StringUtil.NewLine( ) + "Esta mensagem pode conter informa��o confidencial e/ou privilegiada. Se voc� n�o for o destinat�rio ou a pessoa autorizada a receber esta mensagem, n�o pode usar, copiar ou divulgar as informa��es nela contidas ou tomar qualquer a��o baseada nessas informa��es. Se voc� recebeu esta mensagem por engano, por favor avise imediatamente o remetente, respondendo o e-mail e em seguida apague-o.";
         AV20Subject = StringUtil.Format( "Contrato %1 - %2 - Contrato alterado (No reply)", StringUtil.Trim( StringUtil.Str( (decimal)(AV8Contrato_Codigo), 6, 0)), StringUtil.Trim( AV13Contrato_Numero), "", "", "", "", "", "", "");
         AV10WebSession.Set("SendTo", AV11Usuarios.ToXml(false, true, "Collection", ""));
         new prc_enviaremailsemnotificacao(context ).execute(  AV12AreaTrabalho_Codigo,  AV20Subject,  AV19EmailText,  AV21Attachments, ref  AV22Resultado) ;
         AV10WebSession.Remove("SendTo");
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'USUARIO' Routine */
         /* Using cursor P00BN7 */
         pr_default.execute(4, new Object[] {AV16Usuario_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A1Usuario_Codigo = P00BN7_A1Usuario_Codigo[0];
            A341Usuario_UserGamGuid = P00BN7_A341Usuario_UserGamGuid[0];
            AV9GamUser.load( A341Usuario_UserGamGuid);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9GamUser.gxTpr_Email)) )
            {
               AV11Usuarios.Add(A1Usuario_Codigo, 0);
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         P00BN2_A1561SaldoContrato_Codigo = new int[1] ;
         P00BN2_A1560NotaEmpenho_Codigo = new int[1] ;
         P00BN2_A1564NotaEmpenho_Itentificador = new String[] {""} ;
         P00BN2_n1564NotaEmpenho_Itentificador = new bool[] {false} ;
         P00BN2_A1566NotaEmpenho_Valor = new decimal[1] ;
         P00BN2_n1566NotaEmpenho_Valor = new bool[] {false} ;
         P00BN2_A74Contrato_Codigo = new int[1] ;
         A1564NotaEmpenho_Itentificador = "";
         AV14NotaEmpenho_Itentificador = "";
         P00BN3_A74Contrato_Codigo = new int[1] ;
         P00BN3_A77Contrato_Numero = new String[] {""} ;
         A77Contrato_Numero = "";
         AV13Contrato_Numero = "";
         P00BN4_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00BN4_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00BN6_A40000Usuario_Nome = new String[] {""} ;
         A40000Usuario_Nome = "";
         AV17DataRegistro = (DateTime)(DateTime.MinValue);
         AV24USuario_Nome = "";
         AV19EmailText = "";
         AV20Subject = "";
         AV10WebSession = context.GetSession();
         AV11Usuarios = new GxSimpleCollection();
         AV21Attachments = new GxSimpleCollection();
         AV22Resultado = "";
         P00BN7_A1Usuario_Codigo = new int[1] ;
         P00BN7_A341Usuario_UserGamGuid = new String[] {""} ;
         A341Usuario_UserGamGuid = "";
         AV9GamUser = new SdtGAMUser(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_notaempenhonotificargestor__default(),
            new Object[][] {
                new Object[] {
               P00BN2_A1561SaldoContrato_Codigo, P00BN2_A1560NotaEmpenho_Codigo, P00BN2_A1564NotaEmpenho_Itentificador, P00BN2_n1564NotaEmpenho_Itentificador, P00BN2_A1566NotaEmpenho_Valor, P00BN2_n1566NotaEmpenho_Valor, P00BN2_A74Contrato_Codigo
               }
               , new Object[] {
               P00BN3_A74Contrato_Codigo, P00BN3_A77Contrato_Numero
               }
               , new Object[] {
               P00BN4_A1078ContratoGestor_ContratoCod, P00BN4_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               P00BN6_A40000Usuario_Nome
               }
               , new Object[] {
               P00BN7_A1Usuario_Codigo, P00BN7_A341Usuario_UserGamGuid
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV23NotaEmpenho_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private int A1560NotaEmpenho_Codigo ;
      private int A74Contrato_Codigo ;
      private int AV8Contrato_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int AV16Usuario_Codigo ;
      private int AV12AreaTrabalho_Codigo ;
      private int A1Usuario_Codigo ;
      private decimal A1566NotaEmpenho_Valor ;
      private decimal AV15NotaEmpenho_Valor ;
      private String scmdbuf ;
      private String A1564NotaEmpenho_Itentificador ;
      private String AV14NotaEmpenho_Itentificador ;
      private String A77Contrato_Numero ;
      private String AV13Contrato_Numero ;
      private String A40000Usuario_Nome ;
      private String AV24USuario_Nome ;
      private String AV22Resultado ;
      private String A341Usuario_UserGamGuid ;
      private DateTime AV17DataRegistro ;
      private bool n1564NotaEmpenho_Itentificador ;
      private bool n1566NotaEmpenho_Valor ;
      private bool returnInSub ;
      private String AV19EmailText ;
      private String AV20Subject ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV11Usuarios ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00BN2_A1561SaldoContrato_Codigo ;
      private int[] P00BN2_A1560NotaEmpenho_Codigo ;
      private String[] P00BN2_A1564NotaEmpenho_Itentificador ;
      private bool[] P00BN2_n1564NotaEmpenho_Itentificador ;
      private decimal[] P00BN2_A1566NotaEmpenho_Valor ;
      private bool[] P00BN2_n1566NotaEmpenho_Valor ;
      private int[] P00BN2_A74Contrato_Codigo ;
      private int[] P00BN3_A74Contrato_Codigo ;
      private String[] P00BN3_A77Contrato_Numero ;
      private int[] P00BN4_A1078ContratoGestor_ContratoCod ;
      private int[] P00BN4_A1079ContratoGestor_UsuarioCod ;
      private String[] P00BN6_A40000Usuario_Nome ;
      private int[] P00BN7_A1Usuario_Codigo ;
      private String[] P00BN7_A341Usuario_UserGamGuid ;
      private IGxSession AV10WebSession ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Attachments ;
      private SdtGAMUser AV9GamUser ;
      private wwpbaseobjects.SdtWWPContext AV18WWPContext ;
   }

   public class prc_notaempenhonotificargestor__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BN2 ;
          prmP00BN2 = new Object[] {
          new Object[] {"@AV23NotaEmpenho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BN3 ;
          prmP00BN3 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BN4 ;
          prmP00BN4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BN6 ;
          prmP00BN6 = new Object[] {
          new Object[] {"@AV18WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00BN7 ;
          prmP00BN7 = new Object[] {
          new Object[] {"@AV16Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BN2", "SELECT T1.[SaldoContrato_Codigo], T1.[NotaEmpenho_Codigo], T1.[NotaEmpenho_Itentificador], T1.[NotaEmpenho_Valor], T2.[Contrato_Codigo] FROM ([NotaEmpenho] T1 WITH (NOLOCK) INNER JOIN [SaldoContrato] T2 WITH (NOLOCK) ON T2.[SaldoContrato_Codigo] = T1.[SaldoContrato_Codigo]) WHERE T1.[NotaEmpenho_Codigo] = @AV23NotaEmpenho_Codigo ORDER BY T1.[NotaEmpenho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BN2,1,0,false,true )
             ,new CursorDef("P00BN3", "SELECT [Contrato_Codigo], [Contrato_Numero] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV8Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BN3,1,0,true,true )
             ,new CursorDef("P00BN4", "SELECT [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY [ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BN4,100,0,true,false )
             ,new CursorDef("P00BN6", "SELECT COALESCE( T1.[Usuario_Nome], '') AS Usuario_Nome FROM (SELECT [Usuario_Nome] AS Usuario_Nome, [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV18WWPContext__Userid ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BN6,1,0,true,true )
             ,new CursorDef("P00BN7", "SELECT [Usuario_Codigo], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV16Usuario_Codigo ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BN7,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
