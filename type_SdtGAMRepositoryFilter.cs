/*
               File: type_SdtGAMRepositoryFilter
        Description: GAMRepositoryFilter
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:52.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMRepositoryFilter : GxUserType, IGxExternalObject
   {
      public SdtGAMRepositoryFilter( )
      {
         initialize();
      }

      public SdtGAMRepositoryFilter( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMRepositoryFilter_externalReference == null )
         {
            GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
         }
         returntostring = "";
         returntostring = (String)(GAMRepositoryFilter_externalReference.ToString());
         return returntostring ;
      }

      public int gxTpr_Id
      {
         get {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            return GAMRepositoryFilter_externalReference.Id ;
         }

         set {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            GAMRepositoryFilter_externalReference.Id = value;
         }

      }

      public String gxTpr_Guid
      {
         get {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            return GAMRepositoryFilter_externalReference.GUID ;
         }

         set {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            GAMRepositoryFilter_externalReference.GUID = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            return GAMRepositoryFilter_externalReference.Name ;
         }

         set {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            GAMRepositoryFilter_externalReference.Name = value;
         }

      }

      public String gxTpr_Namespace
      {
         get {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            return GAMRepositoryFilter_externalReference.NameSpace ;
         }

         set {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            GAMRepositoryFilter_externalReference.NameSpace = value;
         }

      }

      public int gxTpr_Start
      {
         get {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            return GAMRepositoryFilter_externalReference.Start ;
         }

         set {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            GAMRepositoryFilter_externalReference.Start = value;
         }

      }

      public int gxTpr_Limit
      {
         get {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            return GAMRepositoryFilter_externalReference.Limit ;
         }

         set {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            GAMRepositoryFilter_externalReference.Limit = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMRepositoryFilter_externalReference == null )
            {
               GAMRepositoryFilter_externalReference = new Artech.Security.GAMRepositoryFilter(context);
            }
            return GAMRepositoryFilter_externalReference ;
         }

         set {
            GAMRepositoryFilter_externalReference = (Artech.Security.GAMRepositoryFilter)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMRepositoryFilter GAMRepositoryFilter_externalReference=null ;
   }

}
