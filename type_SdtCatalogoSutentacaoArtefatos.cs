/*
               File: type_SdtCatalogoSutentacaoArtefatos
        Description: Catalogo Sutentacao Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:59.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "CatalogoSutentacaoArtefatos" )]
   [XmlType(TypeName =  "CatalogoSutentacaoArtefatos" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtCatalogoSutentacaoArtefatos : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtCatalogoSutentacaoArtefatos( )
      {
         /* Constructor for serialization */
         gxTv_SdtCatalogoSutentacaoArtefatos_Mode = "";
      }

      public SdtCatalogoSutentacaoArtefatos( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1750CatalogoSutentacao_Codigo ,
                        int AV1749Artefatos_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1750CatalogoSutentacao_Codigo,(int)AV1749Artefatos_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"CatalogoSutentacao_Codigo", typeof(int)}, new Object[]{"Artefatos_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "CatalogoSutentacaoArtefatos");
         metadata.Set("BT", "CatalogoSutentacaoArtefatos");
         metadata.Set("PK", "[ \"CatalogoSutentacao_Codigo\",\"Artefatos_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Artefatos_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"CatalogoSutentacao_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Catalogosutentacao_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Artefatos_codigo_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtCatalogoSutentacaoArtefatos deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtCatalogoSutentacaoArtefatos)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtCatalogoSutentacaoArtefatos obj ;
         obj = this;
         obj.gxTpr_Catalogosutentacao_codigo = deserialized.gxTpr_Catalogosutentacao_codigo;
         obj.gxTpr_Artefatos_codigo = deserialized.gxTpr_Artefatos_codigo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Catalogosutentacao_codigo_Z = deserialized.gxTpr_Catalogosutentacao_codigo_Z;
         obj.gxTpr_Artefatos_codigo_Z = deserialized.gxTpr_Artefatos_codigo_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "CatalogoSutentacao_Codigo") )
               {
                  gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Codigo") )
               {
                  gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtCatalogoSutentacaoArtefatos_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtCatalogoSutentacaoArtefatos_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "CatalogoSutentacao_Codigo_Z") )
               {
                  gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Codigo_Z") )
               {
                  gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "CatalogoSutentacaoArtefatos";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("CatalogoSutentacao_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Artefatos_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtCatalogoSutentacaoArtefatos_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtCatalogoSutentacaoArtefatos_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("CatalogoSutentacao_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Artefatos_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("CatalogoSutentacao_Codigo", gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo, false);
         AddObjectProperty("Artefatos_Codigo", gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtCatalogoSutentacaoArtefatos_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtCatalogoSutentacaoArtefatos_Initialized, false);
            AddObjectProperty("CatalogoSutentacao_Codigo_Z", gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo_Z, false);
            AddObjectProperty("Artefatos_Codigo_Z", gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "CatalogoSutentacao_Codigo" )]
      [  XmlElement( ElementName = "CatalogoSutentacao_Codigo"   )]
      public int gxTpr_Catalogosutentacao_codigo
      {
         get {
            return gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo ;
         }

         set {
            if ( gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo != value )
            {
               gxTv_SdtCatalogoSutentacaoArtefatos_Mode = "INS";
               this.gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo_Z_SetNull( );
               this.gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo_Z_SetNull( );
            }
            gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Artefatos_Codigo" )]
      [  XmlElement( ElementName = "Artefatos_Codigo"   )]
      public int gxTpr_Artefatos_codigo
      {
         get {
            return gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo ;
         }

         set {
            if ( gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo != value )
            {
               gxTv_SdtCatalogoSutentacaoArtefatos_Mode = "INS";
               this.gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo_Z_SetNull( );
               this.gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo_Z_SetNull( );
            }
            gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtCatalogoSutentacaoArtefatos_Mode ;
         }

         set {
            gxTv_SdtCatalogoSutentacaoArtefatos_Mode = (String)(value);
         }

      }

      public void gxTv_SdtCatalogoSutentacaoArtefatos_Mode_SetNull( )
      {
         gxTv_SdtCatalogoSutentacaoArtefatos_Mode = "";
         return  ;
      }

      public bool gxTv_SdtCatalogoSutentacaoArtefatos_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtCatalogoSutentacaoArtefatos_Initialized ;
         }

         set {
            gxTv_SdtCatalogoSutentacaoArtefatos_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtCatalogoSutentacaoArtefatos_Initialized_SetNull( )
      {
         gxTv_SdtCatalogoSutentacaoArtefatos_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtCatalogoSutentacaoArtefatos_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CatalogoSutentacao_Codigo_Z" )]
      [  XmlElement( ElementName = "CatalogoSutentacao_Codigo_Z"   )]
      public int gxTpr_Catalogosutentacao_codigo_Z
      {
         get {
            return gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo_Z ;
         }

         set {
            gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo_Z_SetNull( )
      {
         gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Artefatos_Codigo_Z" )]
      [  XmlElement( ElementName = "Artefatos_Codigo_Z"   )]
      public int gxTpr_Artefatos_codigo_Z
      {
         get {
            return gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo_Z ;
         }

         set {
            gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo_Z_SetNull( )
      {
         gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtCatalogoSutentacaoArtefatos_Mode = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "catalogosutentacaoartefatos", "GeneXus.Programs.catalogosutentacaoartefatos_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtCatalogoSutentacaoArtefatos_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo ;
      private int gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo ;
      private int gxTv_SdtCatalogoSutentacaoArtefatos_Catalogosutentacao_codigo_Z ;
      private int gxTv_SdtCatalogoSutentacaoArtefatos_Artefatos_codigo_Z ;
      private String gxTv_SdtCatalogoSutentacaoArtefatos_Mode ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"CatalogoSutentacaoArtefatos", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtCatalogoSutentacaoArtefatos_RESTInterface : GxGenericCollectionItem<SdtCatalogoSutentacaoArtefatos>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtCatalogoSutentacaoArtefatos_RESTInterface( ) : base()
      {
      }

      public SdtCatalogoSutentacaoArtefatos_RESTInterface( SdtCatalogoSutentacaoArtefatos psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "CatalogoSutentacao_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Catalogosutentacao_codigo
      {
         get {
            return sdt.gxTpr_Catalogosutentacao_codigo ;
         }

         set {
            sdt.gxTpr_Catalogosutentacao_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Artefatos_Codigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Artefatos_codigo
      {
         get {
            return sdt.gxTpr_Artefatos_codigo ;
         }

         set {
            sdt.gxTpr_Artefatos_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtCatalogoSutentacaoArtefatos sdt
      {
         get {
            return (SdtCatalogoSutentacaoArtefatos)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtCatalogoSutentacaoArtefatos() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 6 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
