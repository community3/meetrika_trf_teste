/*
               File: WC_PercentualStatus
        Description: Percentual Status
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:11:19.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_percentualstatus : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_percentualstatus( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_percentualstatus( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_2_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_2_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  A484ContagemResultado_StatusDmn = GetNextPar( );
                  n484ContagemResultado_StatusDmn = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
                  A531ContagemResultado_StatusUltCnt = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  n531ContagemResultado_StatusUltCnt = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A531ContagemResultado_StatusUltCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV5Codigos);
                  AV14TotalDmn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14TotalDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14TotalDmn), 4, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( A456ContagemResultado_Codigo, A484ContagemResultado_StatusDmn, A531ContagemResultado_StatusUltCnt, AV5Codigos, AV14TotalDmn, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAHE2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavStatus_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavStatus_Enabled), 5, 0)));
               edtavQtde_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavQtde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtde_Enabled), 5, 0)));
               edtavCpercentual_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCpercentual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCpercentual_Enabled), 5, 0)));
               WSHE2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Percentual Status") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216111945");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_percentualstatus.aspx") +"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_2", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_STATUSULTCNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCODIGOS", AV5Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCODIGOS", AV5Codigos);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vTOTALDMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14TotalDmn), 4, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormHE2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_percentualstatus.js", "?20206216111947");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_PercentualStatus" ;
      }

      public override String GetPgmdesc( )
      {
         return "Percentual Status" ;
      }

      protected void WBHE0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_percentualstatus.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"2\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Status") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtde") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Percentual") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorodd", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorodd), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcoloreven", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcoloreven), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV15Status));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavStatus_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Qtde), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQtde_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV17cPercentual));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCpercentual_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 2 )
         {
            wbEnd = 0;
            nRC_GXsfl_2 = (short)(nGXsfl_2_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
         }
         wbLoad = true;
      }

      protected void STARTHE2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Percentual Status", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPHE0( ) ;
            }
         }
      }

      protected void WSHE2( )
      {
         STARTHE2( ) ;
         EVTHE2( ) ;
      }

      protected void EVTHE2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPHE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPHE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPHE0( ) ;
                              }
                              nGXsfl_2_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
                              SubsflControlProps_22( ) ;
                              AV15Status = cgiGet( edtavStatus_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavStatus_Internalname, AV15Status);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( AV15Status, ""))));
                              AV11Qtde = (short)(context.localUtil.CToN( cgiGet( edtavQtde_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Qtde), 4, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vQTDE"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( (decimal)(AV11Qtde), "ZZZ9")));
                              AV17cPercentual = cgiGet( edtavCpercentual_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavCpercentual_Internalname, AV17cPercentual);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCPERCENTUAL"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( AV17cPercentual, ""))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E11HE2 */
                                          E11HE2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E12HE2 */
                                          E12HE2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPHE0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEHE2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormHE2( ) ;
            }
         }
      }

      protected void PAHE2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_22( ) ;
         while ( nGXsfl_2_idx <= nRC_GXsfl_2 )
         {
            sendrow_22( ) ;
            nGXsfl_2_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_2_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_2_idx+1));
            sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
            SubsflControlProps_22( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int A456ContagemResultado_Codigo ,
                                       String A484ContagemResultado_StatusDmn ,
                                       short A531ContagemResultado_StatusUltCnt ,
                                       IGxCollection AV5Codigos ,
                                       short AV14TotalDmn ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFHE2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vSTATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV15Status, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSTATUS", StringUtil.RTrim( AV15Status));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vQTDE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(AV11Qtde), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vQTDE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Qtde), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCPERCENTUAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV17cPercentual, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCPERCENTUAL", StringUtil.RTrim( AV17cPercentual));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFHE2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavStatus_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavStatus_Enabled), 5, 0)));
         edtavQtde_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavQtde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtde_Enabled), 5, 0)));
         edtavCpercentual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCpercentual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCpercentual_Enabled), 5, 0)));
      }

      protected void RFHE2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 2;
         /* Execute user event: E11HE2 */
         E11HE2 ();
         nGXsfl_2_idx = 1;
         sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
         SubsflControlProps_22( ) ;
         nGXsfl_2_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorodd", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorodd), 9, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcoloreven", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcoloreven), 9, 0, ".", "")));
         GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_22( ) ;
            /* Execute user event: E12HE2 */
            E12HE2 ();
            wbEnd = 2;
            WBHE0( ) ;
         }
         nGXsfl_2_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPHE0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavStatus_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavStatus_Enabled), 5, 0)));
         edtavQtde_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavQtde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtde_Enabled), 5, 0)));
         edtavCpercentual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCpercentual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCpercentual_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_2 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_2"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void E11HE2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV5Codigos.FromXml(AV7WebSession.Get("Codigos"), "Collection");
         AV14TotalDmn = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14TotalDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14TotalDmn), 4, 0)));
         /* Optimized group. */
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV5Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor H00HE2 */
         pr_default.execute(0);
         cV14TotalDmn = H00HE2_AV14TotalDmn[0];
         pr_default.close(0);
         AV14TotalDmn = (short)(AV14TotalDmn+cV14TotalDmn*1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14TotalDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14TotalDmn), 4, 0)));
         /* End optimized group. */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV5Codigos", AV5Codigos);
      }

      private void E12HE2( )
      {
         /* Load Routine */
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV5Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor H00HE4 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKHE4 = false;
            A484ContagemResultado_StatusDmn = H00HE4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00HE4_n484ContagemResultado_StatusDmn[0];
            A456ContagemResultado_Codigo = H00HE4_A456ContagemResultado_Codigo[0];
            A531ContagemResultado_StatusUltCnt = H00HE4_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00HE4_n531ContagemResultado_StatusUltCnt[0];
            A531ContagemResultado_StatusUltCnt = H00HE4_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00HE4_n531ContagemResultado_StatusUltCnt[0];
            AV15Status = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn) + "/" + gxdomainstatuscontagem.getDescription(context,A531ContagemResultado_StatusUltCnt);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavStatus_Internalname, AV15Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( AV15Status, ""))));
            AV11Qtde = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Qtde), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vQTDE"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( (decimal)(AV11Qtde), "ZZZ9")));
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(H00HE4_A484ContagemResultado_StatusDmn[0], A484ContagemResultado_StatusDmn) == 0 ) && ( H00HE4_A531ContagemResultado_StatusUltCnt[0] == A531ContagemResultado_StatusUltCnt ) )
            {
               BRKHE4 = false;
               A456ContagemResultado_Codigo = H00HE4_A456ContagemResultado_Codigo[0];
               if ( (AV5Codigos.IndexOf(A456ContagemResultado_Codigo)>0) )
               {
                  AV11Qtde = (short)(AV11Qtde+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Qtde), 4, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vQTDE"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( (decimal)(AV11Qtde), "ZZZ9")));
               }
               BRKHE4 = true;
               pr_default.readNext(1);
            }
            AV16Percentual = (decimal)((AV11Qtde/ (decimal)(AV14TotalDmn))*100);
            AV17cPercentual = StringUtil.Trim( StringUtil.Str( AV16Percentual, 6, 2)) + "%";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavCpercentual_Internalname, AV17cPercentual);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCPERCENTUAL"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( AV17cPercentual, ""))));
            sendrow_22( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_2_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(2, GridRow);
            }
            if ( ! BRKHE4 )
            {
               BRKHE4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
         AV15Status = "T O T A I S";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavStatus_Internalname, AV15Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( AV15Status, ""))));
         AV11Qtde = AV14TotalDmn;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Qtde), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vQTDE"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( (decimal)(AV11Qtde), "ZZZ9")));
         AV17cPercentual = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavCpercentual_Internalname, AV17cPercentual);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCPERCENTUAL"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( AV17cPercentual, ""))));
         sendrow_22( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_2_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(2, GridRow);
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAHE2( ) ;
         WSHE2( ) ;
         WEHE2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAHE2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_percentualstatus");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAHE2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
         }
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAHE2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSHE2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSHE2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEHE2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216111971");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("wc_percentualstatus.js", "?20206216111971");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_22( )
      {
         edtavStatus_Internalname = sPrefix+"vSTATUS_"+sGXsfl_2_idx;
         edtavQtde_Internalname = sPrefix+"vQTDE_"+sGXsfl_2_idx;
         edtavCpercentual_Internalname = sPrefix+"vCPERCENTUAL_"+sGXsfl_2_idx;
      }

      protected void SubsflControlProps_fel_22( )
      {
         edtavStatus_Internalname = sPrefix+"vSTATUS_"+sGXsfl_2_fel_idx;
         edtavQtde_Internalname = sPrefix+"vQTDE_"+sGXsfl_2_fel_idx;
         edtavCpercentual_Internalname = sPrefix+"vCPERCENTUAL_"+sGXsfl_2_fel_idx;
      }

      protected void sendrow_22( )
      {
         SubsflControlProps_22( ) ;
         WBHE0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xF5F5F5);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_2_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xF5F5F5);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";")+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_2_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavStatus_Internalname,StringUtil.RTrim( AV15Status),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavStatus_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavStatus_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtde_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Qtde), 4, 0, ",", "")),((edtavQtde_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11Qtde), "ZZZ9")) : context.localUtil.Format( (decimal)(AV11Qtde), "ZZZ9")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavQtde_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavQtde_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCpercentual_Internalname,StringUtil.RTrim( AV17cPercentual),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCpercentual_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavCpercentual_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)7,(short)0,(short)0,(short)2,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"center",(bool)true});
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vSTATUS"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( AV15Status, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vQTDE"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, context.localUtil.Format( (decimal)(AV11Qtde), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCPERCENTUAL"+"_"+sGXsfl_2_idx, GetSecureSignedToken( sPrefix+sGXsfl_2_idx, StringUtil.RTrim( context.localUtil.Format( AV17cPercentual, ""))));
         GridContainer.AddRow(GridRow);
         nGXsfl_2_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_2_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_2_idx+1));
         sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
         SubsflControlProps_22( ) ;
         /* End function sendrow_22 */
      }

      protected void init_default_properties( )
      {
         edtavStatus_Internalname = sPrefix+"vSTATUS";
         edtavQtde_Internalname = sPrefix+"vQTDE";
         edtavCpercentual_Internalname = sPrefix+"vCPERCENTUAL";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavCpercentual_Jsonclick = "";
         edtavQtde_Jsonclick = "";
         edtavStatus_Jsonclick = "";
         subGrid_Backcolor = (int)(0x0);
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavCpercentual_Enabled = 0;
         edtavQtde_Enabled = 0;
         edtavStatus_Enabled = 0;
         subGrid_Titleforecolor = (int)(0x000000);
         subGrid_Backcoloreven = (int)(0xFFFFFF);
         subGrid_Backcolorodd = (int)(0xF5F5F5);
         subGrid_Class = "Grid";
         subGrid_Backcolorstyle = 3;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'AV5Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV14TotalDmn',fld:'vTOTALDMN',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV5Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV14TotalDmn',fld:'vTOTALDMN',pic:'ZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         A484ContagemResultado_StatusDmn = "";
         AV5Codigos = new GxSimpleCollection();
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         GridContainer = new GXWebGrid( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         AV15Status = "";
         AV17cPercentual = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV7WebSession = context.GetSession();
         scmdbuf = "";
         H00HE2_AV14TotalDmn = new short[1] ;
         H00HE4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00HE4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00HE4_A456ContagemResultado_Codigo = new int[1] ;
         H00HE4_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00HE4_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         GridRow = new GXWebRow();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_percentualstatus__default(),
            new Object[][] {
                new Object[] {
               H00HE2_AV14TotalDmn
               }
               , new Object[] {
               H00HE4_A484ContagemResultado_StatusDmn, H00HE4_n484ContagemResultado_StatusDmn, H00HE4_A456ContagemResultado_Codigo, H00HE4_A531ContagemResultado_StatusUltCnt, H00HE4_n531ContagemResultado_StatusUltCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavStatus_Enabled = 0;
         edtavQtde_Enabled = 0;
         edtavCpercentual_Enabled = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_2 ;
      private short nGXsfl_2_idx=1 ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short AV14TotalDmn ;
      private short initialized ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short AV11Qtde ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_2_Refreshing=0 ;
      private short cV14TotalDmn ;
      private short GRID_nEOF ;
      private short subGrid_Backstyle ;
      private int A456ContagemResultado_Codigo ;
      private int edtavStatus_Enabled ;
      private int edtavQtde_Enabled ;
      private int edtavCpercentual_Enabled ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Backcolorodd ;
      private int subGrid_Backcoloreven ;
      private int subGrid_Titleforecolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int subGrid_Islastpage ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private decimal AV16Percentual ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_2_idx="0001" ;
      private String A484ContagemResultado_StatusDmn ;
      private String GXKey ;
      private String edtavStatus_Internalname ;
      private String edtavQtde_Internalname ;
      private String edtavCpercentual_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sStyleString ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String AV15Status ;
      private String AV17cPercentual ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String sGXsfl_2_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavStatus_Jsonclick ;
      private String edtavQtde_Jsonclick ;
      private String edtavCpercentual_Jsonclick ;
      private bool entryPointCalled ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n531ContagemResultado_StatusUltCnt ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool gx_refresh_fired ;
      private bool BRKHE4 ;
      private IGxSession AV7WebSession ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] H00HE2_AV14TotalDmn ;
      private String[] H00HE4_A484ContagemResultado_StatusDmn ;
      private bool[] H00HE4_n484ContagemResultado_StatusDmn ;
      private int[] H00HE4_A456ContagemResultado_Codigo ;
      private short[] H00HE4_A531ContagemResultado_StatusUltCnt ;
      private bool[] H00HE4_n531ContagemResultado_StatusUltCnt ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV5Codigos ;
   }

   public class wc_percentualstatus__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00HE2( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV5Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV5Codigos, "[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      protected Object[] conditional_H00HE4( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV5Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Codigo], COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV5Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_StatusDmn], [ContagemResultado_StatusUltCnt]";
         GXv_Object3[0] = scmdbuf;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00HE2(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 1 :
                     return conditional_H00HE4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00HE2 ;
          prmH00HE2 = new Object[] {
          } ;
          Object[] prmH00HE4 ;
          prmH00HE4 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00HE2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HE2,1,0,true,false )
             ,new CursorDef("H00HE4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HE4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
       }
    }

 }

}
