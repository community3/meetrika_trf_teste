/*
               File: WP_Redmine144
        Description: Redmine1.4.4
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:47:33.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_redmine144 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_redmine144( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_redmine144( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavProjeto = new GXCombobox();
         cmbavStatus = new GXCombobox();
         cmbavUsuario = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_144 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_144_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_144_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( ) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid2") == 0 )
            {
               nRC_GXsfl_62 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_62_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_62_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid2_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid2") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid2_refresh( ) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAKE2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTKE2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823473331");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_redmine144.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_issues", AV11SDT_Issues);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_issues", AV11SDT_Issues);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_62", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_62), 4, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEKE2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTKE2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_redmine144.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_Redmine144" ;
      }

      public override String GetPgmdesc( )
      {
         return "Redmine1.4.4" ;
      }

      protected void WBKE0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "<p>") ;
            wb_table1_4_KE2( true) ;
         }
         else
         {
            wb_table1_4_KE2( false) ;
         }
         return  ;
      }

      protected void wb_table1_4_KE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</p>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUsuario, cmbavUsuario_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17Usuario), 4, 0)), 1, cmbavUsuario_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,158);\"", "", true, "HLP_WP_Redmine144.htm");
            cmbavUsuario.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Usuario), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_Internalname, "Values", (String)(cmbavUsuario.ToJavascriptSource()));
         }
         wbLoad = true;
      }

      protected void STARTKE2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Redmine1.4.4", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPKE0( ) ;
      }

      protected void WSKE2( )
      {
         STARTKE2( ) ;
         EVTKE2( ) ;
      }

      protected void EVTKE2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CARREGARFILTROS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11KE2 */
                              E11KE2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12KE2 */
                                    E12KE2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID2.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID1.LOAD") == 0 ) )
                           {
                              nGXsfl_62_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
                              SubsflControlProps_622( ) ;
                              AV26GXV4 = nGXsfl_62_idx;
                              if ( ( AV11SDT_Issues.gxTpr_Issues.Count >= AV26GXV4 ) && ( AV26GXV4 > 0 ) )
                              {
                                 AV11SDT_Issues.gxTpr_Issues.CurrentItem = ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4));
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid1_Internalname), ",", "."));
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Project.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid2_Internalname), ",", "."));
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Project.gxTpr_Name = cgiGet( edtavCtlname1_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Tracker.gxTpr_Name = cgiGet( edtavCtlname2_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Status.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid4_Internalname), ",", "."));
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Status.gxTpr_Name = cgiGet( edtavCtlname3_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Priority.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid5_Internalname), ",", "."));
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Priority.gxTpr_Name = cgiGet( edtavCtlname4_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Author.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid6_Internalname), ",", "."));
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Author.gxTpr_Name = cgiGet( edtavCtlname5_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Assigned_to.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid7_Internalname), ",", "."));
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Assigned_to.gxTpr_Name = cgiGet( edtavCtlname6_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Subject = cgiGet( edtavCtlsubject_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Description = cgiGet( edtavCtldescription_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Start_date = cgiGet( edtavCtlstart_date_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Due_date = cgiGet( edtavCtldue_date_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Done_ratio = (short)(context.localUtil.CToN( cgiGet( edtavCtldone_ratio_Internalname), ",", "."));
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Estimated_hours = cgiGet( edtavCtlestimated_hours_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Type = cgiGet( edtavCtltype_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Created_on = cgiGet( edtavCtlcreated_on_Internalname);
                                 ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Updated_on = cgiGet( edtavCtlupdated_on_Internalname);
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13KE2 */
                                    E13KE2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID2.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14KE2 */
                                    E14KE2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                                 sEvtType = StringUtil.Right( sEvt, 4);
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                                 if ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID1.LOAD") == 0 )
                                 {
                                    nGXsfl_144_idx = (short)(NumberUtil.Val( sEvtType, "."));
                                    sGXsfl_144_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_144_idx), 4, 0)), 4, "0") + sGXsfl_62_idx;
                                    SubsflControlProps_1445( ) ;
                                    AV46GXV24 = nGXsfl_144_idx;
                                    if ( ( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Count >= AV46GXV24 ) && ( AV46GXV24 > 0 ) )
                                    {
                                       ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.CurrentItem = ((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24));
                                       ((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24)).gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid_Internalname), ",", "."));
                                       ((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24)).gxTpr_Name = cgiGet( edtavCtlname_Internalname);
                                       ((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24)).gxTpr_Value = cgiGet( edtavCtlvalue_Internalname);
                                    }
                                    sEvtType = StringUtil.Right( sEvt, 1);
                                    if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                                    {
                                       sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                       if ( StringUtil.StrCmp(sEvt, "GRID1.LOAD") == 0 )
                                       {
                                          context.wbHandled = 1;
                                          dynload_actions( ) ;
                                          /* Execute user event: E15KE5 */
                                          E15KE5 ();
                                       }
                                       else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                       {
                                          context.wbHandled = 1;
                                          dynload_actions( ) ;
                                       }
                                    }
                                    else
                                    {
                                    }
                                 }
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKE2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAKE2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavProjeto.Name = "vPROJETO";
            cmbavProjeto.WebTags = "";
            cmbavProjeto.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Todos)", 0);
            if ( cmbavProjeto.ItemCount > 0 )
            {
               AV10Projeto = (short)(NumberUtil.Val( cmbavProjeto.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Projeto), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Projeto), 4, 0)));
            }
            cmbavStatus.Name = "vSTATUS";
            cmbavStatus.WebTags = "";
            cmbavStatus.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Todos", 0);
            if ( cmbavStatus.ItemCount > 0 )
            {
               AV15Status = (short)(NumberUtil.Val( cmbavStatus.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15Status), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Status), 4, 0)));
            }
            cmbavUsuario.Name = "vUSUARIO";
            cmbavUsuario.WebTags = "";
            cmbavUsuario.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Todos)", 0);
            if ( cmbavUsuario.ItemCount > 0 )
            {
               AV17Usuario = (short)(NumberUtil.Val( cmbavUsuario.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Usuario), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Usuario), 4, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavHost_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid2_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_622( ) ;
         while ( nGXsfl_62_idx <= nRC_GXsfl_62 )
         {
            sendrow_622( ) ;
            nGXsfl_62_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_62_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_62_idx+1));
            sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
            SubsflControlProps_622( ) ;
         }
         context.GX_webresponse.AddString(Grid2Container.ToJavascriptSource());
         /* End function gxnrGrid2_newrow */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1445( ) ;
         while ( nGXsfl_144_idx <= nRC_GXsfl_144 )
         {
            sendrow_1445( ) ;
            nGXsfl_144_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_144_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_144_idx+1));
            sGXsfl_144_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_144_idx), 4, 0)), 4, "0") + sGXsfl_62_idx;
            SubsflControlProps_1445( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RFKE5( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void gxgrGrid2_refresh( )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID2_nCurrentRecord = 0;
         RFKE2( ) ;
         /* End function gxgrGrid2_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavProjeto.ItemCount > 0 )
         {
            AV10Projeto = (short)(NumberUtil.Val( cmbavProjeto.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Projeto), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Projeto), 4, 0)));
         }
         if ( cmbavStatus.ItemCount > 0 )
         {
            AV15Status = (short)(NumberUtil.Val( cmbavStatus.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15Status), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Status), 4, 0)));
         }
         if ( cmbavUsuario.ItemCount > 0 )
         {
            AV17Usuario = (short)(NumberUtil.Val( cmbavUsuario.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Usuario), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Usuario), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKE2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtltotal_count_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtltotal_count_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtltotal_count_Enabled), 5, 0)));
         edtavCtloffset_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtloffset_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtloffset_Enabled), 5, 0)));
         edtavCtllimit_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtllimit_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtllimit_Enabled), 5, 0)));
         edtavCtlid1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlid1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlid1_Enabled), 5, 0)));
         edtavCtlid2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlid2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlid2_Enabled), 5, 0)));
         edtavCtlname1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname1_Enabled), 5, 0)));
         edtavCtlname2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname2_Enabled), 5, 0)));
         edtavCtlid4_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlid4_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlid4_Enabled), 5, 0)));
         edtavCtlname3_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname3_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname3_Enabled), 5, 0)));
         edtavCtlid5_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlid5_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlid5_Enabled), 5, 0)));
         edtavCtlname4_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname4_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname4_Enabled), 5, 0)));
         edtavCtlid6_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlid6_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlid6_Enabled), 5, 0)));
         edtavCtlname5_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname5_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname5_Enabled), 5, 0)));
         edtavCtlid7_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlid7_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlid7_Enabled), 5, 0)));
         edtavCtlname6_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname6_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname6_Enabled), 5, 0)));
         edtavCtlname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname_Enabled), 5, 0)));
         edtavCtlvalue_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlvalue_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlvalue_Enabled), 5, 0)));
         edtavCtlcreated_on_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcreated_on_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcreated_on_Enabled), 5, 0)));
         edtavCtlupdated_on_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlupdated_on_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlupdated_on_Enabled), 5, 0)));
      }

      protected void RFKE2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid2Container.ClearRows();
         }
         wbStart = 62;
         nGXsfl_62_idx = 1;
         sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
         SubsflControlProps_622( ) ;
         nGXsfl_62_Refreshing = 1;
         Grid2Container.AddObjectProperty("GridName", "Grid2");
         Grid2Container.AddObjectProperty("CmpContext", "");
         Grid2Container.AddObjectProperty("InMasterPage", "false");
         Grid2Container.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Rules", StringUtil.RTrim( "none"));
         Grid2Container.AddObjectProperty("Class", "FreeStyleGrid");
         Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Backcolorstyle), 1, 0, ".", "")));
         Grid2Container.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Borderwidth), 4, 0, ".", "")));
         Grid2Container.PageSize = subGrid2_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_622( ) ;
            /* Execute user event: E14KE2 */
            E14KE2 ();
            wbEnd = 62;
            WBKE0( ) ;
         }
         nGXsfl_62_Refreshing = 0;
      }

      protected void RFKE5( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 144;
         nGXsfl_144_idx = 1;
         sGXsfl_144_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_144_idx), 4, 0)), 4, "0") + sGXsfl_62_idx;
         SubsflControlProps_1445( ) ;
         nGXsfl_144_Refreshing = 1;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "Grid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1445( ) ;
            /* Execute user event: E15KE5 */
            E15KE5 ();
            wbEnd = 144;
            WBKE0( ) ;
         }
         nGXsfl_144_Refreshing = 0;
      }

      protected int subGrid2_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPKE0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtltotal_count_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtltotal_count_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtltotal_count_Enabled), 5, 0)));
         edtavCtloffset_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtloffset_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtloffset_Enabled), 5, 0)));
         edtavCtllimit_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtllimit_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtllimit_Enabled), 5, 0)));
         edtavCtlid1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlid1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlid1_Enabled), 5, 0)));
         edtavCtlid2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlid2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlid2_Enabled), 5, 0)));
         edtavCtlname1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname1_Enabled), 5, 0)));
         edtavCtlname2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname2_Enabled), 5, 0)));
         edtavCtlid4_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlid4_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlid4_Enabled), 5, 0)));
         edtavCtlname3_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname3_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname3_Enabled), 5, 0)));
         edtavCtlid5_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlid5_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlid5_Enabled), 5, 0)));
         edtavCtlname4_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname4_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname4_Enabled), 5, 0)));
         edtavCtlid6_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlid6_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlid6_Enabled), 5, 0)));
         edtavCtlname5_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname5_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname5_Enabled), 5, 0)));
         edtavCtlid7_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlid7_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlid7_Enabled), 5, 0)));
         edtavCtlname6_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname6_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname6_Enabled), 5, 0)));
         edtavCtlname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname_Enabled), 5, 0)));
         edtavCtlvalue_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlvalue_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlvalue_Enabled), 5, 0)));
         edtavCtlcreated_on_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcreated_on_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcreated_on_Enabled), 5, 0)));
         edtavCtlupdated_on_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlupdated_on_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlupdated_on_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13KE2 */
         E13KE2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_issues"), AV11SDT_Issues);
            /* Read variables values. */
            AV6Host = cgiGet( edtavHost_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Host", AV6Host);
            AV19Url = cgiGet( edtavUrl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Url", AV19Url);
            AV8Key = cgiGet( edtavKey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Key", AV8Key);
            cmbavProjeto.Name = cmbavProjeto_Internalname;
            cmbavProjeto.CurrentValue = cgiGet( cmbavProjeto_Internalname);
            AV10Projeto = (short)(NumberUtil.Val( cgiGet( cmbavProjeto_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Projeto), 4, 0)));
            cmbavStatus.Name = cmbavStatus_Internalname;
            cmbavStatus.CurrentValue = cgiGet( cmbavStatus_Internalname);
            AV15Status = (short)(NumberUtil.Val( cgiGet( cmbavStatus_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Status), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtltotal_count_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtltotal_count_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLTOTAL_COUNT");
               GX_FocusControl = edtavCtltotal_count_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11SDT_Issues.gxTpr_Total_count = 0;
            }
            else
            {
               AV11SDT_Issues.gxTpr_Total_count = (short)(context.localUtil.CToN( cgiGet( edtavCtltotal_count_Internalname), ",", "."));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtloffset_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtloffset_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLOFFSET");
               GX_FocusControl = edtavCtloffset_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11SDT_Issues.gxTpr_Offset = 0;
            }
            else
            {
               AV11SDT_Issues.gxTpr_Offset = (short)(context.localUtil.CToN( cgiGet( edtavCtloffset_Internalname), ",", "."));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtllimit_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtllimit_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLLIMIT");
               GX_FocusControl = edtavCtllimit_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11SDT_Issues.gxTpr_Limit = 0;
            }
            else
            {
               AV11SDT_Issues.gxTpr_Limit = (short)(context.localUtil.CToN( cgiGet( edtavCtllimit_Internalname), ",", "."));
            }
            cmbavUsuario.Name = cmbavUsuario_Internalname;
            cmbavUsuario.CurrentValue = cgiGet( cmbavUsuario_Internalname);
            AV17Usuario = (short)(NumberUtil.Val( cgiGet( cmbavUsuario_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Usuario), 4, 0)));
            /* Read saved values. */
            nRC_GXsfl_62 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_62"), ",", "."));
            nRC_GXsfl_62 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_62"), ",", "."));
            nGXsfl_62_fel_idx = 0;
            while ( nGXsfl_62_fel_idx < nRC_GXsfl_62 )
            {
               nGXsfl_62_fel_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_62_fel_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_62_fel_idx+1));
               sGXsfl_62_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_622( ) ;
               AV26GXV4 = nGXsfl_62_fel_idx;
               if ( ( AV11SDT_Issues.gxTpr_Issues.Count >= AV26GXV4 ) && ( AV26GXV4 > 0 ) )
               {
                  AV11SDT_Issues.gxTpr_Issues.CurrentItem = ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4));
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid1_Internalname), ",", "."));
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Project.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid2_Internalname), ",", "."));
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Project.gxTpr_Name = cgiGet( edtavCtlname1_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Tracker.gxTpr_Name = cgiGet( edtavCtlname2_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Status.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid4_Internalname), ",", "."));
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Status.gxTpr_Name = cgiGet( edtavCtlname3_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Priority.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid5_Internalname), ",", "."));
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Priority.gxTpr_Name = cgiGet( edtavCtlname4_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Author.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid6_Internalname), ",", "."));
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Author.gxTpr_Name = cgiGet( edtavCtlname5_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Assigned_to.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid7_Internalname), ",", "."));
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Assigned_to.gxTpr_Name = cgiGet( edtavCtlname6_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Subject = cgiGet( edtavCtlsubject_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Description = cgiGet( edtavCtldescription_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Start_date = cgiGet( edtavCtlstart_date_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Due_date = cgiGet( edtavCtldue_date_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Done_ratio = (short)(context.localUtil.CToN( cgiGet( edtavCtldone_ratio_Internalname), ",", "."));
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Estimated_hours = cgiGet( edtavCtlestimated_hours_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Type = cgiGet( edtavCtltype_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Created_on = cgiGet( edtavCtlcreated_on_Internalname);
                  ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Updated_on = cgiGet( edtavCtlupdated_on_Internalname);
               }
               GXCCtl = "nRC_GXsfl_144_" + sGXsfl_62_fel_idx;
               nRC_GXsfl_144 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
               nGXsfl_144_fel_idx = 0;
               while ( nGXsfl_144_fel_idx < nRC_GXsfl_144 )
               {
                  nGXsfl_144_fel_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_144_fel_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_144_fel_idx+1));
                  sGXsfl_144_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_144_fel_idx), 4, 0)), 4, "0") + sGXsfl_62_fel_idx;
                  SubsflControlProps_fel_1445( ) ;
                  AV46GXV24 = nGXsfl_144_fel_idx;
                  if ( ( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Count >= AV46GXV24 ) && ( AV46GXV24 > 0 ) )
                  {
                     ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.CurrentItem = ((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24));
                     ((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24)).gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid_Internalname), ",", "."));
                     ((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24)).gxTpr_Name = cgiGet( edtavCtlname_Internalname);
                     ((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24)).gxTpr_Value = cgiGet( edtavCtlvalue_Internalname);
                  }
               }
               if ( nGXsfl_144_fel_idx == 0 )
               {
                  nGXsfl_144_idx = 1;
                  sGXsfl_144_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_144_idx), 4, 0)), 4, "0") + sGXsfl_62_fel_idx;
                  SubsflControlProps_1445( ) ;
               }
               nGXsfl_144_fel_idx = 1;
            }
            if ( nGXsfl_62_fel_idx == 0 )
            {
               nGXsfl_62_idx = 1;
               sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
               SubsflControlProps_622( ) ;
            }
            nGXsfl_62_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13KE2 */
         E13KE2 ();
         if (returnInSub) return;
      }

      protected void E13KE2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV20WWPContext) ;
         /* Using cursor H00KE2 */
         pr_default.execute(0, new Object[] {AV20WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29Contratante_Codigo = H00KE2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00KE2_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = H00KE2_A5AreaTrabalho_Codigo[0];
            /* Using cursor H00KE3 */
            pr_default.execute(1, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1384Redmine_ContratanteCod = H00KE3_A1384Redmine_ContratanteCod[0];
               A1381Redmine_Host = H00KE3_A1381Redmine_Host[0];
               A1382Redmine_Url = H00KE3_A1382Redmine_Url[0];
               A1383Redmine_Key = H00KE3_A1383Redmine_Key[0];
               AV6Host = A1381Redmine_Host;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Host", AV6Host);
               AV19Url = A1382Redmine_Url;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Url", AV19Url);
               AV8Key = A1383Redmine_Key;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Key", AV8Key);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void E11KE2( )
      {
         /* 'CarregarFiltros' Routine */
         /* Execute user subroutine: 'GETSTATUS' */
         S112 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'GETPROJECTS' */
         S122 ();
         if (returnInSub) return;
         cmbavStatus.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15Status), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Values", cmbavStatus.ToJavascriptSource());
         cmbavProjeto.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Projeto), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_Internalname, "Values", cmbavProjeto.ToJavascriptSource());
      }

      public void GXEnter( )
      {
         /* Execute user event: E12KE2 */
         E12KE2 ();
         if (returnInSub) return;
      }

      protected void E12KE2( )
      {
         /* Enter Routine */
         AV7httpclient.Host = StringUtil.Trim( AV6Host);
         AV7httpclient.BaseURL = StringUtil.Trim( AV19Url);
         AV5Execute = "issues.xml?project_id=" + StringUtil.Trim( StringUtil.Str( (decimal)(AV10Projeto), 4, 0)) + "&sort=&offset=0&limit=25&page=";
         if ( ! (0==AV15Status) )
         {
            AV5Execute = AV5Execute + "&status_id=" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Status), 4, 0));
         }
         if ( ! (0==AV17Usuario) )
         {
            AV5Execute = AV5Execute + "&assigned_to_id=" + StringUtil.Trim( StringUtil.Str( (decimal)(AV17Usuario), 4, 0));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8Key)) )
         {
            AV5Execute = AV5Execute + "&key=" + StringUtil.Trim( AV8Key);
         }
         AV7httpclient.Execute("GET", AV5Execute);
         AV11SDT_Issues.FromXml(AV7httpclient.ToString(), "");
         gx_BV62 = true;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11SDT_Issues", AV11SDT_Issues);
         nGXsfl_62_bak_idx = nGXsfl_62_idx;
         gxgrGrid2_refresh( ) ;
         nGXsfl_62_idx = nGXsfl_62_bak_idx;
         sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
         SubsflControlProps_622( ) ;
      }

      protected void S122( )
      {
         /* 'GETPROJECTS' Routine */
         AV7httpclient.Host = StringUtil.Trim( AV6Host);
         AV7httpclient.BaseURL = StringUtil.Trim( AV19Url);
         AV5Execute = "projects.xml?";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8Key)) )
         {
            AV5Execute = AV5Execute + "&key=" + AV8Key;
         }
         AV7httpclient.Execute("GET", AV5Execute);
         AV12SDT_Project.FromXml(AV7httpclient.ToString(), "");
         AV12SDT_Project.gxTpr_Projects.Sort("name") ;
         cmbavProjeto.removeAllItems();
         cmbavProjeto.addItem("0", "(Todos)", 0);
         AV54GXV27 = 1;
         while ( AV54GXV27 <= AV12SDT_Project.gxTpr_Projects.Count )
         {
            AV9ProjectItem = ((SdtSDT_Redmineprojects_project)AV12SDT_Project.gxTpr_Projects.Item(AV54GXV27));
            cmbavProjeto.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV9ProjectItem.gxTpr_Id), 4, 0)), AV9ProjectItem.gxTpr_Name, 0);
            AV54GXV27 = (int)(AV54GXV27+1);
         }
      }

      protected void S112( )
      {
         /* 'GETSTATUS' Routine */
         AV7httpclient.Host = StringUtil.Trim( AV6Host);
         AV7httpclient.BaseURL = StringUtil.Trim( AV19Url);
         AV5Execute = "issue_statuses.xml";
         AV7httpclient.Execute("GET", AV5Execute);
         AV13SDT_Status.FromXml(AV7httpclient.ToString(), "");
         AV13SDT_Status.gxTpr_Issue_statuss.Sort("name") ;
         cmbavStatus.removeAllItems();
         cmbavStatus.addItem("0", "(Todos)", 0);
         AV55GXV28 = 1;
         while ( AV55GXV28 <= AV13SDT_Status.gxTpr_Issue_statuss.Count )
         {
            AV16StatusItem = ((SdtSDT_RedmineStatus_issue_status)AV13SDT_Status.gxTpr_Issue_statuss.Item(AV55GXV28));
            cmbavStatus.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV16StatusItem.gxTpr_Id), 4, 0)), AV16StatusItem.gxTpr_Name, 0);
            AV55GXV28 = (int)(AV55GXV28+1);
         }
      }

      protected void S132( )
      {
         /* 'GETUSUARIOS' Routine */
         AV7httpclient.Host = StringUtil.Trim( AV6Host);
         AV7httpclient.BaseURL = StringUtil.Trim( AV19Url);
         AV5Execute = "users.xml?";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8Key)) )
         {
            AV5Execute = AV5Execute + "&key=" + AV8Key;
         }
         AV7httpclient.Execute("GET", AV5Execute);
         AV14SDT_Usuarios.FromXml(AV7httpclient.ToString(), "");
         AV14SDT_Usuarios.gxTpr_Users.Sort("firstname,lastname") ;
         AV56GXV29 = 1;
         while ( AV56GXV29 <= AV14SDT_Usuarios.gxTpr_Users.Count )
         {
            AV18UsuariosItem = ((SdtSDT_RedmineUsers_user)AV14SDT_Usuarios.gxTpr_Users.Item(AV56GXV29));
            cmbavUsuario.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV18UsuariosItem.gxTpr_Id), 4, 0)), AV18UsuariosItem.gxTpr_Firstname+" "+AV18UsuariosItem.gxTpr_Lastname, 0);
            AV56GXV29 = (int)(AV56GXV29+1);
         }
      }

      private void E14KE2( )
      {
         /* Grid2_Load Routine */
         AV26GXV4 = 1;
         while ( AV26GXV4 <= AV11SDT_Issues.gxTpr_Issues.Count )
         {
            AV11SDT_Issues.gxTpr_Issues.CurrentItem = ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 62;
            }
            sendrow_622( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_62_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(62, Grid2Row);
            }
            AV26GXV4 = (short)(AV26GXV4+1);
         }
      }

      private void E15KE5( )
      {
         /* Grid1_Load Routine */
         AV46GXV24 = 1;
         while ( AV46GXV24 <= ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Count )
         {
            ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.CurrentItem = ((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 144;
            }
            sendrow_1445( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_144_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(144, Grid1Row);
            }
            AV46GXV24 = (short)(AV46GXV24+1);
         }
      }

      protected void wb_table1_4_KE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(157), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(232), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock37_Internalname, "Host:", "", "", lblTextblock37_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavHost_Internalname, StringUtil.RTrim( AV6Host), StringUtil.RTrim( context.localUtil.Format( AV6Host, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,9);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavHost_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock39_Internalname, "Url:", "", "", lblTextblock39_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUrl_Internalname, StringUtil.RTrim( AV19Url), StringUtil.RTrim( context.localUtil.Format( AV19Url, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUrl_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock38_Internalname, "Key:", "", "", lblTextblock38_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavKey_Internalname, StringUtil.RTrim( AV8Key), StringUtil.RTrim( context.localUtil.Format( AV8Key, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavKey_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock34_Internalname, "Projeto a importar:", "", "", lblTextblock34_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto, cmbavProjeto_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10Projeto), 4, 0)), 1, cmbavProjeto_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "", true, "HLP_WP_Redmine144.htm");
            cmbavProjeto.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Projeto), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_Internalname, "Values", (String)(cmbavProjeto.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock35_Internalname, "Status:", "", "", lblTextblock35_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavStatus, cmbavStatus_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15Status), 4, 0)), 1, cmbavStatus_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_WP_Redmine144.htm");
            cmbavStatus.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15Status), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Values", (String)(cmbavStatus.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"4\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:16px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(62), 2, 0)+","+"null"+");", "Carregar Filtros", bttButton2_Jsonclick, 5, "Carregar Filtros", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CARREGARFILTROS\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Redmine144.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(62), 2, 0)+","+"null"+");", "Consultar", bttButton1_Jsonclick, 5, "Consultar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  style=\""+CSSHelper.Prettify( "height:2517px")+"\">") ;
            wb_table2_40_KE2( true) ;
         }
         else
         {
            wb_table2_40_KE2( false) ;
         }
         return  ;
      }

      protected void wb_table2_40_KE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_4_KE2e( true) ;
         }
         else
         {
            wb_table1_4_KE2e( false) ;
         }
      }

      protected void wb_table2_40_KE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable9_Internalname, tblTable9_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "total_count", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavCtltotal_count_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11SDT_Issues.gxTpr_Total_count), 4, 0, ",", "")), ((edtavCtltotal_count_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11SDT_Issues.gxTpr_Total_count), "ZZZ9")) : context.localUtil.Format( (decimal)(AV11SDT_Issues.gxTpr_Total_count), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtltotal_count_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCtltotal_count_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "offset", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavCtloffset_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11SDT_Issues.gxTpr_Offset), 2, 0, ",", "")), ((edtavCtloffset_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11SDT_Issues.gxTpr_Offset), "Z9")) : context.localUtil.Format( (decimal)(AV11SDT_Issues.gxTpr_Offset), "Z9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtloffset_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCtloffset_Enabled, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "limit", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavCtllimit_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11SDT_Issues.gxTpr_Limit), 2, 0, ",", "")), ((edtavCtllimit_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11SDT_Issues.gxTpr_Limit), "Z9")) : context.localUtil.Format( (decimal)(AV11SDT_Issues.gxTpr_Limit), "Z9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtllimit_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCtllimit_Enabled, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Redmine144.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /*  Grid Control  */
            Grid2Container.SetIsFreestyle(true);
            Grid2Container.SetWrapped(nGXWrapped);
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid2Container"+"DivS\" data-gxgridid=\"62\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid2_Internalname, subGrid2_Internalname, "", "FreeStyleGrid", 0, "", "", 1, 2, sStyleString, "none", 0);
               Grid2Container.AddObjectProperty("GridName", "Grid2");
            }
            else
            {
               Grid2Container.AddObjectProperty("GridName", "Grid2");
               Grid2Container.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Rules", StringUtil.RTrim( "none"));
               Grid2Container.AddObjectProperty("Class", "FreeStyleGrid");
               Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Backcolorstyle), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Borderwidth), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("CmpContext", "");
               Grid2Container.AddObjectProperty("InMasterPage", "false");
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock5_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlid1_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock6_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlid2_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlname1_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock9_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock10_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlname2_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock12_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlid4_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlname3_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock15_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlid5_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlname4_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock18_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlid6_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlname5_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock21_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlid7_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlname6_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock24_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock25_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock26_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock27_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock28_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock29_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock30_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock31_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock32_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlcreated_on_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock33_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlupdated_on_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowselection), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Selectioncolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowhovering), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Hoveringcolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowcollapsing), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 62 )
         {
            wbEnd = 0;
            nRC_GXsfl_62 = (short)(nGXsfl_62_idx-1);
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV26GXV4 = nGXsfl_62_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid2Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid2", Grid2Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid2ContainerData", Grid2Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid2ContainerData"+"V", Grid2Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid2ContainerData"+"V"+"\" value='"+Grid2Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_40_KE2e( true) ;
         }
         else
         {
            wb_table2_40_KE2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKE2( ) ;
         WSKE2( ) ;
         WEKE2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823473522");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_redmine144.js", "?202042823473522");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1445( )
      {
         edtavCtlid_Internalname = "CTLID_"+sGXsfl_144_idx;
         edtavCtlname_Internalname = "CTLNAME_"+sGXsfl_144_idx;
         edtavCtlvalue_Internalname = "CTLVALUE_"+sGXsfl_144_idx;
      }

      protected void SubsflControlProps_fel_1445( )
      {
         edtavCtlid_Internalname = "CTLID_"+sGXsfl_144_fel_idx;
         edtavCtlname_Internalname = "CTLNAME_"+sGXsfl_144_fel_idx;
         edtavCtlvalue_Internalname = "CTLVALUE_"+sGXsfl_144_fel_idx;
      }

      protected void sendrow_1445( )
      {
         SubsflControlProps_1445( ) ;
         WBKE0( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0x0);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_144_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_144_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCtlid_Enabled!=0)&&(edtavCtlid_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 145,'',false,'"+sGXsfl_144_idx+"',144)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24)).gxTpr_Id), 2, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24)).gxTpr_Id), "Z9")),TempTags+((edtavCtlid_Enabled!=0)&&(edtavCtlid_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlid_Enabled!=0)&&(edtavCtlid_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,145);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)1,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)144,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24)).gxTpr_Name),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlname_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlname_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)9999,(short)0,(short)0,(short)144,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlvalue_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue_custom_fields_custom_field)((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV46GXV24)).gxTpr_Value),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlvalue_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlvalue_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)9999,(short)0,(short)0,(short)144,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_144_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_144_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_144_idx+1));
         sGXsfl_144_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_144_idx), 4, 0)), 4, "0") + sGXsfl_62_idx;
         SubsflControlProps_1445( ) ;
         /* End function sendrow_1445 */
      }

      protected void SubsflControlProps_622( )
      {
         lblTextblock5_Internalname = "TEXTBLOCK5_"+sGXsfl_62_idx;
         edtavCtlid1_Internalname = "CTLID1_"+sGXsfl_62_idx;
         lblTextblock6_Internalname = "TEXTBLOCK6_"+sGXsfl_62_idx;
         edtavCtlid2_Internalname = "CTLID2_"+sGXsfl_62_idx;
         edtavCtlname1_Internalname = "CTLNAME1_"+sGXsfl_62_idx;
         lblTextblock9_Internalname = "TEXTBLOCK9_"+sGXsfl_62_idx;
         lblTextblock10_Internalname = "TEXTBLOCK10_"+sGXsfl_62_idx;
         edtavCtlname2_Internalname = "CTLNAME2_"+sGXsfl_62_idx;
         lblTextblock12_Internalname = "TEXTBLOCK12_"+sGXsfl_62_idx;
         edtavCtlid4_Internalname = "CTLID4_"+sGXsfl_62_idx;
         edtavCtlname3_Internalname = "CTLNAME3_"+sGXsfl_62_idx;
         lblTextblock15_Internalname = "TEXTBLOCK15_"+sGXsfl_62_idx;
         edtavCtlid5_Internalname = "CTLID5_"+sGXsfl_62_idx;
         edtavCtlname4_Internalname = "CTLNAME4_"+sGXsfl_62_idx;
         lblTextblock18_Internalname = "TEXTBLOCK18_"+sGXsfl_62_idx;
         edtavCtlid6_Internalname = "CTLID6_"+sGXsfl_62_idx;
         edtavCtlname5_Internalname = "CTLNAME5_"+sGXsfl_62_idx;
         lblTextblock21_Internalname = "TEXTBLOCK21_"+sGXsfl_62_idx;
         edtavCtlid7_Internalname = "CTLID7_"+sGXsfl_62_idx;
         edtavCtlname6_Internalname = "CTLNAME6_"+sGXsfl_62_idx;
         lblTextblock24_Internalname = "TEXTBLOCK24_"+sGXsfl_62_idx;
         edtavCtlsubject_Internalname = "CTLSUBJECT_"+sGXsfl_62_idx;
         lblTextblock25_Internalname = "TEXTBLOCK25_"+sGXsfl_62_idx;
         edtavCtldescription_Internalname = "CTLDESCRIPTION_"+sGXsfl_62_idx;
         lblTextblock26_Internalname = "TEXTBLOCK26_"+sGXsfl_62_idx;
         edtavCtlstart_date_Internalname = "CTLSTART_DATE_"+sGXsfl_62_idx;
         lblTextblock27_Internalname = "TEXTBLOCK27_"+sGXsfl_62_idx;
         edtavCtldue_date_Internalname = "CTLDUE_DATE_"+sGXsfl_62_idx;
         lblTextblock28_Internalname = "TEXTBLOCK28_"+sGXsfl_62_idx;
         edtavCtldone_ratio_Internalname = "CTLDONE_RATIO_"+sGXsfl_62_idx;
         lblTextblock29_Internalname = "TEXTBLOCK29_"+sGXsfl_62_idx;
         edtavCtlestimated_hours_Internalname = "CTLESTIMATED_HOURS_"+sGXsfl_62_idx;
         lblTextblock30_Internalname = "TEXTBLOCK30_"+sGXsfl_62_idx;
         lblTextblock31_Internalname = "TEXTBLOCK31_"+sGXsfl_62_idx;
         edtavCtltype_Internalname = "CTLTYPE_"+sGXsfl_62_idx;
         lblTextblock32_Internalname = "TEXTBLOCK32_"+sGXsfl_62_idx;
         edtavCtlcreated_on_Internalname = "CTLCREATED_ON_"+sGXsfl_62_idx;
         lblTextblock33_Internalname = "TEXTBLOCK33_"+sGXsfl_62_idx;
         edtavCtlupdated_on_Internalname = "CTLUPDATED_ON_"+sGXsfl_62_idx;
         subGrid1_Internalname = "GRID1_"+sGXsfl_62_idx;
      }

      protected void SubsflControlProps_fel_622( )
      {
         lblTextblock5_Internalname = "TEXTBLOCK5_"+sGXsfl_62_fel_idx;
         edtavCtlid1_Internalname = "CTLID1_"+sGXsfl_62_fel_idx;
         lblTextblock6_Internalname = "TEXTBLOCK6_"+sGXsfl_62_fel_idx;
         edtavCtlid2_Internalname = "CTLID2_"+sGXsfl_62_fel_idx;
         edtavCtlname1_Internalname = "CTLNAME1_"+sGXsfl_62_fel_idx;
         lblTextblock9_Internalname = "TEXTBLOCK9_"+sGXsfl_62_fel_idx;
         lblTextblock10_Internalname = "TEXTBLOCK10_"+sGXsfl_62_fel_idx;
         edtavCtlname2_Internalname = "CTLNAME2_"+sGXsfl_62_fel_idx;
         lblTextblock12_Internalname = "TEXTBLOCK12_"+sGXsfl_62_fel_idx;
         edtavCtlid4_Internalname = "CTLID4_"+sGXsfl_62_fel_idx;
         edtavCtlname3_Internalname = "CTLNAME3_"+sGXsfl_62_fel_idx;
         lblTextblock15_Internalname = "TEXTBLOCK15_"+sGXsfl_62_fel_idx;
         edtavCtlid5_Internalname = "CTLID5_"+sGXsfl_62_fel_idx;
         edtavCtlname4_Internalname = "CTLNAME4_"+sGXsfl_62_fel_idx;
         lblTextblock18_Internalname = "TEXTBLOCK18_"+sGXsfl_62_fel_idx;
         edtavCtlid6_Internalname = "CTLID6_"+sGXsfl_62_fel_idx;
         edtavCtlname5_Internalname = "CTLNAME5_"+sGXsfl_62_fel_idx;
         lblTextblock21_Internalname = "TEXTBLOCK21_"+sGXsfl_62_fel_idx;
         edtavCtlid7_Internalname = "CTLID7_"+sGXsfl_62_fel_idx;
         edtavCtlname6_Internalname = "CTLNAME6_"+sGXsfl_62_fel_idx;
         lblTextblock24_Internalname = "TEXTBLOCK24_"+sGXsfl_62_fel_idx;
         edtavCtlsubject_Internalname = "CTLSUBJECT_"+sGXsfl_62_fel_idx;
         lblTextblock25_Internalname = "TEXTBLOCK25_"+sGXsfl_62_fel_idx;
         edtavCtldescription_Internalname = "CTLDESCRIPTION_"+sGXsfl_62_fel_idx;
         lblTextblock26_Internalname = "TEXTBLOCK26_"+sGXsfl_62_fel_idx;
         edtavCtlstart_date_Internalname = "CTLSTART_DATE_"+sGXsfl_62_fel_idx;
         lblTextblock27_Internalname = "TEXTBLOCK27_"+sGXsfl_62_fel_idx;
         edtavCtldue_date_Internalname = "CTLDUE_DATE_"+sGXsfl_62_fel_idx;
         lblTextblock28_Internalname = "TEXTBLOCK28_"+sGXsfl_62_fel_idx;
         edtavCtldone_ratio_Internalname = "CTLDONE_RATIO_"+sGXsfl_62_fel_idx;
         lblTextblock29_Internalname = "TEXTBLOCK29_"+sGXsfl_62_fel_idx;
         edtavCtlestimated_hours_Internalname = "CTLESTIMATED_HOURS_"+sGXsfl_62_fel_idx;
         lblTextblock30_Internalname = "TEXTBLOCK30_"+sGXsfl_62_fel_idx;
         lblTextblock31_Internalname = "TEXTBLOCK31_"+sGXsfl_62_fel_idx;
         edtavCtltype_Internalname = "CTLTYPE_"+sGXsfl_62_fel_idx;
         lblTextblock32_Internalname = "TEXTBLOCK32_"+sGXsfl_62_fel_idx;
         edtavCtlcreated_on_Internalname = "CTLCREATED_ON_"+sGXsfl_62_fel_idx;
         lblTextblock33_Internalname = "TEXTBLOCK33_"+sGXsfl_62_fel_idx;
         edtavCtlupdated_on_Internalname = "CTLUPDATED_ON_"+sGXsfl_62_fel_idx;
         subGrid1_Internalname = "GRID1_"+sGXsfl_62_fel_idx;
      }

      protected void sendrow_622( )
      {
         SubsflControlProps_622( ) ;
         WBKE0( ) ;
         Grid2Row = GXWebRow.GetNew(context,Grid2Container);
         if ( subGrid2_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid2_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Odd";
            }
         }
         else if ( subGrid2_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid2_Backstyle = 0;
            subGrid2_Backcolor = subGrid2_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Uniform";
            }
         }
         else if ( subGrid2_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid2_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Odd";
            }
            subGrid2_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid2_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid2_Backstyle = 1;
            if ( ((int)(((nGXsfl_62_idx-1)/ (decimal)(1)) % (2))) == 0 )
            {
               subGrid2_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Odd";
               }
            }
            else
            {
               subGrid2_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Even";
               }
            }
         }
         /* Start of Columns property logic. */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            if ( ( 1 == 0 ) && ( nGXsfl_62_idx == 1 ) )
            {
               context.WriteHtmlText( "<tr"+" class=\""+subGrid2_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_62_idx+"\">") ;
            }
            if ( 1 > 0 )
            {
               if ( ( 1 == 1 ) || ( ((int)((nGXsfl_62_idx) % (1))) - 1 == 0 ) )
               {
                  context.WriteHtmlText( "<tr"+" class=\""+subGrid2_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_62_idx+"\">") ;
               }
            }
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock5_Internalname,(String)"Issue",(String)"",(String)"",(String)lblTextblock5_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid1_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Id), 4, 0, ",", "")),((edtavCtlid1_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Id), "ZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Id), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid1_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavCtlid1_Enabled,(short)0,(String)"text",(String)"",(short)4,(String)"chr",(short)1,(String)"row",(short)4,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock6_Internalname,(String)"project",(String)"",(String)"",(String)lblTextblock6_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Single line edit */
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid2_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Project.gxTpr_Id), 2, 0, ",", "")),((edtavCtlid2_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Project.gxTpr_Id), "Z9")) : context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Project.gxTpr_Id), "Z9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid2_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavCtlid2_Enabled,(short)0,(String)"text",(String)"",(short)2,(String)"chr",(short)1,(String)"row",(short)2,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Multiple line edit */
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname1_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Project.gxTpr_Name),(String)"",(String)"",(short)0,(short)1,(int)edtavCtlname1_Enabled,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         /* Table start */
         Grid2Row.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTable2_Internalname+"_"+sGXsfl_62_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("table");
         }
         /* End of table */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock9_Internalname,(String)"tracker",(String)"",(String)"",(String)lblTextblock9_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock10_Internalname,(String)"id",(String)"",(String)"",(String)lblTextblock10_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Multiple line edit */
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname2_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Tracker.gxTpr_Name),(String)"",(String)"",(short)0,(short)1,(int)edtavCtlname2_Enabled,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         /* Table start */
         Grid2Row.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTable3_Internalname+"_"+sGXsfl_62_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("table");
         }
         /* End of table */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock12_Internalname,(String)"status",(String)"",(String)"",(String)lblTextblock12_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Single line edit */
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid4_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Status.gxTpr_Id), 2, 0, ",", "")),((edtavCtlid4_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Status.gxTpr_Id), "Z9")) : context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Status.gxTpr_Id), "Z9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid4_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavCtlid4_Enabled,(short)0,(String)"text",(String)"",(short)2,(String)"chr",(short)1,(String)"row",(short)2,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Multiple line edit */
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname3_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Status.gxTpr_Name),(String)"",(String)"",(short)0,(short)1,(int)edtavCtlname3_Enabled,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         /* Table start */
         Grid2Row.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTable4_Internalname+"_"+sGXsfl_62_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("table");
         }
         /* End of table */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock15_Internalname,(String)"priority",(String)"",(String)"",(String)lblTextblock15_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Single line edit */
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid5_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Priority.gxTpr_Id), 2, 0, ",", "")),((edtavCtlid5_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Priority.gxTpr_Id), "Z9")) : context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Priority.gxTpr_Id), "Z9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid5_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavCtlid5_Enabled,(short)0,(String)"text",(String)"",(short)2,(String)"chr",(short)1,(String)"row",(short)2,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Multiple line edit */
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname4_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Priority.gxTpr_Name),(String)"",(String)"",(short)0,(short)1,(int)edtavCtlname4_Enabled,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         /* Table start */
         Grid2Row.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTable5_Internalname+"_"+sGXsfl_62_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("table");
         }
         /* End of table */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock18_Internalname,(String)"author",(String)"",(String)"",(String)lblTextblock18_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Single line edit */
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid6_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Author.gxTpr_Id), 2, 0, ",", "")),((edtavCtlid6_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Author.gxTpr_Id), "Z9")) : context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Author.gxTpr_Id), "Z9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid6_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavCtlid6_Enabled,(short)0,(String)"text",(String)"",(short)2,(String)"chr",(short)1,(String)"row",(short)2,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Multiple line edit */
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname5_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Author.gxTpr_Name),(String)"",(String)"",(short)0,(short)1,(int)edtavCtlname5_Enabled,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         /* Table start */
         Grid2Row.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTable6_Internalname+"_"+sGXsfl_62_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("table");
         }
         /* End of table */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock21_Internalname,(String)"assigned_to",(String)"",(String)"",(String)lblTextblock21_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Single line edit */
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid7_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Assigned_to.gxTpr_Id), 2, 0, ",", "")),((edtavCtlid7_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Assigned_to.gxTpr_Id), "Z9")) : context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Assigned_to.gxTpr_Id), "Z9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid7_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavCtlid7_Enabled,(short)0,(String)"text",(String)"",(short)2,(String)"chr",(short)1,(String)"row",(short)2,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Multiple line edit */
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname6_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Assigned_to.gxTpr_Name),(String)"",(String)"",(short)0,(short)1,(int)edtavCtlname6_Enabled,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         /* Table start */
         Grid2Row.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTable7_Internalname+"_"+sGXsfl_62_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("table");
         }
         /* End of table */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock24_Internalname,(String)"subject",(String)"",(String)"",(String)lblTextblock24_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         TempTags = " " + ((edtavCtlsubject_Enabled!=0)&&(edtavCtlsubject_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 108,'',false,'"+sGXsfl_62_idx+"',62)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlsubject_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Subject),(String)"",TempTags+((edtavCtlsubject_Enabled!=0)&&(edtavCtlsubject_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlsubject_Enabled!=0)&&(edtavCtlsubject_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,108);\"" : " "),(short)0,(short)1,(short)1,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock25_Internalname,(String)"description",(String)"",(String)"",(String)lblTextblock25_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         TempTags = " " + ((edtavCtldescription_Enabled!=0)&&(edtavCtldescription_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 113,'',false,'"+sGXsfl_62_idx+"',62)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldescription_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Description),(String)"",TempTags+((edtavCtldescription_Enabled!=0)&&(edtavCtldescription_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtldescription_Enabled!=0)&&(edtavCtldescription_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,113);\"" : " "),(short)0,(short)1,(short)1,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock26_Internalname,(String)"Data",(String)"",(String)"",(String)lblTextblock26_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         TempTags = " " + ((edtavCtlstart_date_Enabled!=0)&&(edtavCtlstart_date_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 118,'',false,'"+sGXsfl_62_idx+"',62)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlstart_date_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Start_date),(String)"",TempTags+((edtavCtlstart_date_Enabled!=0)&&(edtavCtlstart_date_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlstart_date_Enabled!=0)&&(edtavCtlstart_date_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,118);\"" : " "),(short)0,(short)1,(short)1,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock27_Internalname,(String)"Para",(String)"",(String)"",(String)lblTextblock27_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         TempTags = " " + ((edtavCtldue_date_Enabled!=0)&&(edtavCtldue_date_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 122,'',false,'"+sGXsfl_62_idx+"',62)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldue_date_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Due_date),(String)"",TempTags+((edtavCtldue_date_Enabled!=0)&&(edtavCtldue_date_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtldue_date_Enabled!=0)&&(edtavCtldue_date_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,122);\"" : " "),(short)0,(short)1,(short)1,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock28_Internalname,(String)"done_ratio",(String)"",(String)"",(String)lblTextblock28_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         TempTags = " " + ((edtavCtldone_ratio_Enabled!=0)&&(edtavCtldone_ratio_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 127,'',false,'"+sGXsfl_62_idx+"',62)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldone_ratio_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Done_ratio), 2, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Done_ratio), "Z9")),TempTags+((edtavCtldone_ratio_Enabled!=0)&&(edtavCtldone_ratio_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtldone_ratio_Enabled!=0)&&(edtavCtldone_ratio_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,127);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtldone_ratio_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)2,(String)"chr",(short)1,(String)"row",(short)2,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock29_Internalname,(String)"estimated_hours",(String)"",(String)"",(String)lblTextblock29_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         TempTags = " " + ((edtavCtlestimated_hours_Enabled!=0)&&(edtavCtlestimated_hours_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 132,'',false,'"+sGXsfl_62_idx+"',62)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlestimated_hours_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Estimated_hours),(String)"",TempTags+((edtavCtlestimated_hours_Enabled!=0)&&(edtavCtlestimated_hours_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlestimated_hours_Enabled!=0)&&(edtavCtlestimated_hours_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,132);\"" : " "),(short)0,(short)1,(short)1,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock30_Internalname,(String)"custom_fields",(String)"",(String)"",(String)lblTextblock30_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Table start */
         Grid2Row.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTable8_Internalname+"_"+sGXsfl_62_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock31_Internalname,(String)"type",(String)"",(String)"",(String)lblTextblock31_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         TempTags = " " + ((edtavCtltype_Enabled!=0)&&(edtavCtltype_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 141,'',false,'"+sGXsfl_62_idx+"',62)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtltype_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Custom_fields.gxTpr_Type),(String)"",TempTags+((edtavCtltype_Enabled!=0)&&(edtavCtltype_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtltype_Enabled!=0)&&(edtavCtltype_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,141);\"" : " "),(short)0,(short)1,(short)1,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /*  Child Grid Control  */
         Grid2Row.AddColumnProperties("subfile", -1, isAjaxCallMode( ), new Object[] {(String)"Grid1Container"});
         if ( isAjaxCallMode( ) )
         {
            Grid1Container = new GXWebGrid( context);
         }
         else
         {
            Grid1Container.Clear();
         }
         Grid1Container.SetWrapped(nGXWrapped);
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"144\">") ;
            sStyleString = "";
            GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
            /* Subfile titles */
            context.WriteHtmlText( "<tr") ;
            context.WriteHtmlTextNl( ">") ;
            if ( subGrid1_Backcolorstyle == 0 )
            {
               subGrid1_Titlebackstyle = 0;
               if ( StringUtil.Len( subGrid1_Class) > 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Title";
               }
            }
            else
            {
               subGrid1_Titlebackstyle = 1;
               if ( subGrid1_Backcolorstyle == 1 )
               {
                  subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                  }
               }
               else
               {
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
            }
            context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
            context.SendWebValue( "id") ;
            context.WriteHtmlTextNl( "</th>") ;
            context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
            context.SendWebValue( "name") ;
            context.WriteHtmlTextNl( "</th>") ;
            context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
            context.SendWebValue( "value") ;
            context.WriteHtmlTextNl( "</th>") ;
            context.WriteHtmlTextNl( "</tr>") ;
            Grid1Container.AddObjectProperty("GridName", "Grid1");
         }
         else
         {
            Grid1Container.AddObjectProperty("GridName", "Grid1");
            Grid1Container.AddObjectProperty("Class", "Grid");
            Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
            Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
            Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
            Grid1Container.AddObjectProperty("CmpContext", "");
            Grid1Container.AddObjectProperty("InMasterPage", "false");
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlname_Enabled), 5, 0, ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlvalue_Enabled), 5, 0, ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
            Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
            Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
            Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
            Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
            Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
         }
         RFKE5( ) ;
         nRC_GXsfl_144 = (short)(nGXsfl_144_idx-1);
         GXCCtl = "nRC_GXsfl_144_" + sGXsfl_62_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_144), 4, 0, ",", "")));
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "</table>") ;
         }
         else
         {
            if ( ! isAjaxCallMode( ) )
            {
               GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"_"+sGXsfl_62_idx, Grid1Container.ToJavascriptSource());
            }
            if ( isAjaxCallMode( ) )
            {
               Grid2Row.AddGrid("Grid1", Grid1Container);
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V_"+sGXsfl_62_idx, Grid1Container.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V_"+sGXsfl_62_idx+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
            }
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("table");
         }
         /* End of table */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock32_Internalname,(String)"created_on",(String)"",(String)"",(String)lblTextblock32_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlcreated_on_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Created_on),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlcreated_on_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavCtlcreated_on_Enabled,(short)0,(String)"text",(String)"",(short)20,(String)"chr",(short)1,(String)"row",(short)20,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock33_Internalname,(String)"updated_on",(String)"",(String)"",(String)lblTextblock33_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlupdated_on_Internalname,StringUtil.RTrim( ((SdtSDT_Redmine144_issues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV26GXV4)).gxTpr_Updated_on),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlupdated_on_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavCtlupdated_on_Enabled,(short)0,(String)"text",(String)"",(short)20,(String)"chr",(short)1,(String)"row",(short)20,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         /* End of Columns property logic. */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            if ( 1 > 0 )
            {
               if ( ((int)((nGXsfl_62_idx) % (1))) == 0 )
               {
                  context.WriteHtmlTextNl( "</tr>") ;
               }
            }
         }
         Grid2Container.AddRow(Grid2Row);
         nGXsfl_62_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_62_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_62_idx+1));
         sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
         SubsflControlProps_622( ) ;
         /* End function sendrow_622 */
      }

      protected void init_default_properties( )
      {
         lblTextblock37_Internalname = "TEXTBLOCK37";
         edtavHost_Internalname = "vHOST";
         lblTextblock39_Internalname = "TEXTBLOCK39";
         edtavUrl_Internalname = "vURL";
         lblTextblock38_Internalname = "TEXTBLOCK38";
         edtavKey_Internalname = "vKEY";
         lblTextblock34_Internalname = "TEXTBLOCK34";
         cmbavProjeto_Internalname = "vPROJETO";
         lblTextblock35_Internalname = "TEXTBLOCK35";
         cmbavStatus_Internalname = "vSTATUS";
         bttButton2_Internalname = "BUTTON2";
         bttButton1_Internalname = "BUTTON1";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavCtltotal_count_Internalname = "CTLTOTAL_COUNT";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavCtloffset_Internalname = "CTLOFFSET";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavCtllimit_Internalname = "CTLLIMIT";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavCtlid1_Internalname = "CTLID1";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         edtavCtlid2_Internalname = "CTLID2";
         edtavCtlname1_Internalname = "CTLNAME1";
         tblTable2_Internalname = "TABLE2";
         lblTextblock9_Internalname = "TEXTBLOCK9";
         lblTextblock10_Internalname = "TEXTBLOCK10";
         edtavCtlname2_Internalname = "CTLNAME2";
         tblTable3_Internalname = "TABLE3";
         lblTextblock12_Internalname = "TEXTBLOCK12";
         edtavCtlid4_Internalname = "CTLID4";
         edtavCtlname3_Internalname = "CTLNAME3";
         tblTable4_Internalname = "TABLE4";
         lblTextblock15_Internalname = "TEXTBLOCK15";
         edtavCtlid5_Internalname = "CTLID5";
         edtavCtlname4_Internalname = "CTLNAME4";
         tblTable5_Internalname = "TABLE5";
         lblTextblock18_Internalname = "TEXTBLOCK18";
         edtavCtlid6_Internalname = "CTLID6";
         edtavCtlname5_Internalname = "CTLNAME5";
         tblTable6_Internalname = "TABLE6";
         lblTextblock21_Internalname = "TEXTBLOCK21";
         edtavCtlid7_Internalname = "CTLID7";
         edtavCtlname6_Internalname = "CTLNAME6";
         tblTable7_Internalname = "TABLE7";
         lblTextblock24_Internalname = "TEXTBLOCK24";
         edtavCtlsubject_Internalname = "CTLSUBJECT";
         lblTextblock25_Internalname = "TEXTBLOCK25";
         edtavCtldescription_Internalname = "CTLDESCRIPTION";
         lblTextblock26_Internalname = "TEXTBLOCK26";
         edtavCtlstart_date_Internalname = "CTLSTART_DATE";
         lblTextblock27_Internalname = "TEXTBLOCK27";
         edtavCtldue_date_Internalname = "CTLDUE_DATE";
         lblTextblock28_Internalname = "TEXTBLOCK28";
         edtavCtldone_ratio_Internalname = "CTLDONE_RATIO";
         lblTextblock29_Internalname = "TEXTBLOCK29";
         edtavCtlestimated_hours_Internalname = "CTLESTIMATED_HOURS";
         lblTextblock30_Internalname = "TEXTBLOCK30";
         lblTextblock31_Internalname = "TEXTBLOCK31";
         edtavCtltype_Internalname = "CTLTYPE";
         edtavCtlid_Internalname = "CTLID";
         edtavCtlname_Internalname = "CTLNAME";
         edtavCtlvalue_Internalname = "CTLVALUE";
         tblTable8_Internalname = "TABLE8";
         lblTextblock32_Internalname = "TEXTBLOCK32";
         edtavCtlcreated_on_Internalname = "CTLCREATED_ON";
         lblTextblock33_Internalname = "TEXTBLOCK33";
         edtavCtlupdated_on_Internalname = "CTLUPDATED_ON";
         tblTable9_Internalname = "TABLE9";
         tblTable1_Internalname = "TABLE1";
         cmbavUsuario_Internalname = "vUSUARIO";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
         subGrid2_Internalname = "GRID2";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavCtlupdated_on_Jsonclick = "";
         edtavCtlcreated_on_Jsonclick = "";
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtavCtltype_Visible = 1;
         edtavCtltype_Enabled = 1;
         edtavCtlestimated_hours_Visible = 1;
         edtavCtlestimated_hours_Enabled = 1;
         edtavCtldone_ratio_Jsonclick = "";
         edtavCtldone_ratio_Visible = 1;
         edtavCtldone_ratio_Enabled = 1;
         edtavCtldue_date_Visible = 1;
         edtavCtldue_date_Enabled = 1;
         edtavCtlstart_date_Visible = 1;
         edtavCtlstart_date_Enabled = 1;
         edtavCtldescription_Visible = 1;
         edtavCtldescription_Enabled = 1;
         edtavCtlsubject_Visible = 1;
         edtavCtlsubject_Enabled = 1;
         edtavCtlid7_Jsonclick = "";
         edtavCtlid6_Jsonclick = "";
         edtavCtlid5_Jsonclick = "";
         edtavCtlid4_Jsonclick = "";
         edtavCtlid2_Jsonclick = "";
         edtavCtlid1_Jsonclick = "";
         subGrid2_Class = "FreeStyleGrid";
         edtavCtlvalue_Jsonclick = "";
         edtavCtlvalue_Enabled = 0;
         edtavCtlname_Jsonclick = "";
         edtavCtlname_Enabled = 0;
         edtavCtlid_Jsonclick = "";
         edtavCtlid_Visible = -1;
         edtavCtlid_Enabled = 1;
         subGrid1_Class = "Grid";
         subGrid2_Allowcollapsing = 0;
         edtavCtlupdated_on_Enabled = 0;
         lblTextblock33_Caption = "updated_on";
         edtavCtlcreated_on_Enabled = 0;
         lblTextblock32_Caption = "created_on";
         lblTextblock31_Caption = "type";
         lblTextblock30_Caption = "custom_fields";
         lblTextblock29_Caption = "estimated_hours";
         lblTextblock28_Caption = "done_ratio";
         lblTextblock27_Caption = "Para";
         lblTextblock26_Caption = "Data";
         lblTextblock25_Caption = "description";
         lblTextblock24_Caption = "subject";
         edtavCtlname6_Enabled = 0;
         edtavCtlid7_Enabled = 0;
         lblTextblock21_Caption = "assigned_to";
         edtavCtlname5_Enabled = 0;
         edtavCtlid6_Enabled = 0;
         lblTextblock18_Caption = "author";
         edtavCtlname4_Enabled = 0;
         edtavCtlid5_Enabled = 0;
         lblTextblock15_Caption = "priority";
         edtavCtlname3_Enabled = 0;
         edtavCtlid4_Enabled = 0;
         lblTextblock12_Caption = "status";
         edtavCtlname2_Enabled = 0;
         lblTextblock10_Caption = "id";
         lblTextblock9_Caption = "tracker";
         edtavCtlname1_Enabled = 0;
         edtavCtlid2_Enabled = 0;
         lblTextblock6_Caption = "project";
         edtavCtlid1_Enabled = 0;
         lblTextblock5_Caption = "Issue";
         edtavCtllimit_Jsonclick = "";
         edtavCtllimit_Enabled = 0;
         edtavCtloffset_Jsonclick = "";
         edtavCtloffset_Enabled = 0;
         edtavCtltotal_count_Jsonclick = "";
         edtavCtltotal_count_Enabled = 0;
         cmbavStatus_Jsonclick = "";
         cmbavProjeto_Jsonclick = "";
         edtavKey_Jsonclick = "";
         edtavUrl_Jsonclick = "";
         edtavHost_Jsonclick = "";
         subGrid1_Backcolorstyle = 0;
         subGrid2_Borderwidth = 0;
         subGrid2_Backcolorstyle = 0;
         edtavCtlupdated_on_Enabled = -1;
         edtavCtlcreated_on_Enabled = -1;
         edtavCtlvalue_Enabled = -1;
         edtavCtlname_Enabled = -1;
         edtavCtlname6_Enabled = -1;
         edtavCtlid7_Enabled = -1;
         edtavCtlname5_Enabled = -1;
         edtavCtlid6_Enabled = -1;
         edtavCtlname4_Enabled = -1;
         edtavCtlid5_Enabled = -1;
         edtavCtlname3_Enabled = -1;
         edtavCtlid4_Enabled = -1;
         edtavCtlname2_Enabled = -1;
         edtavCtlname1_Enabled = -1;
         edtavCtlid2_Enabled = -1;
         edtavCtlid1_Enabled = -1;
         edtavCtllimit_Enabled = -1;
         edtavCtloffset_Enabled = -1;
         edtavCtltotal_count_Enabled = -1;
         cmbavUsuario_Jsonclick = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Redmine1.4.4";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID2_nFirstRecordOnPage',nv:0},{av:'GRID2_nEOF',nv:0},{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'AV11SDT_Issues',fld:'vSDT_ISSUES',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'CARREGARFILTROS'","{handler:'E11KE2',iparms:[{av:'AV6Host',fld:'vHOST',pic:'',nv:''},{av:'AV19Url',fld:'vURL',pic:'',nv:''},{av:'AV8Key',fld:'vKEY',pic:'',nv:''}],oparms:[{av:'AV15Status',fld:'vSTATUS',pic:'ZZZ9',nv:0},{av:'AV10Projeto',fld:'vPROJETO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("ENTER","{handler:'E12KE2',iparms:[{av:'AV6Host',fld:'vHOST',pic:'',nv:''},{av:'AV19Url',fld:'vURL',pic:'',nv:''},{av:'AV10Projeto',fld:'vPROJETO',pic:'ZZZ9',nv:0},{av:'AV15Status',fld:'vSTATUS',pic:'ZZZ9',nv:0},{av:'AV17Usuario',fld:'vUSUARIO',pic:'ZZZ9',nv:0},{av:'AV8Key',fld:'vKEY',pic:'',nv:''},{av:'AV11SDT_Issues',fld:'vSDT_ISSUES',pic:'',nv:null},{av:'GRID2_nFirstRecordOnPage',nv:0},{av:'GRID2_nEOF',nv:0}],oparms:[{av:'AV11SDT_Issues',fld:'vSDT_ISSUES',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV11SDT_Issues = new SdtSDT_Redmine144_issues(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Grid2Container = new GXWebGrid( context);
         Grid1Container = new GXWebGrid( context);
         AV6Host = "";
         AV19Url = "";
         AV8Key = "";
         GXCCtl = "";
         AV20WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         H00KE2_A29Contratante_Codigo = new int[1] ;
         H00KE2_n29Contratante_Codigo = new bool[] {false} ;
         H00KE2_A5AreaTrabalho_Codigo = new int[1] ;
         H00KE3_A1380Redmine_Codigo = new int[1] ;
         H00KE3_A1384Redmine_ContratanteCod = new int[1] ;
         H00KE3_A1381Redmine_Host = new String[] {""} ;
         H00KE3_A1382Redmine_Url = new String[] {""} ;
         H00KE3_A1383Redmine_Key = new String[] {""} ;
         A1381Redmine_Host = "";
         A1382Redmine_Url = "";
         A1383Redmine_Key = "";
         AV7httpclient = new GxHttpClient( context);
         AV5Execute = "";
         AV12SDT_Project = new SdtSDT_Redmineprojects(context);
         AV9ProjectItem = new SdtSDT_Redmineprojects_project(context);
         AV13SDT_Status = new SdtSDT_RedmineStatus(context);
         AV16StatusItem = new SdtSDT_RedmineStatus_issue_status(context);
         AV14SDT_Usuarios = new SdtSDT_RedmineUsers(context);
         AV18UsuariosItem = new SdtSDT_RedmineUsers_user(context);
         Grid2Row = new GXWebRow();
         Grid1Row = new GXWebRow();
         sStyleString = "";
         lblTextblock37_Jsonclick = "";
         lblTextblock39_Jsonclick = "";
         lblTextblock38_Jsonclick = "";
         lblTextblock34_Jsonclick = "";
         lblTextblock35_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttButton2_Jsonclick = "";
         bttButton1_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         Grid2Column = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         subGrid1_Linesclass = "";
         ROClassString = "";
         subGrid2_Linesclass = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock9_Jsonclick = "";
         lblTextblock10_Jsonclick = "";
         lblTextblock12_Jsonclick = "";
         lblTextblock15_Jsonclick = "";
         lblTextblock18_Jsonclick = "";
         lblTextblock21_Jsonclick = "";
         lblTextblock24_Jsonclick = "";
         lblTextblock25_Jsonclick = "";
         lblTextblock26_Jsonclick = "";
         lblTextblock27_Jsonclick = "";
         lblTextblock28_Jsonclick = "";
         lblTextblock29_Jsonclick = "";
         lblTextblock30_Jsonclick = "";
         lblTextblock31_Jsonclick = "";
         Grid1Column = new GXWebColumn();
         lblTextblock32_Jsonclick = "";
         lblTextblock33_Jsonclick = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_redmine144__default(),
            new Object[][] {
                new Object[] {
               H00KE2_A29Contratante_Codigo, H00KE2_n29Contratante_Codigo, H00KE2_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               H00KE3_A1380Redmine_Codigo, H00KE3_A1384Redmine_ContratanteCod, H00KE3_A1381Redmine_Host, H00KE3_A1382Redmine_Url, H00KE3_A1383Redmine_Key
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtltotal_count_Enabled = 0;
         edtavCtloffset_Enabled = 0;
         edtavCtllimit_Enabled = 0;
         edtavCtlid1_Enabled = 0;
         edtavCtlid2_Enabled = 0;
         edtavCtlname1_Enabled = 0;
         edtavCtlname2_Enabled = 0;
         edtavCtlid4_Enabled = 0;
         edtavCtlname3_Enabled = 0;
         edtavCtlid5_Enabled = 0;
         edtavCtlname4_Enabled = 0;
         edtavCtlid6_Enabled = 0;
         edtavCtlname5_Enabled = 0;
         edtavCtlid7_Enabled = 0;
         edtavCtlname6_Enabled = 0;
         edtavCtlname_Enabled = 0;
         edtavCtlvalue_Enabled = 0;
         edtavCtlcreated_on_Enabled = 0;
         edtavCtlupdated_on_Enabled = 0;
      }

      private short nRC_GXsfl_144 ;
      private short nGXsfl_144_idx=1 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_62 ;
      private short nGXsfl_62_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short AV17Usuario ;
      private short AV26GXV4 ;
      private short AV46GXV24 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV10Projeto ;
      private short AV15Status ;
      private short nGXsfl_62_Refreshing=0 ;
      private short subGrid2_Backcolorstyle ;
      private short subGrid2_Borderwidth ;
      private short nGXsfl_144_Refreshing=0 ;
      private short subGrid1_Backcolorstyle ;
      private short nGXsfl_62_fel_idx=1 ;
      private short nGXsfl_144_fel_idx=1 ;
      private short GRID2_nEOF ;
      private short nGXsfl_62_bak_idx=1 ;
      private short subGrid2_Allowselection ;
      private short subGrid2_Allowhovering ;
      private short subGrid2_Allowcollapsing ;
      private short subGrid2_Collapsed ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private short subGrid2_Backstyle ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short GRID1_nEOF ;
      private int subGrid2_Islastpage ;
      private int subGrid1_Islastpage ;
      private int edtavCtltotal_count_Enabled ;
      private int edtavCtloffset_Enabled ;
      private int edtavCtllimit_Enabled ;
      private int edtavCtlid1_Enabled ;
      private int edtavCtlid2_Enabled ;
      private int edtavCtlname1_Enabled ;
      private int edtavCtlname2_Enabled ;
      private int edtavCtlid4_Enabled ;
      private int edtavCtlname3_Enabled ;
      private int edtavCtlid5_Enabled ;
      private int edtavCtlname4_Enabled ;
      private int edtavCtlid6_Enabled ;
      private int edtavCtlname5_Enabled ;
      private int edtavCtlid7_Enabled ;
      private int edtavCtlname6_Enabled ;
      private int edtavCtlname_Enabled ;
      private int edtavCtlvalue_Enabled ;
      private int edtavCtlcreated_on_Enabled ;
      private int edtavCtlupdated_on_Enabled ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A1384Redmine_ContratanteCod ;
      private int AV54GXV27 ;
      private int AV55GXV28 ;
      private int AV56GXV29 ;
      private int subGrid2_Selectioncolor ;
      private int subGrid2_Hoveringcolor ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private int subGrid1_Allbackcolor ;
      private int edtavCtlid_Enabled ;
      private int edtavCtlid_Visible ;
      private int subGrid2_Backcolor ;
      private int subGrid2_Allbackcolor ;
      private int edtavCtlsubject_Enabled ;
      private int edtavCtlsubject_Visible ;
      private int edtavCtldescription_Enabled ;
      private int edtavCtldescription_Visible ;
      private int edtavCtlstart_date_Enabled ;
      private int edtavCtlstart_date_Visible ;
      private int edtavCtldue_date_Enabled ;
      private int edtavCtldue_date_Visible ;
      private int edtavCtldone_ratio_Enabled ;
      private int edtavCtldone_ratio_Visible ;
      private int edtavCtlestimated_hours_Enabled ;
      private int edtavCtlestimated_hours_Visible ;
      private int edtavCtltype_Enabled ;
      private int edtavCtltype_Visible ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private long GRID1_nCurrentRecord ;
      private long GRID2_nCurrentRecord ;
      private long GRID2_nFirstRecordOnPage ;
      private long GRID1_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_144_idx="0001" ;
      private String GXKey ;
      private String sGXsfl_62_idx="0001" ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String cmbavUsuario_Internalname ;
      private String cmbavUsuario_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavCtlid1_Internalname ;
      private String edtavCtlid2_Internalname ;
      private String edtavCtlname1_Internalname ;
      private String edtavCtlname2_Internalname ;
      private String edtavCtlid4_Internalname ;
      private String edtavCtlname3_Internalname ;
      private String edtavCtlid5_Internalname ;
      private String edtavCtlname4_Internalname ;
      private String edtavCtlid6_Internalname ;
      private String edtavCtlname5_Internalname ;
      private String edtavCtlid7_Internalname ;
      private String edtavCtlname6_Internalname ;
      private String edtavCtlsubject_Internalname ;
      private String edtavCtldescription_Internalname ;
      private String edtavCtlstart_date_Internalname ;
      private String edtavCtldue_date_Internalname ;
      private String edtavCtldone_ratio_Internalname ;
      private String edtavCtlestimated_hours_Internalname ;
      private String edtavCtltype_Internalname ;
      private String edtavCtlcreated_on_Internalname ;
      private String edtavCtlupdated_on_Internalname ;
      private String edtavCtlid_Internalname ;
      private String edtavCtlname_Internalname ;
      private String edtavCtlvalue_Internalname ;
      private String edtavHost_Internalname ;
      private String edtavCtltotal_count_Internalname ;
      private String edtavCtloffset_Internalname ;
      private String edtavCtllimit_Internalname ;
      private String AV6Host ;
      private String AV19Url ;
      private String edtavUrl_Internalname ;
      private String AV8Key ;
      private String edtavKey_Internalname ;
      private String cmbavProjeto_Internalname ;
      private String cmbavStatus_Internalname ;
      private String sGXsfl_62_fel_idx="0001" ;
      private String GXCCtl ;
      private String sGXsfl_144_fel_idx="0001" ;
      private String scmdbuf ;
      private String A1381Redmine_Host ;
      private String A1382Redmine_Url ;
      private String A1383Redmine_Key ;
      private String AV5Execute ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String lblTextblock37_Internalname ;
      private String lblTextblock37_Jsonclick ;
      private String edtavHost_Jsonclick ;
      private String lblTextblock39_Internalname ;
      private String lblTextblock39_Jsonclick ;
      private String edtavUrl_Jsonclick ;
      private String lblTextblock38_Internalname ;
      private String lblTextblock38_Jsonclick ;
      private String edtavKey_Jsonclick ;
      private String lblTextblock34_Internalname ;
      private String lblTextblock34_Jsonclick ;
      private String cmbavProjeto_Jsonclick ;
      private String lblTextblock35_Internalname ;
      private String lblTextblock35_Jsonclick ;
      private String cmbavStatus_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String tblTable9_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtavCtltotal_count_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavCtloffset_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavCtllimit_Jsonclick ;
      private String subGrid2_Internalname ;
      private String lblTextblock5_Caption ;
      private String lblTextblock6_Caption ;
      private String lblTextblock9_Caption ;
      private String lblTextblock10_Caption ;
      private String lblTextblock12_Caption ;
      private String lblTextblock15_Caption ;
      private String lblTextblock18_Caption ;
      private String lblTextblock21_Caption ;
      private String lblTextblock24_Caption ;
      private String lblTextblock25_Caption ;
      private String lblTextblock26_Caption ;
      private String lblTextblock27_Caption ;
      private String lblTextblock28_Caption ;
      private String lblTextblock29_Caption ;
      private String lblTextblock30_Caption ;
      private String lblTextblock31_Caption ;
      private String lblTextblock32_Caption ;
      private String lblTextblock33_Caption ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String ROClassString ;
      private String edtavCtlid_Jsonclick ;
      private String edtavCtlname_Jsonclick ;
      private String edtavCtlvalue_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock9_Internalname ;
      private String lblTextblock10_Internalname ;
      private String lblTextblock12_Internalname ;
      private String lblTextblock15_Internalname ;
      private String lblTextblock18_Internalname ;
      private String lblTextblock21_Internalname ;
      private String lblTextblock24_Internalname ;
      private String lblTextblock25_Internalname ;
      private String lblTextblock26_Internalname ;
      private String lblTextblock27_Internalname ;
      private String lblTextblock28_Internalname ;
      private String lblTextblock29_Internalname ;
      private String lblTextblock30_Internalname ;
      private String lblTextblock31_Internalname ;
      private String lblTextblock32_Internalname ;
      private String lblTextblock33_Internalname ;
      private String subGrid1_Internalname ;
      private String subGrid2_Class ;
      private String subGrid2_Linesclass ;
      private String lblTextblock5_Jsonclick ;
      private String edtavCtlid1_Jsonclick ;
      private String lblTextblock6_Jsonclick ;
      private String edtavCtlid2_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblock9_Jsonclick ;
      private String lblTextblock10_Jsonclick ;
      private String tblTable3_Internalname ;
      private String lblTextblock12_Jsonclick ;
      private String edtavCtlid4_Jsonclick ;
      private String tblTable4_Internalname ;
      private String lblTextblock15_Jsonclick ;
      private String edtavCtlid5_Jsonclick ;
      private String tblTable5_Internalname ;
      private String lblTextblock18_Jsonclick ;
      private String edtavCtlid6_Jsonclick ;
      private String tblTable6_Internalname ;
      private String lblTextblock21_Jsonclick ;
      private String edtavCtlid7_Jsonclick ;
      private String tblTable7_Internalname ;
      private String lblTextblock24_Jsonclick ;
      private String lblTextblock25_Jsonclick ;
      private String lblTextblock26_Jsonclick ;
      private String lblTextblock27_Jsonclick ;
      private String lblTextblock28_Jsonclick ;
      private String edtavCtldone_ratio_Jsonclick ;
      private String lblTextblock29_Jsonclick ;
      private String lblTextblock30_Jsonclick ;
      private String tblTable8_Internalname ;
      private String lblTextblock31_Jsonclick ;
      private String lblTextblock32_Jsonclick ;
      private String edtavCtlcreated_on_Jsonclick ;
      private String lblTextblock33_Jsonclick ;
      private String edtavCtlupdated_on_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n29Contratante_Codigo ;
      private bool gx_BV62 ;
      private GXWebGrid Grid2Container ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid2Row ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid2Column ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavProjeto ;
      private GXCombobox cmbavStatus ;
      private GXCombobox cmbavUsuario ;
      private IDataStoreProvider pr_default ;
      private int[] H00KE2_A29Contratante_Codigo ;
      private bool[] H00KE2_n29Contratante_Codigo ;
      private int[] H00KE2_A5AreaTrabalho_Codigo ;
      private int[] H00KE3_A1380Redmine_Codigo ;
      private int[] H00KE3_A1384Redmine_ContratanteCod ;
      private String[] H00KE3_A1381Redmine_Host ;
      private String[] H00KE3_A1382Redmine_Url ;
      private String[] H00KE3_A1383Redmine_Key ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpClient AV7httpclient ;
      private GXWebForm Form ;
      private SdtSDT_Redmineprojects AV12SDT_Project ;
      private SdtSDT_Redmineprojects_project AV9ProjectItem ;
      private SdtSDT_Redmine144_issues AV11SDT_Issues ;
      private SdtSDT_RedmineStatus AV13SDT_Status ;
      private SdtSDT_RedmineStatus_issue_status AV16StatusItem ;
      private SdtSDT_RedmineUsers AV14SDT_Usuarios ;
      private SdtSDT_RedmineUsers_user AV18UsuariosItem ;
      private wwpbaseobjects.SdtWWPContext AV20WWPContext ;
   }

   public class wp_redmine144__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KE2 ;
          prmH00KE2 = new Object[] {
          new Object[] {"@AV20WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KE3 ;
          prmH00KE3 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KE2", "SELECT TOP 1 [Contratante_Codigo], [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV20WWPC_1Areatrabalho_codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KE2,1,0,true,true )
             ,new CursorDef("H00KE3", "SELECT TOP 1 [Redmine_Codigo], [Redmine_ContratanteCod], [Redmine_Host], [Redmine_Url], [Redmine_Key] FROM [Redmine] WITH (NOLOCK) WHERE [Redmine_ContratanteCod] = @Contratante_Codigo ORDER BY [Redmine_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KE3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
