/*
               File: PRC_GamUserDataCriacao
        Description: PRC_Gam User Data Criacao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:56.68
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_gamuserdatacriacao : GXProcedure
   {
      public prc_gamuserdatacriacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_gamuserdatacriacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo ,
                           out DateTime aP1_Usuario_DataCriacao )
      {
         this.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV11Usuario_DataCriacao = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP1_Usuario_DataCriacao=this.AV11Usuario_DataCriacao;
      }

      public DateTime executeUdp( int aP0_Usuario_Codigo )
      {
         this.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV11Usuario_DataCriacao = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP1_Usuario_DataCriacao=this.AV11Usuario_DataCriacao;
         return AV11Usuario_DataCriacao ;
      }

      public void executeSubmit( int aP0_Usuario_Codigo ,
                                 out DateTime aP1_Usuario_DataCriacao )
      {
         prc_gamuserdatacriacao objprc_gamuserdatacriacao;
         objprc_gamuserdatacriacao = new prc_gamuserdatacriacao();
         objprc_gamuserdatacriacao.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_gamuserdatacriacao.AV11Usuario_DataCriacao = DateTime.MinValue ;
         objprc_gamuserdatacriacao.context.SetSubmitInitialConfig(context);
         objprc_gamuserdatacriacao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_gamuserdatacriacao);
         aP1_Usuario_DataCriacao=this.AV11Usuario_DataCriacao;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_gamuserdatacriacao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00DA2 */
         pr_default.execute(0, new Object[] {AV8Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1Usuario_Codigo = P00DA2_A1Usuario_Codigo[0];
            A341Usuario_UserGamGuid = P00DA2_A341Usuario_UserGamGuid[0];
            AV9GamUser.load( A341Usuario_UserGamGuid);
            AV11Usuario_DataCriacao = AV9GamUser.gxTpr_Datecreated;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00DA2_A1Usuario_Codigo = new int[1] ;
         P00DA2_A341Usuario_UserGamGuid = new String[] {""} ;
         A341Usuario_UserGamGuid = "";
         AV9GamUser = new SdtGAMUser(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_gamuserdatacriacao__default(),
            new Object[][] {
                new Object[] {
               P00DA2_A1Usuario_Codigo, P00DA2_A341Usuario_UserGamGuid
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Usuario_Codigo ;
      private int A1Usuario_Codigo ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private DateTime AV11Usuario_DataCriacao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00DA2_A1Usuario_Codigo ;
      private String[] P00DA2_A341Usuario_UserGamGuid ;
      private DateTime aP1_Usuario_DataCriacao ;
      private SdtGAMUser AV9GamUser ;
   }

   public class prc_gamuserdatacriacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DA2 ;
          prmP00DA2 = new Object[] {
          new Object[] {"@AV8Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DA2", "SELECT [Usuario_Codigo], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV8Usuario_Codigo ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DA2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
