/*
               File: type_SdtTmp_File
        Description: Arquivo a processar:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:2:30.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Tmp_File" )]
   [XmlType(TypeName =  "Tmp_File" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtTmp_File : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtTmp_File( )
      {
         /* Constructor for serialization */
         gxTv_SdtTmp_File_File_col1 = "";
         gxTv_SdtTmp_File_File_col2 = "";
         gxTv_SdtTmp_File_File_col3 = "";
         gxTv_SdtTmp_File_Mode = "";
         gxTv_SdtTmp_File_File_col1_Z = "";
         gxTv_SdtTmp_File_File_col2_Z = "";
         gxTv_SdtTmp_File_File_col3_Z = "";
      }

      public SdtTmp_File( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV435File_UsuarioCod ,
                        int AV504File_Row )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV435File_UsuarioCod,(int)AV504File_Row});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"File_UsuarioCod", typeof(int)}, new Object[]{"File_Row", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Tmp_File");
         metadata.Set("BT", "Tmp_File");
         metadata.Set("PK", "[ \"File_UsuarioCod\",\"File_Row\" ]");
         metadata.Set("PKAssigned", "[ \"File_UsuarioCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"File_UsuarioCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_File_usuariocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_File_row_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_File_col1_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_File_col2_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_File_col3_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_File_col1_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_File_col2_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_File_col3_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtTmp_File deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtTmp_File)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtTmp_File obj ;
         obj = this;
         obj.gxTpr_File_usuariocod = deserialized.gxTpr_File_usuariocod;
         obj.gxTpr_File_row = deserialized.gxTpr_File_row;
         obj.gxTpr_File_col1 = deserialized.gxTpr_File_col1;
         obj.gxTpr_File_col2 = deserialized.gxTpr_File_col2;
         obj.gxTpr_File_col3 = deserialized.gxTpr_File_col3;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_File_usuariocod_Z = deserialized.gxTpr_File_usuariocod_Z;
         obj.gxTpr_File_row_Z = deserialized.gxTpr_File_row_Z;
         obj.gxTpr_File_col1_Z = deserialized.gxTpr_File_col1_Z;
         obj.gxTpr_File_col2_Z = deserialized.gxTpr_File_col2_Z;
         obj.gxTpr_File_col3_Z = deserialized.gxTpr_File_col3_Z;
         obj.gxTpr_File_col1_N = deserialized.gxTpr_File_col1_N;
         obj.gxTpr_File_col2_N = deserialized.gxTpr_File_col2_N;
         obj.gxTpr_File_col3_N = deserialized.gxTpr_File_col3_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_UsuarioCod") )
               {
                  gxTv_SdtTmp_File_File_usuariocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_Row") )
               {
                  gxTv_SdtTmp_File_File_row = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_Col1") )
               {
                  gxTv_SdtTmp_File_File_col1 = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_Col2") )
               {
                  gxTv_SdtTmp_File_File_col2 = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_Col3") )
               {
                  gxTv_SdtTmp_File_File_col3 = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtTmp_File_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtTmp_File_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_UsuarioCod_Z") )
               {
                  gxTv_SdtTmp_File_File_usuariocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_Row_Z") )
               {
                  gxTv_SdtTmp_File_File_row_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_Col1_Z") )
               {
                  gxTv_SdtTmp_File_File_col1_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_Col2_Z") )
               {
                  gxTv_SdtTmp_File_File_col2_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_Col3_Z") )
               {
                  gxTv_SdtTmp_File_File_col3_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_Col1_N") )
               {
                  gxTv_SdtTmp_File_File_col1_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_Col2_N") )
               {
                  gxTv_SdtTmp_File_File_col2_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "File_Col3_N") )
               {
                  gxTv_SdtTmp_File_File_col3_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Tmp_File";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("File_UsuarioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTmp_File_File_usuariocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("File_Row", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTmp_File_File_row), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("File_Col1", StringUtil.RTrim( gxTv_SdtTmp_File_File_col1));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("File_Col2", StringUtil.RTrim( gxTv_SdtTmp_File_File_col2));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("File_Col3", StringUtil.RTrim( gxTv_SdtTmp_File_File_col3));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtTmp_File_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTmp_File_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("File_UsuarioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTmp_File_File_usuariocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("File_Row_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTmp_File_File_row_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("File_Col1_Z", StringUtil.RTrim( gxTv_SdtTmp_File_File_col1_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("File_Col2_Z", StringUtil.RTrim( gxTv_SdtTmp_File_File_col2_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("File_Col3_Z", StringUtil.RTrim( gxTv_SdtTmp_File_File_col3_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("File_Col1_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTmp_File_File_col1_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("File_Col2_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTmp_File_File_col2_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("File_Col3_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTmp_File_File_col3_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("File_UsuarioCod", gxTv_SdtTmp_File_File_usuariocod, false);
         AddObjectProperty("File_Row", gxTv_SdtTmp_File_File_row, false);
         AddObjectProperty("File_Col1", gxTv_SdtTmp_File_File_col1, false);
         AddObjectProperty("File_Col2", gxTv_SdtTmp_File_File_col2, false);
         AddObjectProperty("File_Col3", gxTv_SdtTmp_File_File_col3, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtTmp_File_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtTmp_File_Initialized, false);
            AddObjectProperty("File_UsuarioCod_Z", gxTv_SdtTmp_File_File_usuariocod_Z, false);
            AddObjectProperty("File_Row_Z", gxTv_SdtTmp_File_File_row_Z, false);
            AddObjectProperty("File_Col1_Z", gxTv_SdtTmp_File_File_col1_Z, false);
            AddObjectProperty("File_Col2_Z", gxTv_SdtTmp_File_File_col2_Z, false);
            AddObjectProperty("File_Col3_Z", gxTv_SdtTmp_File_File_col3_Z, false);
            AddObjectProperty("File_Col1_N", gxTv_SdtTmp_File_File_col1_N, false);
            AddObjectProperty("File_Col2_N", gxTv_SdtTmp_File_File_col2_N, false);
            AddObjectProperty("File_Col3_N", gxTv_SdtTmp_File_File_col3_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "File_UsuarioCod" )]
      [  XmlElement( ElementName = "File_UsuarioCod"   )]
      public int gxTpr_File_usuariocod
      {
         get {
            return gxTv_SdtTmp_File_File_usuariocod ;
         }

         set {
            if ( gxTv_SdtTmp_File_File_usuariocod != value )
            {
               gxTv_SdtTmp_File_Mode = "INS";
               this.gxTv_SdtTmp_File_File_usuariocod_Z_SetNull( );
               this.gxTv_SdtTmp_File_File_row_Z_SetNull( );
               this.gxTv_SdtTmp_File_File_col1_Z_SetNull( );
               this.gxTv_SdtTmp_File_File_col2_Z_SetNull( );
               this.gxTv_SdtTmp_File_File_col3_Z_SetNull( );
            }
            gxTv_SdtTmp_File_File_usuariocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "File_Row" )]
      [  XmlElement( ElementName = "File_Row"   )]
      public int gxTpr_File_row
      {
         get {
            return gxTv_SdtTmp_File_File_row ;
         }

         set {
            if ( gxTv_SdtTmp_File_File_row != value )
            {
               gxTv_SdtTmp_File_Mode = "INS";
               this.gxTv_SdtTmp_File_File_usuariocod_Z_SetNull( );
               this.gxTv_SdtTmp_File_File_row_Z_SetNull( );
               this.gxTv_SdtTmp_File_File_col1_Z_SetNull( );
               this.gxTv_SdtTmp_File_File_col2_Z_SetNull( );
               this.gxTv_SdtTmp_File_File_col3_Z_SetNull( );
            }
            gxTv_SdtTmp_File_File_row = (int)(value);
         }

      }

      [  SoapElement( ElementName = "File_Col1" )]
      [  XmlElement( ElementName = "File_Col1"   )]
      public String gxTpr_File_col1
      {
         get {
            return gxTv_SdtTmp_File_File_col1 ;
         }

         set {
            gxTv_SdtTmp_File_File_col1_N = 0;
            gxTv_SdtTmp_File_File_col1 = (String)(value);
         }

      }

      public void gxTv_SdtTmp_File_File_col1_SetNull( )
      {
         gxTv_SdtTmp_File_File_col1_N = 1;
         gxTv_SdtTmp_File_File_col1 = "";
         return  ;
      }

      public bool gxTv_SdtTmp_File_File_col1_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "File_Col2" )]
      [  XmlElement( ElementName = "File_Col2"   )]
      public String gxTpr_File_col2
      {
         get {
            return gxTv_SdtTmp_File_File_col2 ;
         }

         set {
            gxTv_SdtTmp_File_File_col2_N = 0;
            gxTv_SdtTmp_File_File_col2 = (String)(value);
         }

      }

      public void gxTv_SdtTmp_File_File_col2_SetNull( )
      {
         gxTv_SdtTmp_File_File_col2_N = 1;
         gxTv_SdtTmp_File_File_col2 = "";
         return  ;
      }

      public bool gxTv_SdtTmp_File_File_col2_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "File_Col3" )]
      [  XmlElement( ElementName = "File_Col3"   )]
      public String gxTpr_File_col3
      {
         get {
            return gxTv_SdtTmp_File_File_col3 ;
         }

         set {
            gxTv_SdtTmp_File_File_col3_N = 0;
            gxTv_SdtTmp_File_File_col3 = (String)(value);
         }

      }

      public void gxTv_SdtTmp_File_File_col3_SetNull( )
      {
         gxTv_SdtTmp_File_File_col3_N = 1;
         gxTv_SdtTmp_File_File_col3 = "";
         return  ;
      }

      public bool gxTv_SdtTmp_File_File_col3_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtTmp_File_Mode ;
         }

         set {
            gxTv_SdtTmp_File_Mode = (String)(value);
         }

      }

      public void gxTv_SdtTmp_File_Mode_SetNull( )
      {
         gxTv_SdtTmp_File_Mode = "";
         return  ;
      }

      public bool gxTv_SdtTmp_File_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtTmp_File_Initialized ;
         }

         set {
            gxTv_SdtTmp_File_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtTmp_File_Initialized_SetNull( )
      {
         gxTv_SdtTmp_File_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtTmp_File_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "File_UsuarioCod_Z" )]
      [  XmlElement( ElementName = "File_UsuarioCod_Z"   )]
      public int gxTpr_File_usuariocod_Z
      {
         get {
            return gxTv_SdtTmp_File_File_usuariocod_Z ;
         }

         set {
            gxTv_SdtTmp_File_File_usuariocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtTmp_File_File_usuariocod_Z_SetNull( )
      {
         gxTv_SdtTmp_File_File_usuariocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtTmp_File_File_usuariocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "File_Row_Z" )]
      [  XmlElement( ElementName = "File_Row_Z"   )]
      public int gxTpr_File_row_Z
      {
         get {
            return gxTv_SdtTmp_File_File_row_Z ;
         }

         set {
            gxTv_SdtTmp_File_File_row_Z = (int)(value);
         }

      }

      public void gxTv_SdtTmp_File_File_row_Z_SetNull( )
      {
         gxTv_SdtTmp_File_File_row_Z = 0;
         return  ;
      }

      public bool gxTv_SdtTmp_File_File_row_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "File_Col1_Z" )]
      [  XmlElement( ElementName = "File_Col1_Z"   )]
      public String gxTpr_File_col1_Z
      {
         get {
            return gxTv_SdtTmp_File_File_col1_Z ;
         }

         set {
            gxTv_SdtTmp_File_File_col1_Z = (String)(value);
         }

      }

      public void gxTv_SdtTmp_File_File_col1_Z_SetNull( )
      {
         gxTv_SdtTmp_File_File_col1_Z = "";
         return  ;
      }

      public bool gxTv_SdtTmp_File_File_col1_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "File_Col2_Z" )]
      [  XmlElement( ElementName = "File_Col2_Z"   )]
      public String gxTpr_File_col2_Z
      {
         get {
            return gxTv_SdtTmp_File_File_col2_Z ;
         }

         set {
            gxTv_SdtTmp_File_File_col2_Z = (String)(value);
         }

      }

      public void gxTv_SdtTmp_File_File_col2_Z_SetNull( )
      {
         gxTv_SdtTmp_File_File_col2_Z = "";
         return  ;
      }

      public bool gxTv_SdtTmp_File_File_col2_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "File_Col3_Z" )]
      [  XmlElement( ElementName = "File_Col3_Z"   )]
      public String gxTpr_File_col3_Z
      {
         get {
            return gxTv_SdtTmp_File_File_col3_Z ;
         }

         set {
            gxTv_SdtTmp_File_File_col3_Z = (String)(value);
         }

      }

      public void gxTv_SdtTmp_File_File_col3_Z_SetNull( )
      {
         gxTv_SdtTmp_File_File_col3_Z = "";
         return  ;
      }

      public bool gxTv_SdtTmp_File_File_col3_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "File_Col1_N" )]
      [  XmlElement( ElementName = "File_Col1_N"   )]
      public short gxTpr_File_col1_N
      {
         get {
            return gxTv_SdtTmp_File_File_col1_N ;
         }

         set {
            gxTv_SdtTmp_File_File_col1_N = (short)(value);
         }

      }

      public void gxTv_SdtTmp_File_File_col1_N_SetNull( )
      {
         gxTv_SdtTmp_File_File_col1_N = 0;
         return  ;
      }

      public bool gxTv_SdtTmp_File_File_col1_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "File_Col2_N" )]
      [  XmlElement( ElementName = "File_Col2_N"   )]
      public short gxTpr_File_col2_N
      {
         get {
            return gxTv_SdtTmp_File_File_col2_N ;
         }

         set {
            gxTv_SdtTmp_File_File_col2_N = (short)(value);
         }

      }

      public void gxTv_SdtTmp_File_File_col2_N_SetNull( )
      {
         gxTv_SdtTmp_File_File_col2_N = 0;
         return  ;
      }

      public bool gxTv_SdtTmp_File_File_col2_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "File_Col3_N" )]
      [  XmlElement( ElementName = "File_Col3_N"   )]
      public short gxTpr_File_col3_N
      {
         get {
            return gxTv_SdtTmp_File_File_col3_N ;
         }

         set {
            gxTv_SdtTmp_File_File_col3_N = (short)(value);
         }

      }

      public void gxTv_SdtTmp_File_File_col3_N_SetNull( )
      {
         gxTv_SdtTmp_File_File_col3_N = 0;
         return  ;
      }

      public bool gxTv_SdtTmp_File_File_col3_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtTmp_File_File_col1 = "";
         gxTv_SdtTmp_File_File_col2 = "";
         gxTv_SdtTmp_File_File_col3 = "";
         gxTv_SdtTmp_File_Mode = "";
         gxTv_SdtTmp_File_File_col1_Z = "";
         gxTv_SdtTmp_File_File_col2_Z = "";
         gxTv_SdtTmp_File_File_col3_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "tmp_file", "GeneXus.Programs.tmp_file_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtTmp_File_Initialized ;
      private short gxTv_SdtTmp_File_File_col1_N ;
      private short gxTv_SdtTmp_File_File_col2_N ;
      private short gxTv_SdtTmp_File_File_col3_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtTmp_File_File_usuariocod ;
      private int gxTv_SdtTmp_File_File_row ;
      private int gxTv_SdtTmp_File_File_usuariocod_Z ;
      private int gxTv_SdtTmp_File_File_row_Z ;
      private String gxTv_SdtTmp_File_Mode ;
      private String sTagName ;
      private String gxTv_SdtTmp_File_File_col1 ;
      private String gxTv_SdtTmp_File_File_col2 ;
      private String gxTv_SdtTmp_File_File_col3 ;
      private String gxTv_SdtTmp_File_File_col1_Z ;
      private String gxTv_SdtTmp_File_File_col2_Z ;
      private String gxTv_SdtTmp_File_File_col3_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Tmp_File", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtTmp_File_RESTInterface : GxGenericCollectionItem<SdtTmp_File>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtTmp_File_RESTInterface( ) : base()
      {
      }

      public SdtTmp_File_RESTInterface( SdtTmp_File psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "File_UsuarioCod" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_File_usuariocod
      {
         get {
            return sdt.gxTpr_File_usuariocod ;
         }

         set {
            sdt.gxTpr_File_usuariocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "File_Row" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_File_row
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_File_row), 8, 0)) ;
         }

         set {
            sdt.gxTpr_File_row = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "File_Col1" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_File_col1
      {
         get {
            return sdt.gxTpr_File_col1 ;
         }

         set {
            sdt.gxTpr_File_col1 = (String)(value);
         }

      }

      [DataMember( Name = "File_Col2" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_File_col2
      {
         get {
            return sdt.gxTpr_File_col2 ;
         }

         set {
            sdt.gxTpr_File_col2 = (String)(value);
         }

      }

      [DataMember( Name = "File_Col3" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_File_col3
      {
         get {
            return sdt.gxTpr_File_col3 ;
         }

         set {
            sdt.gxTpr_File_col3 = (String)(value);
         }

      }

      public SdtTmp_File sdt
      {
         get {
            return (SdtTmp_File)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtTmp_File() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 15 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
