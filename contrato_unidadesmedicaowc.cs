/*
               File: Contrato_UnidadesMedicaoWC
        Description: Contrato_Unidades Medicao WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:54:26.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contrato_unidadesmedicaowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contrato_unidadesmedicaowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contrato_unidadesmedicaowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoUnidades_ContratoCod )
      {
         this.AV7ContratoUnidades_ContratoCod = aP0_ContratoUnidades_ContratoCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContratoUnidades_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoUnidades_ContratoCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContratoUnidades_ContratoCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_11 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_11_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_11_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV21TFContratoUnidades_UndMedNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoUnidades_UndMedNom", AV21TFContratoUnidades_UndMedNom);
                  AV22TFContratoUnidades_UndMedNom_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoUnidades_UndMedNom_Sel", AV22TFContratoUnidades_UndMedNom_Sel);
                  AV25TFContratoUnidades_Produtividade = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV25TFContratoUnidades_Produtividade, 14, 5)));
                  AV26TFContratoUnidades_Produtividade_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoUnidades_Produtividade_To, 14, 5)));
                  AV29TFContratoUnidades_UndMedSigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoUnidades_UndMedSigla", AV29TFContratoUnidades_UndMedSigla);
                  AV30TFContratoUnidades_UndMedSigla_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoUnidades_UndMedSigla_Sel", AV30TFContratoUnidades_UndMedSigla_Sel);
                  AV7ContratoUnidades_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoUnidades_ContratoCod), 6, 0)));
                  AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace", AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace);
                  AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace", AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace);
                  AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace", AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  AV38Pgmname = GetNextPar( );
                  A1207ContratoUnidades_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
                  A1204ContratoUnidades_UndMedCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV17Unidades);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV21TFContratoUnidades_UndMedNom, AV22TFContratoUnidades_UndMedNom_Sel, AV25TFContratoUnidades_Produtividade, AV26TFContratoUnidades_Produtividade_To, AV29TFContratoUnidades_UndMedSigla, AV30TFContratoUnidades_UndMedSigla_Sel, AV7ContratoUnidades_ContratoCod, AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV6WWPContext, AV38Pgmname, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod, AV17Unidades, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAIQ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV38Pgmname = "Contrato_UnidadesMedicaoWC";
               context.Gx_err = 0;
               WSIQ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato_Unidades Medicao WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812542623");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contrato_unidadesmedicaowc.aspx") + "?" + UrlEncode("" +AV7ContratoUnidades_ContratoCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOUNIDADES_UNDMEDNOM", StringUtil.RTrim( AV21TFContratoUnidades_UndMedNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOUNIDADES_UNDMEDNOM_SEL", StringUtil.RTrim( AV22TFContratoUnidades_UndMedNom_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE", StringUtil.LTrim( StringUtil.NToC( AV25TFContratoUnidades_Produtividade, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO", StringUtil.LTrim( StringUtil.NToC( AV26TFContratoUnidades_Produtividade_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA", StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL", StringUtil.RTrim( AV30TFContratoUnidades_UndMedSigla_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_11", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_11), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV32DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV32DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA", AV20ContratoUnidades_UndMedNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA", AV20ContratoUnidades_UndMedNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA", AV24ContratoUnidades_ProdutividadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA", AV24ContratoUnidades_ProdutividadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA", AV28ContratoUnidades_UndMedSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA", AV28ContratoUnidades_UndMedSiglaTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContratoUnidades_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOUNIDADES_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoUnidades_ContratoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV38Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOUNIDADES_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOUNIDADES_UNDMEDCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vUNIDADES", AV17Unidades);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vUNIDADES", AV17Unidades);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"UNIDADEMEDICAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1197UnidadeMedicao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Width", StringUtil.RTrim( Confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Icon", StringUtil.RTrim( Confirmpanel_Icon));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmtext", StringUtil.RTrim( Confirmpanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Caption", StringUtil.RTrim( Ddo_contratounidades_undmednom_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Tooltip", StringUtil.RTrim( Ddo_contratounidades_undmednom_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Cls", StringUtil.RTrim( Ddo_contratounidades_undmednom_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratounidades_undmednom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratounidades_undmednom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratounidades_undmednom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratounidades_undmednom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortedstatus", StringUtil.RTrim( Ddo_contratounidades_undmednom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Includefilter", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Filtertype", StringUtil.RTrim( Ddo_contratounidades_undmednom_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratounidades_undmednom_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalisttype", StringUtil.RTrim( Ddo_contratounidades_undmednom_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistproc", StringUtil.RTrim( Ddo_contratounidades_undmednom_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratounidades_undmednom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortasc", StringUtil.RTrim( Ddo_contratounidades_undmednom_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortdsc", StringUtil.RTrim( Ddo_contratounidades_undmednom_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Loadingdata", StringUtil.RTrim( Ddo_contratounidades_undmednom_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Cleanfilter", StringUtil.RTrim( Ddo_contratounidades_undmednom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Noresultsfound", StringUtil.RTrim( Ddo_contratounidades_undmednom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratounidades_undmednom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Caption", StringUtil.RTrim( Ddo_contratounidades_produtividade_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Tooltip", StringUtil.RTrim( Ddo_contratounidades_produtividade_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Cls", StringUtil.RTrim( Ddo_contratounidades_produtividade_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtext_set", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtextto_set", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratounidades_produtividade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratounidades_produtividade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortedstatus", StringUtil.RTrim( Ddo_contratounidades_produtividade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includefilter", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filtertype", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_contratounidades_produtividade_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortasc", StringUtil.RTrim( Ddo_contratounidades_produtividade_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortdsc", StringUtil.RTrim( Ddo_contratounidades_produtividade_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Cleanfilter", StringUtil.RTrim( Ddo_contratounidades_produtividade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Rangefilterfrom", StringUtil.RTrim( Ddo_contratounidades_produtividade_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Rangefilterto", StringUtil.RTrim( Ddo_contratounidades_produtividade_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_contratounidades_produtividade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Caption", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Tooltip", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Cls", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortedstatus", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filtertype", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_contratounidades_undmedsigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalisttype", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistproc", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratounidades_undmedsigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortasc", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortdsc", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Loadingdata", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Activeeventkey", StringUtil.RTrim( Ddo_contratounidades_undmednom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratounidades_undmednom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratounidades_undmednom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Activeeventkey", StringUtil.RTrim( Ddo_contratounidades_produtividade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtextto_get", StringUtil.RTrim( Ddo_contratounidades_produtividade_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratounidades_undmedsigla_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormIQ2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contrato_unidadesmedicaowc.js", "?202051812542710");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "Contrato_UnidadesMedicaoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato_Unidades Medicao WC" ;
      }

      protected void WBIQ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contrato_unidadesmedicaowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
               context.AddJavascriptSource("DVelop/Shared/dom.js", "");
               context.AddJavascriptSource("DVelop/Shared/event.js", "");
               context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
               context.AddJavascriptSource("DVelop/Shared/container.js", "");
               context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_IQ2( true) ;
         }
         else
         {
            wb_table1_2_IQ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_IQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,26);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contrato_UnidadesMedicaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_Contrato_UnidadesMedicaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmednom_Internalname, StringUtil.RTrim( AV21TFContratoUnidades_UndMedNom), StringUtil.RTrim( context.localUtil.Format( AV21TFContratoUnidades_UndMedNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmednom_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmednom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contrato_UnidadesMedicaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmednom_sel_Internalname, StringUtil.RTrim( AV22TFContratoUnidades_UndMedNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV22TFContratoUnidades_UndMedNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,29);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmednom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmednom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contrato_UnidadesMedicaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_produtividade_Internalname, StringUtil.LTrim( StringUtil.NToC( AV25TFContratoUnidades_Produtividade, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV25TFContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,30);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_produtividade_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_produtividade_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contrato_UnidadesMedicaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_produtividade_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV26TFContratoUnidades_Produtividade_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV26TFContratoUnidades_Produtividade_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,31);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_produtividade_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_produtividade_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contrato_UnidadesMedicaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmedsigla_Internalname, StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla), StringUtil.RTrim( context.localUtil.Format( AV29TFContratoUnidades_UndMedSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,32);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmedsigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmedsigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contrato_UnidadesMedicaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratounidades_undmedsigla_sel_Internalname, StringUtil.RTrim( AV30TFContratoUnidades_UndMedSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV30TFContratoUnidades_UndMedSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratounidades_undmedsigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratounidades_undmedsigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contrato_UnidadesMedicaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname, AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", 0, edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Contrato_UnidadesMedicaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname, AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", 0, edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Contrato_UnidadesMedicaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_11_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname, AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", 0, edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Contrato_UnidadesMedicaoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTIQ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato_Unidades Medicao WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPIQ0( ) ;
            }
         }
      }

      protected void WSIQ2( )
      {
         STARTIQ2( ) ;
         EVTIQ2( ) ;
      }

      protected void EVTIQ2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11IQ2 */
                                    E11IQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOUNIDADES_UNDMEDNOM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12IQ2 */
                                    E12IQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOUNIDADES_PRODUTIVIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13IQ2 */
                                    E13IQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOUNIDADES_UNDMEDSIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14IQ2 */
                                    E14IQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15IQ2 */
                                    E15IQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIQ0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPIQ0( ) ;
                              }
                              nGXsfl_11_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
                              SubsflControlProps_112( ) ;
                              AV16Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16Update)) ? AV35Update_GXI : context.convertURL( context.PathToRelativeUrl( AV16Update))));
                              AV19Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV19Delete)) ? AV36Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV19Delete))));
                              A1205ContratoUnidades_UndMedNom = StringUtil.Upper( cgiGet( edtContratoUnidades_UndMedNom_Internalname));
                              n1205ContratoUnidades_UndMedNom = false;
                              A1208ContratoUnidades_Produtividade = context.localUtil.CToN( cgiGet( edtContratoUnidades_Produtividade_Internalname), ",", ".");
                              n1208ContratoUnidades_Produtividade = false;
                              A1206ContratoUnidades_UndMedSigla = StringUtil.Upper( cgiGet( edtContratoUnidades_UndMedSigla_Internalname));
                              n1206ContratoUnidades_UndMedSigla = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16IQ2 */
                                          E16IQ2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17IQ2 */
                                          E17IQ2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18IQ2 */
                                          E18IQ2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratounidades_undmednom Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOUNIDADES_UNDMEDNOM"), AV21TFContratoUnidades_UndMedNom) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratounidades_undmednom_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOUNIDADES_UNDMEDNOM_SEL"), AV22TFContratoUnidades_UndMedNom_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratounidades_produtividade Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE"), ",", ".") != AV25TFContratoUnidades_Produtividade )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratounidades_produtividade_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO"), ",", ".") != AV26TFContratoUnidades_Produtividade_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratounidades_undmedsigla Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA"), AV29TFContratoUnidades_UndMedSigla) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratounidades_undmedsigla_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL"), AV30TFContratoUnidades_UndMedSigla_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPIQ0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEIQ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormIQ2( ) ;
            }
         }
      }

      protected void PAIQ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_112( ) ;
         while ( nGXsfl_11_idx <= nRC_GXsfl_11 )
         {
            sendrow_112( ) ;
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV21TFContratoUnidades_UndMedNom ,
                                       String AV22TFContratoUnidades_UndMedNom_Sel ,
                                       decimal AV25TFContratoUnidades_Produtividade ,
                                       decimal AV26TFContratoUnidades_Produtividade_To ,
                                       String AV29TFContratoUnidades_UndMedSigla ,
                                       String AV30TFContratoUnidades_UndMedSigla_Sel ,
                                       int AV7ContratoUnidades_ContratoCod ,
                                       String AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace ,
                                       String AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace ,
                                       String AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV38Pgmname ,
                                       int A1207ContratoUnidades_ContratoCod ,
                                       int A1204ContratoUnidades_UndMedCod ,
                                       IGxCollection AV17Unidades ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFIQ2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOUNIDADES_PRODUTIVIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOUNIDADES_PRODUTIVIDADE", StringUtil.LTrim( StringUtil.NToC( A1208ContratoUnidades_Produtividade, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFIQ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV38Pgmname = "Contrato_UnidadesMedicaoWC";
         context.Gx_err = 0;
      }

      protected void RFIQ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 11;
         /* Execute user event: E17IQ2 */
         E17IQ2 ();
         nGXsfl_11_idx = 1;
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         nGXsfl_11_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_112( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV22TFContratoUnidades_UndMedNom_Sel ,
                                                 AV21TFContratoUnidades_UndMedNom ,
                                                 AV25TFContratoUnidades_Produtividade ,
                                                 AV26TFContratoUnidades_Produtividade_To ,
                                                 AV30TFContratoUnidades_UndMedSigla_Sel ,
                                                 AV29TFContratoUnidades_UndMedSigla ,
                                                 A1205ContratoUnidades_UndMedNom ,
                                                 A1208ContratoUnidades_Produtividade ,
                                                 A1206ContratoUnidades_UndMedSigla ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A1207ContratoUnidades_ContratoCod ,
                                                 AV7ContratoUnidades_ContratoCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV21TFContratoUnidades_UndMedNom = StringUtil.PadR( StringUtil.RTrim( AV21TFContratoUnidades_UndMedNom), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoUnidades_UndMedNom", AV21TFContratoUnidades_UndMedNom);
            lV29TFContratoUnidades_UndMedSigla = StringUtil.PadR( StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoUnidades_UndMedSigla", AV29TFContratoUnidades_UndMedSigla);
            /* Using cursor H00IQ2 */
            pr_default.execute(0, new Object[] {AV7ContratoUnidades_ContratoCod, lV21TFContratoUnidades_UndMedNom, AV22TFContratoUnidades_UndMedNom_Sel, AV25TFContratoUnidades_Produtividade, AV26TFContratoUnidades_Produtividade_To, lV29TFContratoUnidades_UndMedSigla, AV30TFContratoUnidades_UndMedSigla_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_11_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1204ContratoUnidades_UndMedCod = H00IQ2_A1204ContratoUnidades_UndMedCod[0];
               A1207ContratoUnidades_ContratoCod = H00IQ2_A1207ContratoUnidades_ContratoCod[0];
               A1206ContratoUnidades_UndMedSigla = H00IQ2_A1206ContratoUnidades_UndMedSigla[0];
               n1206ContratoUnidades_UndMedSigla = H00IQ2_n1206ContratoUnidades_UndMedSigla[0];
               A1208ContratoUnidades_Produtividade = H00IQ2_A1208ContratoUnidades_Produtividade[0];
               n1208ContratoUnidades_Produtividade = H00IQ2_n1208ContratoUnidades_Produtividade[0];
               A1205ContratoUnidades_UndMedNom = H00IQ2_A1205ContratoUnidades_UndMedNom[0];
               n1205ContratoUnidades_UndMedNom = H00IQ2_n1205ContratoUnidades_UndMedNom[0];
               A1206ContratoUnidades_UndMedSigla = H00IQ2_A1206ContratoUnidades_UndMedSigla[0];
               n1206ContratoUnidades_UndMedSigla = H00IQ2_n1206ContratoUnidades_UndMedSigla[0];
               A1205ContratoUnidades_UndMedNom = H00IQ2_A1205ContratoUnidades_UndMedNom[0];
               n1205ContratoUnidades_UndMedNom = H00IQ2_n1205ContratoUnidades_UndMedNom[0];
               /* Execute user event: E18IQ2 */
               E18IQ2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 11;
            WBIQ0( ) ;
         }
         nGXsfl_11_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV22TFContratoUnidades_UndMedNom_Sel ,
                                              AV21TFContratoUnidades_UndMedNom ,
                                              AV25TFContratoUnidades_Produtividade ,
                                              AV26TFContratoUnidades_Produtividade_To ,
                                              AV30TFContratoUnidades_UndMedSigla_Sel ,
                                              AV29TFContratoUnidades_UndMedSigla ,
                                              A1205ContratoUnidades_UndMedNom ,
                                              A1208ContratoUnidades_Produtividade ,
                                              A1206ContratoUnidades_UndMedSigla ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A1207ContratoUnidades_ContratoCod ,
                                              AV7ContratoUnidades_ContratoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV21TFContratoUnidades_UndMedNom = StringUtil.PadR( StringUtil.RTrim( AV21TFContratoUnidades_UndMedNom), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoUnidades_UndMedNom", AV21TFContratoUnidades_UndMedNom);
         lV29TFContratoUnidades_UndMedSigla = StringUtil.PadR( StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoUnidades_UndMedSigla", AV29TFContratoUnidades_UndMedSigla);
         /* Using cursor H00IQ3 */
         pr_default.execute(1, new Object[] {AV7ContratoUnidades_ContratoCod, lV21TFContratoUnidades_UndMedNom, AV22TFContratoUnidades_UndMedNom_Sel, AV25TFContratoUnidades_Produtividade, AV26TFContratoUnidades_Produtividade_To, lV29TFContratoUnidades_UndMedSigla, AV30TFContratoUnidades_UndMedSigla_Sel});
         GRID_nRecordCount = H00IQ3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV21TFContratoUnidades_UndMedNom, AV22TFContratoUnidades_UndMedNom_Sel, AV25TFContratoUnidades_Produtividade, AV26TFContratoUnidades_Produtividade_To, AV29TFContratoUnidades_UndMedSigla, AV30TFContratoUnidades_UndMedSigla_Sel, AV7ContratoUnidades_ContratoCod, AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV6WWPContext, AV38Pgmname, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod, AV17Unidades, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV21TFContratoUnidades_UndMedNom, AV22TFContratoUnidades_UndMedNom_Sel, AV25TFContratoUnidades_Produtividade, AV26TFContratoUnidades_Produtividade_To, AV29TFContratoUnidades_UndMedSigla, AV30TFContratoUnidades_UndMedSigla_Sel, AV7ContratoUnidades_ContratoCod, AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV6WWPContext, AV38Pgmname, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod, AV17Unidades, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV21TFContratoUnidades_UndMedNom, AV22TFContratoUnidades_UndMedNom_Sel, AV25TFContratoUnidades_Produtividade, AV26TFContratoUnidades_Produtividade_To, AV29TFContratoUnidades_UndMedSigla, AV30TFContratoUnidades_UndMedSigla_Sel, AV7ContratoUnidades_ContratoCod, AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV6WWPContext, AV38Pgmname, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod, AV17Unidades, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV21TFContratoUnidades_UndMedNom, AV22TFContratoUnidades_UndMedNom_Sel, AV25TFContratoUnidades_Produtividade, AV26TFContratoUnidades_Produtividade_To, AV29TFContratoUnidades_UndMedSigla, AV30TFContratoUnidades_UndMedSigla_Sel, AV7ContratoUnidades_ContratoCod, AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV6WWPContext, AV38Pgmname, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod, AV17Unidades, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV21TFContratoUnidades_UndMedNom, AV22TFContratoUnidades_UndMedNom_Sel, AV25TFContratoUnidades_Produtividade, AV26TFContratoUnidades_Produtividade_To, AV29TFContratoUnidades_UndMedSigla, AV30TFContratoUnidades_UndMedSigla_Sel, AV7ContratoUnidades_ContratoCod, AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, AV6WWPContext, AV38Pgmname, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod, AV17Unidades, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPIQ0( )
      {
         /* Before Start, stand alone formulas. */
         AV38Pgmname = "Contrato_UnidadesMedicaoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E16IQ2 */
         E16IQ2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV32DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA"), AV20ContratoUnidades_UndMedNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA"), AV24ContratoUnidades_ProdutividadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA"), AV28ContratoUnidades_UndMedSiglaTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            AV21TFContratoUnidades_UndMedNom = StringUtil.Upper( cgiGet( edtavTfcontratounidades_undmednom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoUnidades_UndMedNom", AV21TFContratoUnidades_UndMedNom);
            AV22TFContratoUnidades_UndMedNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratounidades_undmednom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoUnidades_UndMedNom_Sel", AV22TFContratoUnidades_UndMedNom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOUNIDADES_PRODUTIVIDADE");
               GX_FocusControl = edtavTfcontratounidades_produtividade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25TFContratoUnidades_Produtividade = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV25TFContratoUnidades_Produtividade, 14, 5)));
            }
            else
            {
               AV25TFContratoUnidades_Produtividade = context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV25TFContratoUnidades_Produtividade, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO");
               GX_FocusControl = edtavTfcontratounidades_produtividade_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26TFContratoUnidades_Produtividade_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoUnidades_Produtividade_To, 14, 5)));
            }
            else
            {
               AV26TFContratoUnidades_Produtividade_To = context.localUtil.CToN( cgiGet( edtavTfcontratounidades_produtividade_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoUnidades_Produtividade_To, 14, 5)));
            }
            AV29TFContratoUnidades_UndMedSigla = StringUtil.Upper( cgiGet( edtavTfcontratounidades_undmedsigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoUnidades_UndMedSigla", AV29TFContratoUnidades_UndMedSigla);
            AV30TFContratoUnidades_UndMedSigla_Sel = StringUtil.Upper( cgiGet( edtavTfcontratounidades_undmedsigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoUnidades_UndMedSigla_Sel", AV30TFContratoUnidades_UndMedSigla_Sel);
            AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace = cgiGet( edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace", AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace);
            AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace = cgiGet( edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace", AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace);
            AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace = cgiGet( edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace", AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_11"), ",", "."));
            wcpOAV7ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoUnidades_ContratoCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Confirmpanel_Width = cgiGet( sPrefix+"CONFIRMPANEL_Width");
            Confirmpanel_Title = cgiGet( sPrefix+"CONFIRMPANEL_Title");
            Confirmpanel_Icon = cgiGet( sPrefix+"CONFIRMPANEL_Icon");
            Confirmpanel_Confirmtext = cgiGet( sPrefix+"CONFIRMPANEL_Confirmtext");
            Confirmpanel_Confirmtype = cgiGet( sPrefix+"CONFIRMPANEL_Confirmtype");
            Ddo_contratounidades_undmednom_Caption = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Caption");
            Ddo_contratounidades_undmednom_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Tooltip");
            Ddo_contratounidades_undmednom_Cls = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Cls");
            Ddo_contratounidades_undmednom_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Filteredtext_set");
            Ddo_contratounidades_undmednom_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Selectedvalue_set");
            Ddo_contratounidades_undmednom_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Dropdownoptionstype");
            Ddo_contratounidades_undmednom_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Titlecontrolidtoreplace");
            Ddo_contratounidades_undmednom_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Includesortasc"));
            Ddo_contratounidades_undmednom_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Includesortdsc"));
            Ddo_contratounidades_undmednom_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortedstatus");
            Ddo_contratounidades_undmednom_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Includefilter"));
            Ddo_contratounidades_undmednom_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Filtertype");
            Ddo_contratounidades_undmednom_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Filterisrange"));
            Ddo_contratounidades_undmednom_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Includedatalist"));
            Ddo_contratounidades_undmednom_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalisttype");
            Ddo_contratounidades_undmednom_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistproc");
            Ddo_contratounidades_undmednom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratounidades_undmednom_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortasc");
            Ddo_contratounidades_undmednom_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Sortdsc");
            Ddo_contratounidades_undmednom_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Loadingdata");
            Ddo_contratounidades_undmednom_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Cleanfilter");
            Ddo_contratounidades_undmednom_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Noresultsfound");
            Ddo_contratounidades_undmednom_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Searchbuttontext");
            Ddo_contratounidades_produtividade_Caption = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Caption");
            Ddo_contratounidades_produtividade_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Tooltip");
            Ddo_contratounidades_produtividade_Cls = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Cls");
            Ddo_contratounidades_produtividade_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtext_set");
            Ddo_contratounidades_produtividade_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtextto_set");
            Ddo_contratounidades_produtividade_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Dropdownoptionstype");
            Ddo_contratounidades_produtividade_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Titlecontrolidtoreplace");
            Ddo_contratounidades_produtividade_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includesortasc"));
            Ddo_contratounidades_produtividade_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includesortdsc"));
            Ddo_contratounidades_produtividade_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortedstatus");
            Ddo_contratounidades_produtividade_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includefilter"));
            Ddo_contratounidades_produtividade_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filtertype");
            Ddo_contratounidades_produtividade_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filterisrange"));
            Ddo_contratounidades_produtividade_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Includedatalist"));
            Ddo_contratounidades_produtividade_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortasc");
            Ddo_contratounidades_produtividade_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Sortdsc");
            Ddo_contratounidades_produtividade_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Cleanfilter");
            Ddo_contratounidades_produtividade_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Rangefilterfrom");
            Ddo_contratounidades_produtividade_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Rangefilterto");
            Ddo_contratounidades_produtividade_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Searchbuttontext");
            Ddo_contratounidades_undmedsigla_Caption = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Caption");
            Ddo_contratounidades_undmedsigla_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Tooltip");
            Ddo_contratounidades_undmedsigla_Cls = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Cls");
            Ddo_contratounidades_undmedsigla_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filteredtext_set");
            Ddo_contratounidades_undmedsigla_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Selectedvalue_set");
            Ddo_contratounidades_undmedsigla_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Dropdownoptionstype");
            Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Titlecontrolidtoreplace");
            Ddo_contratounidades_undmedsigla_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includesortasc"));
            Ddo_contratounidades_undmedsigla_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includesortdsc"));
            Ddo_contratounidades_undmedsigla_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortedstatus");
            Ddo_contratounidades_undmedsigla_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includefilter"));
            Ddo_contratounidades_undmedsigla_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filtertype");
            Ddo_contratounidades_undmedsigla_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filterisrange"));
            Ddo_contratounidades_undmedsigla_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Includedatalist"));
            Ddo_contratounidades_undmedsigla_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalisttype");
            Ddo_contratounidades_undmedsigla_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistproc");
            Ddo_contratounidades_undmedsigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratounidades_undmedsigla_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortasc");
            Ddo_contratounidades_undmedsigla_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Sortdsc");
            Ddo_contratounidades_undmedsigla_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Loadingdata");
            Ddo_contratounidades_undmedsigla_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Cleanfilter");
            Ddo_contratounidades_undmedsigla_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Noresultsfound");
            Ddo_contratounidades_undmedsigla_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Searchbuttontext");
            Ddo_contratounidades_undmednom_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Activeeventkey");
            Ddo_contratounidades_undmednom_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Filteredtext_get");
            Ddo_contratounidades_undmednom_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM_Selectedvalue_get");
            Ddo_contratounidades_produtividade_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Activeeventkey");
            Ddo_contratounidades_produtividade_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtext_get");
            Ddo_contratounidades_produtividade_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE_Filteredtextto_get");
            Ddo_contratounidades_undmedsigla_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Activeeventkey");
            Ddo_contratounidades_undmedsigla_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Filteredtext_get");
            Ddo_contratounidades_undmedsigla_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOUNIDADES_UNDMEDNOM"), AV21TFContratoUnidades_UndMedNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOUNIDADES_UNDMEDNOM_SEL"), AV22TFContratoUnidades_UndMedNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE"), ",", ".") != AV25TFContratoUnidades_Produtividade )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO"), ",", ".") != AV26TFContratoUnidades_Produtividade_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA"), AV29TFContratoUnidades_UndMedSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL"), AV30TFContratoUnidades_UndMedSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E16IQ2 */
         E16IQ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16IQ2( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontratounidades_undmednom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratounidades_undmednom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmednom_Visible), 5, 0)));
         edtavTfcontratounidades_undmednom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratounidades_undmednom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmednom_sel_Visible), 5, 0)));
         edtavTfcontratounidades_produtividade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratounidades_produtividade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_produtividade_Visible), 5, 0)));
         edtavTfcontratounidades_produtividade_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratounidades_produtividade_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_produtividade_to_Visible), 5, 0)));
         edtavTfcontratounidades_undmedsigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratounidades_undmedsigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmedsigla_Visible), 5, 0)));
         edtavTfcontratounidades_undmedsigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratounidades_undmedsigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratounidades_undmedsigla_sel_Visible), 5, 0)));
         Ddo_contratounidades_undmednom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoUnidades_UndMedNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmednom_Internalname, "TitleControlIdToReplace", Ddo_contratounidades_undmednom_Titlecontrolidtoreplace);
         AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace = Ddo_contratounidades_undmednom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace", AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace);
         edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratounidades_produtividade_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoUnidades_Produtividade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_produtividade_Internalname, "TitleControlIdToReplace", Ddo_contratounidades_produtividade_Titlecontrolidtoreplace);
         AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace = Ddo_contratounidades_produtividade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace", AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace);
         edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoUnidades_UndMedSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmedsigla_Internalname, "TitleControlIdToReplace", Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace);
         AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace = Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace", AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace);
         edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV32DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV32DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E17IQ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV20ContratoUnidades_UndMedNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ContratoUnidades_ProdutividadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28ContratoUnidades_UndMedSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoUnidades_UndMedNom_Titleformat = 2;
         edtContratoUnidades_UndMedNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoUnidades_UndMedNom_Internalname, "Title", edtContratoUnidades_UndMedNom_Title);
         edtContratoUnidades_Produtividade_Titleformat = 2;
         edtContratoUnidades_Produtividade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Prod. Dia", AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoUnidades_Produtividade_Internalname, "Title", edtContratoUnidades_Produtividade_Title);
         edtContratoUnidades_UndMedSigla_Titleformat = 2;
         edtContratoUnidades_UndMedSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "", AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoUnidades_UndMedSigla_Internalname, "Title", edtContratoUnidades_UndMedSigla_Title);
         AV17Unidades.Clear();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV20ContratoUnidades_UndMedNomTitleFilterData", AV20ContratoUnidades_UndMedNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV24ContratoUnidades_ProdutividadeTitleFilterData", AV24ContratoUnidades_ProdutividadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV28ContratoUnidades_UndMedSiglaTitleFilterData", AV28ContratoUnidades_UndMedSiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV17Unidades", AV17Unidades);
      }

      protected void E12IQ2( )
      {
         /* Ddo_contratounidades_undmednom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratounidades_undmednom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratounidades_undmednom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmednom_Internalname, "SortedStatus", Ddo_contratounidades_undmednom_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmednom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratounidades_undmednom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmednom_Internalname, "SortedStatus", Ddo_contratounidades_undmednom_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmednom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV21TFContratoUnidades_UndMedNom = Ddo_contratounidades_undmednom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoUnidades_UndMedNom", AV21TFContratoUnidades_UndMedNom);
            AV22TFContratoUnidades_UndMedNom_Sel = Ddo_contratounidades_undmednom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoUnidades_UndMedNom_Sel", AV22TFContratoUnidades_UndMedNom_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E13IQ2( )
      {
         /* Ddo_contratounidades_produtividade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratounidades_produtividade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratounidades_produtividade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_produtividade_Internalname, "SortedStatus", Ddo_contratounidades_produtividade_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_produtividade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratounidades_produtividade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_produtividade_Internalname, "SortedStatus", Ddo_contratounidades_produtividade_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_produtividade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV25TFContratoUnidades_Produtividade = NumberUtil.Val( Ddo_contratounidades_produtividade_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV25TFContratoUnidades_Produtividade, 14, 5)));
            AV26TFContratoUnidades_Produtividade_To = NumberUtil.Val( Ddo_contratounidades_produtividade_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoUnidades_Produtividade_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E14IQ2( )
      {
         /* Ddo_contratounidades_undmedsigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratounidades_undmedsigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratounidades_undmedsigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmedsigla_Internalname, "SortedStatus", Ddo_contratounidades_undmedsigla_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmedsigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratounidades_undmedsigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmedsigla_Internalname, "SortedStatus", Ddo_contratounidades_undmedsigla_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratounidades_undmedsigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV29TFContratoUnidades_UndMedSigla = Ddo_contratounidades_undmedsigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoUnidades_UndMedSigla", AV29TFContratoUnidades_UndMedSigla);
            AV30TFContratoUnidades_UndMedSigla_Sel = Ddo_contratounidades_undmedsigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoUnidades_UndMedSigla_Sel", AV30TFContratoUnidades_UndMedSigla_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E18IQ2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("contratounidades.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1207ContratoUnidades_ContratoCod) + "," + UrlEncode("" +A1204ContratoUnidades_UndMedCod);
            AV16Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV16Update);
            AV35Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV16Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV16Update);
            AV35Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("contratounidades.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1207ContratoUnidades_ContratoCod) + "," + UrlEncode("" +A1204ContratoUnidades_UndMedCod);
            AV19Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV19Delete);
            AV36Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV19Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV19Delete);
            AV36Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtContratoUnidades_UndMedNom_Link = formatLink("viewcontratounidades.aspx") + "?" + UrlEncode("" +A1207ContratoUnidades_ContratoCod) + "," + UrlEncode(StringUtil.RTrim(""));
         AV17Unidades.Add(A1204ContratoUnidades_UndMedCod, 0);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 11;
         }
         sendrow_112( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_11_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(11, GridRow);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV17Unidades", AV17Unidades);
      }

      protected void E15IQ2( )
      {
         /* 'DoInsert' Routine */
         AV18TemMaisUnidades = false;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A1197UnidadeMedicao_Codigo ,
                                              AV17Unidades },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor H00IQ4 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1197UnidadeMedicao_Codigo = H00IQ4_A1197UnidadeMedicao_Codigo[0];
            AV18TemMaisUnidades = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(2);
         }
         pr_default.close(2);
         if ( AV18TemMaisUnidades )
         {
            context.PopUp(formatLink("contratounidades.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +AV7ContratoUnidades_ContratoCod) + "," + UrlEncode("" +0), new Object[] {});
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else
         {
            this.executeUsercontrolMethod(sPrefix, false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratounidades_undmednom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmednom_Internalname, "SortedStatus", Ddo_contratounidades_undmednom_Sortedstatus);
         Ddo_contratounidades_produtividade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_produtividade_Internalname, "SortedStatus", Ddo_contratounidades_produtividade_Sortedstatus);
         Ddo_contratounidades_undmedsigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmedsigla_Internalname, "SortedStatus", Ddo_contratounidades_undmedsigla_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contratounidades_undmednom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmednom_Internalname, "SortedStatus", Ddo_contratounidades_undmednom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratounidades_produtividade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_produtividade_Internalname, "SortedStatus", Ddo_contratounidades_produtividade_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratounidades_undmedsigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmedsigla_Internalname, "SortedStatus", Ddo_contratounidades_undmedsigla_Sortedstatus);
         }
      }

      protected void S142( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV38Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV38Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV38Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV39GXV1 = 1;
         while ( AV39GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV39GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDNOM") == 0 )
            {
               AV21TFContratoUnidades_UndMedNom = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoUnidades_UndMedNom", AV21TFContratoUnidades_UndMedNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoUnidades_UndMedNom)) )
               {
                  Ddo_contratounidades_undmednom_Filteredtext_set = AV21TFContratoUnidades_UndMedNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmednom_Internalname, "FilteredText_set", Ddo_contratounidades_undmednom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDNOM_SEL") == 0 )
            {
               AV22TFContratoUnidades_UndMedNom_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoUnidades_UndMedNom_Sel", AV22TFContratoUnidades_UndMedNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoUnidades_UndMedNom_Sel)) )
               {
                  Ddo_contratounidades_undmednom_Selectedvalue_set = AV22TFContratoUnidades_UndMedNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmednom_Internalname, "SelectedValue_set", Ddo_contratounidades_undmednom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_PRODUTIVIDADE") == 0 )
            {
               AV25TFContratoUnidades_Produtividade = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( AV25TFContratoUnidades_Produtividade, 14, 5)));
               AV26TFContratoUnidades_Produtividade_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoUnidades_Produtividade_To", StringUtil.LTrim( StringUtil.Str( AV26TFContratoUnidades_Produtividade_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV25TFContratoUnidades_Produtividade) )
               {
                  Ddo_contratounidades_produtividade_Filteredtext_set = StringUtil.Str( AV25TFContratoUnidades_Produtividade, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_produtividade_Internalname, "FilteredText_set", Ddo_contratounidades_produtividade_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV26TFContratoUnidades_Produtividade_To) )
               {
                  Ddo_contratounidades_produtividade_Filteredtextto_set = StringUtil.Str( AV26TFContratoUnidades_Produtividade_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_produtividade_Internalname, "FilteredTextTo_set", Ddo_contratounidades_produtividade_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDSIGLA") == 0 )
            {
               AV29TFContratoUnidades_UndMedSigla = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoUnidades_UndMedSigla", AV29TFContratoUnidades_UndMedSigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla)) )
               {
                  Ddo_contratounidades_undmedsigla_Filteredtext_set = AV29TFContratoUnidades_UndMedSigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmedsigla_Internalname, "FilteredText_set", Ddo_contratounidades_undmedsigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDSIGLA_SEL") == 0 )
            {
               AV30TFContratoUnidades_UndMedSigla_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoUnidades_UndMedSigla_Sel", AV30TFContratoUnidades_UndMedSigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoUnidades_UndMedSigla_Sel)) )
               {
                  Ddo_contratounidades_undmedsigla_Selectedvalue_set = AV30TFContratoUnidades_UndMedSigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratounidades_undmedsigla_Internalname, "SelectedValue_set", Ddo_contratounidades_undmedsigla_Selectedvalue_set);
               }
            }
            AV39GXV1 = (int)(AV39GXV1+1);
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV38Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoUnidades_UndMedNom)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDNOM";
            AV12GridStateFilterValue.gxTpr_Value = AV21TFContratoUnidades_UndMedNom;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoUnidades_UndMedNom_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDNOM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV22TFContratoUnidades_UndMedNom_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV25TFContratoUnidades_Produtividade) && (Convert.ToDecimal(0)==AV26TFContratoUnidades_Produtividade_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_PRODUTIVIDADE";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV25TFContratoUnidades_Produtividade, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV26TFContratoUnidades_Produtividade_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDSIGLA";
            AV12GridStateFilterValue.gxTpr_Value = AV29TFContratoUnidades_UndMedSigla;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoUnidades_UndMedSigla_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOUNIDADES_UNDMEDSIGLA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV30TFContratoUnidades_UndMedSigla_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7ContratoUnidades_ContratoCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CONTRATOUNIDADES_CONTRATOCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7ContratoUnidades_ContratoCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV38Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV38Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratoUnidades";
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E11IQ2( )
      {
         /* Confirmpanel_Close Routine */
         context.wjLoc = formatLink("wwunidademedicao.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void wb_table1_2_IQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratotitle_Internalname, "Unidades de Servi�o Contratadas", "", "", lblContratotitle_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_Contrato_UnidadesMedicaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_IQ2( true) ;
         }
         else
         {
            wb_table2_8_IQ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_IQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_21_IQ2( true) ;
         }
         else
         {
            wb_table3_21_IQ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_21_IQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_IQ2e( true) ;
         }
         else
         {
            wb_table1_2_IQ2e( false) ;
         }
      }

      protected void wb_table3_21_IQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertable_Internalname, tblUsertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_21_IQ2e( true) ;
         }
         else
         {
            wb_table3_21_IQ2e( false) ;
         }
      }

      protected void wb_table2_8_IQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedgrid_Internalname, tblTablemergedgrid_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"11\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoUnidades_UndMedNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoUnidades_UndMedNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoUnidades_UndMedNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoUnidades_Produtividade_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoUnidades_Produtividade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoUnidades_Produtividade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoUnidades_UndMedSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoUnidades_UndMedSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoUnidades_UndMedSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV16Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV19Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1205ContratoUnidades_UndMedNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoUnidades_UndMedNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoUnidades_UndMedNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoUnidades_UndMedNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1208ContratoUnidades_Produtividade, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoUnidades_Produtividade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoUnidades_Produtividade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoUnidades_UndMedSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoUnidades_UndMedSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 11 )
         {
            wbEnd = 0;
            nRC_GXsfl_11 = (short)(nGXsfl_11_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Contrato_UnidadesMedicaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_IQ2e( true) ;
         }
         else
         {
            wb_table2_8_IQ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContratoUnidades_ContratoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoUnidades_ContratoCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAIQ2( ) ;
         WSIQ2( ) ;
         WEIQ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContratoUnidades_ContratoCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAIQ2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contrato_unidadesmedicaowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAIQ2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContratoUnidades_ContratoCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoUnidades_ContratoCod), 6, 0)));
         }
         wcpOAV7ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoUnidades_ContratoCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContratoUnidades_ContratoCod != wcpOAV7ContratoUnidades_ContratoCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContratoUnidades_ContratoCod = AV7ContratoUnidades_ContratoCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContratoUnidades_ContratoCod = cgiGet( sPrefix+"AV7ContratoUnidades_ContratoCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContratoUnidades_ContratoCod) > 0 )
         {
            AV7ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContratoUnidades_ContratoCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoUnidades_ContratoCod), 6, 0)));
         }
         else
         {
            AV7ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContratoUnidades_ContratoCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAIQ2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSIQ2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSIQ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoUnidades_ContratoCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoUnidades_ContratoCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContratoUnidades_ContratoCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoUnidades_ContratoCod_CTRL", StringUtil.RTrim( sCtrlAV7ContratoUnidades_ContratoCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEIQ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812542984");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contrato_unidadesmedicaowc.js", "?202051812542984");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_112( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_11_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_11_idx;
         edtContratoUnidades_UndMedNom_Internalname = sPrefix+"CONTRATOUNIDADES_UNDMEDNOM_"+sGXsfl_11_idx;
         edtContratoUnidades_Produtividade_Internalname = sPrefix+"CONTRATOUNIDADES_PRODUTIVIDADE_"+sGXsfl_11_idx;
         edtContratoUnidades_UndMedSigla_Internalname = sPrefix+"CONTRATOUNIDADES_UNDMEDSIGLA_"+sGXsfl_11_idx;
      }

      protected void SubsflControlProps_fel_112( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_11_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_11_fel_idx;
         edtContratoUnidades_UndMedNom_Internalname = sPrefix+"CONTRATOUNIDADES_UNDMEDNOM_"+sGXsfl_11_fel_idx;
         edtContratoUnidades_Produtividade_Internalname = sPrefix+"CONTRATOUNIDADES_PRODUTIVIDADE_"+sGXsfl_11_fel_idx;
         edtContratoUnidades_UndMedSigla_Internalname = sPrefix+"CONTRATOUNIDADES_UNDMEDSIGLA_"+sGXsfl_11_fel_idx;
      }

      protected void sendrow_112( )
      {
         SubsflControlProps_112( ) ;
         WBIQ0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_11_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_11_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_11_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV16Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV35Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16Update)) ? AV35Update_GXI : context.PathToRelativeUrl( AV16Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV16Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV19Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV19Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV19Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV19Delete)) ? AV36Delete_GXI : context.PathToRelativeUrl( AV19Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV19Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_UndMedNom_Internalname,StringUtil.RTrim( A1205ContratoUnidades_UndMedNom),StringUtil.RTrim( context.localUtil.Format( A1205ContratoUnidades_UndMedNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContratoUnidades_UndMedNom_Link,(String)"",(String)"",(String)"",(String)edtContratoUnidades_UndMedNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_Produtividade_Internalname,StringUtil.LTrim( StringUtil.NToC( A1208ContratoUnidades_Produtividade, 14, 5, ",", "")),context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoUnidades_Produtividade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoUnidades_UndMedSigla_Internalname,StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla),StringUtil.RTrim( context.localUtil.Format( A1206ContratoUnidades_UndMedSigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoUnidades_UndMedSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOUNIDADES_PRODUTIVIDADE"+"_"+sGXsfl_11_idx, GetSecureSignedToken( sPrefix+sGXsfl_11_idx, context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         /* End function sendrow_112 */
      }

      protected void init_default_properties( )
      {
         lblContratotitle_Internalname = sPrefix+"CONTRATOTITLE";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtContratoUnidades_UndMedNom_Internalname = sPrefix+"CONTRATOUNIDADES_UNDMEDNOM";
         edtContratoUnidades_Produtividade_Internalname = sPrefix+"CONTRATOUNIDADES_PRODUTIVIDADE";
         edtContratoUnidades_UndMedSigla_Internalname = sPrefix+"CONTRATOUNIDADES_UNDMEDSIGLA";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTablemergedgrid_Internalname = sPrefix+"TABLEMERGEDGRID";
         Confirmpanel_Internalname = sPrefix+"CONFIRMPANEL";
         tblUsertable_Internalname = sPrefix+"USERTABLE";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontratounidades_undmednom_Internalname = sPrefix+"vTFCONTRATOUNIDADES_UNDMEDNOM";
         edtavTfcontratounidades_undmednom_sel_Internalname = sPrefix+"vTFCONTRATOUNIDADES_UNDMEDNOM_SEL";
         edtavTfcontratounidades_produtividade_Internalname = sPrefix+"vTFCONTRATOUNIDADES_PRODUTIVIDADE";
         edtavTfcontratounidades_produtividade_to_Internalname = sPrefix+"vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO";
         edtavTfcontratounidades_undmedsigla_Internalname = sPrefix+"vTFCONTRATOUNIDADES_UNDMEDSIGLA";
         edtavTfcontratounidades_undmedsigla_sel_Internalname = sPrefix+"vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL";
         Ddo_contratounidades_undmednom_Internalname = sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDNOM";
         edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE";
         Ddo_contratounidades_produtividade_Internalname = sPrefix+"DDO_CONTRATOUNIDADES_PRODUTIVIDADE";
         edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE";
         Ddo_contratounidades_undmedsigla_Internalname = sPrefix+"DDO_CONTRATOUNIDADES_UNDMEDSIGLA";
         edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoUnidades_UndMedSigla_Jsonclick = "";
         edtContratoUnidades_Produtividade_Jsonclick = "";
         edtContratoUnidades_UndMedNom_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoUnidades_UndMedNom_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtContratoUnidades_UndMedSigla_Titleformat = 0;
         edtContratoUnidades_Produtividade_Titleformat = 0;
         edtContratoUnidades_UndMedNom_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtContratoUnidades_UndMedSigla_Title = "";
         edtContratoUnidades_Produtividade_Title = "Prod. Dia";
         edtContratoUnidades_UndMedNom_Title = "Nome";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratounidades_undmedsigla_sel_Jsonclick = "";
         edtavTfcontratounidades_undmedsigla_sel_Visible = 1;
         edtavTfcontratounidades_undmedsigla_Jsonclick = "";
         edtavTfcontratounidades_undmedsigla_Visible = 1;
         edtavTfcontratounidades_produtividade_to_Jsonclick = "";
         edtavTfcontratounidades_produtividade_to_Visible = 1;
         edtavTfcontratounidades_produtividade_Jsonclick = "";
         edtavTfcontratounidades_produtividade_Visible = 1;
         edtavTfcontratounidades_undmednom_sel_Jsonclick = "";
         edtavTfcontratounidades_undmednom_sel_Visible = 1;
         edtavTfcontratounidades_undmednom_Jsonclick = "";
         edtavTfcontratounidades_undmednom_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         Ddo_contratounidades_undmedsigla_Searchbuttontext = "Pesquisar";
         Ddo_contratounidades_undmedsigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratounidades_undmedsigla_Cleanfilter = "Limpar pesquisa";
         Ddo_contratounidades_undmedsigla_Loadingdata = "Carregando dados...";
         Ddo_contratounidades_undmedsigla_Sortdsc = "Ordenar de Z � A";
         Ddo_contratounidades_undmedsigla_Sortasc = "Ordenar de A � Z";
         Ddo_contratounidades_undmedsigla_Datalistupdateminimumcharacters = 0;
         Ddo_contratounidades_undmedsigla_Datalistproc = "GetContrato_UnidadesMedicaoWCFilterData";
         Ddo_contratounidades_undmedsigla_Datalisttype = "Dynamic";
         Ddo_contratounidades_undmedsigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedsigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratounidades_undmedsigla_Filtertype = "Character";
         Ddo_contratounidades_undmedsigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedsigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedsigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace = "";
         Ddo_contratounidades_undmedsigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratounidades_undmedsigla_Cls = "ColumnSettings";
         Ddo_contratounidades_undmedsigla_Tooltip = "Op��es";
         Ddo_contratounidades_undmedsigla_Caption = "";
         Ddo_contratounidades_produtividade_Searchbuttontext = "Pesquisar";
         Ddo_contratounidades_produtividade_Rangefilterto = "At�";
         Ddo_contratounidades_produtividade_Rangefilterfrom = "Desde";
         Ddo_contratounidades_produtividade_Cleanfilter = "Limpar pesquisa";
         Ddo_contratounidades_produtividade_Sortdsc = "Ordenar de Z � A";
         Ddo_contratounidades_produtividade_Sortasc = "Ordenar de A � Z";
         Ddo_contratounidades_produtividade_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratounidades_produtividade_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratounidades_produtividade_Filtertype = "Numeric";
         Ddo_contratounidades_produtividade_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratounidades_produtividade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratounidades_produtividade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratounidades_produtividade_Titlecontrolidtoreplace = "";
         Ddo_contratounidades_produtividade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratounidades_produtividade_Cls = "ColumnSettings";
         Ddo_contratounidades_produtividade_Tooltip = "Op��es";
         Ddo_contratounidades_produtividade_Caption = "";
         Ddo_contratounidades_undmednom_Searchbuttontext = "Pesquisar";
         Ddo_contratounidades_undmednom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratounidades_undmednom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratounidades_undmednom_Loadingdata = "Carregando dados...";
         Ddo_contratounidades_undmednom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratounidades_undmednom_Sortasc = "Ordenar de A � Z";
         Ddo_contratounidades_undmednom_Datalistupdateminimumcharacters = 0;
         Ddo_contratounidades_undmednom_Datalistproc = "GetContrato_UnidadesMedicaoWCFilterData";
         Ddo_contratounidades_undmednom_Datalisttype = "Dynamic";
         Ddo_contratounidades_undmednom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmednom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratounidades_undmednom_Filtertype = "Character";
         Ddo_contratounidades_undmednom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmednom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmednom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratounidades_undmednom_Titlecontrolidtoreplace = "";
         Ddo_contratounidades_undmednom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratounidades_undmednom_Cls = "ColumnSettings";
         Ddo_contratounidades_undmednom_Tooltip = "Op��es";
         Ddo_contratounidades_undmednom_Caption = "";
         Confirmpanel_Confirmtype = "0";
         Confirmpanel_Confirmtext = "Deve cadastar novas Unidades de Medi��o para incluir no contrato";
         Confirmpanel_Icon = "2";
         Confirmpanel_Title = "Aten��o";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV22TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV7ContratoUnidades_ContratoCod',fld:'vCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV20ContratoUnidades_UndMedNomTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ContratoUnidades_ProdutividadeTitleFilterData',fld:'vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV28ContratoUnidades_UndMedSiglaTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContratoUnidades_UndMedNom_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedNom_Title',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Title'},{av:'edtContratoUnidades_Produtividade_Titleformat',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Titleformat'},{av:'edtContratoUnidades_Produtividade_Title',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Title'},{av:'edtContratoUnidades_UndMedSigla_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedSigla_Title',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Title'},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'}]}");
         setEventMetadata("DDO_CONTRATOUNIDADES_UNDMEDNOM.ONOPTIONCLICKED","{handler:'E12IQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV22TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV7ContratoUnidades_ContratoCod',fld:'vCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contratounidades_undmednom_Activeeventkey',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'ActiveEventKey'},{av:'Ddo_contratounidades_undmednom_Filteredtext_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'FilteredText_get'},{av:'Ddo_contratounidades_undmednom_Selectedvalue_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratounidades_undmednom_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SortedStatus'},{av:'AV21TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV22TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratounidades_produtividade_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmedsigla_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOUNIDADES_PRODUTIVIDADE.ONOPTIONCLICKED","{handler:'E13IQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV22TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV7ContratoUnidades_ContratoCod',fld:'vCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contratounidades_produtividade_Activeeventkey',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'ActiveEventKey'},{av:'Ddo_contratounidades_produtividade_Filteredtext_get',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'FilteredText_get'},{av:'Ddo_contratounidades_produtividade_Filteredtextto_get',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratounidades_produtividade_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'SortedStatus'},{av:'AV25TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratounidades_undmednom_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SortedStatus'},{av:'Ddo_contratounidades_undmedsigla_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOUNIDADES_UNDMEDSIGLA.ONOPTIONCLICKED","{handler:'E14IQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV22TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV7ContratoUnidades_ContratoCod',fld:'vCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contratounidades_undmedsigla_Activeeventkey',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'ActiveEventKey'},{av:'Ddo_contratounidades_undmedsigla_Filteredtext_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'FilteredText_get'},{av:'Ddo_contratounidades_undmedsigla_Selectedvalue_get',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratounidades_undmedsigla_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDSIGLA',prop:'SortedStatus'},{av:'AV29TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratounidades_undmednom_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_UNDMEDNOM',prop:'SortedStatus'},{av:'Ddo_contratounidades_produtividade_Sortedstatus',ctrl:'DDO_CONTRATOUNIDADES_PRODUTIVIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E18IQ2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV16Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV19Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtContratoUnidades_UndMedNom_Link',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Link'},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null}]}");
         setEventMetadata("'DOINSERT'","{handler:'E15IQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV22TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV7ContratoUnidades_ContratoCod',fld:'vCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'A1197UnidadeMedicao_Codigo',fld:'UNIDADEMEDICAO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E11IQ2',iparms:[],oparms:[]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV22TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV7ContratoUnidades_ContratoCod',fld:'vCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV20ContratoUnidades_UndMedNomTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ContratoUnidades_ProdutividadeTitleFilterData',fld:'vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV28ContratoUnidades_UndMedSiglaTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContratoUnidades_UndMedNom_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedNom_Title',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Title'},{av:'edtContratoUnidades_Produtividade_Titleformat',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Titleformat'},{av:'edtContratoUnidades_Produtividade_Title',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Title'},{av:'edtContratoUnidades_UndMedSigla_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedSigla_Title',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Title'},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV22TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV7ContratoUnidades_ContratoCod',fld:'vCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV20ContratoUnidades_UndMedNomTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ContratoUnidades_ProdutividadeTitleFilterData',fld:'vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV28ContratoUnidades_UndMedSiglaTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContratoUnidades_UndMedNom_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedNom_Title',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Title'},{av:'edtContratoUnidades_Produtividade_Titleformat',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Titleformat'},{av:'edtContratoUnidades_Produtividade_Title',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Title'},{av:'edtContratoUnidades_UndMedSigla_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedSigla_Title',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Title'},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV22TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV7ContratoUnidades_ContratoCod',fld:'vCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV20ContratoUnidades_UndMedNomTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ContratoUnidades_ProdutividadeTitleFilterData',fld:'vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV28ContratoUnidades_UndMedSiglaTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContratoUnidades_UndMedNom_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedNom_Title',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Title'},{av:'edtContratoUnidades_Produtividade_Titleformat',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Titleformat'},{av:'edtContratoUnidades_Produtividade_Title',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Title'},{av:'edtContratoUnidades_UndMedSigla_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedSigla_Title',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Title'},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1207ContratoUnidades_ContratoCod',fld:'CONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1204ContratoUnidades_UndMedCod',fld:'CONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_PRODUTIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOUNIDADES_UNDMEDSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFContratoUnidades_UndMedNom',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM',pic:'@!',nv:''},{av:'AV22TFContratoUnidades_UndMedNom_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDNOM_SEL',pic:'@!',nv:''},{av:'AV25TFContratoUnidades_Produtividade',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TFContratoUnidades_Produtividade_To',fld:'vTFCONTRATOUNIDADES_PRODUTIVIDADE_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29TFContratoUnidades_UndMedSigla',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA',pic:'@!',nv:''},{av:'AV30TFContratoUnidades_UndMedSigla_Sel',fld:'vTFCONTRATOUNIDADES_UNDMEDSIGLA_SEL',pic:'@!',nv:''},{av:'AV7ContratoUnidades_ContratoCod',fld:'vCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV20ContratoUnidades_UndMedNomTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ContratoUnidades_ProdutividadeTitleFilterData',fld:'vCONTRATOUNIDADES_PRODUTIVIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV28ContratoUnidades_UndMedSiglaTitleFilterData',fld:'vCONTRATOUNIDADES_UNDMEDSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContratoUnidades_UndMedNom_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedNom_Title',ctrl:'CONTRATOUNIDADES_UNDMEDNOM',prop:'Title'},{av:'edtContratoUnidades_Produtividade_Titleformat',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Titleformat'},{av:'edtContratoUnidades_Produtividade_Title',ctrl:'CONTRATOUNIDADES_PRODUTIVIDADE',prop:'Title'},{av:'edtContratoUnidades_UndMedSigla_Titleformat',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Titleformat'},{av:'edtContratoUnidades_UndMedSigla_Title',ctrl:'CONTRATOUNIDADES_UNDMEDSIGLA',prop:'Title'},{av:'AV17Unidades',fld:'vUNIDADES',pic:'',nv:null},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Ddo_contratounidades_undmednom_Activeeventkey = "";
         Ddo_contratounidades_undmednom_Filteredtext_get = "";
         Ddo_contratounidades_undmednom_Selectedvalue_get = "";
         Ddo_contratounidades_produtividade_Activeeventkey = "";
         Ddo_contratounidades_produtividade_Filteredtext_get = "";
         Ddo_contratounidades_produtividade_Filteredtextto_get = "";
         Ddo_contratounidades_undmedsigla_Activeeventkey = "";
         Ddo_contratounidades_undmedsigla_Filteredtext_get = "";
         Ddo_contratounidades_undmedsigla_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV21TFContratoUnidades_UndMedNom = "";
         AV22TFContratoUnidades_UndMedNom_Sel = "";
         AV29TFContratoUnidades_UndMedSigla = "";
         AV30TFContratoUnidades_UndMedSigla_Sel = "";
         AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace = "";
         AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace = "";
         AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV38Pgmname = "";
         AV17Unidades = new GxSimpleCollection();
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV32DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV20ContratoUnidades_UndMedNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ContratoUnidades_ProdutividadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28ContratoUnidades_UndMedSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Confirmpanel_Width = "";
         Ddo_contratounidades_undmednom_Filteredtext_set = "";
         Ddo_contratounidades_undmednom_Selectedvalue_set = "";
         Ddo_contratounidades_undmednom_Sortedstatus = "";
         Ddo_contratounidades_produtividade_Filteredtext_set = "";
         Ddo_contratounidades_produtividade_Filteredtextto_set = "";
         Ddo_contratounidades_produtividade_Sortedstatus = "";
         Ddo_contratounidades_undmedsigla_Filteredtext_set = "";
         Ddo_contratounidades_undmedsigla_Selectedvalue_set = "";
         Ddo_contratounidades_undmedsigla_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV16Update = "";
         AV35Update_GXI = "";
         AV19Delete = "";
         AV36Delete_GXI = "";
         A1205ContratoUnidades_UndMedNom = "";
         A1206ContratoUnidades_UndMedSigla = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV21TFContratoUnidades_UndMedNom = "";
         lV29TFContratoUnidades_UndMedSigla = "";
         H00IQ2_A1204ContratoUnidades_UndMedCod = new int[1] ;
         H00IQ2_A1207ContratoUnidades_ContratoCod = new int[1] ;
         H00IQ2_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         H00IQ2_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         H00IQ2_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         H00IQ2_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         H00IQ2_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         H00IQ2_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         H00IQ3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         H00IQ4_A1197UnidadeMedicao_Codigo = new int[1] ;
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         lblContratotitle_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContratoUnidades_ContratoCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contrato_unidadesmedicaowc__default(),
            new Object[][] {
                new Object[] {
               H00IQ2_A1204ContratoUnidades_UndMedCod, H00IQ2_A1207ContratoUnidades_ContratoCod, H00IQ2_A1206ContratoUnidades_UndMedSigla, H00IQ2_n1206ContratoUnidades_UndMedSigla, H00IQ2_A1208ContratoUnidades_Produtividade, H00IQ2_n1208ContratoUnidades_Produtividade, H00IQ2_A1205ContratoUnidades_UndMedNom, H00IQ2_n1205ContratoUnidades_UndMedNom
               }
               , new Object[] {
               H00IQ3_AGRID_nRecordCount
               }
               , new Object[] {
               H00IQ4_A1197UnidadeMedicao_Codigo
               }
            }
         );
         AV38Pgmname = "Contrato_UnidadesMedicaoWC";
         /* GeneXus formulas. */
         AV38Pgmname = "Contrato_UnidadesMedicaoWC";
         context.Gx_err = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_11 ;
      private short nGXsfl_11_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_11_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoUnidades_UndMedNom_Titleformat ;
      private short edtContratoUnidades_Produtividade_Titleformat ;
      private short edtContratoUnidades_UndMedSigla_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContratoUnidades_ContratoCod ;
      private int wcpOAV7ContratoUnidades_ContratoCod ;
      private int subGrid_Rows ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int A1204ContratoUnidades_UndMedCod ;
      private int A1197UnidadeMedicao_Codigo ;
      private int Ddo_contratounidades_undmednom_Datalistupdateminimumcharacters ;
      private int Ddo_contratounidades_undmedsigla_Datalistupdateminimumcharacters ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontratounidades_undmednom_Visible ;
      private int edtavTfcontratounidades_undmednom_sel_Visible ;
      private int edtavTfcontratounidades_produtividade_Visible ;
      private int edtavTfcontratounidades_produtividade_to_Visible ;
      private int edtavTfcontratounidades_undmedsigla_Visible ;
      private int edtavTfcontratounidades_undmedsigla_sel_Visible ;
      private int edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int imgInsert_Enabled ;
      private int AV39GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV25TFContratoUnidades_Produtividade ;
      private decimal AV26TFContratoUnidades_Produtividade_To ;
      private decimal A1208ContratoUnidades_Produtividade ;
      private String Ddo_contratounidades_undmednom_Activeeventkey ;
      private String Ddo_contratounidades_undmednom_Filteredtext_get ;
      private String Ddo_contratounidades_undmednom_Selectedvalue_get ;
      private String Ddo_contratounidades_produtividade_Activeeventkey ;
      private String Ddo_contratounidades_produtividade_Filteredtext_get ;
      private String Ddo_contratounidades_produtividade_Filteredtextto_get ;
      private String Ddo_contratounidades_undmedsigla_Activeeventkey ;
      private String Ddo_contratounidades_undmedsigla_Filteredtext_get ;
      private String Ddo_contratounidades_undmedsigla_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_11_idx="0001" ;
      private String AV21TFContratoUnidades_UndMedNom ;
      private String AV22TFContratoUnidades_UndMedNom_Sel ;
      private String AV29TFContratoUnidades_UndMedSigla ;
      private String AV30TFContratoUnidades_UndMedSigla_Sel ;
      private String AV38Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Confirmpanel_Width ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Icon ;
      private String Confirmpanel_Confirmtext ;
      private String Confirmpanel_Confirmtype ;
      private String Ddo_contratounidades_undmednom_Caption ;
      private String Ddo_contratounidades_undmednom_Tooltip ;
      private String Ddo_contratounidades_undmednom_Cls ;
      private String Ddo_contratounidades_undmednom_Filteredtext_set ;
      private String Ddo_contratounidades_undmednom_Selectedvalue_set ;
      private String Ddo_contratounidades_undmednom_Dropdownoptionstype ;
      private String Ddo_contratounidades_undmednom_Titlecontrolidtoreplace ;
      private String Ddo_contratounidades_undmednom_Sortedstatus ;
      private String Ddo_contratounidades_undmednom_Filtertype ;
      private String Ddo_contratounidades_undmednom_Datalisttype ;
      private String Ddo_contratounidades_undmednom_Datalistproc ;
      private String Ddo_contratounidades_undmednom_Sortasc ;
      private String Ddo_contratounidades_undmednom_Sortdsc ;
      private String Ddo_contratounidades_undmednom_Loadingdata ;
      private String Ddo_contratounidades_undmednom_Cleanfilter ;
      private String Ddo_contratounidades_undmednom_Noresultsfound ;
      private String Ddo_contratounidades_undmednom_Searchbuttontext ;
      private String Ddo_contratounidades_produtividade_Caption ;
      private String Ddo_contratounidades_produtividade_Tooltip ;
      private String Ddo_contratounidades_produtividade_Cls ;
      private String Ddo_contratounidades_produtividade_Filteredtext_set ;
      private String Ddo_contratounidades_produtividade_Filteredtextto_set ;
      private String Ddo_contratounidades_produtividade_Dropdownoptionstype ;
      private String Ddo_contratounidades_produtividade_Titlecontrolidtoreplace ;
      private String Ddo_contratounidades_produtividade_Sortedstatus ;
      private String Ddo_contratounidades_produtividade_Filtertype ;
      private String Ddo_contratounidades_produtividade_Sortasc ;
      private String Ddo_contratounidades_produtividade_Sortdsc ;
      private String Ddo_contratounidades_produtividade_Cleanfilter ;
      private String Ddo_contratounidades_produtividade_Rangefilterfrom ;
      private String Ddo_contratounidades_produtividade_Rangefilterto ;
      private String Ddo_contratounidades_produtividade_Searchbuttontext ;
      private String Ddo_contratounidades_undmedsigla_Caption ;
      private String Ddo_contratounidades_undmedsigla_Tooltip ;
      private String Ddo_contratounidades_undmedsigla_Cls ;
      private String Ddo_contratounidades_undmedsigla_Filteredtext_set ;
      private String Ddo_contratounidades_undmedsigla_Selectedvalue_set ;
      private String Ddo_contratounidades_undmedsigla_Dropdownoptionstype ;
      private String Ddo_contratounidades_undmedsigla_Titlecontrolidtoreplace ;
      private String Ddo_contratounidades_undmedsigla_Sortedstatus ;
      private String Ddo_contratounidades_undmedsigla_Filtertype ;
      private String Ddo_contratounidades_undmedsigla_Datalisttype ;
      private String Ddo_contratounidades_undmedsigla_Datalistproc ;
      private String Ddo_contratounidades_undmedsigla_Sortasc ;
      private String Ddo_contratounidades_undmedsigla_Sortdsc ;
      private String Ddo_contratounidades_undmedsigla_Loadingdata ;
      private String Ddo_contratounidades_undmedsigla_Cleanfilter ;
      private String Ddo_contratounidades_undmedsigla_Noresultsfound ;
      private String Ddo_contratounidades_undmedsigla_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontratounidades_undmednom_Internalname ;
      private String edtavTfcontratounidades_undmednom_Jsonclick ;
      private String edtavTfcontratounidades_undmednom_sel_Internalname ;
      private String edtavTfcontratounidades_undmednom_sel_Jsonclick ;
      private String edtavTfcontratounidades_produtividade_Internalname ;
      private String edtavTfcontratounidades_produtividade_Jsonclick ;
      private String edtavTfcontratounidades_produtividade_to_Internalname ;
      private String edtavTfcontratounidades_produtividade_to_Jsonclick ;
      private String edtavTfcontratounidades_undmedsigla_Internalname ;
      private String edtavTfcontratounidades_undmedsigla_Jsonclick ;
      private String edtavTfcontratounidades_undmedsigla_sel_Internalname ;
      private String edtavTfcontratounidades_undmedsigla_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contratounidades_undmednomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratounidades_produtividadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratounidades_undmedsiglatitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String A1205ContratoUnidades_UndMedNom ;
      private String edtContratoUnidades_UndMedNom_Internalname ;
      private String edtContratoUnidades_Produtividade_Internalname ;
      private String A1206ContratoUnidades_UndMedSigla ;
      private String edtContratoUnidades_UndMedSigla_Internalname ;
      private String scmdbuf ;
      private String lV21TFContratoUnidades_UndMedNom ;
      private String lV29TFContratoUnidades_UndMedSigla ;
      private String subGrid_Internalname ;
      private String Ddo_contratounidades_undmednom_Internalname ;
      private String Ddo_contratounidades_produtividade_Internalname ;
      private String Ddo_contratounidades_undmedsigla_Internalname ;
      private String edtContratoUnidades_UndMedNom_Title ;
      private String edtContratoUnidades_Produtividade_Title ;
      private String edtContratoUnidades_UndMedSigla_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContratoUnidades_UndMedNom_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String lblContratotitle_Internalname ;
      private String lblContratotitle_Jsonclick ;
      private String tblUsertable_Internalname ;
      private String tblTablemergedgrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7ContratoUnidades_ContratoCod ;
      private String sGXsfl_11_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoUnidades_UndMedNom_Jsonclick ;
      private String edtContratoUnidades_Produtividade_Jsonclick ;
      private String edtContratoUnidades_UndMedSigla_Jsonclick ;
      private String Confirmpanel_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Ddo_contratounidades_undmednom_Includesortasc ;
      private bool Ddo_contratounidades_undmednom_Includesortdsc ;
      private bool Ddo_contratounidades_undmednom_Includefilter ;
      private bool Ddo_contratounidades_undmednom_Filterisrange ;
      private bool Ddo_contratounidades_undmednom_Includedatalist ;
      private bool Ddo_contratounidades_produtividade_Includesortasc ;
      private bool Ddo_contratounidades_produtividade_Includesortdsc ;
      private bool Ddo_contratounidades_produtividade_Includefilter ;
      private bool Ddo_contratounidades_produtividade_Filterisrange ;
      private bool Ddo_contratounidades_produtividade_Includedatalist ;
      private bool Ddo_contratounidades_undmedsigla_Includesortasc ;
      private bool Ddo_contratounidades_undmedsigla_Includesortdsc ;
      private bool Ddo_contratounidades_undmedsigla_Includefilter ;
      private bool Ddo_contratounidades_undmedsigla_Filterisrange ;
      private bool Ddo_contratounidades_undmedsigla_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1205ContratoUnidades_UndMedNom ;
      private bool n1208ContratoUnidades_Produtividade ;
      private bool n1206ContratoUnidades_UndMedSigla ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV18TemMaisUnidades ;
      private bool AV16Update_IsBlob ;
      private bool AV19Delete_IsBlob ;
      private String AV23ddo_ContratoUnidades_UndMedNomTitleControlIdToReplace ;
      private String AV27ddo_ContratoUnidades_ProdutividadeTitleControlIdToReplace ;
      private String AV31ddo_ContratoUnidades_UndMedSiglaTitleControlIdToReplace ;
      private String AV35Update_GXI ;
      private String AV36Delete_GXI ;
      private String AV16Update ;
      private String AV19Delete ;
      private String imgInsert_Bitmap ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00IQ2_A1204ContratoUnidades_UndMedCod ;
      private int[] H00IQ2_A1207ContratoUnidades_ContratoCod ;
      private String[] H00IQ2_A1206ContratoUnidades_UndMedSigla ;
      private bool[] H00IQ2_n1206ContratoUnidades_UndMedSigla ;
      private decimal[] H00IQ2_A1208ContratoUnidades_Produtividade ;
      private bool[] H00IQ2_n1208ContratoUnidades_Produtividade ;
      private String[] H00IQ2_A1205ContratoUnidades_UndMedNom ;
      private bool[] H00IQ2_n1205ContratoUnidades_UndMedNom ;
      private long[] H00IQ3_AGRID_nRecordCount ;
      private int[] H00IQ4_A1197UnidadeMedicao_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV17Unidades ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV20ContratoUnidades_UndMedNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV24ContratoUnidades_ProdutividadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV28ContratoUnidades_UndMedSiglaTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV32DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class contrato_unidadesmedicaowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00IQ2( IGxContext context ,
                                             String AV22TFContratoUnidades_UndMedNom_Sel ,
                                             String AV21TFContratoUnidades_UndMedNom ,
                                             decimal AV25TFContratoUnidades_Produtividade ,
                                             decimal AV26TFContratoUnidades_Produtividade_To ,
                                             String AV30TFContratoUnidades_UndMedSigla_Sel ,
                                             String AV29TFContratoUnidades_UndMedSigla ,
                                             String A1205ContratoUnidades_UndMedNom ,
                                             decimal A1208ContratoUnidades_Produtividade ,
                                             String A1206ContratoUnidades_UndMedSigla ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1207ContratoUnidades_ContratoCod ,
                                             int AV7ContratoUnidades_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T1.[ContratoUnidades_ContratoCod], T2.[UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla, T1.[ContratoUnidades_Produtividade], T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom";
         sFromString = " FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContratoUnidades_ContratoCod] = @AV7ContratoUnidades_ContratoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoUnidades_UndMedNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoUnidades_UndMedNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV21TFContratoUnidades_UndMedNom)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoUnidades_UndMedNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV22TFContratoUnidades_UndMedNom_Sel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFContratoUnidades_Produtividade) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] >= @AV25TFContratoUnidades_Produtividade)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFContratoUnidades_Produtividade_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] <= @AV26TFContratoUnidades_Produtividade_To)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoUnidades_UndMedSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] like @lV29TFContratoUnidades_UndMedSigla)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoUnidades_UndMedSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] = @AV30TFContratoUnidades_UndMedSigla_Sel)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_Produtividade]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_Produtividade] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Sigla]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Sigla] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoUnidades_ContratoCod], T1.[ContratoUnidades_UndMedCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00IQ3( IGxContext context ,
                                             String AV22TFContratoUnidades_UndMedNom_Sel ,
                                             String AV21TFContratoUnidades_UndMedNom ,
                                             decimal AV25TFContratoUnidades_Produtividade ,
                                             decimal AV26TFContratoUnidades_Produtividade_To ,
                                             String AV30TFContratoUnidades_UndMedSigla_Sel ,
                                             String AV29TFContratoUnidades_UndMedSigla ,
                                             String A1205ContratoUnidades_UndMedNom ,
                                             decimal A1208ContratoUnidades_Produtividade ,
                                             String A1206ContratoUnidades_UndMedSigla ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1207ContratoUnidades_ContratoCod ,
                                             int AV7ContratoUnidades_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoUnidades_ContratoCod] = @AV7ContratoUnidades_ContratoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoUnidades_UndMedNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoUnidades_UndMedNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV21TFContratoUnidades_UndMedNom)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoUnidades_UndMedNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV22TFContratoUnidades_UndMedNom_Sel)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFContratoUnidades_Produtividade) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] >= @AV25TFContratoUnidades_Produtividade)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFContratoUnidades_Produtividade_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] <= @AV26TFContratoUnidades_Produtividade_To)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoUnidades_UndMedSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFContratoUnidades_UndMedSigla)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] like @lV29TFContratoUnidades_UndMedSigla)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoUnidades_UndMedSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] = @AV30TFContratoUnidades_UndMedSigla_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H00IQ4( IGxContext context ,
                                             int A1197UnidadeMedicao_Codigo ,
                                             IGxCollection AV17Unidades )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 [UnidadeMedicao_Codigo] FROM [UnidadeMedicao] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17Unidades, "[UnidadeMedicao_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [UnidadeMedicao_Codigo]";
         GXv_Object6[0] = scmdbuf;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00IQ2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (decimal)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
               case 1 :
                     return conditional_H00IQ3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (decimal)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
               case 2 :
                     return conditional_H00IQ4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00IQ2 ;
          prmH00IQ2 = new Object[] {
          new Object[] {"@AV7ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV21TFContratoUnidades_UndMedNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV22TFContratoUnidades_UndMedNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV25TFContratoUnidades_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26TFContratoUnidades_Produtividade_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@lV29TFContratoUnidades_UndMedSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV30TFContratoUnidades_UndMedSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00IQ3 ;
          prmH00IQ3 = new Object[] {
          new Object[] {"@AV7ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV21TFContratoUnidades_UndMedNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV22TFContratoUnidades_UndMedNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV25TFContratoUnidades_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26TFContratoUnidades_Produtividade_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@lV29TFContratoUnidades_UndMedSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV30TFContratoUnidades_UndMedSigla_Sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmH00IQ4 ;
          prmH00IQ4 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00IQ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IQ2,11,0,true,false )
             ,new CursorDef("H00IQ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IQ3,1,0,true,false )
             ,new CursorDef("H00IQ4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00IQ4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

}
