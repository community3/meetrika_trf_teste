/*
               File: DemandaNotificacoesWC
        Description: Demanda Notificacoes WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/17/2020 1:55:42.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class demandanotificacoeswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public demandanotificacoeswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public demandanotificacoeswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo )
      {
         this.AV7ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContagemResultadoNotificacao_Aut = new GXCombobox();
         cmbContagemResultadoNotificacao_Sec = new GXCombobox();
         cmbContagemResultadoNotificacao_Logged = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContagemResultado_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV17TFContagemResultadoNotificacao_DataHora = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContagemResultadoNotificacao_DataHora", context.localUtil.TToC( AV17TFContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
                  AV18TFContagemResultadoNotificacao_DataHora_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContagemResultadoNotificacao_DataHora_To", context.localUtil.TToC( AV18TFContagemResultadoNotificacao_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
                  AV23TFContagemResultadoNotificacao_UsuNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagemResultadoNotificacao_UsuNom", AV23TFContagemResultadoNotificacao_UsuNom);
                  AV24TFContagemResultadoNotificacao_UsuNom_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContagemResultadoNotificacao_UsuNom_Sel", AV24TFContagemResultadoNotificacao_UsuNom_Sel);
                  AV64TFContagemResultadoNotificacao_Assunto = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFContagemResultadoNotificacao_Assunto", AV64TFContagemResultadoNotificacao_Assunto);
                  AV65TFContagemResultadoNotificacao_Assunto_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65TFContagemResultadoNotificacao_Assunto_Sel", AV65TFContagemResultadoNotificacao_Assunto_Sel);
                  AV27TFContagemResultadoNotificacao_Destinatarios = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27TFContagemResultadoNotificacao_Destinatarios), 6, 0)));
                  AV28TFContagemResultadoNotificacao_Destinatarios_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContagemResultadoNotificacao_Destinatarios_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28TFContagemResultadoNotificacao_Destinatarios_To), 6, 0)));
                  AV31TFContagemResultadoNotificacao_Demandas = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContagemResultadoNotificacao_Demandas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContagemResultadoNotificacao_Demandas), 6, 0)));
                  AV32TFContagemResultadoNotificacao_Demandas_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFContagemResultadoNotificacao_Demandas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContagemResultadoNotificacao_Demandas_To), 6, 0)));
                  AV35TFContagemResultadoNotificacao_Host = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFContagemResultadoNotificacao_Host", AV35TFContagemResultadoNotificacao_Host);
                  AV36TFContagemResultadoNotificacao_Host_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFContagemResultadoNotificacao_Host_Sel", AV36TFContagemResultadoNotificacao_Host_Sel);
                  AV39TFContagemResultadoNotificacao_User = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFContagemResultadoNotificacao_User", AV39TFContagemResultadoNotificacao_User);
                  AV40TFContagemResultadoNotificacao_User_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFContagemResultadoNotificacao_User_Sel", AV40TFContagemResultadoNotificacao_User_Sel);
                  AV43TFContagemResultadoNotificacao_Port = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContagemResultadoNotificacao_Port), 4, 0)));
                  AV44TFContagemResultadoNotificacao_Port_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFContagemResultadoNotificacao_Port_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContagemResultadoNotificacao_Port_To), 4, 0)));
                  AV47TFContagemResultadoNotificacao_Aut_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFContagemResultadoNotificacao_Aut_Sel", StringUtil.Str( (decimal)(AV47TFContagemResultadoNotificacao_Aut_Sel), 1, 0));
                  AV7ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
                  AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace", AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace);
                  AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace", AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace);
                  AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace", AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace);
                  AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace", AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace);
                  AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace", AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace);
                  AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace", AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace);
                  AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace", AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace);
                  AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace", AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace);
                  AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace", AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace);
                  AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace", AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace);
                  AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace", AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV61Codigos);
                  AV69Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV51TFContagemResultadoNotificacao_Sec_Sels);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV55TFContagemResultadoNotificacao_Logged_Sels);
                  A1413ContagemResultadoNotificacao_UsuCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A1412ContagemResultadoNotificacao_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContagemResultadoNotificacao_DataHora, AV18TFContagemResultadoNotificacao_DataHora_To, AV23TFContagemResultadoNotificacao_UsuNom, AV24TFContagemResultadoNotificacao_UsuNom_Sel, AV64TFContagemResultadoNotificacao_Assunto, AV65TFContagemResultadoNotificacao_Assunto_Sel, AV27TFContagemResultadoNotificacao_Destinatarios, AV28TFContagemResultadoNotificacao_Destinatarios_To, AV31TFContagemResultadoNotificacao_Demandas, AV32TFContagemResultadoNotificacao_Demandas_To, AV35TFContagemResultadoNotificacao_Host, AV36TFContagemResultadoNotificacao_Host_Sel, AV39TFContagemResultadoNotificacao_User, AV40TFContagemResultadoNotificacao_User_Sel, AV43TFContagemResultadoNotificacao_Port, AV44TFContagemResultadoNotificacao_Port_To, AV47TFContagemResultadoNotificacao_Aut_Sel, AV7ContagemResultado_Codigo, AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace, AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace, AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace, AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace, AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace, AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace, AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace, AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace, AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace, AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace, AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace, AV61Codigos, AV69Pgmname, AV51TFContagemResultadoNotificacao_Sec_Sels, AV55TFContagemResultadoNotificacao_Logged_Sels, A1413ContagemResultadoNotificacao_UsuCod, A1412ContagemResultadoNotificacao_Codigo, AV6WWPContext, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAQX2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV69Pgmname = "DemandaNotificacoesWC";
               context.Gx_err = 0;
               WSQX2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Demanda Notificacoes WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206171554254");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("demandanotificacoeswc.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA", context.localUtil.TToC( AV17TFContagemResultadoNotificacao_DataHora, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO", context.localUtil.TToC( AV18TFContagemResultadoNotificacao_DataHora_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM", StringUtil.RTrim( AV23TFContagemResultadoNotificacao_UsuNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL", StringUtil.RTrim( AV24TFContagemResultadoNotificacao_UsuNom_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO", AV64TFContagemResultadoNotificacao_Assunto);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL", AV65TFContagemResultadoNotificacao_Assunto_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27TFContagemResultadoNotificacao_Destinatarios), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28TFContagemResultadoNotificacao_Destinatarios_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFContagemResultadoNotificacao_Demandas), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContagemResultadoNotificacao_Demandas_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_HOST", StringUtil.RTrim( AV35TFContagemResultadoNotificacao_Host));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL", StringUtil.RTrim( AV36TFContagemResultadoNotificacao_Host_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_USER", StringUtil.RTrim( AV39TFContagemResultadoNotificacao_User));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL", StringUtil.RTrim( AV40TFContagemResultadoNotificacao_User_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_PORT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFContagemResultadoNotificacao_Port), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFContagemResultadoNotificacao_Port_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFContagemResultadoNotificacao_Aut_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV57DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV57DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_DATAHORATITLEFILTERDATA", AV16ContagemResultadoNotificacao_DataHoraTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_DATAHORATITLEFILTERDATA", AV16ContagemResultadoNotificacao_DataHoraTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_USUNOMTITLEFILTERDATA", AV22ContagemResultadoNotificacao_UsuNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_USUNOMTITLEFILTERDATA", AV22ContagemResultadoNotificacao_UsuNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLEFILTERDATA", AV63ContagemResultadoNotificacao_AssuntoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLEFILTERDATA", AV63ContagemResultadoNotificacao_AssuntoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLEFILTERDATA", AV26ContagemResultadoNotificacao_DestinatariosTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLEFILTERDATA", AV26ContagemResultadoNotificacao_DestinatariosTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLEFILTERDATA", AV30ContagemResultadoNotificacao_DemandasTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLEFILTERDATA", AV30ContagemResultadoNotificacao_DemandasTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_HOSTTITLEFILTERDATA", AV34ContagemResultadoNotificacao_HostTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_HOSTTITLEFILTERDATA", AV34ContagemResultadoNotificacao_HostTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_USERTITLEFILTERDATA", AV38ContagemResultadoNotificacao_UserTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_USERTITLEFILTERDATA", AV38ContagemResultadoNotificacao_UserTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_PORTTITLEFILTERDATA", AV42ContagemResultadoNotificacao_PortTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_PORTTITLEFILTERDATA", AV42ContagemResultadoNotificacao_PortTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_AUTTITLEFILTERDATA", AV46ContagemResultadoNotificacao_AutTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_AUTTITLEFILTERDATA", AV46ContagemResultadoNotificacao_AutTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_SECTITLEFILTERDATA", AV49ContagemResultadoNotificacao_SecTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_SECTITLEFILTERDATA", AV49ContagemResultadoNotificacao_SecTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLEFILTERDATA", AV53ContagemResultadoNotificacao_LoggedTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLEFILTERDATA", AV53ContagemResultadoNotificacao_LoggedTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCODIGOS", AV61Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCODIGOS", AV61Codigos);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV69Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS", AV51TFContagemResultadoNotificacao_Sec_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS", AV51TFContagemResultadoNotificacao_Sec_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS", AV55TFContagemResultadoNotificacao_Logged_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS", AV55TFContagemResultadoNotificacao_Logged_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Caption", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Tooltip", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Cls", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_datahora_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_datahora_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_datahora_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Filtertype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_datahora_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_datahora_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Sortasc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Caption", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Tooltip", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Cls", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_usunom_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_usunom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_usunom_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Filtertype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_usunom_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_usunom_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Datalisttype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Datalistproc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadonotificacao_usunom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Sortasc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Caption", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Tooltip", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Cls", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_assunto_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_assunto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_assunto_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Filtertype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_assunto_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_assunto_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Datalisttype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Datalistproc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadonotificacao_assunto_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Sortasc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Caption", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Tooltip", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Cls", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_destinatarios_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_destinatarios_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_destinatarios_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Filtertype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_destinatarios_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_destinatarios_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Caption", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Tooltip", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Cls", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_demandas_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_demandas_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_demandas_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Filtertype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_demandas_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_demandas_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Caption", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Tooltip", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Cls", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_host_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_host_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_host_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Filtertype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_host_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_host_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Datalisttype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Datalistproc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadonotificacao_host_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Sortasc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Caption", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Tooltip", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Cls", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_user_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_user_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_user_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Filtertype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_user_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_user_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Datalisttype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Datalistproc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadonotificacao_user_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Sortasc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Caption", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Tooltip", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Cls", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_port_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_port_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_port_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Filtertype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_port_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_port_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Sortasc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Caption", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Tooltip", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Cls", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_aut_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_aut_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_aut_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_aut_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Datalisttype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Sortasc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Caption", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Tooltip", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Cls", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_sec_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_sec_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_sec_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_sec_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Datalisttype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_sec_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Sortasc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Caption", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Tooltip", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Cls", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_logged_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_logged_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_logged_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_logged_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Datalisttype", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contagemresultadonotificacao_logged_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Sortasc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_datahora_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_usunom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_assunto_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_destinatarios_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_demandas_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_host_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_user_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_port_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_aut_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_sec_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultadonotificacao_logged_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormQX2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("demandanotificacoeswc.js", "?20206171554725");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "DemandaNotificacoesWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Demanda Notificacoes WC" ;
      }

      protected void WBQX0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "demandanotificacoeswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_QX2( true) ;
         }
         else
         {
            wb_table1_2_QX2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_QX2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,27);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadonotificacao_datahora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_datahora_Internalname, context.localUtil.TToC( AV17TFContagemResultadoNotificacao_DataHora, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV17TFContagemResultadoNotificacao_DataHora, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,29);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_datahora_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_datahora_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_DemandaNotificacoesWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadonotificacao_datahora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadonotificacao_datahora_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_DemandaNotificacoesWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadonotificacao_datahora_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_datahora_to_Internalname, context.localUtil.TToC( AV18TFContagemResultadoNotificacao_DataHora_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV18TFContagemResultadoNotificacao_DataHora_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,30);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_datahora_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_datahora_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_DemandaNotificacoesWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadonotificacao_datahora_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadonotificacao_datahora_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_DemandaNotificacoesWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultadonotificacao_datahoraauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadonotificacao_datahoraauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadonotificacao_datahoraauxdate_Internalname, context.localUtil.Format(AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate, "99/99/99"), context.localUtil.Format( AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,32);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadonotificacao_datahoraauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_DemandaNotificacoesWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadonotificacao_datahoraauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_DemandaNotificacoesWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadonotificacao_datahoraauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadonotificacao_datahoraauxdateto_Internalname, context.localUtil.Format(AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo, "99/99/99"), context.localUtil.Format( AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,33);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadonotificacao_datahoraauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_DemandaNotificacoesWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadonotificacao_datahoraauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_DemandaNotificacoesWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_usunom_Internalname, StringUtil.RTrim( AV23TFContagemResultadoNotificacao_UsuNom), StringUtil.RTrim( context.localUtil.Format( AV23TFContagemResultadoNotificacao_UsuNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,34);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_usunom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontagemresultadonotificacao_usunom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_usunom_sel_Internalname, StringUtil.RTrim( AV24TFContagemResultadoNotificacao_UsuNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV24TFContagemResultadoNotificacao_UsuNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,35);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_usunom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontagemresultadonotificacao_usunom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_DemandaNotificacoesWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontagemresultadonotificacao_assunto_Internalname, AV64TFContagemResultadoNotificacao_Assunto, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", 0, edtavTfcontagemresultadonotificacao_assunto_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontagemresultadonotificacao_assunto_sel_Internalname, AV65TFContagemResultadoNotificacao_Assunto_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", 0, edtavTfcontagemresultadonotificacao_assunto_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_destinatarios_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27TFContagemResultadoNotificacao_Destinatarios), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV27TFContagemResultadoNotificacao_Destinatarios), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_destinatarios_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_destinatarios_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_destinatarios_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28TFContagemResultadoNotificacao_Destinatarios_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV28TFContagemResultadoNotificacao_Destinatarios_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_destinatarios_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_destinatarios_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_demandas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFContagemResultadoNotificacao_Demandas), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31TFContagemResultadoNotificacao_Demandas), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_demandas_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_demandas_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_demandas_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContagemResultadoNotificacao_Demandas_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFContagemResultadoNotificacao_Demandas_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_demandas_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_demandas_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_host_Internalname, StringUtil.RTrim( AV35TFContagemResultadoNotificacao_Host), StringUtil.RTrim( context.localUtil.Format( AV35TFContagemResultadoNotificacao_Host, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_host_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_host_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_host_sel_Internalname, StringUtil.RTrim( AV36TFContagemResultadoNotificacao_Host_Sel), StringUtil.RTrim( context.localUtil.Format( AV36TFContagemResultadoNotificacao_Host_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_host_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_host_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_user_Internalname, StringUtil.RTrim( AV39TFContagemResultadoNotificacao_User), StringUtil.RTrim( context.localUtil.Format( AV39TFContagemResultadoNotificacao_User, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_user_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_user_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_user_sel_Internalname, StringUtil.RTrim( AV40TFContagemResultadoNotificacao_User_Sel), StringUtil.RTrim( context.localUtil.Format( AV40TFContagemResultadoNotificacao_User_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,45);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_user_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_user_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_port_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFContagemResultadoNotificacao_Port), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43TFContagemResultadoNotificacao_Port), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,46);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_port_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_port_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_port_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFContagemResultadoNotificacao_Port_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFContagemResultadoNotificacao_Port_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,47);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_port_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_port_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_DemandaNotificacoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadonotificacao_aut_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFContagemResultadoNotificacao_Aut_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47TFContagemResultadoNotificacao_Aut_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,48);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadonotificacao_aut_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadonotificacao_aut_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_DemandaNotificacoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadonotificacao_datahoratitlecontrolidtoreplace_Internalname, AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", 0, edtavDdo_contagemresultadonotificacao_datahoratitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadonotificacao_usunomtitlecontrolidtoreplace_Internalname, AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", 0, edtavDdo_contagemresultadonotificacao_usunomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadonotificacao_assuntotitlecontrolidtoreplace_Internalname, AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, edtavDdo_contagemresultadonotificacao_assuntotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadonotificacao_destinatariostitlecontrolidtoreplace_Internalname, AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavDdo_contagemresultadonotificacao_destinatariostitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadonotificacao_demandastitlecontrolidtoreplace_Internalname, AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", 0, edtavDdo_contagemresultadonotificacao_demandastitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOSTContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadonotificacao_hosttitlecontrolidtoreplace_Internalname, AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", 0, edtavDdo_contagemresultadonotificacao_hosttitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USERContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadonotificacao_usertitlecontrolidtoreplace_Internalname, AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", 0, edtavDdo_contagemresultadonotificacao_usertitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORTContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadonotificacao_porttitlecontrolidtoreplace_Internalname, AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", 0, edtavDdo_contagemresultadonotificacao_porttitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUTContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadonotificacao_auttitlecontrolidtoreplace_Internalname, AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", 0, edtavDdo_contagemresultadonotificacao_auttitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SECContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadonotificacao_sectitlecontrolidtoreplace_Internalname, AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", 0, edtavDdo_contagemresultadonotificacao_sectitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadonotificacao_loggedtitlecontrolidtoreplace_Internalname, AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", 0, edtavDdo_contagemresultadonotificacao_loggedtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_DemandaNotificacoesWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTQX2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Demanda Notificacoes WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPQX0( ) ;
            }
         }
      }

      protected void WSQX2( )
      {
         STARTQX2( ) ;
         EVTQX2( ) ;
      }

      protected void EVTQX2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11QX2 */
                                    E11QX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12QX2 */
                                    E12QX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13QX2 */
                                    E13QX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14QX2 */
                                    E14QX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15QX2 */
                                    E15QX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16QX2 */
                                    E16QX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADONOTIFICACAO_HOST.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17QX2 */
                                    E17QX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADONOTIFICACAO_USER.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18QX2 */
                                    E18QX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADONOTIFICACAO_PORT.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19QX2 */
                                    E19QX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADONOTIFICACAO_AUT.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20QX2 */
                                    E20QX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADONOTIFICACAO_SEC.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21QX2 */
                                    E21QX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22QX2 */
                                    E22QX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQX0( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                              A1412ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Codigo_Internalname), ",", "."));
                              A1413ContagemResultadoNotificacao_UsuCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_UsuCod_Internalname), ",", "."));
                              A1416ContagemResultadoNotificacao_DataHora = context.localUtil.CToT( cgiGet( edtContagemResultadoNotificacao_DataHora_Internalname), 0);
                              n1416ContagemResultadoNotificacao_DataHora = false;
                              A1422ContagemResultadoNotificacao_UsuNom = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_UsuNom_Internalname));
                              n1422ContagemResultadoNotificacao_UsuNom = false;
                              A1417ContagemResultadoNotificacao_Assunto = cgiGet( edtContagemResultadoNotificacao_Assunto_Internalname);
                              n1417ContagemResultadoNotificacao_Assunto = false;
                              A1962ContagemResultadoNotificacao_Destinatarios = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Destinatarios_Internalname), ",", "."));
                              n1962ContagemResultadoNotificacao_Destinatarios = false;
                              A1963ContagemResultadoNotificacao_Demandas = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Demandas_Internalname), ",", "."));
                              n1963ContagemResultadoNotificacao_Demandas = false;
                              A1956ContagemResultadoNotificacao_Host = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_Host_Internalname));
                              n1956ContagemResultadoNotificacao_Host = false;
                              A1957ContagemResultadoNotificacao_User = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_User_Internalname));
                              n1957ContagemResultadoNotificacao_User = false;
                              A1958ContagemResultadoNotificacao_Port = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Port_Internalname), ",", "."));
                              n1958ContagemResultadoNotificacao_Port = false;
                              cmbContagemResultadoNotificacao_Aut.Name = cmbContagemResultadoNotificacao_Aut_Internalname;
                              cmbContagemResultadoNotificacao_Aut.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Aut_Internalname);
                              A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cgiGet( cmbContagemResultadoNotificacao_Aut_Internalname));
                              n1959ContagemResultadoNotificacao_Aut = false;
                              cmbContagemResultadoNotificacao_Sec.Name = cmbContagemResultadoNotificacao_Sec_Internalname;
                              cmbContagemResultadoNotificacao_Sec.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Sec_Internalname);
                              A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cgiGet( cmbContagemResultadoNotificacao_Sec_Internalname), "."));
                              n1960ContagemResultadoNotificacao_Sec = false;
                              cmbContagemResultadoNotificacao_Logged.Name = cmbContagemResultadoNotificacao_Logged_Internalname;
                              cmbContagemResultadoNotificacao_Logged.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Logged_Internalname);
                              A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cgiGet( cmbContagemResultadoNotificacao_Logged_Internalname), "."));
                              n1961ContagemResultadoNotificacao_Logged = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23QX2 */
                                          E23QX2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24QX2 */
                                          E24QX2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25QX2 */
                                          E25QX2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_datahora Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA"), 0) != AV17TFContagemResultadoNotificacao_DataHora )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_datahora_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO"), 0) != AV18TFContagemResultadoNotificacao_DataHora_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_usunom Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM"), AV23TFContagemResultadoNotificacao_UsuNom) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_usunom_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL"), AV24TFContagemResultadoNotificacao_UsuNom_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_assunto Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO"), AV64TFContagemResultadoNotificacao_Assunto) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_assunto_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL"), AV65TFContagemResultadoNotificacao_Assunto_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_destinatarios Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS"), ",", ".") != Convert.ToDecimal( AV27TFContagemResultadoNotificacao_Destinatarios )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_destinatarios_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO"), ",", ".") != Convert.ToDecimal( AV28TFContagemResultadoNotificacao_Destinatarios_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_demandas Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS"), ",", ".") != Convert.ToDecimal( AV31TFContagemResultadoNotificacao_Demandas )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_demandas_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO"), ",", ".") != Convert.ToDecimal( AV32TFContagemResultadoNotificacao_Demandas_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_host Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_HOST"), AV35TFContagemResultadoNotificacao_Host) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_host_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL"), AV36TFContagemResultadoNotificacao_Host_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_user Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_USER"), AV39TFContagemResultadoNotificacao_User) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_user_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL"), AV40TFContagemResultadoNotificacao_User_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_port Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_PORT"), ",", ".") != Convert.ToDecimal( AV43TFContagemResultadoNotificacao_Port )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_port_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO"), ",", ".") != Convert.ToDecimal( AV44TFContagemResultadoNotificacao_Port_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontagemresultadonotificacao_aut_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL"), ",", ".") != Convert.ToDecimal( AV47TFContagemResultadoNotificacao_Aut_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPQX0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEQX2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormQX2( ) ;
            }
         }
      }

      protected void PAQX2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_AUT_" + sGXsfl_8_idx;
            cmbContagemResultadoNotificacao_Aut.Name = GXCCtl;
            cmbContagemResultadoNotificacao_Aut.WebTags = "";
            cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContagemResultadoNotificacao_Aut.ItemCount > 0 )
            {
               A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cmbContagemResultadoNotificacao_Aut.getValidValue(StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut)));
               n1959ContagemResultadoNotificacao_Aut = false;
            }
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_SEC_" + sGXsfl_8_idx;
            cmbContagemResultadoNotificacao_Sec.Name = GXCCtl;
            cmbContagemResultadoNotificacao_Sec.WebTags = "";
            cmbContagemResultadoNotificacao_Sec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
            cmbContagemResultadoNotificacao_Sec.addItem("1", "SSL e TLS", 0);
            if ( cmbContagemResultadoNotificacao_Sec.ItemCount > 0 )
            {
               A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Sec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0))), "."));
               n1960ContagemResultadoNotificacao_Sec = false;
            }
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_LOGGED_" + sGXsfl_8_idx;
            cmbContagemResultadoNotificacao_Logged.Name = GXCCtl;
            cmbContagemResultadoNotificacao_Logged.WebTags = "";
            cmbContagemResultadoNotificacao_Logged.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Sem dados", 0);
            cmbContagemResultadoNotificacao_Logged.addItem("1", "Com sucesso", 0);
            cmbContagemResultadoNotificacao_Logged.addItem("2", "Mal sucedido", 0);
            if ( cmbContagemResultadoNotificacao_Logged.ItemCount > 0 )
            {
               A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Logged.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0))), "."));
               n1961ContagemResultadoNotificacao_Logged = false;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       DateTime AV17TFContagemResultadoNotificacao_DataHora ,
                                       DateTime AV18TFContagemResultadoNotificacao_DataHora_To ,
                                       String AV23TFContagemResultadoNotificacao_UsuNom ,
                                       String AV24TFContagemResultadoNotificacao_UsuNom_Sel ,
                                       String AV64TFContagemResultadoNotificacao_Assunto ,
                                       String AV65TFContagemResultadoNotificacao_Assunto_Sel ,
                                       int AV27TFContagemResultadoNotificacao_Destinatarios ,
                                       int AV28TFContagemResultadoNotificacao_Destinatarios_To ,
                                       int AV31TFContagemResultadoNotificacao_Demandas ,
                                       int AV32TFContagemResultadoNotificacao_Demandas_To ,
                                       String AV35TFContagemResultadoNotificacao_Host ,
                                       String AV36TFContagemResultadoNotificacao_Host_Sel ,
                                       String AV39TFContagemResultadoNotificacao_User ,
                                       String AV40TFContagemResultadoNotificacao_User_Sel ,
                                       short AV43TFContagemResultadoNotificacao_Port ,
                                       short AV44TFContagemResultadoNotificacao_Port_To ,
                                       short AV47TFContagemResultadoNotificacao_Aut_Sel ,
                                       int AV7ContagemResultado_Codigo ,
                                       String AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace ,
                                       String AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace ,
                                       String AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace ,
                                       String AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace ,
                                       String AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace ,
                                       String AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace ,
                                       String AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace ,
                                       String AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace ,
                                       String AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace ,
                                       String AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace ,
                                       String AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace ,
                                       IGxCollection AV61Codigos ,
                                       String AV69Pgmname ,
                                       IGxCollection AV51TFContagemResultadoNotificacao_Sec_Sels ,
                                       IGxCollection AV55TFContagemResultadoNotificacao_Logged_Sels ,
                                       int A1413ContagemResultadoNotificacao_UsuCod ,
                                       long A1412ContagemResultadoNotificacao_Codigo ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFQX2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADONOTIFICACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFQX2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV69Pgmname = "DemandaNotificacoesWC";
         context.Gx_err = 0;
      }

      protected void RFQX2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E24QX2 */
         E24QX2 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A1960ContagemResultadoNotificacao_Sec ,
                                                 AV51TFContagemResultadoNotificacao_Sec_Sels ,
                                                 A1961ContagemResultadoNotificacao_Logged ,
                                                 AV55TFContagemResultadoNotificacao_Logged_Sels ,
                                                 AV17TFContagemResultadoNotificacao_DataHora ,
                                                 AV18TFContagemResultadoNotificacao_DataHora_To ,
                                                 AV24TFContagemResultadoNotificacao_UsuNom_Sel ,
                                                 AV23TFContagemResultadoNotificacao_UsuNom ,
                                                 AV65TFContagemResultadoNotificacao_Assunto_Sel ,
                                                 AV64TFContagemResultadoNotificacao_Assunto ,
                                                 AV36TFContagemResultadoNotificacao_Host_Sel ,
                                                 AV35TFContagemResultadoNotificacao_Host ,
                                                 AV40TFContagemResultadoNotificacao_User_Sel ,
                                                 AV39TFContagemResultadoNotificacao_User ,
                                                 AV43TFContagemResultadoNotificacao_Port ,
                                                 AV44TFContagemResultadoNotificacao_Port_To ,
                                                 AV47TFContagemResultadoNotificacao_Aut_Sel ,
                                                 AV51TFContagemResultadoNotificacao_Sec_Sels.Count ,
                                                 AV55TFContagemResultadoNotificacao_Logged_Sels.Count ,
                                                 A1416ContagemResultadoNotificacao_DataHora ,
                                                 A1422ContagemResultadoNotificacao_UsuNom ,
                                                 A1417ContagemResultadoNotificacao_Assunto ,
                                                 A1956ContagemResultadoNotificacao_Host ,
                                                 A1957ContagemResultadoNotificacao_User ,
                                                 A1958ContagemResultadoNotificacao_Port ,
                                                 A1959ContagemResultadoNotificacao_Aut ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A456ContagemResultado_Codigo ,
                                                 AV7ContagemResultado_Codigo ,
                                                 AV27TFContagemResultadoNotificacao_Destinatarios ,
                                                 A1962ContagemResultadoNotificacao_Destinatarios ,
                                                 AV28TFContagemResultadoNotificacao_Destinatarios_To ,
                                                 AV31TFContagemResultadoNotificacao_Demandas ,
                                                 A1963ContagemResultadoNotificacao_Demandas ,
                                                 AV32TFContagemResultadoNotificacao_Demandas_To },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV23TFContagemResultadoNotificacao_UsuNom = StringUtil.PadR( StringUtil.RTrim( AV23TFContagemResultadoNotificacao_UsuNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagemResultadoNotificacao_UsuNom", AV23TFContagemResultadoNotificacao_UsuNom);
            lV64TFContagemResultadoNotificacao_Assunto = StringUtil.Concat( StringUtil.RTrim( AV64TFContagemResultadoNotificacao_Assunto), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFContagemResultadoNotificacao_Assunto", AV64TFContagemResultadoNotificacao_Assunto);
            lV35TFContagemResultadoNotificacao_Host = StringUtil.PadR( StringUtil.RTrim( AV35TFContagemResultadoNotificacao_Host), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFContagemResultadoNotificacao_Host", AV35TFContagemResultadoNotificacao_Host);
            lV39TFContagemResultadoNotificacao_User = StringUtil.PadR( StringUtil.RTrim( AV39TFContagemResultadoNotificacao_User), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFContagemResultadoNotificacao_User", AV39TFContagemResultadoNotificacao_User);
            /* Using cursor H00QX4 */
            pr_default.execute(0, new Object[] {AV7ContagemResultado_Codigo, AV27TFContagemResultadoNotificacao_Destinatarios, AV27TFContagemResultadoNotificacao_Destinatarios, AV28TFContagemResultadoNotificacao_Destinatarios_To, AV28TFContagemResultadoNotificacao_Destinatarios_To, AV31TFContagemResultadoNotificacao_Demandas, AV31TFContagemResultadoNotificacao_Demandas, AV32TFContagemResultadoNotificacao_Demandas_To, AV32TFContagemResultadoNotificacao_Demandas_To, AV17TFContagemResultadoNotificacao_DataHora, AV18TFContagemResultadoNotificacao_DataHora_To, lV23TFContagemResultadoNotificacao_UsuNom, AV24TFContagemResultadoNotificacao_UsuNom_Sel, lV64TFContagemResultadoNotificacao_Assunto, AV65TFContagemResultadoNotificacao_Assunto_Sel, lV35TFContagemResultadoNotificacao_Host, AV36TFContagemResultadoNotificacao_Host_Sel, lV39TFContagemResultadoNotificacao_User, AV40TFContagemResultadoNotificacao_User_Sel, AV43TFContagemResultadoNotificacao_Port, AV44TFContagemResultadoNotificacao_Port_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1427ContagemResultadoNotificacao_UsuPesCod = H00QX4_A1427ContagemResultadoNotificacao_UsuPesCod[0];
               n1427ContagemResultadoNotificacao_UsuPesCod = H00QX4_n1427ContagemResultadoNotificacao_UsuPesCod[0];
               A1961ContagemResultadoNotificacao_Logged = H00QX4_A1961ContagemResultadoNotificacao_Logged[0];
               n1961ContagemResultadoNotificacao_Logged = H00QX4_n1961ContagemResultadoNotificacao_Logged[0];
               A1960ContagemResultadoNotificacao_Sec = H00QX4_A1960ContagemResultadoNotificacao_Sec[0];
               n1960ContagemResultadoNotificacao_Sec = H00QX4_n1960ContagemResultadoNotificacao_Sec[0];
               A1959ContagemResultadoNotificacao_Aut = H00QX4_A1959ContagemResultadoNotificacao_Aut[0];
               n1959ContagemResultadoNotificacao_Aut = H00QX4_n1959ContagemResultadoNotificacao_Aut[0];
               A1958ContagemResultadoNotificacao_Port = H00QX4_A1958ContagemResultadoNotificacao_Port[0];
               n1958ContagemResultadoNotificacao_Port = H00QX4_n1958ContagemResultadoNotificacao_Port[0];
               A1957ContagemResultadoNotificacao_User = H00QX4_A1957ContagemResultadoNotificacao_User[0];
               n1957ContagemResultadoNotificacao_User = H00QX4_n1957ContagemResultadoNotificacao_User[0];
               A1956ContagemResultadoNotificacao_Host = H00QX4_A1956ContagemResultadoNotificacao_Host[0];
               n1956ContagemResultadoNotificacao_Host = H00QX4_n1956ContagemResultadoNotificacao_Host[0];
               A1417ContagemResultadoNotificacao_Assunto = H00QX4_A1417ContagemResultadoNotificacao_Assunto[0];
               n1417ContagemResultadoNotificacao_Assunto = H00QX4_n1417ContagemResultadoNotificacao_Assunto[0];
               A1422ContagemResultadoNotificacao_UsuNom = H00QX4_A1422ContagemResultadoNotificacao_UsuNom[0];
               n1422ContagemResultadoNotificacao_UsuNom = H00QX4_n1422ContagemResultadoNotificacao_UsuNom[0];
               A1416ContagemResultadoNotificacao_DataHora = H00QX4_A1416ContagemResultadoNotificacao_DataHora[0];
               n1416ContagemResultadoNotificacao_DataHora = H00QX4_n1416ContagemResultadoNotificacao_DataHora[0];
               A1413ContagemResultadoNotificacao_UsuCod = H00QX4_A1413ContagemResultadoNotificacao_UsuCod[0];
               A1412ContagemResultadoNotificacao_Codigo = H00QX4_A1412ContagemResultadoNotificacao_Codigo[0];
               A456ContagemResultado_Codigo = H00QX4_A456ContagemResultado_Codigo[0];
               A1963ContagemResultadoNotificacao_Demandas = H00QX4_A1963ContagemResultadoNotificacao_Demandas[0];
               n1963ContagemResultadoNotificacao_Demandas = H00QX4_n1963ContagemResultadoNotificacao_Demandas[0];
               A1962ContagemResultadoNotificacao_Destinatarios = H00QX4_A1962ContagemResultadoNotificacao_Destinatarios[0];
               n1962ContagemResultadoNotificacao_Destinatarios = H00QX4_n1962ContagemResultadoNotificacao_Destinatarios[0];
               A1961ContagemResultadoNotificacao_Logged = H00QX4_A1961ContagemResultadoNotificacao_Logged[0];
               n1961ContagemResultadoNotificacao_Logged = H00QX4_n1961ContagemResultadoNotificacao_Logged[0];
               A1960ContagemResultadoNotificacao_Sec = H00QX4_A1960ContagemResultadoNotificacao_Sec[0];
               n1960ContagemResultadoNotificacao_Sec = H00QX4_n1960ContagemResultadoNotificacao_Sec[0];
               A1959ContagemResultadoNotificacao_Aut = H00QX4_A1959ContagemResultadoNotificacao_Aut[0];
               n1959ContagemResultadoNotificacao_Aut = H00QX4_n1959ContagemResultadoNotificacao_Aut[0];
               A1958ContagemResultadoNotificacao_Port = H00QX4_A1958ContagemResultadoNotificacao_Port[0];
               n1958ContagemResultadoNotificacao_Port = H00QX4_n1958ContagemResultadoNotificacao_Port[0];
               A1957ContagemResultadoNotificacao_User = H00QX4_A1957ContagemResultadoNotificacao_User[0];
               n1957ContagemResultadoNotificacao_User = H00QX4_n1957ContagemResultadoNotificacao_User[0];
               A1956ContagemResultadoNotificacao_Host = H00QX4_A1956ContagemResultadoNotificacao_Host[0];
               n1956ContagemResultadoNotificacao_Host = H00QX4_n1956ContagemResultadoNotificacao_Host[0];
               A1417ContagemResultadoNotificacao_Assunto = H00QX4_A1417ContagemResultadoNotificacao_Assunto[0];
               n1417ContagemResultadoNotificacao_Assunto = H00QX4_n1417ContagemResultadoNotificacao_Assunto[0];
               A1416ContagemResultadoNotificacao_DataHora = H00QX4_A1416ContagemResultadoNotificacao_DataHora[0];
               n1416ContagemResultadoNotificacao_DataHora = H00QX4_n1416ContagemResultadoNotificacao_DataHora[0];
               A1413ContagemResultadoNotificacao_UsuCod = H00QX4_A1413ContagemResultadoNotificacao_UsuCod[0];
               A1427ContagemResultadoNotificacao_UsuPesCod = H00QX4_A1427ContagemResultadoNotificacao_UsuPesCod[0];
               n1427ContagemResultadoNotificacao_UsuPesCod = H00QX4_n1427ContagemResultadoNotificacao_UsuPesCod[0];
               A1422ContagemResultadoNotificacao_UsuNom = H00QX4_A1422ContagemResultadoNotificacao_UsuNom[0];
               n1422ContagemResultadoNotificacao_UsuNom = H00QX4_n1422ContagemResultadoNotificacao_UsuNom[0];
               A1963ContagemResultadoNotificacao_Demandas = H00QX4_A1963ContagemResultadoNotificacao_Demandas[0];
               n1963ContagemResultadoNotificacao_Demandas = H00QX4_n1963ContagemResultadoNotificacao_Demandas[0];
               A1962ContagemResultadoNotificacao_Destinatarios = H00QX4_A1962ContagemResultadoNotificacao_Destinatarios[0];
               n1962ContagemResultadoNotificacao_Destinatarios = H00QX4_n1962ContagemResultadoNotificacao_Destinatarios[0];
               /* Execute user event: E25QX2 */
               E25QX2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 8;
            WBQX0( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1960ContagemResultadoNotificacao_Sec ,
                                              AV51TFContagemResultadoNotificacao_Sec_Sels ,
                                              A1961ContagemResultadoNotificacao_Logged ,
                                              AV55TFContagemResultadoNotificacao_Logged_Sels ,
                                              AV17TFContagemResultadoNotificacao_DataHora ,
                                              AV18TFContagemResultadoNotificacao_DataHora_To ,
                                              AV24TFContagemResultadoNotificacao_UsuNom_Sel ,
                                              AV23TFContagemResultadoNotificacao_UsuNom ,
                                              AV65TFContagemResultadoNotificacao_Assunto_Sel ,
                                              AV64TFContagemResultadoNotificacao_Assunto ,
                                              AV36TFContagemResultadoNotificacao_Host_Sel ,
                                              AV35TFContagemResultadoNotificacao_Host ,
                                              AV40TFContagemResultadoNotificacao_User_Sel ,
                                              AV39TFContagemResultadoNotificacao_User ,
                                              AV43TFContagemResultadoNotificacao_Port ,
                                              AV44TFContagemResultadoNotificacao_Port_To ,
                                              AV47TFContagemResultadoNotificacao_Aut_Sel ,
                                              AV51TFContagemResultadoNotificacao_Sec_Sels.Count ,
                                              AV55TFContagemResultadoNotificacao_Logged_Sels.Count ,
                                              A1416ContagemResultadoNotificacao_DataHora ,
                                              A1422ContagemResultadoNotificacao_UsuNom ,
                                              A1417ContagemResultadoNotificacao_Assunto ,
                                              A1956ContagemResultadoNotificacao_Host ,
                                              A1957ContagemResultadoNotificacao_User ,
                                              A1958ContagemResultadoNotificacao_Port ,
                                              A1959ContagemResultadoNotificacao_Aut ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A456ContagemResultado_Codigo ,
                                              AV7ContagemResultado_Codigo ,
                                              AV27TFContagemResultadoNotificacao_Destinatarios ,
                                              A1962ContagemResultadoNotificacao_Destinatarios ,
                                              AV28TFContagemResultadoNotificacao_Destinatarios_To ,
                                              AV31TFContagemResultadoNotificacao_Demandas ,
                                              A1963ContagemResultadoNotificacao_Demandas ,
                                              AV32TFContagemResultadoNotificacao_Demandas_To },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV23TFContagemResultadoNotificacao_UsuNom = StringUtil.PadR( StringUtil.RTrim( AV23TFContagemResultadoNotificacao_UsuNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagemResultadoNotificacao_UsuNom", AV23TFContagemResultadoNotificacao_UsuNom);
         lV64TFContagemResultadoNotificacao_Assunto = StringUtil.Concat( StringUtil.RTrim( AV64TFContagemResultadoNotificacao_Assunto), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFContagemResultadoNotificacao_Assunto", AV64TFContagemResultadoNotificacao_Assunto);
         lV35TFContagemResultadoNotificacao_Host = StringUtil.PadR( StringUtil.RTrim( AV35TFContagemResultadoNotificacao_Host), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFContagemResultadoNotificacao_Host", AV35TFContagemResultadoNotificacao_Host);
         lV39TFContagemResultadoNotificacao_User = StringUtil.PadR( StringUtil.RTrim( AV39TFContagemResultadoNotificacao_User), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFContagemResultadoNotificacao_User", AV39TFContagemResultadoNotificacao_User);
         /* Using cursor H00QX7 */
         pr_default.execute(1, new Object[] {AV7ContagemResultado_Codigo, AV27TFContagemResultadoNotificacao_Destinatarios, AV27TFContagemResultadoNotificacao_Destinatarios, AV28TFContagemResultadoNotificacao_Destinatarios_To, AV28TFContagemResultadoNotificacao_Destinatarios_To, AV31TFContagemResultadoNotificacao_Demandas, AV31TFContagemResultadoNotificacao_Demandas, AV32TFContagemResultadoNotificacao_Demandas_To, AV32TFContagemResultadoNotificacao_Demandas_To, AV17TFContagemResultadoNotificacao_DataHora, AV18TFContagemResultadoNotificacao_DataHora_To, lV23TFContagemResultadoNotificacao_UsuNom, AV24TFContagemResultadoNotificacao_UsuNom_Sel, lV64TFContagemResultadoNotificacao_Assunto, AV65TFContagemResultadoNotificacao_Assunto_Sel, lV35TFContagemResultadoNotificacao_Host, AV36TFContagemResultadoNotificacao_Host_Sel, lV39TFContagemResultadoNotificacao_User, AV40TFContagemResultadoNotificacao_User_Sel, AV43TFContagemResultadoNotificacao_Port, AV44TFContagemResultadoNotificacao_Port_To});
         GRID_nRecordCount = H00QX7_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContagemResultadoNotificacao_DataHora, AV18TFContagemResultadoNotificacao_DataHora_To, AV23TFContagemResultadoNotificacao_UsuNom, AV24TFContagemResultadoNotificacao_UsuNom_Sel, AV64TFContagemResultadoNotificacao_Assunto, AV65TFContagemResultadoNotificacao_Assunto_Sel, AV27TFContagemResultadoNotificacao_Destinatarios, AV28TFContagemResultadoNotificacao_Destinatarios_To, AV31TFContagemResultadoNotificacao_Demandas, AV32TFContagemResultadoNotificacao_Demandas_To, AV35TFContagemResultadoNotificacao_Host, AV36TFContagemResultadoNotificacao_Host_Sel, AV39TFContagemResultadoNotificacao_User, AV40TFContagemResultadoNotificacao_User_Sel, AV43TFContagemResultadoNotificacao_Port, AV44TFContagemResultadoNotificacao_Port_To, AV47TFContagemResultadoNotificacao_Aut_Sel, AV7ContagemResultado_Codigo, AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace, AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace, AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace, AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace, AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace, AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace, AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace, AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace, AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace, AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace, AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace, AV61Codigos, AV69Pgmname, AV51TFContagemResultadoNotificacao_Sec_Sels, AV55TFContagemResultadoNotificacao_Logged_Sels, A1413ContagemResultadoNotificacao_UsuCod, A1412ContagemResultadoNotificacao_Codigo, AV6WWPContext, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContagemResultadoNotificacao_DataHora, AV18TFContagemResultadoNotificacao_DataHora_To, AV23TFContagemResultadoNotificacao_UsuNom, AV24TFContagemResultadoNotificacao_UsuNom_Sel, AV64TFContagemResultadoNotificacao_Assunto, AV65TFContagemResultadoNotificacao_Assunto_Sel, AV27TFContagemResultadoNotificacao_Destinatarios, AV28TFContagemResultadoNotificacao_Destinatarios_To, AV31TFContagemResultadoNotificacao_Demandas, AV32TFContagemResultadoNotificacao_Demandas_To, AV35TFContagemResultadoNotificacao_Host, AV36TFContagemResultadoNotificacao_Host_Sel, AV39TFContagemResultadoNotificacao_User, AV40TFContagemResultadoNotificacao_User_Sel, AV43TFContagemResultadoNotificacao_Port, AV44TFContagemResultadoNotificacao_Port_To, AV47TFContagemResultadoNotificacao_Aut_Sel, AV7ContagemResultado_Codigo, AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace, AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace, AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace, AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace, AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace, AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace, AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace, AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace, AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace, AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace, AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace, AV61Codigos, AV69Pgmname, AV51TFContagemResultadoNotificacao_Sec_Sels, AV55TFContagemResultadoNotificacao_Logged_Sels, A1413ContagemResultadoNotificacao_UsuCod, A1412ContagemResultadoNotificacao_Codigo, AV6WWPContext, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContagemResultadoNotificacao_DataHora, AV18TFContagemResultadoNotificacao_DataHora_To, AV23TFContagemResultadoNotificacao_UsuNom, AV24TFContagemResultadoNotificacao_UsuNom_Sel, AV64TFContagemResultadoNotificacao_Assunto, AV65TFContagemResultadoNotificacao_Assunto_Sel, AV27TFContagemResultadoNotificacao_Destinatarios, AV28TFContagemResultadoNotificacao_Destinatarios_To, AV31TFContagemResultadoNotificacao_Demandas, AV32TFContagemResultadoNotificacao_Demandas_To, AV35TFContagemResultadoNotificacao_Host, AV36TFContagemResultadoNotificacao_Host_Sel, AV39TFContagemResultadoNotificacao_User, AV40TFContagemResultadoNotificacao_User_Sel, AV43TFContagemResultadoNotificacao_Port, AV44TFContagemResultadoNotificacao_Port_To, AV47TFContagemResultadoNotificacao_Aut_Sel, AV7ContagemResultado_Codigo, AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace, AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace, AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace, AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace, AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace, AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace, AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace, AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace, AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace, AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace, AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace, AV61Codigos, AV69Pgmname, AV51TFContagemResultadoNotificacao_Sec_Sels, AV55TFContagemResultadoNotificacao_Logged_Sels, A1413ContagemResultadoNotificacao_UsuCod, A1412ContagemResultadoNotificacao_Codigo, AV6WWPContext, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContagemResultadoNotificacao_DataHora, AV18TFContagemResultadoNotificacao_DataHora_To, AV23TFContagemResultadoNotificacao_UsuNom, AV24TFContagemResultadoNotificacao_UsuNom_Sel, AV64TFContagemResultadoNotificacao_Assunto, AV65TFContagemResultadoNotificacao_Assunto_Sel, AV27TFContagemResultadoNotificacao_Destinatarios, AV28TFContagemResultadoNotificacao_Destinatarios_To, AV31TFContagemResultadoNotificacao_Demandas, AV32TFContagemResultadoNotificacao_Demandas_To, AV35TFContagemResultadoNotificacao_Host, AV36TFContagemResultadoNotificacao_Host_Sel, AV39TFContagemResultadoNotificacao_User, AV40TFContagemResultadoNotificacao_User_Sel, AV43TFContagemResultadoNotificacao_Port, AV44TFContagemResultadoNotificacao_Port_To, AV47TFContagemResultadoNotificacao_Aut_Sel, AV7ContagemResultado_Codigo, AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace, AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace, AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace, AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace, AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace, AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace, AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace, AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace, AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace, AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace, AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace, AV61Codigos, AV69Pgmname, AV51TFContagemResultadoNotificacao_Sec_Sels, AV55TFContagemResultadoNotificacao_Logged_Sels, A1413ContagemResultadoNotificacao_UsuCod, A1412ContagemResultadoNotificacao_Codigo, AV6WWPContext, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFContagemResultadoNotificacao_DataHora, AV18TFContagemResultadoNotificacao_DataHora_To, AV23TFContagemResultadoNotificacao_UsuNom, AV24TFContagemResultadoNotificacao_UsuNom_Sel, AV64TFContagemResultadoNotificacao_Assunto, AV65TFContagemResultadoNotificacao_Assunto_Sel, AV27TFContagemResultadoNotificacao_Destinatarios, AV28TFContagemResultadoNotificacao_Destinatarios_To, AV31TFContagemResultadoNotificacao_Demandas, AV32TFContagemResultadoNotificacao_Demandas_To, AV35TFContagemResultadoNotificacao_Host, AV36TFContagemResultadoNotificacao_Host_Sel, AV39TFContagemResultadoNotificacao_User, AV40TFContagemResultadoNotificacao_User_Sel, AV43TFContagemResultadoNotificacao_Port, AV44TFContagemResultadoNotificacao_Port_To, AV47TFContagemResultadoNotificacao_Aut_Sel, AV7ContagemResultado_Codigo, AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace, AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace, AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace, AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace, AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace, AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace, AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace, AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace, AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace, AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace, AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace, AV61Codigos, AV69Pgmname, AV51TFContagemResultadoNotificacao_Sec_Sels, AV55TFContagemResultadoNotificacao_Logged_Sels, A1413ContagemResultadoNotificacao_UsuCod, A1412ContagemResultadoNotificacao_Codigo, AV6WWPContext, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPQX0( )
      {
         /* Before Start, stand alone formulas. */
         AV69Pgmname = "DemandaNotificacoesWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23QX2 */
         E23QX2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV57DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_DATAHORATITLEFILTERDATA"), AV16ContagemResultadoNotificacao_DataHoraTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_USUNOMTITLEFILTERDATA"), AV22ContagemResultadoNotificacao_UsuNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLEFILTERDATA"), AV63ContagemResultadoNotificacao_AssuntoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLEFILTERDATA"), AV26ContagemResultadoNotificacao_DestinatariosTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLEFILTERDATA"), AV30ContagemResultadoNotificacao_DemandasTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_HOSTTITLEFILTERDATA"), AV34ContagemResultadoNotificacao_HostTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_USERTITLEFILTERDATA"), AV38ContagemResultadoNotificacao_UserTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_PORTTITLEFILTERDATA"), AV42ContagemResultadoNotificacao_PortTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_AUTTITLEFILTERDATA"), AV46ContagemResultadoNotificacao_AutTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_SECTITLEFILTERDATA"), AV49ContagemResultadoNotificacao_SecTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLEFILTERDATA"), AV53ContagemResultadoNotificacao_LoggedTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadonotificacao_datahora_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Notificacao_Data Hora"}), 1, "vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA");
               GX_FocusControl = edtavTfcontagemresultadonotificacao_datahora_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17TFContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContagemResultadoNotificacao_DataHora", context.localUtil.TToC( AV17TFContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV17TFContagemResultadoNotificacao_DataHora = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadonotificacao_datahora_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContagemResultadoNotificacao_DataHora", context.localUtil.TToC( AV17TFContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadonotificacao_datahora_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Notificacao_Data Hora_To"}), 1, "vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO");
               GX_FocusControl = edtavTfcontagemresultadonotificacao_datahora_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18TFContagemResultadoNotificacao_DataHora_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContagemResultadoNotificacao_DataHora_To", context.localUtil.TToC( AV18TFContagemResultadoNotificacao_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV18TFContagemResultadoNotificacao_DataHora_To = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadonotificacao_datahora_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContagemResultadoNotificacao_DataHora_To", context.localUtil.TToC( AV18TFContagemResultadoNotificacao_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadonotificacao_datahoraauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Notificacao_Data Hora Aux Date"}), 1, "vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORAAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultadonotificacao_datahoraauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate", context.localUtil.Format(AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate, "99/99/99"));
            }
            else
            {
               AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadonotificacao_datahoraauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate", context.localUtil.Format(AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadonotificacao_datahoraauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Notificacao_Data Hora Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORAAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultadonotificacao_datahoraauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo", context.localUtil.Format(AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo, "99/99/99"));
            }
            else
            {
               AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadonotificacao_datahoraauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo", context.localUtil.Format(AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo, "99/99/99"));
            }
            AV23TFContagemResultadoNotificacao_UsuNom = StringUtil.Upper( cgiGet( edtavTfcontagemresultadonotificacao_usunom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagemResultadoNotificacao_UsuNom", AV23TFContagemResultadoNotificacao_UsuNom);
            AV24TFContagemResultadoNotificacao_UsuNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemresultadonotificacao_usunom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContagemResultadoNotificacao_UsuNom_Sel", AV24TFContagemResultadoNotificacao_UsuNom_Sel);
            AV64TFContagemResultadoNotificacao_Assunto = cgiGet( edtavTfcontagemresultadonotificacao_assunto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFContagemResultadoNotificacao_Assunto", AV64TFContagemResultadoNotificacao_Assunto);
            AV65TFContagemResultadoNotificacao_Assunto_Sel = cgiGet( edtavTfcontagemresultadonotificacao_assunto_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65TFContagemResultadoNotificacao_Assunto_Sel", AV65TFContagemResultadoNotificacao_Assunto_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_destinatarios_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_destinatarios_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS");
               GX_FocusControl = edtavTfcontagemresultadonotificacao_destinatarios_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV27TFContagemResultadoNotificacao_Destinatarios = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27TFContagemResultadoNotificacao_Destinatarios), 6, 0)));
            }
            else
            {
               AV27TFContagemResultadoNotificacao_Destinatarios = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_destinatarios_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27TFContagemResultadoNotificacao_Destinatarios), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_destinatarios_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_destinatarios_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO");
               GX_FocusControl = edtavTfcontagemresultadonotificacao_destinatarios_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28TFContagemResultadoNotificacao_Destinatarios_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContagemResultadoNotificacao_Destinatarios_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28TFContagemResultadoNotificacao_Destinatarios_To), 6, 0)));
            }
            else
            {
               AV28TFContagemResultadoNotificacao_Destinatarios_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_destinatarios_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContagemResultadoNotificacao_Destinatarios_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28TFContagemResultadoNotificacao_Destinatarios_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_demandas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_demandas_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS");
               GX_FocusControl = edtavTfcontagemresultadonotificacao_demandas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFContagemResultadoNotificacao_Demandas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContagemResultadoNotificacao_Demandas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContagemResultadoNotificacao_Demandas), 6, 0)));
            }
            else
            {
               AV31TFContagemResultadoNotificacao_Demandas = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_demandas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContagemResultadoNotificacao_Demandas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContagemResultadoNotificacao_Demandas), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_demandas_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_demandas_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO");
               GX_FocusControl = edtavTfcontagemresultadonotificacao_demandas_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFContagemResultadoNotificacao_Demandas_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFContagemResultadoNotificacao_Demandas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContagemResultadoNotificacao_Demandas_To), 6, 0)));
            }
            else
            {
               AV32TFContagemResultadoNotificacao_Demandas_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_demandas_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFContagemResultadoNotificacao_Demandas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContagemResultadoNotificacao_Demandas_To), 6, 0)));
            }
            AV35TFContagemResultadoNotificacao_Host = StringUtil.Upper( cgiGet( edtavTfcontagemresultadonotificacao_host_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFContagemResultadoNotificacao_Host", AV35TFContagemResultadoNotificacao_Host);
            AV36TFContagemResultadoNotificacao_Host_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemresultadonotificacao_host_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFContagemResultadoNotificacao_Host_Sel", AV36TFContagemResultadoNotificacao_Host_Sel);
            AV39TFContagemResultadoNotificacao_User = StringUtil.Upper( cgiGet( edtavTfcontagemresultadonotificacao_user_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFContagemResultadoNotificacao_User", AV39TFContagemResultadoNotificacao_User);
            AV40TFContagemResultadoNotificacao_User_Sel = StringUtil.Upper( cgiGet( edtavTfcontagemresultadonotificacao_user_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFContagemResultadoNotificacao_User_Sel", AV40TFContagemResultadoNotificacao_User_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_port_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_port_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADONOTIFICACAO_PORT");
               GX_FocusControl = edtavTfcontagemresultadonotificacao_port_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFContagemResultadoNotificacao_Port = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContagemResultadoNotificacao_Port), 4, 0)));
            }
            else
            {
               AV43TFContagemResultadoNotificacao_Port = (short)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_port_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContagemResultadoNotificacao_Port), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_port_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_port_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO");
               GX_FocusControl = edtavTfcontagemresultadonotificacao_port_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFContagemResultadoNotificacao_Port_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFContagemResultadoNotificacao_Port_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContagemResultadoNotificacao_Port_To), 4, 0)));
            }
            else
            {
               AV44TFContagemResultadoNotificacao_Port_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_port_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFContagemResultadoNotificacao_Port_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContagemResultadoNotificacao_Port_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_aut_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_aut_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL");
               GX_FocusControl = edtavTfcontagemresultadonotificacao_aut_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFContagemResultadoNotificacao_Aut_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFContagemResultadoNotificacao_Aut_Sel", StringUtil.Str( (decimal)(AV47TFContagemResultadoNotificacao_Aut_Sel), 1, 0));
            }
            else
            {
               AV47TFContagemResultadoNotificacao_Aut_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadonotificacao_aut_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFContagemResultadoNotificacao_Aut_Sel", StringUtil.Str( (decimal)(AV47TFContagemResultadoNotificacao_Aut_Sel), 1, 0));
            }
            AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadonotificacao_datahoratitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace", AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace);
            AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadonotificacao_usunomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace", AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace);
            AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadonotificacao_assuntotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace", AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace);
            AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadonotificacao_destinatariostitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace", AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace);
            AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadonotificacao_demandastitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace", AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace);
            AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadonotificacao_hosttitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace", AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace);
            AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadonotificacao_usertitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace", AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace);
            AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadonotificacao_porttitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace", AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace);
            AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadonotificacao_auttitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace", AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace);
            AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadonotificacao_sectitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace", AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace);
            AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadonotificacao_loggedtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace", AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV59GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV60GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContagemResultado_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contagemresultadonotificacao_datahora_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Caption");
            Ddo_contagemresultadonotificacao_datahora_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Tooltip");
            Ddo_contagemresultadonotificacao_datahora_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Cls");
            Ddo_contagemresultadonotificacao_datahora_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Filteredtext_set");
            Ddo_contagemresultadonotificacao_datahora_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Filteredtextto_set");
            Ddo_contagemresultadonotificacao_datahora_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Dropdownoptionstype");
            Ddo_contagemresultadonotificacao_datahora_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Titlecontrolidtoreplace");
            Ddo_contagemresultadonotificacao_datahora_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Includesortasc"));
            Ddo_contagemresultadonotificacao_datahora_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Includesortdsc"));
            Ddo_contagemresultadonotificacao_datahora_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Sortedstatus");
            Ddo_contagemresultadonotificacao_datahora_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Includefilter"));
            Ddo_contagemresultadonotificacao_datahora_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Filtertype");
            Ddo_contagemresultadonotificacao_datahora_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Filterisrange"));
            Ddo_contagemresultadonotificacao_datahora_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Includedatalist"));
            Ddo_contagemresultadonotificacao_datahora_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Sortasc");
            Ddo_contagemresultadonotificacao_datahora_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Sortdsc");
            Ddo_contagemresultadonotificacao_datahora_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Cleanfilter");
            Ddo_contagemresultadonotificacao_datahora_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Rangefilterfrom");
            Ddo_contagemresultadonotificacao_datahora_Rangefilterto = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Rangefilterto");
            Ddo_contagemresultadonotificacao_datahora_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Searchbuttontext");
            Ddo_contagemresultadonotificacao_usunom_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Caption");
            Ddo_contagemresultadonotificacao_usunom_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Tooltip");
            Ddo_contagemresultadonotificacao_usunom_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Cls");
            Ddo_contagemresultadonotificacao_usunom_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Filteredtext_set");
            Ddo_contagemresultadonotificacao_usunom_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Selectedvalue_set");
            Ddo_contagemresultadonotificacao_usunom_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Dropdownoptionstype");
            Ddo_contagemresultadonotificacao_usunom_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Titlecontrolidtoreplace");
            Ddo_contagemresultadonotificacao_usunom_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Includesortasc"));
            Ddo_contagemresultadonotificacao_usunom_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Includesortdsc"));
            Ddo_contagemresultadonotificacao_usunom_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Sortedstatus");
            Ddo_contagemresultadonotificacao_usunom_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Includefilter"));
            Ddo_contagemresultadonotificacao_usunom_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Filtertype");
            Ddo_contagemresultadonotificacao_usunom_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Filterisrange"));
            Ddo_contagemresultadonotificacao_usunom_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Includedatalist"));
            Ddo_contagemresultadonotificacao_usunom_Datalisttype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Datalisttype");
            Ddo_contagemresultadonotificacao_usunom_Datalistproc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Datalistproc");
            Ddo_contagemresultadonotificacao_usunom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadonotificacao_usunom_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Sortasc");
            Ddo_contagemresultadonotificacao_usunom_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Sortdsc");
            Ddo_contagemresultadonotificacao_usunom_Loadingdata = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Loadingdata");
            Ddo_contagemresultadonotificacao_usunom_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Cleanfilter");
            Ddo_contagemresultadonotificacao_usunom_Noresultsfound = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Noresultsfound");
            Ddo_contagemresultadonotificacao_usunom_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Searchbuttontext");
            Ddo_contagemresultadonotificacao_assunto_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Caption");
            Ddo_contagemresultadonotificacao_assunto_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Tooltip");
            Ddo_contagemresultadonotificacao_assunto_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Cls");
            Ddo_contagemresultadonotificacao_assunto_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Filteredtext_set");
            Ddo_contagemresultadonotificacao_assunto_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Selectedvalue_set");
            Ddo_contagemresultadonotificacao_assunto_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Dropdownoptionstype");
            Ddo_contagemresultadonotificacao_assunto_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Titlecontrolidtoreplace");
            Ddo_contagemresultadonotificacao_assunto_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Includesortasc"));
            Ddo_contagemresultadonotificacao_assunto_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Includesortdsc"));
            Ddo_contagemresultadonotificacao_assunto_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Sortedstatus");
            Ddo_contagemresultadonotificacao_assunto_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Includefilter"));
            Ddo_contagemresultadonotificacao_assunto_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Filtertype");
            Ddo_contagemresultadonotificacao_assunto_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Filterisrange"));
            Ddo_contagemresultadonotificacao_assunto_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Includedatalist"));
            Ddo_contagemresultadonotificacao_assunto_Datalisttype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Datalisttype");
            Ddo_contagemresultadonotificacao_assunto_Datalistproc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Datalistproc");
            Ddo_contagemresultadonotificacao_assunto_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadonotificacao_assunto_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Sortasc");
            Ddo_contagemresultadonotificacao_assunto_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Sortdsc");
            Ddo_contagemresultadonotificacao_assunto_Loadingdata = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Loadingdata");
            Ddo_contagemresultadonotificacao_assunto_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Cleanfilter");
            Ddo_contagemresultadonotificacao_assunto_Noresultsfound = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Noresultsfound");
            Ddo_contagemresultadonotificacao_assunto_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Searchbuttontext");
            Ddo_contagemresultadonotificacao_destinatarios_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Caption");
            Ddo_contagemresultadonotificacao_destinatarios_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Tooltip");
            Ddo_contagemresultadonotificacao_destinatarios_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Cls");
            Ddo_contagemresultadonotificacao_destinatarios_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Filteredtext_set");
            Ddo_contagemresultadonotificacao_destinatarios_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Filteredtextto_set");
            Ddo_contagemresultadonotificacao_destinatarios_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Dropdownoptionstype");
            Ddo_contagemresultadonotificacao_destinatarios_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Titlecontrolidtoreplace");
            Ddo_contagemresultadonotificacao_destinatarios_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Includesortasc"));
            Ddo_contagemresultadonotificacao_destinatarios_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Includesortdsc"));
            Ddo_contagemresultadonotificacao_destinatarios_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Includefilter"));
            Ddo_contagemresultadonotificacao_destinatarios_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Filtertype");
            Ddo_contagemresultadonotificacao_destinatarios_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Filterisrange"));
            Ddo_contagemresultadonotificacao_destinatarios_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Includedatalist"));
            Ddo_contagemresultadonotificacao_destinatarios_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Cleanfilter");
            Ddo_contagemresultadonotificacao_destinatarios_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Rangefilterfrom");
            Ddo_contagemresultadonotificacao_destinatarios_Rangefilterto = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Rangefilterto");
            Ddo_contagemresultadonotificacao_destinatarios_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Searchbuttontext");
            Ddo_contagemresultadonotificacao_demandas_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Caption");
            Ddo_contagemresultadonotificacao_demandas_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Tooltip");
            Ddo_contagemresultadonotificacao_demandas_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Cls");
            Ddo_contagemresultadonotificacao_demandas_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Filteredtext_set");
            Ddo_contagemresultadonotificacao_demandas_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Filteredtextto_set");
            Ddo_contagemresultadonotificacao_demandas_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Dropdownoptionstype");
            Ddo_contagemresultadonotificacao_demandas_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Titlecontrolidtoreplace");
            Ddo_contagemresultadonotificacao_demandas_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Includesortasc"));
            Ddo_contagemresultadonotificacao_demandas_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Includesortdsc"));
            Ddo_contagemresultadonotificacao_demandas_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Includefilter"));
            Ddo_contagemresultadonotificacao_demandas_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Filtertype");
            Ddo_contagemresultadonotificacao_demandas_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Filterisrange"));
            Ddo_contagemresultadonotificacao_demandas_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Includedatalist"));
            Ddo_contagemresultadonotificacao_demandas_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Cleanfilter");
            Ddo_contagemresultadonotificacao_demandas_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Rangefilterfrom");
            Ddo_contagemresultadonotificacao_demandas_Rangefilterto = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Rangefilterto");
            Ddo_contagemresultadonotificacao_demandas_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Searchbuttontext");
            Ddo_contagemresultadonotificacao_host_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Caption");
            Ddo_contagemresultadonotificacao_host_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Tooltip");
            Ddo_contagemresultadonotificacao_host_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Cls");
            Ddo_contagemresultadonotificacao_host_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Filteredtext_set");
            Ddo_contagemresultadonotificacao_host_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Selectedvalue_set");
            Ddo_contagemresultadonotificacao_host_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Dropdownoptionstype");
            Ddo_contagemresultadonotificacao_host_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Titlecontrolidtoreplace");
            Ddo_contagemresultadonotificacao_host_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Includesortasc"));
            Ddo_contagemresultadonotificacao_host_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Includesortdsc"));
            Ddo_contagemresultadonotificacao_host_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Sortedstatus");
            Ddo_contagemresultadonotificacao_host_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Includefilter"));
            Ddo_contagemresultadonotificacao_host_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Filtertype");
            Ddo_contagemresultadonotificacao_host_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Filterisrange"));
            Ddo_contagemresultadonotificacao_host_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Includedatalist"));
            Ddo_contagemresultadonotificacao_host_Datalisttype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Datalisttype");
            Ddo_contagemresultadonotificacao_host_Datalistproc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Datalistproc");
            Ddo_contagemresultadonotificacao_host_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadonotificacao_host_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Sortasc");
            Ddo_contagemresultadonotificacao_host_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Sortdsc");
            Ddo_contagemresultadonotificacao_host_Loadingdata = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Loadingdata");
            Ddo_contagemresultadonotificacao_host_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Cleanfilter");
            Ddo_contagemresultadonotificacao_host_Noresultsfound = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Noresultsfound");
            Ddo_contagemresultadonotificacao_host_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Searchbuttontext");
            Ddo_contagemresultadonotificacao_user_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Caption");
            Ddo_contagemresultadonotificacao_user_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Tooltip");
            Ddo_contagemresultadonotificacao_user_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Cls");
            Ddo_contagemresultadonotificacao_user_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Filteredtext_set");
            Ddo_contagemresultadonotificacao_user_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Selectedvalue_set");
            Ddo_contagemresultadonotificacao_user_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Dropdownoptionstype");
            Ddo_contagemresultadonotificacao_user_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Titlecontrolidtoreplace");
            Ddo_contagemresultadonotificacao_user_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Includesortasc"));
            Ddo_contagemresultadonotificacao_user_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Includesortdsc"));
            Ddo_contagemresultadonotificacao_user_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Sortedstatus");
            Ddo_contagemresultadonotificacao_user_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Includefilter"));
            Ddo_contagemresultadonotificacao_user_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Filtertype");
            Ddo_contagemresultadonotificacao_user_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Filterisrange"));
            Ddo_contagemresultadonotificacao_user_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Includedatalist"));
            Ddo_contagemresultadonotificacao_user_Datalisttype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Datalisttype");
            Ddo_contagemresultadonotificacao_user_Datalistproc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Datalistproc");
            Ddo_contagemresultadonotificacao_user_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadonotificacao_user_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Sortasc");
            Ddo_contagemresultadonotificacao_user_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Sortdsc");
            Ddo_contagemresultadonotificacao_user_Loadingdata = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Loadingdata");
            Ddo_contagemresultadonotificacao_user_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Cleanfilter");
            Ddo_contagemresultadonotificacao_user_Noresultsfound = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Noresultsfound");
            Ddo_contagemresultadonotificacao_user_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Searchbuttontext");
            Ddo_contagemresultadonotificacao_port_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Caption");
            Ddo_contagemresultadonotificacao_port_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Tooltip");
            Ddo_contagemresultadonotificacao_port_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Cls");
            Ddo_contagemresultadonotificacao_port_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Filteredtext_set");
            Ddo_contagemresultadonotificacao_port_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Filteredtextto_set");
            Ddo_contagemresultadonotificacao_port_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Dropdownoptionstype");
            Ddo_contagemresultadonotificacao_port_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Titlecontrolidtoreplace");
            Ddo_contagemresultadonotificacao_port_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Includesortasc"));
            Ddo_contagemresultadonotificacao_port_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Includesortdsc"));
            Ddo_contagemresultadonotificacao_port_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Sortedstatus");
            Ddo_contagemresultadonotificacao_port_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Includefilter"));
            Ddo_contagemresultadonotificacao_port_Filtertype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Filtertype");
            Ddo_contagemresultadonotificacao_port_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Filterisrange"));
            Ddo_contagemresultadonotificacao_port_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Includedatalist"));
            Ddo_contagemresultadonotificacao_port_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Sortasc");
            Ddo_contagemresultadonotificacao_port_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Sortdsc");
            Ddo_contagemresultadonotificacao_port_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Cleanfilter");
            Ddo_contagemresultadonotificacao_port_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Rangefilterfrom");
            Ddo_contagemresultadonotificacao_port_Rangefilterto = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Rangefilterto");
            Ddo_contagemresultadonotificacao_port_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Searchbuttontext");
            Ddo_contagemresultadonotificacao_aut_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Caption");
            Ddo_contagemresultadonotificacao_aut_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Tooltip");
            Ddo_contagemresultadonotificacao_aut_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Cls");
            Ddo_contagemresultadonotificacao_aut_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Selectedvalue_set");
            Ddo_contagemresultadonotificacao_aut_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Dropdownoptionstype");
            Ddo_contagemresultadonotificacao_aut_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Titlecontrolidtoreplace");
            Ddo_contagemresultadonotificacao_aut_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Includesortasc"));
            Ddo_contagemresultadonotificacao_aut_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Includesortdsc"));
            Ddo_contagemresultadonotificacao_aut_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Sortedstatus");
            Ddo_contagemresultadonotificacao_aut_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Includefilter"));
            Ddo_contagemresultadonotificacao_aut_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Includedatalist"));
            Ddo_contagemresultadonotificacao_aut_Datalisttype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Datalisttype");
            Ddo_contagemresultadonotificacao_aut_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Datalistfixedvalues");
            Ddo_contagemresultadonotificacao_aut_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Sortasc");
            Ddo_contagemresultadonotificacao_aut_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Sortdsc");
            Ddo_contagemresultadonotificacao_aut_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Cleanfilter");
            Ddo_contagemresultadonotificacao_aut_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Searchbuttontext");
            Ddo_contagemresultadonotificacao_sec_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Caption");
            Ddo_contagemresultadonotificacao_sec_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Tooltip");
            Ddo_contagemresultadonotificacao_sec_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Cls");
            Ddo_contagemresultadonotificacao_sec_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Selectedvalue_set");
            Ddo_contagemresultadonotificacao_sec_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Dropdownoptionstype");
            Ddo_contagemresultadonotificacao_sec_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Titlecontrolidtoreplace");
            Ddo_contagemresultadonotificacao_sec_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Includesortasc"));
            Ddo_contagemresultadonotificacao_sec_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Includesortdsc"));
            Ddo_contagemresultadonotificacao_sec_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Sortedstatus");
            Ddo_contagemresultadonotificacao_sec_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Includefilter"));
            Ddo_contagemresultadonotificacao_sec_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Includedatalist"));
            Ddo_contagemresultadonotificacao_sec_Datalisttype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Datalisttype");
            Ddo_contagemresultadonotificacao_sec_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Allowmultipleselection"));
            Ddo_contagemresultadonotificacao_sec_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Datalistfixedvalues");
            Ddo_contagemresultadonotificacao_sec_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Sortasc");
            Ddo_contagemresultadonotificacao_sec_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Sortdsc");
            Ddo_contagemresultadonotificacao_sec_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Cleanfilter");
            Ddo_contagemresultadonotificacao_sec_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Searchbuttontext");
            Ddo_contagemresultadonotificacao_logged_Caption = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Caption");
            Ddo_contagemresultadonotificacao_logged_Tooltip = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Tooltip");
            Ddo_contagemresultadonotificacao_logged_Cls = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Cls");
            Ddo_contagemresultadonotificacao_logged_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Selectedvalue_set");
            Ddo_contagemresultadonotificacao_logged_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Dropdownoptionstype");
            Ddo_contagemresultadonotificacao_logged_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Titlecontrolidtoreplace");
            Ddo_contagemresultadonotificacao_logged_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Includesortasc"));
            Ddo_contagemresultadonotificacao_logged_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Includesortdsc"));
            Ddo_contagemresultadonotificacao_logged_Sortedstatus = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Sortedstatus");
            Ddo_contagemresultadonotificacao_logged_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Includefilter"));
            Ddo_contagemresultadonotificacao_logged_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Includedatalist"));
            Ddo_contagemresultadonotificacao_logged_Datalisttype = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Datalisttype");
            Ddo_contagemresultadonotificacao_logged_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Allowmultipleselection"));
            Ddo_contagemresultadonotificacao_logged_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Datalistfixedvalues");
            Ddo_contagemresultadonotificacao_logged_Sortasc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Sortasc");
            Ddo_contagemresultadonotificacao_logged_Sortdsc = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Sortdsc");
            Ddo_contagemresultadonotificacao_logged_Cleanfilter = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Cleanfilter");
            Ddo_contagemresultadonotificacao_logged_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contagemresultadonotificacao_datahora_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Activeeventkey");
            Ddo_contagemresultadonotificacao_datahora_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Filteredtext_get");
            Ddo_contagemresultadonotificacao_datahora_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA_Filteredtextto_get");
            Ddo_contagemresultadonotificacao_usunom_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Activeeventkey");
            Ddo_contagemresultadonotificacao_usunom_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Filteredtext_get");
            Ddo_contagemresultadonotificacao_usunom_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM_Selectedvalue_get");
            Ddo_contagemresultadonotificacao_assunto_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Activeeventkey");
            Ddo_contagemresultadonotificacao_assunto_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Filteredtext_get");
            Ddo_contagemresultadonotificacao_assunto_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_Selectedvalue_get");
            Ddo_contagemresultadonotificacao_destinatarios_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Activeeventkey");
            Ddo_contagemresultadonotificacao_destinatarios_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Filteredtext_get");
            Ddo_contagemresultadonotificacao_destinatarios_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_Filteredtextto_get");
            Ddo_contagemresultadonotificacao_demandas_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Activeeventkey");
            Ddo_contagemresultadonotificacao_demandas_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Filteredtext_get");
            Ddo_contagemresultadonotificacao_demandas_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_Filteredtextto_get");
            Ddo_contagemresultadonotificacao_host_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Activeeventkey");
            Ddo_contagemresultadonotificacao_host_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Filteredtext_get");
            Ddo_contagemresultadonotificacao_host_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST_Selectedvalue_get");
            Ddo_contagemresultadonotificacao_user_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Activeeventkey");
            Ddo_contagemresultadonotificacao_user_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Filteredtext_get");
            Ddo_contagemresultadonotificacao_user_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER_Selectedvalue_get");
            Ddo_contagemresultadonotificacao_port_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Activeeventkey");
            Ddo_contagemresultadonotificacao_port_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Filteredtext_get");
            Ddo_contagemresultadonotificacao_port_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT_Filteredtextto_get");
            Ddo_contagemresultadonotificacao_aut_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Activeeventkey");
            Ddo_contagemresultadonotificacao_aut_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT_Selectedvalue_get");
            Ddo_contagemresultadonotificacao_sec_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Activeeventkey");
            Ddo_contagemresultadonotificacao_sec_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC_Selectedvalue_get");
            Ddo_contagemresultadonotificacao_logged_Activeeventkey = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Activeeventkey");
            Ddo_contagemresultadonotificacao_logged_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA"), 0) != AV17TFContagemResultadoNotificacao_DataHora )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO"), 0) != AV18TFContagemResultadoNotificacao_DataHora_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM"), AV23TFContagemResultadoNotificacao_UsuNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL"), AV24TFContagemResultadoNotificacao_UsuNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO"), AV64TFContagemResultadoNotificacao_Assunto) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL"), AV65TFContagemResultadoNotificacao_Assunto_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS"), ",", ".") != Convert.ToDecimal( AV27TFContagemResultadoNotificacao_Destinatarios )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO"), ",", ".") != Convert.ToDecimal( AV28TFContagemResultadoNotificacao_Destinatarios_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS"), ",", ".") != Convert.ToDecimal( AV31TFContagemResultadoNotificacao_Demandas )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO"), ",", ".") != Convert.ToDecimal( AV32TFContagemResultadoNotificacao_Demandas_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_HOST"), AV35TFContagemResultadoNotificacao_Host) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL"), AV36TFContagemResultadoNotificacao_Host_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_USER"), AV39TFContagemResultadoNotificacao_User) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL"), AV40TFContagemResultadoNotificacao_User_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_PORT"), ",", ".") != Convert.ToDecimal( AV43TFContagemResultadoNotificacao_Port )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO"), ",", ".") != Convert.ToDecimal( AV44TFContagemResultadoNotificacao_Port_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL"), ",", ".") != Convert.ToDecimal( AV47TFContagemResultadoNotificacao_Aut_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23QX2 */
         E23QX2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23QX2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontagemresultadonotificacao_datahora_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_datahora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_datahora_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_datahora_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_datahora_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_datahora_to_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_usunom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_usunom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_usunom_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_usunom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_usunom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_usunom_sel_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_assunto_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_assunto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_assunto_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_assunto_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_assunto_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_assunto_sel_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_destinatarios_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_destinatarios_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_destinatarios_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_destinatarios_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_destinatarios_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_destinatarios_to_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_demandas_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_demandas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_demandas_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_demandas_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_demandas_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_demandas_to_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_host_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_host_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_host_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_host_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_host_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_host_sel_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_user_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_user_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_user_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_user_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_user_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_user_sel_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_port_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_port_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_port_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_port_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_port_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_port_to_Visible), 5, 0)));
         edtavTfcontagemresultadonotificacao_aut_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontagemresultadonotificacao_aut_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadonotificacao_aut_sel_Visible), 5, 0)));
         Ddo_contagemresultadonotificacao_datahora_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoNotificacao_DataHora";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_datahora_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadonotificacao_datahora_Titlecontrolidtoreplace);
         AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace = Ddo_contagemresultadonotificacao_datahora_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace", AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace);
         edtavDdo_contagemresultadonotificacao_datahoratitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadonotificacao_datahoratitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadonotificacao_datahoratitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadonotificacao_usunom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoNotificacao_UsuNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_usunom_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadonotificacao_usunom_Titlecontrolidtoreplace);
         AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace = Ddo_contagemresultadonotificacao_usunom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace", AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace);
         edtavDdo_contagemresultadonotificacao_usunomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadonotificacao_usunomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadonotificacao_usunomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadonotificacao_assunto_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoNotificacao_Assunto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_assunto_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadonotificacao_assunto_Titlecontrolidtoreplace);
         AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace = Ddo_contagemresultadonotificacao_assunto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace", AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace);
         edtavDdo_contagemresultadonotificacao_assuntotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadonotificacao_assuntotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadonotificacao_assuntotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadonotificacao_destinatarios_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoNotificacao_Destinatarios";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_destinatarios_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadonotificacao_destinatarios_Titlecontrolidtoreplace);
         AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace = Ddo_contagemresultadonotificacao_destinatarios_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace", AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace);
         edtavDdo_contagemresultadonotificacao_destinatariostitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadonotificacao_destinatariostitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadonotificacao_destinatariostitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadonotificacao_demandas_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoNotificacao_Demandas";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_demandas_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadonotificacao_demandas_Titlecontrolidtoreplace);
         AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace = Ddo_contagemresultadonotificacao_demandas_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace", AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace);
         edtavDdo_contagemresultadonotificacao_demandastitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadonotificacao_demandastitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadonotificacao_demandastitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadonotificacao_host_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoNotificacao_Host";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_host_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadonotificacao_host_Titlecontrolidtoreplace);
         AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace = Ddo_contagemresultadonotificacao_host_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace", AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace);
         edtavDdo_contagemresultadonotificacao_hosttitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadonotificacao_hosttitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadonotificacao_hosttitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadonotificacao_user_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoNotificacao_User";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_user_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadonotificacao_user_Titlecontrolidtoreplace);
         AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace = Ddo_contagemresultadonotificacao_user_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace", AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace);
         edtavDdo_contagemresultadonotificacao_usertitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadonotificacao_usertitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadonotificacao_usertitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadonotificacao_port_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoNotificacao_Port";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_port_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadonotificacao_port_Titlecontrolidtoreplace);
         AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace = Ddo_contagemresultadonotificacao_port_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace", AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace);
         edtavDdo_contagemresultadonotificacao_porttitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadonotificacao_porttitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadonotificacao_porttitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadonotificacao_aut_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoNotificacao_Aut";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_aut_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadonotificacao_aut_Titlecontrolidtoreplace);
         AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace = Ddo_contagemresultadonotificacao_aut_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace", AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace);
         edtavDdo_contagemresultadonotificacao_auttitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadonotificacao_auttitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadonotificacao_auttitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadonotificacao_sec_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoNotificacao_Sec";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_sec_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadonotificacao_sec_Titlecontrolidtoreplace);
         AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace = Ddo_contagemresultadonotificacao_sec_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace", AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace);
         edtavDdo_contagemresultadonotificacao_sectitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadonotificacao_sectitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadonotificacao_sectitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadonotificacao_logged_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoNotificacao_Logged";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_logged_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadonotificacao_logged_Titlecontrolidtoreplace);
         AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace = Ddo_contagemresultadonotificacao_logged_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace", AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace);
         edtavDdo_contagemresultadonotificacao_loggedtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contagemresultadonotificacao_loggedtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadonotificacao_loggedtitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV57DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV57DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E24QX2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV16ContagemResultadoNotificacao_DataHoraTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV22ContagemResultadoNotificacao_UsuNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63ContagemResultadoNotificacao_AssuntoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV26ContagemResultadoNotificacao_DestinatariosTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV30ContagemResultadoNotificacao_DemandasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ContagemResultadoNotificacao_HostTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ContagemResultadoNotificacao_UserTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42ContagemResultadoNotificacao_PortTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46ContagemResultadoNotificacao_AutTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49ContagemResultadoNotificacao_SecTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53ContagemResultadoNotificacao_LoggedTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemResultadoNotificacao_DataHora_Titleformat = 2;
         edtContagemResultadoNotificacao_DataHora_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_DataHora_Internalname, "Title", edtContagemResultadoNotificacao_DataHora_Title);
         edtContagemResultadoNotificacao_UsuNom_Titleformat = 2;
         edtContagemResultadoNotificacao_UsuNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Remetente", AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_UsuNom_Internalname, "Title", edtContagemResultadoNotificacao_UsuNom_Title);
         edtContagemResultadoNotificacao_Assunto_Titleformat = 2;
         edtContagemResultadoNotificacao_Assunto_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Assunto", AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_Assunto_Internalname, "Title", edtContagemResultadoNotificacao_Assunto_Title);
         edtContagemResultadoNotificacao_Destinatarios_Titleformat = 2;
         edtContagemResultadoNotificacao_Destinatarios_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Destinat�rios", AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_Destinatarios_Internalname, "Title", edtContagemResultadoNotificacao_Destinatarios_Title);
         edtContagemResultadoNotificacao_Demandas_Titleformat = 2;
         edtContagemResultadoNotificacao_Demandas_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Demandas", AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_Demandas_Internalname, "Title", edtContagemResultadoNotificacao_Demandas_Title);
         edtContagemResultadoNotificacao_Host_Titleformat = 2;
         edtContagemResultadoNotificacao_Host_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Host", AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_Host_Internalname, "Title", edtContagemResultadoNotificacao_Host_Title);
         edtContagemResultadoNotificacao_User_Titleformat = 2;
         edtContagemResultadoNotificacao_User_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usu�rio", AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_User_Internalname, "Title", edtContagemResultadoNotificacao_User_Title);
         edtContagemResultadoNotificacao_Port_Titleformat = 2;
         edtContagemResultadoNotificacao_Port_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Porta", AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_Port_Internalname, "Title", edtContagemResultadoNotificacao_Port_Title);
         cmbContagemResultadoNotificacao_Aut_Titleformat = 2;
         cmbContagemResultadoNotificacao_Aut.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Autentica��o", AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultadoNotificacao_Aut_Internalname, "Title", cmbContagemResultadoNotificacao_Aut.Title.Text);
         cmbContagemResultadoNotificacao_Sec_Titleformat = 2;
         cmbContagemResultadoNotificacao_Sec.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Seguran�a", AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultadoNotificacao_Sec_Internalname, "Title", cmbContagemResultadoNotificacao_Sec.Title.Text);
         cmbContagemResultadoNotificacao_Logged_Titleformat = 2;
         cmbContagemResultadoNotificacao_Logged.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Login", AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultadoNotificacao_Logged_Internalname, "Title", cmbContagemResultadoNotificacao_Logged.Title.Text);
         AV59GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59GridCurrentPage), 10, 0)));
         AV60GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60GridPageCount), 10, 0)));
         edtContagemResultadoNotificacao_DataHora_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_DataHora_Internalname, "Linktarget", edtContagemResultadoNotificacao_DataHora_Linktarget);
         AV61Codigos.Add(AV7ContagemResultado_Codigo, 0);
         AV62WebSession.Set("DemandaCodigo", AV61Codigos.ToXml(false, true, "Collection", ""));
         AV62WebSession.Set("CodigoNtf", StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0));
         AV62WebSession.Set("Caller", "NtfDmn");
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV16ContagemResultadoNotificacao_DataHoraTitleFilterData", AV16ContagemResultadoNotificacao_DataHoraTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV22ContagemResultadoNotificacao_UsuNomTitleFilterData", AV22ContagemResultadoNotificacao_UsuNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV63ContagemResultadoNotificacao_AssuntoTitleFilterData", AV63ContagemResultadoNotificacao_AssuntoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV26ContagemResultadoNotificacao_DestinatariosTitleFilterData", AV26ContagemResultadoNotificacao_DestinatariosTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV30ContagemResultadoNotificacao_DemandasTitleFilterData", AV30ContagemResultadoNotificacao_DemandasTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV34ContagemResultadoNotificacao_HostTitleFilterData", AV34ContagemResultadoNotificacao_HostTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV38ContagemResultadoNotificacao_UserTitleFilterData", AV38ContagemResultadoNotificacao_UserTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV42ContagemResultadoNotificacao_PortTitleFilterData", AV42ContagemResultadoNotificacao_PortTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV46ContagemResultadoNotificacao_AutTitleFilterData", AV46ContagemResultadoNotificacao_AutTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV49ContagemResultadoNotificacao_SecTitleFilterData", AV49ContagemResultadoNotificacao_SecTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV53ContagemResultadoNotificacao_LoggedTitleFilterData", AV53ContagemResultadoNotificacao_LoggedTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV61Codigos", AV61Codigos);
      }

      protected void E11QX2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV58PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV58PageToGo) ;
         }
      }

      protected void E12QX2( )
      {
         /* Ddo_contagemresultadonotificacao_datahora_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_datahora_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_datahora_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_datahora_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_datahora_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_datahora_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_datahora_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_datahora_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_datahora_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_datahora_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV17TFContagemResultadoNotificacao_DataHora = context.localUtil.CToT( Ddo_contagemresultadonotificacao_datahora_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContagemResultadoNotificacao_DataHora", context.localUtil.TToC( AV17TFContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
            AV18TFContagemResultadoNotificacao_DataHora_To = context.localUtil.CToT( Ddo_contagemresultadonotificacao_datahora_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContagemResultadoNotificacao_DataHora_To", context.localUtil.TToC( AV18TFContagemResultadoNotificacao_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV18TFContagemResultadoNotificacao_DataHora_To) )
            {
               AV18TFContagemResultadoNotificacao_DataHora_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV18TFContagemResultadoNotificacao_DataHora_To)), (short)(DateTimeUtil.Month( AV18TFContagemResultadoNotificacao_DataHora_To)), (short)(DateTimeUtil.Day( AV18TFContagemResultadoNotificacao_DataHora_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContagemResultadoNotificacao_DataHora_To", context.localUtil.TToC( AV18TFContagemResultadoNotificacao_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E13QX2( )
      {
         /* Ddo_contagemresultadonotificacao_usunom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_usunom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_usunom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_usunom_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_usunom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_usunom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_usunom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_usunom_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_usunom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_usunom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV23TFContagemResultadoNotificacao_UsuNom = Ddo_contagemresultadonotificacao_usunom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagemResultadoNotificacao_UsuNom", AV23TFContagemResultadoNotificacao_UsuNom);
            AV24TFContagemResultadoNotificacao_UsuNom_Sel = Ddo_contagemresultadonotificacao_usunom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContagemResultadoNotificacao_UsuNom_Sel", AV24TFContagemResultadoNotificacao_UsuNom_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E14QX2( )
      {
         /* Ddo_contagemresultadonotificacao_assunto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_assunto_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_assunto_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_assunto_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_assunto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_assunto_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_assunto_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_assunto_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_assunto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_assunto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV64TFContagemResultadoNotificacao_Assunto = Ddo_contagemresultadonotificacao_assunto_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFContagemResultadoNotificacao_Assunto", AV64TFContagemResultadoNotificacao_Assunto);
            AV65TFContagemResultadoNotificacao_Assunto_Sel = Ddo_contagemresultadonotificacao_assunto_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65TFContagemResultadoNotificacao_Assunto_Sel", AV65TFContagemResultadoNotificacao_Assunto_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E15QX2( )
      {
         /* Ddo_contagemresultadonotificacao_destinatarios_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_destinatarios_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV27TFContagemResultadoNotificacao_Destinatarios = (int)(NumberUtil.Val( Ddo_contagemresultadonotificacao_destinatarios_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27TFContagemResultadoNotificacao_Destinatarios), 6, 0)));
            AV28TFContagemResultadoNotificacao_Destinatarios_To = (int)(NumberUtil.Val( Ddo_contagemresultadonotificacao_destinatarios_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContagemResultadoNotificacao_Destinatarios_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28TFContagemResultadoNotificacao_Destinatarios_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E16QX2( )
      {
         /* Ddo_contagemresultadonotificacao_demandas_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_demandas_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFContagemResultadoNotificacao_Demandas = (int)(NumberUtil.Val( Ddo_contagemresultadonotificacao_demandas_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContagemResultadoNotificacao_Demandas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContagemResultadoNotificacao_Demandas), 6, 0)));
            AV32TFContagemResultadoNotificacao_Demandas_To = (int)(NumberUtil.Val( Ddo_contagemresultadonotificacao_demandas_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFContagemResultadoNotificacao_Demandas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContagemResultadoNotificacao_Demandas_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E17QX2( )
      {
         /* Ddo_contagemresultadonotificacao_host_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_host_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_host_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_host_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_host_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_host_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_host_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_host_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_host_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_host_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFContagemResultadoNotificacao_Host = Ddo_contagemresultadonotificacao_host_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFContagemResultadoNotificacao_Host", AV35TFContagemResultadoNotificacao_Host);
            AV36TFContagemResultadoNotificacao_Host_Sel = Ddo_contagemresultadonotificacao_host_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFContagemResultadoNotificacao_Host_Sel", AV36TFContagemResultadoNotificacao_Host_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E18QX2( )
      {
         /* Ddo_contagemresultadonotificacao_user_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_user_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_user_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_user_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_user_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_user_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_user_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_user_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_user_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_user_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFContagemResultadoNotificacao_User = Ddo_contagemresultadonotificacao_user_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFContagemResultadoNotificacao_User", AV39TFContagemResultadoNotificacao_User);
            AV40TFContagemResultadoNotificacao_User_Sel = Ddo_contagemresultadonotificacao_user_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFContagemResultadoNotificacao_User_Sel", AV40TFContagemResultadoNotificacao_User_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E19QX2( )
      {
         /* Ddo_contagemresultadonotificacao_port_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_port_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_port_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_port_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_port_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_port_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_port_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_port_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_port_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_port_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFContagemResultadoNotificacao_Port = (short)(NumberUtil.Val( Ddo_contagemresultadonotificacao_port_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContagemResultadoNotificacao_Port), 4, 0)));
            AV44TFContagemResultadoNotificacao_Port_To = (short)(NumberUtil.Val( Ddo_contagemresultadonotificacao_port_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFContagemResultadoNotificacao_Port_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContagemResultadoNotificacao_Port_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E20QX2( )
      {
         /* Ddo_contagemresultadonotificacao_aut_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_aut_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_aut_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_aut_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_aut_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_aut_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_aut_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_aut_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_aut_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_aut_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFContagemResultadoNotificacao_Aut_Sel = (short)(NumberUtil.Val( Ddo_contagemresultadonotificacao_aut_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFContagemResultadoNotificacao_Aut_Sel", StringUtil.Str( (decimal)(AV47TFContagemResultadoNotificacao_Aut_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
      }

      protected void E21QX2( )
      {
         /* Ddo_contagemresultadonotificacao_sec_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_sec_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_sec_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_sec_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_sec_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_sec_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_sec_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_sec_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_sec_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_sec_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFContagemResultadoNotificacao_Sec_SelsJson = Ddo_contagemresultadonotificacao_sec_Selectedvalue_get;
            AV51TFContagemResultadoNotificacao_Sec_Sels.FromJSonString(StringUtil.StringReplace( AV50TFContagemResultadoNotificacao_Sec_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV51TFContagemResultadoNotificacao_Sec_Sels", AV51TFContagemResultadoNotificacao_Sec_Sels);
      }

      protected void E22QX2( )
      {
         /* Ddo_contagemresultadonotificacao_logged_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_logged_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_logged_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_logged_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_logged_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_logged_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadonotificacao_logged_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_logged_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_logged_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadonotificacao_logged_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV54TFContagemResultadoNotificacao_Logged_SelsJson = Ddo_contagemresultadonotificacao_logged_Selectedvalue_get;
            AV55TFContagemResultadoNotificacao_Logged_Sels.FromJSonString(StringUtil.StringReplace( AV54TFContagemResultadoNotificacao_Logged_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV55TFContagemResultadoNotificacao_Logged_Sels", AV55TFContagemResultadoNotificacao_Logged_Sels);
      }

      private void E25QX2( )
      {
         /* Grid_Load Routine */
         edtContagemResultadoNotificacao_UsuNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A1413ContagemResultadoNotificacao_UsuCod) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContagemResultadoNotificacao_DataHora_Link = formatLink("viewcontagemresultadonotificacao.aspx") + "?" + UrlEncode("" +A1412ContagemResultadoNotificacao_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         if ( AV6WWPContext.gxTpr_Contratante_codigo > 0 )
         {
            GXt_int2 = AV6WWPContext.gxTpr_Areatrabalho_codigo;
            if ( ! new prc_usuarioehcontratantedaarea(context).executeUdp( ref  GXt_int2, ref  A1413ContagemResultadoNotificacao_UsuCod) )
            {
               edtContagemResultadoNotificacao_UsuNom_Link = "";
            }
         }
         else if ( AV6WWPContext.gxTpr_Contratada_codigo > 0 )
         {
            if ( ! new prc_usuarioehcontratada(context).executeUdp(  AV6WWPContext.gxTpr_Contratada_codigo,  A1413ContagemResultadoNotificacao_UsuCod) )
            {
               edtContagemResultadoNotificacao_UsuNom_Link = "";
            }
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contagemresultadonotificacao_datahora_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_datahora_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_datahora_Sortedstatus);
         Ddo_contagemresultadonotificacao_usunom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_usunom_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_usunom_Sortedstatus);
         Ddo_contagemresultadonotificacao_assunto_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_assunto_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_assunto_Sortedstatus);
         Ddo_contagemresultadonotificacao_host_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_host_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_host_Sortedstatus);
         Ddo_contagemresultadonotificacao_user_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_user_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_user_Sortedstatus);
         Ddo_contagemresultadonotificacao_port_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_port_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_port_Sortedstatus);
         Ddo_contagemresultadonotificacao_aut_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_aut_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_aut_Sortedstatus);
         Ddo_contagemresultadonotificacao_sec_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_sec_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_sec_Sortedstatus);
         Ddo_contagemresultadonotificacao_logged_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_logged_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_logged_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contagemresultadonotificacao_datahora_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_datahora_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_datahora_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contagemresultadonotificacao_usunom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_usunom_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_usunom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contagemresultadonotificacao_assunto_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_assunto_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_assunto_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contagemresultadonotificacao_host_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_host_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_host_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contagemresultadonotificacao_user_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_user_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_user_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contagemresultadonotificacao_port_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_port_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_port_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contagemresultadonotificacao_aut_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_aut_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_aut_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_contagemresultadonotificacao_sec_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_sec_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_sec_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_contagemresultadonotificacao_logged_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_logged_Internalname, "SortedStatus", Ddo_contagemresultadonotificacao_logged_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV69Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV69Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV69Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV70GXV1 = 1;
         while ( AV70GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV70GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_DATAHORA") == 0 )
            {
               AV17TFContagemResultadoNotificacao_DataHora = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFContagemResultadoNotificacao_DataHora", context.localUtil.TToC( AV17TFContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
               AV18TFContagemResultadoNotificacao_DataHora_To = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContagemResultadoNotificacao_DataHora_To", context.localUtil.TToC( AV18TFContagemResultadoNotificacao_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV17TFContagemResultadoNotificacao_DataHora) )
               {
                  AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate = DateTimeUtil.ResetTime(AV17TFContagemResultadoNotificacao_DataHora);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate", context.localUtil.Format(AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate, "99/99/99"));
                  Ddo_contagemresultadonotificacao_datahora_Filteredtext_set = context.localUtil.DToC( AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_datahora_Internalname, "FilteredText_set", Ddo_contagemresultadonotificacao_datahora_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV18TFContagemResultadoNotificacao_DataHora_To) )
               {
                  AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo = DateTimeUtil.ResetTime(AV18TFContagemResultadoNotificacao_DataHora_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo", context.localUtil.Format(AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo, "99/99/99"));
                  Ddo_contagemresultadonotificacao_datahora_Filteredtextto_set = context.localUtil.DToC( AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_datahora_Internalname, "FilteredTextTo_set", Ddo_contagemresultadonotificacao_datahora_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_USUNOM") == 0 )
            {
               AV23TFContagemResultadoNotificacao_UsuNom = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContagemResultadoNotificacao_UsuNom", AV23TFContagemResultadoNotificacao_UsuNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContagemResultadoNotificacao_UsuNom)) )
               {
                  Ddo_contagemresultadonotificacao_usunom_Filteredtext_set = AV23TFContagemResultadoNotificacao_UsuNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_usunom_Internalname, "FilteredText_set", Ddo_contagemresultadonotificacao_usunom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL") == 0 )
            {
               AV24TFContagemResultadoNotificacao_UsuNom_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContagemResultadoNotificacao_UsuNom_Sel", AV24TFContagemResultadoNotificacao_UsuNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContagemResultadoNotificacao_UsuNom_Sel)) )
               {
                  Ddo_contagemresultadonotificacao_usunom_Selectedvalue_set = AV24TFContagemResultadoNotificacao_UsuNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_usunom_Internalname, "SelectedValue_set", Ddo_contagemresultadonotificacao_usunom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO") == 0 )
            {
               AV64TFContagemResultadoNotificacao_Assunto = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFContagemResultadoNotificacao_Assunto", AV64TFContagemResultadoNotificacao_Assunto);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFContagemResultadoNotificacao_Assunto)) )
               {
                  Ddo_contagemresultadonotificacao_assunto_Filteredtext_set = AV64TFContagemResultadoNotificacao_Assunto;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_assunto_Internalname, "FilteredText_set", Ddo_contagemresultadonotificacao_assunto_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL") == 0 )
            {
               AV65TFContagemResultadoNotificacao_Assunto_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65TFContagemResultadoNotificacao_Assunto_Sel", AV65TFContagemResultadoNotificacao_Assunto_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContagemResultadoNotificacao_Assunto_Sel)) )
               {
                  Ddo_contagemresultadonotificacao_assunto_Selectedvalue_set = AV65TFContagemResultadoNotificacao_Assunto_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_assunto_Internalname, "SelectedValue_set", Ddo_contagemresultadonotificacao_assunto_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS") == 0 )
            {
               AV27TFContagemResultadoNotificacao_Destinatarios = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContagemResultadoNotificacao_Destinatarios", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27TFContagemResultadoNotificacao_Destinatarios), 6, 0)));
               AV28TFContagemResultadoNotificacao_Destinatarios_To = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContagemResultadoNotificacao_Destinatarios_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28TFContagemResultadoNotificacao_Destinatarios_To), 6, 0)));
               if ( ! (0==AV27TFContagemResultadoNotificacao_Destinatarios) )
               {
                  Ddo_contagemresultadonotificacao_destinatarios_Filteredtext_set = StringUtil.Str( (decimal)(AV27TFContagemResultadoNotificacao_Destinatarios), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_destinatarios_Internalname, "FilteredText_set", Ddo_contagemresultadonotificacao_destinatarios_Filteredtext_set);
               }
               if ( ! (0==AV28TFContagemResultadoNotificacao_Destinatarios_To) )
               {
                  Ddo_contagemresultadonotificacao_destinatarios_Filteredtextto_set = StringUtil.Str( (decimal)(AV28TFContagemResultadoNotificacao_Destinatarios_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_destinatarios_Internalname, "FilteredTextTo_set", Ddo_contagemresultadonotificacao_destinatarios_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS") == 0 )
            {
               AV31TFContagemResultadoNotificacao_Demandas = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContagemResultadoNotificacao_Demandas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContagemResultadoNotificacao_Demandas), 6, 0)));
               AV32TFContagemResultadoNotificacao_Demandas_To = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFContagemResultadoNotificacao_Demandas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContagemResultadoNotificacao_Demandas_To), 6, 0)));
               if ( ! (0==AV31TFContagemResultadoNotificacao_Demandas) )
               {
                  Ddo_contagemresultadonotificacao_demandas_Filteredtext_set = StringUtil.Str( (decimal)(AV31TFContagemResultadoNotificacao_Demandas), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_demandas_Internalname, "FilteredText_set", Ddo_contagemresultadonotificacao_demandas_Filteredtext_set);
               }
               if ( ! (0==AV32TFContagemResultadoNotificacao_Demandas_To) )
               {
                  Ddo_contagemresultadonotificacao_demandas_Filteredtextto_set = StringUtil.Str( (decimal)(AV32TFContagemResultadoNotificacao_Demandas_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_demandas_Internalname, "FilteredTextTo_set", Ddo_contagemresultadonotificacao_demandas_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_HOST") == 0 )
            {
               AV35TFContagemResultadoNotificacao_Host = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35TFContagemResultadoNotificacao_Host", AV35TFContagemResultadoNotificacao_Host);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContagemResultadoNotificacao_Host)) )
               {
                  Ddo_contagemresultadonotificacao_host_Filteredtext_set = AV35TFContagemResultadoNotificacao_Host;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_host_Internalname, "FilteredText_set", Ddo_contagemresultadonotificacao_host_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL") == 0 )
            {
               AV36TFContagemResultadoNotificacao_Host_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFContagemResultadoNotificacao_Host_Sel", AV36TFContagemResultadoNotificacao_Host_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContagemResultadoNotificacao_Host_Sel)) )
               {
                  Ddo_contagemresultadonotificacao_host_Selectedvalue_set = AV36TFContagemResultadoNotificacao_Host_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_host_Internalname, "SelectedValue_set", Ddo_contagemresultadonotificacao_host_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_USER") == 0 )
            {
               AV39TFContagemResultadoNotificacao_User = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFContagemResultadoNotificacao_User", AV39TFContagemResultadoNotificacao_User);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFContagemResultadoNotificacao_User)) )
               {
                  Ddo_contagemresultadonotificacao_user_Filteredtext_set = AV39TFContagemResultadoNotificacao_User;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_user_Internalname, "FilteredText_set", Ddo_contagemresultadonotificacao_user_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_USER_SEL") == 0 )
            {
               AV40TFContagemResultadoNotificacao_User_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFContagemResultadoNotificacao_User_Sel", AV40TFContagemResultadoNotificacao_User_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContagemResultadoNotificacao_User_Sel)) )
               {
                  Ddo_contagemresultadonotificacao_user_Selectedvalue_set = AV40TFContagemResultadoNotificacao_User_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_user_Internalname, "SelectedValue_set", Ddo_contagemresultadonotificacao_user_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_PORT") == 0 )
            {
               AV43TFContagemResultadoNotificacao_Port = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContagemResultadoNotificacao_Port), 4, 0)));
               AV44TFContagemResultadoNotificacao_Port_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFContagemResultadoNotificacao_Port_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContagemResultadoNotificacao_Port_To), 4, 0)));
               if ( ! (0==AV43TFContagemResultadoNotificacao_Port) )
               {
                  Ddo_contagemresultadonotificacao_port_Filteredtext_set = StringUtil.Str( (decimal)(AV43TFContagemResultadoNotificacao_Port), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_port_Internalname, "FilteredText_set", Ddo_contagemresultadonotificacao_port_Filteredtext_set);
               }
               if ( ! (0==AV44TFContagemResultadoNotificacao_Port_To) )
               {
                  Ddo_contagemresultadonotificacao_port_Filteredtextto_set = StringUtil.Str( (decimal)(AV44TFContagemResultadoNotificacao_Port_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_port_Internalname, "FilteredTextTo_set", Ddo_contagemresultadonotificacao_port_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL") == 0 )
            {
               AV47TFContagemResultadoNotificacao_Aut_Sel = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFContagemResultadoNotificacao_Aut_Sel", StringUtil.Str( (decimal)(AV47TFContagemResultadoNotificacao_Aut_Sel), 1, 0));
               if ( ! (0==AV47TFContagemResultadoNotificacao_Aut_Sel) )
               {
                  Ddo_contagemresultadonotificacao_aut_Selectedvalue_set = StringUtil.Str( (decimal)(AV47TFContagemResultadoNotificacao_Aut_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_aut_Internalname, "SelectedValue_set", Ddo_contagemresultadonotificacao_aut_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_SEC_SEL") == 0 )
            {
               AV50TFContagemResultadoNotificacao_Sec_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV51TFContagemResultadoNotificacao_Sec_Sels.FromJSonString(AV50TFContagemResultadoNotificacao_Sec_SelsJson);
               if ( ! ( AV51TFContagemResultadoNotificacao_Sec_Sels.Count == 0 ) )
               {
                  Ddo_contagemresultadonotificacao_sec_Selectedvalue_set = AV50TFContagemResultadoNotificacao_Sec_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_sec_Internalname, "SelectedValue_set", Ddo_contagemresultadonotificacao_sec_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SEL") == 0 )
            {
               AV54TFContagemResultadoNotificacao_Logged_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV55TFContagemResultadoNotificacao_Logged_Sels.FromJSonString(AV54TFContagemResultadoNotificacao_Logged_SelsJson);
               if ( ! ( AV55TFContagemResultadoNotificacao_Logged_Sels.Count == 0 ) )
               {
                  Ddo_contagemresultadonotificacao_logged_Selectedvalue_set = AV54TFContagemResultadoNotificacao_Logged_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contagemresultadonotificacao_logged_Internalname, "SelectedValue_set", Ddo_contagemresultadonotificacao_logged_Selectedvalue_set);
               }
            }
            AV70GXV1 = (int)(AV70GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV69Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV17TFContagemResultadoNotificacao_DataHora) && (DateTime.MinValue==AV18TFContagemResultadoNotificacao_DataHora_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_DATAHORA";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV17TFContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " ");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV18TFContagemResultadoNotificacao_DataHora_To, 8, 5, 0, 3, "/", ":", " ");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContagemResultadoNotificacao_UsuNom)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_USUNOM";
            AV12GridStateFilterValue.gxTpr_Value = AV23TFContagemResultadoNotificacao_UsuNom;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContagemResultadoNotificacao_UsuNom_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV24TFContagemResultadoNotificacao_UsuNom_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFContagemResultadoNotificacao_Assunto)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO";
            AV12GridStateFilterValue.gxTpr_Value = AV64TFContagemResultadoNotificacao_Assunto;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContagemResultadoNotificacao_Assunto_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV65TFContagemResultadoNotificacao_Assunto_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV27TFContagemResultadoNotificacao_Destinatarios) && (0==AV28TFContagemResultadoNotificacao_Destinatarios_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV27TFContagemResultadoNotificacao_Destinatarios), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV28TFContagemResultadoNotificacao_Destinatarios_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV31TFContagemResultadoNotificacao_Demandas) && (0==AV32TFContagemResultadoNotificacao_Demandas_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV31TFContagemResultadoNotificacao_Demandas), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV32TFContagemResultadoNotificacao_Demandas_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContagemResultadoNotificacao_Host)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_HOST";
            AV12GridStateFilterValue.gxTpr_Value = AV35TFContagemResultadoNotificacao_Host;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContagemResultadoNotificacao_Host_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV36TFContagemResultadoNotificacao_Host_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFContagemResultadoNotificacao_User)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_USER";
            AV12GridStateFilterValue.gxTpr_Value = AV39TFContagemResultadoNotificacao_User;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContagemResultadoNotificacao_User_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_USER_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV40TFContagemResultadoNotificacao_User_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV43TFContagemResultadoNotificacao_Port) && (0==AV44TFContagemResultadoNotificacao_Port_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_PORT";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV43TFContagemResultadoNotificacao_Port), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV44TFContagemResultadoNotificacao_Port_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV47TFContagemResultadoNotificacao_Aut_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV47TFContagemResultadoNotificacao_Aut_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV51TFContagemResultadoNotificacao_Sec_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_SEC_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV51TFContagemResultadoNotificacao_Sec_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV55TFContagemResultadoNotificacao_Logged_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV55TFContagemResultadoNotificacao_Logged_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7ContagemResultado_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CONTAGEMRESULTADO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV69Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV69Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContagemResultadoNotificacao";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContagemResultado_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_QX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_QX2( true) ;
         }
         else
         {
            wb_table2_5_QX2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_QX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_QX2e( true) ;
         }
         else
         {
            wb_table1_2_QX2e( false) ;
         }
      }

      protected void wb_table2_5_QX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "da Notifica��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Usu�rio Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_DataHora_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_DataHora_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_DataHora_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_UsuNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_UsuNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_UsuNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_Assunto_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_Assunto_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_Assunto_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_Destinatarios_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_Destinatarios_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_Destinatarios_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_Demandas_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_Demandas_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_Demandas_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_Host_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_Host_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_Host_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_User_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_User_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_User_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_Port_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_Port_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_Port_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemResultadoNotificacao_Aut_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemResultadoNotificacao_Aut.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemResultadoNotificacao_Aut.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemResultadoNotificacao_Sec_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemResultadoNotificacao_Sec.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemResultadoNotificacao_Sec.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemResultadoNotificacao_Logged_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemResultadoNotificacao_Logged.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemResultadoNotificacao_Logged.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_DataHora_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DataHora_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultadoNotificacao_DataHora_Link));
               GridColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtContagemResultadoNotificacao_DataHora_Linktarget));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1422ContagemResultadoNotificacao_UsuNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_UsuNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_UsuNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultadoNotificacao_UsuNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1417ContagemResultadoNotificacao_Assunto);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_Assunto_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_Assunto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_Destinatarios_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_Destinatarios_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1963ContagemResultadoNotificacao_Demandas), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_Demandas_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_Demandas_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1956ContagemResultadoNotificacao_Host));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_Host_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_Host_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1957ContagemResultadoNotificacao_User));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_User_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_User_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_Port_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_Port_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemResultadoNotificacao_Aut.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemResultadoNotificacao_Aut_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemResultadoNotificacao_Sec.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemResultadoNotificacao_Sec_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemResultadoNotificacao_Logged.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemResultadoNotificacao_Logged_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_QX2e( true) ;
         }
         else
         {
            wb_table2_5_QX2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAQX2( ) ;
         WSQX2( ) ;
         WEQX2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContagemResultado_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAQX2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "demandanotificacoeswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAQX2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         }
         wcpOAV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContagemResultado_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContagemResultado_Codigo != wcpOAV7ContagemResultado_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContagemResultado_Codigo = AV7ContagemResultado_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContagemResultado_Codigo = cgiGet( sPrefix+"AV7ContagemResultado_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContagemResultado_Codigo) > 0 )
         {
            AV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContagemResultado_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         }
         else
         {
            AV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContagemResultado_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAQX2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSQX2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSQX2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContagemResultado_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContagemResultado_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContagemResultado_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7ContagemResultado_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEQX2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020617155589");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("demandanotificacoeswc.js", "?2020617155589");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO_"+sGXsfl_8_idx;
         edtContagemResultadoNotificacao_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_CODIGO_"+sGXsfl_8_idx;
         edtContagemResultadoNotificacao_UsuCod_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_USUCOD_"+sGXsfl_8_idx;
         edtContagemResultadoNotificacao_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DATAHORA_"+sGXsfl_8_idx;
         edtContagemResultadoNotificacao_UsuNom_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_USUNOM_"+sGXsfl_8_idx;
         edtContagemResultadoNotificacao_Assunto_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_"+sGXsfl_8_idx;
         edtContagemResultadoNotificacao_Destinatarios_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_"+sGXsfl_8_idx;
         edtContagemResultadoNotificacao_Demandas_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_"+sGXsfl_8_idx;
         edtContagemResultadoNotificacao_Host_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_HOST_"+sGXsfl_8_idx;
         edtContagemResultadoNotificacao_User_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_USER_"+sGXsfl_8_idx;
         edtContagemResultadoNotificacao_Port_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_PORT_"+sGXsfl_8_idx;
         cmbContagemResultadoNotificacao_Aut_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_AUT_"+sGXsfl_8_idx;
         cmbContagemResultadoNotificacao_Sec_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_SEC_"+sGXsfl_8_idx;
         cmbContagemResultadoNotificacao_Logged_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_LOGGED_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO_"+sGXsfl_8_fel_idx;
         edtContagemResultadoNotificacao_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_CODIGO_"+sGXsfl_8_fel_idx;
         edtContagemResultadoNotificacao_UsuCod_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_USUCOD_"+sGXsfl_8_fel_idx;
         edtContagemResultadoNotificacao_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DATAHORA_"+sGXsfl_8_fel_idx;
         edtContagemResultadoNotificacao_UsuNom_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_USUNOM_"+sGXsfl_8_fel_idx;
         edtContagemResultadoNotificacao_Assunto_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_ASSUNTO_"+sGXsfl_8_fel_idx;
         edtContagemResultadoNotificacao_Destinatarios_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_"+sGXsfl_8_fel_idx;
         edtContagemResultadoNotificacao_Demandas_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DEMANDAS_"+sGXsfl_8_fel_idx;
         edtContagemResultadoNotificacao_Host_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_HOST_"+sGXsfl_8_fel_idx;
         edtContagemResultadoNotificacao_User_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_USER_"+sGXsfl_8_fel_idx;
         edtContagemResultadoNotificacao_Port_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_PORT_"+sGXsfl_8_fel_idx;
         cmbContagemResultadoNotificacao_Aut_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_AUT_"+sGXsfl_8_fel_idx;
         cmbContagemResultadoNotificacao_Sec_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_SEC_"+sGXsfl_8_fel_idx;
         cmbContagemResultadoNotificacao_Logged_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_LOGGED_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBQX0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")),context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo10",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_UsuCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_UsuCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DataHora_Internalname,context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1416ContagemResultadoNotificacao_DataHora, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContagemResultadoNotificacao_DataHora_Link,(String)edtContagemResultadoNotificacao_DataHora_Linktarget,(String)"",(String)"",(String)edtContagemResultadoNotificacao_DataHora_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_UsuNom_Internalname,StringUtil.RTrim( A1422ContagemResultadoNotificacao_UsuNom),StringUtil.RTrim( context.localUtil.Format( A1422ContagemResultadoNotificacao_UsuNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContagemResultadoNotificacao_UsuNom_Link,(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_UsuNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_Assunto_Internalname,(String)A1417ContagemResultadoNotificacao_Assunto,(String)A1417ContagemResultadoNotificacao_Assunto,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_Assunto_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1000,(short)0,(short)0,(short)8,(short)1,(short)0,(short)-1,(bool)true,(String)"Observacao",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_Destinatarios_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1962ContagemResultadoNotificacao_Destinatarios), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_Destinatarios_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_Demandas_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1963ContagemResultadoNotificacao_Demandas), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1963ContagemResultadoNotificacao_Demandas), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_Demandas_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_Host_Internalname,StringUtil.RTrim( A1956ContagemResultadoNotificacao_Host),StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_Host_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_User_Internalname,StringUtil.RTrim( A1957ContagemResultadoNotificacao_User),StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_User_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_Port_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_Port_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_AUT_" + sGXsfl_8_idx;
            cmbContagemResultadoNotificacao_Aut.Name = GXCCtl;
            cmbContagemResultadoNotificacao_Aut.WebTags = "";
            cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContagemResultadoNotificacao_Aut.ItemCount > 0 )
            {
               A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cmbContagemResultadoNotificacao_Aut.getValidValue(StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut)));
               n1959ContagemResultadoNotificacao_Aut = false;
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultadoNotificacao_Aut,(String)cmbContagemResultadoNotificacao_Aut_Internalname,StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut),(short)1,(String)cmbContagemResultadoNotificacao_Aut_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemResultadoNotificacao_Aut.CurrentValue = StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultadoNotificacao_Aut_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Aut.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_SEC_" + sGXsfl_8_idx;
            cmbContagemResultadoNotificacao_Sec.Name = GXCCtl;
            cmbContagemResultadoNotificacao_Sec.WebTags = "";
            cmbContagemResultadoNotificacao_Sec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
            cmbContagemResultadoNotificacao_Sec.addItem("1", "SSL e TLS", 0);
            if ( cmbContagemResultadoNotificacao_Sec.ItemCount > 0 )
            {
               A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Sec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0))), "."));
               n1960ContagemResultadoNotificacao_Sec = false;
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultadoNotificacao_Sec,(String)cmbContagemResultadoNotificacao_Sec_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)),(short)1,(String)cmbContagemResultadoNotificacao_Sec_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemResultadoNotificacao_Sec.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultadoNotificacao_Sec_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Sec.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "CONTAGEMRESULTADONOTIFICACAO_LOGGED_" + sGXsfl_8_idx;
            cmbContagemResultadoNotificacao_Logged.Name = GXCCtl;
            cmbContagemResultadoNotificacao_Logged.WebTags = "";
            cmbContagemResultadoNotificacao_Logged.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Sem dados", 0);
            cmbContagemResultadoNotificacao_Logged.addItem("1", "Com sucesso", 0);
            cmbContagemResultadoNotificacao_Logged.addItem("2", "Mal sucedido", 0);
            if ( cmbContagemResultadoNotificacao_Logged.ItemCount > 0 )
            {
               A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Logged.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0))), "."));
               n1961ContagemResultadoNotificacao_Logged = false;
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultadoNotificacao_Logged,(String)cmbContagemResultadoNotificacao_Logged_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)),(short)1,(String)cmbContagemResultadoNotificacao_Logged_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemResultadoNotificacao_Logged.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultadoNotificacao_Logged_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Logged.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_CODIGO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_CODIGO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO";
         edtContagemResultadoNotificacao_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_CODIGO";
         edtContagemResultadoNotificacao_UsuCod_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_USUCOD";
         edtContagemResultadoNotificacao_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         edtContagemResultadoNotificacao_UsuNom_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_USUNOM";
         edtContagemResultadoNotificacao_Assunto_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_ASSUNTO";
         edtContagemResultadoNotificacao_Destinatarios_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS";
         edtContagemResultadoNotificacao_Demandas_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DEMANDAS";
         edtContagemResultadoNotificacao_Host_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_HOST";
         edtContagemResultadoNotificacao_User_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_USER";
         edtContagemResultadoNotificacao_Port_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_PORT";
         cmbContagemResultadoNotificacao_Aut_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_AUT";
         cmbContagemResultadoNotificacao_Sec_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_SEC";
         cmbContagemResultadoNotificacao_Logged_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_LOGGED";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontagemresultadonotificacao_datahora_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         edtavTfcontagemresultadonotificacao_datahora_to_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO";
         edtavDdo_contagemresultadonotificacao_datahoraauxdate_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORAAUXDATE";
         edtavDdo_contagemresultadonotificacao_datahoraauxdateto_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORAAUXDATETO";
         divDdo_contagemresultadonotificacao_datahoraauxdates_Internalname = sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORAAUXDATES";
         edtavTfcontagemresultadonotificacao_usunom_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM";
         edtavTfcontagemresultadonotificacao_usunom_sel_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL";
         edtavTfcontagemresultadonotificacao_assunto_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO";
         edtavTfcontagemresultadonotificacao_assunto_sel_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL";
         edtavTfcontagemresultadonotificacao_destinatarios_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS";
         edtavTfcontagemresultadonotificacao_destinatarios_to_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO";
         edtavTfcontagemresultadonotificacao_demandas_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS";
         edtavTfcontagemresultadonotificacao_demandas_to_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO";
         edtavTfcontagemresultadonotificacao_host_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_HOST";
         edtavTfcontagemresultadonotificacao_host_sel_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL";
         edtavTfcontagemresultadonotificacao_user_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_USER";
         edtavTfcontagemresultadonotificacao_user_sel_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL";
         edtavTfcontagemresultadonotificacao_port_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_PORT";
         edtavTfcontagemresultadonotificacao_port_to_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO";
         edtavTfcontagemresultadonotificacao_aut_sel_Internalname = sPrefix+"vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL";
         Ddo_contagemresultadonotificacao_datahora_Internalname = sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         edtavDdo_contagemresultadonotificacao_datahoratitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadonotificacao_usunom_Internalname = sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM";
         edtavDdo_contagemresultadonotificacao_usunomtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadonotificacao_assunto_Internalname = sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO";
         edtavDdo_contagemresultadonotificacao_assuntotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadonotificacao_destinatarios_Internalname = sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS";
         edtavDdo_contagemresultadonotificacao_destinatariostitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadonotificacao_demandas_Internalname = sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS";
         edtavDdo_contagemresultadonotificacao_demandastitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadonotificacao_host_Internalname = sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_HOST";
         edtavDdo_contagemresultadonotificacao_hosttitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadonotificacao_user_Internalname = sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_USER";
         edtavDdo_contagemresultadonotificacao_usertitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadonotificacao_port_Internalname = sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_PORT";
         edtavDdo_contagemresultadonotificacao_porttitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadonotificacao_aut_Internalname = sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_AUT";
         edtavDdo_contagemresultadonotificacao_auttitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadonotificacao_sec_Internalname = sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_SEC";
         edtavDdo_contagemresultadonotificacao_sectitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadonotificacao_logged_Internalname = sPrefix+"DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED";
         edtavDdo_contagemresultadonotificacao_loggedtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbContagemResultadoNotificacao_Logged_Jsonclick = "";
         cmbContagemResultadoNotificacao_Sec_Jsonclick = "";
         cmbContagemResultadoNotificacao_Aut_Jsonclick = "";
         edtContagemResultadoNotificacao_Port_Jsonclick = "";
         edtContagemResultadoNotificacao_User_Jsonclick = "";
         edtContagemResultadoNotificacao_Host_Jsonclick = "";
         edtContagemResultadoNotificacao_Demandas_Jsonclick = "";
         edtContagemResultadoNotificacao_Destinatarios_Jsonclick = "";
         edtContagemResultadoNotificacao_Assunto_Jsonclick = "";
         edtContagemResultadoNotificacao_UsuNom_Jsonclick = "";
         edtContagemResultadoNotificacao_DataHora_Jsonclick = "";
         edtContagemResultadoNotificacao_UsuCod_Jsonclick = "";
         edtContagemResultadoNotificacao_Codigo_Jsonclick = "";
         edtContagemResultado_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContagemResultadoNotificacao_UsuNom_Link = "";
         edtContagemResultadoNotificacao_DataHora_Link = "";
         cmbContagemResultadoNotificacao_Logged_Titleformat = 0;
         cmbContagemResultadoNotificacao_Sec_Titleformat = 0;
         cmbContagemResultadoNotificacao_Aut_Titleformat = 0;
         edtContagemResultadoNotificacao_Port_Titleformat = 0;
         edtContagemResultadoNotificacao_User_Titleformat = 0;
         edtContagemResultadoNotificacao_Host_Titleformat = 0;
         edtContagemResultadoNotificacao_Demandas_Titleformat = 0;
         edtContagemResultadoNotificacao_Destinatarios_Titleformat = 0;
         edtContagemResultadoNotificacao_Assunto_Titleformat = 0;
         edtContagemResultadoNotificacao_UsuNom_Titleformat = 0;
         edtContagemResultadoNotificacao_DataHora_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtContagemResultadoNotificacao_DataHora_Linktarget = "";
         cmbContagemResultadoNotificacao_Logged.Title.Text = "Login";
         cmbContagemResultadoNotificacao_Sec.Title.Text = "Seguran�a";
         cmbContagemResultadoNotificacao_Aut.Title.Text = "Autentica��o";
         edtContagemResultadoNotificacao_Port_Title = "Porta";
         edtContagemResultadoNotificacao_User_Title = "Usu�rio";
         edtContagemResultadoNotificacao_Host_Title = "Host";
         edtContagemResultadoNotificacao_Demandas_Title = "Demandas";
         edtContagemResultadoNotificacao_Destinatarios_Title = "Destinat�rios";
         edtContagemResultadoNotificacao_Assunto_Title = "Assunto";
         edtContagemResultadoNotificacao_UsuNom_Title = "Remetente";
         edtContagemResultadoNotificacao_DataHora_Title = "Data";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contagemresultadonotificacao_loggedtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadonotificacao_sectitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadonotificacao_auttitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadonotificacao_porttitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadonotificacao_usertitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadonotificacao_hosttitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadonotificacao_demandastitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadonotificacao_destinatariostitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadonotificacao_assuntotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadonotificacao_usunomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadonotificacao_datahoratitlecontrolidtoreplace_Visible = 1;
         edtavTfcontagemresultadonotificacao_aut_sel_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_aut_sel_Visible = 1;
         edtavTfcontagemresultadonotificacao_port_to_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_port_to_Visible = 1;
         edtavTfcontagemresultadonotificacao_port_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_port_Visible = 1;
         edtavTfcontagemresultadonotificacao_user_sel_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_user_sel_Visible = 1;
         edtavTfcontagemresultadonotificacao_user_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_user_Visible = 1;
         edtavTfcontagemresultadonotificacao_host_sel_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_host_sel_Visible = 1;
         edtavTfcontagemresultadonotificacao_host_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_host_Visible = 1;
         edtavTfcontagemresultadonotificacao_demandas_to_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_demandas_to_Visible = 1;
         edtavTfcontagemresultadonotificacao_demandas_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_demandas_Visible = 1;
         edtavTfcontagemresultadonotificacao_destinatarios_to_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_destinatarios_to_Visible = 1;
         edtavTfcontagemresultadonotificacao_destinatarios_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_destinatarios_Visible = 1;
         edtavTfcontagemresultadonotificacao_assunto_sel_Visible = 1;
         edtavTfcontagemresultadonotificacao_assunto_Visible = 1;
         edtavTfcontagemresultadonotificacao_usunom_sel_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_usunom_sel_Visible = 1;
         edtavTfcontagemresultadonotificacao_usunom_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_usunom_Visible = 1;
         edtavDdo_contagemresultadonotificacao_datahoraauxdateto_Jsonclick = "";
         edtavDdo_contagemresultadonotificacao_datahoraauxdate_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_datahora_to_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_datahora_to_Visible = 1;
         edtavTfcontagemresultadonotificacao_datahora_Jsonclick = "";
         edtavTfcontagemresultadonotificacao_datahora_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         Ddo_contagemresultadonotificacao_logged_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contagemresultadonotificacao_logged_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadonotificacao_logged_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadonotificacao_logged_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadonotificacao_logged_Datalistfixedvalues = "1:Com sucesso,2:Mal sucedido";
         Ddo_contagemresultadonotificacao_logged_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_logged_Datalisttype = "FixedValues";
         Ddo_contagemresultadonotificacao_logged_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_logged_Includefilter = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_logged_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_logged_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_logged_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadonotificacao_logged_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadonotificacao_logged_Cls = "ColumnSettings";
         Ddo_contagemresultadonotificacao_logged_Tooltip = "Op��es";
         Ddo_contagemresultadonotificacao_logged_Caption = "";
         Ddo_contagemresultadonotificacao_sec_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contagemresultadonotificacao_sec_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadonotificacao_sec_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadonotificacao_sec_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadonotificacao_sec_Datalistfixedvalues = "1:SSL e TLS";
         Ddo_contagemresultadonotificacao_sec_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_sec_Datalisttype = "FixedValues";
         Ddo_contagemresultadonotificacao_sec_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_sec_Includefilter = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_sec_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_sec_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_sec_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadonotificacao_sec_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadonotificacao_sec_Cls = "ColumnSettings";
         Ddo_contagemresultadonotificacao_sec_Tooltip = "Op��es";
         Ddo_contagemresultadonotificacao_sec_Caption = "";
         Ddo_contagemresultadonotificacao_aut_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadonotificacao_aut_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadonotificacao_aut_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadonotificacao_aut_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadonotificacao_aut_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contagemresultadonotificacao_aut_Datalisttype = "FixedValues";
         Ddo_contagemresultadonotificacao_aut_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_aut_Includefilter = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_aut_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_aut_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_aut_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadonotificacao_aut_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadonotificacao_aut_Cls = "ColumnSettings";
         Ddo_contagemresultadonotificacao_aut_Tooltip = "Op��es";
         Ddo_contagemresultadonotificacao_aut_Caption = "";
         Ddo_contagemresultadonotificacao_port_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadonotificacao_port_Rangefilterto = "At�";
         Ddo_contagemresultadonotificacao_port_Rangefilterfrom = "Desde";
         Ddo_contagemresultadonotificacao_port_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadonotificacao_port_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadonotificacao_port_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadonotificacao_port_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_port_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_port_Filtertype = "Numeric";
         Ddo_contagemresultadonotificacao_port_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_port_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_port_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_port_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadonotificacao_port_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadonotificacao_port_Cls = "ColumnSettings";
         Ddo_contagemresultadonotificacao_port_Tooltip = "Op��es";
         Ddo_contagemresultadonotificacao_port_Caption = "";
         Ddo_contagemresultadonotificacao_user_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadonotificacao_user_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadonotificacao_user_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadonotificacao_user_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadonotificacao_user_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadonotificacao_user_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadonotificacao_user_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadonotificacao_user_Datalistproc = "GetDemandaNotificacoesWCFilterData";
         Ddo_contagemresultadonotificacao_user_Datalisttype = "Dynamic";
         Ddo_contagemresultadonotificacao_user_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_user_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_user_Filtertype = "Character";
         Ddo_contagemresultadonotificacao_user_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_user_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_user_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_user_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadonotificacao_user_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadonotificacao_user_Cls = "ColumnSettings";
         Ddo_contagemresultadonotificacao_user_Tooltip = "Op��es";
         Ddo_contagemresultadonotificacao_user_Caption = "";
         Ddo_contagemresultadonotificacao_host_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadonotificacao_host_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadonotificacao_host_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadonotificacao_host_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadonotificacao_host_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadonotificacao_host_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadonotificacao_host_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadonotificacao_host_Datalistproc = "GetDemandaNotificacoesWCFilterData";
         Ddo_contagemresultadonotificacao_host_Datalisttype = "Dynamic";
         Ddo_contagemresultadonotificacao_host_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_host_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_host_Filtertype = "Character";
         Ddo_contagemresultadonotificacao_host_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_host_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_host_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_host_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadonotificacao_host_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadonotificacao_host_Cls = "ColumnSettings";
         Ddo_contagemresultadonotificacao_host_Tooltip = "Op��es";
         Ddo_contagemresultadonotificacao_host_Caption = "";
         Ddo_contagemresultadonotificacao_demandas_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadonotificacao_demandas_Rangefilterto = "At�";
         Ddo_contagemresultadonotificacao_demandas_Rangefilterfrom = "Desde";
         Ddo_contagemresultadonotificacao_demandas_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadonotificacao_demandas_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_demandas_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_demandas_Filtertype = "Numeric";
         Ddo_contagemresultadonotificacao_demandas_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_demandas_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_demandas_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_demandas_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadonotificacao_demandas_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadonotificacao_demandas_Cls = "ColumnSettings";
         Ddo_contagemresultadonotificacao_demandas_Tooltip = "Op��es";
         Ddo_contagemresultadonotificacao_demandas_Caption = "";
         Ddo_contagemresultadonotificacao_destinatarios_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadonotificacao_destinatarios_Rangefilterto = "At�";
         Ddo_contagemresultadonotificacao_destinatarios_Rangefilterfrom = "Desde";
         Ddo_contagemresultadonotificacao_destinatarios_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadonotificacao_destinatarios_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_destinatarios_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_destinatarios_Filtertype = "Numeric";
         Ddo_contagemresultadonotificacao_destinatarios_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_destinatarios_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_destinatarios_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_destinatarios_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadonotificacao_destinatarios_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadonotificacao_destinatarios_Cls = "ColumnSettings";
         Ddo_contagemresultadonotificacao_destinatarios_Tooltip = "Op��es";
         Ddo_contagemresultadonotificacao_destinatarios_Caption = "";
         Ddo_contagemresultadonotificacao_assunto_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadonotificacao_assunto_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadonotificacao_assunto_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadonotificacao_assunto_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadonotificacao_assunto_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadonotificacao_assunto_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadonotificacao_assunto_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadonotificacao_assunto_Datalistproc = "GetDemandaNotificacoesWCFilterData";
         Ddo_contagemresultadonotificacao_assunto_Datalisttype = "Dynamic";
         Ddo_contagemresultadonotificacao_assunto_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_assunto_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_assunto_Filtertype = "Character";
         Ddo_contagemresultadonotificacao_assunto_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_assunto_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_assunto_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_assunto_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadonotificacao_assunto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadonotificacao_assunto_Cls = "ColumnSettings";
         Ddo_contagemresultadonotificacao_assunto_Tooltip = "Op��es";
         Ddo_contagemresultadonotificacao_assunto_Caption = "";
         Ddo_contagemresultadonotificacao_usunom_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadonotificacao_usunom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadonotificacao_usunom_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadonotificacao_usunom_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadonotificacao_usunom_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadonotificacao_usunom_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadonotificacao_usunom_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadonotificacao_usunom_Datalistproc = "GetDemandaNotificacoesWCFilterData";
         Ddo_contagemresultadonotificacao_usunom_Datalisttype = "Dynamic";
         Ddo_contagemresultadonotificacao_usunom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_usunom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_usunom_Filtertype = "Character";
         Ddo_contagemresultadonotificacao_usunom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_usunom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_usunom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_usunom_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadonotificacao_usunom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadonotificacao_usunom_Cls = "ColumnSettings";
         Ddo_contagemresultadonotificacao_usunom_Tooltip = "Op��es";
         Ddo_contagemresultadonotificacao_usunom_Caption = "";
         Ddo_contagemresultadonotificacao_datahora_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadonotificacao_datahora_Rangefilterto = "At�";
         Ddo_contagemresultadonotificacao_datahora_Rangefilterfrom = "Desde";
         Ddo_contagemresultadonotificacao_datahora_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadonotificacao_datahora_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadonotificacao_datahora_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadonotificacao_datahora_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadonotificacao_datahora_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_datahora_Filtertype = "Date";
         Ddo_contagemresultadonotificacao_datahora_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_datahora_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_datahora_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadonotificacao_datahora_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadonotificacao_datahora_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadonotificacao_datahora_Cls = "ColumnSettings";
         Ddo_contagemresultadonotificacao_datahora_Tooltip = "Op��es";
         Ddo_contagemresultadonotificacao_datahora_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null}],oparms:[{av:'AV16ContagemResultadoNotificacao_DataHoraTitleFilterData',fld:'vCONTAGEMRESULTADONOTIFICACAO_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV22ContagemResultadoNotificacao_UsuNomTitleFilterData',fld:'vCONTAGEMRESULTADONOTIFICACAO_USUNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV63ContagemResultadoNotificacao_AssuntoTitleFilterData',fld:'vCONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV26ContagemResultadoNotificacao_DestinatariosTitleFilterData',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLEFILTERDATA',pic:'',nv:null},{av:'AV30ContagemResultadoNotificacao_DemandasTitleFilterData',fld:'vCONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLEFILTERDATA',pic:'',nv:null},{av:'AV34ContagemResultadoNotificacao_HostTitleFilterData',fld:'vCONTAGEMRESULTADONOTIFICACAO_HOSTTITLEFILTERDATA',pic:'',nv:null},{av:'AV38ContagemResultadoNotificacao_UserTitleFilterData',fld:'vCONTAGEMRESULTADONOTIFICACAO_USERTITLEFILTERDATA',pic:'',nv:null},{av:'AV42ContagemResultadoNotificacao_PortTitleFilterData',fld:'vCONTAGEMRESULTADONOTIFICACAO_PORTTITLEFILTERDATA',pic:'',nv:null},{av:'AV46ContagemResultadoNotificacao_AutTitleFilterData',fld:'vCONTAGEMRESULTADONOTIFICACAO_AUTTITLEFILTERDATA',pic:'',nv:null},{av:'AV49ContagemResultadoNotificacao_SecTitleFilterData',fld:'vCONTAGEMRESULTADONOTIFICACAO_SECTITLEFILTERDATA',pic:'',nv:null},{av:'AV53ContagemResultadoNotificacao_LoggedTitleFilterData',fld:'vCONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultadoNotificacao_DataHora_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DataHora_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'Title'},{av:'edtContagemResultadoNotificacao_UsuNom_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_UsuNom_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'Title'},{av:'edtContagemResultadoNotificacao_Assunto_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_Assunto_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'Title'},{av:'edtContagemResultadoNotificacao_Destinatarios_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_Destinatarios_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',prop:'Title'},{av:'edtContagemResultadoNotificacao_Demandas_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DEMANDAS',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_Demandas_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DEMANDAS',prop:'Title'},{av:'edtContagemResultadoNotificacao_Host_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_Host_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'Title'},{av:'edtContagemResultadoNotificacao_User_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USER',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_User_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USER',prop:'Title'},{av:'edtContagemResultadoNotificacao_Port_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_Port_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'Title'},{av:'cmbContagemResultadoNotificacao_Aut'},{av:'cmbContagemResultadoNotificacao_Sec'},{av:'cmbContagemResultadoNotificacao_Logged'},{av:'AV59GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtContagemResultadoNotificacao_DataHora_Linktarget',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'Linktarget'},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11QX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA.ONOPTIONCLICKED","{handler:'E12QX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadonotificacao_datahora_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadonotificacao_datahora_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'FilteredText_get'},{av:'Ddo_contagemresultadonotificacao_datahora_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadonotificacao_datahora_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'SortedStatus'},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadonotificacao_usunom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_assunto_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_host_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_user_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USER',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_port_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_aut_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_AUT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_sec_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_SEC',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_logged_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM.ONOPTIONCLICKED","{handler:'E13QX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadonotificacao_usunom_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadonotificacao_usunom_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'FilteredText_get'},{av:'Ddo_contagemresultadonotificacao_usunom_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadonotificacao_usunom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'SortedStatus'},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'Ddo_contagemresultadonotificacao_datahora_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_assunto_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_host_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_user_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USER',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_port_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_aut_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_AUT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_sec_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_SEC',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_logged_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO.ONOPTIONCLICKED","{handler:'E14QX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadonotificacao_assunto_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadonotificacao_assunto_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'FilteredText_get'},{av:'Ddo_contagemresultadonotificacao_assunto_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadonotificacao_assunto_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'SortedStatus'},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'Ddo_contagemresultadonotificacao_datahora_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_usunom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_host_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_user_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USER',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_port_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_aut_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_AUT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_sec_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_SEC',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_logged_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS.ONOPTIONCLICKED","{handler:'E15QX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadonotificacao_destinatarios_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadonotificacao_destinatarios_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',prop:'FilteredText_get'},{av:'Ddo_contagemresultadonotificacao_destinatarios_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS.ONOPTIONCLICKED","{handler:'E16QX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadonotificacao_demandas_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadonotificacao_demandas_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS',prop:'FilteredText_get'},{av:'Ddo_contagemresultadonotificacao_demandas_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDAS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADONOTIFICACAO_HOST.ONOPTIONCLICKED","{handler:'E17QX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadonotificacao_host_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadonotificacao_host_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'FilteredText_get'},{av:'Ddo_contagemresultadonotificacao_host_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadonotificacao_host_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'SortedStatus'},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'Ddo_contagemresultadonotificacao_datahora_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_usunom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_assunto_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_user_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USER',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_port_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_aut_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_AUT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_sec_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_SEC',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_logged_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADONOTIFICACAO_USER.ONOPTIONCLICKED","{handler:'E18QX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadonotificacao_user_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USER',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadonotificacao_user_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USER',prop:'FilteredText_get'},{av:'Ddo_contagemresultadonotificacao_user_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USER',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadonotificacao_user_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USER',prop:'SortedStatus'},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'Ddo_contagemresultadonotificacao_datahora_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_usunom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_assunto_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_host_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_port_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_aut_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_AUT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_sec_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_SEC',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_logged_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADONOTIFICACAO_PORT.ONOPTIONCLICKED","{handler:'E19QX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadonotificacao_port_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadonotificacao_port_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'FilteredText_get'},{av:'Ddo_contagemresultadonotificacao_port_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadonotificacao_port_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'SortedStatus'},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contagemresultadonotificacao_datahora_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_usunom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_assunto_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_host_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_user_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USER',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_aut_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_AUT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_sec_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_SEC',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_logged_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADONOTIFICACAO_AUT.ONOPTIONCLICKED","{handler:'E20QX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadonotificacao_aut_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_AUT',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadonotificacao_aut_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_AUT',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadonotificacao_aut_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_AUT',prop:'SortedStatus'},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'Ddo_contagemresultadonotificacao_datahora_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_usunom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_assunto_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_host_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_user_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USER',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_port_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_sec_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_SEC',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_logged_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADONOTIFICACAO_SEC.ONOPTIONCLICKED","{handler:'E21QX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadonotificacao_sec_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_SEC',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadonotificacao_sec_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_SEC',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadonotificacao_sec_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_SEC',prop:'SortedStatus'},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'Ddo_contagemresultadonotificacao_datahora_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_usunom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_assunto_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_host_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_user_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USER',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_port_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_aut_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_AUT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_logged_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED.ONOPTIONCLICKED","{handler:'E22QX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFContagemResultadoNotificacao_DataHora',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFContagemResultadoNotificacao_DataHora_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoNotificacao_UsuNom',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM',pic:'@!',nv:''},{av:'AV24TFContagemResultadoNotificacao_UsuNom_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USUNOM_SEL',pic:'@!',nv:''},{av:'AV64TFContagemResultadoNotificacao_Assunto',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'AV65TFContagemResultadoNotificacao_Assunto_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_ASSUNTO_SEL',pic:'',nv:''},{av:'AV27TFContagemResultadoNotificacao_Destinatarios',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS',pic:'ZZZZZ9',nv:0},{av:'AV28TFContagemResultadoNotificacao_Destinatarios_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DESTINATARIOS_TO',pic:'ZZZZZ9',nv:0},{av:'AV31TFContagemResultadoNotificacao_Demandas',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoNotificacao_Demandas_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_DEMANDAS_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoNotificacao_Host',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST',pic:'@!',nv:''},{av:'AV36TFContagemResultadoNotificacao_Host_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_HOST_SEL',pic:'@!',nv:''},{av:'AV39TFContagemResultadoNotificacao_User',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER',pic:'@!',nv:''},{av:'AV40TFContagemResultadoNotificacao_User_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_USER_SEL',pic:'@!',nv:''},{av:'AV43TFContagemResultadoNotificacao_Port',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT',pic:'ZZZ9',nv:0},{av:'AV44TFContagemResultadoNotificacao_Port_To',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_PORT_TO',pic:'ZZZ9',nv:0},{av:'AV47TFContagemResultadoNotificacao_Aut_Sel',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_AUT_SEL',pic:'9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USUNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DESTINATARIOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_DEMANDASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_HOSTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_USERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_PORTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_AUTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_SECTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADONOTIFICACAO_LOGGEDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV51TFContagemResultadoNotificacao_Sec_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_SEC_SELS',pic:'',nv:null},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_contagemresultadonotificacao_logged_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadonotificacao_logged_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadonotificacao_logged_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_LOGGED',prop:'SortedStatus'},{av:'AV55TFContagemResultadoNotificacao_Logged_Sels',fld:'vTFCONTAGEMRESULTADONOTIFICACAO_LOGGED_SELS',pic:'',nv:null},{av:'Ddo_contagemresultadonotificacao_datahora_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_usunom_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_assunto_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_host_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_HOST',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_user_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_USER',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_port_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_PORT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_aut_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_AUT',prop:'SortedStatus'},{av:'Ddo_contagemresultadonotificacao_sec_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADONOTIFICACAO_SEC',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E25QX2',iparms:[{av:'A1413ContagemResultadoNotificacao_UsuCod',fld:'CONTAGEMRESULTADONOTIFICACAO_USUCOD',pic:'ZZZZZ9',nv:0},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'edtContagemResultadoNotificacao_UsuNom_Link',ctrl:'CONTAGEMRESULTADONOTIFICACAO_USUNOM',prop:'Link'},{av:'edtContagemResultadoNotificacao_DataHora_Link',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DATAHORA',prop:'Link'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contagemresultadonotificacao_datahora_Activeeventkey = "";
         Ddo_contagemresultadonotificacao_datahora_Filteredtext_get = "";
         Ddo_contagemresultadonotificacao_datahora_Filteredtextto_get = "";
         Ddo_contagemresultadonotificacao_usunom_Activeeventkey = "";
         Ddo_contagemresultadonotificacao_usunom_Filteredtext_get = "";
         Ddo_contagemresultadonotificacao_usunom_Selectedvalue_get = "";
         Ddo_contagemresultadonotificacao_assunto_Activeeventkey = "";
         Ddo_contagemresultadonotificacao_assunto_Filteredtext_get = "";
         Ddo_contagemresultadonotificacao_assunto_Selectedvalue_get = "";
         Ddo_contagemresultadonotificacao_destinatarios_Activeeventkey = "";
         Ddo_contagemresultadonotificacao_destinatarios_Filteredtext_get = "";
         Ddo_contagemresultadonotificacao_destinatarios_Filteredtextto_get = "";
         Ddo_contagemresultadonotificacao_demandas_Activeeventkey = "";
         Ddo_contagemresultadonotificacao_demandas_Filteredtext_get = "";
         Ddo_contagemresultadonotificacao_demandas_Filteredtextto_get = "";
         Ddo_contagemresultadonotificacao_host_Activeeventkey = "";
         Ddo_contagemresultadonotificacao_host_Filteredtext_get = "";
         Ddo_contagemresultadonotificacao_host_Selectedvalue_get = "";
         Ddo_contagemresultadonotificacao_user_Activeeventkey = "";
         Ddo_contagemresultadonotificacao_user_Filteredtext_get = "";
         Ddo_contagemresultadonotificacao_user_Selectedvalue_get = "";
         Ddo_contagemresultadonotificacao_port_Activeeventkey = "";
         Ddo_contagemresultadonotificacao_port_Filteredtext_get = "";
         Ddo_contagemresultadonotificacao_port_Filteredtextto_get = "";
         Ddo_contagemresultadonotificacao_aut_Activeeventkey = "";
         Ddo_contagemresultadonotificacao_aut_Selectedvalue_get = "";
         Ddo_contagemresultadonotificacao_sec_Activeeventkey = "";
         Ddo_contagemresultadonotificacao_sec_Selectedvalue_get = "";
         Ddo_contagemresultadonotificacao_logged_Activeeventkey = "";
         Ddo_contagemresultadonotificacao_logged_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV17TFContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         AV18TFContagemResultadoNotificacao_DataHora_To = (DateTime)(DateTime.MinValue);
         AV23TFContagemResultadoNotificacao_UsuNom = "";
         AV24TFContagemResultadoNotificacao_UsuNom_Sel = "";
         AV64TFContagemResultadoNotificacao_Assunto = "";
         AV65TFContagemResultadoNotificacao_Assunto_Sel = "";
         AV35TFContagemResultadoNotificacao_Host = "";
         AV36TFContagemResultadoNotificacao_Host_Sel = "";
         AV39TFContagemResultadoNotificacao_User = "";
         AV40TFContagemResultadoNotificacao_User_Sel = "";
         AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace = "";
         AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace = "";
         AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace = "";
         AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace = "";
         AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace = "";
         AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace = "";
         AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace = "";
         AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace = "";
         AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace = "";
         AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace = "";
         AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace = "";
         AV61Codigos = new GxSimpleCollection();
         AV69Pgmname = "";
         AV51TFContagemResultadoNotificacao_Sec_Sels = new GxSimpleCollection();
         AV55TFContagemResultadoNotificacao_Logged_Sels = new GxSimpleCollection();
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV57DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV16ContagemResultadoNotificacao_DataHoraTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV22ContagemResultadoNotificacao_UsuNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63ContagemResultadoNotificacao_AssuntoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV26ContagemResultadoNotificacao_DestinatariosTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV30ContagemResultadoNotificacao_DemandasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ContagemResultadoNotificacao_HostTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ContagemResultadoNotificacao_UserTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42ContagemResultadoNotificacao_PortTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46ContagemResultadoNotificacao_AutTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49ContagemResultadoNotificacao_SecTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53ContagemResultadoNotificacao_LoggedTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contagemresultadonotificacao_datahora_Filteredtext_set = "";
         Ddo_contagemresultadonotificacao_datahora_Filteredtextto_set = "";
         Ddo_contagemresultadonotificacao_datahora_Sortedstatus = "";
         Ddo_contagemresultadonotificacao_usunom_Filteredtext_set = "";
         Ddo_contagemresultadonotificacao_usunom_Selectedvalue_set = "";
         Ddo_contagemresultadonotificacao_usunom_Sortedstatus = "";
         Ddo_contagemresultadonotificacao_assunto_Filteredtext_set = "";
         Ddo_contagemresultadonotificacao_assunto_Selectedvalue_set = "";
         Ddo_contagemresultadonotificacao_assunto_Sortedstatus = "";
         Ddo_contagemresultadonotificacao_destinatarios_Filteredtext_set = "";
         Ddo_contagemresultadonotificacao_destinatarios_Filteredtextto_set = "";
         Ddo_contagemresultadonotificacao_demandas_Filteredtext_set = "";
         Ddo_contagemresultadonotificacao_demandas_Filteredtextto_set = "";
         Ddo_contagemresultadonotificacao_host_Filteredtext_set = "";
         Ddo_contagemresultadonotificacao_host_Selectedvalue_set = "";
         Ddo_contagemresultadonotificacao_host_Sortedstatus = "";
         Ddo_contagemresultadonotificacao_user_Filteredtext_set = "";
         Ddo_contagemresultadonotificacao_user_Selectedvalue_set = "";
         Ddo_contagemresultadonotificacao_user_Sortedstatus = "";
         Ddo_contagemresultadonotificacao_port_Filteredtext_set = "";
         Ddo_contagemresultadonotificacao_port_Filteredtextto_set = "";
         Ddo_contagemresultadonotificacao_port_Sortedstatus = "";
         Ddo_contagemresultadonotificacao_aut_Selectedvalue_set = "";
         Ddo_contagemresultadonotificacao_aut_Sortedstatus = "";
         Ddo_contagemresultadonotificacao_sec_Selectedvalue_set = "";
         Ddo_contagemresultadonotificacao_sec_Sortedstatus = "";
         Ddo_contagemresultadonotificacao_logged_Selectedvalue_set = "";
         Ddo_contagemresultadonotificacao_logged_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate = DateTime.MinValue;
         AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         A1422ContagemResultadoNotificacao_UsuNom = "";
         A1417ContagemResultadoNotificacao_Assunto = "";
         A1956ContagemResultadoNotificacao_Host = "";
         A1957ContagemResultadoNotificacao_User = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV23TFContagemResultadoNotificacao_UsuNom = "";
         lV64TFContagemResultadoNotificacao_Assunto = "";
         lV35TFContagemResultadoNotificacao_Host = "";
         lV39TFContagemResultadoNotificacao_User = "";
         H00QX4_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         H00QX4_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         H00QX4_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         H00QX4_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         H00QX4_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         H00QX4_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         H00QX4_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         H00QX4_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         H00QX4_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         H00QX4_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         H00QX4_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         H00QX4_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         H00QX4_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         H00QX4_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         H00QX4_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         H00QX4_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         H00QX4_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         H00QX4_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         H00QX4_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         H00QX4_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         H00QX4_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         H00QX4_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         H00QX4_A456ContagemResultado_Codigo = new int[1] ;
         H00QX4_A1963ContagemResultadoNotificacao_Demandas = new int[1] ;
         H00QX4_n1963ContagemResultadoNotificacao_Demandas = new bool[] {false} ;
         H00QX4_A1962ContagemResultadoNotificacao_Destinatarios = new int[1] ;
         H00QX4_n1962ContagemResultadoNotificacao_Destinatarios = new bool[] {false} ;
         H00QX7_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV62WebSession = context.GetSession();
         AV50TFContagemResultadoNotificacao_Sec_SelsJson = "";
         AV54TFContagemResultadoNotificacao_Logged_SelsJson = "";
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContagemResultado_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.demandanotificacoeswc__default(),
            new Object[][] {
                new Object[] {
               H00QX4_A1427ContagemResultadoNotificacao_UsuPesCod, H00QX4_n1427ContagemResultadoNotificacao_UsuPesCod, H00QX4_A1961ContagemResultadoNotificacao_Logged, H00QX4_n1961ContagemResultadoNotificacao_Logged, H00QX4_A1960ContagemResultadoNotificacao_Sec, H00QX4_n1960ContagemResultadoNotificacao_Sec, H00QX4_A1959ContagemResultadoNotificacao_Aut, H00QX4_n1959ContagemResultadoNotificacao_Aut, H00QX4_A1958ContagemResultadoNotificacao_Port, H00QX4_n1958ContagemResultadoNotificacao_Port,
               H00QX4_A1957ContagemResultadoNotificacao_User, H00QX4_n1957ContagemResultadoNotificacao_User, H00QX4_A1956ContagemResultadoNotificacao_Host, H00QX4_n1956ContagemResultadoNotificacao_Host, H00QX4_A1417ContagemResultadoNotificacao_Assunto, H00QX4_n1417ContagemResultadoNotificacao_Assunto, H00QX4_A1422ContagemResultadoNotificacao_UsuNom, H00QX4_n1422ContagemResultadoNotificacao_UsuNom, H00QX4_A1416ContagemResultadoNotificacao_DataHora, H00QX4_n1416ContagemResultadoNotificacao_DataHora,
               H00QX4_A1413ContagemResultadoNotificacao_UsuCod, H00QX4_A1412ContagemResultadoNotificacao_Codigo, H00QX4_A456ContagemResultado_Codigo, H00QX4_A1963ContagemResultadoNotificacao_Demandas, H00QX4_n1963ContagemResultadoNotificacao_Demandas, H00QX4_A1962ContagemResultadoNotificacao_Destinatarios, H00QX4_n1962ContagemResultadoNotificacao_Destinatarios
               }
               , new Object[] {
               H00QX7_AGRID_nRecordCount
               }
            }
         );
         AV69Pgmname = "DemandaNotificacoesWC";
         /* GeneXus formulas. */
         AV69Pgmname = "DemandaNotificacoesWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short AV13OrderedBy ;
      private short AV43TFContagemResultadoNotificacao_Port ;
      private short AV44TFContagemResultadoNotificacao_Port_To ;
      private short AV47TFContagemResultadoNotificacao_Aut_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A1958ContagemResultadoNotificacao_Port ;
      private short A1960ContagemResultadoNotificacao_Sec ;
      private short A1961ContagemResultadoNotificacao_Logged ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultadoNotificacao_DataHora_Titleformat ;
      private short edtContagemResultadoNotificacao_UsuNom_Titleformat ;
      private short edtContagemResultadoNotificacao_Assunto_Titleformat ;
      private short edtContagemResultadoNotificacao_Destinatarios_Titleformat ;
      private short edtContagemResultadoNotificacao_Demandas_Titleformat ;
      private short edtContagemResultadoNotificacao_Host_Titleformat ;
      private short edtContagemResultadoNotificacao_User_Titleformat ;
      private short edtContagemResultadoNotificacao_Port_Titleformat ;
      private short cmbContagemResultadoNotificacao_Aut_Titleformat ;
      private short cmbContagemResultadoNotificacao_Sec_Titleformat ;
      private short cmbContagemResultadoNotificacao_Logged_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContagemResultado_Codigo ;
      private int wcpOAV7ContagemResultado_Codigo ;
      private int subGrid_Rows ;
      private int AV27TFContagemResultadoNotificacao_Destinatarios ;
      private int AV28TFContagemResultadoNotificacao_Destinatarios_To ;
      private int AV31TFContagemResultadoNotificacao_Demandas ;
      private int AV32TFContagemResultadoNotificacao_Demandas_To ;
      private int A1413ContagemResultadoNotificacao_UsuCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contagemresultadonotificacao_usunom_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultadonotificacao_assunto_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultadonotificacao_host_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultadonotificacao_user_Datalistupdateminimumcharacters ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontagemresultadonotificacao_datahora_Visible ;
      private int edtavTfcontagemresultadonotificacao_datahora_to_Visible ;
      private int edtavTfcontagemresultadonotificacao_usunom_Visible ;
      private int edtavTfcontagemresultadonotificacao_usunom_sel_Visible ;
      private int edtavTfcontagemresultadonotificacao_assunto_Visible ;
      private int edtavTfcontagemresultadonotificacao_assunto_sel_Visible ;
      private int edtavTfcontagemresultadonotificacao_destinatarios_Visible ;
      private int edtavTfcontagemresultadonotificacao_destinatarios_to_Visible ;
      private int edtavTfcontagemresultadonotificacao_demandas_Visible ;
      private int edtavTfcontagemresultadonotificacao_demandas_to_Visible ;
      private int edtavTfcontagemresultadonotificacao_host_Visible ;
      private int edtavTfcontagemresultadonotificacao_host_sel_Visible ;
      private int edtavTfcontagemresultadonotificacao_user_Visible ;
      private int edtavTfcontagemresultadonotificacao_user_sel_Visible ;
      private int edtavTfcontagemresultadonotificacao_port_Visible ;
      private int edtavTfcontagemresultadonotificacao_port_to_Visible ;
      private int edtavTfcontagemresultadonotificacao_aut_sel_Visible ;
      private int edtavDdo_contagemresultadonotificacao_datahoratitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadonotificacao_usunomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadonotificacao_assuntotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadonotificacao_destinatariostitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadonotificacao_demandastitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadonotificacao_hosttitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadonotificacao_usertitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadonotificacao_porttitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadonotificacao_auttitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadonotificacao_sectitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadonotificacao_loggedtitlecontrolidtoreplace_Visible ;
      private int A456ContagemResultado_Codigo ;
      private int A1962ContagemResultadoNotificacao_Destinatarios ;
      private int A1963ContagemResultadoNotificacao_Demandas ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV51TFContagemResultadoNotificacao_Sec_Sels_Count ;
      private int AV55TFContagemResultadoNotificacao_Logged_Sels_Count ;
      private int A1427ContagemResultadoNotificacao_UsuPesCod ;
      private int AV58PageToGo ;
      private int GXt_int2 ;
      private int AV70GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long A1412ContagemResultadoNotificacao_Codigo ;
      private long GRID_nFirstRecordOnPage ;
      private long AV59GridCurrentPage ;
      private long AV60GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contagemresultadonotificacao_datahora_Activeeventkey ;
      private String Ddo_contagemresultadonotificacao_datahora_Filteredtext_get ;
      private String Ddo_contagemresultadonotificacao_datahora_Filteredtextto_get ;
      private String Ddo_contagemresultadonotificacao_usunom_Activeeventkey ;
      private String Ddo_contagemresultadonotificacao_usunom_Filteredtext_get ;
      private String Ddo_contagemresultadonotificacao_usunom_Selectedvalue_get ;
      private String Ddo_contagemresultadonotificacao_assunto_Activeeventkey ;
      private String Ddo_contagemresultadonotificacao_assunto_Filteredtext_get ;
      private String Ddo_contagemresultadonotificacao_assunto_Selectedvalue_get ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Activeeventkey ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Filteredtext_get ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Filteredtextto_get ;
      private String Ddo_contagemresultadonotificacao_demandas_Activeeventkey ;
      private String Ddo_contagemresultadonotificacao_demandas_Filteredtext_get ;
      private String Ddo_contagemresultadonotificacao_demandas_Filteredtextto_get ;
      private String Ddo_contagemresultadonotificacao_host_Activeeventkey ;
      private String Ddo_contagemresultadonotificacao_host_Filteredtext_get ;
      private String Ddo_contagemresultadonotificacao_host_Selectedvalue_get ;
      private String Ddo_contagemresultadonotificacao_user_Activeeventkey ;
      private String Ddo_contagemresultadonotificacao_user_Filteredtext_get ;
      private String Ddo_contagemresultadonotificacao_user_Selectedvalue_get ;
      private String Ddo_contagemresultadonotificacao_port_Activeeventkey ;
      private String Ddo_contagemresultadonotificacao_port_Filteredtext_get ;
      private String Ddo_contagemresultadonotificacao_port_Filteredtextto_get ;
      private String Ddo_contagemresultadonotificacao_aut_Activeeventkey ;
      private String Ddo_contagemresultadonotificacao_aut_Selectedvalue_get ;
      private String Ddo_contagemresultadonotificacao_sec_Activeeventkey ;
      private String Ddo_contagemresultadonotificacao_sec_Selectedvalue_get ;
      private String Ddo_contagemresultadonotificacao_logged_Activeeventkey ;
      private String Ddo_contagemresultadonotificacao_logged_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String AV23TFContagemResultadoNotificacao_UsuNom ;
      private String AV24TFContagemResultadoNotificacao_UsuNom_Sel ;
      private String AV35TFContagemResultadoNotificacao_Host ;
      private String AV36TFContagemResultadoNotificacao_Host_Sel ;
      private String AV39TFContagemResultadoNotificacao_User ;
      private String AV40TFContagemResultadoNotificacao_User_Sel ;
      private String AV69Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contagemresultadonotificacao_datahora_Caption ;
      private String Ddo_contagemresultadonotificacao_datahora_Tooltip ;
      private String Ddo_contagemresultadonotificacao_datahora_Cls ;
      private String Ddo_contagemresultadonotificacao_datahora_Filteredtext_set ;
      private String Ddo_contagemresultadonotificacao_datahora_Filteredtextto_set ;
      private String Ddo_contagemresultadonotificacao_datahora_Dropdownoptionstype ;
      private String Ddo_contagemresultadonotificacao_datahora_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadonotificacao_datahora_Sortedstatus ;
      private String Ddo_contagemresultadonotificacao_datahora_Filtertype ;
      private String Ddo_contagemresultadonotificacao_datahora_Sortasc ;
      private String Ddo_contagemresultadonotificacao_datahora_Sortdsc ;
      private String Ddo_contagemresultadonotificacao_datahora_Cleanfilter ;
      private String Ddo_contagemresultadonotificacao_datahora_Rangefilterfrom ;
      private String Ddo_contagemresultadonotificacao_datahora_Rangefilterto ;
      private String Ddo_contagemresultadonotificacao_datahora_Searchbuttontext ;
      private String Ddo_contagemresultadonotificacao_usunom_Caption ;
      private String Ddo_contagemresultadonotificacao_usunom_Tooltip ;
      private String Ddo_contagemresultadonotificacao_usunom_Cls ;
      private String Ddo_contagemresultadonotificacao_usunom_Filteredtext_set ;
      private String Ddo_contagemresultadonotificacao_usunom_Selectedvalue_set ;
      private String Ddo_contagemresultadonotificacao_usunom_Dropdownoptionstype ;
      private String Ddo_contagemresultadonotificacao_usunom_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadonotificacao_usunom_Sortedstatus ;
      private String Ddo_contagemresultadonotificacao_usunom_Filtertype ;
      private String Ddo_contagemresultadonotificacao_usunom_Datalisttype ;
      private String Ddo_contagemresultadonotificacao_usunom_Datalistproc ;
      private String Ddo_contagemresultadonotificacao_usunom_Sortasc ;
      private String Ddo_contagemresultadonotificacao_usunom_Sortdsc ;
      private String Ddo_contagemresultadonotificacao_usunom_Loadingdata ;
      private String Ddo_contagemresultadonotificacao_usunom_Cleanfilter ;
      private String Ddo_contagemresultadonotificacao_usunom_Noresultsfound ;
      private String Ddo_contagemresultadonotificacao_usunom_Searchbuttontext ;
      private String Ddo_contagemresultadonotificacao_assunto_Caption ;
      private String Ddo_contagemresultadonotificacao_assunto_Tooltip ;
      private String Ddo_contagemresultadonotificacao_assunto_Cls ;
      private String Ddo_contagemresultadonotificacao_assunto_Filteredtext_set ;
      private String Ddo_contagemresultadonotificacao_assunto_Selectedvalue_set ;
      private String Ddo_contagemresultadonotificacao_assunto_Dropdownoptionstype ;
      private String Ddo_contagemresultadonotificacao_assunto_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadonotificacao_assunto_Sortedstatus ;
      private String Ddo_contagemresultadonotificacao_assunto_Filtertype ;
      private String Ddo_contagemresultadonotificacao_assunto_Datalisttype ;
      private String Ddo_contagemresultadonotificacao_assunto_Datalistproc ;
      private String Ddo_contagemresultadonotificacao_assunto_Sortasc ;
      private String Ddo_contagemresultadonotificacao_assunto_Sortdsc ;
      private String Ddo_contagemresultadonotificacao_assunto_Loadingdata ;
      private String Ddo_contagemresultadonotificacao_assunto_Cleanfilter ;
      private String Ddo_contagemresultadonotificacao_assunto_Noresultsfound ;
      private String Ddo_contagemresultadonotificacao_assunto_Searchbuttontext ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Caption ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Tooltip ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Cls ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Filteredtext_set ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Filteredtextto_set ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Dropdownoptionstype ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Filtertype ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Cleanfilter ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Rangefilterfrom ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Rangefilterto ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Searchbuttontext ;
      private String Ddo_contagemresultadonotificacao_demandas_Caption ;
      private String Ddo_contagemresultadonotificacao_demandas_Tooltip ;
      private String Ddo_contagemresultadonotificacao_demandas_Cls ;
      private String Ddo_contagemresultadonotificacao_demandas_Filteredtext_set ;
      private String Ddo_contagemresultadonotificacao_demandas_Filteredtextto_set ;
      private String Ddo_contagemresultadonotificacao_demandas_Dropdownoptionstype ;
      private String Ddo_contagemresultadonotificacao_demandas_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadonotificacao_demandas_Filtertype ;
      private String Ddo_contagemresultadonotificacao_demandas_Cleanfilter ;
      private String Ddo_contagemresultadonotificacao_demandas_Rangefilterfrom ;
      private String Ddo_contagemresultadonotificacao_demandas_Rangefilterto ;
      private String Ddo_contagemresultadonotificacao_demandas_Searchbuttontext ;
      private String Ddo_contagemresultadonotificacao_host_Caption ;
      private String Ddo_contagemresultadonotificacao_host_Tooltip ;
      private String Ddo_contagemresultadonotificacao_host_Cls ;
      private String Ddo_contagemresultadonotificacao_host_Filteredtext_set ;
      private String Ddo_contagemresultadonotificacao_host_Selectedvalue_set ;
      private String Ddo_contagemresultadonotificacao_host_Dropdownoptionstype ;
      private String Ddo_contagemresultadonotificacao_host_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadonotificacao_host_Sortedstatus ;
      private String Ddo_contagemresultadonotificacao_host_Filtertype ;
      private String Ddo_contagemresultadonotificacao_host_Datalisttype ;
      private String Ddo_contagemresultadonotificacao_host_Datalistproc ;
      private String Ddo_contagemresultadonotificacao_host_Sortasc ;
      private String Ddo_contagemresultadonotificacao_host_Sortdsc ;
      private String Ddo_contagemresultadonotificacao_host_Loadingdata ;
      private String Ddo_contagemresultadonotificacao_host_Cleanfilter ;
      private String Ddo_contagemresultadonotificacao_host_Noresultsfound ;
      private String Ddo_contagemresultadonotificacao_host_Searchbuttontext ;
      private String Ddo_contagemresultadonotificacao_user_Caption ;
      private String Ddo_contagemresultadonotificacao_user_Tooltip ;
      private String Ddo_contagemresultadonotificacao_user_Cls ;
      private String Ddo_contagemresultadonotificacao_user_Filteredtext_set ;
      private String Ddo_contagemresultadonotificacao_user_Selectedvalue_set ;
      private String Ddo_contagemresultadonotificacao_user_Dropdownoptionstype ;
      private String Ddo_contagemresultadonotificacao_user_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadonotificacao_user_Sortedstatus ;
      private String Ddo_contagemresultadonotificacao_user_Filtertype ;
      private String Ddo_contagemresultadonotificacao_user_Datalisttype ;
      private String Ddo_contagemresultadonotificacao_user_Datalistproc ;
      private String Ddo_contagemresultadonotificacao_user_Sortasc ;
      private String Ddo_contagemresultadonotificacao_user_Sortdsc ;
      private String Ddo_contagemresultadonotificacao_user_Loadingdata ;
      private String Ddo_contagemresultadonotificacao_user_Cleanfilter ;
      private String Ddo_contagemresultadonotificacao_user_Noresultsfound ;
      private String Ddo_contagemresultadonotificacao_user_Searchbuttontext ;
      private String Ddo_contagemresultadonotificacao_port_Caption ;
      private String Ddo_contagemresultadonotificacao_port_Tooltip ;
      private String Ddo_contagemresultadonotificacao_port_Cls ;
      private String Ddo_contagemresultadonotificacao_port_Filteredtext_set ;
      private String Ddo_contagemresultadonotificacao_port_Filteredtextto_set ;
      private String Ddo_contagemresultadonotificacao_port_Dropdownoptionstype ;
      private String Ddo_contagemresultadonotificacao_port_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadonotificacao_port_Sortedstatus ;
      private String Ddo_contagemresultadonotificacao_port_Filtertype ;
      private String Ddo_contagemresultadonotificacao_port_Sortasc ;
      private String Ddo_contagemresultadonotificacao_port_Sortdsc ;
      private String Ddo_contagemresultadonotificacao_port_Cleanfilter ;
      private String Ddo_contagemresultadonotificacao_port_Rangefilterfrom ;
      private String Ddo_contagemresultadonotificacao_port_Rangefilterto ;
      private String Ddo_contagemresultadonotificacao_port_Searchbuttontext ;
      private String Ddo_contagemresultadonotificacao_aut_Caption ;
      private String Ddo_contagemresultadonotificacao_aut_Tooltip ;
      private String Ddo_contagemresultadonotificacao_aut_Cls ;
      private String Ddo_contagemresultadonotificacao_aut_Selectedvalue_set ;
      private String Ddo_contagemresultadonotificacao_aut_Dropdownoptionstype ;
      private String Ddo_contagemresultadonotificacao_aut_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadonotificacao_aut_Sortedstatus ;
      private String Ddo_contagemresultadonotificacao_aut_Datalisttype ;
      private String Ddo_contagemresultadonotificacao_aut_Datalistfixedvalues ;
      private String Ddo_contagemresultadonotificacao_aut_Sortasc ;
      private String Ddo_contagemresultadonotificacao_aut_Sortdsc ;
      private String Ddo_contagemresultadonotificacao_aut_Cleanfilter ;
      private String Ddo_contagemresultadonotificacao_aut_Searchbuttontext ;
      private String Ddo_contagemresultadonotificacao_sec_Caption ;
      private String Ddo_contagemresultadonotificacao_sec_Tooltip ;
      private String Ddo_contagemresultadonotificacao_sec_Cls ;
      private String Ddo_contagemresultadonotificacao_sec_Selectedvalue_set ;
      private String Ddo_contagemresultadonotificacao_sec_Dropdownoptionstype ;
      private String Ddo_contagemresultadonotificacao_sec_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadonotificacao_sec_Sortedstatus ;
      private String Ddo_contagemresultadonotificacao_sec_Datalisttype ;
      private String Ddo_contagemresultadonotificacao_sec_Datalistfixedvalues ;
      private String Ddo_contagemresultadonotificacao_sec_Sortasc ;
      private String Ddo_contagemresultadonotificacao_sec_Sortdsc ;
      private String Ddo_contagemresultadonotificacao_sec_Cleanfilter ;
      private String Ddo_contagemresultadonotificacao_sec_Searchbuttontext ;
      private String Ddo_contagemresultadonotificacao_logged_Caption ;
      private String Ddo_contagemresultadonotificacao_logged_Tooltip ;
      private String Ddo_contagemresultadonotificacao_logged_Cls ;
      private String Ddo_contagemresultadonotificacao_logged_Selectedvalue_set ;
      private String Ddo_contagemresultadonotificacao_logged_Dropdownoptionstype ;
      private String Ddo_contagemresultadonotificacao_logged_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadonotificacao_logged_Sortedstatus ;
      private String Ddo_contagemresultadonotificacao_logged_Datalisttype ;
      private String Ddo_contagemresultadonotificacao_logged_Datalistfixedvalues ;
      private String Ddo_contagemresultadonotificacao_logged_Sortasc ;
      private String Ddo_contagemresultadonotificacao_logged_Sortdsc ;
      private String Ddo_contagemresultadonotificacao_logged_Cleanfilter ;
      private String Ddo_contagemresultadonotificacao_logged_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_datahora_Internalname ;
      private String edtavTfcontagemresultadonotificacao_datahora_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_datahora_to_Internalname ;
      private String edtavTfcontagemresultadonotificacao_datahora_to_Jsonclick ;
      private String divDdo_contagemresultadonotificacao_datahoraauxdates_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_datahoraauxdate_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_datahoraauxdate_Jsonclick ;
      private String edtavDdo_contagemresultadonotificacao_datahoraauxdateto_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_datahoraauxdateto_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_usunom_Internalname ;
      private String edtavTfcontagemresultadonotificacao_usunom_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_usunom_sel_Internalname ;
      private String edtavTfcontagemresultadonotificacao_usunom_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTfcontagemresultadonotificacao_assunto_Internalname ;
      private String edtavTfcontagemresultadonotificacao_assunto_sel_Internalname ;
      private String edtavTfcontagemresultadonotificacao_destinatarios_Internalname ;
      private String edtavTfcontagemresultadonotificacao_destinatarios_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_destinatarios_to_Internalname ;
      private String edtavTfcontagemresultadonotificacao_destinatarios_to_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_demandas_Internalname ;
      private String edtavTfcontagemresultadonotificacao_demandas_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_demandas_to_Internalname ;
      private String edtavTfcontagemresultadonotificacao_demandas_to_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_host_Internalname ;
      private String edtavTfcontagemresultadonotificacao_host_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_host_sel_Internalname ;
      private String edtavTfcontagemresultadonotificacao_host_sel_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_user_Internalname ;
      private String edtavTfcontagemresultadonotificacao_user_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_user_sel_Internalname ;
      private String edtavTfcontagemresultadonotificacao_user_sel_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_port_Internalname ;
      private String edtavTfcontagemresultadonotificacao_port_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_port_to_Internalname ;
      private String edtavTfcontagemresultadonotificacao_port_to_Jsonclick ;
      private String edtavTfcontagemresultadonotificacao_aut_sel_Internalname ;
      private String edtavTfcontagemresultadonotificacao_aut_sel_Jsonclick ;
      private String edtavDdo_contagemresultadonotificacao_datahoratitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_usunomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_assuntotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_destinatariostitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_demandastitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_hosttitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_usertitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_porttitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_auttitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_sectitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadonotificacao_loggedtitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultadoNotificacao_Codigo_Internalname ;
      private String edtContagemResultadoNotificacao_UsuCod_Internalname ;
      private String edtContagemResultadoNotificacao_DataHora_Internalname ;
      private String A1422ContagemResultadoNotificacao_UsuNom ;
      private String edtContagemResultadoNotificacao_UsuNom_Internalname ;
      private String edtContagemResultadoNotificacao_Assunto_Internalname ;
      private String edtContagemResultadoNotificacao_Destinatarios_Internalname ;
      private String edtContagemResultadoNotificacao_Demandas_Internalname ;
      private String A1956ContagemResultadoNotificacao_Host ;
      private String edtContagemResultadoNotificacao_Host_Internalname ;
      private String A1957ContagemResultadoNotificacao_User ;
      private String edtContagemResultadoNotificacao_User_Internalname ;
      private String edtContagemResultadoNotificacao_Port_Internalname ;
      private String cmbContagemResultadoNotificacao_Aut_Internalname ;
      private String cmbContagemResultadoNotificacao_Sec_Internalname ;
      private String cmbContagemResultadoNotificacao_Logged_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV23TFContagemResultadoNotificacao_UsuNom ;
      private String lV35TFContagemResultadoNotificacao_Host ;
      private String lV39TFContagemResultadoNotificacao_User ;
      private String subGrid_Internalname ;
      private String Ddo_contagemresultadonotificacao_datahora_Internalname ;
      private String Ddo_contagemresultadonotificacao_usunom_Internalname ;
      private String Ddo_contagemresultadonotificacao_assunto_Internalname ;
      private String Ddo_contagemresultadonotificacao_destinatarios_Internalname ;
      private String Ddo_contagemresultadonotificacao_demandas_Internalname ;
      private String Ddo_contagemresultadonotificacao_host_Internalname ;
      private String Ddo_contagemresultadonotificacao_user_Internalname ;
      private String Ddo_contagemresultadonotificacao_port_Internalname ;
      private String Ddo_contagemresultadonotificacao_aut_Internalname ;
      private String Ddo_contagemresultadonotificacao_sec_Internalname ;
      private String Ddo_contagemresultadonotificacao_logged_Internalname ;
      private String edtContagemResultadoNotificacao_DataHora_Title ;
      private String edtContagemResultadoNotificacao_UsuNom_Title ;
      private String edtContagemResultadoNotificacao_Assunto_Title ;
      private String edtContagemResultadoNotificacao_Destinatarios_Title ;
      private String edtContagemResultadoNotificacao_Demandas_Title ;
      private String edtContagemResultadoNotificacao_Host_Title ;
      private String edtContagemResultadoNotificacao_User_Title ;
      private String edtContagemResultadoNotificacao_Port_Title ;
      private String edtContagemResultadoNotificacao_DataHora_Linktarget ;
      private String edtContagemResultadoNotificacao_UsuNom_Link ;
      private String edtContagemResultadoNotificacao_DataHora_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7ContagemResultado_Codigo ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultadoNotificacao_Codigo_Jsonclick ;
      private String edtContagemResultadoNotificacao_UsuCod_Jsonclick ;
      private String edtContagemResultadoNotificacao_DataHora_Jsonclick ;
      private String edtContagemResultadoNotificacao_UsuNom_Jsonclick ;
      private String edtContagemResultadoNotificacao_Assunto_Jsonclick ;
      private String edtContagemResultadoNotificacao_Destinatarios_Jsonclick ;
      private String edtContagemResultadoNotificacao_Demandas_Jsonclick ;
      private String edtContagemResultadoNotificacao_Host_Jsonclick ;
      private String edtContagemResultadoNotificacao_User_Jsonclick ;
      private String edtContagemResultadoNotificacao_Port_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Aut_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Sec_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Logged_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV17TFContagemResultadoNotificacao_DataHora ;
      private DateTime AV18TFContagemResultadoNotificacao_DataHora_To ;
      private DateTime A1416ContagemResultadoNotificacao_DataHora ;
      private DateTime AV19DDO_ContagemResultadoNotificacao_DataHoraAuxDate ;
      private DateTime AV20DDO_ContagemResultadoNotificacao_DataHoraAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contagemresultadonotificacao_datahora_Includesortasc ;
      private bool Ddo_contagemresultadonotificacao_datahora_Includesortdsc ;
      private bool Ddo_contagemresultadonotificacao_datahora_Includefilter ;
      private bool Ddo_contagemresultadonotificacao_datahora_Filterisrange ;
      private bool Ddo_contagemresultadonotificacao_datahora_Includedatalist ;
      private bool Ddo_contagemresultadonotificacao_usunom_Includesortasc ;
      private bool Ddo_contagemresultadonotificacao_usunom_Includesortdsc ;
      private bool Ddo_contagemresultadonotificacao_usunom_Includefilter ;
      private bool Ddo_contagemresultadonotificacao_usunom_Filterisrange ;
      private bool Ddo_contagemresultadonotificacao_usunom_Includedatalist ;
      private bool Ddo_contagemresultadonotificacao_assunto_Includesortasc ;
      private bool Ddo_contagemresultadonotificacao_assunto_Includesortdsc ;
      private bool Ddo_contagemresultadonotificacao_assunto_Includefilter ;
      private bool Ddo_contagemresultadonotificacao_assunto_Filterisrange ;
      private bool Ddo_contagemresultadonotificacao_assunto_Includedatalist ;
      private bool Ddo_contagemresultadonotificacao_destinatarios_Includesortasc ;
      private bool Ddo_contagemresultadonotificacao_destinatarios_Includesortdsc ;
      private bool Ddo_contagemresultadonotificacao_destinatarios_Includefilter ;
      private bool Ddo_contagemresultadonotificacao_destinatarios_Filterisrange ;
      private bool Ddo_contagemresultadonotificacao_destinatarios_Includedatalist ;
      private bool Ddo_contagemresultadonotificacao_demandas_Includesortasc ;
      private bool Ddo_contagemresultadonotificacao_demandas_Includesortdsc ;
      private bool Ddo_contagemresultadonotificacao_demandas_Includefilter ;
      private bool Ddo_contagemresultadonotificacao_demandas_Filterisrange ;
      private bool Ddo_contagemresultadonotificacao_demandas_Includedatalist ;
      private bool Ddo_contagemresultadonotificacao_host_Includesortasc ;
      private bool Ddo_contagemresultadonotificacao_host_Includesortdsc ;
      private bool Ddo_contagemresultadonotificacao_host_Includefilter ;
      private bool Ddo_contagemresultadonotificacao_host_Filterisrange ;
      private bool Ddo_contagemresultadonotificacao_host_Includedatalist ;
      private bool Ddo_contagemresultadonotificacao_user_Includesortasc ;
      private bool Ddo_contagemresultadonotificacao_user_Includesortdsc ;
      private bool Ddo_contagemresultadonotificacao_user_Includefilter ;
      private bool Ddo_contagemresultadonotificacao_user_Filterisrange ;
      private bool Ddo_contagemresultadonotificacao_user_Includedatalist ;
      private bool Ddo_contagemresultadonotificacao_port_Includesortasc ;
      private bool Ddo_contagemresultadonotificacao_port_Includesortdsc ;
      private bool Ddo_contagemresultadonotificacao_port_Includefilter ;
      private bool Ddo_contagemresultadonotificacao_port_Filterisrange ;
      private bool Ddo_contagemresultadonotificacao_port_Includedatalist ;
      private bool Ddo_contagemresultadonotificacao_aut_Includesortasc ;
      private bool Ddo_contagemresultadonotificacao_aut_Includesortdsc ;
      private bool Ddo_contagemresultadonotificacao_aut_Includefilter ;
      private bool Ddo_contagemresultadonotificacao_aut_Includedatalist ;
      private bool Ddo_contagemresultadonotificacao_sec_Includesortasc ;
      private bool Ddo_contagemresultadonotificacao_sec_Includesortdsc ;
      private bool Ddo_contagemresultadonotificacao_sec_Includefilter ;
      private bool Ddo_contagemresultadonotificacao_sec_Includedatalist ;
      private bool Ddo_contagemresultadonotificacao_sec_Allowmultipleselection ;
      private bool Ddo_contagemresultadonotificacao_logged_Includesortasc ;
      private bool Ddo_contagemresultadonotificacao_logged_Includesortdsc ;
      private bool Ddo_contagemresultadonotificacao_logged_Includefilter ;
      private bool Ddo_contagemresultadonotificacao_logged_Includedatalist ;
      private bool Ddo_contagemresultadonotificacao_logged_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1416ContagemResultadoNotificacao_DataHora ;
      private bool n1422ContagemResultadoNotificacao_UsuNom ;
      private bool n1417ContagemResultadoNotificacao_Assunto ;
      private bool n1962ContagemResultadoNotificacao_Destinatarios ;
      private bool n1963ContagemResultadoNotificacao_Demandas ;
      private bool n1956ContagemResultadoNotificacao_Host ;
      private bool n1957ContagemResultadoNotificacao_User ;
      private bool n1958ContagemResultadoNotificacao_Port ;
      private bool A1959ContagemResultadoNotificacao_Aut ;
      private bool n1959ContagemResultadoNotificacao_Aut ;
      private bool n1960ContagemResultadoNotificacao_Sec ;
      private bool n1961ContagemResultadoNotificacao_Logged ;
      private bool n1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String A1417ContagemResultadoNotificacao_Assunto ;
      private String AV50TFContagemResultadoNotificacao_Sec_SelsJson ;
      private String AV54TFContagemResultadoNotificacao_Logged_SelsJson ;
      private String AV64TFContagemResultadoNotificacao_Assunto ;
      private String AV65TFContagemResultadoNotificacao_Assunto_Sel ;
      private String AV21ddo_ContagemResultadoNotificacao_DataHoraTitleControlIdToReplace ;
      private String AV25ddo_ContagemResultadoNotificacao_UsuNomTitleControlIdToReplace ;
      private String AV66ddo_ContagemResultadoNotificacao_AssuntoTitleControlIdToReplace ;
      private String AV29ddo_ContagemResultadoNotificacao_DestinatariosTitleControlIdToReplace ;
      private String AV33ddo_ContagemResultadoNotificacao_DemandasTitleControlIdToReplace ;
      private String AV37ddo_ContagemResultadoNotificacao_HostTitleControlIdToReplace ;
      private String AV41ddo_ContagemResultadoNotificacao_UserTitleControlIdToReplace ;
      private String AV45ddo_ContagemResultadoNotificacao_PortTitleControlIdToReplace ;
      private String AV48ddo_ContagemResultadoNotificacao_AutTitleControlIdToReplace ;
      private String AV52ddo_ContagemResultadoNotificacao_SecTitleControlIdToReplace ;
      private String AV56ddo_ContagemResultadoNotificacao_LoggedTitleControlIdToReplace ;
      private String lV64TFContagemResultadoNotificacao_Assunto ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContagemResultadoNotificacao_Aut ;
      private GXCombobox cmbContagemResultadoNotificacao_Sec ;
      private GXCombobox cmbContagemResultadoNotificacao_Logged ;
      private IDataStoreProvider pr_default ;
      private int[] H00QX4_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] H00QX4_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private short[] H00QX4_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] H00QX4_n1961ContagemResultadoNotificacao_Logged ;
      private short[] H00QX4_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] H00QX4_n1960ContagemResultadoNotificacao_Sec ;
      private bool[] H00QX4_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] H00QX4_n1959ContagemResultadoNotificacao_Aut ;
      private short[] H00QX4_A1958ContagemResultadoNotificacao_Port ;
      private bool[] H00QX4_n1958ContagemResultadoNotificacao_Port ;
      private String[] H00QX4_A1957ContagemResultadoNotificacao_User ;
      private bool[] H00QX4_n1957ContagemResultadoNotificacao_User ;
      private String[] H00QX4_A1956ContagemResultadoNotificacao_Host ;
      private bool[] H00QX4_n1956ContagemResultadoNotificacao_Host ;
      private String[] H00QX4_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] H00QX4_n1417ContagemResultadoNotificacao_Assunto ;
      private String[] H00QX4_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] H00QX4_n1422ContagemResultadoNotificacao_UsuNom ;
      private DateTime[] H00QX4_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] H00QX4_n1416ContagemResultadoNotificacao_DataHora ;
      private int[] H00QX4_A1413ContagemResultadoNotificacao_UsuCod ;
      private long[] H00QX4_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] H00QX4_A456ContagemResultado_Codigo ;
      private int[] H00QX4_A1963ContagemResultadoNotificacao_Demandas ;
      private bool[] H00QX4_n1963ContagemResultadoNotificacao_Demandas ;
      private int[] H00QX4_A1962ContagemResultadoNotificacao_Destinatarios ;
      private bool[] H00QX4_n1962ContagemResultadoNotificacao_Destinatarios ;
      private long[] H00QX7_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private IGxSession AV62WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV61Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV51TFContagemResultadoNotificacao_Sec_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV55TFContagemResultadoNotificacao_Logged_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV16ContagemResultadoNotificacao_DataHoraTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV22ContagemResultadoNotificacao_UsuNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV63ContagemResultadoNotificacao_AssuntoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV26ContagemResultadoNotificacao_DestinatariosTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30ContagemResultadoNotificacao_DemandasTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34ContagemResultadoNotificacao_HostTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38ContagemResultadoNotificacao_UserTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42ContagemResultadoNotificacao_PortTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46ContagemResultadoNotificacao_AutTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49ContagemResultadoNotificacao_SecTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV53ContagemResultadoNotificacao_LoggedTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV57DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class demandanotificacoeswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00QX4( IGxContext context ,
                                             short A1960ContagemResultadoNotificacao_Sec ,
                                             IGxCollection AV51TFContagemResultadoNotificacao_Sec_Sels ,
                                             short A1961ContagemResultadoNotificacao_Logged ,
                                             IGxCollection AV55TFContagemResultadoNotificacao_Logged_Sels ,
                                             DateTime AV17TFContagemResultadoNotificacao_DataHora ,
                                             DateTime AV18TFContagemResultadoNotificacao_DataHora_To ,
                                             String AV24TFContagemResultadoNotificacao_UsuNom_Sel ,
                                             String AV23TFContagemResultadoNotificacao_UsuNom ,
                                             String AV65TFContagemResultadoNotificacao_Assunto_Sel ,
                                             String AV64TFContagemResultadoNotificacao_Assunto ,
                                             String AV36TFContagemResultadoNotificacao_Host_Sel ,
                                             String AV35TFContagemResultadoNotificacao_Host ,
                                             String AV40TFContagemResultadoNotificacao_User_Sel ,
                                             String AV39TFContagemResultadoNotificacao_User ,
                                             short AV43TFContagemResultadoNotificacao_Port ,
                                             short AV44TFContagemResultadoNotificacao_Port_To ,
                                             short AV47TFContagemResultadoNotificacao_Aut_Sel ,
                                             int AV51TFContagemResultadoNotificacao_Sec_Sels_Count ,
                                             int AV55TFContagemResultadoNotificacao_Logged_Sels_Count ,
                                             DateTime A1416ContagemResultadoNotificacao_DataHora ,
                                             String A1422ContagemResultadoNotificacao_UsuNom ,
                                             String A1417ContagemResultadoNotificacao_Assunto ,
                                             String A1956ContagemResultadoNotificacao_Host ,
                                             String A1957ContagemResultadoNotificacao_User ,
                                             short A1958ContagemResultadoNotificacao_Port ,
                                             bool A1959ContagemResultadoNotificacao_Aut ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A456ContagemResultado_Codigo ,
                                             int AV7ContagemResultado_Codigo ,
                                             int AV27TFContagemResultadoNotificacao_Destinatarios ,
                                             int A1962ContagemResultadoNotificacao_Destinatarios ,
                                             int AV28TFContagemResultadoNotificacao_Destinatarios_To ,
                                             int AV31TFContagemResultadoNotificacao_Demandas ,
                                             int A1963ContagemResultadoNotificacao_Demandas ,
                                             int AV32TFContagemResultadoNotificacao_Demandas_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [26] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T3.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, T2.[ContagemResultadoNotificacao_Logged], T2.[ContagemResultadoNotificacao_Sec], T2.[ContagemResultadoNotificacao_Aut], T2.[ContagemResultadoNotificacao_Port], T2.[ContagemResultadoNotificacao_User], T2.[ContagemResultadoNotificacao_Host], T2.[ContagemResultadoNotificacao_Assunto], T4.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, T2.[ContagemResultadoNotificacao_DataHora], T2.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultado_Codigo], COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) AS ContagemResultadoNotificacao_Demandas, COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) AS ContagemResultadoNotificacao_Destinatarios";
         sFromString = " FROM ((((([ContagemResultadoNotificacaoDemanda] T1 WITH (NOLOCK) INNER JOIN [ContagemResultadoNotificacao] T2 WITH (NOLOCK) ON T2.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T2.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T5 ON T5.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T6 ON T6.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContagemResultado_Codigo] = @AV7ContagemResultado_Codigo)";
         sWhereString = sWhereString + " and ((@AV27TFContagemResultadoNotificacao_Destinatarios = convert(int, 0)) or ( COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) >= @AV27TFContagemResultadoNotificacao_Destinatarios))";
         sWhereString = sWhereString + " and ((@AV28TFContagemResultadoNotificacao_Destinatarios_To = convert(int, 0)) or ( COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) <= @AV28TFContagemResultadoNotificacao_Destinatarios_To))";
         sWhereString = sWhereString + " and ((@AV31TFContagemResultadoNotificacao_Demandas = convert(int, 0)) or ( COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) >= @AV31TFContagemResultadoNotificacao_Demandas))";
         sWhereString = sWhereString + " and ((@AV32TFContagemResultadoNotificacao_Demandas_To = convert(int, 0)) or ( COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) <= @AV32TFContagemResultadoNotificacao_Demandas_To))";
         if ( ! (DateTime.MinValue==AV17TFContagemResultadoNotificacao_DataHora) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_DataHora] >= @AV17TFContagemResultadoNotificacao_DataHora)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFContagemResultadoNotificacao_DataHora_To) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_DataHora] <= @AV18TFContagemResultadoNotificacao_DataHora_To)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContagemResultadoNotificacao_UsuNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContagemResultadoNotificacao_UsuNom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV23TFContagemResultadoNotificacao_UsuNom)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContagemResultadoNotificacao_UsuNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV24TFContagemResultadoNotificacao_UsuNom_Sel)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContagemResultadoNotificacao_Assunto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFContagemResultadoNotificacao_Assunto)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Assunto] like @lV64TFContagemResultadoNotificacao_Assunto)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContagemResultadoNotificacao_Assunto_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Assunto] = @AV65TFContagemResultadoNotificacao_Assunto_Sel)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContagemResultadoNotificacao_Host_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContagemResultadoNotificacao_Host)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Host] like @lV35TFContagemResultadoNotificacao_Host)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContagemResultadoNotificacao_Host_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Host] = @AV36TFContagemResultadoNotificacao_Host_Sel)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContagemResultadoNotificacao_User_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFContagemResultadoNotificacao_User)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_User] like @lV39TFContagemResultadoNotificacao_User)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContagemResultadoNotificacao_User_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_User] = @AV40TFContagemResultadoNotificacao_User_Sel)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (0==AV43TFContagemResultadoNotificacao_Port) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Port] >= @AV43TFContagemResultadoNotificacao_Port)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (0==AV44TFContagemResultadoNotificacao_Port_To) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Port] <= @AV44TFContagemResultadoNotificacao_Port_To)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( AV47TFContagemResultadoNotificacao_Aut_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Aut] = 1)";
         }
         if ( AV47TFContagemResultadoNotificacao_Aut_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Aut] = 0)";
         }
         if ( AV51TFContagemResultadoNotificacao_Sec_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV51TFContagemResultadoNotificacao_Sec_Sels, "T2.[ContagemResultadoNotificacao_Sec] IN (", ")") + ")";
         }
         if ( AV55TFContagemResultadoNotificacao_Logged_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV55TFContagemResultadoNotificacao_Logged_Sels, "T2.[ContagemResultadoNotificacao_Logged] IN (", ")") + ")";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_DataHora]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T2.[ContagemResultadoNotificacao_DataHora] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T4.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_Assunto]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T2.[ContagemResultadoNotificacao_Assunto] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_Host]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T2.[ContagemResultadoNotificacao_Host] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_User]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T2.[ContagemResultadoNotificacao_User] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_Port]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T2.[ContagemResultadoNotificacao_Port] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_Aut]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T2.[ContagemResultadoNotificacao_Aut] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_Sec]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T2.[ContagemResultadoNotificacao_Sec] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo], T2.[ContagemResultadoNotificacao_Logged]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T2.[ContagemResultadoNotificacao_Logged] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultado_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00QX7( IGxContext context ,
                                             short A1960ContagemResultadoNotificacao_Sec ,
                                             IGxCollection AV51TFContagemResultadoNotificacao_Sec_Sels ,
                                             short A1961ContagemResultadoNotificacao_Logged ,
                                             IGxCollection AV55TFContagemResultadoNotificacao_Logged_Sels ,
                                             DateTime AV17TFContagemResultadoNotificacao_DataHora ,
                                             DateTime AV18TFContagemResultadoNotificacao_DataHora_To ,
                                             String AV24TFContagemResultadoNotificacao_UsuNom_Sel ,
                                             String AV23TFContagemResultadoNotificacao_UsuNom ,
                                             String AV65TFContagemResultadoNotificacao_Assunto_Sel ,
                                             String AV64TFContagemResultadoNotificacao_Assunto ,
                                             String AV36TFContagemResultadoNotificacao_Host_Sel ,
                                             String AV35TFContagemResultadoNotificacao_Host ,
                                             String AV40TFContagemResultadoNotificacao_User_Sel ,
                                             String AV39TFContagemResultadoNotificacao_User ,
                                             short AV43TFContagemResultadoNotificacao_Port ,
                                             short AV44TFContagemResultadoNotificacao_Port_To ,
                                             short AV47TFContagemResultadoNotificacao_Aut_Sel ,
                                             int AV51TFContagemResultadoNotificacao_Sec_Sels_Count ,
                                             int AV55TFContagemResultadoNotificacao_Logged_Sels_Count ,
                                             DateTime A1416ContagemResultadoNotificacao_DataHora ,
                                             String A1422ContagemResultadoNotificacao_UsuNom ,
                                             String A1417ContagemResultadoNotificacao_Assunto ,
                                             String A1956ContagemResultadoNotificacao_Host ,
                                             String A1957ContagemResultadoNotificacao_User ,
                                             short A1958ContagemResultadoNotificacao_Port ,
                                             bool A1959ContagemResultadoNotificacao_Aut ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A456ContagemResultado_Codigo ,
                                             int AV7ContagemResultado_Codigo ,
                                             int AV27TFContagemResultadoNotificacao_Destinatarios ,
                                             int A1962ContagemResultadoNotificacao_Destinatarios ,
                                             int AV28TFContagemResultadoNotificacao_Destinatarios_To ,
                                             int AV31TFContagemResultadoNotificacao_Demandas ,
                                             int A1963ContagemResultadoNotificacao_Demandas ,
                                             int AV32TFContagemResultadoNotificacao_Demandas_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [21] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((((([ContagemResultadoNotificacaoDemanda] T1 WITH (NOLOCK) INNER JOIN [ContagemResultadoNotificacao] T2 WITH (NOLOCK) ON T2.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T2.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Demandas, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T5 ON T5.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoNotificacao_Destinatarios, [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacaoDestinatario] WITH (NOLOCK) GROUP BY [ContagemResultadoNotificacao_Codigo] ) T6 ON T6.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_Codigo] = @AV7ContagemResultado_Codigo)";
         scmdbuf = scmdbuf + " and ((@AV27TFContagemResultadoNotificacao_Destinatarios = convert(int, 0)) or ( COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) >= @AV27TFContagemResultadoNotificacao_Destinatarios))";
         scmdbuf = scmdbuf + " and ((@AV28TFContagemResultadoNotificacao_Destinatarios_To = convert(int, 0)) or ( COALESCE( T6.[ContagemResultadoNotificacao_Destinatarios], 0) <= @AV28TFContagemResultadoNotificacao_Destinatarios_To))";
         scmdbuf = scmdbuf + " and ((@AV31TFContagemResultadoNotificacao_Demandas = convert(int, 0)) or ( COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) >= @AV31TFContagemResultadoNotificacao_Demandas))";
         scmdbuf = scmdbuf + " and ((@AV32TFContagemResultadoNotificacao_Demandas_To = convert(int, 0)) or ( COALESCE( T5.[ContagemResultadoNotificacao_Demandas], 0) <= @AV32TFContagemResultadoNotificacao_Demandas_To))";
         if ( ! (DateTime.MinValue==AV17TFContagemResultadoNotificacao_DataHora) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_DataHora] >= @AV17TFContagemResultadoNotificacao_DataHora)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFContagemResultadoNotificacao_DataHora_To) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_DataHora] <= @AV18TFContagemResultadoNotificacao_DataHora_To)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContagemResultadoNotificacao_UsuNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContagemResultadoNotificacao_UsuNom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV23TFContagemResultadoNotificacao_UsuNom)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContagemResultadoNotificacao_UsuNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV24TFContagemResultadoNotificacao_UsuNom_Sel)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContagemResultadoNotificacao_Assunto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFContagemResultadoNotificacao_Assunto)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Assunto] like @lV64TFContagemResultadoNotificacao_Assunto)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContagemResultadoNotificacao_Assunto_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Assunto] = @AV65TFContagemResultadoNotificacao_Assunto_Sel)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContagemResultadoNotificacao_Host_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContagemResultadoNotificacao_Host)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Host] like @lV35TFContagemResultadoNotificacao_Host)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContagemResultadoNotificacao_Host_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Host] = @AV36TFContagemResultadoNotificacao_Host_Sel)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContagemResultadoNotificacao_User_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFContagemResultadoNotificacao_User)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_User] like @lV39TFContagemResultadoNotificacao_User)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContagemResultadoNotificacao_User_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_User] = @AV40TFContagemResultadoNotificacao_User_Sel)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (0==AV43TFContagemResultadoNotificacao_Port) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Port] >= @AV43TFContagemResultadoNotificacao_Port)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! (0==AV44TFContagemResultadoNotificacao_Port_To) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Port] <= @AV44TFContagemResultadoNotificacao_Port_To)";
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( AV47TFContagemResultadoNotificacao_Aut_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Aut] = 1)";
         }
         if ( AV47TFContagemResultadoNotificacao_Aut_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultadoNotificacao_Aut] = 0)";
         }
         if ( AV51TFContagemResultadoNotificacao_Sec_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV51TFContagemResultadoNotificacao_Sec_Sels, "T2.[ContagemResultadoNotificacao_Sec] IN (", ")") + ")";
         }
         if ( AV55TFContagemResultadoNotificacao_Logged_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV55TFContagemResultadoNotificacao_Logged_Sels, "T2.[ContagemResultadoNotificacao_Logged] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00QX4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] );
               case 1 :
                     return conditional_H00QX7(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00QX4 ;
          prmH00QX4 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27TFContagemResultadoNotificacao_Destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27TFContagemResultadoNotificacao_Destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28TFContagemResultadoNotificacao_Destinatarios_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28TFContagemResultadoNotificacao_Destinatarios_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV31TFContagemResultadoNotificacao_Demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV31TFContagemResultadoNotificacao_Demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFContagemResultadoNotificacao_Demandas_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFContagemResultadoNotificacao_Demandas_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContagemResultadoNotificacao_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18TFContagemResultadoNotificacao_DataHora_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV23TFContagemResultadoNotificacao_UsuNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV24TFContagemResultadoNotificacao_UsuNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV64TFContagemResultadoNotificacao_Assunto",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV65TFContagemResultadoNotificacao_Assunto_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV35TFContagemResultadoNotificacao_Host",SqlDbType.Char,50,0} ,
          new Object[] {"@AV36TFContagemResultadoNotificacao_Host_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39TFContagemResultadoNotificacao_User",SqlDbType.Char,50,0} ,
          new Object[] {"@AV40TFContagemResultadoNotificacao_User_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV43TFContagemResultadoNotificacao_Port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV44TFContagemResultadoNotificacao_Port_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00QX7 ;
          prmH00QX7 = new Object[] {
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27TFContagemResultadoNotificacao_Destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27TFContagemResultadoNotificacao_Destinatarios",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28TFContagemResultadoNotificacao_Destinatarios_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28TFContagemResultadoNotificacao_Destinatarios_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV31TFContagemResultadoNotificacao_Demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV31TFContagemResultadoNotificacao_Demandas",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFContagemResultadoNotificacao_Demandas_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFContagemResultadoNotificacao_Demandas_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContagemResultadoNotificacao_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18TFContagemResultadoNotificacao_DataHora_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV23TFContagemResultadoNotificacao_UsuNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV24TFContagemResultadoNotificacao_UsuNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV64TFContagemResultadoNotificacao_Assunto",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV65TFContagemResultadoNotificacao_Assunto_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV35TFContagemResultadoNotificacao_Host",SqlDbType.Char,50,0} ,
          new Object[] {"@AV36TFContagemResultadoNotificacao_Host_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39TFContagemResultadoNotificacao_User",SqlDbType.Char,50,0} ,
          new Object[] {"@AV40TFContagemResultadoNotificacao_User_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV43TFContagemResultadoNotificacao_Port",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV44TFContagemResultadoNotificacao_Port_To",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00QX4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QX4,11,0,true,false )
             ,new CursorDef("H00QX7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QX7,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((long[]) buf[21])[0] = rslt.getLong(12) ;
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                return;
       }
    }

 }

}
