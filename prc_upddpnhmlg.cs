/*
               File: PRC_UpdDpnHmlg
        Description: Update dependencias para homologar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:52.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_upddpnhmlg : GXProcedure
   {
      public prc_upddpnhmlg( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_upddpnhmlg( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public int executeUdp( )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         return A456ContagemResultado_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo )
      {
         prc_upddpnhmlg objprc_upddpnhmlg;
         objprc_upddpnhmlg = new prc_upddpnhmlg();
         objprc_upddpnhmlg.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_upddpnhmlg.context.SetSubmitInitialConfig(context);
         objprc_upddpnhmlg.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_upddpnhmlg);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_upddpnhmlg)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AL2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1457ContagemResultado_TemDpnHmlg = P00AL2_A1457ContagemResultado_TemDpnHmlg[0];
            n1457ContagemResultado_TemDpnHmlg = P00AL2_n1457ContagemResultado_TemDpnHmlg[0];
            GXt_boolean1 = A1457ContagemResultado_TemDpnHmlg;
            new prc_temdpnhmlg(context ).execute( ref  A456ContagemResultado_Codigo, out  GXt_boolean1) ;
            A1457ContagemResultado_TemDpnHmlg = GXt_boolean1;
            n1457ContagemResultado_TemDpnHmlg = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00AL3 */
            pr_default.execute(1, new Object[] {n1457ContagemResultado_TemDpnHmlg, A1457ContagemResultado_TemDpnHmlg, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00AL4 */
            pr_default.execute(2, new Object[] {n1457ContagemResultado_TemDpnHmlg, A1457ContagemResultado_TemDpnHmlg, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AL2_A456ContagemResultado_Codigo = new int[1] ;
         P00AL2_A1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P00AL2_n1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_upddpnhmlg__default(),
            new Object[][] {
                new Object[] {
               P00AL2_A456ContagemResultado_Codigo, P00AL2_A1457ContagemResultado_TemDpnHmlg, P00AL2_n1457ContagemResultado_TemDpnHmlg
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private bool A1457ContagemResultado_TemDpnHmlg ;
      private bool n1457ContagemResultado_TemDpnHmlg ;
      private bool GXt_boolean1 ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00AL2_A456ContagemResultado_Codigo ;
      private bool[] P00AL2_A1457ContagemResultado_TemDpnHmlg ;
      private bool[] P00AL2_n1457ContagemResultado_TemDpnHmlg ;
   }

   public class prc_upddpnhmlg__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AL2 ;
          prmP00AL2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AL3 ;
          prmP00AL3 = new Object[] {
          new Object[] {"@ContagemResultado_TemDpnHmlg",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AL4 ;
          prmP00AL4 = new Object[] {
          new Object[] {"@ContagemResultado_TemDpnHmlg",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AL2", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_TemDpnHmlg] FROM [ContagemResultado] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_TemDpnHmlg] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AL2,1,0,true,true )
             ,new CursorDef("P00AL3", "UPDATE [ContagemResultado] SET [ContagemResultado_TemDpnHmlg]=@ContagemResultado_TemDpnHmlg  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AL3)
             ,new CursorDef("P00AL4", "UPDATE [ContagemResultado] SET [ContagemResultado_TemDpnHmlg]=@ContagemResultado_TemDpnHmlg  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AL4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
