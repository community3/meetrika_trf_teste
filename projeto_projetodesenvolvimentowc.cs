/*
               File: Projeto_ProjetoDesenvolvimentoWC
        Description: Projeto_Projeto Desenvolvimento WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:8.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class projeto_projetodesenvolvimentowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public projeto_projetodesenvolvimentowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public projeto_projetodesenvolvimentowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ProjetoDesenvolvimento_ProjetoCod )
      {
         this.AV18ProjetoDesenvolvimento_ProjetoCod = aP0_ProjetoDesenvolvimento_ProjetoCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavDynamicfiltersselector1 = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV18ProjetoDesenvolvimento_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV18ProjetoDesenvolvimento_ProjetoCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_30 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_30_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_30_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV15DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
                  AV16ProjetoDesenvolvimento_SistemaNom1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16ProjetoDesenvolvimento_SistemaNom1", AV16ProjetoDesenvolvimento_SistemaNom1);
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV20TFProjetoDesenvolvimento_SistemaNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFProjetoDesenvolvimento_SistemaNom", AV20TFProjetoDesenvolvimento_SistemaNom);
                  AV21TFProjetoDesenvolvimento_SistemaNom_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFProjetoDesenvolvimento_SistemaNom_Sel", AV21TFProjetoDesenvolvimento_SistemaNom_Sel);
                  AV18ProjetoDesenvolvimento_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
                  AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace", AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace);
                  AV29Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ProjetoDesenvolvimento_SistemaNom1, AV14OrderedDsc, AV20TFProjetoDesenvolvimento_SistemaNom, AV21TFProjetoDesenvolvimento_SistemaNom_Sel, AV18ProjetoDesenvolvimento_ProjetoCod, AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace, AV29Pgmname, AV11GridState, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PADQ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV29Pgmname = "Projeto_ProjetoDesenvolvimentoWC";
               context.Gx_err = 0;
               WSDQ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Projeto_Projeto Desenvolvimento WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282250869");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("projeto_projetodesenvolvimentowc.aspx") + "?" + UrlEncode("" +AV18ProjetoDesenvolvimento_ProjetoCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vPROJETODESENVOLVIMENTO_SISTEMANOM1", AV16ProjetoDesenvolvimento_SistemaNom1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFPROJETODESENVOLVIMENTO_SISTEMANOM", AV20TFProjetoDesenvolvimento_SistemaNom);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFPROJETODESENVOLVIMENTO_SISTEMANOM_SEL", AV21TFProjetoDesenvolvimento_SistemaNom_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_30", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_30), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV23DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV23DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vPROJETODESENVOLVIMENTO_SISTEMANOMTITLEFILTERDATA", AV19ProjetoDesenvolvimento_SistemaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vPROJETODESENVOLVIMENTO_SISTEMANOMTITLEFILTERDATA", AV19ProjetoDesenvolvimento_SistemaNomTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV18ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV18ProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPROJETODESENVOLVIMENTO_PROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18ProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV29Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Caption", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Tooltip", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Cls", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Filteredtext_set", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Includesortasc", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_sistemanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_sistemanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Sortedstatus", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Includefilter", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_sistemanom_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Filtertype", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Filterisrange", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_sistemanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Includedatalist", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_sistemanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Datalisttype", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Datalistfixedvalues", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Datalistproc", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_projetodesenvolvimento_sistemanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Sortasc", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Sortdsc", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Loadingdata", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Cleanfilter", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Rangefilterfrom", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Rangefilterto", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Noresultsfound", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Searchbuttontext", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Activeeventkey", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Filteredtext_get", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemanom_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormDQ2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("projeto_projetodesenvolvimentowc.js", "?20204282250937");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "Projeto_ProjetoDesenvolvimentoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Projeto_Projeto Desenvolvimento WC" ;
      }

      protected void WBDQ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "projeto_projetodesenvolvimentowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_DQ2( true) ;
         }
         else
         {
            wb_table1_2_DQ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_Projeto_ProjetoDesenvolvimentoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojetodesenvolvimento_sistemanom_Internalname, AV20TFProjetoDesenvolvimento_SistemaNom, StringUtil.RTrim( context.localUtil.Format( AV20TFProjetoDesenvolvimento_SistemaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojetodesenvolvimento_sistemanom_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojetodesenvolvimento_sistemanom_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Projeto_ProjetoDesenvolvimentoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojetodesenvolvimento_sistemanom_sel_Internalname, AV21TFProjetoDesenvolvimento_SistemaNom_Sel, StringUtil.RTrim( context.localUtil.Format( AV21TFProjetoDesenvolvimento_SistemaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojetodesenvolvimento_sistemanom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojetodesenvolvimento_sistemanom_sel_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Projeto_ProjetoDesenvolvimentoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_30_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_projetodesenvolvimento_sistemanomtitlecontrolidtoreplace_Internalname, AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, edtavDdo_projetodesenvolvimento_sistemanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Projeto_ProjetoDesenvolvimentoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTDQ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Projeto_Projeto Desenvolvimento WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPDQ0( ) ;
            }
         }
      }

      protected void WSDQ2( )
      {
         STARTDQ2( ) ;
         EVTDQ2( ) ;
      }

      protected void EVTDQ2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11DQ2 */
                                    E11DQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PROJETODESENVOLVIMENTO_SISTEMANOM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12DQ2 */
                                    E12DQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13DQ2 */
                                    E13DQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDQ0( ) ;
                              }
                              nGXsfl_30_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
                              SubsflControlProps_302( ) ;
                              A669ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( edtProjetoDesenvolvimento_ProjetoCod_Internalname), ",", "."));
                              A670ProjetoDesenvolvimento_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtProjetoDesenvolvimento_SistemaCod_Internalname), ",", "."));
                              A671ProjetoDesenvolvimento_SistemaNom = StringUtil.Upper( cgiGet( edtProjetoDesenvolvimento_SistemaNom_Internalname));
                              n671ProjetoDesenvolvimento_SistemaNom = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14DQ2 */
                                          E14DQ2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15DQ2 */
                                          E15DQ2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16DQ2 */
                                          E16DQ2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Projetodesenvolvimento_sistemanom1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vPROJETODESENVOLVIMENTO_SISTEMANOM1"), AV16ProjetoDesenvolvimento_SistemaNom1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfprojetodesenvolvimento_sistemanom Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFPROJETODESENVOLVIMENTO_SISTEMANOM"), AV20TFProjetoDesenvolvimento_SistemaNom) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfprojetodesenvolvimento_sistemanom_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFPROJETODESENVOLVIMENTO_SISTEMANOM_SEL"), AV21TFProjetoDesenvolvimento_SistemaNom_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPDQ0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDQ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormDQ2( ) ;
            }
         }
      }

      protected void PADQ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("PROJETODESENVOLVIMENTO_SISTEMANOM", "Sistema", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_302( ) ;
         while ( nGXsfl_30_idx <= nRC_GXsfl_30 )
         {
            sendrow_302( ) ;
            nGXsfl_30_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_30_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_30_idx+1));
            sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
            SubsflControlProps_302( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV16ProjetoDesenvolvimento_SistemaNom1 ,
                                       bool AV14OrderedDsc ,
                                       String AV20TFProjetoDesenvolvimento_SistemaNom ,
                                       String AV21TFProjetoDesenvolvimento_SistemaNom_Sel ,
                                       int AV18ProjetoDesenvolvimento_ProjetoCod ,
                                       String AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace ,
                                       String AV29Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFDQ2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETODESENVOLVIMENTO_PROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"PROJETODESENVOLVIMENTO_PROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETODESENVOLVIMENTO_SISTEMACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"PROJETODESENVOLVIMENTO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDQ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV29Pgmname = "Projeto_ProjetoDesenvolvimentoWC";
         context.Gx_err = 0;
      }

      protected void RFDQ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 30;
         /* Execute user event: E15DQ2 */
         E15DQ2 ();
         nGXsfl_30_idx = 1;
         sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
         SubsflControlProps_302( ) ;
         nGXsfl_30_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_302( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16ProjetoDesenvolvimento_SistemaNom1 ,
                                                 AV21TFProjetoDesenvolvimento_SistemaNom_Sel ,
                                                 AV20TFProjetoDesenvolvimento_SistemaNom ,
                                                 A671ProjetoDesenvolvimento_SistemaNom ,
                                                 AV14OrderedDsc ,
                                                 A669ProjetoDesenvolvimento_ProjetoCod ,
                                                 AV18ProjetoDesenvolvimento_ProjetoCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV16ProjetoDesenvolvimento_SistemaNom1 = StringUtil.Concat( StringUtil.RTrim( AV16ProjetoDesenvolvimento_SistemaNom1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16ProjetoDesenvolvimento_SistemaNom1", AV16ProjetoDesenvolvimento_SistemaNom1);
            lV20TFProjetoDesenvolvimento_SistemaNom = StringUtil.Concat( StringUtil.RTrim( AV20TFProjetoDesenvolvimento_SistemaNom), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFProjetoDesenvolvimento_SistemaNom", AV20TFProjetoDesenvolvimento_SistemaNom);
            /* Using cursor H00DQ2 */
            pr_default.execute(0, new Object[] {AV18ProjetoDesenvolvimento_ProjetoCod, lV16ProjetoDesenvolvimento_SistemaNom1, lV20TFProjetoDesenvolvimento_SistemaNom, AV21TFProjetoDesenvolvimento_SistemaNom_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_30_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A671ProjetoDesenvolvimento_SistemaNom = H00DQ2_A671ProjetoDesenvolvimento_SistemaNom[0];
               n671ProjetoDesenvolvimento_SistemaNom = H00DQ2_n671ProjetoDesenvolvimento_SistemaNom[0];
               A670ProjetoDesenvolvimento_SistemaCod = H00DQ2_A670ProjetoDesenvolvimento_SistemaCod[0];
               A669ProjetoDesenvolvimento_ProjetoCod = H00DQ2_A669ProjetoDesenvolvimento_ProjetoCod[0];
               A671ProjetoDesenvolvimento_SistemaNom = H00DQ2_A671ProjetoDesenvolvimento_SistemaNom[0];
               n671ProjetoDesenvolvimento_SistemaNom = H00DQ2_n671ProjetoDesenvolvimento_SistemaNom[0];
               /* Execute user event: E16DQ2 */
               E16DQ2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 30;
            WBDQ0( ) ;
         }
         nGXsfl_30_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16ProjetoDesenvolvimento_SistemaNom1 ,
                                              AV21TFProjetoDesenvolvimento_SistemaNom_Sel ,
                                              AV20TFProjetoDesenvolvimento_SistemaNom ,
                                              A671ProjetoDesenvolvimento_SistemaNom ,
                                              AV14OrderedDsc ,
                                              A669ProjetoDesenvolvimento_ProjetoCod ,
                                              AV18ProjetoDesenvolvimento_ProjetoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV16ProjetoDesenvolvimento_SistemaNom1 = StringUtil.Concat( StringUtil.RTrim( AV16ProjetoDesenvolvimento_SistemaNom1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16ProjetoDesenvolvimento_SistemaNom1", AV16ProjetoDesenvolvimento_SistemaNom1);
         lV20TFProjetoDesenvolvimento_SistemaNom = StringUtil.Concat( StringUtil.RTrim( AV20TFProjetoDesenvolvimento_SistemaNom), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFProjetoDesenvolvimento_SistemaNom", AV20TFProjetoDesenvolvimento_SistemaNom);
         /* Using cursor H00DQ3 */
         pr_default.execute(1, new Object[] {AV18ProjetoDesenvolvimento_ProjetoCod, lV16ProjetoDesenvolvimento_SistemaNom1, lV20TFProjetoDesenvolvimento_SistemaNom, AV21TFProjetoDesenvolvimento_SistemaNom_Sel});
         GRID_nRecordCount = H00DQ3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ProjetoDesenvolvimento_SistemaNom1, AV14OrderedDsc, AV20TFProjetoDesenvolvimento_SistemaNom, AV21TFProjetoDesenvolvimento_SistemaNom_Sel, AV18ProjetoDesenvolvimento_ProjetoCod, AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace, AV29Pgmname, AV11GridState, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ProjetoDesenvolvimento_SistemaNom1, AV14OrderedDsc, AV20TFProjetoDesenvolvimento_SistemaNom, AV21TFProjetoDesenvolvimento_SistemaNom_Sel, AV18ProjetoDesenvolvimento_ProjetoCod, AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace, AV29Pgmname, AV11GridState, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ProjetoDesenvolvimento_SistemaNom1, AV14OrderedDsc, AV20TFProjetoDesenvolvimento_SistemaNom, AV21TFProjetoDesenvolvimento_SistemaNom_Sel, AV18ProjetoDesenvolvimento_ProjetoCod, AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace, AV29Pgmname, AV11GridState, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ProjetoDesenvolvimento_SistemaNom1, AV14OrderedDsc, AV20TFProjetoDesenvolvimento_SistemaNom, AV21TFProjetoDesenvolvimento_SistemaNom_Sel, AV18ProjetoDesenvolvimento_ProjetoCod, AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace, AV29Pgmname, AV11GridState, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ProjetoDesenvolvimento_SistemaNom1, AV14OrderedDsc, AV20TFProjetoDesenvolvimento_SistemaNom, AV21TFProjetoDesenvolvimento_SistemaNom_Sel, AV18ProjetoDesenvolvimento_ProjetoCod, AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace, AV29Pgmname, AV11GridState, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPDQ0( )
      {
         /* Before Start, stand alone formulas. */
         AV29Pgmname = "Projeto_ProjetoDesenvolvimentoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E14DQ2 */
         E14DQ2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV23DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vPROJETODESENVOLVIMENTO_SISTEMANOMTITLEFILTERDATA"), AV19ProjetoDesenvolvimento_SistemaNomTitleFilterData);
            /* Read variables values. */
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV16ProjetoDesenvolvimento_SistemaNom1 = StringUtil.Upper( cgiGet( edtavProjetodesenvolvimento_sistemanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16ProjetoDesenvolvimento_SistemaNom1", AV16ProjetoDesenvolvimento_SistemaNom1);
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            AV20TFProjetoDesenvolvimento_SistemaNom = StringUtil.Upper( cgiGet( edtavTfprojetodesenvolvimento_sistemanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFProjetoDesenvolvimento_SistemaNom", AV20TFProjetoDesenvolvimento_SistemaNom);
            AV21TFProjetoDesenvolvimento_SistemaNom_Sel = StringUtil.Upper( cgiGet( edtavTfprojetodesenvolvimento_sistemanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFProjetoDesenvolvimento_SistemaNom_Sel", AV21TFProjetoDesenvolvimento_SistemaNom_Sel);
            AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace = cgiGet( edtavDdo_projetodesenvolvimento_sistemanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace", AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_30 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_30"), ",", "."));
            AV25GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV26GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV18ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV18ProjetoDesenvolvimento_ProjetoCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_projetodesenvolvimento_sistemanom_Caption = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Caption");
            Ddo_projetodesenvolvimento_sistemanom_Tooltip = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Tooltip");
            Ddo_projetodesenvolvimento_sistemanom_Cls = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Cls");
            Ddo_projetodesenvolvimento_sistemanom_Filteredtext_set = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Filteredtext_set");
            Ddo_projetodesenvolvimento_sistemanom_Selectedvalue_set = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Selectedvalue_set");
            Ddo_projetodesenvolvimento_sistemanom_Dropdownoptionstype = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Dropdownoptionstype");
            Ddo_projetodesenvolvimento_sistemanom_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Titlecontrolidtoreplace");
            Ddo_projetodesenvolvimento_sistemanom_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Includesortasc"));
            Ddo_projetodesenvolvimento_sistemanom_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Includesortdsc"));
            Ddo_projetodesenvolvimento_sistemanom_Sortedstatus = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Sortedstatus");
            Ddo_projetodesenvolvimento_sistemanom_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Includefilter"));
            Ddo_projetodesenvolvimento_sistemanom_Filtertype = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Filtertype");
            Ddo_projetodesenvolvimento_sistemanom_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Filterisrange"));
            Ddo_projetodesenvolvimento_sistemanom_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Includedatalist"));
            Ddo_projetodesenvolvimento_sistemanom_Datalisttype = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Datalisttype");
            Ddo_projetodesenvolvimento_sistemanom_Datalistfixedvalues = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Datalistfixedvalues");
            Ddo_projetodesenvolvimento_sistemanom_Datalistproc = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Datalistproc");
            Ddo_projetodesenvolvimento_sistemanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_projetodesenvolvimento_sistemanom_Sortasc = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Sortasc");
            Ddo_projetodesenvolvimento_sistemanom_Sortdsc = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Sortdsc");
            Ddo_projetodesenvolvimento_sistemanom_Loadingdata = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Loadingdata");
            Ddo_projetodesenvolvimento_sistemanom_Cleanfilter = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Cleanfilter");
            Ddo_projetodesenvolvimento_sistemanom_Rangefilterfrom = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Rangefilterfrom");
            Ddo_projetodesenvolvimento_sistemanom_Rangefilterto = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Rangefilterto");
            Ddo_projetodesenvolvimento_sistemanom_Noresultsfound = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Noresultsfound");
            Ddo_projetodesenvolvimento_sistemanom_Searchbuttontext = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_projetodesenvolvimento_sistemanom_Activeeventkey = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Activeeventkey");
            Ddo_projetodesenvolvimento_sistemanom_Filteredtext_get = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Filteredtext_get");
            Ddo_projetodesenvolvimento_sistemanom_Selectedvalue_get = cgiGet( sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vPROJETODESENVOLVIMENTO_SISTEMANOM1"), AV16ProjetoDesenvolvimento_SistemaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFPROJETODESENVOLVIMENTO_SISTEMANOM"), AV20TFProjetoDesenvolvimento_SistemaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFPROJETODESENVOLVIMENTO_SISTEMANOM_SEL"), AV21TFProjetoDesenvolvimento_SistemaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E14DQ2 */
         E14DQ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14DQ2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV15DynamicFiltersSelector1 = "PROJETODESENVOLVIMENTO_SISTEMANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavTfprojetodesenvolvimento_sistemanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfprojetodesenvolvimento_sistemanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojetodesenvolvimento_sistemanom_Visible), 5, 0)));
         edtavTfprojetodesenvolvimento_sistemanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfprojetodesenvolvimento_sistemanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojetodesenvolvimento_sistemanom_sel_Visible), 5, 0)));
         Ddo_projetodesenvolvimento_sistemanom_Titlecontrolidtoreplace = subGrid_Internalname+"_ProjetoDesenvolvimento_SistemaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_projetodesenvolvimento_sistemanom_Internalname, "TitleControlIdToReplace", Ddo_projetodesenvolvimento_sistemanom_Titlecontrolidtoreplace);
         AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace = Ddo_projetodesenvolvimento_sistemanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace", AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace);
         edtavDdo_projetodesenvolvimento_sistemanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_projetodesenvolvimento_sistemanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_projetodesenvolvimento_sistemanomtitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV23DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV23DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E15DQ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV19ProjetoDesenvolvimento_SistemaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtProjetoDesenvolvimento_SistemaNom_Titleformat = 2;
         edtProjetoDesenvolvimento_SistemaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtProjetoDesenvolvimento_SistemaNom_Internalname, "Title", edtProjetoDesenvolvimento_SistemaNom_Title);
         AV25GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25GridCurrentPage), 10, 0)));
         AV26GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV19ProjetoDesenvolvimento_SistemaNomTitleFilterData", AV19ProjetoDesenvolvimento_SistemaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11DQ2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV24PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV24PageToGo) ;
         }
      }

      protected void E12DQ2( )
      {
         /* Ddo_projetodesenvolvimento_sistemanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_projetodesenvolvimento_sistemanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_projetodesenvolvimento_sistemanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_projetodesenvolvimento_sistemanom_Internalname, "SortedStatus", Ddo_projetodesenvolvimento_sistemanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_projetodesenvolvimento_sistemanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_projetodesenvolvimento_sistemanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_projetodesenvolvimento_sistemanom_Internalname, "SortedStatus", Ddo_projetodesenvolvimento_sistemanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_projetodesenvolvimento_sistemanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV20TFProjetoDesenvolvimento_SistemaNom = Ddo_projetodesenvolvimento_sistemanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFProjetoDesenvolvimento_SistemaNom", AV20TFProjetoDesenvolvimento_SistemaNom);
            AV21TFProjetoDesenvolvimento_SistemaNom_Sel = Ddo_projetodesenvolvimento_sistemanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFProjetoDesenvolvimento_SistemaNom_Sel", AV21TFProjetoDesenvolvimento_SistemaNom_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E16DQ2( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 30;
         }
         sendrow_302( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_30_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(30, GridRow);
         }
      }

      protected void E13DQ2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         Ddo_projetodesenvolvimento_sistemanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_projetodesenvolvimento_sistemanom_Internalname, "SortedStatus", Ddo_projetodesenvolvimento_sistemanom_Sortedstatus);
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavProjetodesenvolvimento_sistemanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavProjetodesenvolvimento_sistemanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjetodesenvolvimento_sistemanom1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETODESENVOLVIMENTO_SISTEMANOM") == 0 )
         {
            edtavProjetodesenvolvimento_sistemanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavProjetodesenvolvimento_sistemanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjetodesenvolvimento_sistemanom1_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV17Session.Get(AV29Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV29Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV17Session.Get(AV29Pgmname+"GridState"), "");
         }
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30GXV1 = 1;
         while ( AV30GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV30GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFPROJETODESENVOLVIMENTO_SISTEMANOM") == 0 )
            {
               AV20TFProjetoDesenvolvimento_SistemaNom = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFProjetoDesenvolvimento_SistemaNom", AV20TFProjetoDesenvolvimento_SistemaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFProjetoDesenvolvimento_SistemaNom)) )
               {
                  Ddo_projetodesenvolvimento_sistemanom_Filteredtext_set = AV20TFProjetoDesenvolvimento_SistemaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_projetodesenvolvimento_sistemanom_Internalname, "FilteredText_set", Ddo_projetodesenvolvimento_sistemanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFPROJETODESENVOLVIMENTO_SISTEMANOM_SEL") == 0 )
            {
               AV21TFProjetoDesenvolvimento_SistemaNom_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFProjetoDesenvolvimento_SistemaNom_Sel", AV21TFProjetoDesenvolvimento_SistemaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFProjetoDesenvolvimento_SistemaNom_Sel)) )
               {
                  Ddo_projetodesenvolvimento_sistemanom_Selectedvalue_set = AV21TFProjetoDesenvolvimento_SistemaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_projetodesenvolvimento_sistemanom_Internalname, "SelectedValue_set", Ddo_projetodesenvolvimento_sistemanom_Selectedvalue_set);
               }
            }
            AV30GXV1 = (int)(AV30GXV1+1);
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETODESENVOLVIMENTO_SISTEMANOM") == 0 )
            {
               AV16ProjetoDesenvolvimento_SistemaNom1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16ProjetoDesenvolvimento_SistemaNom1", AV16ProjetoDesenvolvimento_SistemaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV17Session.Get(AV29Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFProjetoDesenvolvimento_SistemaNom)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFPROJETODESENVOLVIMENTO_SISTEMANOM";
            AV12GridStateFilterValue.gxTpr_Value = AV20TFProjetoDesenvolvimento_SistemaNom;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFProjetoDesenvolvimento_SistemaNom_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFPROJETODESENVOLVIMENTO_SISTEMANOM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV21TFProjetoDesenvolvimento_SistemaNom_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV18ProjetoDesenvolvimento_ProjetoCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&PROJETODESENVOLVIMENTO_PROJETOCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV18ProjetoDesenvolvimento_ProjetoCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV29Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV13GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETODESENVOLVIMENTO_SISTEMANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV16ProjetoDesenvolvimento_SistemaNom1)) )
         {
            AV13GridStateDynamicFilter.gxTpr_Value = AV16ProjetoDesenvolvimento_SistemaNom1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
         }
      }

      protected void S122( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV29Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ProjetoDesenvolvimento";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ProjetoDesenvolvimento_ProjetoCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV18ProjetoDesenvolvimento_ProjetoCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV17Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_DQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_DQ2( true) ;
         }
         else
         {
            wb_table2_8_DQ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_DQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_27_DQ2( true) ;
         }
         else
         {
            wb_table3_27_DQ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_27_DQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DQ2e( true) ;
         }
         else
         {
            wb_table1_2_DQ2e( false) ;
         }
      }

      protected void wb_table3_27_DQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"30\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Projeto") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtProjetoDesenvolvimento_SistemaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtProjetoDesenvolvimento_SistemaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtProjetoDesenvolvimento_SistemaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A671ProjetoDesenvolvimento_SistemaNom);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtProjetoDesenvolvimento_SistemaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoDesenvolvimento_SistemaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 30 )
         {
            wbEnd = 0;
            nRC_GXsfl_30 = (short)(nGXsfl_30_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_27_DQ2e( true) ;
         }
         else
         {
            wb_table3_27_DQ2e( false) ;
         }
      }

      protected void wb_table2_8_DQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table4_11_DQ2( true) ;
         }
         else
         {
            wb_table4_11_DQ2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_DQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_DQ2e( true) ;
         }
         else
         {
            wb_table2_8_DQ2e( false) ;
         }
      }

      protected void wb_table4_11_DQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table5_14_DQ2( true) ;
         }
         else
         {
            wb_table5_14_DQ2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_DQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_DQ2e( true) ;
         }
         else
         {
            wb_table4_11_DQ2e( false) ;
         }
      }

      protected void wb_table5_14_DQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Projeto_ProjetoDesenvolvimentoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_30_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_Projeto_ProjetoDesenvolvimentoWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Projeto_ProjetoDesenvolvimentoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_30_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjetodesenvolvimento_sistemanom1_Internalname, AV16ProjetoDesenvolvimento_SistemaNom1, StringUtil.RTrim( context.localUtil.Format( AV16ProjetoDesenvolvimento_SistemaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjetodesenvolvimento_sistemanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjetodesenvolvimento_sistemanom1_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Projeto_ProjetoDesenvolvimentoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_DQ2e( true) ;
         }
         else
         {
            wb_table5_14_DQ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV18ProjetoDesenvolvimento_ProjetoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADQ2( ) ;
         WSDQ2( ) ;
         WEDQ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV18ProjetoDesenvolvimento_ProjetoCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PADQ2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "projeto_projetodesenvolvimentowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PADQ2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV18ProjetoDesenvolvimento_ProjetoCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
         }
         wcpOAV18ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV18ProjetoDesenvolvimento_ProjetoCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV18ProjetoDesenvolvimento_ProjetoCod != wcpOAV18ProjetoDesenvolvimento_ProjetoCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV18ProjetoDesenvolvimento_ProjetoCod = AV18ProjetoDesenvolvimento_ProjetoCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV18ProjetoDesenvolvimento_ProjetoCod = cgiGet( sPrefix+"AV18ProjetoDesenvolvimento_ProjetoCod_CTRL");
         if ( StringUtil.Len( sCtrlAV18ProjetoDesenvolvimento_ProjetoCod) > 0 )
         {
            AV18ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV18ProjetoDesenvolvimento_ProjetoCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ProjetoDesenvolvimento_ProjetoCod), 6, 0)));
         }
         else
         {
            AV18ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV18ProjetoDesenvolvimento_ProjetoCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PADQ2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSDQ2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSDQ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV18ProjetoDesenvolvimento_ProjetoCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18ProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV18ProjetoDesenvolvimento_ProjetoCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV18ProjetoDesenvolvimento_ProjetoCod_CTRL", StringUtil.RTrim( sCtrlAV18ProjetoDesenvolvimento_ProjetoCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEDQ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822501129");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("projeto_projetodesenvolvimentowc.js", "?202042822501129");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_302( )
      {
         edtProjetoDesenvolvimento_ProjetoCod_Internalname = sPrefix+"PROJETODESENVOLVIMENTO_PROJETOCOD_"+sGXsfl_30_idx;
         edtProjetoDesenvolvimento_SistemaCod_Internalname = sPrefix+"PROJETODESENVOLVIMENTO_SISTEMACOD_"+sGXsfl_30_idx;
         edtProjetoDesenvolvimento_SistemaNom_Internalname = sPrefix+"PROJETODESENVOLVIMENTO_SISTEMANOM_"+sGXsfl_30_idx;
      }

      protected void SubsflControlProps_fel_302( )
      {
         edtProjetoDesenvolvimento_ProjetoCod_Internalname = sPrefix+"PROJETODESENVOLVIMENTO_PROJETOCOD_"+sGXsfl_30_fel_idx;
         edtProjetoDesenvolvimento_SistemaCod_Internalname = sPrefix+"PROJETODESENVOLVIMENTO_SISTEMACOD_"+sGXsfl_30_fel_idx;
         edtProjetoDesenvolvimento_SistemaNom_Internalname = sPrefix+"PROJETODESENVOLVIMENTO_SISTEMANOM_"+sGXsfl_30_fel_idx;
      }

      protected void sendrow_302( )
      {
         SubsflControlProps_302( ) ;
         WBDQ0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_30_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_30_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_30_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjetoDesenvolvimento_ProjetoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjetoDesenvolvimento_ProjetoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjetoDesenvolvimento_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjetoDesenvolvimento_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjetoDesenvolvimento_SistemaNom_Internalname,(String)A671ProjetoDesenvolvimento_SistemaNom,StringUtil.RTrim( context.localUtil.Format( A671ProjetoDesenvolvimento_SistemaNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjetoDesenvolvimento_SistemaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)30,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETODESENVOLVIMENTO_PROJETOCOD"+"_"+sGXsfl_30_idx, GetSecureSignedToken( sPrefix+sGXsfl_30_idx, context.localUtil.Format( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETODESENVOLVIMENTO_SISTEMACOD"+"_"+sGXsfl_30_idx, GetSecureSignedToken( sPrefix+sGXsfl_30_idx, context.localUtil.Format( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_30_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_30_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_30_idx+1));
            sGXsfl_30_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_30_idx), 4, 0)), 4, "0");
            SubsflControlProps_302( ) ;
         }
         /* End function sendrow_302 */
      }

      protected void init_default_properties( )
      {
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavProjetodesenvolvimento_sistemanom1_Internalname = sPrefix+"vPROJETODESENVOLVIMENTO_SISTEMANOM1";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtProjetoDesenvolvimento_ProjetoCod_Internalname = sPrefix+"PROJETODESENVOLVIMENTO_PROJETOCOD";
         edtProjetoDesenvolvimento_SistemaCod_Internalname = sPrefix+"PROJETODESENVOLVIMENTO_SISTEMACOD";
         edtProjetoDesenvolvimento_SistemaNom_Internalname = sPrefix+"PROJETODESENVOLVIMENTO_SISTEMANOM";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfprojetodesenvolvimento_sistemanom_Internalname = sPrefix+"vTFPROJETODESENVOLVIMENTO_SISTEMANOM";
         edtavTfprojetodesenvolvimento_sistemanom_sel_Internalname = sPrefix+"vTFPROJETODESENVOLVIMENTO_SISTEMANOM_SEL";
         Ddo_projetodesenvolvimento_sistemanom_Internalname = sPrefix+"DDO_PROJETODESENVOLVIMENTO_SISTEMANOM";
         edtavDdo_projetodesenvolvimento_sistemanomtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_PROJETODESENVOLVIMENTO_SISTEMANOMTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtProjetoDesenvolvimento_SistemaNom_Jsonclick = "";
         edtProjetoDesenvolvimento_SistemaCod_Jsonclick = "";
         edtProjetoDesenvolvimento_ProjetoCod_Jsonclick = "";
         edtavProjetodesenvolvimento_sistemanom1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtProjetoDesenvolvimento_SistemaNom_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavProjetodesenvolvimento_sistemanom1_Visible = 1;
         edtProjetoDesenvolvimento_SistemaNom_Title = "Nome";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_projetodesenvolvimento_sistemanomtitlecontrolidtoreplace_Visible = 1;
         edtavTfprojetodesenvolvimento_sistemanom_sel_Jsonclick = "";
         edtavTfprojetodesenvolvimento_sistemanom_sel_Visible = 1;
         edtavTfprojetodesenvolvimento_sistemanom_Jsonclick = "";
         edtavTfprojetodesenvolvimento_sistemanom_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         Ddo_projetodesenvolvimento_sistemanom_Searchbuttontext = "Pesquisar";
         Ddo_projetodesenvolvimento_sistemanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_projetodesenvolvimento_sistemanom_Rangefilterto = "At�";
         Ddo_projetodesenvolvimento_sistemanom_Rangefilterfrom = "Desde";
         Ddo_projetodesenvolvimento_sistemanom_Cleanfilter = "Limpar pesquisa";
         Ddo_projetodesenvolvimento_sistemanom_Loadingdata = "Carregando dados...";
         Ddo_projetodesenvolvimento_sistemanom_Sortdsc = "Ordenar de Z � A";
         Ddo_projetodesenvolvimento_sistemanom_Sortasc = "Ordenar de A � Z";
         Ddo_projetodesenvolvimento_sistemanom_Datalistupdateminimumcharacters = 0;
         Ddo_projetodesenvolvimento_sistemanom_Datalistproc = "GetProjeto_ProjetoDesenvolvimentoWCFilterData";
         Ddo_projetodesenvolvimento_sistemanom_Datalistfixedvalues = "";
         Ddo_projetodesenvolvimento_sistemanom_Datalisttype = "Dynamic";
         Ddo_projetodesenvolvimento_sistemanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_projetodesenvolvimento_sistemanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_projetodesenvolvimento_sistemanom_Filtertype = "Character";
         Ddo_projetodesenvolvimento_sistemanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_projetodesenvolvimento_sistemanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_projetodesenvolvimento_sistemanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_projetodesenvolvimento_sistemanom_Titlecontrolidtoreplace = "";
         Ddo_projetodesenvolvimento_sistemanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_projetodesenvolvimento_sistemanom_Cls = "ColumnSettings";
         Ddo_projetodesenvolvimento_sistemanom_Tooltip = "Op��es";
         Ddo_projetodesenvolvimento_sistemanom_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_SISTEMANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFProjetoDesenvolvimento_SistemaNom',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMANOM',pic:'@!',nv:''},{av:'AV21TFProjetoDesenvolvimento_SistemaNom_Sel',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMANOM_SEL',pic:'@!',nv:''},{av:'AV18ProjetoDesenvolvimento_ProjetoCod',fld:'vPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ProjetoDesenvolvimento_SistemaNom1',fld:'vPROJETODESENVOLVIMENTO_SISTEMANOM1',pic:'@!',nv:''}],oparms:[{av:'AV19ProjetoDesenvolvimento_SistemaNomTitleFilterData',fld:'vPROJETODESENVOLVIMENTO_SISTEMANOMTITLEFILTERDATA',pic:'',nv:null},{av:'edtProjetoDesenvolvimento_SistemaNom_Titleformat',ctrl:'PROJETODESENVOLVIMENTO_SISTEMANOM',prop:'Titleformat'},{av:'edtProjetoDesenvolvimento_SistemaNom_Title',ctrl:'PROJETODESENVOLVIMENTO_SISTEMANOM',prop:'Title'},{av:'AV25GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV26GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11DQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ProjetoDesenvolvimento_SistemaNom1',fld:'vPROJETODESENVOLVIMENTO_SISTEMANOM1',pic:'@!',nv:''},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFProjetoDesenvolvimento_SistemaNom',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMANOM',pic:'@!',nv:''},{av:'AV21TFProjetoDesenvolvimento_SistemaNom_Sel',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMANOM_SEL',pic:'@!',nv:''},{av:'AV18ProjetoDesenvolvimento_ProjetoCod',fld:'vPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_SISTEMANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_PROJETODESENVOLVIMENTO_SISTEMANOM.ONOPTIONCLICKED","{handler:'E12DQ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ProjetoDesenvolvimento_SistemaNom1',fld:'vPROJETODESENVOLVIMENTO_SISTEMANOM1',pic:'@!',nv:''},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFProjetoDesenvolvimento_SistemaNom',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMANOM',pic:'@!',nv:''},{av:'AV21TFProjetoDesenvolvimento_SistemaNom_Sel',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMANOM_SEL',pic:'@!',nv:''},{av:'AV18ProjetoDesenvolvimento_ProjetoCod',fld:'vPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_SISTEMANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Ddo_projetodesenvolvimento_sistemanom_Activeeventkey',ctrl:'DDO_PROJETODESENVOLVIMENTO_SISTEMANOM',prop:'ActiveEventKey'},{av:'Ddo_projetodesenvolvimento_sistemanom_Filteredtext_get',ctrl:'DDO_PROJETODESENVOLVIMENTO_SISTEMANOM',prop:'FilteredText_get'},{av:'Ddo_projetodesenvolvimento_sistemanom_Selectedvalue_get',ctrl:'DDO_PROJETODESENVOLVIMENTO_SISTEMANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_projetodesenvolvimento_sistemanom_Sortedstatus',ctrl:'DDO_PROJETODESENVOLVIMENTO_SISTEMANOM',prop:'SortedStatus'},{av:'AV20TFProjetoDesenvolvimento_SistemaNom',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMANOM',pic:'@!',nv:''},{av:'AV21TFProjetoDesenvolvimento_SistemaNom_Sel',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMANOM_SEL',pic:'@!',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E16DQ2',iparms:[],oparms:[]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E13DQ2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavProjetodesenvolvimento_sistemanom1_Visible',ctrl:'vPROJETODESENVOLVIMENTO_SISTEMANOM1',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_projetodesenvolvimento_sistemanom_Activeeventkey = "";
         Ddo_projetodesenvolvimento_sistemanom_Filteredtext_get = "";
         Ddo_projetodesenvolvimento_sistemanom_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15DynamicFiltersSelector1 = "";
         AV16ProjetoDesenvolvimento_SistemaNom1 = "";
         AV20TFProjetoDesenvolvimento_SistemaNom = "";
         AV21TFProjetoDesenvolvimento_SistemaNom_Sel = "";
         AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace = "";
         AV29Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV23DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV19ProjetoDesenvolvimento_SistemaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_projetodesenvolvimento_sistemanom_Filteredtext_set = "";
         Ddo_projetodesenvolvimento_sistemanom_Selectedvalue_set = "";
         Ddo_projetodesenvolvimento_sistemanom_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A671ProjetoDesenvolvimento_SistemaNom = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV16ProjetoDesenvolvimento_SistemaNom1 = "";
         lV20TFProjetoDesenvolvimento_SistemaNom = "";
         H00DQ2_A671ProjetoDesenvolvimento_SistemaNom = new String[] {""} ;
         H00DQ2_n671ProjetoDesenvolvimento_SistemaNom = new bool[] {false} ;
         H00DQ2_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         H00DQ2_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         H00DQ3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV17Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV18ProjetoDesenvolvimento_ProjetoCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.projeto_projetodesenvolvimentowc__default(),
            new Object[][] {
                new Object[] {
               H00DQ2_A671ProjetoDesenvolvimento_SistemaNom, H00DQ2_n671ProjetoDesenvolvimento_SistemaNom, H00DQ2_A670ProjetoDesenvolvimento_SistemaCod, H00DQ2_A669ProjetoDesenvolvimento_ProjetoCod
               }
               , new Object[] {
               H00DQ3_AGRID_nRecordCount
               }
            }
         );
         AV29Pgmname = "Projeto_ProjetoDesenvolvimentoWC";
         /* GeneXus formulas. */
         AV29Pgmname = "Projeto_ProjetoDesenvolvimentoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_30 ;
      private short nGXsfl_30_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_30_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtProjetoDesenvolvimento_SistemaNom_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV18ProjetoDesenvolvimento_ProjetoCod ;
      private int wcpOAV18ProjetoDesenvolvimento_ProjetoCod ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_projetodesenvolvimento_sistemanom_Datalistupdateminimumcharacters ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfprojetodesenvolvimento_sistemanom_Visible ;
      private int edtavTfprojetodesenvolvimento_sistemanom_sel_Visible ;
      private int edtavDdo_projetodesenvolvimento_sistemanomtitlecontrolidtoreplace_Visible ;
      private int A669ProjetoDesenvolvimento_ProjetoCod ;
      private int A670ProjetoDesenvolvimento_SistemaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV24PageToGo ;
      private int edtavProjetodesenvolvimento_sistemanom1_Visible ;
      private int AV30GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV25GridCurrentPage ;
      private long AV26GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_projetodesenvolvimento_sistemanom_Activeeventkey ;
      private String Ddo_projetodesenvolvimento_sistemanom_Filteredtext_get ;
      private String Ddo_projetodesenvolvimento_sistemanom_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_30_idx="0001" ;
      private String AV29Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_projetodesenvolvimento_sistemanom_Caption ;
      private String Ddo_projetodesenvolvimento_sistemanom_Tooltip ;
      private String Ddo_projetodesenvolvimento_sistemanom_Cls ;
      private String Ddo_projetodesenvolvimento_sistemanom_Filteredtext_set ;
      private String Ddo_projetodesenvolvimento_sistemanom_Selectedvalue_set ;
      private String Ddo_projetodesenvolvimento_sistemanom_Dropdownoptionstype ;
      private String Ddo_projetodesenvolvimento_sistemanom_Titlecontrolidtoreplace ;
      private String Ddo_projetodesenvolvimento_sistemanom_Sortedstatus ;
      private String Ddo_projetodesenvolvimento_sistemanom_Filtertype ;
      private String Ddo_projetodesenvolvimento_sistemanom_Datalisttype ;
      private String Ddo_projetodesenvolvimento_sistemanom_Datalistfixedvalues ;
      private String Ddo_projetodesenvolvimento_sistemanom_Datalistproc ;
      private String Ddo_projetodesenvolvimento_sistemanom_Sortasc ;
      private String Ddo_projetodesenvolvimento_sistemanom_Sortdsc ;
      private String Ddo_projetodesenvolvimento_sistemanom_Loadingdata ;
      private String Ddo_projetodesenvolvimento_sistemanom_Cleanfilter ;
      private String Ddo_projetodesenvolvimento_sistemanom_Rangefilterfrom ;
      private String Ddo_projetodesenvolvimento_sistemanom_Rangefilterto ;
      private String Ddo_projetodesenvolvimento_sistemanom_Noresultsfound ;
      private String Ddo_projetodesenvolvimento_sistemanom_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfprojetodesenvolvimento_sistemanom_Internalname ;
      private String edtavTfprojetodesenvolvimento_sistemanom_Jsonclick ;
      private String edtavTfprojetodesenvolvimento_sistemanom_sel_Internalname ;
      private String edtavTfprojetodesenvolvimento_sistemanom_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_projetodesenvolvimento_sistemanomtitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtProjetoDesenvolvimento_ProjetoCod_Internalname ;
      private String edtProjetoDesenvolvimento_SistemaCod_Internalname ;
      private String edtProjetoDesenvolvimento_SistemaNom_Internalname ;
      private String scmdbuf ;
      private String edtavProjetodesenvolvimento_sistemanom1_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_projetodesenvolvimento_sistemanom_Internalname ;
      private String edtProjetoDesenvolvimento_SistemaNom_Title ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String tblTablefilters_Internalname ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavProjetodesenvolvimento_sistemanom1_Jsonclick ;
      private String sCtrlAV18ProjetoDesenvolvimento_ProjetoCod ;
      private String sGXsfl_30_fel_idx="0001" ;
      private String ROClassString ;
      private String edtProjetoDesenvolvimento_ProjetoCod_Jsonclick ;
      private String edtProjetoDesenvolvimento_SistemaCod_Jsonclick ;
      private String edtProjetoDesenvolvimento_SistemaNom_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_projetodesenvolvimento_sistemanom_Includesortasc ;
      private bool Ddo_projetodesenvolvimento_sistemanom_Includesortdsc ;
      private bool Ddo_projetodesenvolvimento_sistemanom_Includefilter ;
      private bool Ddo_projetodesenvolvimento_sistemanom_Filterisrange ;
      private bool Ddo_projetodesenvolvimento_sistemanom_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n671ProjetoDesenvolvimento_SistemaNom ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV16ProjetoDesenvolvimento_SistemaNom1 ;
      private String AV20TFProjetoDesenvolvimento_SistemaNom ;
      private String AV21TFProjetoDesenvolvimento_SistemaNom_Sel ;
      private String AV22ddo_ProjetoDesenvolvimento_SistemaNomTitleControlIdToReplace ;
      private String A671ProjetoDesenvolvimento_SistemaNom ;
      private String lV16ProjetoDesenvolvimento_SistemaNom1 ;
      private String lV20TFProjetoDesenvolvimento_SistemaNom ;
      private IGxSession AV17Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private IDataStoreProvider pr_default ;
      private String[] H00DQ2_A671ProjetoDesenvolvimento_SistemaNom ;
      private bool[] H00DQ2_n671ProjetoDesenvolvimento_SistemaNom ;
      private int[] H00DQ2_A670ProjetoDesenvolvimento_SistemaCod ;
      private int[] H00DQ2_A669ProjetoDesenvolvimento_ProjetoCod ;
      private long[] H00DQ3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV19ProjetoDesenvolvimento_SistemaNomTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV23DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class projeto_projetodesenvolvimentowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00DQ2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV16ProjetoDesenvolvimento_SistemaNom1 ,
                                             String AV21TFProjetoDesenvolvimento_SistemaNom_Sel ,
                                             String AV20TFProjetoDesenvolvimento_SistemaNom ,
                                             String A671ProjetoDesenvolvimento_SistemaNom ,
                                             bool AV14OrderedDsc ,
                                             int A669ProjetoDesenvolvimento_ProjetoCod ,
                                             int AV18ProjetoDesenvolvimento_ProjetoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [9] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Sistema_Nome] AS ProjetoDesenvolvimento_SistemaNom, T1.[ProjetoDesenvolvimento_SistemaCod] AS ProjetoDesenvolvimento_SistemaCod, T1.[ProjetoDesenvolvimento_ProjetoCod]";
         sFromString = " FROM ([ProjetoDesenvolvimento] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ProjetoDesenvolvimento_SistemaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ProjetoDesenvolvimento_ProjetoCod] = @AV18ProjetoDesenvolvimento_ProjetoCod)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETODESENVOLVIMENTO_SISTEMANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16ProjetoDesenvolvimento_SistemaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV16ProjetoDesenvolvimento_SistemaNom1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFProjetoDesenvolvimento_SistemaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFProjetoDesenvolvimento_SistemaNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV20TFProjetoDesenvolvimento_SistemaNom)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFProjetoDesenvolvimento_SistemaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] = @AV21TFProjetoDesenvolvimento_SistemaNom_Sel)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ProjetoDesenvolvimento_ProjetoCod], T2.[Sistema_Nome]";
         }
         else if ( AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ProjetoDesenvolvimento_ProjetoCod] DESC, T2.[Sistema_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ProjetoDesenvolvimento_ProjetoCod], T1.[ProjetoDesenvolvimento_SistemaCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00DQ3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV16ProjetoDesenvolvimento_SistemaNom1 ,
                                             String AV21TFProjetoDesenvolvimento_SistemaNom_Sel ,
                                             String AV20TFProjetoDesenvolvimento_SistemaNom ,
                                             String A671ProjetoDesenvolvimento_SistemaNom ,
                                             bool AV14OrderedDsc ,
                                             int A669ProjetoDesenvolvimento_ProjetoCod ,
                                             int AV18ProjetoDesenvolvimento_ProjetoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [4] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ProjetoDesenvolvimento] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ProjetoDesenvolvimento_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ProjetoDesenvolvimento_ProjetoCod] = @AV18ProjetoDesenvolvimento_ProjetoCod)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETODESENVOLVIMENTO_SISTEMANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16ProjetoDesenvolvimento_SistemaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV16ProjetoDesenvolvimento_SistemaNom1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFProjetoDesenvolvimento_SistemaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFProjetoDesenvolvimento_SistemaNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV20TFProjetoDesenvolvimento_SistemaNom)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFProjetoDesenvolvimento_SistemaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] = @AV21TFProjetoDesenvolvimento_SistemaNom_Sel)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00DQ2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
               case 1 :
                     return conditional_H00DQ3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DQ2 ;
          prmH00DQ2 = new Object[] {
          new Object[] {"@AV18ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16ProjetoDesenvolvimento_SistemaNom1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV20TFProjetoDesenvolvimento_SistemaNom",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV21TFProjetoDesenvolvimento_SistemaNom_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00DQ3 ;
          prmH00DQ3 = new Object[] {
          new Object[] {"@AV18ProjetoDesenvolvimento_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16ProjetoDesenvolvimento_SistemaNom1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV20TFProjetoDesenvolvimento_SistemaNom",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV21TFProjetoDesenvolvimento_SistemaNom_Sel",SqlDbType.VarChar,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DQ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DQ2,11,0,true,false )
             ,new CursorDef("H00DQ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DQ3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

}
