/*
               File: GxAfterEventReplicator
        Description: Gx After Event Replicator
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:57.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxaftereventreplicator : GXProcedure
   {
      public gxaftereventreplicator( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public gxaftereventreplicator( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( IGxCollection aP0_EventResults ,
                           SdtGxSynchroInfoSDT aP1_GxSynchroInfo )
      {
         this.AV8EventResults = aP0_EventResults;
         this.GxSynchroInfo = aP1_GxSynchroInfo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( IGxCollection aP0_EventResults ,
                                 SdtGxSynchroInfoSDT aP1_GxSynchroInfo )
      {
         gxaftereventreplicator objgxaftereventreplicator;
         objgxaftereventreplicator = new gxaftereventreplicator();
         objgxaftereventreplicator.AV8EventResults = aP0_EventResults;
         objgxaftereventreplicator.GxSynchroInfo = aP1_GxSynchroInfo;
         objgxaftereventreplicator.context.SetSubmitInitialConfig(context);
         objgxaftereventreplicator.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgxaftereventreplicator);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((gxaftereventreplicator)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      [ObjectCollection(ItemType=typeof( SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem ))]
      private IGxCollection AV8EventResults ;
      private SdtGxSynchroInfoSDT GxSynchroInfo ;
   }

}
