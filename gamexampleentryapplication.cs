/*
               File: GAMExampleEntryApplication
        Description: Application
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:12:4.85
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexampleentryapplication : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamexampleentryapplication( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamexampleentryapplication( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Gx_mode ,
                           ref long aP1_Id )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV24Id = aP1_Id;
         executePrivate();
         aP0_Gx_mode=this.Gx_mode;
         aP1_Id=this.AV24Id;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavAccessrequirespermission = new GXCheckbox();
         chkavClientaccessuniquebyuser = new GXCheckbox();
         chkavClientallowremoteauth = new GXCheckbox();
         chkavClientallowgetuserroles = new GXCheckbox();
         chkavClientallowgetuseradddata = new GXCheckbox();
         chkavAutoregisteranomymoususer = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               Gx_mode = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV24Id = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Id), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")));
               }
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpage", "GeneXus.Programs.gammasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA2O2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START2O2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020428231255");
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexampleentryapplication.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV24Id)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE2O2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT2O2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamexampleentryapplication.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV24Id) ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleEntryApplication" ;
      }

      public override String GetPgmdesc( )
      {
         return "Application" ;
      }

      protected void WB2O0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_2O2( true) ;
         }
         else
         {
            wb_table1_2_2O2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_2O2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table2_46_2O2( true) ;
         }
         else
         {
            wb_table2_46_2O2( false) ;
         }
         return  ;
      }

      protected void wb_table2_46_2O2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table3_83_2O2( true) ;
         }
         else
         {
            wb_table3_83_2O2( false) ;
         }
         return  ;
      }

      protected void wb_table3_83_2O2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table4_116_2O2( true) ;
         }
         else
         {
            wb_table4_116_2O2( false) ;
         }
         return  ;
      }

      protected void wb_table4_116_2O2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAutoregisteranomymoususer_Internalname, StringUtil.BoolToStr( AV9AutoRegisterAnomymousUser), "", "", chkavAutoregisteranomymoususer.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(125, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"");
         }
         wbLoad = true;
      }

      protected void START2O2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Application", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP2O0( ) ;
      }

      protected void WS2O2( )
      {
         START2O2( ) ;
         EVT2O2( ) ;
      }

      protected void EVT2O2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E112O2 */
                              E112O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E122O2 */
                              E122O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E132O2 */
                                    E132O2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REVOKE-AUTHORIZE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E142O2 */
                              E142O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CLOSE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E152O2 */
                              E152O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'GENERATEKEYGAMREMOTE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E162O2 */
                              E162O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E172O2 */
                              E172O2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE2O2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA2O2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            chkavAccessrequirespermission.Name = "vACCESSREQUIRESPERMISSION";
            chkavAccessrequirespermission.WebTags = "";
            chkavAccessrequirespermission.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAccessrequirespermission_Internalname, "TitleCaption", chkavAccessrequirespermission.Caption);
            chkavAccessrequirespermission.CheckedValue = "false";
            chkavClientaccessuniquebyuser.Name = "vCLIENTACCESSUNIQUEBYUSER";
            chkavClientaccessuniquebyuser.WebTags = "";
            chkavClientaccessuniquebyuser.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientaccessuniquebyuser_Internalname, "TitleCaption", chkavClientaccessuniquebyuser.Caption);
            chkavClientaccessuniquebyuser.CheckedValue = "false";
            chkavClientallowremoteauth.Name = "vCLIENTALLOWREMOTEAUTH";
            chkavClientallowremoteauth.WebTags = "";
            chkavClientallowremoteauth.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientallowremoteauth_Internalname, "TitleCaption", chkavClientallowremoteauth.Caption);
            chkavClientallowremoteauth.CheckedValue = "false";
            chkavClientallowgetuserroles.Name = "vCLIENTALLOWGETUSERROLES";
            chkavClientallowgetuserroles.WebTags = "";
            chkavClientallowgetuserroles.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientallowgetuserroles_Internalname, "TitleCaption", chkavClientallowgetuserroles.Caption);
            chkavClientallowgetuserroles.CheckedValue = "false";
            chkavClientallowgetuseradddata.Name = "vCLIENTALLOWGETUSERADDDATA";
            chkavClientallowgetuseradddata.WebTags = "";
            chkavClientallowgetuseradddata.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientallowgetuseradddata_Internalname, "TitleCaption", chkavClientallowgetuseradddata.Caption);
            chkavClientallowgetuseradddata.CheckedValue = "false";
            chkavAutoregisteranomymoususer.Name = "vAUTOREGISTERANOMYMOUSUSER";
            chkavAutoregisteranomymoususer.WebTags = "";
            chkavAutoregisteranomymoususer.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAutoregisteranomymoususer_Internalname, "TitleCaption", chkavAutoregisteranomymoususer.Caption);
            chkavAutoregisteranomymoususer.CheckedValue = "false";
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavGuid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF2O2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavId_Enabled), 5, 0)));
         edtavGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)));
         edtavClientrevoked_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientrevoked_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientrevoked_Enabled), 5, 0)));
      }

      protected void RF2O2( )
      {
         /* Execute user event: E122O2 */
         E122O2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E172O2 */
            E172O2 ();
            WB2O0( ) ;
         }
      }

      protected void STRUP2O0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavId_Enabled), 5, 0)));
         edtavGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)));
         edtavClientrevoked_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientrevoked_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientrevoked_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112O2 */
         E112O2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV24Id = (long)(context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Id), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")));
            AV23GUID = cgiGet( edtavGuid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23GUID", AV23GUID);
            AV26Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Name", AV26Name);
            AV20Dsc = cgiGet( edtavDsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Dsc", AV20Dsc);
            AV31Version = cgiGet( edtavVersion_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Version", AV31Version);
            AV18Company = cgiGet( edtavCompany_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Company", AV18Company);
            AV19Copyright = cgiGet( edtavCopyright_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Copyright", AV19Copyright);
            AV6AccessRequiresPermission = StringUtil.StrToBool( cgiGet( chkavAccessrequirespermission_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AccessRequiresPermission", AV6AccessRequiresPermission);
            AV13ClientId = cgiGet( edtavClientid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ClientId", AV13ClientId);
            AV16ClientSecret = cgiGet( edtavClientsecret_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ClientSecret", AV16ClientSecret);
            AV10ClientAccessUniqueByUser = StringUtil.StrToBool( cgiGet( chkavClientaccessuniquebyuser_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ClientAccessUniqueByUser", AV10ClientAccessUniqueByUser);
            if ( context.localUtil.VCDateTime( cgiGet( edtavClientrevoked_Internalname), 2, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Client Revoked"}), 1, "vCLIENTREVOKED");
               GX_FocusControl = edtavClientrevoked_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV15ClientRevoked = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ClientRevoked", context.localUtil.TToC( AV15ClientRevoked, 10, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV15ClientRevoked = context.localUtil.CToT( cgiGet( edtavClientrevoked_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ClientRevoked", context.localUtil.TToC( AV15ClientRevoked, 10, 5, 0, 3, "/", ":", " "));
            }
            AV11ClientAllowRemoteAuth = StringUtil.StrToBool( cgiGet( chkavClientallowremoteauth_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ClientAllowRemoteAuth", AV11ClientAllowRemoteAuth);
            AV33ClientAllowGetUserRoles = StringUtil.StrToBool( cgiGet( chkavClientallowgetuserroles_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ClientAllowGetUserRoles", AV33ClientAllowGetUserRoles);
            AV34ClientAllowGetUserAddData = StringUtil.StrToBool( cgiGet( chkavClientallowgetuseradddata_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ClientAllowGetUserAddData", AV34ClientAllowGetUserAddData);
            AV32ClientLocalLoginURL = cgiGet( edtavClientlocalloginurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ClientLocalLoginURL", AV32ClientLocalLoginURL);
            AV35ClientCallbackURL = cgiGet( edtavClientcallbackurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ClientCallbackURL", AV35ClientCallbackURL);
            AV14ClientImageURL = cgiGet( edtavClientimageurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ClientImageURL", AV14ClientImageURL);
            AV12ClientEncryptionKey = cgiGet( edtavClientencryptionkey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ClientEncryptionKey", AV12ClientEncryptionKey);
            AV9AutoRegisterAnomymousUser = StringUtil.StrToBool( cgiGet( chkavAutoregisteranomymoususer_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AutoRegisterAnomymousUser", AV9AutoRegisterAnomymousUser);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E112O2 */
         E112O2 ();
         if (returnInSub) return;
      }

      protected void E112O2( )
      {
         /* Start Routine */
         chkavAutoregisteranomymoususer.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAutoregisteranomymoususer_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAutoregisteranomymoususer.Visible), 5, 0)));
         AV30User = new SdtGAMUser(context).get();
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            AV7Application.load( AV24Id);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Id), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")));
            AV24Id = AV7Application.gxTpr_Id;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Id), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")));
            AV23GUID = AV7Application.gxTpr_Guid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23GUID", AV23GUID);
            AV26Name = AV7Application.gxTpr_Name;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Name", AV26Name);
            AV20Dsc = AV7Application.gxTpr_Description;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Dsc", AV20Dsc);
            AV31Version = AV7Application.gxTpr_Version;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Version", AV31Version);
            AV19Copyright = AV7Application.gxTpr_Copyright;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Copyright", AV19Copyright);
            AV18Company = AV7Application.gxTpr_Companyname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Company", AV18Company);
            AV6AccessRequiresPermission = AV7Application.gxTpr_Accessrequirespermission;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AccessRequiresPermission", AV6AccessRequiresPermission);
            AV9AutoRegisterAnomymousUser = AV7Application.gxTpr_Clientautoregisteranomymoususer;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AutoRegisterAnomymousUser", AV9AutoRegisterAnomymousUser);
            AV13ClientId = AV7Application.gxTpr_Clientid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ClientId", AV13ClientId);
            AV16ClientSecret = AV7Application.gxTpr_Clientsecret;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ClientSecret", AV16ClientSecret);
            AV10ClientAccessUniqueByUser = AV7Application.gxTpr_Clientaccessuniquebyuser;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ClientAccessUniqueByUser", AV10ClientAccessUniqueByUser);
            AV15ClientRevoked = AV7Application.gxTpr_Clientrevoked;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ClientRevoked", context.localUtil.TToC( AV15ClientRevoked, 10, 5, 0, 3, "/", ":", " "));
            AV11ClientAllowRemoteAuth = AV7Application.gxTpr_Clientallowremoteauthentication;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ClientAllowRemoteAuth", AV11ClientAllowRemoteAuth);
            AV33ClientAllowGetUserRoles = AV7Application.gxTpr_Clientallowgetuserroles;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ClientAllowGetUserRoles", AV33ClientAllowGetUserRoles);
            AV34ClientAllowGetUserAddData = AV7Application.gxTpr_Clientallowgetuseradditionaldata;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ClientAllowGetUserAddData", AV34ClientAllowGetUserAddData);
            AV32ClientLocalLoginURL = AV7Application.gxTpr_Clientlocalloginurl;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ClientLocalLoginURL", AV32ClientLocalLoginURL);
            AV35ClientCallbackURL = AV7Application.gxTpr_Clientcallbackurl;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ClientCallbackURL", AV35ClientCallbackURL);
            AV14ClientImageURL = AV7Application.gxTpr_Clientimageurl;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ClientImageURL", AV14ClientImageURL);
            AV12ClientEncryptionKey = AV7Application.gxTpr_Clientencryptionkey;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ClientEncryptionKey", AV12ClientEncryptionKey);
            if ( (DateTime.MinValue==AV15ClientRevoked) )
            {
               bttBtnrevokeallow_Caption = "Revoke";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnrevokeallow_Internalname, "Caption", bttBtnrevokeallow_Caption);
            }
            else
            {
               bttBtnrevokeallow_Caption = "Authorize";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnrevokeallow_Internalname, "Caption", bttBtnrevokeallow_Caption);
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            edtavGuid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)));
            edtavName_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
            edtavDsc_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDsc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDsc_Enabled), 5, 0)));
            edtavVersion_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVersion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVersion_Enabled), 5, 0)));
            edtavCopyright_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCopyright_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCopyright_Enabled), 5, 0)));
            edtavCompany_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCompany_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCompany_Enabled), 5, 0)));
            chkavAccessrequirespermission.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAccessrequirespermission_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAccessrequirespermission.Enabled), 5, 0)));
            edtavClientid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientid_Enabled), 5, 0)));
            edtavClientsecret_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientsecret_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientsecret_Enabled), 5, 0)));
            chkavClientaccessuniquebyuser.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientaccessuniquebyuser_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavClientaccessuniquebyuser.Enabled), 5, 0)));
            edtavClientrevoked_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientrevoked_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientrevoked_Enabled), 5, 0)));
            chkavClientallowremoteauth.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientallowremoteauth_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavClientallowremoteauth.Enabled), 5, 0)));
            chkavClientallowgetuserroles.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientallowgetuserroles_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavClientallowgetuserroles.Enabled), 5, 0)));
            chkavClientallowgetuseradddata.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientallowgetuseradddata_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavClientallowgetuseradddata.Enabled), 5, 0)));
            edtavClientcallbackurl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientcallbackurl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientcallbackurl_Enabled), 5, 0)));
            edtavClientimageurl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientimageurl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientimageurl_Enabled), 5, 0)));
            edtavClientencryptionkey_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientencryptionkey_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientencryptionkey_Enabled), 5, 0)));
            bttBtnconfirm_Caption = "Delete";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Caption", bttBtnconfirm_Caption);
         }
         tblTblclient3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblclient3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblclient3_Visible), 5, 0)));
         if ( AV11ClientAllowRemoteAuth )
         {
            tblTblclient3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblclient3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblclient3_Visible), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtnconfirm_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirm_Visible), 5, 0)));
         }
      }

      protected void E122O2( )
      {
         /* Refresh Routine */
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            bttBtnrevokeallow_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnrevokeallow_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnrevokeallow_Visible), 5, 0)));
         }
         else
         {
            bttBtnrevokeallow_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnrevokeallow_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnrevokeallow_Visible), 5, 0)));
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E132O2 */
         E132O2 ();
         if (returnInSub) return;
      }

      protected void E132O2( )
      {
         /* Enter Routine */
         AV7Application.gxTpr_Id = AV24Id;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) )
         {
            AV7Application.gxTpr_Name = AV26Name;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Description = AV20Dsc;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Version = AV31Version;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Copyright = AV19Copyright;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Companyname = AV18Company;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Accessrequirespermission = AV6AccessRequiresPermission;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Clientautoregisteranomymoususer = AV9AutoRegisterAnomymousUser;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Clientid = AV13ClientId;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Clientsecret = AV16ClientSecret;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Clientaccessuniquebyuser = AV10ClientAccessUniqueByUser;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Clientallowremoteauthentication = AV11ClientAllowRemoteAuth;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Clientallowgetuserroles = AV33ClientAllowGetUserRoles;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Clientallowgetuseradditionaldata = AV34ClientAllowGetUserAddData;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Clientlocalloginurl = AV32ClientLocalLoginURL;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Clientcallbackurl = AV35ClientCallbackURL;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Clientimageurl = AV14ClientImageURL;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.gxTpr_Clientencryptionkey = AV12ClientEncryptionKey;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Application", AV7Application);
            AV7Application.save();
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            AV7Application.delete();
         }
         if ( AV7Application.success() )
         {
            context.CommitDataStores( "GAMExampleEntryApplication");
            context.setWebReturnParms(new Object[] {(String)Gx_mode,(long)AV24Id});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            AV22Errors = AV7Application.geterrors();
            /* Execute user subroutine: 'ERRORES' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void E142O2( )
      {
         /* 'Revoke-Authorize' Routine */
         AV7Application.load( AV24Id);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Id), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")));
         if ( (DateTime.MinValue==AV7Application.gxTpr_Clientrevoked) )
         {
            AV5isOk = AV7Application.revokeclient(out  AV22Errors);
         }
         else
         {
            AV5isOk = AV7Application.authorizeclient(out  AV22Errors);
         }
         if ( AV5isOk )
         {
            context.CommitDataStores( "GAMExampleEntryApplication");
            context.setWebReturnParms(new Object[] {(String)Gx_mode,(long)AV24Id});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            /* Execute user subroutine: 'ERRORES' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void E152O2( )
      {
         /* 'Close' Routine */
         context.setWebReturnParms(new Object[] {(String)Gx_mode,(long)AV24Id});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'ERRORES' Routine */
         if ( AV22Errors.Count > 0 )
         {
            AV39GXV1 = 1;
            while ( AV39GXV1 <= AV22Errors.Count )
            {
               AV21Error = ((SdtGAMError)AV22Errors.Item(AV39GXV1));
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV21Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
               AV39GXV1 = (int)(AV39GXV1+1);
            }
         }
      }

      protected void E162O2( )
      {
         /* 'GenerateKeyGAMRemote' Routine */
         AV12ClientEncryptionKey = Crypto.GetEncryptionKey( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ClientEncryptionKey", AV12ClientEncryptionKey);
      }

      protected void nextLoad( )
      {
      }

      protected void E172O2( )
      {
         /* Load Routine */
      }

      protected void wb_table4_116_2O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(700), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblbuttons_Internalname, tblTblbuttons_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:18px;width:150px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:29px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", bttBtnconfirm_Caption, bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnconfirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclose_Internalname, "", "Close", bttBtnclose_Jsonclick, 5, "Close", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CLOSE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_116_2O2e( true) ;
         }
         else
         {
            wb_table4_116_2O2e( false) ;
         }
      }

      protected void wb_table3_83_2O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblclient3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(750), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblclient3_Internalname, tblTblclient3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:200px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTballowgetuserroles_Internalname, "Can get user roles?", "", "", lblTballowgetuserroles_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClientallowgetuserroles_Internalname, StringUtil.BoolToStr( AV33ClientAllowGetUserRoles), "", "", 1, chkavClientallowgetuserroles.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(88, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:22px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTballowgetuseradditionaldata_Internalname, "Can get user additional data?", "", "", lblTballowgetuseradditionaldata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClientallowgetuseradddata_Internalname, StringUtil.BoolToStr( AV34ClientAllowGetUserAddData), "", "", 1, chkavClientallowgetuseradddata.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(93, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:28px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTblocalloginurl_Internalname, "Local login URL", "", "", lblTblocalloginurl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientlocalloginurl_Internalname, AV32ClientLocalLoginURL, StringUtil.RTrim( context.localUtil.Format( AV32ClientLocalLoginURL, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientlocalloginurl_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbsiteurl_Internalname, "Callback URL", "", "", lblTbsiteurl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientcallbackurl_Internalname, AV35ClientCallbackURL, StringUtil.RTrim( context.localUtil.Format( AV35ClientCallbackURL, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientcallbackurl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavClientcallbackurl_Enabled, 1, "text", "", 80, "chr", 1, "row", 1000, 0, 0, 0, 1, -1, -1, true, "URLString", "left", true, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbclientimageurl_Internalname, "Image URL", "", "", lblTbclientimageurl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientimageurl_Internalname, AV14ClientImageURL, StringUtil.RTrim( context.localUtil.Format( AV14ClientImageURL, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientimageurl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavClientimageurl_Enabled, 1, "text", "", 80, "chr", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgamrprivenckey_Internalname, "Private encription key", "", "", lblTbgamrprivenckey_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientencryptionkey_Internalname, StringUtil.RTrim( AV12ClientEncryptionKey), StringUtil.RTrim( context.localUtil.Format( AV12ClientEncryptionKey, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientencryptionkey_Jsonclick, 0, "Attribute", "", "", "", 1, edtavClientencryptionkey_Enabled, 1, "text", "", 40, "chr", 1, "row", 32, 0, 0, 0, 1, -1, 0, true, "GAMEncryptionKey", "left", true, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtngenkeygamremote_Internalname, "", "Generate Key", bttBtngenkeygamremote_Jsonclick, 5, "Generate Key", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'GENERATEKEYGAMREMOTE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_83_2O2e( true) ;
         }
         else
         {
            wb_table3_83_2O2e( false) ;
         }
      }

      protected void wb_table2_46_2O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(750), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblclient_Internalname, tblTblclient_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:200px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbformtitle_Internalname, "Client Application data", "", "", lblTbformtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:12.0pt; font-weight:normal; font-style:normal;", "Title", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbclientid_Internalname, "Client Id", "", "", lblTbclientid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientid_Internalname, StringUtil.RTrim( AV13ClientId), StringUtil.RTrim( context.localUtil.Format( AV13ClientId, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientid_Jsonclick, 0, "Attribute", "", "", "", 1, edtavClientid_Enabled, 1, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, 0, true, "GAMClientApplicationId", "left", true, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbsecret_Internalname, "Client Secret", "", "", lblTbsecret_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientsecret_Internalname, StringUtil.RTrim( AV16ClientSecret), StringUtil.RTrim( context.localUtil.Format( AV16ClientSecret, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientsecret_Jsonclick, 0, "Attribute", "", "", "", 1, edtavClientsecret_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, 0, true, "GAMClientApplicationSecret", "left", true, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbsinleuseraccess_Internalname, "Single user access?", "", "", lblTbsinleuseraccess_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClientaccessuniquebyuser_Internalname, StringUtil.BoolToStr( AV10ClientAccessUniqueByUser), "", "", 1, chkavClientaccessuniquebyuser.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(65, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbrevoked_Internalname, "Revoked", "", "", lblTbrevoked_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavClientrevoked_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavClientrevoked_Internalname, context.localUtil.TToC( AV15ClientRevoked, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV15ClientRevoked, "99/99/9999 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientrevoked_Jsonclick, 0, "Attribute", "", "", "", 1, edtavClientrevoked_Enabled, 1, "text", "", 16, "chr", 1, "row", 16, 0, 0, 0, 1, -1, 0, true, "GAMDateTime", "right", false, "HLP_GAMExampleEntryApplication.htm");
            GxWebStd.gx_bitmap( context, edtavClientrevoked_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavClientrevoked_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnrevokeallow_Internalname, "", bttBtnrevokeallow_Caption, bttBtnrevokeallow_Jsonclick, 5, "Revoke", "", StyleString, ClassString, bttBtnrevokeallow_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'REVOKE-AUTHORIZE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTballowremoteauth_Internalname, "Allow remote authentication?", "", "", lblTballowremoteauth_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClientallowremoteauth_Internalname, StringUtil.BoolToStr( AV11ClientAllowRemoteAuth), "", "", 1, chkavClientallowremoteauth.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(77, this, 'true', 'false');gx.ajax.executeCliEvent('e182o1_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:15px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_46_2O2e( true) ;
         }
         else
         {
            wb_table2_46_2O2e( false) ;
         }
      }

      protected void wb_table1_2_2O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(750), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblgral_Internalname, tblTblgral_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:200px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbid_Internalname, "Id", "", "", lblTbid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24Id), 12, 0, ",", "")), ((edtavId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavId_Jsonclick, 0, "Attribute", "", "", "", 1, edtavId_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "GAMKeyNumLong", "right", false, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbguid2_Internalname, "GUID", "", "", lblTbguid2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuid_Internalname, StringUtil.RTrim( AV23GUID), StringUtil.RTrim( context.localUtil.Format( AV23GUID, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,12);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuid_Jsonclick, 0, "Attribute", "", "", "", 1, edtavGuid_Enabled, 1, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, 0, true, "GAMGUID", "left", true, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname_Internalname, "Name", "", "", lblTbname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, StringUtil.RTrim( AV26Name), StringUtil.RTrim( context.localUtil.Format( AV26Name, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "Attribute", "", "", "", 1, edtavName_Enabled, 1, "text", "", 80, "chr", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdsc_Internalname, "Description", "", "", lblTbdsc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDsc_Internalname, StringUtil.RTrim( AV20Dsc), StringUtil.RTrim( context.localUtil.Format( AV20Dsc, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDsc_Jsonclick, 0, "Attribute", "", "", "", 1, edtavDsc_Enabled, 1, "text", "", 80, "chr", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbversion_Internalname, "Version", "", "", lblTbversion_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVersion_Internalname, StringUtil.RTrim( AV31Version), StringUtil.RTrim( context.localUtil.Format( AV31Version, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVersion_Jsonclick, 0, "Attribute", "", "", "", 1, edtavVersion_Enabled, 1, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionShort", "left", true, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcompany_Internalname, "Company", "", "", lblTbcompany_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCompany_Internalname, StringUtil.RTrim( AV18Company), StringUtil.RTrim( context.localUtil.Format( AV18Company, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCompany_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCompany_Enabled, 1, "text", "", 80, "chr", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcopyright_Internalname, "Copyright", "", "", lblTbcopyright_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCopyright_Internalname, StringUtil.RTrim( AV19Copyright), StringUtil.RTrim( context.localUtil.Format( AV19Copyright, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCopyright_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCopyright_Enabled, 1, "text", "", 80, "chr", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbreqprm_Internalname, "Require permissions?", "", "", lblTbreqprm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryApplication.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAccessrequirespermission_Internalname, StringUtil.BoolToStr( AV6AccessRequiresPermission), "", "", 1, chkavAccessrequirespermission.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(42, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:15px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2O2e( true) ;
         }
         else
         {
            wb_table1_2_2O2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         Gx_mode = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         AV24Id = Convert.ToInt64(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Id), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA2O2( ) ;
         WS2O2( ) ;
         WE2O2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249785");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282312634");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamexampleentryapplication.js", "?20204282312634");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbid_Internalname = "TBID";
         edtavId_Internalname = "vID";
         lblTbguid2_Internalname = "TBGUID2";
         edtavGuid_Internalname = "vGUID";
         lblTbname_Internalname = "TBNAME";
         edtavName_Internalname = "vNAME";
         lblTbdsc_Internalname = "TBDSC";
         edtavDsc_Internalname = "vDSC";
         lblTbversion_Internalname = "TBVERSION";
         edtavVersion_Internalname = "vVERSION";
         lblTbcompany_Internalname = "TBCOMPANY";
         edtavCompany_Internalname = "vCOMPANY";
         lblTbcopyright_Internalname = "TBCOPYRIGHT";
         edtavCopyright_Internalname = "vCOPYRIGHT";
         lblTbreqprm_Internalname = "TBREQPRM";
         chkavAccessrequirespermission_Internalname = "vACCESSREQUIRESPERMISSION";
         tblTblgral_Internalname = "TBLGRAL";
         lblTbformtitle_Internalname = "TBFORMTITLE";
         lblTbclientid_Internalname = "TBCLIENTID";
         edtavClientid_Internalname = "vCLIENTID";
         lblTbsecret_Internalname = "TBSECRET";
         edtavClientsecret_Internalname = "vCLIENTSECRET";
         lblTbsinleuseraccess_Internalname = "TBSINLEUSERACCESS";
         chkavClientaccessuniquebyuser_Internalname = "vCLIENTACCESSUNIQUEBYUSER";
         lblTbrevoked_Internalname = "TBREVOKED";
         edtavClientrevoked_Internalname = "vCLIENTREVOKED";
         bttBtnrevokeallow_Internalname = "BTNREVOKEALLOW";
         lblTballowremoteauth_Internalname = "TBALLOWREMOTEAUTH";
         chkavClientallowremoteauth_Internalname = "vCLIENTALLOWREMOTEAUTH";
         tblTblclient_Internalname = "TBLCLIENT";
         lblTballowgetuserroles_Internalname = "TBALLOWGETUSERROLES";
         chkavClientallowgetuserroles_Internalname = "vCLIENTALLOWGETUSERROLES";
         lblTballowgetuseradditionaldata_Internalname = "TBALLOWGETUSERADDITIONALDATA";
         chkavClientallowgetuseradddata_Internalname = "vCLIENTALLOWGETUSERADDDATA";
         lblTblocalloginurl_Internalname = "TBLOCALLOGINURL";
         edtavClientlocalloginurl_Internalname = "vCLIENTLOCALLOGINURL";
         lblTbsiteurl_Internalname = "TBSITEURL";
         edtavClientcallbackurl_Internalname = "vCLIENTCALLBACKURL";
         lblTbclientimageurl_Internalname = "TBCLIENTIMAGEURL";
         edtavClientimageurl_Internalname = "vCLIENTIMAGEURL";
         lblTbgamrprivenckey_Internalname = "TBGAMRPRIVENCKEY";
         edtavClientencryptionkey_Internalname = "vCLIENTENCRYPTIONKEY";
         bttBtngenkeygamremote_Internalname = "BTNGENKEYGAMREMOTE";
         tblTblclient3_Internalname = "TBLCLIENT3";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtnclose_Internalname = "BTNCLOSE";
         tblTblbuttons_Internalname = "TBLBUTTONS";
         chkavAutoregisteranomymoususer_Internalname = "vAUTOREGISTERANOMYMOUSUSER";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         edtavCopyright_Jsonclick = "";
         edtavCompany_Jsonclick = "";
         edtavVersion_Jsonclick = "";
         edtavDsc_Jsonclick = "";
         edtavName_Jsonclick = "";
         edtavGuid_Jsonclick = "";
         edtavId_Jsonclick = "";
         edtavId_Enabled = 0;
         bttBtnrevokeallow_Visible = 1;
         edtavClientrevoked_Jsonclick = "";
         edtavClientsecret_Jsonclick = "";
         edtavClientid_Jsonclick = "";
         edtavClientencryptionkey_Jsonclick = "";
         edtavClientimageurl_Jsonclick = "";
         edtavClientcallbackurl_Jsonclick = "";
         edtavClientlocalloginurl_Jsonclick = "";
         bttBtnconfirm_Visible = 1;
         tblTblclient3_Visible = 1;
         bttBtnconfirm_Caption = "Confirmar";
         edtavClientencryptionkey_Enabled = 1;
         edtavClientimageurl_Enabled = 1;
         edtavClientcallbackurl_Enabled = 1;
         chkavClientallowgetuseradddata.Enabled = 1;
         chkavClientallowgetuserroles.Enabled = 1;
         chkavClientallowremoteauth.Enabled = 1;
         edtavClientrevoked_Enabled = 1;
         chkavClientaccessuniquebyuser.Enabled = 1;
         edtavClientsecret_Enabled = 1;
         edtavClientid_Enabled = 1;
         chkavAccessrequirespermission.Enabled = 1;
         edtavCompany_Enabled = 1;
         edtavCopyright_Enabled = 1;
         edtavVersion_Enabled = 1;
         edtavDsc_Enabled = 1;
         edtavName_Enabled = 1;
         edtavGuid_Enabled = 1;
         bttBtnrevokeallow_Caption = "Revoke";
         chkavAutoregisteranomymoususer.Caption = "";
         chkavClientallowgetuseradddata.Caption = "";
         chkavClientallowgetuserroles.Caption = "";
         chkavClientallowremoteauth.Caption = "";
         chkavClientaccessuniquebyuser.Caption = "";
         chkavAccessrequirespermission.Caption = "";
         chkavAutoregisteranomymoususer.Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Application";
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOGx_mode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV23GUID = "";
         AV26Name = "";
         AV20Dsc = "";
         AV31Version = "";
         AV18Company = "";
         AV19Copyright = "";
         AV13ClientId = "";
         AV16ClientSecret = "";
         AV15ClientRevoked = (DateTime)(DateTime.MinValue);
         AV32ClientLocalLoginURL = "";
         AV35ClientCallbackURL = "";
         AV14ClientImageURL = "";
         AV12ClientEncryptionKey = "";
         AV30User = new SdtGAMUser(context);
         AV7Application = new SdtGAMApplication(context);
         AV22Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV21Error = new SdtGAMError(context);
         sStyleString = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtnclose_Jsonclick = "";
         lblTballowgetuserroles_Jsonclick = "";
         lblTballowgetuseradditionaldata_Jsonclick = "";
         lblTblocalloginurl_Jsonclick = "";
         lblTbsiteurl_Jsonclick = "";
         lblTbclientimageurl_Jsonclick = "";
         lblTbgamrprivenckey_Jsonclick = "";
         bttBtngenkeygamremote_Jsonclick = "";
         lblTbformtitle_Jsonclick = "";
         lblTbclientid_Jsonclick = "";
         lblTbsecret_Jsonclick = "";
         lblTbsinleuseraccess_Jsonclick = "";
         lblTbrevoked_Jsonclick = "";
         bttBtnrevokeallow_Jsonclick = "";
         lblTballowremoteauth_Jsonclick = "";
         lblTbid_Jsonclick = "";
         lblTbguid2_Jsonclick = "";
         lblTbname_Jsonclick = "";
         lblTbdsc_Jsonclick = "";
         lblTbversion_Jsonclick = "";
         lblTbcompany_Jsonclick = "";
         lblTbcopyright_Jsonclick = "";
         lblTbreqprm_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexampleentryapplication__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         edtavGuid_Enabled = 0;
         edtavClientrevoked_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int edtavId_Enabled ;
      private int edtavGuid_Enabled ;
      private int edtavClientrevoked_Enabled ;
      private int edtavName_Enabled ;
      private int edtavDsc_Enabled ;
      private int edtavVersion_Enabled ;
      private int edtavCopyright_Enabled ;
      private int edtavCompany_Enabled ;
      private int edtavClientid_Enabled ;
      private int edtavClientsecret_Enabled ;
      private int edtavClientcallbackurl_Enabled ;
      private int edtavClientimageurl_Enabled ;
      private int edtavClientencryptionkey_Enabled ;
      private int tblTblclient3_Visible ;
      private int bttBtnconfirm_Visible ;
      private int bttBtnrevokeallow_Visible ;
      private int AV39GXV1 ;
      private int idxLst ;
      private long AV24Id ;
      private long wcpOAV24Id ;
      private String Gx_mode ;
      private String wcpOGx_mode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavAutoregisteranomymoususer_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavAccessrequirespermission_Internalname ;
      private String chkavClientaccessuniquebyuser_Internalname ;
      private String chkavClientallowremoteauth_Internalname ;
      private String chkavClientallowgetuserroles_Internalname ;
      private String chkavClientallowgetuseradddata_Internalname ;
      private String edtavGuid_Internalname ;
      private String edtavId_Internalname ;
      private String edtavClientrevoked_Internalname ;
      private String AV23GUID ;
      private String AV26Name ;
      private String edtavName_Internalname ;
      private String AV20Dsc ;
      private String edtavDsc_Internalname ;
      private String AV31Version ;
      private String edtavVersion_Internalname ;
      private String AV18Company ;
      private String edtavCompany_Internalname ;
      private String AV19Copyright ;
      private String edtavCopyright_Internalname ;
      private String AV13ClientId ;
      private String edtavClientid_Internalname ;
      private String AV16ClientSecret ;
      private String edtavClientsecret_Internalname ;
      private String edtavClientlocalloginurl_Internalname ;
      private String edtavClientcallbackurl_Internalname ;
      private String edtavClientimageurl_Internalname ;
      private String AV12ClientEncryptionKey ;
      private String edtavClientencryptionkey_Internalname ;
      private String bttBtnrevokeallow_Caption ;
      private String bttBtnrevokeallow_Internalname ;
      private String bttBtnconfirm_Caption ;
      private String bttBtnconfirm_Internalname ;
      private String tblTblclient3_Internalname ;
      private String sStyleString ;
      private String tblTblbuttons_Internalname ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtnclose_Internalname ;
      private String bttBtnclose_Jsonclick ;
      private String lblTballowgetuserroles_Internalname ;
      private String lblTballowgetuserroles_Jsonclick ;
      private String lblTballowgetuseradditionaldata_Internalname ;
      private String lblTballowgetuseradditionaldata_Jsonclick ;
      private String lblTblocalloginurl_Internalname ;
      private String lblTblocalloginurl_Jsonclick ;
      private String edtavClientlocalloginurl_Jsonclick ;
      private String lblTbsiteurl_Internalname ;
      private String lblTbsiteurl_Jsonclick ;
      private String edtavClientcallbackurl_Jsonclick ;
      private String lblTbclientimageurl_Internalname ;
      private String lblTbclientimageurl_Jsonclick ;
      private String edtavClientimageurl_Jsonclick ;
      private String lblTbgamrprivenckey_Internalname ;
      private String lblTbgamrprivenckey_Jsonclick ;
      private String edtavClientencryptionkey_Jsonclick ;
      private String bttBtngenkeygamremote_Internalname ;
      private String bttBtngenkeygamremote_Jsonclick ;
      private String tblTblclient_Internalname ;
      private String lblTbformtitle_Internalname ;
      private String lblTbformtitle_Jsonclick ;
      private String lblTbclientid_Internalname ;
      private String lblTbclientid_Jsonclick ;
      private String edtavClientid_Jsonclick ;
      private String lblTbsecret_Internalname ;
      private String lblTbsecret_Jsonclick ;
      private String edtavClientsecret_Jsonclick ;
      private String lblTbsinleuseraccess_Internalname ;
      private String lblTbsinleuseraccess_Jsonclick ;
      private String lblTbrevoked_Internalname ;
      private String lblTbrevoked_Jsonclick ;
      private String edtavClientrevoked_Jsonclick ;
      private String bttBtnrevokeallow_Jsonclick ;
      private String lblTballowremoteauth_Internalname ;
      private String lblTballowremoteauth_Jsonclick ;
      private String tblTblgral_Internalname ;
      private String lblTbid_Internalname ;
      private String lblTbid_Jsonclick ;
      private String edtavId_Jsonclick ;
      private String lblTbguid2_Internalname ;
      private String lblTbguid2_Jsonclick ;
      private String edtavGuid_Jsonclick ;
      private String lblTbname_Internalname ;
      private String lblTbname_Jsonclick ;
      private String edtavName_Jsonclick ;
      private String lblTbdsc_Internalname ;
      private String lblTbdsc_Jsonclick ;
      private String edtavDsc_Jsonclick ;
      private String lblTbversion_Internalname ;
      private String lblTbversion_Jsonclick ;
      private String edtavVersion_Jsonclick ;
      private String lblTbcompany_Internalname ;
      private String lblTbcompany_Jsonclick ;
      private String edtavCompany_Jsonclick ;
      private String lblTbcopyright_Internalname ;
      private String lblTbcopyright_Jsonclick ;
      private String edtavCopyright_Jsonclick ;
      private String lblTbreqprm_Internalname ;
      private String lblTbreqprm_Jsonclick ;
      private DateTime AV15ClientRevoked ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool AV9AutoRegisterAnomymousUser ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV6AccessRequiresPermission ;
      private bool AV10ClientAccessUniqueByUser ;
      private bool AV11ClientAllowRemoteAuth ;
      private bool AV33ClientAllowGetUserRoles ;
      private bool AV34ClientAllowGetUserAddData ;
      private bool returnInSub ;
      private bool AV5isOk ;
      private String AV32ClientLocalLoginURL ;
      private String AV35ClientCallbackURL ;
      private String AV14ClientImageURL ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Gx_mode ;
      private long aP1_Id ;
      private GXCheckbox chkavAccessrequirespermission ;
      private GXCheckbox chkavClientaccessuniquebyuser ;
      private GXCheckbox chkavClientallowremoteauth ;
      private GXCheckbox chkavClientallowgetuserroles ;
      private GXCheckbox chkavClientallowgetuseradddata ;
      private GXCheckbox chkavAutoregisteranomymoususer ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV22Errors ;
      private GXWebForm Form ;
      private SdtGAMApplication AV7Application ;
      private SdtGAMError AV21Error ;
      private SdtGAMUser AV30User ;
   }

   public class gamexampleentryapplication__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
