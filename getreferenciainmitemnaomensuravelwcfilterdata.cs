/*
               File: GetReferenciaINMItemNaoMensuravelWCFilterData
        Description: Get Referencia INMItem Nao Mensuravel WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:2.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getreferenciainmitemnaomensuravelwcfilterdata : GXProcedure
   {
      public getreferenciainmitemnaomensuravelwcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getreferenciainmitemnaomensuravelwcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
         return AV32OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getreferenciainmitemnaomensuravelwcfilterdata objgetreferenciainmitemnaomensuravelwcfilterdata;
         objgetreferenciainmitemnaomensuravelwcfilterdata = new getreferenciainmitemnaomensuravelwcfilterdata();
         objgetreferenciainmitemnaomensuravelwcfilterdata.AV23DDOName = aP0_DDOName;
         objgetreferenciainmitemnaomensuravelwcfilterdata.AV21SearchTxt = aP1_SearchTxt;
         objgetreferenciainmitemnaomensuravelwcfilterdata.AV22SearchTxtTo = aP2_SearchTxtTo;
         objgetreferenciainmitemnaomensuravelwcfilterdata.AV27OptionsJson = "" ;
         objgetreferenciainmitemnaomensuravelwcfilterdata.AV30OptionsDescJson = "" ;
         objgetreferenciainmitemnaomensuravelwcfilterdata.AV32OptionIndexesJson = "" ;
         objgetreferenciainmitemnaomensuravelwcfilterdata.context.SetSubmitInitialConfig(context);
         objgetreferenciainmitemnaomensuravelwcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetreferenciainmitemnaomensuravelwcfilterdata);
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getreferenciainmitemnaomensuravelwcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV26Options = (IGxCollection)(new GxSimpleCollection());
         AV29OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV31OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_ITEMNAOMENSURAVEL_CODIGO") == 0 )
         {
            /* Execute user subroutine: 'LOADITEMNAOMENSURAVEL_CODIGOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADITEMNAOMENSURAVEL_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_ITEMNAOMENSURAVEL_REFERENCIA") == 0 )
         {
            /* Execute user subroutine: 'LOADITEMNAOMENSURAVEL_REFERENCIAOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV27OptionsJson = AV26Options.ToJSonString(false);
         AV30OptionsDescJson = AV29OptionsDesc.ToJSonString(false);
         AV32OptionIndexesJson = AV31OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV34Session.Get("ReferenciaINMItemNaoMensuravelWCGridState"), "") == 0 )
         {
            AV36GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ReferenciaINMItemNaoMensuravelWCGridState"), "");
         }
         else
         {
            AV36GridState.FromXml(AV34Session.Get("ReferenciaINMItemNaoMensuravelWCGridState"), "");
         }
         AV59GXV1 = 1;
         while ( AV59GXV1 <= AV36GridState.gxTpr_Filtervalues.Count )
         {
            AV37GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV36GridState.gxTpr_Filtervalues.Item(AV59GXV1));
            if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_CODIGO") == 0 )
            {
               AV10TFItemNaoMensuravel_Codigo = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_CODIGO_SEL") == 0 )
            {
               AV11TFItemNaoMensuravel_Codigo_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_DESCRICAO") == 0 )
            {
               AV12TFItemNaoMensuravel_Descricao = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_DESCRICAO_SEL") == 0 )
            {
               AV13TFItemNaoMensuravel_Descricao_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_REFERENCIA") == 0 )
            {
               AV14TFItemNaoMensuravel_Referencia = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_REFERENCIA_SEL") == 0 )
            {
               AV15TFItemNaoMensuravel_Referencia_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_VALOR") == 0 )
            {
               AV16TFItemNaoMensuravel_Valor = NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, ".");
               AV17TFItemNaoMensuravel_Valor_To = NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_TIPO_SEL") == 0 )
            {
               AV18TFItemNaoMensuravel_Tipo_SelsJson = AV37GridStateFilterValue.gxTpr_Value;
               AV19TFItemNaoMensuravel_Tipo_Sels.FromJSonString(AV18TFItemNaoMensuravel_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_ATIVO_SEL") == 0 )
            {
               AV20TFItemNaoMensuravel_Ativo_Sel = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "PARM_&REFERENCIAINM_CODIGO") == 0 )
            {
               AV56ReferenciaINM_Codigo = (int)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            AV59GXV1 = (int)(AV59GXV1+1);
         }
         if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(1));
            AV39DynamicFiltersSelector1 = AV38GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 )
            {
               AV40DynamicFiltersOperator1 = AV38GridStateDynamicFilter.gxTpr_Operator;
               AV41ItemNaoMensuravel_Codigo1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
            {
               AV40DynamicFiltersOperator1 = AV38GridStateDynamicFilter.gxTpr_Operator;
               AV42ItemNaoMensuravel_Descricao1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_TIPO") == 0 )
            {
               AV43ItemNaoMensuravel_Tipo1 = (short)(NumberUtil.Val( AV38GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV44DynamicFiltersEnabled2 = true;
               AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(2));
               AV45DynamicFiltersSelector2 = AV38GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 )
               {
                  AV46DynamicFiltersOperator2 = AV38GridStateDynamicFilter.gxTpr_Operator;
                  AV47ItemNaoMensuravel_Codigo2 = AV38GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
               {
                  AV46DynamicFiltersOperator2 = AV38GridStateDynamicFilter.gxTpr_Operator;
                  AV48ItemNaoMensuravel_Descricao2 = AV38GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_TIPO") == 0 )
               {
                  AV49ItemNaoMensuravel_Tipo2 = (short)(NumberUtil.Val( AV38GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV50DynamicFiltersEnabled3 = true;
                  AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(3));
                  AV51DynamicFiltersSelector3 = AV38GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 )
                  {
                     AV52DynamicFiltersOperator3 = AV38GridStateDynamicFilter.gxTpr_Operator;
                     AV53ItemNaoMensuravel_Codigo3 = AV38GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
                  {
                     AV52DynamicFiltersOperator3 = AV38GridStateDynamicFilter.gxTpr_Operator;
                     AV54ItemNaoMensuravel_Descricao3 = AV38GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_TIPO") == 0 )
                  {
                     AV55ItemNaoMensuravel_Tipo3 = (short)(NumberUtil.Val( AV38GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADITEMNAOMENSURAVEL_CODIGOOPTIONS' Routine */
         AV10TFItemNaoMensuravel_Codigo = AV21SearchTxt;
         AV11TFItemNaoMensuravel_Codigo_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A717ItemNaoMensuravel_Tipo ,
                                              AV19TFItemNaoMensuravel_Tipo_Sels ,
                                              AV39DynamicFiltersSelector1 ,
                                              AV40DynamicFiltersOperator1 ,
                                              AV41ItemNaoMensuravel_Codigo1 ,
                                              AV42ItemNaoMensuravel_Descricao1 ,
                                              AV43ItemNaoMensuravel_Tipo1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46DynamicFiltersOperator2 ,
                                              AV47ItemNaoMensuravel_Codigo2 ,
                                              AV48ItemNaoMensuravel_Descricao2 ,
                                              AV49ItemNaoMensuravel_Tipo2 ,
                                              AV50DynamicFiltersEnabled3 ,
                                              AV51DynamicFiltersSelector3 ,
                                              AV52DynamicFiltersOperator3 ,
                                              AV53ItemNaoMensuravel_Codigo3 ,
                                              AV54ItemNaoMensuravel_Descricao3 ,
                                              AV55ItemNaoMensuravel_Tipo3 ,
                                              AV11TFItemNaoMensuravel_Codigo_Sel ,
                                              AV10TFItemNaoMensuravel_Codigo ,
                                              AV13TFItemNaoMensuravel_Descricao_Sel ,
                                              AV12TFItemNaoMensuravel_Descricao ,
                                              AV15TFItemNaoMensuravel_Referencia_Sel ,
                                              AV14TFItemNaoMensuravel_Referencia ,
                                              AV16TFItemNaoMensuravel_Valor ,
                                              AV17TFItemNaoMensuravel_Valor_To ,
                                              AV19TFItemNaoMensuravel_Tipo_Sels.Count ,
                                              AV20TFItemNaoMensuravel_Ativo_Sel ,
                                              A715ItemNaoMensuravel_Codigo ,
                                              A714ItemNaoMensuravel_Descricao ,
                                              A1804ItemNaoMensuravel_Referencia ,
                                              A719ItemNaoMensuravel_Valor ,
                                              A716ItemNaoMensuravel_Ativo ,
                                              AV56ReferenciaINM_Codigo ,
                                              A709ReferenciaINM_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV41ItemNaoMensuravel_Codigo1 = StringUtil.PadR( StringUtil.RTrim( AV41ItemNaoMensuravel_Codigo1), 20, "%");
         lV41ItemNaoMensuravel_Codigo1 = StringUtil.PadR( StringUtil.RTrim( AV41ItemNaoMensuravel_Codigo1), 20, "%");
         lV42ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV42ItemNaoMensuravel_Descricao1), "%", "");
         lV42ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV42ItemNaoMensuravel_Descricao1), "%", "");
         lV47ItemNaoMensuravel_Codigo2 = StringUtil.PadR( StringUtil.RTrim( AV47ItemNaoMensuravel_Codigo2), 20, "%");
         lV47ItemNaoMensuravel_Codigo2 = StringUtil.PadR( StringUtil.RTrim( AV47ItemNaoMensuravel_Codigo2), 20, "%");
         lV48ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_Descricao2), "%", "");
         lV48ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_Descricao2), "%", "");
         lV53ItemNaoMensuravel_Codigo3 = StringUtil.PadR( StringUtil.RTrim( AV53ItemNaoMensuravel_Codigo3), 20, "%");
         lV53ItemNaoMensuravel_Codigo3 = StringUtil.PadR( StringUtil.RTrim( AV53ItemNaoMensuravel_Codigo3), 20, "%");
         lV54ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_Descricao3), "%", "");
         lV54ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_Descricao3), "%", "");
         lV10TFItemNaoMensuravel_Codigo = StringUtil.PadR( StringUtil.RTrim( AV10TFItemNaoMensuravel_Codigo), 20, "%");
         lV12TFItemNaoMensuravel_Descricao = StringUtil.Concat( StringUtil.RTrim( AV12TFItemNaoMensuravel_Descricao), "%", "");
         lV14TFItemNaoMensuravel_Referencia = StringUtil.PadR( StringUtil.RTrim( AV14TFItemNaoMensuravel_Referencia), 15, "%");
         /* Using cursor P00SC2 */
         pr_default.execute(0, new Object[] {AV56ReferenciaINM_Codigo, lV41ItemNaoMensuravel_Codigo1, lV41ItemNaoMensuravel_Codigo1, lV42ItemNaoMensuravel_Descricao1, lV42ItemNaoMensuravel_Descricao1, AV43ItemNaoMensuravel_Tipo1, lV47ItemNaoMensuravel_Codigo2, lV47ItemNaoMensuravel_Codigo2, lV48ItemNaoMensuravel_Descricao2, lV48ItemNaoMensuravel_Descricao2, AV49ItemNaoMensuravel_Tipo2, lV53ItemNaoMensuravel_Codigo3, lV53ItemNaoMensuravel_Codigo3, lV54ItemNaoMensuravel_Descricao3, lV54ItemNaoMensuravel_Descricao3, AV55ItemNaoMensuravel_Tipo3, lV10TFItemNaoMensuravel_Codigo, AV11TFItemNaoMensuravel_Codigo_Sel, lV12TFItemNaoMensuravel_Descricao, AV13TFItemNaoMensuravel_Descricao_Sel, lV14TFItemNaoMensuravel_Referencia, AV15TFItemNaoMensuravel_Referencia_Sel, AV16TFItemNaoMensuravel_Valor, AV17TFItemNaoMensuravel_Valor_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKSC2 = false;
            A709ReferenciaINM_Codigo = P00SC2_A709ReferenciaINM_Codigo[0];
            A715ItemNaoMensuravel_Codigo = P00SC2_A715ItemNaoMensuravel_Codigo[0];
            A716ItemNaoMensuravel_Ativo = P00SC2_A716ItemNaoMensuravel_Ativo[0];
            A719ItemNaoMensuravel_Valor = P00SC2_A719ItemNaoMensuravel_Valor[0];
            A1804ItemNaoMensuravel_Referencia = P00SC2_A1804ItemNaoMensuravel_Referencia[0];
            n1804ItemNaoMensuravel_Referencia = P00SC2_n1804ItemNaoMensuravel_Referencia[0];
            A717ItemNaoMensuravel_Tipo = P00SC2_A717ItemNaoMensuravel_Tipo[0];
            A714ItemNaoMensuravel_Descricao = P00SC2_A714ItemNaoMensuravel_Descricao[0];
            A718ItemNaoMensuravel_AreaTrabalhoCod = P00SC2_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            AV33count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00SC2_A709ReferenciaINM_Codigo[0] == A709ReferenciaINM_Codigo ) && ( StringUtil.StrCmp(P00SC2_A715ItemNaoMensuravel_Codigo[0], A715ItemNaoMensuravel_Codigo) == 0 ) )
            {
               BRKSC2 = false;
               A718ItemNaoMensuravel_AreaTrabalhoCod = P00SC2_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               AV33count = (long)(AV33count+1);
               BRKSC2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A715ItemNaoMensuravel_Codigo)) )
            {
               AV25Option = A715ItemNaoMensuravel_Codigo;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSC2 )
            {
               BRKSC2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADITEMNAOMENSURAVEL_DESCRICAOOPTIONS' Routine */
         AV12TFItemNaoMensuravel_Descricao = AV21SearchTxt;
         AV13TFItemNaoMensuravel_Descricao_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A717ItemNaoMensuravel_Tipo ,
                                              AV19TFItemNaoMensuravel_Tipo_Sels ,
                                              AV39DynamicFiltersSelector1 ,
                                              AV40DynamicFiltersOperator1 ,
                                              AV41ItemNaoMensuravel_Codigo1 ,
                                              AV42ItemNaoMensuravel_Descricao1 ,
                                              AV43ItemNaoMensuravel_Tipo1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46DynamicFiltersOperator2 ,
                                              AV47ItemNaoMensuravel_Codigo2 ,
                                              AV48ItemNaoMensuravel_Descricao2 ,
                                              AV49ItemNaoMensuravel_Tipo2 ,
                                              AV50DynamicFiltersEnabled3 ,
                                              AV51DynamicFiltersSelector3 ,
                                              AV52DynamicFiltersOperator3 ,
                                              AV53ItemNaoMensuravel_Codigo3 ,
                                              AV54ItemNaoMensuravel_Descricao3 ,
                                              AV55ItemNaoMensuravel_Tipo3 ,
                                              AV11TFItemNaoMensuravel_Codigo_Sel ,
                                              AV10TFItemNaoMensuravel_Codigo ,
                                              AV13TFItemNaoMensuravel_Descricao_Sel ,
                                              AV12TFItemNaoMensuravel_Descricao ,
                                              AV15TFItemNaoMensuravel_Referencia_Sel ,
                                              AV14TFItemNaoMensuravel_Referencia ,
                                              AV16TFItemNaoMensuravel_Valor ,
                                              AV17TFItemNaoMensuravel_Valor_To ,
                                              AV19TFItemNaoMensuravel_Tipo_Sels.Count ,
                                              AV20TFItemNaoMensuravel_Ativo_Sel ,
                                              A715ItemNaoMensuravel_Codigo ,
                                              A714ItemNaoMensuravel_Descricao ,
                                              A1804ItemNaoMensuravel_Referencia ,
                                              A719ItemNaoMensuravel_Valor ,
                                              A716ItemNaoMensuravel_Ativo ,
                                              AV56ReferenciaINM_Codigo ,
                                              A709ReferenciaINM_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV41ItemNaoMensuravel_Codigo1 = StringUtil.PadR( StringUtil.RTrim( AV41ItemNaoMensuravel_Codigo1), 20, "%");
         lV41ItemNaoMensuravel_Codigo1 = StringUtil.PadR( StringUtil.RTrim( AV41ItemNaoMensuravel_Codigo1), 20, "%");
         lV42ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV42ItemNaoMensuravel_Descricao1), "%", "");
         lV42ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV42ItemNaoMensuravel_Descricao1), "%", "");
         lV47ItemNaoMensuravel_Codigo2 = StringUtil.PadR( StringUtil.RTrim( AV47ItemNaoMensuravel_Codigo2), 20, "%");
         lV47ItemNaoMensuravel_Codigo2 = StringUtil.PadR( StringUtil.RTrim( AV47ItemNaoMensuravel_Codigo2), 20, "%");
         lV48ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_Descricao2), "%", "");
         lV48ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_Descricao2), "%", "");
         lV53ItemNaoMensuravel_Codigo3 = StringUtil.PadR( StringUtil.RTrim( AV53ItemNaoMensuravel_Codigo3), 20, "%");
         lV53ItemNaoMensuravel_Codigo3 = StringUtil.PadR( StringUtil.RTrim( AV53ItemNaoMensuravel_Codigo3), 20, "%");
         lV54ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_Descricao3), "%", "");
         lV54ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_Descricao3), "%", "");
         lV10TFItemNaoMensuravel_Codigo = StringUtil.PadR( StringUtil.RTrim( AV10TFItemNaoMensuravel_Codigo), 20, "%");
         lV12TFItemNaoMensuravel_Descricao = StringUtil.Concat( StringUtil.RTrim( AV12TFItemNaoMensuravel_Descricao), "%", "");
         lV14TFItemNaoMensuravel_Referencia = StringUtil.PadR( StringUtil.RTrim( AV14TFItemNaoMensuravel_Referencia), 15, "%");
         /* Using cursor P00SC3 */
         pr_default.execute(1, new Object[] {AV56ReferenciaINM_Codigo, lV41ItemNaoMensuravel_Codigo1, lV41ItemNaoMensuravel_Codigo1, lV42ItemNaoMensuravel_Descricao1, lV42ItemNaoMensuravel_Descricao1, AV43ItemNaoMensuravel_Tipo1, lV47ItemNaoMensuravel_Codigo2, lV47ItemNaoMensuravel_Codigo2, lV48ItemNaoMensuravel_Descricao2, lV48ItemNaoMensuravel_Descricao2, AV49ItemNaoMensuravel_Tipo2, lV53ItemNaoMensuravel_Codigo3, lV53ItemNaoMensuravel_Codigo3, lV54ItemNaoMensuravel_Descricao3, lV54ItemNaoMensuravel_Descricao3, AV55ItemNaoMensuravel_Tipo3, lV10TFItemNaoMensuravel_Codigo, AV11TFItemNaoMensuravel_Codigo_Sel, lV12TFItemNaoMensuravel_Descricao, AV13TFItemNaoMensuravel_Descricao_Sel, lV14TFItemNaoMensuravel_Referencia, AV15TFItemNaoMensuravel_Referencia_Sel, AV16TFItemNaoMensuravel_Valor, AV17TFItemNaoMensuravel_Valor_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKSC4 = false;
            A709ReferenciaINM_Codigo = P00SC3_A709ReferenciaINM_Codigo[0];
            A714ItemNaoMensuravel_Descricao = P00SC3_A714ItemNaoMensuravel_Descricao[0];
            A716ItemNaoMensuravel_Ativo = P00SC3_A716ItemNaoMensuravel_Ativo[0];
            A719ItemNaoMensuravel_Valor = P00SC3_A719ItemNaoMensuravel_Valor[0];
            A1804ItemNaoMensuravel_Referencia = P00SC3_A1804ItemNaoMensuravel_Referencia[0];
            n1804ItemNaoMensuravel_Referencia = P00SC3_n1804ItemNaoMensuravel_Referencia[0];
            A717ItemNaoMensuravel_Tipo = P00SC3_A717ItemNaoMensuravel_Tipo[0];
            A715ItemNaoMensuravel_Codigo = P00SC3_A715ItemNaoMensuravel_Codigo[0];
            A718ItemNaoMensuravel_AreaTrabalhoCod = P00SC3_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            AV33count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00SC3_A709ReferenciaINM_Codigo[0] == A709ReferenciaINM_Codigo ) && ( StringUtil.StrCmp(P00SC3_A714ItemNaoMensuravel_Descricao[0], A714ItemNaoMensuravel_Descricao) == 0 ) )
            {
               BRKSC4 = false;
               A715ItemNaoMensuravel_Codigo = P00SC3_A715ItemNaoMensuravel_Codigo[0];
               A718ItemNaoMensuravel_AreaTrabalhoCod = P00SC3_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               AV33count = (long)(AV33count+1);
               BRKSC4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A714ItemNaoMensuravel_Descricao)) )
            {
               AV25Option = A714ItemNaoMensuravel_Descricao;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSC4 )
            {
               BRKSC4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADITEMNAOMENSURAVEL_REFERENCIAOPTIONS' Routine */
         AV14TFItemNaoMensuravel_Referencia = AV21SearchTxt;
         AV15TFItemNaoMensuravel_Referencia_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A717ItemNaoMensuravel_Tipo ,
                                              AV19TFItemNaoMensuravel_Tipo_Sels ,
                                              AV39DynamicFiltersSelector1 ,
                                              AV40DynamicFiltersOperator1 ,
                                              AV41ItemNaoMensuravel_Codigo1 ,
                                              AV42ItemNaoMensuravel_Descricao1 ,
                                              AV43ItemNaoMensuravel_Tipo1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46DynamicFiltersOperator2 ,
                                              AV47ItemNaoMensuravel_Codigo2 ,
                                              AV48ItemNaoMensuravel_Descricao2 ,
                                              AV49ItemNaoMensuravel_Tipo2 ,
                                              AV50DynamicFiltersEnabled3 ,
                                              AV51DynamicFiltersSelector3 ,
                                              AV52DynamicFiltersOperator3 ,
                                              AV53ItemNaoMensuravel_Codigo3 ,
                                              AV54ItemNaoMensuravel_Descricao3 ,
                                              AV55ItemNaoMensuravel_Tipo3 ,
                                              AV11TFItemNaoMensuravel_Codigo_Sel ,
                                              AV10TFItemNaoMensuravel_Codigo ,
                                              AV13TFItemNaoMensuravel_Descricao_Sel ,
                                              AV12TFItemNaoMensuravel_Descricao ,
                                              AV15TFItemNaoMensuravel_Referencia_Sel ,
                                              AV14TFItemNaoMensuravel_Referencia ,
                                              AV16TFItemNaoMensuravel_Valor ,
                                              AV17TFItemNaoMensuravel_Valor_To ,
                                              AV19TFItemNaoMensuravel_Tipo_Sels.Count ,
                                              AV20TFItemNaoMensuravel_Ativo_Sel ,
                                              A715ItemNaoMensuravel_Codigo ,
                                              A714ItemNaoMensuravel_Descricao ,
                                              A1804ItemNaoMensuravel_Referencia ,
                                              A719ItemNaoMensuravel_Valor ,
                                              A716ItemNaoMensuravel_Ativo ,
                                              AV56ReferenciaINM_Codigo ,
                                              A709ReferenciaINM_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV41ItemNaoMensuravel_Codigo1 = StringUtil.PadR( StringUtil.RTrim( AV41ItemNaoMensuravel_Codigo1), 20, "%");
         lV41ItemNaoMensuravel_Codigo1 = StringUtil.PadR( StringUtil.RTrim( AV41ItemNaoMensuravel_Codigo1), 20, "%");
         lV42ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV42ItemNaoMensuravel_Descricao1), "%", "");
         lV42ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV42ItemNaoMensuravel_Descricao1), "%", "");
         lV47ItemNaoMensuravel_Codigo2 = StringUtil.PadR( StringUtil.RTrim( AV47ItemNaoMensuravel_Codigo2), 20, "%");
         lV47ItemNaoMensuravel_Codigo2 = StringUtil.PadR( StringUtil.RTrim( AV47ItemNaoMensuravel_Codigo2), 20, "%");
         lV48ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_Descricao2), "%", "");
         lV48ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_Descricao2), "%", "");
         lV53ItemNaoMensuravel_Codigo3 = StringUtil.PadR( StringUtil.RTrim( AV53ItemNaoMensuravel_Codigo3), 20, "%");
         lV53ItemNaoMensuravel_Codigo3 = StringUtil.PadR( StringUtil.RTrim( AV53ItemNaoMensuravel_Codigo3), 20, "%");
         lV54ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_Descricao3), "%", "");
         lV54ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_Descricao3), "%", "");
         lV10TFItemNaoMensuravel_Codigo = StringUtil.PadR( StringUtil.RTrim( AV10TFItemNaoMensuravel_Codigo), 20, "%");
         lV12TFItemNaoMensuravel_Descricao = StringUtil.Concat( StringUtil.RTrim( AV12TFItemNaoMensuravel_Descricao), "%", "");
         lV14TFItemNaoMensuravel_Referencia = StringUtil.PadR( StringUtil.RTrim( AV14TFItemNaoMensuravel_Referencia), 15, "%");
         /* Using cursor P00SC4 */
         pr_default.execute(2, new Object[] {AV56ReferenciaINM_Codigo, lV41ItemNaoMensuravel_Codigo1, lV41ItemNaoMensuravel_Codigo1, lV42ItemNaoMensuravel_Descricao1, lV42ItemNaoMensuravel_Descricao1, AV43ItemNaoMensuravel_Tipo1, lV47ItemNaoMensuravel_Codigo2, lV47ItemNaoMensuravel_Codigo2, lV48ItemNaoMensuravel_Descricao2, lV48ItemNaoMensuravel_Descricao2, AV49ItemNaoMensuravel_Tipo2, lV53ItemNaoMensuravel_Codigo3, lV53ItemNaoMensuravel_Codigo3, lV54ItemNaoMensuravel_Descricao3, lV54ItemNaoMensuravel_Descricao3, AV55ItemNaoMensuravel_Tipo3, lV10TFItemNaoMensuravel_Codigo, AV11TFItemNaoMensuravel_Codigo_Sel, lV12TFItemNaoMensuravel_Descricao, AV13TFItemNaoMensuravel_Descricao_Sel, lV14TFItemNaoMensuravel_Referencia, AV15TFItemNaoMensuravel_Referencia_Sel, AV16TFItemNaoMensuravel_Valor, AV17TFItemNaoMensuravel_Valor_To});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKSC6 = false;
            A709ReferenciaINM_Codigo = P00SC4_A709ReferenciaINM_Codigo[0];
            A1804ItemNaoMensuravel_Referencia = P00SC4_A1804ItemNaoMensuravel_Referencia[0];
            n1804ItemNaoMensuravel_Referencia = P00SC4_n1804ItemNaoMensuravel_Referencia[0];
            A716ItemNaoMensuravel_Ativo = P00SC4_A716ItemNaoMensuravel_Ativo[0];
            A719ItemNaoMensuravel_Valor = P00SC4_A719ItemNaoMensuravel_Valor[0];
            A717ItemNaoMensuravel_Tipo = P00SC4_A717ItemNaoMensuravel_Tipo[0];
            A714ItemNaoMensuravel_Descricao = P00SC4_A714ItemNaoMensuravel_Descricao[0];
            A715ItemNaoMensuravel_Codigo = P00SC4_A715ItemNaoMensuravel_Codigo[0];
            A718ItemNaoMensuravel_AreaTrabalhoCod = P00SC4_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            AV33count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00SC4_A709ReferenciaINM_Codigo[0] == A709ReferenciaINM_Codigo ) && ( StringUtil.StrCmp(P00SC4_A1804ItemNaoMensuravel_Referencia[0], A1804ItemNaoMensuravel_Referencia) == 0 ) )
            {
               BRKSC6 = false;
               A715ItemNaoMensuravel_Codigo = P00SC4_A715ItemNaoMensuravel_Codigo[0];
               A718ItemNaoMensuravel_AreaTrabalhoCod = P00SC4_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               AV33count = (long)(AV33count+1);
               BRKSC6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1804ItemNaoMensuravel_Referencia)) )
            {
               AV25Option = A1804ItemNaoMensuravel_Referencia;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSC6 )
            {
               BRKSC6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV26Options = new GxSimpleCollection();
         AV29OptionsDesc = new GxSimpleCollection();
         AV31OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV34Session = context.GetSession();
         AV36GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV37GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFItemNaoMensuravel_Codigo = "";
         AV11TFItemNaoMensuravel_Codigo_Sel = "";
         AV12TFItemNaoMensuravel_Descricao = "";
         AV13TFItemNaoMensuravel_Descricao_Sel = "";
         AV14TFItemNaoMensuravel_Referencia = "";
         AV15TFItemNaoMensuravel_Referencia_Sel = "";
         AV18TFItemNaoMensuravel_Tipo_SelsJson = "";
         AV19TFItemNaoMensuravel_Tipo_Sels = new GxSimpleCollection();
         AV38GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV39DynamicFiltersSelector1 = "";
         AV41ItemNaoMensuravel_Codigo1 = "";
         AV42ItemNaoMensuravel_Descricao1 = "";
         AV45DynamicFiltersSelector2 = "";
         AV47ItemNaoMensuravel_Codigo2 = "";
         AV48ItemNaoMensuravel_Descricao2 = "";
         AV51DynamicFiltersSelector3 = "";
         AV53ItemNaoMensuravel_Codigo3 = "";
         AV54ItemNaoMensuravel_Descricao3 = "";
         scmdbuf = "";
         lV41ItemNaoMensuravel_Codigo1 = "";
         lV42ItemNaoMensuravel_Descricao1 = "";
         lV47ItemNaoMensuravel_Codigo2 = "";
         lV48ItemNaoMensuravel_Descricao2 = "";
         lV53ItemNaoMensuravel_Codigo3 = "";
         lV54ItemNaoMensuravel_Descricao3 = "";
         lV10TFItemNaoMensuravel_Codigo = "";
         lV12TFItemNaoMensuravel_Descricao = "";
         lV14TFItemNaoMensuravel_Referencia = "";
         A715ItemNaoMensuravel_Codigo = "";
         A714ItemNaoMensuravel_Descricao = "";
         A1804ItemNaoMensuravel_Referencia = "";
         P00SC2_A709ReferenciaINM_Codigo = new int[1] ;
         P00SC2_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         P00SC2_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         P00SC2_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         P00SC2_A1804ItemNaoMensuravel_Referencia = new String[] {""} ;
         P00SC2_n1804ItemNaoMensuravel_Referencia = new bool[] {false} ;
         P00SC2_A717ItemNaoMensuravel_Tipo = new short[1] ;
         P00SC2_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         P00SC2_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         AV25Option = "";
         P00SC3_A709ReferenciaINM_Codigo = new int[1] ;
         P00SC3_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         P00SC3_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         P00SC3_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         P00SC3_A1804ItemNaoMensuravel_Referencia = new String[] {""} ;
         P00SC3_n1804ItemNaoMensuravel_Referencia = new bool[] {false} ;
         P00SC3_A717ItemNaoMensuravel_Tipo = new short[1] ;
         P00SC3_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         P00SC3_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         P00SC4_A709ReferenciaINM_Codigo = new int[1] ;
         P00SC4_A1804ItemNaoMensuravel_Referencia = new String[] {""} ;
         P00SC4_n1804ItemNaoMensuravel_Referencia = new bool[] {false} ;
         P00SC4_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         P00SC4_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         P00SC4_A717ItemNaoMensuravel_Tipo = new short[1] ;
         P00SC4_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         P00SC4_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         P00SC4_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getreferenciainmitemnaomensuravelwcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00SC2_A709ReferenciaINM_Codigo, P00SC2_A715ItemNaoMensuravel_Codigo, P00SC2_A716ItemNaoMensuravel_Ativo, P00SC2_A719ItemNaoMensuravel_Valor, P00SC2_A1804ItemNaoMensuravel_Referencia, P00SC2_n1804ItemNaoMensuravel_Referencia, P00SC2_A717ItemNaoMensuravel_Tipo, P00SC2_A714ItemNaoMensuravel_Descricao, P00SC2_A718ItemNaoMensuravel_AreaTrabalhoCod
               }
               , new Object[] {
               P00SC3_A709ReferenciaINM_Codigo, P00SC3_A714ItemNaoMensuravel_Descricao, P00SC3_A716ItemNaoMensuravel_Ativo, P00SC3_A719ItemNaoMensuravel_Valor, P00SC3_A1804ItemNaoMensuravel_Referencia, P00SC3_n1804ItemNaoMensuravel_Referencia, P00SC3_A717ItemNaoMensuravel_Tipo, P00SC3_A715ItemNaoMensuravel_Codigo, P00SC3_A718ItemNaoMensuravel_AreaTrabalhoCod
               }
               , new Object[] {
               P00SC4_A709ReferenciaINM_Codigo, P00SC4_A1804ItemNaoMensuravel_Referencia, P00SC4_n1804ItemNaoMensuravel_Referencia, P00SC4_A716ItemNaoMensuravel_Ativo, P00SC4_A719ItemNaoMensuravel_Valor, P00SC4_A717ItemNaoMensuravel_Tipo, P00SC4_A714ItemNaoMensuravel_Descricao, P00SC4_A715ItemNaoMensuravel_Codigo, P00SC4_A718ItemNaoMensuravel_AreaTrabalhoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV20TFItemNaoMensuravel_Ativo_Sel ;
      private short AV40DynamicFiltersOperator1 ;
      private short AV43ItemNaoMensuravel_Tipo1 ;
      private short AV46DynamicFiltersOperator2 ;
      private short AV49ItemNaoMensuravel_Tipo2 ;
      private short AV52DynamicFiltersOperator3 ;
      private short AV55ItemNaoMensuravel_Tipo3 ;
      private short A717ItemNaoMensuravel_Tipo ;
      private int AV59GXV1 ;
      private int AV56ReferenciaINM_Codigo ;
      private int AV19TFItemNaoMensuravel_Tipo_Sels_Count ;
      private int A709ReferenciaINM_Codigo ;
      private int A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private long AV33count ;
      private decimal AV16TFItemNaoMensuravel_Valor ;
      private decimal AV17TFItemNaoMensuravel_Valor_To ;
      private decimal A719ItemNaoMensuravel_Valor ;
      private String AV10TFItemNaoMensuravel_Codigo ;
      private String AV11TFItemNaoMensuravel_Codigo_Sel ;
      private String AV14TFItemNaoMensuravel_Referencia ;
      private String AV15TFItemNaoMensuravel_Referencia_Sel ;
      private String AV41ItemNaoMensuravel_Codigo1 ;
      private String AV47ItemNaoMensuravel_Codigo2 ;
      private String AV53ItemNaoMensuravel_Codigo3 ;
      private String scmdbuf ;
      private String lV41ItemNaoMensuravel_Codigo1 ;
      private String lV47ItemNaoMensuravel_Codigo2 ;
      private String lV53ItemNaoMensuravel_Codigo3 ;
      private String lV10TFItemNaoMensuravel_Codigo ;
      private String lV14TFItemNaoMensuravel_Referencia ;
      private String A715ItemNaoMensuravel_Codigo ;
      private String A1804ItemNaoMensuravel_Referencia ;
      private bool returnInSub ;
      private bool AV44DynamicFiltersEnabled2 ;
      private bool AV50DynamicFiltersEnabled3 ;
      private bool A716ItemNaoMensuravel_Ativo ;
      private bool BRKSC2 ;
      private bool n1804ItemNaoMensuravel_Referencia ;
      private bool BRKSC4 ;
      private bool BRKSC6 ;
      private String AV32OptionIndexesJson ;
      private String AV27OptionsJson ;
      private String AV30OptionsDescJson ;
      private String AV18TFItemNaoMensuravel_Tipo_SelsJson ;
      private String AV42ItemNaoMensuravel_Descricao1 ;
      private String AV48ItemNaoMensuravel_Descricao2 ;
      private String AV54ItemNaoMensuravel_Descricao3 ;
      private String lV42ItemNaoMensuravel_Descricao1 ;
      private String lV48ItemNaoMensuravel_Descricao2 ;
      private String lV54ItemNaoMensuravel_Descricao3 ;
      private String A714ItemNaoMensuravel_Descricao ;
      private String AV23DDOName ;
      private String AV21SearchTxt ;
      private String AV22SearchTxtTo ;
      private String AV12TFItemNaoMensuravel_Descricao ;
      private String AV13TFItemNaoMensuravel_Descricao_Sel ;
      private String AV39DynamicFiltersSelector1 ;
      private String AV45DynamicFiltersSelector2 ;
      private String AV51DynamicFiltersSelector3 ;
      private String lV12TFItemNaoMensuravel_Descricao ;
      private String AV25Option ;
      private IGxSession AV34Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00SC2_A709ReferenciaINM_Codigo ;
      private String[] P00SC2_A715ItemNaoMensuravel_Codigo ;
      private bool[] P00SC2_A716ItemNaoMensuravel_Ativo ;
      private decimal[] P00SC2_A719ItemNaoMensuravel_Valor ;
      private String[] P00SC2_A1804ItemNaoMensuravel_Referencia ;
      private bool[] P00SC2_n1804ItemNaoMensuravel_Referencia ;
      private short[] P00SC2_A717ItemNaoMensuravel_Tipo ;
      private String[] P00SC2_A714ItemNaoMensuravel_Descricao ;
      private int[] P00SC2_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int[] P00SC3_A709ReferenciaINM_Codigo ;
      private String[] P00SC3_A714ItemNaoMensuravel_Descricao ;
      private bool[] P00SC3_A716ItemNaoMensuravel_Ativo ;
      private decimal[] P00SC3_A719ItemNaoMensuravel_Valor ;
      private String[] P00SC3_A1804ItemNaoMensuravel_Referencia ;
      private bool[] P00SC3_n1804ItemNaoMensuravel_Referencia ;
      private short[] P00SC3_A717ItemNaoMensuravel_Tipo ;
      private String[] P00SC3_A715ItemNaoMensuravel_Codigo ;
      private int[] P00SC3_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int[] P00SC4_A709ReferenciaINM_Codigo ;
      private String[] P00SC4_A1804ItemNaoMensuravel_Referencia ;
      private bool[] P00SC4_n1804ItemNaoMensuravel_Referencia ;
      private bool[] P00SC4_A716ItemNaoMensuravel_Ativo ;
      private decimal[] P00SC4_A719ItemNaoMensuravel_Valor ;
      private short[] P00SC4_A717ItemNaoMensuravel_Tipo ;
      private String[] P00SC4_A714ItemNaoMensuravel_Descricao ;
      private String[] P00SC4_A715ItemNaoMensuravel_Codigo ;
      private int[] P00SC4_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV19TFItemNaoMensuravel_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV36GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV37GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV38GridStateDynamicFilter ;
   }

   public class getreferenciainmitemnaomensuravelwcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00SC2( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV19TFItemNaoMensuravel_Tipo_Sels ,
                                             String AV39DynamicFiltersSelector1 ,
                                             short AV40DynamicFiltersOperator1 ,
                                             String AV41ItemNaoMensuravel_Codigo1 ,
                                             String AV42ItemNaoMensuravel_Descricao1 ,
                                             short AV43ItemNaoMensuravel_Tipo1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             short AV46DynamicFiltersOperator2 ,
                                             String AV47ItemNaoMensuravel_Codigo2 ,
                                             String AV48ItemNaoMensuravel_Descricao2 ,
                                             short AV49ItemNaoMensuravel_Tipo2 ,
                                             bool AV50DynamicFiltersEnabled3 ,
                                             String AV51DynamicFiltersSelector3 ,
                                             short AV52DynamicFiltersOperator3 ,
                                             String AV53ItemNaoMensuravel_Codigo3 ,
                                             String AV54ItemNaoMensuravel_Descricao3 ,
                                             short AV55ItemNaoMensuravel_Tipo3 ,
                                             String AV11TFItemNaoMensuravel_Codigo_Sel ,
                                             String AV10TFItemNaoMensuravel_Codigo ,
                                             String AV13TFItemNaoMensuravel_Descricao_Sel ,
                                             String AV12TFItemNaoMensuravel_Descricao ,
                                             String AV15TFItemNaoMensuravel_Referencia_Sel ,
                                             String AV14TFItemNaoMensuravel_Referencia ,
                                             decimal AV16TFItemNaoMensuravel_Valor ,
                                             decimal AV17TFItemNaoMensuravel_Valor_To ,
                                             int AV19TFItemNaoMensuravel_Tipo_Sels_Count ,
                                             short AV20TFItemNaoMensuravel_Ativo_Sel ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             String A1804ItemNaoMensuravel_Referencia ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             bool A716ItemNaoMensuravel_Ativo ,
                                             int AV56ReferenciaINM_Codigo ,
                                             int A709ReferenciaINM_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [24] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ReferenciaINM_Codigo], [ItemNaoMensuravel_Codigo], [ItemNaoMensuravel_Ativo], [ItemNaoMensuravel_Valor], [ItemNaoMensuravel_Referencia], [ItemNaoMensuravel_Tipo], [ItemNaoMensuravel_Descricao], [ItemNaoMensuravel_AreaTrabalhoCod] FROM [ItemNaoMensuravel] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ReferenciaINM_Codigo] = @AV56ReferenciaINM_Codigo)";
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV40DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41ItemNaoMensuravel_Codigo1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV41ItemNaoMensuravel_Codigo1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV40DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41ItemNaoMensuravel_Codigo1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV41ItemNaoMensuravel_Codigo1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV40DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ItemNaoMensuravel_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV42ItemNaoMensuravel_Descricao1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV40DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ItemNaoMensuravel_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV42ItemNaoMensuravel_Descricao1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV43ItemNaoMensuravel_Tipo1) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV43ItemNaoMensuravel_Tipo1)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Codigo2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV47ItemNaoMensuravel_Codigo2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Codigo2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV47ItemNaoMensuravel_Codigo2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV48ItemNaoMensuravel_Descricao2)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV48ItemNaoMensuravel_Descricao2)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV49ItemNaoMensuravel_Tipo2) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV49ItemNaoMensuravel_Tipo2)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV52DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Codigo3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV53ItemNaoMensuravel_Codigo3)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV52DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Codigo3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV53ItemNaoMensuravel_Codigo3)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV54ItemNaoMensuravel_Descricao3)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV54ItemNaoMensuravel_Descricao3)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV55ItemNaoMensuravel_Tipo3) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV55ItemNaoMensuravel_Tipo3)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFItemNaoMensuravel_Codigo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFItemNaoMensuravel_Codigo)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV10TFItemNaoMensuravel_Codigo)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFItemNaoMensuravel_Codigo_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] = @AV11TFItemNaoMensuravel_Codigo_Sel)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFItemNaoMensuravel_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV12TFItemNaoMensuravel_Descricao)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] = @AV13TFItemNaoMensuravel_Descricao_Sel)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Referencia_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFItemNaoMensuravel_Referencia)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Referencia] like @lV14TFItemNaoMensuravel_Referencia)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Referencia_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Referencia] = @AV15TFItemNaoMensuravel_Referencia_Sel)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV16TFItemNaoMensuravel_Valor) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Valor] >= @AV16TFItemNaoMensuravel_Valor)";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV17TFItemNaoMensuravel_Valor_To) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Valor] <= @AV17TFItemNaoMensuravel_Valor_To)";
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( AV19TFItemNaoMensuravel_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19TFItemNaoMensuravel_Tipo_Sels, "[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
         }
         if ( AV20TFItemNaoMensuravel_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Ativo] = 1)";
         }
         if ( AV20TFItemNaoMensuravel_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ReferenciaINM_Codigo], [ItemNaoMensuravel_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00SC3( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV19TFItemNaoMensuravel_Tipo_Sels ,
                                             String AV39DynamicFiltersSelector1 ,
                                             short AV40DynamicFiltersOperator1 ,
                                             String AV41ItemNaoMensuravel_Codigo1 ,
                                             String AV42ItemNaoMensuravel_Descricao1 ,
                                             short AV43ItemNaoMensuravel_Tipo1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             short AV46DynamicFiltersOperator2 ,
                                             String AV47ItemNaoMensuravel_Codigo2 ,
                                             String AV48ItemNaoMensuravel_Descricao2 ,
                                             short AV49ItemNaoMensuravel_Tipo2 ,
                                             bool AV50DynamicFiltersEnabled3 ,
                                             String AV51DynamicFiltersSelector3 ,
                                             short AV52DynamicFiltersOperator3 ,
                                             String AV53ItemNaoMensuravel_Codigo3 ,
                                             String AV54ItemNaoMensuravel_Descricao3 ,
                                             short AV55ItemNaoMensuravel_Tipo3 ,
                                             String AV11TFItemNaoMensuravel_Codigo_Sel ,
                                             String AV10TFItemNaoMensuravel_Codigo ,
                                             String AV13TFItemNaoMensuravel_Descricao_Sel ,
                                             String AV12TFItemNaoMensuravel_Descricao ,
                                             String AV15TFItemNaoMensuravel_Referencia_Sel ,
                                             String AV14TFItemNaoMensuravel_Referencia ,
                                             decimal AV16TFItemNaoMensuravel_Valor ,
                                             decimal AV17TFItemNaoMensuravel_Valor_To ,
                                             int AV19TFItemNaoMensuravel_Tipo_Sels_Count ,
                                             short AV20TFItemNaoMensuravel_Ativo_Sel ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             String A1804ItemNaoMensuravel_Referencia ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             bool A716ItemNaoMensuravel_Ativo ,
                                             int AV56ReferenciaINM_Codigo ,
                                             int A709ReferenciaINM_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [24] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [ReferenciaINM_Codigo], [ItemNaoMensuravel_Descricao], [ItemNaoMensuravel_Ativo], [ItemNaoMensuravel_Valor], [ItemNaoMensuravel_Referencia], [ItemNaoMensuravel_Tipo], [ItemNaoMensuravel_Codigo], [ItemNaoMensuravel_AreaTrabalhoCod] FROM [ItemNaoMensuravel] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ReferenciaINM_Codigo] = @AV56ReferenciaINM_Codigo)";
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV40DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41ItemNaoMensuravel_Codigo1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV41ItemNaoMensuravel_Codigo1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV40DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41ItemNaoMensuravel_Codigo1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV41ItemNaoMensuravel_Codigo1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV40DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ItemNaoMensuravel_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV42ItemNaoMensuravel_Descricao1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV40DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ItemNaoMensuravel_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV42ItemNaoMensuravel_Descricao1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV43ItemNaoMensuravel_Tipo1) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV43ItemNaoMensuravel_Tipo1)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Codigo2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV47ItemNaoMensuravel_Codigo2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Codigo2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV47ItemNaoMensuravel_Codigo2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV48ItemNaoMensuravel_Descricao2)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV48ItemNaoMensuravel_Descricao2)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV49ItemNaoMensuravel_Tipo2) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV49ItemNaoMensuravel_Tipo2)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV52DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Codigo3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV53ItemNaoMensuravel_Codigo3)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV52DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Codigo3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV53ItemNaoMensuravel_Codigo3)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV54ItemNaoMensuravel_Descricao3)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV54ItemNaoMensuravel_Descricao3)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV55ItemNaoMensuravel_Tipo3) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV55ItemNaoMensuravel_Tipo3)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFItemNaoMensuravel_Codigo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFItemNaoMensuravel_Codigo)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV10TFItemNaoMensuravel_Codigo)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFItemNaoMensuravel_Codigo_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] = @AV11TFItemNaoMensuravel_Codigo_Sel)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFItemNaoMensuravel_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV12TFItemNaoMensuravel_Descricao)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] = @AV13TFItemNaoMensuravel_Descricao_Sel)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Referencia_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFItemNaoMensuravel_Referencia)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Referencia] like @lV14TFItemNaoMensuravel_Referencia)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Referencia_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Referencia] = @AV15TFItemNaoMensuravel_Referencia_Sel)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV16TFItemNaoMensuravel_Valor) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Valor] >= @AV16TFItemNaoMensuravel_Valor)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV17TFItemNaoMensuravel_Valor_To) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Valor] <= @AV17TFItemNaoMensuravel_Valor_To)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( AV19TFItemNaoMensuravel_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19TFItemNaoMensuravel_Tipo_Sels, "[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
         }
         if ( AV20TFItemNaoMensuravel_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Ativo] = 1)";
         }
         if ( AV20TFItemNaoMensuravel_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ReferenciaINM_Codigo], [ItemNaoMensuravel_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00SC4( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV19TFItemNaoMensuravel_Tipo_Sels ,
                                             String AV39DynamicFiltersSelector1 ,
                                             short AV40DynamicFiltersOperator1 ,
                                             String AV41ItemNaoMensuravel_Codigo1 ,
                                             String AV42ItemNaoMensuravel_Descricao1 ,
                                             short AV43ItemNaoMensuravel_Tipo1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             short AV46DynamicFiltersOperator2 ,
                                             String AV47ItemNaoMensuravel_Codigo2 ,
                                             String AV48ItemNaoMensuravel_Descricao2 ,
                                             short AV49ItemNaoMensuravel_Tipo2 ,
                                             bool AV50DynamicFiltersEnabled3 ,
                                             String AV51DynamicFiltersSelector3 ,
                                             short AV52DynamicFiltersOperator3 ,
                                             String AV53ItemNaoMensuravel_Codigo3 ,
                                             String AV54ItemNaoMensuravel_Descricao3 ,
                                             short AV55ItemNaoMensuravel_Tipo3 ,
                                             String AV11TFItemNaoMensuravel_Codigo_Sel ,
                                             String AV10TFItemNaoMensuravel_Codigo ,
                                             String AV13TFItemNaoMensuravel_Descricao_Sel ,
                                             String AV12TFItemNaoMensuravel_Descricao ,
                                             String AV15TFItemNaoMensuravel_Referencia_Sel ,
                                             String AV14TFItemNaoMensuravel_Referencia ,
                                             decimal AV16TFItemNaoMensuravel_Valor ,
                                             decimal AV17TFItemNaoMensuravel_Valor_To ,
                                             int AV19TFItemNaoMensuravel_Tipo_Sels_Count ,
                                             short AV20TFItemNaoMensuravel_Ativo_Sel ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             String A1804ItemNaoMensuravel_Referencia ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             bool A716ItemNaoMensuravel_Ativo ,
                                             int AV56ReferenciaINM_Codigo ,
                                             int A709ReferenciaINM_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [24] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT [ReferenciaINM_Codigo], [ItemNaoMensuravel_Referencia], [ItemNaoMensuravel_Ativo], [ItemNaoMensuravel_Valor], [ItemNaoMensuravel_Tipo], [ItemNaoMensuravel_Descricao], [ItemNaoMensuravel_Codigo], [ItemNaoMensuravel_AreaTrabalhoCod] FROM [ItemNaoMensuravel] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ReferenciaINM_Codigo] = @AV56ReferenciaINM_Codigo)";
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV40DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41ItemNaoMensuravel_Codigo1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV41ItemNaoMensuravel_Codigo1)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV40DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41ItemNaoMensuravel_Codigo1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV41ItemNaoMensuravel_Codigo1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV40DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ItemNaoMensuravel_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV42ItemNaoMensuravel_Descricao1)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV40DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ItemNaoMensuravel_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV42ItemNaoMensuravel_Descricao1)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV43ItemNaoMensuravel_Tipo1) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV43ItemNaoMensuravel_Tipo1)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Codigo2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV47ItemNaoMensuravel_Codigo2)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Codigo2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV47ItemNaoMensuravel_Codigo2)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV48ItemNaoMensuravel_Descricao2)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV48ItemNaoMensuravel_Descricao2)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV49ItemNaoMensuravel_Tipo2) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV49ItemNaoMensuravel_Tipo2)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV52DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Codigo3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV53ItemNaoMensuravel_Codigo3)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_CODIGO") == 0 ) && ( AV52DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Codigo3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like '%' + @lV53ItemNaoMensuravel_Codigo3)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV54ItemNaoMensuravel_Descricao3)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like '%' + @lV54ItemNaoMensuravel_Descricao3)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_TIPO") == 0 ) && ( ! (0==AV55ItemNaoMensuravel_Tipo3) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Tipo] = @AV55ItemNaoMensuravel_Tipo3)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFItemNaoMensuravel_Codigo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFItemNaoMensuravel_Codigo)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] like @lV10TFItemNaoMensuravel_Codigo)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFItemNaoMensuravel_Codigo_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Codigo] = @AV11TFItemNaoMensuravel_Codigo_Sel)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFItemNaoMensuravel_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] like @lV12TFItemNaoMensuravel_Descricao)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Descricao] = @AV13TFItemNaoMensuravel_Descricao_Sel)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Referencia_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFItemNaoMensuravel_Referencia)) ) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Referencia] like @lV14TFItemNaoMensuravel_Referencia)";
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Referencia_Sel)) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Referencia] = @AV15TFItemNaoMensuravel_Referencia_Sel)";
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV16TFItemNaoMensuravel_Valor) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Valor] >= @AV16TFItemNaoMensuravel_Valor)";
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV17TFItemNaoMensuravel_Valor_To) )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Valor] <= @AV17TFItemNaoMensuravel_Valor_To)";
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( AV19TFItemNaoMensuravel_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19TFItemNaoMensuravel_Tipo_Sels, "[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
         }
         if ( AV20TFItemNaoMensuravel_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Ativo] = 1)";
         }
         if ( AV20TFItemNaoMensuravel_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([ItemNaoMensuravel_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ReferenciaINM_Codigo], [ItemNaoMensuravel_Referencia]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00SC2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (int)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (decimal)dynConstraints[32] , (bool)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] );
               case 1 :
                     return conditional_P00SC3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (int)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (decimal)dynConstraints[32] , (bool)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] );
               case 2 :
                     return conditional_P00SC4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (int)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (decimal)dynConstraints[32] , (bool)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SC2 ;
          prmP00SC2 = new Object[] {
          new Object[] {"@AV56ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41ItemNaoMensuravel_Codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV41ItemNaoMensuravel_Codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV42ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV42ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV43ItemNaoMensuravel_Tipo1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV47ItemNaoMensuravel_Codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV47ItemNaoMensuravel_Codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV49ItemNaoMensuravel_Tipo2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV55ItemNaoMensuravel_Tipo3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV10TFItemNaoMensuravel_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFItemNaoMensuravel_Codigo_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFItemNaoMensuravel_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV13TFItemNaoMensuravel_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV14TFItemNaoMensuravel_Referencia",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFItemNaoMensuravel_Referencia_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV16TFItemNaoMensuravel_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV17TFItemNaoMensuravel_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00SC3 ;
          prmP00SC3 = new Object[] {
          new Object[] {"@AV56ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41ItemNaoMensuravel_Codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV41ItemNaoMensuravel_Codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV42ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV42ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV43ItemNaoMensuravel_Tipo1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV47ItemNaoMensuravel_Codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV47ItemNaoMensuravel_Codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV49ItemNaoMensuravel_Tipo2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV55ItemNaoMensuravel_Tipo3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV10TFItemNaoMensuravel_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFItemNaoMensuravel_Codigo_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFItemNaoMensuravel_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV13TFItemNaoMensuravel_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV14TFItemNaoMensuravel_Referencia",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFItemNaoMensuravel_Referencia_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV16TFItemNaoMensuravel_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV17TFItemNaoMensuravel_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00SC4 ;
          prmP00SC4 = new Object[] {
          new Object[] {"@AV56ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41ItemNaoMensuravel_Codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV41ItemNaoMensuravel_Codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV42ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV42ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV43ItemNaoMensuravel_Tipo1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV47ItemNaoMensuravel_Codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV47ItemNaoMensuravel_Codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV49ItemNaoMensuravel_Tipo2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AV55ItemNaoMensuravel_Tipo3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV10TFItemNaoMensuravel_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFItemNaoMensuravel_Codigo_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFItemNaoMensuravel_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV13TFItemNaoMensuravel_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV14TFItemNaoMensuravel_Referencia",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFItemNaoMensuravel_Referencia_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV16TFItemNaoMensuravel_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV17TFItemNaoMensuravel_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SC2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SC2,100,0,true,false )
             ,new CursorDef("P00SC3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SC3,100,0,true,false )
             ,new CursorDef("P00SC4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SC4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((String[]) buf[6])[0] = rslt.getLongVarchar(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getreferenciainmitemnaomensuravelwcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getreferenciainmitemnaomensuravelwcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getreferenciainmitemnaomensuravelwcfilterdata") )
          {
             return  ;
          }
          getreferenciainmitemnaomensuravelwcfilterdata worker = new getreferenciainmitemnaomensuravelwcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
