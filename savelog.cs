/*
               File: savelog
        Description: log de a��es
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:33.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class savelog : GXProcedure
   {
      public savelog( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public savelog( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( short aP0_LogFile ,
                           String aP1_Txt )
      {
         this.AV18LogFile = aP0_LogFile;
         this.AV10Txt = aP1_Txt;
         initialize();
         executePrivate();
      }

      public void executeSubmit( short aP0_LogFile ,
                                 String aP1_Txt )
      {
         savelog objsavelog;
         objsavelog = new savelog();
         objsavelog.AV18LogFile = aP0_LogFile;
         objsavelog.AV10Txt = aP1_Txt;
         objsavelog.context.SetSubmitInitialConfig(context);
         objsavelog.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objsavelog);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((savelog)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new getserverpath(context ).execute( ref  AV24Pgmname, ref  AV16AbsolutePath) ;
         if ( AV18LogFile == 1 )
         {
            AV15FileName = "Envio de e-mail.txt";
            AV20Titulo = "Data/Hora, Parametros >> �rea, Assunto, Texto, Destinat�rios, Dados, Sistema, Configura��o >> Usu�rio,Host, Porta, Autentica��o, Seguran�a, Login, Envio";
         }
         AV12Directory.Source = AV16AbsolutePath+"Eventos";
         AV14Arquivo = AV16AbsolutePath + "Eventos\\" + AV15FileName;
         AV21File.Source = AV14Arquivo;
         AV8append = 1;
         if ( ! AV12Directory.Exists() )
         {
            AV12Directory.Create();
         }
         if ( ! AV21File.Exists() )
         {
            /* Execute user subroutine: 'TITULO' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV11TxtAuxiliar = context.localUtil.TToC( DateTimeUtil.ServerNow( context, "DEFAULT"), 10, 8, 0, 3, "/", ":", " ") + ", " + StringUtil.Trim( AV10Txt);
         if ( ! AV19Opened )
         {
            AV9FlagFile = context.FileIOInstance.dfwopen( AV14Arquivo, "", "", AV8append, "UTF8");
         }
         if ( AV9FlagFile == 0 )
         {
            AV9FlagFile = context.FileIOInstance.dfwptxt( AV11TxtAuxiliar, 0);
            AV9FlagFile = context.FileIOInstance.dfwnext( );
            AV9FlagFile = context.FileIOInstance.dfwclose( );
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'TITULO' Routine */
         AV9FlagFile = context.FileIOInstance.dfwopen( AV14Arquivo, "", "", AV8append, "UTF8");
         if ( AV9FlagFile == 0 )
         {
            AV19Opened = true;
            AV9FlagFile = context.FileIOInstance.dfwptxt( AV20Titulo, 0);
            AV9FlagFile = context.FileIOInstance.dfwnext( );
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV24Pgmname = "";
         AV16AbsolutePath = "";
         AV15FileName = "";
         AV20Titulo = "";
         AV12Directory = new GxDirectory(context.GetPhysicalPath());
         AV14Arquivo = "";
         AV21File = new GxFile(context.GetPhysicalPath());
         AV11TxtAuxiliar = "";
         AV24Pgmname = "savelog";
         /* GeneXus formulas. */
         AV24Pgmname = "savelog";
         context.Gx_err = 0;
      }

      private short AV18LogFile ;
      private short AV8append ;
      private short AV9FlagFile ;
      private String AV24Pgmname ;
      private String AV16AbsolutePath ;
      private String AV15FileName ;
      private String AV20Titulo ;
      private bool returnInSub ;
      private bool AV19Opened ;
      private String AV10Txt ;
      private String AV14Arquivo ;
      private String AV11TxtAuxiliar ;
      private GxFile AV21File ;
      private GxDirectory AV12Directory ;
   }

}
