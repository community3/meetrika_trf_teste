/*
               File: FuncaoAPFFuncaoAPFEvidenciaWC
        Description: Funcao APFFuncao APFEvidencia WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:50:0.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaoapffuncaoapfevidenciawc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public funcaoapffuncaoapfevidenciawc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public funcaoapffuncaoapfevidenciawc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPF_Codigo ,
                           int aP1_FuncaoAPF_SistemaCod )
      {
         this.AV7FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV8FuncaoAPF_SistemaCod = aP1_FuncaoAPF_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0)));
                  AV8FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoAPF_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7FuncaoAPF_Codigo,(int)AV8FuncaoAPF_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_22 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_22_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_22_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV25TFFuncaoAPFEvidencia_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFFuncaoAPFEvidencia_Descricao", AV25TFFuncaoAPFEvidencia_Descricao);
                  AV26TFFuncaoAPFEvidencia_Descricao_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFFuncaoAPFEvidencia_Descricao_Sel", AV26TFFuncaoAPFEvidencia_Descricao_Sel);
                  AV29TFFuncaoAPFEvidencia_NomeArq = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFFuncaoAPFEvidencia_NomeArq", AV29TFFuncaoAPFEvidencia_NomeArq);
                  AV30TFFuncaoAPFEvidencia_NomeArq_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFFuncaoAPFEvidencia_NomeArq_Sel", AV30TFFuncaoAPFEvidencia_NomeArq_Sel);
                  AV33TFFuncaoAPFEvidencia_TipoArq = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFFuncaoAPFEvidencia_TipoArq", AV33TFFuncaoAPFEvidencia_TipoArq);
                  AV34TFFuncaoAPFEvidencia_TipoArq_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFFuncaoAPFEvidencia_TipoArq_Sel", AV34TFFuncaoAPFEvidencia_TipoArq_Sel);
                  AV7FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0)));
                  AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace", AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace);
                  AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace", AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace);
                  AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace", AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace);
                  AV59Pgmname = GetNextPar( );
                  AV8FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoAPF_SistemaCod), 6, 0)));
                  A406FuncaoAPFEvidencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A360FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n360FuncaoAPF_SistemaCod = false;
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV25TFFuncaoAPFEvidencia_Descricao, AV26TFFuncaoAPFEvidencia_Descricao_Sel, AV29TFFuncaoAPFEvidencia_NomeArq, AV30TFFuncaoAPFEvidencia_NomeArq_Sel, AV33TFFuncaoAPFEvidencia_TipoArq, AV34TFFuncaoAPFEvidencia_TipoArq_Sel, AV7FuncaoAPF_Codigo, AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV59Pgmname, AV8FuncaoAPF_SistemaCod, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAAC2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV59Pgmname = "FuncaoAPFFuncaoAPFEvidenciaWC";
               context.Gx_err = 0;
               WSAC2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Funcao APFFuncao APFEvidencia WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205181250025");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaoapffuncaoapfevidenciawc.aspx") + "?" + UrlEncode("" +AV7FuncaoAPF_Codigo) + "," + UrlEncode("" +AV8FuncaoAPF_SistemaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_DESCRICAO", AV25TFFuncaoAPFEvidencia_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL", AV26TFFuncaoAPFEvidencia_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ", StringUtil.RTrim( AV29TFFuncaoAPFEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL", StringUtil.RTrim( AV30TFFuncaoAPFEvidencia_NomeArq_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_TIPOARQ", StringUtil.RTrim( AV33TFFuncaoAPFEvidencia_TipoArq));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL", StringUtil.RTrim( AV34TFFuncaoAPFEvidencia_TipoArq_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_22", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_22), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV36DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV36DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOAPFEVIDENCIA_DESCRICAOTITLEFILTERDATA", AV24FuncaoAPFEvidencia_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOAPFEVIDENCIA_DESCRICAOTITLEFILTERDATA", AV24FuncaoAPFEvidencia_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA", AV28FuncaoAPFEvidencia_NomeArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA", AV28FuncaoAPFEvidencia_NomeArqTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOAPFEVIDENCIA_TIPOARQTITLEFILTERDATA", AV32FuncaoAPFEvidencia_TipoArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOAPFEVIDENCIA_TIPOARQTITLEFILTERDATA", AV32FuncaoAPFEvidencia_TipoArqTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV8FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV8FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV59Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Caption", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Cls", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapfevidencia_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Caption", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Tooltip", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Cls", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filtertype", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalisttype", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalistproc", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapfevidencia_nomearq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Sortasc", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Loadingdata", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Caption", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Tooltip", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Cls", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_tipoarq_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_tipoarq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_tipoarq_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filtertype", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_tipoarq_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_tipoarq_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Datalisttype", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Datalistproc", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapfevidencia_tipoarq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Sortasc", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Loadingdata", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormAC2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("funcaoapffuncaoapfevidenciawc.js", "?20205181250177");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "FuncaoAPFFuncaoAPFEvidenciaWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcao APFFuncao APFEvidencia WC" ;
      }

      protected void WBAC0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "funcaoapffuncaoapfevidenciawc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_AC2( true) ;
         }
         else
         {
            wb_table1_2_AC2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AC2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_22_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoAPFFuncaoAPFEvidenciaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_22_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_FuncaoAPFFuncaoAPFEvidenciaWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_22_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapfevidencia_descricao_Internalname, AV25TFFuncaoAPFEvidencia_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", 0, edtavTffuncaoapfevidencia_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_FuncaoAPFFuncaoAPFEvidenciaWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_22_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapfevidencia_descricao_sel_Internalname, AV26TFFuncaoAPFEvidencia_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", 0, edtavTffuncaoapfevidencia_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_FuncaoAPFFuncaoAPFEvidenciaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_22_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfevidencia_nomearq_Internalname, StringUtil.RTrim( AV29TFFuncaoAPFEvidencia_NomeArq), StringUtil.RTrim( context.localUtil.Format( AV29TFFuncaoAPFEvidencia_NomeArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfevidencia_nomearq_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfevidencia_nomearq_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoAPFFuncaoAPFEvidenciaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'" + sGXsfl_22_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfevidencia_nomearq_sel_Internalname, StringUtil.RTrim( AV30TFFuncaoAPFEvidencia_NomeArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV30TFFuncaoAPFEvidencia_NomeArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfevidencia_nomearq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfevidencia_nomearq_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoAPFFuncaoAPFEvidenciaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'" + sGXsfl_22_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfevidencia_tipoarq_Internalname, StringUtil.RTrim( AV33TFFuncaoAPFEvidencia_TipoArq), StringUtil.RTrim( context.localUtil.Format( AV33TFFuncaoAPFEvidencia_TipoArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfevidencia_tipoarq_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfevidencia_tipoarq_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoAPFFuncaoAPFEvidenciaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'" + sGXsfl_22_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfevidencia_tipoarq_sel_Internalname, StringUtil.RTrim( AV34TFFuncaoAPFEvidencia_TipoArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV34TFFuncaoAPFEvidencia_TipoArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfevidencia_tipoarq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfevidencia_tipoarq_sel_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoAPFFuncaoAPFEvidenciaWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'" + sGXsfl_22_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Internalname, AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", 0, edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_FuncaoAPFFuncaoAPFEvidenciaWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'" + sGXsfl_22_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname, AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", 0, edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_FuncaoAPFFuncaoAPFEvidenciaWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'" + sGXsfl_22_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Internalname, AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", 0, edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_FuncaoAPFFuncaoAPFEvidenciaWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTAC2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Funcao APFFuncao APFEvidencia WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPAC0( ) ;
            }
         }
      }

      protected void WSAC2( )
      {
         STARTAC2( ) ;
         EVTAC2( ) ;
      }

      protected void EVTAC2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11AC2 */
                                    E11AC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12AC2 */
                                    E12AC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13AC2 */
                                    E13AC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14AC2 */
                                    E14AC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15AC2 */
                                    E15AC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAC0( ) ;
                              }
                              nGXsfl_22_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_22_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_22_idx), 4, 0)), 4, "0");
                              SubsflControlProps_222( ) ;
                              AV16Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16Update)) ? AV56Update_GXI : context.convertURL( context.PathToRelativeUrl( AV16Update))));
                              AV17Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete)) ? AV57Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV17Delete))));
                              AV18Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV18Display)) ? AV58Display_GXI : context.convertURL( context.PathToRelativeUrl( AV18Display))));
                              A406FuncaoAPFEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFEvidencia_Codigo_Internalname), ",", "."));
                              A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                              A360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_SistemaCod_Internalname), ",", "."));
                              n360FuncaoAPF_SistemaCod = false;
                              A407FuncaoAPFEvidencia_Descricao = cgiGet( edtFuncaoAPFEvidencia_Descricao_Internalname);
                              n407FuncaoAPFEvidencia_Descricao = false;
                              A409FuncaoAPFEvidencia_NomeArq = cgiGet( edtFuncaoAPFEvidencia_NomeArq_Internalname);
                              n409FuncaoAPFEvidencia_NomeArq = false;
                              A410FuncaoAPFEvidencia_TipoArq = cgiGet( edtFuncaoAPFEvidencia_TipoArq_Internalname);
                              n410FuncaoAPFEvidencia_TipoArq = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16AC2 */
                                          E16AC2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17AC2 */
                                          E17AC2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18AC2 */
                                          E18AC2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfevidencia_descricao Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_DESCRICAO"), AV25TFFuncaoAPFEvidencia_Descricao) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfevidencia_descricao_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL"), AV26TFFuncaoAPFEvidencia_Descricao_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfevidencia_nomearq Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ"), AV29TFFuncaoAPFEvidencia_NomeArq) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfevidencia_nomearq_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL"), AV30TFFuncaoAPFEvidencia_NomeArq_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfevidencia_tipoarq Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_TIPOARQ"), AV33TFFuncaoAPFEvidencia_TipoArq) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfevidencia_tipoarq_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL"), AV34TFFuncaoAPFEvidencia_TipoArq_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPAC0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEAC2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormAC2( ) ;
            }
         }
      }

      protected void PAAC2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_222( ) ;
         while ( nGXsfl_22_idx <= nRC_GXsfl_22 )
         {
            sendrow_222( ) ;
            nGXsfl_22_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_22_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_22_idx+1));
            sGXsfl_22_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_22_idx), 4, 0)), 4, "0");
            SubsflControlProps_222( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV25TFFuncaoAPFEvidencia_Descricao ,
                                       String AV26TFFuncaoAPFEvidencia_Descricao_Sel ,
                                       String AV29TFFuncaoAPFEvidencia_NomeArq ,
                                       String AV30TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                       String AV33TFFuncaoAPFEvidencia_TipoArq ,
                                       String AV34TFFuncaoAPFEvidencia_TipoArq_Sel ,
                                       int AV7FuncaoAPF_Codigo ,
                                       String AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace ,
                                       String AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace ,
                                       String AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace ,
                                       String AV59Pgmname ,
                                       int AV8FuncaoAPF_SistemaCod ,
                                       int A406FuncaoAPFEvidencia_Codigo ,
                                       int A165FuncaoAPF_Codigo ,
                                       int A360FuncaoAPF_SistemaCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFAC2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A406FuncaoAPFEvidencia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_DESCRICAO", GetSecureSignedToken( sPrefix, A407FuncaoAPFEvidencia_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFEVIDENCIA_DESCRICAO", A407FuncaoAPFEvidencia_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_NOMEARQ", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A409FuncaoAPFEvidencia_NomeArq, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFEVIDENCIA_NOMEARQ", StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_TIPOARQ", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A410FuncaoAPFEvidencia_TipoArq, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFEVIDENCIA_TIPOARQ", StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAC2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV59Pgmname = "FuncaoAPFFuncaoAPFEvidenciaWC";
         context.Gx_err = 0;
      }

      protected void RFAC2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 22;
         /* Execute user event: E17AC2 */
         E17AC2 ();
         nGXsfl_22_idx = 1;
         sGXsfl_22_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_22_idx), 4, 0)), 4, "0");
         SubsflControlProps_222( ) ;
         nGXsfl_22_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_222( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV26TFFuncaoAPFEvidencia_Descricao_Sel ,
                                                 AV25TFFuncaoAPFEvidencia_Descricao ,
                                                 AV30TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                                 AV29TFFuncaoAPFEvidencia_NomeArq ,
                                                 AV34TFFuncaoAPFEvidencia_TipoArq_Sel ,
                                                 AV33TFFuncaoAPFEvidencia_TipoArq ,
                                                 A407FuncaoAPFEvidencia_Descricao ,
                                                 A409FuncaoAPFEvidencia_NomeArq ,
                                                 A410FuncaoAPFEvidencia_TipoArq ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A165FuncaoAPF_Codigo ,
                                                 AV7FuncaoAPF_Codigo ,
                                                 A750FuncaoAPFEvidencia_Ativo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                 }
            });
            lV25TFFuncaoAPFEvidencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV25TFFuncaoAPFEvidencia_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFFuncaoAPFEvidencia_Descricao", AV25TFFuncaoAPFEvidencia_Descricao);
            lV29TFFuncaoAPFEvidencia_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV29TFFuncaoAPFEvidencia_NomeArq), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFFuncaoAPFEvidencia_NomeArq", AV29TFFuncaoAPFEvidencia_NomeArq);
            lV33TFFuncaoAPFEvidencia_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV33TFFuncaoAPFEvidencia_TipoArq), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFFuncaoAPFEvidencia_TipoArq", AV33TFFuncaoAPFEvidencia_TipoArq);
            /* Using cursor H00AC2 */
            pr_default.execute(0, new Object[] {AV7FuncaoAPF_Codigo, lV25TFFuncaoAPFEvidencia_Descricao, AV26TFFuncaoAPFEvidencia_Descricao_Sel, lV29TFFuncaoAPFEvidencia_NomeArq, AV30TFFuncaoAPFEvidencia_NomeArq_Sel, lV33TFFuncaoAPFEvidencia_TipoArq, AV34TFFuncaoAPFEvidencia_TipoArq_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_22_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A750FuncaoAPFEvidencia_Ativo = H00AC2_A750FuncaoAPFEvidencia_Ativo[0];
               n750FuncaoAPFEvidencia_Ativo = H00AC2_n750FuncaoAPFEvidencia_Ativo[0];
               A410FuncaoAPFEvidencia_TipoArq = H00AC2_A410FuncaoAPFEvidencia_TipoArq[0];
               n410FuncaoAPFEvidencia_TipoArq = H00AC2_n410FuncaoAPFEvidencia_TipoArq[0];
               A409FuncaoAPFEvidencia_NomeArq = H00AC2_A409FuncaoAPFEvidencia_NomeArq[0];
               n409FuncaoAPFEvidencia_NomeArq = H00AC2_n409FuncaoAPFEvidencia_NomeArq[0];
               A407FuncaoAPFEvidencia_Descricao = H00AC2_A407FuncaoAPFEvidencia_Descricao[0];
               n407FuncaoAPFEvidencia_Descricao = H00AC2_n407FuncaoAPFEvidencia_Descricao[0];
               A360FuncaoAPF_SistemaCod = H00AC2_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H00AC2_n360FuncaoAPF_SistemaCod[0];
               A165FuncaoAPF_Codigo = H00AC2_A165FuncaoAPF_Codigo[0];
               A406FuncaoAPFEvidencia_Codigo = H00AC2_A406FuncaoAPFEvidencia_Codigo[0];
               A360FuncaoAPF_SistemaCod = H00AC2_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H00AC2_n360FuncaoAPF_SistemaCod[0];
               /* Execute user event: E18AC2 */
               E18AC2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 22;
            WBAC0( ) ;
         }
         nGXsfl_22_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV26TFFuncaoAPFEvidencia_Descricao_Sel ,
                                              AV25TFFuncaoAPFEvidencia_Descricao ,
                                              AV30TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                              AV29TFFuncaoAPFEvidencia_NomeArq ,
                                              AV34TFFuncaoAPFEvidencia_TipoArq_Sel ,
                                              AV33TFFuncaoAPFEvidencia_TipoArq ,
                                              A407FuncaoAPFEvidencia_Descricao ,
                                              A409FuncaoAPFEvidencia_NomeArq ,
                                              A410FuncaoAPFEvidencia_TipoArq ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A165FuncaoAPF_Codigo ,
                                              AV7FuncaoAPF_Codigo ,
                                              A750FuncaoAPFEvidencia_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV25TFFuncaoAPFEvidencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV25TFFuncaoAPFEvidencia_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFFuncaoAPFEvidencia_Descricao", AV25TFFuncaoAPFEvidencia_Descricao);
         lV29TFFuncaoAPFEvidencia_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV29TFFuncaoAPFEvidencia_NomeArq), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFFuncaoAPFEvidencia_NomeArq", AV29TFFuncaoAPFEvidencia_NomeArq);
         lV33TFFuncaoAPFEvidencia_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV33TFFuncaoAPFEvidencia_TipoArq), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFFuncaoAPFEvidencia_TipoArq", AV33TFFuncaoAPFEvidencia_TipoArq);
         /* Using cursor H00AC3 */
         pr_default.execute(1, new Object[] {AV7FuncaoAPF_Codigo, lV25TFFuncaoAPFEvidencia_Descricao, AV26TFFuncaoAPFEvidencia_Descricao_Sel, lV29TFFuncaoAPFEvidencia_NomeArq, AV30TFFuncaoAPFEvidencia_NomeArq_Sel, lV33TFFuncaoAPFEvidencia_TipoArq, AV34TFFuncaoAPFEvidencia_TipoArq_Sel});
         GRID_nRecordCount = H00AC3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV25TFFuncaoAPFEvidencia_Descricao, AV26TFFuncaoAPFEvidencia_Descricao_Sel, AV29TFFuncaoAPFEvidencia_NomeArq, AV30TFFuncaoAPFEvidencia_NomeArq_Sel, AV33TFFuncaoAPFEvidencia_TipoArq, AV34TFFuncaoAPFEvidencia_TipoArq_Sel, AV7FuncaoAPF_Codigo, AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV59Pgmname, AV8FuncaoAPF_SistemaCod, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV25TFFuncaoAPFEvidencia_Descricao, AV26TFFuncaoAPFEvidencia_Descricao_Sel, AV29TFFuncaoAPFEvidencia_NomeArq, AV30TFFuncaoAPFEvidencia_NomeArq_Sel, AV33TFFuncaoAPFEvidencia_TipoArq, AV34TFFuncaoAPFEvidencia_TipoArq_Sel, AV7FuncaoAPF_Codigo, AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV59Pgmname, AV8FuncaoAPF_SistemaCod, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV25TFFuncaoAPFEvidencia_Descricao, AV26TFFuncaoAPFEvidencia_Descricao_Sel, AV29TFFuncaoAPFEvidencia_NomeArq, AV30TFFuncaoAPFEvidencia_NomeArq_Sel, AV33TFFuncaoAPFEvidencia_TipoArq, AV34TFFuncaoAPFEvidencia_TipoArq_Sel, AV7FuncaoAPF_Codigo, AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV59Pgmname, AV8FuncaoAPF_SistemaCod, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV25TFFuncaoAPFEvidencia_Descricao, AV26TFFuncaoAPFEvidencia_Descricao_Sel, AV29TFFuncaoAPFEvidencia_NomeArq, AV30TFFuncaoAPFEvidencia_NomeArq_Sel, AV33TFFuncaoAPFEvidencia_TipoArq, AV34TFFuncaoAPFEvidencia_TipoArq_Sel, AV7FuncaoAPF_Codigo, AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV59Pgmname, AV8FuncaoAPF_SistemaCod, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV25TFFuncaoAPFEvidencia_Descricao, AV26TFFuncaoAPFEvidencia_Descricao_Sel, AV29TFFuncaoAPFEvidencia_NomeArq, AV30TFFuncaoAPFEvidencia_NomeArq_Sel, AV33TFFuncaoAPFEvidencia_TipoArq, AV34TFFuncaoAPFEvidencia_TipoArq_Sel, AV7FuncaoAPF_Codigo, AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV59Pgmname, AV8FuncaoAPF_SistemaCod, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPAC0( )
      {
         /* Before Start, stand alone formulas. */
         AV59Pgmname = "FuncaoAPFFuncaoAPFEvidenciaWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E16AC2 */
         E16AC2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV36DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAOAPFEVIDENCIA_DESCRICAOTITLEFILTERDATA"), AV24FuncaoAPFEvidencia_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA"), AV28FuncaoAPFEvidencia_NomeArqTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAOAPFEVIDENCIA_TIPOARQTITLEFILTERDATA"), AV32FuncaoAPFEvidencia_TipoArqTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            else
            {
               AV14OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            AV25TFFuncaoAPFEvidencia_Descricao = cgiGet( edtavTffuncaoapfevidencia_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFFuncaoAPFEvidencia_Descricao", AV25TFFuncaoAPFEvidencia_Descricao);
            AV26TFFuncaoAPFEvidencia_Descricao_Sel = cgiGet( edtavTffuncaoapfevidencia_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFFuncaoAPFEvidencia_Descricao_Sel", AV26TFFuncaoAPFEvidencia_Descricao_Sel);
            AV29TFFuncaoAPFEvidencia_NomeArq = cgiGet( edtavTffuncaoapfevidencia_nomearq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFFuncaoAPFEvidencia_NomeArq", AV29TFFuncaoAPFEvidencia_NomeArq);
            AV30TFFuncaoAPFEvidencia_NomeArq_Sel = cgiGet( edtavTffuncaoapfevidencia_nomearq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFFuncaoAPFEvidencia_NomeArq_Sel", AV30TFFuncaoAPFEvidencia_NomeArq_Sel);
            AV33TFFuncaoAPFEvidencia_TipoArq = cgiGet( edtavTffuncaoapfevidencia_tipoarq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFFuncaoAPFEvidencia_TipoArq", AV33TFFuncaoAPFEvidencia_TipoArq);
            AV34TFFuncaoAPFEvidencia_TipoArq_Sel = cgiGet( edtavTffuncaoapfevidencia_tipoarq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFFuncaoAPFEvidencia_TipoArq_Sel", AV34TFFuncaoAPFEvidencia_TipoArq_Sel);
            AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace", AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace);
            AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace", AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace);
            AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace", AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_22 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_22"), ",", "."));
            AV38GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV39GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7FuncaoAPF_Codigo"), ",", "."));
            wcpOAV8FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV8FuncaoAPF_SistemaCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_funcaoapfevidencia_descricao_Caption = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Caption");
            Ddo_funcaoapfevidencia_descricao_Tooltip = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Tooltip");
            Ddo_funcaoapfevidencia_descricao_Cls = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Cls");
            Ddo_funcaoapfevidencia_descricao_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filteredtext_set");
            Ddo_funcaoapfevidencia_descricao_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Selectedvalue_set");
            Ddo_funcaoapfevidencia_descricao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Dropdownoptionstype");
            Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_funcaoapfevidencia_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includesortasc"));
            Ddo_funcaoapfevidencia_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includesortdsc"));
            Ddo_funcaoapfevidencia_descricao_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Sortedstatus");
            Ddo_funcaoapfevidencia_descricao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includefilter"));
            Ddo_funcaoapfevidencia_descricao_Filtertype = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filtertype");
            Ddo_funcaoapfevidencia_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filterisrange"));
            Ddo_funcaoapfevidencia_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includedatalist"));
            Ddo_funcaoapfevidencia_descricao_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Datalisttype");
            Ddo_funcaoapfevidencia_descricao_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Datalistproc");
            Ddo_funcaoapfevidencia_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapfevidencia_descricao_Sortasc = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Sortasc");
            Ddo_funcaoapfevidencia_descricao_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Sortdsc");
            Ddo_funcaoapfevidencia_descricao_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Loadingdata");
            Ddo_funcaoapfevidencia_descricao_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Cleanfilter");
            Ddo_funcaoapfevidencia_descricao_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Noresultsfound");
            Ddo_funcaoapfevidencia_descricao_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Searchbuttontext");
            Ddo_funcaoapfevidencia_nomearq_Caption = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Caption");
            Ddo_funcaoapfevidencia_nomearq_Tooltip = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Tooltip");
            Ddo_funcaoapfevidencia_nomearq_Cls = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Cls");
            Ddo_funcaoapfevidencia_nomearq_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filteredtext_set");
            Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Selectedvalue_set");
            Ddo_funcaoapfevidencia_nomearq_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Dropdownoptionstype");
            Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Titlecontrolidtoreplace");
            Ddo_funcaoapfevidencia_nomearq_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includesortasc"));
            Ddo_funcaoapfevidencia_nomearq_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includesortdsc"));
            Ddo_funcaoapfevidencia_nomearq_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Sortedstatus");
            Ddo_funcaoapfevidencia_nomearq_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includefilter"));
            Ddo_funcaoapfevidencia_nomearq_Filtertype = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filtertype");
            Ddo_funcaoapfevidencia_nomearq_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filterisrange"));
            Ddo_funcaoapfevidencia_nomearq_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includedatalist"));
            Ddo_funcaoapfevidencia_nomearq_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalisttype");
            Ddo_funcaoapfevidencia_nomearq_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalistproc");
            Ddo_funcaoapfevidencia_nomearq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapfevidencia_nomearq_Sortasc = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Sortasc");
            Ddo_funcaoapfevidencia_nomearq_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Sortdsc");
            Ddo_funcaoapfevidencia_nomearq_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Loadingdata");
            Ddo_funcaoapfevidencia_nomearq_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Cleanfilter");
            Ddo_funcaoapfevidencia_nomearq_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Noresultsfound");
            Ddo_funcaoapfevidencia_nomearq_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Searchbuttontext");
            Ddo_funcaoapfevidencia_tipoarq_Caption = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Caption");
            Ddo_funcaoapfevidencia_tipoarq_Tooltip = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Tooltip");
            Ddo_funcaoapfevidencia_tipoarq_Cls = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Cls");
            Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filteredtext_set");
            Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Selectedvalue_set");
            Ddo_funcaoapfevidencia_tipoarq_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Dropdownoptionstype");
            Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Titlecontrolidtoreplace");
            Ddo_funcaoapfevidencia_tipoarq_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includesortasc"));
            Ddo_funcaoapfevidencia_tipoarq_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includesortdsc"));
            Ddo_funcaoapfevidencia_tipoarq_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Sortedstatus");
            Ddo_funcaoapfevidencia_tipoarq_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includefilter"));
            Ddo_funcaoapfevidencia_tipoarq_Filtertype = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filtertype");
            Ddo_funcaoapfevidencia_tipoarq_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filterisrange"));
            Ddo_funcaoapfevidencia_tipoarq_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includedatalist"));
            Ddo_funcaoapfevidencia_tipoarq_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Datalisttype");
            Ddo_funcaoapfevidencia_tipoarq_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Datalistproc");
            Ddo_funcaoapfevidencia_tipoarq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapfevidencia_tipoarq_Sortasc = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Sortasc");
            Ddo_funcaoapfevidencia_tipoarq_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Sortdsc");
            Ddo_funcaoapfevidencia_tipoarq_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Loadingdata");
            Ddo_funcaoapfevidencia_tipoarq_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Cleanfilter");
            Ddo_funcaoapfevidencia_tipoarq_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Noresultsfound");
            Ddo_funcaoapfevidencia_tipoarq_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_funcaoapfevidencia_descricao_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Activeeventkey");
            Ddo_funcaoapfevidencia_descricao_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filteredtext_get");
            Ddo_funcaoapfevidencia_descricao_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Selectedvalue_get");
            Ddo_funcaoapfevidencia_nomearq_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Activeeventkey");
            Ddo_funcaoapfevidencia_nomearq_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filteredtext_get");
            Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Selectedvalue_get");
            Ddo_funcaoapfevidencia_tipoarq_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Activeeventkey");
            Ddo_funcaoapfevidencia_tipoarq_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filteredtext_get");
            Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_DESCRICAO"), AV25TFFuncaoAPFEvidencia_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL"), AV26TFFuncaoAPFEvidencia_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ"), AV29TFFuncaoAPFEvidencia_NomeArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL"), AV30TFFuncaoAPFEvidencia_NomeArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_TIPOARQ"), AV33TFFuncaoAPFEvidencia_TipoArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL"), AV34TFFuncaoAPFEvidencia_TipoArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E16AC2 */
         E16AC2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16AC2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTffuncaoapfevidencia_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfevidencia_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_descricao_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfevidencia_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_descricao_sel_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_nomearq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfevidencia_nomearq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_nomearq_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_nomearq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfevidencia_nomearq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_nomearq_sel_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_tipoarq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfevidencia_tipoarq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_tipoarq_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_tipoarq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfevidencia_tipoarq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_tipoarq_sel_Visible), 5, 0)));
         Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFEvidencia_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_descricao_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace);
         AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace = Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace", AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace);
         edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFEvidencia_NomeArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_nomearq_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace);
         AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace = Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace", AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace);
         edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFEvidencia_TipoArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace);
         AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace = Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace", AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace);
         edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV36DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV36DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E17AC2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV24FuncaoAPFEvidencia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28FuncaoAPFEvidencia_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32FuncaoAPFEvidencia_TipoArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoAPFEvidencia_Descricao_Titleformat = 2;
         edtFuncaoAPFEvidencia_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFEvidencia_Descricao_Internalname, "Title", edtFuncaoAPFEvidencia_Descricao_Title);
         edtFuncaoAPFEvidencia_NomeArq_Titleformat = 2;
         edtFuncaoAPFEvidencia_NomeArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Arquivo", AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFEvidencia_NomeArq_Internalname, "Title", edtFuncaoAPFEvidencia_NomeArq_Title);
         edtFuncaoAPFEvidencia_TipoArq_Titleformat = 2;
         edtFuncaoAPFEvidencia_TipoArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFEvidencia_TipoArq_Internalname, "Title", edtFuncaoAPFEvidencia_TipoArq_Title);
         AV38GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38GridCurrentPage), 10, 0)));
         AV39GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV24FuncaoAPFEvidencia_DescricaoTitleFilterData", AV24FuncaoAPFEvidencia_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV28FuncaoAPFEvidencia_NomeArqTitleFilterData", AV28FuncaoAPFEvidencia_NomeArqTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV32FuncaoAPFEvidencia_TipoArqTitleFilterData", AV32FuncaoAPFEvidencia_TipoArqTitleFilterData);
      }

      protected void E11AC2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV37PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV37PageToGo) ;
         }
      }

      protected void E12AC2( )
      {
         /* Ddo_funcaoapfevidencia_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfevidencia_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_descricao_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfevidencia_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_descricao_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV25TFFuncaoAPFEvidencia_Descricao = Ddo_funcaoapfevidencia_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFFuncaoAPFEvidencia_Descricao", AV25TFFuncaoAPFEvidencia_Descricao);
            AV26TFFuncaoAPFEvidencia_Descricao_Sel = Ddo_funcaoapfevidencia_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFFuncaoAPFEvidencia_Descricao_Sel", AV26TFFuncaoAPFEvidencia_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E13AC2( )
      {
         /* Ddo_funcaoapfevidencia_nomearq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_nomearq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfevidencia_nomearq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_nomearq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_nomearq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfevidencia_nomearq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_nomearq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_nomearq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV29TFFuncaoAPFEvidencia_NomeArq = Ddo_funcaoapfevidencia_nomearq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFFuncaoAPFEvidencia_NomeArq", AV29TFFuncaoAPFEvidencia_NomeArq);
            AV30TFFuncaoAPFEvidencia_NomeArq_Sel = Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFFuncaoAPFEvidencia_NomeArq_Sel", AV30TFFuncaoAPFEvidencia_NomeArq_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E14AC2( )
      {
         /* Ddo_funcaoapfevidencia_tipoarq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_tipoarq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfevidencia_tipoarq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_tipoarq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_tipoarq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfevidencia_tipoarq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_tipoarq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_tipoarq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV33TFFuncaoAPFEvidencia_TipoArq = Ddo_funcaoapfevidencia_tipoarq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFFuncaoAPFEvidencia_TipoArq", AV33TFFuncaoAPFEvidencia_TipoArq);
            AV34TFFuncaoAPFEvidencia_TipoArq_Sel = Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFFuncaoAPFEvidencia_TipoArq_Sel", AV34TFFuncaoAPFEvidencia_TipoArq_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E18AC2( )
      {
         /* Grid_Load Routine */
         AV16Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV16Update);
         AV56Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("funcaoapfevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A406FuncaoAPFEvidencia_Codigo) + "," + UrlEncode("" +A165FuncaoAPF_Codigo);
         AV17Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV17Delete);
         AV57Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("funcaoapfevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A406FuncaoAPFEvidencia_Codigo) + "," + UrlEncode("" +A165FuncaoAPF_Codigo);
         AV18Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV18Display);
         AV58Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewfuncaoapfevidencia.aspx") + "?" + UrlEncode("" +A406FuncaoAPFEvidencia_Codigo) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A360FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 22;
         }
         sendrow_222( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_22_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(22, GridRow);
         }
      }

      protected void E15AC2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("funcaoapfevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV7FuncaoAPF_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_funcaoapfevidencia_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_descricao_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_descricao_Sortedstatus);
         Ddo_funcaoapfevidencia_nomearq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_nomearq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_nomearq_Sortedstatus);
         Ddo_funcaoapfevidencia_tipoarq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_tipoarq_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 1 )
         {
            Ddo_funcaoapfevidencia_descricao_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_descricao_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_descricao_Sortedstatus);
         }
         else if ( AV14OrderedBy == 2 )
         {
            Ddo_funcaoapfevidencia_nomearq_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_nomearq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_nomearq_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_funcaoapfevidencia_tipoarq_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_tipoarq_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV19Session.Get(AV59Pgmname+"GridState"), "") == 0 )
         {
            AV12GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV59Pgmname+"GridState"), "");
         }
         else
         {
            AV12GridState.FromXml(AV19Session.Get(AV59Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV12GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV12GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV60GXV1 = 1;
         while ( AV60GXV1 <= AV12GridState.gxTpr_Filtervalues.Count )
         {
            AV13GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV12GridState.gxTpr_Filtervalues.Item(AV60GXV1));
            if ( StringUtil.StrCmp(AV13GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
            {
               AV25TFFuncaoAPFEvidencia_Descricao = AV13GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFFuncaoAPFEvidencia_Descricao", AV25TFFuncaoAPFEvidencia_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFFuncaoAPFEvidencia_Descricao)) )
               {
                  Ddo_funcaoapfevidencia_descricao_Filteredtext_set = AV25TFFuncaoAPFEvidencia_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_descricao_Internalname, "FilteredText_set", Ddo_funcaoapfevidencia_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV13GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL") == 0 )
            {
               AV26TFFuncaoAPFEvidencia_Descricao_Sel = AV13GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFFuncaoAPFEvidencia_Descricao_Sel", AV26TFFuncaoAPFEvidencia_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFFuncaoAPFEvidencia_Descricao_Sel)) )
               {
                  Ddo_funcaoapfevidencia_descricao_Selectedvalue_set = AV26TFFuncaoAPFEvidencia_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_descricao_Internalname, "SelectedValue_set", Ddo_funcaoapfevidencia_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV13GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_NOMEARQ") == 0 )
            {
               AV29TFFuncaoAPFEvidencia_NomeArq = AV13GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFFuncaoAPFEvidencia_NomeArq", AV29TFFuncaoAPFEvidencia_NomeArq);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFFuncaoAPFEvidencia_NomeArq)) )
               {
                  Ddo_funcaoapfevidencia_nomearq_Filteredtext_set = AV29TFFuncaoAPFEvidencia_NomeArq;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_nomearq_Internalname, "FilteredText_set", Ddo_funcaoapfevidencia_nomearq_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV13GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL") == 0 )
            {
               AV30TFFuncaoAPFEvidencia_NomeArq_Sel = AV13GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFFuncaoAPFEvidencia_NomeArq_Sel", AV30TFFuncaoAPFEvidencia_NomeArq_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFFuncaoAPFEvidencia_NomeArq_Sel)) )
               {
                  Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set = AV30TFFuncaoAPFEvidencia_NomeArq_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_nomearq_Internalname, "SelectedValue_set", Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV13GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_TIPOARQ") == 0 )
            {
               AV33TFFuncaoAPFEvidencia_TipoArq = AV13GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFFuncaoAPFEvidencia_TipoArq", AV33TFFuncaoAPFEvidencia_TipoArq);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFFuncaoAPFEvidencia_TipoArq)) )
               {
                  Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set = AV33TFFuncaoAPFEvidencia_TipoArq;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "FilteredText_set", Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV13GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL") == 0 )
            {
               AV34TFFuncaoAPFEvidencia_TipoArq_Sel = AV13GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFFuncaoAPFEvidencia_TipoArq_Sel", AV34TFFuncaoAPFEvidencia_TipoArq_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFFuncaoAPFEvidencia_TipoArq_Sel)) )
               {
                  Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set = AV34TFFuncaoAPFEvidencia_TipoArq_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "SelectedValue_set", Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set);
               }
            }
            AV60GXV1 = (int)(AV60GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV12GridState.FromXml(AV19Session.Get(AV59Pgmname+"GridState"), "");
         AV12GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV12GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV12GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFFuncaoAPFEvidencia_Descricao)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_DESCRICAO";
            AV13GridStateFilterValue.gxTpr_Value = AV25TFFuncaoAPFEvidencia_Descricao;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFFuncaoAPFEvidencia_Descricao_Sel)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL";
            AV13GridStateFilterValue.gxTpr_Value = AV26TFFuncaoAPFEvidencia_Descricao_Sel;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFFuncaoAPFEvidencia_NomeArq)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_NOMEARQ";
            AV13GridStateFilterValue.gxTpr_Value = AV29TFFuncaoAPFEvidencia_NomeArq;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFFuncaoAPFEvidencia_NomeArq_Sel)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL";
            AV13GridStateFilterValue.gxTpr_Value = AV30TFFuncaoAPFEvidencia_NomeArq_Sel;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFFuncaoAPFEvidencia_TipoArq)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_TIPOARQ";
            AV13GridStateFilterValue.gxTpr_Value = AV33TFFuncaoAPFEvidencia_TipoArq;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFFuncaoAPFEvidencia_TipoArq_Sel)) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL";
            AV13GridStateFilterValue.gxTpr_Value = AV34TFFuncaoAPFEvidencia_TipoArq_Sel;
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! (0==AV7FuncaoAPF_Codigo) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "PARM_&FUNCAOAPF_CODIGO";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         if ( ! (0==AV8FuncaoAPF_SistemaCod) )
         {
            AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV13GridStateFilterValue.gxTpr_Name = "PARM_&FUNCAOAPF_SISTEMACOD";
            AV13GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV8FuncaoAPF_SistemaCod), 6, 0);
            AV12GridState.gxTpr_Filtervalues.Add(AV13GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV59Pgmname+"GridState",  AV12GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10TrnContext.gxTpr_Callerobject = AV59Pgmname;
         AV10TrnContext.gxTpr_Callerondelete = true;
         AV10TrnContext.gxTpr_Callerurl = AV9HTTPRequest.ScriptName+"?"+AV9HTTPRequest.QueryString;
         AV10TrnContext.gxTpr_Transactionname = "FuncaoAPFEvidencia";
         AV11TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11TrnContextAtt.gxTpr_Attributename = "FuncaoAPF_Codigo";
         AV11TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0);
         AV10TrnContext.gxTpr_Attributes.Add(AV11TrnContextAtt, 0);
         AV19Session.Set("TrnContext", AV10TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_AC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_7_AC2( true) ;
         }
         else
         {
            wb_table2_7_AC2( false) ;
         }
         return  ;
      }

      protected void wb_table2_7_AC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_19_AC2( true) ;
         }
         else
         {
            wb_table3_19_AC2( false) ;
         }
         return  ;
      }

      protected void wb_table3_19_AC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFFuncaoAPFEvidenciaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AC2e( true) ;
         }
         else
         {
            wb_table1_2_AC2e( false) ;
         }
      }

      protected void wb_table3_19_AC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"22\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Funcao APFEvidencia_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Fun��o de Transa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFEvidencia_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFEvidencia_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFEvidencia_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFEvidencia_NomeArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFEvidencia_NomeArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFEvidencia_NomeArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFEvidencia_TipoArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFEvidencia_TipoArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFEvidencia_TipoArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV16Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV17Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV18Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A407FuncaoAPFEvidencia_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFEvidencia_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFEvidencia_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFEvidencia_NomeArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFEvidencia_NomeArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFEvidencia_TipoArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFEvidencia_TipoArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 22 )
         {
            wbEnd = 0;
            nRC_GXsfl_22 = (short)(nGXsfl_22_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_19_AC2e( true) ;
         }
         else
         {
            wb_table3_19_AC2e( false) ;
         }
      }

      protected void wb_table2_7_AC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_10_AC2( true) ;
         }
         else
         {
            wb_table4_10_AC2( false) ;
         }
         return  ;
      }

      protected void wb_table4_10_AC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_AC2( true) ;
         }
         else
         {
            wb_table5_14_AC2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_AC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_7_AC2e( true) ;
         }
         else
         {
            wb_table2_7_AC2e( false) ;
         }
      }

      protected void wb_table5_14_AC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_AC2e( true) ;
         }
         else
         {
            wb_table5_14_AC2e( false) ;
         }
      }

      protected void wb_table4_10_AC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_10_AC2e( true) ;
         }
         else
         {
            wb_table4_10_AC2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0)));
         AV8FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoAPF_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAC2( ) ;
         WSAC2( ) ;
         WEAC2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7FuncaoAPF_Codigo = (String)((String)getParm(obj,0));
         sCtrlAV8FuncaoAPF_SistemaCod = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAAC2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "funcaoapffuncaoapfevidenciawc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAAC2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0)));
            AV8FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoAPF_SistemaCod), 6, 0)));
         }
         wcpOAV7FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7FuncaoAPF_Codigo"), ",", "."));
         wcpOAV8FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV8FuncaoAPF_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7FuncaoAPF_Codigo != wcpOAV7FuncaoAPF_Codigo ) || ( AV8FuncaoAPF_SistemaCod != wcpOAV8FuncaoAPF_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7FuncaoAPF_Codigo = AV7FuncaoAPF_Codigo;
         wcpOAV8FuncaoAPF_SistemaCod = AV8FuncaoAPF_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7FuncaoAPF_Codigo = cgiGet( sPrefix+"AV7FuncaoAPF_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7FuncaoAPF_Codigo) > 0 )
         {
            AV7FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7FuncaoAPF_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0)));
         }
         else
         {
            AV7FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7FuncaoAPF_Codigo_PARM"), ",", "."));
         }
         sCtrlAV8FuncaoAPF_SistemaCod = cgiGet( sPrefix+"AV8FuncaoAPF_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV8FuncaoAPF_SistemaCod) > 0 )
         {
            AV8FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV8FuncaoAPF_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoAPF_SistemaCod), 6, 0)));
         }
         else
         {
            AV8FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV8FuncaoAPF_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAAC2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSAC2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSAC2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7FuncaoAPF_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoAPF_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7FuncaoAPF_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7FuncaoAPF_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7FuncaoAPF_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV8FuncaoAPF_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV8FuncaoAPF_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV8FuncaoAPF_SistemaCod_CTRL", StringUtil.RTrim( sCtrlAV8FuncaoAPF_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEAC2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181250615");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("funcaoapffuncaoapfevidenciawc.js", "?20205181250615");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_222( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_22_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_22_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_22_idx;
         edtFuncaoAPFEvidencia_Codigo_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_CODIGO_"+sGXsfl_22_idx;
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO_"+sGXsfl_22_idx;
         edtFuncaoAPF_SistemaCod_Internalname = sPrefix+"FUNCAOAPF_SISTEMACOD_"+sGXsfl_22_idx;
         edtFuncaoAPFEvidencia_Descricao_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_DESCRICAO_"+sGXsfl_22_idx;
         edtFuncaoAPFEvidencia_NomeArq_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_NOMEARQ_"+sGXsfl_22_idx;
         edtFuncaoAPFEvidencia_TipoArq_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_TIPOARQ_"+sGXsfl_22_idx;
      }

      protected void SubsflControlProps_fel_222( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_22_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_22_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_22_fel_idx;
         edtFuncaoAPFEvidencia_Codigo_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_CODIGO_"+sGXsfl_22_fel_idx;
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO_"+sGXsfl_22_fel_idx;
         edtFuncaoAPF_SistemaCod_Internalname = sPrefix+"FUNCAOAPF_SISTEMACOD_"+sGXsfl_22_fel_idx;
         edtFuncaoAPFEvidencia_Descricao_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_DESCRICAO_"+sGXsfl_22_fel_idx;
         edtFuncaoAPFEvidencia_NomeArq_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_NOMEARQ_"+sGXsfl_22_fel_idx;
         edtFuncaoAPFEvidencia_TipoArq_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_TIPOARQ_"+sGXsfl_22_fel_idx;
      }

      protected void sendrow_222( )
      {
         SubsflControlProps_222( ) ;
         WBAC0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_22_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_22_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_22_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV16Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV56Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16Update)) ? AV56Update_GXI : context.PathToRelativeUrl( AV16Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV16Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV17Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV57Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete)) ? AV57Delete_GXI : context.PathToRelativeUrl( AV17Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV17Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV18Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV18Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV58Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV18Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV18Display)) ? AV58Display_GXI : context.PathToRelativeUrl( AV18Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV18Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFEvidencia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A406FuncaoAPFEvidencia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFEvidencia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)22,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)22,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)22,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFEvidencia_Descricao_Internalname,(String)A407FuncaoAPFEvidencia_Descricao,(String)A407FuncaoAPFEvidencia_Descricao,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFEvidencia_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)22,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFEvidencia_NomeArq_Internalname,StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFEvidencia_NomeArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)22,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFEvidencia_TipoArq_Internalname,StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFEvidencia_TipoArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)22,(short)1,(short)-1,(short)-1,(bool)true,(String)"TipoArq",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_CODIGO"+"_"+sGXsfl_22_idx, GetSecureSignedToken( sPrefix+sGXsfl_22_idx, context.localUtil.Format( (decimal)(A406FuncaoAPFEvidencia_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_CODIGO"+"_"+sGXsfl_22_idx, GetSecureSignedToken( sPrefix+sGXsfl_22_idx, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_DESCRICAO"+"_"+sGXsfl_22_idx, GetSecureSignedToken( sPrefix+sGXsfl_22_idx, A407FuncaoAPFEvidencia_Descricao));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_NOMEARQ"+"_"+sGXsfl_22_idx, GetSecureSignedToken( sPrefix+sGXsfl_22_idx, StringUtil.RTrim( context.localUtil.Format( A409FuncaoAPFEvidencia_NomeArq, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_TIPOARQ"+"_"+sGXsfl_22_idx, GetSecureSignedToken( sPrefix+sGXsfl_22_idx, StringUtil.RTrim( context.localUtil.Format( A410FuncaoAPFEvidencia_TipoArq, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_22_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_22_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_22_idx+1));
            sGXsfl_22_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_22_idx), 4, 0)), 4, "0");
            SubsflControlProps_222( ) ;
         }
         /* End function sendrow_222 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtFuncaoAPFEvidencia_Codigo_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_CODIGO";
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO";
         edtFuncaoAPF_SistemaCod_Internalname = sPrefix+"FUNCAOAPF_SISTEMACOD";
         edtFuncaoAPFEvidencia_Descricao_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_DESCRICAO";
         edtFuncaoAPFEvidencia_NomeArq_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_NOMEARQ";
         edtFuncaoAPFEvidencia_TipoArq_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_TIPOARQ";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTffuncaoapfevidencia_descricao_Internalname = sPrefix+"vTFFUNCAOAPFEVIDENCIA_DESCRICAO";
         edtavTffuncaoapfevidencia_descricao_sel_Internalname = sPrefix+"vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL";
         edtavTffuncaoapfevidencia_nomearq_Internalname = sPrefix+"vTFFUNCAOAPFEVIDENCIA_NOMEARQ";
         edtavTffuncaoapfevidencia_nomearq_sel_Internalname = sPrefix+"vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL";
         edtavTffuncaoapfevidencia_tipoarq_Internalname = sPrefix+"vTFFUNCAOAPFEVIDENCIA_TIPOARQ";
         edtavTffuncaoapfevidencia_tipoarq_sel_Internalname = sPrefix+"vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL";
         Ddo_funcaoapfevidencia_descricao_Internalname = sPrefix+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAO";
         edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapfevidencia_nomearq_Internalname = sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ";
         edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapfevidencia_tipoarq_Internalname = sPrefix+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQ";
         edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtFuncaoAPFEvidencia_TipoArq_Jsonclick = "";
         edtFuncaoAPFEvidencia_NomeArq_Jsonclick = "";
         edtFuncaoAPFEvidencia_Descricao_Jsonclick = "";
         edtFuncaoAPF_SistemaCod_Jsonclick = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         edtFuncaoAPFEvidencia_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtFuncaoAPFEvidencia_TipoArq_Titleformat = 0;
         edtFuncaoAPFEvidencia_NomeArq_Titleformat = 0;
         edtFuncaoAPFEvidencia_Descricao_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtFuncaoAPFEvidencia_TipoArq_Title = "Tipo";
         edtFuncaoAPFEvidencia_NomeArq_Title = "Arquivo";
         edtFuncaoAPFEvidencia_Descricao_Title = "Descri��o";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaoapfevidencia_tipoarq_sel_Jsonclick = "";
         edtavTffuncaoapfevidencia_tipoarq_sel_Visible = 1;
         edtavTffuncaoapfevidencia_tipoarq_Jsonclick = "";
         edtavTffuncaoapfevidencia_tipoarq_Visible = 1;
         edtavTffuncaoapfevidencia_nomearq_sel_Jsonclick = "";
         edtavTffuncaoapfevidencia_nomearq_sel_Visible = 1;
         edtavTffuncaoapfevidencia_nomearq_Jsonclick = "";
         edtavTffuncaoapfevidencia_nomearq_Visible = 1;
         edtavTffuncaoapfevidencia_descricao_sel_Visible = 1;
         edtavTffuncaoapfevidencia_descricao_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         Ddo_funcaoapfevidencia_tipoarq_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfevidencia_tipoarq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapfevidencia_tipoarq_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfevidencia_tipoarq_Loadingdata = "Carregando dados...";
         Ddo_funcaoapfevidencia_tipoarq_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfevidencia_tipoarq_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfevidencia_tipoarq_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapfevidencia_tipoarq_Datalistproc = "GetFuncaoAPFFuncaoAPFEvidenciaWCFilterData";
         Ddo_funcaoapfevidencia_tipoarq_Datalisttype = "Dynamic";
         Ddo_funcaoapfevidencia_tipoarq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_tipoarq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapfevidencia_tipoarq_Filtertype = "Character";
         Ddo_funcaoapfevidencia_tipoarq_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_tipoarq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_tipoarq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfevidencia_tipoarq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfevidencia_tipoarq_Cls = "ColumnSettings";
         Ddo_funcaoapfevidencia_tipoarq_Tooltip = "Op��es";
         Ddo_funcaoapfevidencia_tipoarq_Caption = "";
         Ddo_funcaoapfevidencia_nomearq_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfevidencia_nomearq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapfevidencia_nomearq_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfevidencia_nomearq_Loadingdata = "Carregando dados...";
         Ddo_funcaoapfevidencia_nomearq_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfevidencia_nomearq_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfevidencia_nomearq_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapfevidencia_nomearq_Datalistproc = "GetFuncaoAPFFuncaoAPFEvidenciaWCFilterData";
         Ddo_funcaoapfevidencia_nomearq_Datalisttype = "Dynamic";
         Ddo_funcaoapfevidencia_nomearq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_nomearq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapfevidencia_nomearq_Filtertype = "Character";
         Ddo_funcaoapfevidencia_nomearq_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_nomearq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_nomearq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfevidencia_nomearq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfevidencia_nomearq_Cls = "ColumnSettings";
         Ddo_funcaoapfevidencia_nomearq_Tooltip = "Op��es";
         Ddo_funcaoapfevidencia_nomearq_Caption = "";
         Ddo_funcaoapfevidencia_descricao_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfevidencia_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapfevidencia_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfevidencia_descricao_Loadingdata = "Carregando dados...";
         Ddo_funcaoapfevidencia_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfevidencia_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfevidencia_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapfevidencia_descricao_Datalistproc = "GetFuncaoAPFFuncaoAPFEvidenciaWCFilterData";
         Ddo_funcaoapfevidencia_descricao_Datalisttype = "Dynamic";
         Ddo_funcaoapfevidencia_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapfevidencia_descricao_Filtertype = "Character";
         Ddo_funcaoapfevidencia_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfevidencia_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfevidencia_descricao_Cls = "ColumnSettings";
         Ddo_funcaoapfevidencia_descricao_Tooltip = "Op��es";
         Ddo_funcaoapfevidencia_descricao_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV25TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV26TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV29TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV30TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV33TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV34TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV7FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV24FuncaoAPFEvidencia_DescricaoTitleFilterData',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV28FuncaoAPFEvidencia_NomeArqTitleFilterData',fld:'vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA',pic:'',nv:null},{av:'AV32FuncaoAPFEvidencia_TipoArqTitleFilterData',fld:'vFUNCAOAPFEVIDENCIA_TIPOARQTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPFEvidencia_Descricao_Titleformat',ctrl:'FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'Titleformat'},{av:'edtFuncaoAPFEvidencia_Descricao_Title',ctrl:'FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'Title'},{av:'edtFuncaoAPFEvidencia_NomeArq_Titleformat',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Titleformat'},{av:'edtFuncaoAPFEvidencia_NomeArq_Title',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Title'},{av:'edtFuncaoAPFEvidencia_TipoArq_Titleformat',ctrl:'FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'Titleformat'},{av:'edtFuncaoAPFEvidencia_TipoArq_Title',ctrl:'FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'Title'},{av:'AV38GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV39GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11AC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV25TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV26TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV29TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV30TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV33TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV34TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV7FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV8FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FUNCAOAPFEVIDENCIA_DESCRICAO.ONOPTIONCLICKED","{handler:'E12AC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV25TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV26TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV29TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV30TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV33TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV34TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV7FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV8FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_funcaoapfevidencia_descricao_Activeeventkey',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfevidencia_descricao_Filteredtext_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_funcaoapfevidencia_descricao_Selectedvalue_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfevidencia_descricao_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'SortedStatus'},{av:'AV25TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV26TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_funcaoapfevidencia_nomearq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_funcaoapfevidencia_tipoarq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPFEVIDENCIA_NOMEARQ.ONOPTIONCLICKED","{handler:'E13AC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV25TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV26TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV29TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV30TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV33TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV34TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV7FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV8FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_funcaoapfevidencia_nomearq_Activeeventkey',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfevidencia_nomearq_Filteredtext_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'FilteredText_get'},{av:'Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfevidencia_nomearq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'SortedStatus'},{av:'AV29TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV30TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_funcaoapfevidencia_descricao_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_funcaoapfevidencia_tipoarq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPFEVIDENCIA_TIPOARQ.ONOPTIONCLICKED","{handler:'E14AC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV25TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV26TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV29TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV30TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV33TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV34TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV7FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV8FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_funcaoapfevidencia_tipoarq_Activeeventkey',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfevidencia_tipoarq_Filteredtext_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'FilteredText_get'},{av:'Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfevidencia_tipoarq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'SortedStatus'},{av:'AV33TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV34TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'Ddo_funcaoapfevidencia_descricao_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_funcaoapfevidencia_nomearq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E18AC2',iparms:[{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV16Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV17Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV18Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E15AC2',iparms:[{av:'AV7FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_funcaoapfevidencia_descricao_Activeeventkey = "";
         Ddo_funcaoapfevidencia_descricao_Filteredtext_get = "";
         Ddo_funcaoapfevidencia_descricao_Selectedvalue_get = "";
         Ddo_funcaoapfevidencia_nomearq_Activeeventkey = "";
         Ddo_funcaoapfevidencia_nomearq_Filteredtext_get = "";
         Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get = "";
         Ddo_funcaoapfevidencia_tipoarq_Activeeventkey = "";
         Ddo_funcaoapfevidencia_tipoarq_Filteredtext_get = "";
         Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV25TFFuncaoAPFEvidencia_Descricao = "";
         AV26TFFuncaoAPFEvidencia_Descricao_Sel = "";
         AV29TFFuncaoAPFEvidencia_NomeArq = "";
         AV30TFFuncaoAPFEvidencia_NomeArq_Sel = "";
         AV33TFFuncaoAPFEvidencia_TipoArq = "";
         AV34TFFuncaoAPFEvidencia_TipoArq_Sel = "";
         AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace = "";
         AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace = "";
         AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace = "";
         AV59Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV36DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV24FuncaoAPFEvidencia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV28FuncaoAPFEvidencia_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32FuncaoAPFEvidencia_TipoArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaoapfevidencia_descricao_Filteredtext_set = "";
         Ddo_funcaoapfevidencia_descricao_Selectedvalue_set = "";
         Ddo_funcaoapfevidencia_descricao_Sortedstatus = "";
         Ddo_funcaoapfevidencia_nomearq_Filteredtext_set = "";
         Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set = "";
         Ddo_funcaoapfevidencia_nomearq_Sortedstatus = "";
         Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set = "";
         Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set = "";
         Ddo_funcaoapfevidencia_tipoarq_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV16Update = "";
         AV56Update_GXI = "";
         AV17Delete = "";
         AV57Delete_GXI = "";
         AV18Display = "";
         AV58Display_GXI = "";
         A407FuncaoAPFEvidencia_Descricao = "";
         A409FuncaoAPFEvidencia_NomeArq = "";
         A410FuncaoAPFEvidencia_TipoArq = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV25TFFuncaoAPFEvidencia_Descricao = "";
         lV29TFFuncaoAPFEvidencia_NomeArq = "";
         lV33TFFuncaoAPFEvidencia_TipoArq = "";
         H00AC2_A750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         H00AC2_n750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         H00AC2_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         H00AC2_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         H00AC2_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         H00AC2_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         H00AC2_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         H00AC2_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         H00AC2_A360FuncaoAPF_SistemaCod = new int[1] ;
         H00AC2_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H00AC2_A165FuncaoAPF_Codigo = new int[1] ;
         H00AC2_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         H00AC3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV19Session = context.GetSession();
         AV12GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV13GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9HTTPRequest = new GxHttpRequest( context);
         AV11TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         imgInsert_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7FuncaoAPF_Codigo = "";
         sCtrlAV8FuncaoAPF_SistemaCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaoapffuncaoapfevidenciawc__default(),
            new Object[][] {
                new Object[] {
               H00AC2_A750FuncaoAPFEvidencia_Ativo, H00AC2_n750FuncaoAPFEvidencia_Ativo, H00AC2_A410FuncaoAPFEvidencia_TipoArq, H00AC2_n410FuncaoAPFEvidencia_TipoArq, H00AC2_A409FuncaoAPFEvidencia_NomeArq, H00AC2_n409FuncaoAPFEvidencia_NomeArq, H00AC2_A407FuncaoAPFEvidencia_Descricao, H00AC2_n407FuncaoAPFEvidencia_Descricao, H00AC2_A360FuncaoAPF_SistemaCod, H00AC2_n360FuncaoAPF_SistemaCod,
               H00AC2_A165FuncaoAPF_Codigo, H00AC2_A406FuncaoAPFEvidencia_Codigo
               }
               , new Object[] {
               H00AC3_AGRID_nRecordCount
               }
            }
         );
         AV59Pgmname = "FuncaoAPFFuncaoAPFEvidenciaWC";
         /* GeneXus formulas. */
         AV59Pgmname = "FuncaoAPFFuncaoAPFEvidenciaWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_22 ;
      private short nGXsfl_22_idx=1 ;
      private short AV14OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_22_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtFuncaoAPFEvidencia_Descricao_Titleformat ;
      private short edtFuncaoAPFEvidencia_NomeArq_Titleformat ;
      private short edtFuncaoAPFEvidencia_TipoArq_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7FuncaoAPF_Codigo ;
      private int AV8FuncaoAPF_SistemaCod ;
      private int wcpOAV7FuncaoAPF_Codigo ;
      private int wcpOAV8FuncaoAPF_SistemaCod ;
      private int subGrid_Rows ;
      private int A406FuncaoAPFEvidencia_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int A360FuncaoAPF_SistemaCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_funcaoapfevidencia_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_funcaoapfevidencia_nomearq_Datalistupdateminimumcharacters ;
      private int Ddo_funcaoapfevidencia_tipoarq_Datalistupdateminimumcharacters ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTffuncaoapfevidencia_descricao_Visible ;
      private int edtavTffuncaoapfevidencia_descricao_sel_Visible ;
      private int edtavTffuncaoapfevidencia_nomearq_Visible ;
      private int edtavTffuncaoapfevidencia_nomearq_sel_Visible ;
      private int edtavTffuncaoapfevidencia_tipoarq_Visible ;
      private int edtavTffuncaoapfevidencia_tipoarq_sel_Visible ;
      private int edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV37PageToGo ;
      private int AV60GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV38GridCurrentPage ;
      private long AV39GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_funcaoapfevidencia_descricao_Activeeventkey ;
      private String Ddo_funcaoapfevidencia_descricao_Filteredtext_get ;
      private String Ddo_funcaoapfevidencia_descricao_Selectedvalue_get ;
      private String Ddo_funcaoapfevidencia_nomearq_Activeeventkey ;
      private String Ddo_funcaoapfevidencia_nomearq_Filteredtext_get ;
      private String Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get ;
      private String Ddo_funcaoapfevidencia_tipoarq_Activeeventkey ;
      private String Ddo_funcaoapfevidencia_tipoarq_Filteredtext_get ;
      private String Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_22_idx="0001" ;
      private String AV29TFFuncaoAPFEvidencia_NomeArq ;
      private String AV30TFFuncaoAPFEvidencia_NomeArq_Sel ;
      private String AV33TFFuncaoAPFEvidencia_TipoArq ;
      private String AV34TFFuncaoAPFEvidencia_TipoArq_Sel ;
      private String AV59Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_funcaoapfevidencia_descricao_Caption ;
      private String Ddo_funcaoapfevidencia_descricao_Tooltip ;
      private String Ddo_funcaoapfevidencia_descricao_Cls ;
      private String Ddo_funcaoapfevidencia_descricao_Filteredtext_set ;
      private String Ddo_funcaoapfevidencia_descricao_Selectedvalue_set ;
      private String Ddo_funcaoapfevidencia_descricao_Dropdownoptionstype ;
      private String Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfevidencia_descricao_Sortedstatus ;
      private String Ddo_funcaoapfevidencia_descricao_Filtertype ;
      private String Ddo_funcaoapfevidencia_descricao_Datalisttype ;
      private String Ddo_funcaoapfevidencia_descricao_Datalistproc ;
      private String Ddo_funcaoapfevidencia_descricao_Sortasc ;
      private String Ddo_funcaoapfevidencia_descricao_Sortdsc ;
      private String Ddo_funcaoapfevidencia_descricao_Loadingdata ;
      private String Ddo_funcaoapfevidencia_descricao_Cleanfilter ;
      private String Ddo_funcaoapfevidencia_descricao_Noresultsfound ;
      private String Ddo_funcaoapfevidencia_descricao_Searchbuttontext ;
      private String Ddo_funcaoapfevidencia_nomearq_Caption ;
      private String Ddo_funcaoapfevidencia_nomearq_Tooltip ;
      private String Ddo_funcaoapfevidencia_nomearq_Cls ;
      private String Ddo_funcaoapfevidencia_nomearq_Filteredtext_set ;
      private String Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set ;
      private String Ddo_funcaoapfevidencia_nomearq_Dropdownoptionstype ;
      private String Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfevidencia_nomearq_Sortedstatus ;
      private String Ddo_funcaoapfevidencia_nomearq_Filtertype ;
      private String Ddo_funcaoapfevidencia_nomearq_Datalisttype ;
      private String Ddo_funcaoapfevidencia_nomearq_Datalistproc ;
      private String Ddo_funcaoapfevidencia_nomearq_Sortasc ;
      private String Ddo_funcaoapfevidencia_nomearq_Sortdsc ;
      private String Ddo_funcaoapfevidencia_nomearq_Loadingdata ;
      private String Ddo_funcaoapfevidencia_nomearq_Cleanfilter ;
      private String Ddo_funcaoapfevidencia_nomearq_Noresultsfound ;
      private String Ddo_funcaoapfevidencia_nomearq_Searchbuttontext ;
      private String Ddo_funcaoapfevidencia_tipoarq_Caption ;
      private String Ddo_funcaoapfevidencia_tipoarq_Tooltip ;
      private String Ddo_funcaoapfevidencia_tipoarq_Cls ;
      private String Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set ;
      private String Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set ;
      private String Ddo_funcaoapfevidencia_tipoarq_Dropdownoptionstype ;
      private String Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfevidencia_tipoarq_Sortedstatus ;
      private String Ddo_funcaoapfevidencia_tipoarq_Filtertype ;
      private String Ddo_funcaoapfevidencia_tipoarq_Datalisttype ;
      private String Ddo_funcaoapfevidencia_tipoarq_Datalistproc ;
      private String Ddo_funcaoapfevidencia_tipoarq_Sortasc ;
      private String Ddo_funcaoapfevidencia_tipoarq_Sortdsc ;
      private String Ddo_funcaoapfevidencia_tipoarq_Loadingdata ;
      private String Ddo_funcaoapfevidencia_tipoarq_Cleanfilter ;
      private String Ddo_funcaoapfevidencia_tipoarq_Noresultsfound ;
      private String Ddo_funcaoapfevidencia_tipoarq_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTffuncaoapfevidencia_descricao_Internalname ;
      private String edtavTffuncaoapfevidencia_descricao_sel_Internalname ;
      private String edtavTffuncaoapfevidencia_nomearq_Internalname ;
      private String edtavTffuncaoapfevidencia_nomearq_Jsonclick ;
      private String edtavTffuncaoapfevidencia_nomearq_sel_Internalname ;
      private String edtavTffuncaoapfevidencia_nomearq_sel_Jsonclick ;
      private String edtavTffuncaoapfevidencia_tipoarq_Internalname ;
      private String edtavTffuncaoapfevidencia_tipoarq_Jsonclick ;
      private String edtavTffuncaoapfevidencia_tipoarq_sel_Internalname ;
      private String edtavTffuncaoapfevidencia_tipoarq_sel_Jsonclick ;
      private String edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtFuncaoAPFEvidencia_Codigo_Internalname ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPF_SistemaCod_Internalname ;
      private String edtFuncaoAPFEvidencia_Descricao_Internalname ;
      private String A409FuncaoAPFEvidencia_NomeArq ;
      private String edtFuncaoAPFEvidencia_NomeArq_Internalname ;
      private String A410FuncaoAPFEvidencia_TipoArq ;
      private String edtFuncaoAPFEvidencia_TipoArq_Internalname ;
      private String scmdbuf ;
      private String lV29TFFuncaoAPFEvidencia_NomeArq ;
      private String lV33TFFuncaoAPFEvidencia_TipoArq ;
      private String subGrid_Internalname ;
      private String Ddo_funcaoapfevidencia_descricao_Internalname ;
      private String Ddo_funcaoapfevidencia_nomearq_Internalname ;
      private String Ddo_funcaoapfevidencia_tipoarq_Internalname ;
      private String edtFuncaoAPFEvidencia_Descricao_Title ;
      private String edtFuncaoAPFEvidencia_NomeArq_Title ;
      private String edtFuncaoAPFEvidencia_TipoArq_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String sCtrlAV7FuncaoAPF_Codigo ;
      private String sCtrlAV8FuncaoAPF_SistemaCod ;
      private String sGXsfl_22_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFuncaoAPFEvidencia_Codigo_Jsonclick ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String edtFuncaoAPF_SistemaCod_Jsonclick ;
      private String edtFuncaoAPFEvidencia_Descricao_Jsonclick ;
      private String edtFuncaoAPFEvidencia_NomeArq_Jsonclick ;
      private String edtFuncaoAPFEvidencia_TipoArq_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_funcaoapfevidencia_descricao_Includesortasc ;
      private bool Ddo_funcaoapfevidencia_descricao_Includesortdsc ;
      private bool Ddo_funcaoapfevidencia_descricao_Includefilter ;
      private bool Ddo_funcaoapfevidencia_descricao_Filterisrange ;
      private bool Ddo_funcaoapfevidencia_descricao_Includedatalist ;
      private bool Ddo_funcaoapfevidencia_nomearq_Includesortasc ;
      private bool Ddo_funcaoapfevidencia_nomearq_Includesortdsc ;
      private bool Ddo_funcaoapfevidencia_nomearq_Includefilter ;
      private bool Ddo_funcaoapfevidencia_nomearq_Filterisrange ;
      private bool Ddo_funcaoapfevidencia_nomearq_Includedatalist ;
      private bool Ddo_funcaoapfevidencia_tipoarq_Includesortasc ;
      private bool Ddo_funcaoapfevidencia_tipoarq_Includesortdsc ;
      private bool Ddo_funcaoapfevidencia_tipoarq_Includefilter ;
      private bool Ddo_funcaoapfevidencia_tipoarq_Filterisrange ;
      private bool Ddo_funcaoapfevidencia_tipoarq_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n407FuncaoAPFEvidencia_Descricao ;
      private bool n409FuncaoAPFEvidencia_NomeArq ;
      private bool n410FuncaoAPFEvidencia_TipoArq ;
      private bool A750FuncaoAPFEvidencia_Ativo ;
      private bool n750FuncaoAPFEvidencia_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV16Update_IsBlob ;
      private bool AV17Delete_IsBlob ;
      private bool AV18Display_IsBlob ;
      private String A407FuncaoAPFEvidencia_Descricao ;
      private String AV25TFFuncaoAPFEvidencia_Descricao ;
      private String AV26TFFuncaoAPFEvidencia_Descricao_Sel ;
      private String AV27ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace ;
      private String AV31ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace ;
      private String AV35ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace ;
      private String AV56Update_GXI ;
      private String AV57Delete_GXI ;
      private String AV58Display_GXI ;
      private String lV25TFFuncaoAPFEvidencia_Descricao ;
      private String AV16Update ;
      private String AV17Delete ;
      private String AV18Display ;
      private IGxSession AV19Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] H00AC2_A750FuncaoAPFEvidencia_Ativo ;
      private bool[] H00AC2_n750FuncaoAPFEvidencia_Ativo ;
      private String[] H00AC2_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] H00AC2_n410FuncaoAPFEvidencia_TipoArq ;
      private String[] H00AC2_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] H00AC2_n409FuncaoAPFEvidencia_NomeArq ;
      private String[] H00AC2_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] H00AC2_n407FuncaoAPFEvidencia_Descricao ;
      private int[] H00AC2_A360FuncaoAPF_SistemaCod ;
      private bool[] H00AC2_n360FuncaoAPF_SistemaCod ;
      private int[] H00AC2_A165FuncaoAPF_Codigo ;
      private int[] H00AC2_A406FuncaoAPFEvidencia_Codigo ;
      private long[] H00AC3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV9HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV24FuncaoAPFEvidencia_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV28FuncaoAPFEvidencia_NomeArqTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV32FuncaoAPFEvidencia_TipoArqTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV11TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV12GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV13GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV36DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class funcaoapffuncaoapfevidenciawc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00AC2( IGxContext context ,
                                             String AV26TFFuncaoAPFEvidencia_Descricao_Sel ,
                                             String AV25TFFuncaoAPFEvidencia_Descricao ,
                                             String AV30TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                             String AV29TFFuncaoAPFEvidencia_NomeArq ,
                                             String AV34TFFuncaoAPFEvidencia_TipoArq_Sel ,
                                             String AV33TFFuncaoAPFEvidencia_TipoArq ,
                                             String A407FuncaoAPFEvidencia_Descricao ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             String A410FuncaoAPFEvidencia_TipoArq ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A165FuncaoAPF_Codigo ,
                                             int AV7FuncaoAPF_Codigo ,
                                             bool A750FuncaoAPFEvidencia_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[FuncaoAPFEvidencia_Ativo], T1.[FuncaoAPFEvidencia_TipoArq], T1.[FuncaoAPFEvidencia_NomeArq], T1.[FuncaoAPFEvidencia_Descricao], T2.[FuncaoAPF_SistemaCod], T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFEvidencia_Codigo]";
         sFromString = " FROM ([FuncaoAPFEvidencia] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[FuncaoAPF_Codigo] = @AV7FuncaoAPF_Codigo)";
         sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Ativo] = 1)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV26TFFuncaoAPFEvidencia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFFuncaoAPFEvidencia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like @lV25TFFuncaoAPFEvidencia_Descricao)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFFuncaoAPFEvidencia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] = @AV26TFFuncaoAPFEvidencia_Descricao_Sel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV30TFFuncaoAPFEvidencia_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFFuncaoAPFEvidencia_NomeArq)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] like @lV29TFFuncaoAPFEvidencia_NomeArq)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFFuncaoAPFEvidencia_NomeArq_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] = @AV30TFFuncaoAPFEvidencia_NomeArq_Sel)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV34TFFuncaoAPFEvidencia_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFFuncaoAPFEvidencia_TipoArq)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_TipoArq] like @lV33TFFuncaoAPFEvidencia_TipoArq)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFFuncaoAPFEvidencia_TipoArq_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_TipoArq] = @AV34TFFuncaoAPFEvidencia_TipoArq_Sel)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFEvidencia_Descricao]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo] DESC, T1.[FuncaoAPFEvidencia_Descricao] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFEvidencia_NomeArq]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo] DESC, T1.[FuncaoAPFEvidencia_NomeArq] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFEvidencia_TipoArq]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo] DESC, T1.[FuncaoAPFEvidencia_TipoArq] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFEvidencia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00AC3( IGxContext context ,
                                             String AV26TFFuncaoAPFEvidencia_Descricao_Sel ,
                                             String AV25TFFuncaoAPFEvidencia_Descricao ,
                                             String AV30TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                             String AV29TFFuncaoAPFEvidencia_NomeArq ,
                                             String AV34TFFuncaoAPFEvidencia_TipoArq_Sel ,
                                             String AV33TFFuncaoAPFEvidencia_TipoArq ,
                                             String A407FuncaoAPFEvidencia_Descricao ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             String A410FuncaoAPFEvidencia_TipoArq ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A165FuncaoAPF_Codigo ,
                                             int AV7FuncaoAPF_Codigo ,
                                             bool A750FuncaoAPFEvidencia_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([FuncaoAPFEvidencia] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoAPF_Codigo] = @AV7FuncaoAPF_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[FuncaoAPFEvidencia_Ativo] = 1)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV26TFFuncaoAPFEvidencia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFFuncaoAPFEvidencia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like @lV25TFFuncaoAPFEvidencia_Descricao)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFFuncaoAPFEvidencia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] = @AV26TFFuncaoAPFEvidencia_Descricao_Sel)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV30TFFuncaoAPFEvidencia_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TFFuncaoAPFEvidencia_NomeArq)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] like @lV29TFFuncaoAPFEvidencia_NomeArq)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFFuncaoAPFEvidencia_NomeArq_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] = @AV30TFFuncaoAPFEvidencia_NomeArq_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV34TFFuncaoAPFEvidencia_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFFuncaoAPFEvidencia_TipoArq)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_TipoArq] like @lV33TFFuncaoAPFEvidencia_TipoArq)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFFuncaoAPFEvidencia_TipoArq_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_TipoArq] = @AV34TFFuncaoAPFEvidencia_TipoArq_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00AC2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (bool)dynConstraints[13] );
               case 1 :
                     return conditional_H00AC3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (bool)dynConstraints[13] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AC2 ;
          prmH00AC2 = new Object[] {
          new Object[] {"@AV7FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV25TFFuncaoAPFEvidencia_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV26TFFuncaoAPFEvidencia_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV29TFFuncaoAPFEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV30TFFuncaoAPFEvidencia_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV33TFFuncaoAPFEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV34TFFuncaoAPFEvidencia_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00AC3 ;
          prmH00AC3 = new Object[] {
          new Object[] {"@AV7FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV25TFFuncaoAPFEvidencia_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV26TFFuncaoAPFEvidencia_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV29TFFuncaoAPFEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV30TFFuncaoAPFEvidencia_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV33TFFuncaoAPFEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV34TFFuncaoAPFEvidencia_TipoArq_Sel",SqlDbType.Char,10,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AC2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AC2,11,0,true,false )
             ,new CursorDef("H00AC3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AC3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

}
