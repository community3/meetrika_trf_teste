/*
               File: PR_SistemaExcFunDados
        Description: Excluir Registro em Massa de Fun��es - Dados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:20.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class pr_sistemaexcfundados : GXProcedure
   {
      public pr_sistemaexcfundados( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public pr_sistemaexcfundados( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_FuncaoDados_Codigo ,
                           ref int aP1_FuncaoDados_SistemaCod ,
                           ref String aP2_MensagemSistema )
      {
         this.AV8FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.AV11FuncaoDados_SistemaCod = aP1_FuncaoDados_SistemaCod;
         this.AV9MensagemSistema = aP2_MensagemSistema;
         initialize();
         executePrivate();
         aP0_FuncaoDados_Codigo=this.AV8FuncaoDados_Codigo;
         aP1_FuncaoDados_SistemaCod=this.AV11FuncaoDados_SistemaCod;
         aP2_MensagemSistema=this.AV9MensagemSistema;
      }

      public String executeUdp( ref int aP0_FuncaoDados_Codigo ,
                                ref int aP1_FuncaoDados_SistemaCod )
      {
         this.AV8FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.AV11FuncaoDados_SistemaCod = aP1_FuncaoDados_SistemaCod;
         this.AV9MensagemSistema = aP2_MensagemSistema;
         initialize();
         executePrivate();
         aP0_FuncaoDados_Codigo=this.AV8FuncaoDados_Codigo;
         aP1_FuncaoDados_SistemaCod=this.AV11FuncaoDados_SistemaCod;
         aP2_MensagemSistema=this.AV9MensagemSistema;
         return AV9MensagemSistema ;
      }

      public void executeSubmit( ref int aP0_FuncaoDados_Codigo ,
                                 ref int aP1_FuncaoDados_SistemaCod ,
                                 ref String aP2_MensagemSistema )
      {
         pr_sistemaexcfundados objpr_sistemaexcfundados;
         objpr_sistemaexcfundados = new pr_sistemaexcfundados();
         objpr_sistemaexcfundados.AV8FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         objpr_sistemaexcfundados.AV11FuncaoDados_SistemaCod = aP1_FuncaoDados_SistemaCod;
         objpr_sistemaexcfundados.AV9MensagemSistema = aP2_MensagemSistema;
         objpr_sistemaexcfundados.context.SetSubmitInitialConfig(context);
         objpr_sistemaexcfundados.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objpr_sistemaexcfundados);
         aP0_FuncaoDados_Codigo=this.AV8FuncaoDados_Codigo;
         aP1_FuncaoDados_SistemaCod=this.AV11FuncaoDados_SistemaCod;
         aP2_MensagemSistema=this.AV9MensagemSistema;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((pr_sistemaexcfundados)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10TotRegistros = 0;
         /* Using cursor P00YJ2 */
         pr_default.execute(0, new Object[] {AV8FuncaoDados_Codigo, AV11FuncaoDados_SistemaCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A370FuncaoDados_SistemaCod = P00YJ2_A370FuncaoDados_SistemaCod[0];
            A368FuncaoDados_Codigo = P00YJ2_A368FuncaoDados_Codigo[0];
            /* Using cursor P00YJ3 */
            pr_default.execute(1, new Object[] {AV8FuncaoDados_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A368FuncaoDados_Codigo = P00YJ3_A368FuncaoDados_Codigo[0];
               A172Tabela_Codigo = P00YJ3_A172Tabela_Codigo[0];
               AV10TotRegistros = (int)(AV10TotRegistros+1);
               /* Using cursor P00YJ4 */
               pr_default.execute(2, new Object[] {A368FuncaoDados_Codigo, A172Tabela_Codigo});
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosTabela") ;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Using cursor P00YJ5 */
            pr_default.execute(3, new Object[] {AV8FuncaoDados_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A165FuncaoAPF_Codigo = P00YJ5_A165FuncaoAPF_Codigo[0];
               A364FuncaoAPFAtributos_AtributosCod = P00YJ5_A364FuncaoAPFAtributos_AtributosCod[0];
               /* Using cursor P00YJ6 */
               pr_default.execute(4, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
               AV10TotRegistros = (int)(AV10TotRegistros+1);
               pr_default.readNext(3);
            }
            pr_default.close(3);
            /* Using cursor P00YJ7 */
            pr_default.execute(5, new Object[] {AV8FuncaoDados_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A1261FuncaoDadosPreImp_FnDadosCodigo = P00YJ7_A1261FuncaoDadosPreImp_FnDadosCodigo[0];
               A1259FuncaoDadosPreImp_Sequencial = P00YJ7_A1259FuncaoDadosPreImp_Sequencial[0];
               /* Using cursor P00YJ8 */
               pr_default.execute(6, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo, A1259FuncaoDadosPreImp_Sequencial});
               pr_default.close(6);
               dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosPreImp") ;
               AV10TotRegistros = (int)(AV10TotRegistros+1);
               pr_default.readNext(5);
            }
            pr_default.close(5);
            AV10TotRegistros = (int)(AV10TotRegistros+1);
            /* Using cursor P00YJ9 */
            pr_default.execute(7, new Object[] {A368FuncaoDados_Codigo});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( AV10TotRegistros > 0 )
         {
            AV9MensagemSistema = "Total Registros Exclu�dos: " + StringUtil.Str( (decimal)(AV10TotRegistros), 6, 0);
         }
         else
         {
            AV9MensagemSistema = "Nenhum Registro Exclu�do";
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PR_SistemaExcFunDados");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00YJ2_A370FuncaoDados_SistemaCod = new int[1] ;
         P00YJ2_A368FuncaoDados_Codigo = new int[1] ;
         P00YJ3_A368FuncaoDados_Codigo = new int[1] ;
         P00YJ3_A172Tabela_Codigo = new int[1] ;
         P00YJ5_A165FuncaoAPF_Codigo = new int[1] ;
         P00YJ5_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P00YJ7_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         P00YJ7_A1259FuncaoDadosPreImp_Sequencial = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.pr_sistemaexcfundados__default(),
            new Object[][] {
                new Object[] {
               P00YJ2_A370FuncaoDados_SistemaCod, P00YJ2_A368FuncaoDados_Codigo
               }
               , new Object[] {
               P00YJ3_A368FuncaoDados_Codigo, P00YJ3_A172Tabela_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P00YJ5_A165FuncaoAPF_Codigo, P00YJ5_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               }
               , new Object[] {
               P00YJ7_A1261FuncaoDadosPreImp_FnDadosCodigo, P00YJ7_A1259FuncaoDadosPreImp_Sequencial
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1259FuncaoDadosPreImp_Sequencial ;
      private int AV8FuncaoDados_Codigo ;
      private int AV11FuncaoDados_SistemaCod ;
      private int AV10TotRegistros ;
      private int A370FuncaoDados_SistemaCod ;
      private int A368FuncaoDados_Codigo ;
      private int A172Tabela_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private String scmdbuf ;
      private String AV9MensagemSistema ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_FuncaoDados_Codigo ;
      private int aP1_FuncaoDados_SistemaCod ;
      private String aP2_MensagemSistema ;
      private IDataStoreProvider pr_default ;
      private int[] P00YJ2_A370FuncaoDados_SistemaCod ;
      private int[] P00YJ2_A368FuncaoDados_Codigo ;
      private int[] P00YJ3_A368FuncaoDados_Codigo ;
      private int[] P00YJ3_A172Tabela_Codigo ;
      private int[] P00YJ5_A165FuncaoAPF_Codigo ;
      private int[] P00YJ5_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P00YJ7_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private short[] P00YJ7_A1259FuncaoDadosPreImp_Sequencial ;
   }

   public class pr_sistemaexcfundados__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00YJ2 ;
          prmP00YJ2 = new Object[] {
          new Object[] {"@AV8FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YJ3 ;
          prmP00YJ3 = new Object[] {
          new Object[] {"@AV8FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YJ4 ;
          prmP00YJ4 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YJ5 ;
          prmP00YJ5 = new Object[] {
          new Object[] {"@AV8FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YJ6 ;
          prmP00YJ6 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YJ7 ;
          prmP00YJ7 = new Object[] {
          new Object[] {"@AV8FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YJ8 ;
          prmP00YJ8 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmP00YJ9 ;
          prmP00YJ9 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00YJ2", "SELECT [FuncaoDados_SistemaCod], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (UPDLOCK) WHERE ([FuncaoDados_Codigo] = @AV8FuncaoDados_Codigo) AND ([FuncaoDados_SistemaCod] = @AV11FuncaoDados_SistemaCod) ORDER BY [FuncaoDados_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YJ2,1,0,true,true )
             ,new CursorDef("P00YJ3", "SELECT [FuncaoDados_Codigo], [Tabela_Codigo] FROM [FuncaoDadosTabela] WITH (UPDLOCK) WHERE [FuncaoDados_Codigo] = @AV8FuncaoDados_Codigo ORDER BY [FuncaoDados_Codigo], [Tabela_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YJ3,100,0,true,false )
             ,new CursorDef("P00YJ4", "DELETE FROM [FuncaoDadosTabela]  WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo AND [Tabela_Codigo] = @Tabela_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00YJ4)
             ,new CursorDef("P00YJ5", "SELECT [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] FROM [FuncoesAPFAtributos] WITH (UPDLOCK) WHERE [FuncaoAPF_Codigo] = @AV8FuncaoDados_Codigo ORDER BY [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YJ5,100,0,true,false )
             ,new CursorDef("P00YJ6", "DELETE FROM [FuncoesAPFAtributos]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo AND [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00YJ6)
             ,new CursorDef("P00YJ7", "SELECT [FuncaoDadosPreImp_FnDadosCodigo], [FuncaoDadosPreImp_Sequencial] FROM [FuncaoDadosPreImp] WITH (UPDLOCK) WHERE [FuncaoDadosPreImp_FnDadosCodigo] = @AV8FuncaoDados_Codigo ORDER BY [FuncaoDadosPreImp_FnDadosCodigo], [FuncaoDadosPreImp_Sequencial] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YJ7,100,0,true,false )
             ,new CursorDef("P00YJ8", "DELETE FROM [FuncaoDadosPreImp]  WHERE [FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDadosPreImp_FnDadosCodigo AND [FuncaoDadosPreImp_Sequencial] = @FuncaoDadosPreImp_Sequencial", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00YJ8)
             ,new CursorDef("P00YJ9", "DELETE FROM [FuncaoDados]  WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00YJ9)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
