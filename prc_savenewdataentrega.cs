/*
               File: PRC_SaveNewDataEntrega
        Description: Save New Data Entrega
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:30.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_savenewdataentrega : GXProcedure
   {
      public prc_savenewdataentrega( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_savenewdataentrega( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           DateTime aP1_NovaDataEntrega ,
                           bool aP2_UpdDataPrevista )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8NovaDataEntrega = aP1_NovaDataEntrega;
         this.AV9UpdDataPrevista = aP2_UpdDataPrevista;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 DateTime aP1_NovaDataEntrega ,
                                 bool aP2_UpdDataPrevista )
      {
         prc_savenewdataentrega objprc_savenewdataentrega;
         objprc_savenewdataentrega = new prc_savenewdataentrega();
         objprc_savenewdataentrega.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_savenewdataentrega.AV8NovaDataEntrega = aP1_NovaDataEntrega;
         objprc_savenewdataentrega.AV9UpdDataPrevista = aP2_UpdDataPrevista;
         objprc_savenewdataentrega.context.SetSubmitInitialConfig(context);
         objprc_savenewdataentrega.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_savenewdataentrega);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_savenewdataentrega)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00872 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A472ContagemResultado_DataEntrega = P00872_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00872_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P00872_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00872_n912ContagemResultado_HoraEntrega[0];
            A1351ContagemResultado_DataPrevista = P00872_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00872_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV8NovaDataEntrega);
            n472ContagemResultado_DataEntrega = false;
            if ( DateTimeUtil.Hour( AV8NovaDataEntrega) > 0 )
            {
               A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV8NovaDataEntrega);
               n912ContagemResultado_HoraEntrega = false;
            }
            if ( AV9UpdDataPrevista )
            {
               A1351ContagemResultado_DataPrevista = AV8NovaDataEntrega;
               n1351ContagemResultado_DataPrevista = false;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00873 */
            pr_default.execute(1, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00874 */
            pr_default.execute(2, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00872_A456ContagemResultado_Codigo = new int[1] ;
         P00872_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00872_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00872_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00872_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00872_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00872_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_savenewdataentrega__default(),
            new Object[][] {
                new Object[] {
               P00872_A456ContagemResultado_Codigo, P00872_A472ContagemResultado_DataEntrega, P00872_n472ContagemResultado_DataEntrega, P00872_A912ContagemResultado_HoraEntrega, P00872_n912ContagemResultado_HoraEntrega, P00872_A1351ContagemResultado_DataPrevista, P00872_n1351ContagemResultado_DataPrevista
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private DateTime AV8NovaDataEntrega ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool AV9UpdDataPrevista ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n1351ContagemResultado_DataPrevista ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00872_A456ContagemResultado_Codigo ;
      private DateTime[] P00872_A472ContagemResultado_DataEntrega ;
      private bool[] P00872_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00872_A912ContagemResultado_HoraEntrega ;
      private bool[] P00872_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P00872_A1351ContagemResultado_DataPrevista ;
      private bool[] P00872_n1351ContagemResultado_DataPrevista ;
   }

   public class prc_savenewdataentrega__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00872 ;
          prmP00872 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00873 ;
          prmP00873 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00874 ;
          prmP00874 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00872", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega], [ContagemResultado_DataPrevista] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00872,1,0,true,true )
             ,new CursorDef("P00873", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00873)
             ,new CursorDef("P00874", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00874)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
       }
    }

 }

}
