/*
               File: PRC_FaltamAnexarArtefatos
        Description: Faltam Anexar Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:54.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_faltamanexarartefatos : GXProcedure
   {
      public prc_faltamanexarartefatos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_faltamanexarartefatos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultadoArtefato_OSCod ,
                           out bool aP1_FaltaAnexar )
      {
         this.A1772ContagemResultadoArtefato_OSCod = aP0_ContagemResultadoArtefato_OSCod;
         this.AV11FaltaAnexar = false ;
         initialize();
         executePrivate();
         aP0_ContagemResultadoArtefato_OSCod=this.A1772ContagemResultadoArtefato_OSCod;
         aP1_FaltaAnexar=this.AV11FaltaAnexar;
      }

      public bool executeUdp( ref int aP0_ContagemResultadoArtefato_OSCod )
      {
         this.A1772ContagemResultadoArtefato_OSCod = aP0_ContagemResultadoArtefato_OSCod;
         this.AV11FaltaAnexar = false ;
         initialize();
         executePrivate();
         aP0_ContagemResultadoArtefato_OSCod=this.A1772ContagemResultadoArtefato_OSCod;
         aP1_FaltaAnexar=this.AV11FaltaAnexar;
         return AV11FaltaAnexar ;
      }

      public void executeSubmit( ref int aP0_ContagemResultadoArtefato_OSCod ,
                                 out bool aP1_FaltaAnexar )
      {
         prc_faltamanexarartefatos objprc_faltamanexarartefatos;
         objprc_faltamanexarartefatos = new prc_faltamanexarartefatos();
         objprc_faltamanexarartefatos.A1772ContagemResultadoArtefato_OSCod = aP0_ContagemResultadoArtefato_OSCod;
         objprc_faltamanexarartefatos.AV11FaltaAnexar = false ;
         objprc_faltamanexarartefatos.context.SetSubmitInitialConfig(context);
         objprc_faltamanexarartefatos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_faltamanexarartefatos);
         aP0_ContagemResultadoArtefato_OSCod=this.A1772ContagemResultadoArtefato_OSCod;
         aP1_FaltaAnexar=this.AV11FaltaAnexar;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_faltamanexarartefatos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9Artefatos.FromXml(AV12WebSession.Get("ArquivosEvd"), "SDT_ContagemResultadoEvidencias.ArquivoCollection");
         /* Using cursor P00CW2 */
         pr_default.execute(0, new Object[] {A1772ContagemResultadoArtefato_OSCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1773ContagemResultadoArtefato_AnxCod = P00CW2_A1773ContagemResultadoArtefato_AnxCod[0];
            n1773ContagemResultadoArtefato_AnxCod = P00CW2_n1773ContagemResultadoArtefato_AnxCod[0];
            A1770ContagemResultadoArtefato_EvdCod = P00CW2_A1770ContagemResultadoArtefato_EvdCod[0];
            n1770ContagemResultadoArtefato_EvdCod = P00CW2_n1770ContagemResultadoArtefato_EvdCod[0];
            A1771ContagemResultadoArtefato_ArtefatoCod = P00CW2_A1771ContagemResultadoArtefato_ArtefatoCod[0];
            A1769ContagemResultadoArtefato_Codigo = P00CW2_A1769ContagemResultadoArtefato_Codigo[0];
            AV11FaltaAnexar = true;
            AV10Artefatos_Codigo = A1771ContagemResultadoArtefato_ArtefatoCod;
            /* Execute user subroutine: 'MEMORYSEARCH' */
            S111 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               this.cleanup();
               if (true) return;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'MEMORYSEARCH' Routine */
         AV16GXV1 = 1;
         while ( AV16GXV1 <= AV9Artefatos.Count )
         {
            AV8Artefato = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV9Artefatos.Item(AV16GXV1));
            if ( AV8Artefato.gxTpr_Artefatos_codigo == AV10Artefatos_Codigo )
            {
               AV11FaltaAnexar = false;
               if (true) break;
            }
            AV16GXV1 = (int)(AV16GXV1+1);
         }
         if ( AV11FaltaAnexar )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9Artefatos = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         AV12WebSession = context.GetSession();
         scmdbuf = "";
         P00CW2_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         P00CW2_A1773ContagemResultadoArtefato_AnxCod = new int[1] ;
         P00CW2_n1773ContagemResultadoArtefato_AnxCod = new bool[] {false} ;
         P00CW2_A1770ContagemResultadoArtefato_EvdCod = new int[1] ;
         P00CW2_n1770ContagemResultadoArtefato_EvdCod = new bool[] {false} ;
         P00CW2_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         P00CW2_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         AV8Artefato = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_faltamanexarartefatos__default(),
            new Object[][] {
                new Object[] {
               P00CW2_A1772ContagemResultadoArtefato_OSCod, P00CW2_A1773ContagemResultadoArtefato_AnxCod, P00CW2_n1773ContagemResultadoArtefato_AnxCod, P00CW2_A1770ContagemResultadoArtefato_EvdCod, P00CW2_n1770ContagemResultadoArtefato_EvdCod, P00CW2_A1771ContagemResultadoArtefato_ArtefatoCod, P00CW2_A1769ContagemResultadoArtefato_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1772ContagemResultadoArtefato_OSCod ;
      private int A1773ContagemResultadoArtefato_AnxCod ;
      private int A1770ContagemResultadoArtefato_EvdCod ;
      private int A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int A1769ContagemResultadoArtefato_Codigo ;
      private int AV10Artefatos_Codigo ;
      private int AV16GXV1 ;
      private String scmdbuf ;
      private bool AV11FaltaAnexar ;
      private bool n1773ContagemResultadoArtefato_AnxCod ;
      private bool n1770ContagemResultadoArtefato_EvdCod ;
      private bool returnInSub ;
      private IGxSession AV12WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultadoArtefato_OSCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00CW2_A1772ContagemResultadoArtefato_OSCod ;
      private int[] P00CW2_A1773ContagemResultadoArtefato_AnxCod ;
      private bool[] P00CW2_n1773ContagemResultadoArtefato_AnxCod ;
      private int[] P00CW2_A1770ContagemResultadoArtefato_EvdCod ;
      private bool[] P00CW2_n1770ContagemResultadoArtefato_EvdCod ;
      private int[] P00CW2_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int[] P00CW2_A1769ContagemResultadoArtefato_Codigo ;
      private bool aP1_FaltaAnexar ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV9Artefatos ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV8Artefato ;
   }

   public class prc_faltamanexarartefatos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CW2 ;
          prmP00CW2 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_OSCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CW2", "SELECT [ContagemResultadoArtefato_OSCod], [ContagemResultadoArtefato_AnxCod], [ContagemResultadoArtefato_EvdCod], [ContagemResultadoArtefato_ArtefatoCod], [ContagemResultadoArtefato_Codigo] FROM [ContagemResultadoArtefato] WITH (NOLOCK) WHERE ([ContagemResultadoArtefato_OSCod] = @ContagemResultadoArtefato_OSCod) AND ([ContagemResultadoArtefato_EvdCod] IS NULL) AND ([ContagemResultadoArtefato_AnxCod] IS NULL) ORDER BY [ContagemResultadoArtefato_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CW2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
