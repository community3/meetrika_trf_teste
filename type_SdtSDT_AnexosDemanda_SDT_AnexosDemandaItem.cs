/*
               File: type_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem
        Description: SDT_AnexosDemanda
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:58.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_AnexosDemanda.SDT_AnexosDemandaItem" )]
   [XmlType(TypeName =  "SDT_AnexosDemanda.SDT_AnexosDemandaItem" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem : GxUserType
   {
      public SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodemanda = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Nomearquivo = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipoarquivo = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Arquivo = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodocumento = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Descricao = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Link = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Entidade = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_descricao = "";
      }

      public SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem obj ;
         obj = this;
         obj.gxTpr_Tipodemanda = deserialized.gxTpr_Tipodemanda;
         obj.gxTpr_Codigo = deserialized.gxTpr_Codigo;
         obj.gxTpr_Nomearquivo = deserialized.gxTpr_Nomearquivo;
         obj.gxTpr_Tipoarquivo = deserialized.gxTpr_Tipoarquivo;
         obj.gxTpr_Arquivo = deserialized.gxTpr_Arquivo;
         obj.gxTpr_Tipodocumento = deserialized.gxTpr_Tipodocumento;
         obj.gxTpr_Descricao = deserialized.gxTpr_Descricao;
         obj.gxTpr_Dataupload = deserialized.gxTpr_Dataupload;
         obj.gxTpr_Link = deserialized.gxTpr_Link;
         obj.gxTpr_Owner = deserialized.gxTpr_Owner;
         obj.gxTpr_Entidade = deserialized.gxTpr_Entidade;
         obj.gxTpr_Artefatos_codigo = deserialized.gxTpr_Artefatos_codigo;
         obj.gxTpr_Artefatos_descricao = deserialized.gxTpr_Artefatos_descricao;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDemanda") )
               {
                  gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodemanda = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Codigo") )
               {
                  gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "NomeArquivo") )
               {
                  gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Nomearquivo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoArquivo") )
               {
                  gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipoarquivo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Arquivo") )
               {
                  gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Arquivo=context.FileFromBase64( oReader.Value) ;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento") )
               {
                  gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodocumento = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Descricao") )
               {
                  gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DataUpload") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Link") )
               {
                  gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Owner") )
               {
                  gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Owner = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Entidade") )
               {
                  gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Entidade = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Codigo") )
               {
                  gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Artefatos_Descricao") )
               {
                  gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_AnexosDemanda.SDT_AnexosDemandaItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("TipoDemanda", StringUtil.RTrim( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodemanda));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("NomeArquivo", StringUtil.RTrim( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Nomearquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("TipoArquivo", StringUtil.RTrim( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipoarquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Arquivo", context.FileToBase64( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Arquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("TipoDocumento", StringUtil.RTrim( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodocumento));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Descricao", StringUtil.RTrim( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload) )
         {
            oWriter.WriteStartElement("DataUpload");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("DataUpload", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("Link", StringUtil.RTrim( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Owner", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Owner), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Entidade", StringUtil.RTrim( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Entidade));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Artefatos_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Artefatos_Descricao", StringUtil.RTrim( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("TipoDemanda", gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodemanda, false);
         AddObjectProperty("Codigo", gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Codigo, false);
         AddObjectProperty("NomeArquivo", gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Nomearquivo, false);
         AddObjectProperty("TipoArquivo", gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipoarquivo, false);
         AddObjectProperty("Arquivo", gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Arquivo, false);
         AddObjectProperty("TipoDocumento", gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodocumento, false);
         AddObjectProperty("Descricao", gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Descricao, false);
         datetime_STZ = gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("DataUpload", sDateCnv, false);
         AddObjectProperty("Link", gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Link, false);
         AddObjectProperty("Owner", gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Owner, false);
         AddObjectProperty("Entidade", gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Entidade, false);
         AddObjectProperty("Artefatos_Codigo", gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_codigo, false);
         AddObjectProperty("Artefatos_Descricao", gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_descricao, false);
         return  ;
      }

      [  SoapElement( ElementName = "TipoDemanda" )]
      [  XmlElement( ElementName = "TipoDemanda"   )]
      public String gxTpr_Tipodemanda
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodemanda ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodemanda = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Codigo" )]
      [  XmlElement( ElementName = "Codigo"   )]
      public int gxTpr_Codigo
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Codigo ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "NomeArquivo" )]
      [  XmlElement( ElementName = "NomeArquivo"   )]
      public String gxTpr_Nomearquivo
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Nomearquivo ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Nomearquivo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "TipoArquivo" )]
      [  XmlElement( ElementName = "TipoArquivo"   )]
      public String gxTpr_Tipoarquivo
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipoarquivo ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipoarquivo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Arquivo" )]
      [  XmlElement( ElementName = "Arquivo"   )]
      [GxUpload()]
      public byte[] gxTpr_Arquivo_Blob
      {
         get {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            return context.FileToByteArray( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Arquivo) ;
         }

         set {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Arquivo=context.FileFromByteArray( value) ;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      [GxUpload()]
      public String gxTpr_Arquivo
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Arquivo ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Arquivo = value;
         }

      }

      [  SoapElement( ElementName = "TipoDocumento" )]
      [  XmlElement( ElementName = "TipoDocumento"   )]
      public String gxTpr_Tipodocumento
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodocumento ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodocumento = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Descricao" )]
      [  XmlElement( ElementName = "Descricao"   )]
      public String gxTpr_Descricao
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Descricao ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "DataUpload" )]
      [  XmlElement( ElementName = "DataUpload"  , IsNullable=true )]
      public string gxTpr_Dataupload_Nullable
      {
         get {
            if ( gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload = DateTime.MinValue;
            else
               gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Dataupload
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Link" )]
      [  XmlElement( ElementName = "Link"   )]
      public String gxTpr_Link
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Link ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Link = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Owner" )]
      [  XmlElement( ElementName = "Owner"   )]
      public int gxTpr_Owner
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Owner ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Owner = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Entidade" )]
      [  XmlElement( ElementName = "Entidade"   )]
      public String gxTpr_Entidade
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Entidade ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Entidade = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Artefatos_Codigo" )]
      [  XmlElement( ElementName = "Artefatos_Codigo"   )]
      public int gxTpr_Artefatos_codigo
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_codigo ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Artefatos_Descricao" )]
      [  XmlElement( ElementName = "Artefatos_Descricao"   )]
      public String gxTpr_Artefatos_descricao
      {
         get {
            return gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_descricao ;
         }

         set {
            gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_descricao = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodemanda = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Nomearquivo = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipoarquivo = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Arquivo = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodocumento = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Descricao = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Link = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Entidade = "";
         gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_descricao = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Codigo ;
      protected int gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Owner ;
      protected int gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_codigo ;
      protected String gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodemanda ;
      protected String gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Nomearquivo ;
      protected String gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipoarquivo ;
      protected String gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Tipodocumento ;
      protected String gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Entidade ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Dataupload ;
      protected DateTime datetime_STZ ;
      protected String gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Descricao ;
      protected String gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Link ;
      protected String gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Artefatos_descricao ;
      protected String gxTv_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_Arquivo ;
   }

   [DataContract(Name = @"SDT_AnexosDemanda.SDT_AnexosDemandaItem", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_RESTInterface : GxGenericCollectionItem<SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_RESTInterface( ) : base()
      {
      }

      public SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem_RESTInterface( SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "TipoDemanda" , Order = 0 )]
      public String gxTpr_Tipodemanda
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tipodemanda) ;
         }

         set {
            sdt.gxTpr_Tipodemanda = (String)(value);
         }

      }

      [DataMember( Name = "Codigo" , Order = 1 )]
      public Nullable<int> gxTpr_Codigo
      {
         get {
            return sdt.gxTpr_Codigo ;
         }

         set {
            sdt.gxTpr_Codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "NomeArquivo" , Order = 2 )]
      public String gxTpr_Nomearquivo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Nomearquivo) ;
         }

         set {
            sdt.gxTpr_Nomearquivo = (String)(value);
         }

      }

      [DataMember( Name = "TipoArquivo" , Order = 3 )]
      public String gxTpr_Tipoarquivo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tipoarquivo) ;
         }

         set {
            sdt.gxTpr_Tipoarquivo = (String)(value);
         }

      }

      [DataMember( Name = "Arquivo" , Order = 4 )]
      [GxUpload()]
      public String gxTpr_Arquivo
      {
         get {
            return PathUtil.RelativePath( sdt.gxTpr_Arquivo) ;
         }

         set {
            sdt.gxTpr_Arquivo = value;
         }

      }

      [DataMember( Name = "TipoDocumento" , Order = 5 )]
      public String gxTpr_Tipodocumento
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tipodocumento) ;
         }

         set {
            sdt.gxTpr_Tipodocumento = (String)(value);
         }

      }

      [DataMember( Name = "Descricao" , Order = 6 )]
      public String gxTpr_Descricao
      {
         get {
            return sdt.gxTpr_Descricao ;
         }

         set {
            sdt.gxTpr_Descricao = (String)(value);
         }

      }

      [DataMember( Name = "DataUpload" , Order = 7 )]
      public String gxTpr_Dataupload
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Dataupload) ;
         }

         set {
            sdt.gxTpr_Dataupload = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "Link" , Order = 8 )]
      public String gxTpr_Link
      {
         get {
            return sdt.gxTpr_Link ;
         }

         set {
            sdt.gxTpr_Link = (String)(value);
         }

      }

      [DataMember( Name = "Owner" , Order = 9 )]
      public Nullable<int> gxTpr_Owner
      {
         get {
            return sdt.gxTpr_Owner ;
         }

         set {
            sdt.gxTpr_Owner = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Entidade" , Order = 10 )]
      public String gxTpr_Entidade
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Entidade) ;
         }

         set {
            sdt.gxTpr_Entidade = (String)(value);
         }

      }

      [DataMember( Name = "Artefatos_Codigo" , Order = 11 )]
      public Nullable<int> gxTpr_Artefatos_codigo
      {
         get {
            return sdt.gxTpr_Artefatos_codigo ;
         }

         set {
            sdt.gxTpr_Artefatos_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Artefatos_Descricao" , Order = 12 )]
      public String gxTpr_Artefatos_descricao
      {
         get {
            return sdt.gxTpr_Artefatos_descricao ;
         }

         set {
            sdt.gxTpr_Artefatos_descricao = (String)(value);
         }

      }

      public SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem sdt
      {
         get {
            return (SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem() ;
         }
      }

   }

}
