/*
               File: PRC_DemandasVinculadas
        Description: Stub for PRC_DemandasVinculadas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/4/2020 0:16:46.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_demandasvinculadas : GXProcedure
   {
      public prc_demandasvinculadas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_demandasvinculadas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           int aP1_Contrato_Codigo ,
                           int aP2_ContratoServicos_Codigo ,
                           DateTime aP3_PrazoDemanda ,
                           String aP4_ContagemResultado_AgrupadorAux ,
                           short aP5_IsModificarAgrupador )
      {
         this.AV2Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV3Contrato_Codigo = aP1_Contrato_Codigo;
         this.AV4ContratoServicos_Codigo = aP2_ContratoServicos_Codigo;
         this.AV5PrazoDemanda = aP3_PrazoDemanda;
         this.AV6ContagemResultado_AgrupadorAux = aP4_ContagemResultado_AgrupadorAux;
         this.AV7IsModificarAgrupador = aP5_IsModificarAgrupador;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_Codigo ,
                                 int aP1_Contrato_Codigo ,
                                 int aP2_ContratoServicos_Codigo ,
                                 DateTime aP3_PrazoDemanda ,
                                 String aP4_ContagemResultado_AgrupadorAux ,
                                 short aP5_IsModificarAgrupador )
      {
         prc_demandasvinculadas objprc_demandasvinculadas;
         objprc_demandasvinculadas = new prc_demandasvinculadas();
         objprc_demandasvinculadas.AV2Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_demandasvinculadas.AV3Contrato_Codigo = aP1_Contrato_Codigo;
         objprc_demandasvinculadas.AV4ContratoServicos_Codigo = aP2_ContratoServicos_Codigo;
         objprc_demandasvinculadas.AV5PrazoDemanda = aP3_PrazoDemanda;
         objprc_demandasvinculadas.AV6ContagemResultado_AgrupadorAux = aP4_ContagemResultado_AgrupadorAux;
         objprc_demandasvinculadas.AV7IsModificarAgrupador = aP5_IsModificarAgrupador;
         objprc_demandasvinculadas.context.SetSubmitInitialConfig(context);
         objprc_demandasvinculadas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_demandasvinculadas);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_demandasvinculadas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2Contratada_Codigo,(int)AV3Contrato_Codigo,(int)AV4ContratoServicos_Codigo,(DateTime)AV5PrazoDemanda,(String)AV6ContagemResultado_AgrupadorAux,(short)AV7IsModificarAgrupador} ;
         ClassLoader.Execute("aprc_demandasvinculadas","GeneXus.Programs.aprc_demandasvinculadas", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 6 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV7IsModificarAgrupador ;
      private int AV2Contratada_Codigo ;
      private int AV3Contrato_Codigo ;
      private int AV4ContratoServicos_Codigo ;
      private String AV6ContagemResultado_AgrupadorAux ;
      private DateTime AV5PrazoDemanda ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
