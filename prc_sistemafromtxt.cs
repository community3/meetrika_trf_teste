/*
               File: PRC_SistemaFromTXT
        Description: Sistema From TXT
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:35.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_sistemafromtxt : GXProcedure
   {
      public prc_sistemafromtxt( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_sistemafromtxt( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Arquivo )
      {
         this.AV8Arquivo = aP0_Arquivo;
         initialize();
         executePrivate();
         aP0_Arquivo=this.AV8Arquivo;
      }

      public String executeUdp( )
      {
         this.AV8Arquivo = aP0_Arquivo;
         initialize();
         executePrivate();
         aP0_Arquivo=this.AV8Arquivo;
         return AV8Arquivo ;
      }

      public void executeSubmit( ref String aP0_Arquivo )
      {
         prc_sistemafromtxt objprc_sistemafromtxt;
         objprc_sistemafromtxt = new prc_sistemafromtxt();
         objprc_sistemafromtxt.AV8Arquivo = aP0_Arquivo;
         objprc_sistemafromtxt.context.SetSubmitInitialConfig(context);
         objprc_sistemafromtxt.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_sistemafromtxt);
         aP0_Arquivo=this.AV8Arquivo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_sistemafromtxt)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9i = context.FileIOInstance.dfropen( AV8Arquivo, 1024, ",", "\"", "");
         if ( AV9i == 0 )
         {
            while ( context.FileIOInstance.dfrnext( ) == 0 )
            {
               GXt_int1 = context.FileIOInstance.dfrgtxt( out  AV10Linha, 1024);
               AV9i = GXt_int1;
               /* Execute user subroutine: 'PROCESSARLINHA' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
            AV9i = context.FileIOInstance.dfrclose( );
         }
         else
         {
            AV17Texto = "Could not open file";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PROCESSARLINHA' Routine */
         AV9i = 0;
         AV11v = (short)(StringUtil.StringSearch( AV10Linha, ",", AV9i)-1);
         AV12Tabela_Nome = StringUtil.Substring( AV10Linha, AV9i, AV11v);
         AV9i = (short)(AV11v+2);
         AV11v = (short)(StringUtil.StringSearch( AV10Linha, ",", AV9i)-1);
         AV13Atributos_Nome = StringUtil.Substring( AV10Linha, AV9i, AV11v);
         AV9i = (short)(AV11v+2);
         AV11v = (short)(StringUtil.StringSearch( AV10Linha, ",", AV9i)-1);
         AV18Tipo = StringUtil.Substring( AV10Linha, AV9i, AV11v);
         if ( AV18Tipo - "C" )
         {
            AV14Atributos_TipoDados = "C";
         }
         else if ( AV18Tipo - "N" )
         {
            AV14Atributos_TipoDados = "N";
         }
         else if ( AV18Tipo - "D" )
         {
            AV14Atributos_TipoDados = "D";
         }
         else if ( AV18Tipo - "DT" )
         {
            AV14Atributos_TipoDados = "DT";
         }
         else if ( AV18Tipo - "VC" )
         {
            AV14Atributos_TipoDados = "VC";
         }
         else if ( AV18Tipo - "Bool" )
         {
            AV14Atributos_TipoDados = "Bool";
         }
         else if ( AV18Tipo - "Blob" )
         {
            AV14Atributos_TipoDados = "Blob";
         }
         else
         {
            AV14Atributos_TipoDados = "Outr";
         }
         AV9i = (short)(AV11v+2);
         AV11v = (short)(StringUtil.StringSearch( AV10Linha, ",", AV9i)-1);
         AV9i = (short)(AV11v+2);
         AV11v = (short)(StringUtil.StringSearch( AV10Linha, ",", AV9i)-1);
         AV9i = (short)(AV11v+2);
         AV11v = (short)(StringUtil.StringSearch( AV10Linha, ",", AV9i)-1);
         AV9i = (short)(AV11v+2);
         AV15Atributos_PK = (bool)(((StringUtil.StrCmp(StringUtil.Substring( AV10Linha, AV9i, 1), "P")==0)));
         AV16Atributos_FK = (bool)(((StringUtil.StrCmp(StringUtil.Substring( AV10Linha, AV9i, 1), "F")==0)));
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10Linha = "";
         AV17Texto = "";
         AV12Tabela_Nome = "";
         AV13Atributos_Nome = "";
         AV18Tipo = "";
         AV14Atributos_TipoDados = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9i ;
      private short GXt_int1 ;
      private short AV11v ;
      private String AV10Linha ;
      private String AV17Texto ;
      private String AV12Tabela_Nome ;
      private String AV13Atributos_Nome ;
      private String AV18Tipo ;
      private String AV14Atributos_TipoDados ;
      private bool returnInSub ;
      private bool AV15Atributos_PK ;
      private bool AV16Atributos_FK ;
      private String AV8Arquivo ;
      private String aP0_Arquivo ;
   }

}
