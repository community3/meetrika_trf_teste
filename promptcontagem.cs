/*
               File: PromptContagem
        Description: Prompt Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:39:48.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontagem : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContagem_Codigo ,
                           ref int aP1_InOutContagem_ContratadaCod )
      {
         this.AV7InOutContagem_Codigo = aP0_InOutContagem_Codigo;
         this.AV8InOutContagem_ContratadaCod = aP1_InOutContagem_ContratadaCod;
         executePrivate();
         aP0_InOutContagem_Codigo=this.AV7InOutContagem_Codigo;
         aP1_InOutContagem_ContratadaCod=this.AV8InOutContagem_ContratadaCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbContagem_Tecnica = new GXCombobox();
         cmbContagem_Tipo = new GXCombobox();
         cmbContagem_Status = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_86 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_86_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_86_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Contagem_ContratadaCod1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contagem_ContratadaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contagem_ContratadaCod1), 6, 0)));
               AV18Contagem_AreaTrabalhoDes1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contagem_AreaTrabalhoDes1", AV18Contagem_AreaTrabalhoDes1);
               AV19Contagem_UsuarioContadorPessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contagem_UsuarioContadorPessoaNom1", AV19Contagem_UsuarioContadorPessoaNom1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23Contagem_ContratadaCod2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contagem_ContratadaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contagem_ContratadaCod2), 6, 0)));
               AV24Contagem_AreaTrabalhoDes2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contagem_AreaTrabalhoDes2", AV24Contagem_AreaTrabalhoDes2);
               AV25Contagem_UsuarioContadorPessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contagem_UsuarioContadorPessoaNom2", AV25Contagem_UsuarioContadorPessoaNom2);
               AV27DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
               AV29Contagem_ContratadaCod3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Contagem_ContratadaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Contagem_ContratadaCod3), 6, 0)));
               AV30Contagem_AreaTrabalhoDes3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Contagem_AreaTrabalhoDes3", AV30Contagem_AreaTrabalhoDes3);
               AV31Contagem_UsuarioContadorPessoaNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Contagem_UsuarioContadorPessoaNom3", AV31Contagem_UsuarioContadorPessoaNom3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV26DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contagem_ContratadaCod1, AV18Contagem_AreaTrabalhoDes1, AV19Contagem_UsuarioContadorPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contagem_ContratadaCod2, AV24Contagem_AreaTrabalhoDes2, AV25Contagem_UsuarioContadorPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Contagem_ContratadaCod3, AV30Contagem_AreaTrabalhoDes3, AV31Contagem_UsuarioContadorPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContagem_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagem_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContagem_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContagem_ContratadaCod), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAHZ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSHZ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEHZ2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299394880");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontagem.aspx") + "?" + UrlEncode("" +AV7InOutContagem_Codigo) + "," + UrlEncode("" +AV8InOutContagem_ContratadaCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_CONTRATADACOD1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Contagem_ContratadaCod1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_AREATRABALHODES1", AV18Contagem_AreaTrabalhoDes1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_USUARIOCONTADORPESSOANOM1", StringUtil.RTrim( AV19Contagem_UsuarioContadorPessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_CONTRATADACOD2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23Contagem_ContratadaCod2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_AREATRABALHODES2", AV24Contagem_AreaTrabalhoDes2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_USUARIOCONTADORPESSOANOM2", StringUtil.RTrim( AV25Contagem_UsuarioContadorPessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV27DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_CONTRATADACOD3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29Contagem_ContratadaCod3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_AREATRABALHODES3", AV30Contagem_AreaTrabalhoDes3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEM_USUARIOCONTADORPESSOANOM3", StringUtil.RTrim( AV31Contagem_UsuarioContadorPessoaNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV26DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_86", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_86), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV173GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV174GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV33DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV32DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContagem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEM_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8InOutContagem_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormHZ2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContagem" ;
      }

      public override String GetPgmdesc( )
      {
         return "Prompt Contagem" ;
      }

      protected void WBHZ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_HZ2( true) ;
         }
         else
         {
            wb_table1_2_HZ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_HZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(117, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV26DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(118, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"");
         }
         wbLoad = true;
      }

      protected void STARTHZ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Prompt Contagem", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPHZ0( ) ;
      }

      protected void WSHZ2( )
      {
         STARTHZ2( ) ;
         EVTHZ2( ) ;
      }

      protected void EVTHZ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11HZ2 */
                           E11HZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12HZ2 */
                           E12HZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13HZ2 */
                           E13HZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14HZ2 */
                           E14HZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15HZ2 */
                           E15HZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16HZ2 */
                           E16HZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17HZ2 */
                           E17HZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18HZ2 */
                           E18HZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19HZ2 */
                           E19HZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20HZ2 */
                           E20HZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21HZ2 */
                           E21HZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_86_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
                           SubsflControlProps_862( ) ;
                           GXCCtl = "CONTAGEM_PROPOSITO_Enabled_" + sGXsfl_86_idx;
                           Contagem_proposito_Enabled = StringUtil.StrToBool( cgiGet( GXCCtl));
                           context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Contagem_proposito_Internalname, "Enabled", StringUtil.BoolToStr( Contagem_proposito_Enabled));
                           GXCCtl = "CONTAGEM_ESCOPO_Enabled_" + sGXsfl_86_idx;
                           Contagem_escopo_Enabled = StringUtil.StrToBool( cgiGet( GXCCtl));
                           context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Contagem_escopo_Internalname, "Enabled", StringUtil.BoolToStr( Contagem_escopo_Enabled));
                           GXCCtl = "CONTAGEM_FRONTEIRA_Enabled_" + sGXsfl_86_idx;
                           Contagem_fronteira_Enabled = StringUtil.StrToBool( cgiGet( GXCCtl));
                           context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Contagem_fronteira_Internalname, "Enabled", StringUtil.BoolToStr( Contagem_fronteira_Enabled));
                           GXCCtl = "CONTAGEM_OBSERVACAO_Enabled_" + sGXsfl_86_idx;
                           Contagem_observacao_Enabled = StringUtil.StrToBool( cgiGet( GXCCtl));
                           context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Contagem_observacao_Internalname, "Enabled", StringUtil.BoolToStr( Contagem_observacao_Enabled));
                           AV34Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Select)) ? AV177Select_GXI : context.convertURL( context.PathToRelativeUrl( AV34Select))));
                           A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
                           A193Contagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_AreaTrabalhoCod_Internalname), ",", "."));
                           A1118Contagem_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_ContratadaCod_Internalname), ",", "."));
                           n1118Contagem_ContratadaCod = false;
                           A197Contagem_DataCriacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagem_DataCriacao_Internalname), 0));
                           A194Contagem_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtContagem_AreaTrabalhoDes_Internalname));
                           n194Contagem_AreaTrabalhoDes = false;
                           cmbContagem_Tecnica.Name = cmbContagem_Tecnica_Internalname;
                           cmbContagem_Tecnica.CurrentValue = cgiGet( cmbContagem_Tecnica_Internalname);
                           A195Contagem_Tecnica = cgiGet( cmbContagem_Tecnica_Internalname);
                           n195Contagem_Tecnica = false;
                           cmbContagem_Tipo.Name = cmbContagem_Tipo_Internalname;
                           cmbContagem_Tipo.CurrentValue = cgiGet( cmbContagem_Tipo_Internalname);
                           A196Contagem_Tipo = cgiGet( cmbContagem_Tipo_Internalname);
                           n196Contagem_Tipo = false;
                           A1059Contagem_Notas = cgiGet( edtContagem_Notas_Internalname);
                           n1059Contagem_Notas = false;
                           A943Contagem_PFB = context.localUtil.CToN( cgiGet( edtContagem_PFB_Internalname), ",", ".");
                           n943Contagem_PFB = false;
                           A944Contagem_PFL = context.localUtil.CToN( cgiGet( edtContagem_PFL_Internalname), ",", ".");
                           n944Contagem_PFL = false;
                           A1119Contagem_Divergencia = context.localUtil.CToN( cgiGet( edtContagem_Divergencia_Internalname), ",", ".");
                           n1119Contagem_Divergencia = false;
                           A213Contagem_UsuarioContadorCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorCod_Internalname), ",", "."));
                           n213Contagem_UsuarioContadorCod = false;
                           A214Contagem_UsuarioContadorPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorPessoaCod_Internalname), ",", "."));
                           n214Contagem_UsuarioContadorPessoaCod = false;
                           A215Contagem_UsuarioContadorPessoaNom = StringUtil.Upper( cgiGet( edtContagem_UsuarioContadorPessoaNom_Internalname));
                           n215Contagem_UsuarioContadorPessoaNom = false;
                           cmbContagem_Status.Name = cmbContagem_Status_Internalname;
                           cmbContagem_Status.CurrentValue = cgiGet( cmbContagem_Status_Internalname);
                           A262Contagem_Status = cgiGet( cmbContagem_Status_Internalname);
                           n262Contagem_Status = false;
                           A945Contagem_Demanda = cgiGet( edtContagem_Demanda_Internalname);
                           n945Contagem_Demanda = false;
                           A946Contagem_Link = cgiGet( edtContagem_Link_Internalname);
                           n946Contagem_Link = false;
                           A947Contagem_Fator = context.localUtil.CToN( cgiGet( edtContagem_Fator_Internalname), ",", ".");
                           n947Contagem_Fator = false;
                           A1117Contagem_Deflator = context.localUtil.CToN( cgiGet( edtContagem_Deflator_Internalname), ",", ".");
                           n1117Contagem_Deflator = false;
                           A940Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_SistemaCod_Internalname), ",", "."));
                           n940Contagem_SistemaCod = false;
                           A941Contagem_SistemaSigla = StringUtil.Upper( cgiGet( edtContagem_SistemaSigla_Internalname));
                           n941Contagem_SistemaSigla = false;
                           GXCCtl = "CONTAGEM_PROPOSITO_" + sGXsfl_86_idx;
                           A199Contagem_Proposito = cgiGet( GXCCtl);
                           n199Contagem_Proposito = false;
                           GXCCtl = "CONTAGEM_ESCOPO_" + sGXsfl_86_idx;
                           A200Contagem_Escopo = cgiGet( GXCCtl);
                           n200Contagem_Escopo = false;
                           GXCCtl = "CONTAGEM_FRONTEIRA_" + sGXsfl_86_idx;
                           A201Contagem_Fronteira = cgiGet( GXCCtl);
                           n201Contagem_Fronteira = false;
                           GXCCtl = "CONTAGEM_OBSERVACAO_" + sGXsfl_86_idx;
                           A202Contagem_Observacao = cgiGet( GXCCtl);
                           n202Contagem_Observacao = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E22HZ2 */
                                 E22HZ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23HZ2 */
                                 E23HZ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E24HZ2 */
                                 E24HZ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_contratadacod1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEM_CONTRATADACOD1"), ",", ".") != Convert.ToDecimal( AV17Contagem_ContratadaCod1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_areatrabalhodes1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_AREATRABALHODES1"), AV18Contagem_AreaTrabalhoDes1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_usuariocontadorpessoanom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_USUARIOCONTADORPESSOANOM1"), AV19Contagem_UsuarioContadorPessoaNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_contratadacod2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEM_CONTRATADACOD2"), ",", ".") != Convert.ToDecimal( AV23Contagem_ContratadaCod2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_areatrabalhodes2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_AREATRABALHODES2"), AV24Contagem_AreaTrabalhoDes2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_usuariocontadorpessoanom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_USUARIOCONTADORPESSOANOM2"), AV25Contagem_UsuarioContadorPessoaNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_contratadacod3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEM_CONTRATADACOD3"), ",", ".") != Convert.ToDecimal( AV29Contagem_ContratadaCod3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_areatrabalhodes3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_AREATRABALHODES3"), AV30Contagem_AreaTrabalhoDes3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagem_usuariocontadorpessoanom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_USUARIOCONTADORPESSOANOM3"), AV31Contagem_UsuarioContadorPessoaNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E25HZ2 */
                                       E25HZ2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEHZ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormHZ2( ) ;
            }
         }
      }

      protected void PAHZ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEM_CONTRATADACOD", "Contratada", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEM_AREATRABALHODES", "�rea de Trabalho", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEM_USUARIOCONTADORPESSOANOM", "do Contador", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("4", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("5", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEM_CONTRATADACOD", "Contratada", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEM_AREATRABALHODES", "�rea de Trabalho", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEM_USUARIOCONTADORPESSOANOM", "do Contador", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            cmbavDynamicfiltersoperator2.addItem("3", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("4", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("5", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEM_CONTRATADACOD", "Contratada", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEM_AREATRABALHODES", "�rea de Trabalho", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEM_USUARIOCONTADORPESSOANOM", "do Contador", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
            cmbavDynamicfiltersoperator3.addItem("3", "<", 0);
            cmbavDynamicfiltersoperator3.addItem("4", "=", 0);
            cmbavDynamicfiltersoperator3.addItem("5", ">", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "CONTAGEM_TECNICA_" + sGXsfl_86_idx;
            cmbContagem_Tecnica.Name = GXCCtl;
            cmbContagem_Tecnica.WebTags = "";
            cmbContagem_Tecnica.addItem("", "Nenhuma", 0);
            cmbContagem_Tecnica.addItem("1", "Indicativa", 0);
            cmbContagem_Tecnica.addItem("2", "Estimada", 0);
            cmbContagem_Tecnica.addItem("3", "Detalhada", 0);
            if ( cmbContagem_Tecnica.ItemCount > 0 )
            {
               A195Contagem_Tecnica = cmbContagem_Tecnica.getValidValue(A195Contagem_Tecnica);
               n195Contagem_Tecnica = false;
            }
            GXCCtl = "CONTAGEM_TIPO_" + sGXsfl_86_idx;
            cmbContagem_Tipo.Name = GXCCtl;
            cmbContagem_Tipo.WebTags = "";
            cmbContagem_Tipo.addItem("", "(Nenhum)", 0);
            cmbContagem_Tipo.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbContagem_Tipo.addItem("M", "Projeto de Melhor�a", 0);
            cmbContagem_Tipo.addItem("C", "Projeto de Contagem", 0);
            cmbContagem_Tipo.addItem("A", "Aplica��o", 0);
            if ( cmbContagem_Tipo.ItemCount > 0 )
            {
               A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
               n196Contagem_Tipo = false;
            }
            GXCCtl = "CONTAGEM_STATUS_" + sGXsfl_86_idx;
            cmbContagem_Status.Name = GXCCtl;
            cmbContagem_Status.WebTags = "";
            if ( cmbContagem_Status.ItemCount > 0 )
            {
               A262Contagem_Status = cmbContagem_Status.getValidValue(A262Contagem_Status);
               n262Contagem_Status = false;
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_862( ) ;
         while ( nGXsfl_86_idx <= nRC_GXsfl_86 )
         {
            sendrow_862( ) ;
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       int AV17Contagem_ContratadaCod1 ,
                                       String AV18Contagem_AreaTrabalhoDes1 ,
                                       String AV19Contagem_UsuarioContadorPessoaNom1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       int AV23Contagem_ContratadaCod2 ,
                                       String AV24Contagem_AreaTrabalhoDes2 ,
                                       String AV25Contagem_UsuarioContadorPessoaNom2 ,
                                       String AV27DynamicFiltersSelector3 ,
                                       short AV28DynamicFiltersOperator3 ,
                                       int AV29Contagem_ContratadaCod3 ,
                                       String AV30Contagem_AreaTrabalhoDes3 ,
                                       String AV31Contagem_UsuarioContadorPessoaNom3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV26DynamicFiltersEnabled3 )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFHZ2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_CONTRATADACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1118Contagem_ContratadaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1118Contagem_ContratadaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_DATACRIACAO", GetSecureSignedToken( "", A197Contagem_DataCriacao));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_DATACRIACAO", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_TECNICA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A195Contagem_Tecnica, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_TECNICA", StringUtil.RTrim( A195Contagem_Tecnica));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A196Contagem_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_TIPO", StringUtil.RTrim( A196Contagem_Tipo));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_NOTAS", GetSecureSignedToken( "", A1059Contagem_Notas));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_NOTAS", A1059Contagem_Notas);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_PFB", GetSecureSignedToken( "", context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_PFB", StringUtil.LTrim( StringUtil.NToC( A943Contagem_PFB, 13, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_PFL", GetSecureSignedToken( "", context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_PFL", StringUtil.LTrim( StringUtil.NToC( A944Contagem_PFL, 13, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_DIVERGENCIA", GetSecureSignedToken( "", context.localUtil.Format( A1119Contagem_Divergencia, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_DIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( A1119Contagem_Divergencia, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_USUARIOCONTADORCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_USUARIOCONTADORCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_STATUS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A262Contagem_Status, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_STATUS", StringUtil.RTrim( A262Contagem_Status));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_DEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A945Contagem_Demanda, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_DEMANDA", A945Contagem_Demanda);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_LINK", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A946Contagem_Link, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_LINK", A946Contagem_Link);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_FATOR", GetSecureSignedToken( "", context.localUtil.Format( A947Contagem_Fator, "9.99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_FATOR", StringUtil.LTrim( StringUtil.NToC( A947Contagem_Fator, 4, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_DEFLATOR", GetSecureSignedToken( "", context.localUtil.Format( A1117Contagem_Deflator, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_DEFLATOR", StringUtil.LTrim( StringUtil.NToC( A1117Contagem_Deflator, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A940Contagem_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A940Contagem_SistemaCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFHZ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFHZ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 86;
         /* Execute user event: E23HZ2 */
         E23HZ2 ();
         nGXsfl_86_idx = 1;
         sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
         SubsflControlProps_862( ) ;
         nGXsfl_86_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_862( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17Contagem_ContratadaCod1 ,
                                                 AV18Contagem_AreaTrabalhoDes1 ,
                                                 AV19Contagem_UsuarioContadorPessoaNom1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV22DynamicFiltersOperator2 ,
                                                 AV23Contagem_ContratadaCod2 ,
                                                 AV24Contagem_AreaTrabalhoDes2 ,
                                                 AV25Contagem_UsuarioContadorPessoaNom2 ,
                                                 AV26DynamicFiltersEnabled3 ,
                                                 AV27DynamicFiltersSelector3 ,
                                                 AV28DynamicFiltersOperator3 ,
                                                 AV29Contagem_ContratadaCod3 ,
                                                 AV30Contagem_AreaTrabalhoDes3 ,
                                                 AV31Contagem_UsuarioContadorPessoaNom3 ,
                                                 A1118Contagem_ContratadaCod ,
                                                 A194Contagem_AreaTrabalhoDes ,
                                                 A215Contagem_UsuarioContadorPessoaNom ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV18Contagem_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV18Contagem_AreaTrabalhoDes1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contagem_AreaTrabalhoDes1", AV18Contagem_AreaTrabalhoDes1);
            lV18Contagem_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV18Contagem_AreaTrabalhoDes1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contagem_AreaTrabalhoDes1", AV18Contagem_AreaTrabalhoDes1);
            lV19Contagem_UsuarioContadorPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19Contagem_UsuarioContadorPessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contagem_UsuarioContadorPessoaNom1", AV19Contagem_UsuarioContadorPessoaNom1);
            lV19Contagem_UsuarioContadorPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19Contagem_UsuarioContadorPessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contagem_UsuarioContadorPessoaNom1", AV19Contagem_UsuarioContadorPessoaNom1);
            lV24Contagem_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV24Contagem_AreaTrabalhoDes2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contagem_AreaTrabalhoDes2", AV24Contagem_AreaTrabalhoDes2);
            lV24Contagem_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV24Contagem_AreaTrabalhoDes2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contagem_AreaTrabalhoDes2", AV24Contagem_AreaTrabalhoDes2);
            lV25Contagem_UsuarioContadorPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV25Contagem_UsuarioContadorPessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contagem_UsuarioContadorPessoaNom2", AV25Contagem_UsuarioContadorPessoaNom2);
            lV25Contagem_UsuarioContadorPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV25Contagem_UsuarioContadorPessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contagem_UsuarioContadorPessoaNom2", AV25Contagem_UsuarioContadorPessoaNom2);
            lV30Contagem_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV30Contagem_AreaTrabalhoDes3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Contagem_AreaTrabalhoDes3", AV30Contagem_AreaTrabalhoDes3);
            lV30Contagem_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV30Contagem_AreaTrabalhoDes3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Contagem_AreaTrabalhoDes3", AV30Contagem_AreaTrabalhoDes3);
            lV31Contagem_UsuarioContadorPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV31Contagem_UsuarioContadorPessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Contagem_UsuarioContadorPessoaNom3", AV31Contagem_UsuarioContadorPessoaNom3);
            lV31Contagem_UsuarioContadorPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV31Contagem_UsuarioContadorPessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Contagem_UsuarioContadorPessoaNom3", AV31Contagem_UsuarioContadorPessoaNom3);
            /* Using cursor H00HZ2 */
            pr_default.execute(0, new Object[] {AV17Contagem_ContratadaCod1, AV17Contagem_ContratadaCod1, AV17Contagem_ContratadaCod1, lV18Contagem_AreaTrabalhoDes1, lV18Contagem_AreaTrabalhoDes1, lV19Contagem_UsuarioContadorPessoaNom1, lV19Contagem_UsuarioContadorPessoaNom1, AV23Contagem_ContratadaCod2, AV23Contagem_ContratadaCod2, AV23Contagem_ContratadaCod2, lV24Contagem_AreaTrabalhoDes2, lV24Contagem_AreaTrabalhoDes2, lV25Contagem_UsuarioContadorPessoaNom2, lV25Contagem_UsuarioContadorPessoaNom2, AV29Contagem_ContratadaCod3, AV29Contagem_ContratadaCod3, AV29Contagem_ContratadaCod3, lV30Contagem_AreaTrabalhoDes3, lV30Contagem_AreaTrabalhoDes3, lV31Contagem_UsuarioContadorPessoaNom3, lV31Contagem_UsuarioContadorPessoaNom3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_86_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A199Contagem_Proposito = H00HZ2_A199Contagem_Proposito[0];
               n199Contagem_Proposito = H00HZ2_n199Contagem_Proposito[0];
               A200Contagem_Escopo = H00HZ2_A200Contagem_Escopo[0];
               n200Contagem_Escopo = H00HZ2_n200Contagem_Escopo[0];
               A201Contagem_Fronteira = H00HZ2_A201Contagem_Fronteira[0];
               n201Contagem_Fronteira = H00HZ2_n201Contagem_Fronteira[0];
               A202Contagem_Observacao = H00HZ2_A202Contagem_Observacao[0];
               n202Contagem_Observacao = H00HZ2_n202Contagem_Observacao[0];
               A941Contagem_SistemaSigla = H00HZ2_A941Contagem_SistemaSigla[0];
               n941Contagem_SistemaSigla = H00HZ2_n941Contagem_SistemaSigla[0];
               A940Contagem_SistemaCod = H00HZ2_A940Contagem_SistemaCod[0];
               n940Contagem_SistemaCod = H00HZ2_n940Contagem_SistemaCod[0];
               A1117Contagem_Deflator = H00HZ2_A1117Contagem_Deflator[0];
               n1117Contagem_Deflator = H00HZ2_n1117Contagem_Deflator[0];
               A947Contagem_Fator = H00HZ2_A947Contagem_Fator[0];
               n947Contagem_Fator = H00HZ2_n947Contagem_Fator[0];
               A946Contagem_Link = H00HZ2_A946Contagem_Link[0];
               n946Contagem_Link = H00HZ2_n946Contagem_Link[0];
               A945Contagem_Demanda = H00HZ2_A945Contagem_Demanda[0];
               n945Contagem_Demanda = H00HZ2_n945Contagem_Demanda[0];
               A262Contagem_Status = H00HZ2_A262Contagem_Status[0];
               n262Contagem_Status = H00HZ2_n262Contagem_Status[0];
               A215Contagem_UsuarioContadorPessoaNom = H00HZ2_A215Contagem_UsuarioContadorPessoaNom[0];
               n215Contagem_UsuarioContadorPessoaNom = H00HZ2_n215Contagem_UsuarioContadorPessoaNom[0];
               A214Contagem_UsuarioContadorPessoaCod = H00HZ2_A214Contagem_UsuarioContadorPessoaCod[0];
               n214Contagem_UsuarioContadorPessoaCod = H00HZ2_n214Contagem_UsuarioContadorPessoaCod[0];
               A213Contagem_UsuarioContadorCod = H00HZ2_A213Contagem_UsuarioContadorCod[0];
               n213Contagem_UsuarioContadorCod = H00HZ2_n213Contagem_UsuarioContadorCod[0];
               A1119Contagem_Divergencia = H00HZ2_A1119Contagem_Divergencia[0];
               n1119Contagem_Divergencia = H00HZ2_n1119Contagem_Divergencia[0];
               A944Contagem_PFL = H00HZ2_A944Contagem_PFL[0];
               n944Contagem_PFL = H00HZ2_n944Contagem_PFL[0];
               A943Contagem_PFB = H00HZ2_A943Contagem_PFB[0];
               n943Contagem_PFB = H00HZ2_n943Contagem_PFB[0];
               A1059Contagem_Notas = H00HZ2_A1059Contagem_Notas[0];
               n1059Contagem_Notas = H00HZ2_n1059Contagem_Notas[0];
               A196Contagem_Tipo = H00HZ2_A196Contagem_Tipo[0];
               n196Contagem_Tipo = H00HZ2_n196Contagem_Tipo[0];
               A195Contagem_Tecnica = H00HZ2_A195Contagem_Tecnica[0];
               n195Contagem_Tecnica = H00HZ2_n195Contagem_Tecnica[0];
               A194Contagem_AreaTrabalhoDes = H00HZ2_A194Contagem_AreaTrabalhoDes[0];
               n194Contagem_AreaTrabalhoDes = H00HZ2_n194Contagem_AreaTrabalhoDes[0];
               A197Contagem_DataCriacao = H00HZ2_A197Contagem_DataCriacao[0];
               A1118Contagem_ContratadaCod = H00HZ2_A1118Contagem_ContratadaCod[0];
               n1118Contagem_ContratadaCod = H00HZ2_n1118Contagem_ContratadaCod[0];
               A193Contagem_AreaTrabalhoCod = H00HZ2_A193Contagem_AreaTrabalhoCod[0];
               A192Contagem_Codigo = H00HZ2_A192Contagem_Codigo[0];
               A941Contagem_SistemaSigla = H00HZ2_A941Contagem_SistemaSigla[0];
               n941Contagem_SistemaSigla = H00HZ2_n941Contagem_SistemaSigla[0];
               A214Contagem_UsuarioContadorPessoaCod = H00HZ2_A214Contagem_UsuarioContadorPessoaCod[0];
               n214Contagem_UsuarioContadorPessoaCod = H00HZ2_n214Contagem_UsuarioContadorPessoaCod[0];
               A215Contagem_UsuarioContadorPessoaNom = H00HZ2_A215Contagem_UsuarioContadorPessoaNom[0];
               n215Contagem_UsuarioContadorPessoaNom = H00HZ2_n215Contagem_UsuarioContadorPessoaNom[0];
               A194Contagem_AreaTrabalhoDes = H00HZ2_A194Contagem_AreaTrabalhoDes[0];
               n194Contagem_AreaTrabalhoDes = H00HZ2_n194Contagem_AreaTrabalhoDes[0];
               /* Execute user event: E24HZ2 */
               E24HZ2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 86;
            WBHZ0( ) ;
         }
         nGXsfl_86_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17Contagem_ContratadaCod1 ,
                                              AV18Contagem_AreaTrabalhoDes1 ,
                                              AV19Contagem_UsuarioContadorPessoaNom1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV22DynamicFiltersOperator2 ,
                                              AV23Contagem_ContratadaCod2 ,
                                              AV24Contagem_AreaTrabalhoDes2 ,
                                              AV25Contagem_UsuarioContadorPessoaNom2 ,
                                              AV26DynamicFiltersEnabled3 ,
                                              AV27DynamicFiltersSelector3 ,
                                              AV28DynamicFiltersOperator3 ,
                                              AV29Contagem_ContratadaCod3 ,
                                              AV30Contagem_AreaTrabalhoDes3 ,
                                              AV31Contagem_UsuarioContadorPessoaNom3 ,
                                              A1118Contagem_ContratadaCod ,
                                              A194Contagem_AreaTrabalhoDes ,
                                              A215Contagem_UsuarioContadorPessoaNom ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV18Contagem_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV18Contagem_AreaTrabalhoDes1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contagem_AreaTrabalhoDes1", AV18Contagem_AreaTrabalhoDes1);
         lV18Contagem_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV18Contagem_AreaTrabalhoDes1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contagem_AreaTrabalhoDes1", AV18Contagem_AreaTrabalhoDes1);
         lV19Contagem_UsuarioContadorPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19Contagem_UsuarioContadorPessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contagem_UsuarioContadorPessoaNom1", AV19Contagem_UsuarioContadorPessoaNom1);
         lV19Contagem_UsuarioContadorPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19Contagem_UsuarioContadorPessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contagem_UsuarioContadorPessoaNom1", AV19Contagem_UsuarioContadorPessoaNom1);
         lV24Contagem_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV24Contagem_AreaTrabalhoDes2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contagem_AreaTrabalhoDes2", AV24Contagem_AreaTrabalhoDes2);
         lV24Contagem_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV24Contagem_AreaTrabalhoDes2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contagem_AreaTrabalhoDes2", AV24Contagem_AreaTrabalhoDes2);
         lV25Contagem_UsuarioContadorPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV25Contagem_UsuarioContadorPessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contagem_UsuarioContadorPessoaNom2", AV25Contagem_UsuarioContadorPessoaNom2);
         lV25Contagem_UsuarioContadorPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV25Contagem_UsuarioContadorPessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contagem_UsuarioContadorPessoaNom2", AV25Contagem_UsuarioContadorPessoaNom2);
         lV30Contagem_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV30Contagem_AreaTrabalhoDes3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Contagem_AreaTrabalhoDes3", AV30Contagem_AreaTrabalhoDes3);
         lV30Contagem_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV30Contagem_AreaTrabalhoDes3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Contagem_AreaTrabalhoDes3", AV30Contagem_AreaTrabalhoDes3);
         lV31Contagem_UsuarioContadorPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV31Contagem_UsuarioContadorPessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Contagem_UsuarioContadorPessoaNom3", AV31Contagem_UsuarioContadorPessoaNom3);
         lV31Contagem_UsuarioContadorPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV31Contagem_UsuarioContadorPessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Contagem_UsuarioContadorPessoaNom3", AV31Contagem_UsuarioContadorPessoaNom3);
         /* Using cursor H00HZ3 */
         pr_default.execute(1, new Object[] {AV17Contagem_ContratadaCod1, AV17Contagem_ContratadaCod1, AV17Contagem_ContratadaCod1, lV18Contagem_AreaTrabalhoDes1, lV18Contagem_AreaTrabalhoDes1, lV19Contagem_UsuarioContadorPessoaNom1, lV19Contagem_UsuarioContadorPessoaNom1, AV23Contagem_ContratadaCod2, AV23Contagem_ContratadaCod2, AV23Contagem_ContratadaCod2, lV24Contagem_AreaTrabalhoDes2, lV24Contagem_AreaTrabalhoDes2, lV25Contagem_UsuarioContadorPessoaNom2, lV25Contagem_UsuarioContadorPessoaNom2, AV29Contagem_ContratadaCod3, AV29Contagem_ContratadaCod3, AV29Contagem_ContratadaCod3, lV30Contagem_AreaTrabalhoDes3, lV30Contagem_AreaTrabalhoDes3, lV31Contagem_UsuarioContadorPessoaNom3, lV31Contagem_UsuarioContadorPessoaNom3});
         GRID_nRecordCount = H00HZ3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contagem_ContratadaCod1, AV18Contagem_AreaTrabalhoDes1, AV19Contagem_UsuarioContadorPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contagem_ContratadaCod2, AV24Contagem_AreaTrabalhoDes2, AV25Contagem_UsuarioContadorPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Contagem_ContratadaCod3, AV30Contagem_AreaTrabalhoDes3, AV31Contagem_UsuarioContadorPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contagem_ContratadaCod1, AV18Contagem_AreaTrabalhoDes1, AV19Contagem_UsuarioContadorPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contagem_ContratadaCod2, AV24Contagem_AreaTrabalhoDes2, AV25Contagem_UsuarioContadorPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Contagem_ContratadaCod3, AV30Contagem_AreaTrabalhoDes3, AV31Contagem_UsuarioContadorPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contagem_ContratadaCod1, AV18Contagem_AreaTrabalhoDes1, AV19Contagem_UsuarioContadorPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contagem_ContratadaCod2, AV24Contagem_AreaTrabalhoDes2, AV25Contagem_UsuarioContadorPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Contagem_ContratadaCod3, AV30Contagem_AreaTrabalhoDes3, AV31Contagem_UsuarioContadorPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contagem_ContratadaCod1, AV18Contagem_AreaTrabalhoDes1, AV19Contagem_UsuarioContadorPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contagem_ContratadaCod2, AV24Contagem_AreaTrabalhoDes2, AV25Contagem_UsuarioContadorPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Contagem_ContratadaCod3, AV30Contagem_AreaTrabalhoDes3, AV31Contagem_UsuarioContadorPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contagem_ContratadaCod1, AV18Contagem_AreaTrabalhoDes1, AV19Contagem_UsuarioContadorPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contagem_ContratadaCod2, AV24Contagem_AreaTrabalhoDes2, AV25Contagem_UsuarioContadorPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Contagem_ContratadaCod3, AV30Contagem_AreaTrabalhoDes3, AV31Contagem_UsuarioContadorPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         }
         return (int)(0) ;
      }

      protected void STRUPHZ0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22HZ2 */
         E22HZ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagem_contratadacod1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagem_contratadacod1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEM_CONTRATADACOD1");
               GX_FocusControl = edtavContagem_contratadacod1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17Contagem_ContratadaCod1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contagem_ContratadaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contagem_ContratadaCod1), 6, 0)));
            }
            else
            {
               AV17Contagem_ContratadaCod1 = (int)(context.localUtil.CToN( cgiGet( edtavContagem_contratadacod1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contagem_ContratadaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contagem_ContratadaCod1), 6, 0)));
            }
            AV18Contagem_AreaTrabalhoDes1 = StringUtil.Upper( cgiGet( edtavContagem_areatrabalhodes1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contagem_AreaTrabalhoDes1", AV18Contagem_AreaTrabalhoDes1);
            AV19Contagem_UsuarioContadorPessoaNom1 = StringUtil.Upper( cgiGet( edtavContagem_usuariocontadorpessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contagem_UsuarioContadorPessoaNom1", AV19Contagem_UsuarioContadorPessoaNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagem_contratadacod2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagem_contratadacod2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEM_CONTRATADACOD2");
               GX_FocusControl = edtavContagem_contratadacod2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23Contagem_ContratadaCod2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contagem_ContratadaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contagem_ContratadaCod2), 6, 0)));
            }
            else
            {
               AV23Contagem_ContratadaCod2 = (int)(context.localUtil.CToN( cgiGet( edtavContagem_contratadacod2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contagem_ContratadaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contagem_ContratadaCod2), 6, 0)));
            }
            AV24Contagem_AreaTrabalhoDes2 = StringUtil.Upper( cgiGet( edtavContagem_areatrabalhodes2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contagem_AreaTrabalhoDes2", AV24Contagem_AreaTrabalhoDes2);
            AV25Contagem_UsuarioContadorPessoaNom2 = StringUtil.Upper( cgiGet( edtavContagem_usuariocontadorpessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contagem_UsuarioContadorPessoaNom2", AV25Contagem_UsuarioContadorPessoaNom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV27DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagem_contratadacod3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagem_contratadacod3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEM_CONTRATADACOD3");
               GX_FocusControl = edtavContagem_contratadacod3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29Contagem_ContratadaCod3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Contagem_ContratadaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Contagem_ContratadaCod3), 6, 0)));
            }
            else
            {
               AV29Contagem_ContratadaCod3 = (int)(context.localUtil.CToN( cgiGet( edtavContagem_contratadacod3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Contagem_ContratadaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Contagem_ContratadaCod3), 6, 0)));
            }
            AV30Contagem_AreaTrabalhoDes3 = StringUtil.Upper( cgiGet( edtavContagem_areatrabalhodes3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Contagem_AreaTrabalhoDes3", AV30Contagem_AreaTrabalhoDes3);
            AV31Contagem_UsuarioContadorPessoaNom3 = StringUtil.Upper( cgiGet( edtavContagem_usuariocontadorpessoanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Contagem_UsuarioContadorPessoaNom3", AV31Contagem_UsuarioContadorPessoaNom3);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV26DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_86 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_86"), ",", "."));
            AV173GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV174GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEM_CONTRATADACOD1"), ",", ".") != Convert.ToDecimal( AV17Contagem_ContratadaCod1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_AREATRABALHODES1"), AV18Contagem_AreaTrabalhoDes1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_USUARIOCONTADORPESSOANOM1"), AV19Contagem_UsuarioContadorPessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEM_CONTRATADACOD2"), ",", ".") != Convert.ToDecimal( AV23Contagem_ContratadaCod2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_AREATRABALHODES2"), AV24Contagem_AreaTrabalhoDes2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_USUARIOCONTADORPESSOANOM2"), AV25Contagem_UsuarioContadorPessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEM_CONTRATADACOD3"), ",", ".") != Convert.ToDecimal( AV29Contagem_ContratadaCod3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_AREATRABALHODES3"), AV30Contagem_AreaTrabalhoDes3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEM_USUARIOCONTADORPESSOANOM3"), AV31Contagem_UsuarioContadorPessoaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22HZ2 */
         E22HZ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22HZ2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTAGEM_CONTRATADACOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "CONTAGEM_CONTRATADACOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersSelector3 = "CONTAGEM_CONTRATADACOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = "Select Contagem ";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Contratada", 0);
         cmbavOrderedby.addItem("2", "C�digo", 0);
         cmbavOrderedby.addItem("3", "Cod. �rea de Trabalho", 0);
         cmbavOrderedby.addItem("4", "Data da contagem", 0);
         cmbavOrderedby.addItem("5", "�rea de Trabalho", 0);
         cmbavOrderedby.addItem("6", "T�cnica de Contagem", 0);
         cmbavOrderedby.addItem("7", "Tipo de Contagem", 0);
         cmbavOrderedby.addItem("8", "Notas", 0);
         cmbavOrderedby.addItem("9", "PFB", 0);
         cmbavOrderedby.addItem("10", "PFL", 0);
         cmbavOrderedby.addItem("11", "Contagem_Divergencia", 0);
         cmbavOrderedby.addItem("12", "Usu�rio Contador", 0);
         cmbavOrderedby.addItem("13", "Pessoa Contador", 0);
         cmbavOrderedby.addItem("14", "do Contador", 0);
         cmbavOrderedby.addItem("15", "Status", 0);
         cmbavOrderedby.addItem("16", "Demanda", 0);
         cmbavOrderedby.addItem("17", "Link", 0);
         cmbavOrderedby.addItem("18", "Fator", 0);
         cmbavOrderedby.addItem("19", "Deflator", 0);
         cmbavOrderedby.addItem("20", "Sistema", 0);
         cmbavOrderedby.addItem("21", "Sistema", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E23HZ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_CONTRATADACOD") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("4", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("5", ">", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_AREATRABALHODES") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_CONTRATADACOD") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
               cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "<", 0);
               cmbavDynamicfiltersoperator2.addItem("4", "=", 0);
               cmbavDynamicfiltersoperator2.addItem("5", ">", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_AREATRABALHODES") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            if ( AV26DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_CONTRATADACOD") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "<", 0);
                  cmbavDynamicfiltersoperator3.addItem("4", "=", 0);
                  cmbavDynamicfiltersoperator3.addItem("5", ">", 0);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_AREATRABALHODES") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
            }
         }
         edtContagem_Codigo_Titleformat = 2;
         edtContagem_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "C�digo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Codigo_Internalname, "Title", edtContagem_Codigo_Title);
         edtContagem_AreaTrabalhoCod_Titleformat = 2;
         edtContagem_AreaTrabalhoCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Cod. �rea de Trabalho", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_AreaTrabalhoCod_Internalname, "Title", edtContagem_AreaTrabalhoCod_Title);
         edtContagem_ContratadaCod_Titleformat = 2;
         edtContagem_ContratadaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Contratada", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_ContratadaCod_Internalname, "Title", edtContagem_ContratadaCod_Title);
         edtContagem_DataCriacao_Titleformat = 2;
         edtContagem_DataCriacao_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Data da contagem", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_DataCriacao_Internalname, "Title", edtContagem_DataCriacao_Title);
         edtContagem_AreaTrabalhoDes_Titleformat = 2;
         edtContagem_AreaTrabalhoDes_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "�rea de Trabalho", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_AreaTrabalhoDes_Internalname, "Title", edtContagem_AreaTrabalhoDes_Title);
         cmbContagem_Tecnica_Titleformat = 2;
         cmbContagem_Tecnica.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV13OrderedBy==6) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "T�cnica de Contagem", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tecnica_Internalname, "Title", cmbContagem_Tecnica.Title.Text);
         cmbContagem_Tipo_Titleformat = 2;
         cmbContagem_Tipo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV13OrderedBy==7) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tipo de Contagem", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tipo_Internalname, "Title", cmbContagem_Tipo.Title.Text);
         edtContagem_Notas_Titleformat = 2;
         edtContagem_Notas_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV13OrderedBy==8) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Notas", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Notas_Internalname, "Title", edtContagem_Notas_Title);
         edtContagem_PFB_Titleformat = 2;
         edtContagem_PFB_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV13OrderedBy==9) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "PFB", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_PFB_Internalname, "Title", edtContagem_PFB_Title);
         edtContagem_PFL_Titleformat = 2;
         edtContagem_PFL_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 10);' >%5</span>", ((AV13OrderedBy==10) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "PFL", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_PFL_Internalname, "Title", edtContagem_PFL_Title);
         edtContagem_Divergencia_Titleformat = 2;
         edtContagem_Divergencia_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 11);' >%5</span>", ((AV13OrderedBy==11) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Contagem_Divergencia", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Divergencia_Internalname, "Title", edtContagem_Divergencia_Title);
         edtContagem_UsuarioContadorCod_Titleformat = 2;
         edtContagem_UsuarioContadorCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 12);' >%5</span>", ((AV13OrderedBy==12) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Usu�rio Contador", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorCod_Internalname, "Title", edtContagem_UsuarioContadorCod_Title);
         edtContagem_UsuarioContadorPessoaCod_Titleformat = 2;
         edtContagem_UsuarioContadorPessoaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 13);' >%5</span>", ((AV13OrderedBy==13) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Pessoa Contador", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorPessoaCod_Internalname, "Title", edtContagem_UsuarioContadorPessoaCod_Title);
         edtContagem_UsuarioContadorPessoaNom_Titleformat = 2;
         edtContagem_UsuarioContadorPessoaNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 14);' >%5</span>", ((AV13OrderedBy==14) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "do Contador", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorPessoaNom_Internalname, "Title", edtContagem_UsuarioContadorPessoaNom_Title);
         cmbContagem_Status_Titleformat = 2;
         cmbContagem_Status.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 15);' >%5</span>", ((AV13OrderedBy==15) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Status", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Status_Internalname, "Title", cmbContagem_Status.Title.Text);
         edtContagem_Demanda_Titleformat = 2;
         edtContagem_Demanda_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 16);' >%5</span>", ((AV13OrderedBy==16) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Demanda", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Demanda_Internalname, "Title", edtContagem_Demanda_Title);
         edtContagem_Link_Titleformat = 2;
         edtContagem_Link_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 17);' >%5</span>", ((AV13OrderedBy==17) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Link", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Link_Internalname, "Title", edtContagem_Link_Title);
         edtContagem_Fator_Titleformat = 2;
         edtContagem_Fator_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 18);' >%5</span>", ((AV13OrderedBy==18) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Fator", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Fator_Internalname, "Title", edtContagem_Fator_Title);
         edtContagem_Deflator_Titleformat = 2;
         edtContagem_Deflator_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 19);' >%5</span>", ((AV13OrderedBy==19) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Deflator", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Deflator_Internalname, "Title", edtContagem_Deflator_Title);
         edtContagem_SistemaCod_Titleformat = 2;
         edtContagem_SistemaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 20);' >%5</span>", ((AV13OrderedBy==20) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Sistema", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_SistemaCod_Internalname, "Title", edtContagem_SistemaCod_Title);
         edtContagem_SistemaSigla_Titleformat = 2;
         edtContagem_SistemaSigla_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 21);' >%5</span>", ((AV13OrderedBy==21) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Sistema", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_SistemaSigla_Internalname, "Title", edtContagem_SistemaSigla_Title);
         AV173GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV173GridCurrentPage), 10, 0)));
         AV174GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV174GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV174GridPageCount), 10, 0)));
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E11HZ2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV172PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV172PageToGo) ;
         }
      }

      private void E24HZ2( )
      {
         /* Grid_Load Routine */
         AV34Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV34Select);
         AV177Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 86;
         }
         sendrow_862( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_86_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(86, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E25HZ2 */
         E25HZ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25HZ2( )
      {
         /* Enter Routine */
         AV7InOutContagem_Codigo = A192Contagem_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagem_Codigo), 6, 0)));
         AV8InOutContagem_ContratadaCod = A1118Contagem_ContratadaCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContagem_ContratadaCod), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContagem_Codigo,(int)AV8InOutContagem_ContratadaCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E12HZ2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E17HZ2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contagem_ContratadaCod1, AV18Contagem_AreaTrabalhoDes1, AV19Contagem_UsuarioContadorPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contagem_ContratadaCod2, AV24Contagem_AreaTrabalhoDes2, AV25Contagem_UsuarioContadorPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Contagem_ContratadaCod3, AV30Contagem_AreaTrabalhoDes3, AV31Contagem_UsuarioContadorPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
      }

      protected void E13HZ2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contagem_ContratadaCod1, AV18Contagem_AreaTrabalhoDes1, AV19Contagem_UsuarioContadorPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contagem_ContratadaCod2, AV24Contagem_AreaTrabalhoDes2, AV25Contagem_UsuarioContadorPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Contagem_ContratadaCod3, AV30Contagem_AreaTrabalhoDes3, AV31Contagem_UsuarioContadorPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E18HZ2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19HZ2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV26DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contagem_ContratadaCod1, AV18Contagem_AreaTrabalhoDes1, AV19Contagem_UsuarioContadorPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contagem_ContratadaCod2, AV24Contagem_AreaTrabalhoDes2, AV25Contagem_UsuarioContadorPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Contagem_ContratadaCod3, AV30Contagem_AreaTrabalhoDes3, AV31Contagem_UsuarioContadorPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
      }

      protected void E14HZ2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contagem_ContratadaCod1, AV18Contagem_AreaTrabalhoDes1, AV19Contagem_UsuarioContadorPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contagem_ContratadaCod2, AV24Contagem_AreaTrabalhoDes2, AV25Contagem_UsuarioContadorPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Contagem_ContratadaCod3, AV30Contagem_AreaTrabalhoDes3, AV31Contagem_UsuarioContadorPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20HZ2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E15HZ2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contagem_ContratadaCod1, AV18Contagem_AreaTrabalhoDes1, AV19Contagem_UsuarioContadorPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contagem_ContratadaCod2, AV24Contagem_AreaTrabalhoDes2, AV25Contagem_UsuarioContadorPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Contagem_ContratadaCod3, AV30Contagem_AreaTrabalhoDes3, AV31Contagem_UsuarioContadorPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21HZ2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV28DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E16HZ2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContagem_contratadacod1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_contratadacod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_contratadacod1_Visible), 5, 0)));
         edtavContagem_areatrabalhodes1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_areatrabalhodes1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_areatrabalhodes1_Visible), 5, 0)));
         edtavContagem_usuariocontadorpessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_usuariocontadorpessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_usuariocontadorpessoanom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_CONTRATADACOD") == 0 )
         {
            edtavContagem_contratadacod1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_contratadacod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_contratadacod1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_AREATRABALHODES") == 0 )
         {
            edtavContagem_areatrabalhodes1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_areatrabalhodes1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_areatrabalhodes1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
         {
            edtavContagem_usuariocontadorpessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_usuariocontadorpessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_usuariocontadorpessoanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContagem_contratadacod2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_contratadacod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_contratadacod2_Visible), 5, 0)));
         edtavContagem_areatrabalhodes2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_areatrabalhodes2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_areatrabalhodes2_Visible), 5, 0)));
         edtavContagem_usuariocontadorpessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_usuariocontadorpessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_usuariocontadorpessoanom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_CONTRATADACOD") == 0 )
         {
            edtavContagem_contratadacod2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_contratadacod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_contratadacod2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_AREATRABALHODES") == 0 )
         {
            edtavContagem_areatrabalhodes2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_areatrabalhodes2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_areatrabalhodes2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
         {
            edtavContagem_usuariocontadorpessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_usuariocontadorpessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_usuariocontadorpessoanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContagem_contratadacod3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_contratadacod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_contratadacod3_Visible), 5, 0)));
         edtavContagem_areatrabalhodes3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_areatrabalhodes3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_areatrabalhodes3_Visible), 5, 0)));
         edtavContagem_usuariocontadorpessoanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_usuariocontadorpessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_usuariocontadorpessoanom3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_CONTRATADACOD") == 0 )
         {
            edtavContagem_contratadacod3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_contratadacod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_contratadacod3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_AREATRABALHODES") == 0 )
         {
            edtavContagem_areatrabalhodes3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_areatrabalhodes3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_areatrabalhodes3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
         {
            edtavContagem_usuariocontadorpessoanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagem_usuariocontadorpessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_usuariocontadorpessoanom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S162( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "CONTAGEM_CONTRATADACOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23Contagem_ContratadaCod2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contagem_ContratadaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contagem_ContratadaCod2), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         AV27DynamicFiltersSelector3 = "CONTAGEM_CONTRATADACOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         AV28DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         AV29Contagem_ContratadaCod3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Contagem_ContratadaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Contagem_ContratadaCod3), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'CLEANFILTERS' Routine */
         AV15DynamicFiltersSelector1 = "CONTAGEM_CONTRATADACOD";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Contagem_ContratadaCod1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contagem_ContratadaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contagem_ContratadaCod1), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_CONTRATADACOD") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Contagem_ContratadaCod1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contagem_ContratadaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contagem_ContratadaCod1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_AREATRABALHODES") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18Contagem_AreaTrabalhoDes1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contagem_AreaTrabalhoDes1", AV18Contagem_AreaTrabalhoDes1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV19Contagem_UsuarioContadorPessoaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contagem_UsuarioContadorPessoaNom1", AV19Contagem_UsuarioContadorPessoaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_CONTRATADACOD") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23Contagem_ContratadaCod2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contagem_ContratadaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Contagem_ContratadaCod2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_AREATRABALHODES") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24Contagem_AreaTrabalhoDes2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Contagem_AreaTrabalhoDes2", AV24Contagem_AreaTrabalhoDes2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV25Contagem_UsuarioContadorPessoaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Contagem_UsuarioContadorPessoaNom2", AV25Contagem_UsuarioContadorPessoaNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV26DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV27DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_CONTRATADACOD") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV29Contagem_ContratadaCod3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Contagem_ContratadaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Contagem_ContratadaCod3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_AREATRABALHODES") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV30Contagem_AreaTrabalhoDes3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Contagem_AreaTrabalhoDes3", AV30Contagem_AreaTrabalhoDes3);
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV31Contagem_UsuarioContadorPessoaNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Contagem_UsuarioContadorPessoaNom3", AV31Contagem_UsuarioContadorPessoaNom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV32DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV33DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ! (0==AV17Contagem_ContratadaCod1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV17Contagem_ContratadaCod1), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contagem_AreaTrabalhoDes1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Contagem_AreaTrabalhoDes1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contagem_UsuarioContadorPessoaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19Contagem_UsuarioContadorPessoaNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ! (0==AV23Contagem_ContratadaCod2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV23Contagem_ContratadaCod2), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contagem_AreaTrabalhoDes2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV24Contagem_AreaTrabalhoDes2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Contagem_UsuarioContadorPessoaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Contagem_UsuarioContadorPessoaNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV26DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV27DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ! (0==AV29Contagem_ContratadaCod3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV29Contagem_ContratadaCod3), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Contagem_AreaTrabalhoDes3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV30Contagem_AreaTrabalhoDes3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Contagem_UsuarioContadorPessoaNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31Contagem_UsuarioContadorPessoaNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_HZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_HZ2( true) ;
         }
         else
         {
            wb_table2_5_HZ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_HZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_80_HZ2( true) ;
         }
         else
         {
            wb_table3_80_HZ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_80_HZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_HZ2e( true) ;
         }
         else
         {
            wb_table1_2_HZ2e( false) ;
         }
      }

      protected void wb_table3_80_HZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_83_HZ2( true) ;
         }
         else
         {
            wb_table4_83_HZ2( false) ;
         }
         return  ;
      }

      protected void wb_table4_83_HZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_80_HZ2e( true) ;
         }
         else
         {
            wb_table3_80_HZ2e( false) ;
         }
      }

      protected void wb_table4_83_HZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"86\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_AreaTrabalhoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_AreaTrabalhoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_AreaTrabalhoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_ContratadaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_ContratadaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_ContratadaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_DataCriacao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_DataCriacao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_DataCriacao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_AreaTrabalhoDes_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_AreaTrabalhoDes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_AreaTrabalhoDes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagem_Tecnica_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagem_Tecnica.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagem_Tecnica.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagem_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagem_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagem_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_Notas_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_Notas_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_Notas_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_PFB_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_PFB_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_PFB_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_PFL_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_PFL_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_PFL_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_Divergencia_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_Divergencia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_Divergencia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_UsuarioContadorCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_UsuarioContadorCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_UsuarioContadorCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_UsuarioContadorPessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_UsuarioContadorPessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_UsuarioContadorPessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_UsuarioContadorPessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_UsuarioContadorPessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_UsuarioContadorPessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagem_Status_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagem_Status.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagem_Status.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_Demanda_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_Demanda_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_Demanda_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_Link_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_Link_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_Link_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_Fator_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_Fator_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_Fator_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_Deflator_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_Deflator_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_Deflator_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_SistemaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_SistemaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_SistemaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_SistemaSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_SistemaSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_SistemaSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_AreaTrabalhoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_AreaTrabalhoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1118Contagem_ContratadaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_ContratadaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_ContratadaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_DataCriacao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_DataCriacao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A194Contagem_AreaTrabalhoDes);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_AreaTrabalhoDes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_AreaTrabalhoDes_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A195Contagem_Tecnica));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagem_Tecnica.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagem_Tecnica_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A196Contagem_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagem_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagem_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1059Contagem_Notas);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_Notas_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_Notas_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A943Contagem_PFB, 13, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_PFB_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_PFB_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A944Contagem_PFL, 13, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_PFL_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_PFL_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1119Contagem_Divergencia, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_Divergencia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_Divergencia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_UsuarioContadorCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_UsuarioContadorCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_UsuarioContadorPessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_UsuarioContadorPessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A215Contagem_UsuarioContadorPessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_UsuarioContadorPessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_UsuarioContadorPessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A262Contagem_Status));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagem_Status.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagem_Status_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A945Contagem_Demanda);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_Demanda_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_Demanda_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A946Contagem_Link);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_Link_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_Link_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A947Contagem_Fator, 4, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_Fator_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_Fator_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1117Contagem_Deflator, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_Deflator_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_Deflator_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A940Contagem_SistemaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_SistemaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_SistemaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A941Contagem_SistemaSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_SistemaSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_SistemaSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 86 )
         {
            wbEnd = 0;
            nRC_GXsfl_86 = (short)(nGXsfl_86_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_83_HZ2e( true) ;
         }
         else
         {
            wb_table4_83_HZ2e( false) ;
         }
      }

      protected void wb_table2_5_HZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContagem.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_14_HZ2( true) ;
         }
         else
         {
            wb_table5_14_HZ2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_HZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_HZ2e( true) ;
         }
         else
         {
            wb_table2_5_HZ2e( false) ;
         }
      }

      protected void wb_table5_14_HZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_HZ2( true) ;
         }
         else
         {
            wb_table6_19_HZ2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_HZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_HZ2e( true) ;
         }
         else
         {
            wb_table5_14_HZ2e( false) ;
         }
      }

      protected void wb_table6_19_HZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContagem.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_HZ2( true) ;
         }
         else
         {
            wb_table7_28_HZ2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_HZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_PromptContagem.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_47_HZ2( true) ;
         }
         else
         {
            wb_table8_47_HZ2( false) ;
         }
         return  ;
      }

      protected void wb_table8_47_HZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV27DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_PromptContagem.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_66_HZ2( true) ;
         }
         else
         {
            wb_table9_66_HZ2( false) ;
         }
         return  ;
      }

      protected void wb_table9_66_HZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_HZ2e( true) ;
         }
         else
         {
            wb_table6_19_HZ2e( false) ;
         }
      }

      protected void wb_table9_66_HZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_PromptContagem.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_contratadacod3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29Contagem_ContratadaCod3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29Contagem_ContratadaCod3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_contratadacod3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_contratadacod3_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_areatrabalhodes3_Internalname, AV30Contagem_AreaTrabalhoDes3, StringUtil.RTrim( context.localUtil.Format( AV30Contagem_AreaTrabalhoDes3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_areatrabalhodes3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_areatrabalhodes3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_usuariocontadorpessoanom3_Internalname, StringUtil.RTrim( AV31Contagem_UsuarioContadorPessoaNom3), StringUtil.RTrim( context.localUtil.Format( AV31Contagem_UsuarioContadorPessoaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_usuariocontadorpessoanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_usuariocontadorpessoanom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_66_HZ2e( true) ;
         }
         else
         {
            wb_table9_66_HZ2e( false) ;
         }
      }

      protected void wb_table8_47_HZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_PromptContagem.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_contratadacod2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23Contagem_ContratadaCod2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23Contagem_ContratadaCod2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_contratadacod2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_contratadacod2_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_areatrabalhodes2_Internalname, AV24Contagem_AreaTrabalhoDes2, StringUtil.RTrim( context.localUtil.Format( AV24Contagem_AreaTrabalhoDes2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_areatrabalhodes2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_areatrabalhodes2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_usuariocontadorpessoanom2_Internalname, StringUtil.RTrim( AV25Contagem_UsuarioContadorPessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV25Contagem_UsuarioContadorPessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_usuariocontadorpessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_usuariocontadorpessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_47_HZ2e( true) ;
         }
         else
         {
            wb_table8_47_HZ2e( false) ;
         }
      }

      protected void wb_table7_28_HZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContagem.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_contratadacod1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Contagem_ContratadaCod1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV17Contagem_ContratadaCod1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_contratadacod1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_contratadacod1_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_areatrabalhodes1_Internalname, AV18Contagem_AreaTrabalhoDes1, StringUtil.RTrim( context.localUtil.Format( AV18Contagem_AreaTrabalhoDes1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_areatrabalhodes1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_areatrabalhodes1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_usuariocontadorpessoanom1_Internalname, StringUtil.RTrim( AV19Contagem_UsuarioContadorPessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV19Contagem_UsuarioContadorPessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_usuariocontadorpessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_usuariocontadorpessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_HZ2e( true) ;
         }
         else
         {
            wb_table7_28_HZ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContagem_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagem_Codigo), 6, 0)));
         AV8InOutContagem_ContratadaCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContagem_ContratadaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAHZ2( ) ;
         WSHZ2( ) ;
         WEHZ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299395258");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontagem.js", "?20205299395259");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_idx;
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO_"+sGXsfl_86_idx;
         edtContagem_AreaTrabalhoCod_Internalname = "CONTAGEM_AREATRABALHOCOD_"+sGXsfl_86_idx;
         edtContagem_ContratadaCod_Internalname = "CONTAGEM_CONTRATADACOD_"+sGXsfl_86_idx;
         edtContagem_DataCriacao_Internalname = "CONTAGEM_DATACRIACAO_"+sGXsfl_86_idx;
         edtContagem_AreaTrabalhoDes_Internalname = "CONTAGEM_AREATRABALHODES_"+sGXsfl_86_idx;
         cmbContagem_Tecnica_Internalname = "CONTAGEM_TECNICA_"+sGXsfl_86_idx;
         cmbContagem_Tipo_Internalname = "CONTAGEM_TIPO_"+sGXsfl_86_idx;
         Contagem_proposito_Internalname = "CONTAGEM_PROPOSITO_"+sGXsfl_86_idx;
         Contagem_escopo_Internalname = "CONTAGEM_ESCOPO_"+sGXsfl_86_idx;
         Contagem_fronteira_Internalname = "CONTAGEM_FRONTEIRA_"+sGXsfl_86_idx;
         Contagem_observacao_Internalname = "CONTAGEM_OBSERVACAO_"+sGXsfl_86_idx;
         edtContagem_Notas_Internalname = "CONTAGEM_NOTAS_"+sGXsfl_86_idx;
         edtContagem_PFB_Internalname = "CONTAGEM_PFB_"+sGXsfl_86_idx;
         edtContagem_PFL_Internalname = "CONTAGEM_PFL_"+sGXsfl_86_idx;
         edtContagem_Divergencia_Internalname = "CONTAGEM_DIVERGENCIA_"+sGXsfl_86_idx;
         edtContagem_UsuarioContadorCod_Internalname = "CONTAGEM_USUARIOCONTADORCOD_"+sGXsfl_86_idx;
         edtContagem_UsuarioContadorPessoaCod_Internalname = "CONTAGEM_USUARIOCONTADORPESSOACOD_"+sGXsfl_86_idx;
         edtContagem_UsuarioContadorPessoaNom_Internalname = "CONTAGEM_USUARIOCONTADORPESSOANOM_"+sGXsfl_86_idx;
         cmbContagem_Status_Internalname = "CONTAGEM_STATUS_"+sGXsfl_86_idx;
         edtContagem_Demanda_Internalname = "CONTAGEM_DEMANDA_"+sGXsfl_86_idx;
         edtContagem_Link_Internalname = "CONTAGEM_LINK_"+sGXsfl_86_idx;
         edtContagem_Fator_Internalname = "CONTAGEM_FATOR_"+sGXsfl_86_idx;
         edtContagem_Deflator_Internalname = "CONTAGEM_DEFLATOR_"+sGXsfl_86_idx;
         edtContagem_SistemaCod_Internalname = "CONTAGEM_SISTEMACOD_"+sGXsfl_86_idx;
         edtContagem_SistemaSigla_Internalname = "CONTAGEM_SISTEMASIGLA_"+sGXsfl_86_idx;
      }

      protected void SubsflControlProps_fel_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_fel_idx;
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO_"+sGXsfl_86_fel_idx;
         edtContagem_AreaTrabalhoCod_Internalname = "CONTAGEM_AREATRABALHOCOD_"+sGXsfl_86_fel_idx;
         edtContagem_ContratadaCod_Internalname = "CONTAGEM_CONTRATADACOD_"+sGXsfl_86_fel_idx;
         edtContagem_DataCriacao_Internalname = "CONTAGEM_DATACRIACAO_"+sGXsfl_86_fel_idx;
         edtContagem_AreaTrabalhoDes_Internalname = "CONTAGEM_AREATRABALHODES_"+sGXsfl_86_fel_idx;
         cmbContagem_Tecnica_Internalname = "CONTAGEM_TECNICA_"+sGXsfl_86_fel_idx;
         cmbContagem_Tipo_Internalname = "CONTAGEM_TIPO_"+sGXsfl_86_fel_idx;
         Contagem_proposito_Internalname = "CONTAGEM_PROPOSITO_"+sGXsfl_86_fel_idx;
         Contagem_escopo_Internalname = "CONTAGEM_ESCOPO_"+sGXsfl_86_fel_idx;
         Contagem_fronteira_Internalname = "CONTAGEM_FRONTEIRA_"+sGXsfl_86_fel_idx;
         Contagem_observacao_Internalname = "CONTAGEM_OBSERVACAO_"+sGXsfl_86_fel_idx;
         edtContagem_Notas_Internalname = "CONTAGEM_NOTAS_"+sGXsfl_86_fel_idx;
         edtContagem_PFB_Internalname = "CONTAGEM_PFB_"+sGXsfl_86_fel_idx;
         edtContagem_PFL_Internalname = "CONTAGEM_PFL_"+sGXsfl_86_fel_idx;
         edtContagem_Divergencia_Internalname = "CONTAGEM_DIVERGENCIA_"+sGXsfl_86_fel_idx;
         edtContagem_UsuarioContadorCod_Internalname = "CONTAGEM_USUARIOCONTADORCOD_"+sGXsfl_86_fel_idx;
         edtContagem_UsuarioContadorPessoaCod_Internalname = "CONTAGEM_USUARIOCONTADORPESSOACOD_"+sGXsfl_86_fel_idx;
         edtContagem_UsuarioContadorPessoaNom_Internalname = "CONTAGEM_USUARIOCONTADORPESSOANOM_"+sGXsfl_86_fel_idx;
         cmbContagem_Status_Internalname = "CONTAGEM_STATUS_"+sGXsfl_86_fel_idx;
         edtContagem_Demanda_Internalname = "CONTAGEM_DEMANDA_"+sGXsfl_86_fel_idx;
         edtContagem_Link_Internalname = "CONTAGEM_LINK_"+sGXsfl_86_fel_idx;
         edtContagem_Fator_Internalname = "CONTAGEM_FATOR_"+sGXsfl_86_fel_idx;
         edtContagem_Deflator_Internalname = "CONTAGEM_DEFLATOR_"+sGXsfl_86_fel_idx;
         edtContagem_SistemaCod_Internalname = "CONTAGEM_SISTEMACOD_"+sGXsfl_86_fel_idx;
         edtContagem_SistemaSigla_Internalname = "CONTAGEM_SISTEMASIGLA_"+sGXsfl_86_fel_idx;
      }

      protected void sendrow_862( )
      {
         SubsflControlProps_862( ) ;
         WBHZ0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_86_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_86_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_86_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 87,'',false,'',86)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV34Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV177Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Select)) ? AV177Select_GXI : context.PathToRelativeUrl( AV34Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_86_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV34Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_ContratadaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1118Contagem_ContratadaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1118Contagem_ContratadaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_ContratadaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_DataCriacao_Internalname,context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"),context.localUtil.Format( A197Contagem_DataCriacao, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_DataCriacao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_AreaTrabalhoDes_Internalname,(String)A194Contagem_AreaTrabalhoDes,StringUtil.RTrim( context.localUtil.Format( A194Contagem_AreaTrabalhoDes, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_AreaTrabalhoDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_86_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEM_TECNICA_" + sGXsfl_86_idx;
               cmbContagem_Tecnica.Name = GXCCtl;
               cmbContagem_Tecnica.WebTags = "";
               cmbContagem_Tecnica.addItem("", "Nenhuma", 0);
               cmbContagem_Tecnica.addItem("1", "Indicativa", 0);
               cmbContagem_Tecnica.addItem("2", "Estimada", 0);
               cmbContagem_Tecnica.addItem("3", "Detalhada", 0);
               if ( cmbContagem_Tecnica.ItemCount > 0 )
               {
                  A195Contagem_Tecnica = cmbContagem_Tecnica.getValidValue(A195Contagem_Tecnica);
                  n195Contagem_Tecnica = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagem_Tecnica,(String)cmbContagem_Tecnica_Internalname,StringUtil.RTrim( A195Contagem_Tecnica),(short)1,(String)cmbContagem_Tecnica_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagem_Tecnica.CurrentValue = StringUtil.RTrim( A195Contagem_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tecnica_Internalname, "Values", (String)(cmbContagem_Tecnica.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_86_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEM_TIPO_" + sGXsfl_86_idx;
               cmbContagem_Tipo.Name = GXCCtl;
               cmbContagem_Tipo.WebTags = "";
               cmbContagem_Tipo.addItem("", "(Nenhum)", 0);
               cmbContagem_Tipo.addItem("D", "Projeto de Desenvolvimento", 0);
               cmbContagem_Tipo.addItem("M", "Projeto de Melhor�a", 0);
               cmbContagem_Tipo.addItem("C", "Projeto de Contagem", 0);
               cmbContagem_Tipo.addItem("A", "Aplica��o", 0);
               if ( cmbContagem_Tipo.ItemCount > 0 )
               {
                  A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
                  n196Contagem_Tipo = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagem_Tipo,(String)cmbContagem_Tipo_Internalname,StringUtil.RTrim( A196Contagem_Tipo),(short)1,(String)cmbContagem_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagem_Tipo.CurrentValue = StringUtil.RTrim( A196Contagem_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tipo_Internalname, "Values", (String)(cmbContagem_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td>") ;
            }
            /* User Defined Control */
            GridRow.AddColumnProperties("usercontrol", -1, isAjaxCallMode( ), new Object[] {(String)"CONTAGEM_PROPOSITOContainer"+"_"+sGXsfl_86_idx,(short)1});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td>") ;
            }
            /* User Defined Control */
            GridRow.AddColumnProperties("usercontrol", -1, isAjaxCallMode( ), new Object[] {(String)"CONTAGEM_ESCOPOContainer"+"_"+sGXsfl_86_idx,(short)1});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td>") ;
            }
            /* User Defined Control */
            GridRow.AddColumnProperties("usercontrol", -1, isAjaxCallMode( ), new Object[] {(String)"CONTAGEM_FRONTEIRAContainer"+"_"+sGXsfl_86_idx,(short)1});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td>") ;
            }
            /* User Defined Control */
            GridRow.AddColumnProperties("usercontrol", -1, isAjaxCallMode( ), new Object[] {(String)"CONTAGEM_OBSERVACAOContainer"+"_"+sGXsfl_86_idx,(short)1});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_Notas_Internalname,(String)A1059Contagem_Notas,(String)A1059Contagem_Notas,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_Notas_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)1,(short)86,(short)1,(short)0,(short)0,(bool)true,(String)"Html",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_PFB_Internalname,StringUtil.LTrim( StringUtil.NToC( A943Contagem_PFB, 13, 5, ",", "")),context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_PFB_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)13,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_PFL_Internalname,StringUtil.LTrim( StringUtil.NToC( A944Contagem_PFL, 13, 5, ",", "")),context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_PFL_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)13,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_Divergencia_Internalname,StringUtil.LTrim( StringUtil.NToC( A1119Contagem_Divergencia, 6, 2, ",", "")),context.localUtil.Format( A1119Contagem_Divergencia, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_Divergencia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_UsuarioContadorCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_UsuarioContadorCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_UsuarioContadorPessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A214Contagem_UsuarioContadorPessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_UsuarioContadorPessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_UsuarioContadorPessoaNom_Internalname,StringUtil.RTrim( A215Contagem_UsuarioContadorPessoaNom),StringUtil.RTrim( context.localUtil.Format( A215Contagem_UsuarioContadorPessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_UsuarioContadorPessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_86_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEM_STATUS_" + sGXsfl_86_idx;
               cmbContagem_Status.Name = GXCCtl;
               cmbContagem_Status.WebTags = "";
               if ( cmbContagem_Status.ItemCount > 0 )
               {
                  A262Contagem_Status = cmbContagem_Status.getValidValue(A262Contagem_Status);
                  n262Contagem_Status = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagem_Status,(String)cmbContagem_Status_Internalname,StringUtil.RTrim( A262Contagem_Status),(short)1,(String)cmbContagem_Status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagem_Status.CurrentValue = StringUtil.RTrim( A262Contagem_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Status_Internalname, "Values", (String)(cmbContagem_Status.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_Demanda_Internalname,(String)A945Contagem_Demanda,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_Demanda_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_Link_Internalname,(String)A946Contagem_Link,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_Link_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)255,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_Fator_Internalname,StringUtil.LTrim( StringUtil.NToC( A947Contagem_Fator, 4, 2, ",", "")),context.localUtil.Format( A947Contagem_Fator, "9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_Fator_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_Deflator_Internalname,StringUtil.LTrim( StringUtil.NToC( A1117Contagem_Deflator, 6, 2, ",", "")),context.localUtil.Format( A1117Contagem_Deflator, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_Deflator_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A940Contagem_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A940Contagem_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_SistemaSigla_Internalname,StringUtil.RTrim( A941Contagem_SistemaSigla),StringUtil.RTrim( context.localUtil.Format( A941Contagem_SistemaSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_SistemaSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GXCCtl = "CONTAGEM_PROPOSITO_" + sGXsfl_86_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, A199Contagem_Proposito);
            GXCCtl = "CONTAGEM_ESCOPO_" + sGXsfl_86_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, A200Contagem_Escopo);
            GXCCtl = "CONTAGEM_FRONTEIRA_" + sGXsfl_86_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, A201Contagem_Fronteira);
            GXCCtl = "CONTAGEM_OBSERVACAO_" + sGXsfl_86_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, A202Contagem_Observacao);
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_CODIGO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_AREATRABALHOCOD"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_CONTRATADACOD"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A1118Contagem_ContratadaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_DATACRIACAO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, A197Contagem_DataCriacao));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_TECNICA"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, StringUtil.RTrim( context.localUtil.Format( A195Contagem_Tecnica, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_TIPO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, StringUtil.RTrim( context.localUtil.Format( A196Contagem_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_NOTAS"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, A1059Contagem_Notas));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_PFB"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_PFL"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_DIVERGENCIA"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A1119Contagem_Divergencia, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_USUARIOCONTADORCOD"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_STATUS"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, StringUtil.RTrim( context.localUtil.Format( A262Contagem_Status, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_DEMANDA"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, StringUtil.RTrim( context.localUtil.Format( A945Contagem_Demanda, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_LINK"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, StringUtil.RTrim( context.localUtil.Format( A946Contagem_Link, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_FATOR"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A947Contagem_Fator, "9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_DEFLATOR"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A1117Contagem_Deflator, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_SISTEMACOD"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A940Contagem_SistemaCod), "ZZZZZ9")));
            GXCCtl = "CONTAGEM_PROPOSITO_Enabled_" + sGXsfl_86_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.BoolToStr( Contagem_proposito_Enabled));
            GXCCtl = "CONTAGEM_ESCOPO_Enabled_" + sGXsfl_86_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.BoolToStr( Contagem_escopo_Enabled));
            GXCCtl = "CONTAGEM_FRONTEIRA_Enabled_" + sGXsfl_86_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.BoolToStr( Contagem_fronteira_Enabled));
            GXCCtl = "CONTAGEM_OBSERVACAO_Enabled_" + sGXsfl_86_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.BoolToStr( Contagem_observacao_Enabled));
            GridContainer.AddRow(GridRow);
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         /* End function sendrow_862 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContagem_contratadacod1_Internalname = "vCONTAGEM_CONTRATADACOD1";
         edtavContagem_areatrabalhodes1_Internalname = "vCONTAGEM_AREATRABALHODES1";
         edtavContagem_usuariocontadorpessoanom1_Internalname = "vCONTAGEM_USUARIOCONTADORPESSOANOM1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContagem_contratadacod2_Internalname = "vCONTAGEM_CONTRATADACOD2";
         edtavContagem_areatrabalhodes2_Internalname = "vCONTAGEM_AREATRABALHODES2";
         edtavContagem_usuariocontadorpessoanom2_Internalname = "vCONTAGEM_USUARIOCONTADORPESSOANOM2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContagem_contratadacod3_Internalname = "vCONTAGEM_CONTRATADACOD3";
         edtavContagem_areatrabalhodes3_Internalname = "vCONTAGEM_AREATRABALHODES3";
         edtavContagem_usuariocontadorpessoanom3_Internalname = "vCONTAGEM_USUARIOCONTADORPESSOANOM3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO";
         edtContagem_AreaTrabalhoCod_Internalname = "CONTAGEM_AREATRABALHOCOD";
         edtContagem_ContratadaCod_Internalname = "CONTAGEM_CONTRATADACOD";
         edtContagem_DataCriacao_Internalname = "CONTAGEM_DATACRIACAO";
         edtContagem_AreaTrabalhoDes_Internalname = "CONTAGEM_AREATRABALHODES";
         cmbContagem_Tecnica_Internalname = "CONTAGEM_TECNICA";
         cmbContagem_Tipo_Internalname = "CONTAGEM_TIPO";
         Contagem_proposito_Internalname = "CONTAGEM_PROPOSITO";
         Contagem_escopo_Internalname = "CONTAGEM_ESCOPO";
         Contagem_fronteira_Internalname = "CONTAGEM_FRONTEIRA";
         Contagem_observacao_Internalname = "CONTAGEM_OBSERVACAO";
         edtContagem_Notas_Internalname = "CONTAGEM_NOTAS";
         edtContagem_PFB_Internalname = "CONTAGEM_PFB";
         edtContagem_PFL_Internalname = "CONTAGEM_PFL";
         edtContagem_Divergencia_Internalname = "CONTAGEM_DIVERGENCIA";
         edtContagem_UsuarioContadorCod_Internalname = "CONTAGEM_USUARIOCONTADORCOD";
         edtContagem_UsuarioContadorPessoaCod_Internalname = "CONTAGEM_USUARIOCONTADORPESSOACOD";
         edtContagem_UsuarioContadorPessoaNom_Internalname = "CONTAGEM_USUARIOCONTADORPESSOANOM";
         cmbContagem_Status_Internalname = "CONTAGEM_STATUS";
         edtContagem_Demanda_Internalname = "CONTAGEM_DEMANDA";
         edtContagem_Link_Internalname = "CONTAGEM_LINK";
         edtContagem_Fator_Internalname = "CONTAGEM_FATOR";
         edtContagem_Deflator_Internalname = "CONTAGEM_DEFLATOR";
         edtContagem_SistemaCod_Internalname = "CONTAGEM_SISTEMACOD";
         edtContagem_SistemaSigla_Internalname = "CONTAGEM_SISTEMASIGLA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagem_SistemaSigla_Jsonclick = "";
         edtContagem_SistemaCod_Jsonclick = "";
         edtContagem_Deflator_Jsonclick = "";
         edtContagem_Fator_Jsonclick = "";
         edtContagem_Link_Jsonclick = "";
         edtContagem_Demanda_Jsonclick = "";
         cmbContagem_Status_Jsonclick = "";
         edtContagem_UsuarioContadorPessoaNom_Jsonclick = "";
         edtContagem_UsuarioContadorPessoaCod_Jsonclick = "";
         edtContagem_UsuarioContadorCod_Jsonclick = "";
         edtContagem_Divergencia_Jsonclick = "";
         edtContagem_PFL_Jsonclick = "";
         edtContagem_PFB_Jsonclick = "";
         edtContagem_Notas_Jsonclick = "";
         Contagem_observacao_Enabled = Convert.ToBoolean( 0);
         Contagem_fronteira_Enabled = Convert.ToBoolean( 0);
         Contagem_escopo_Enabled = Convert.ToBoolean( 0);
         Contagem_proposito_Enabled = Convert.ToBoolean( 0);
         cmbContagem_Tipo_Jsonclick = "";
         cmbContagem_Tecnica_Jsonclick = "";
         edtContagem_AreaTrabalhoDes_Jsonclick = "";
         edtContagem_DataCriacao_Jsonclick = "";
         edtContagem_ContratadaCod_Jsonclick = "";
         edtContagem_AreaTrabalhoCod_Jsonclick = "";
         edtContagem_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContagem_usuariocontadorpessoanom1_Jsonclick = "";
         edtavContagem_areatrabalhodes1_Jsonclick = "";
         edtavContagem_contratadacod1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContagem_usuariocontadorpessoanom2_Jsonclick = "";
         edtavContagem_areatrabalhodes2_Jsonclick = "";
         edtavContagem_contratadacod2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContagem_usuariocontadorpessoanom3_Jsonclick = "";
         edtavContagem_areatrabalhodes3_Jsonclick = "";
         edtavContagem_contratadacod3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContagem_SistemaSigla_Titleformat = 0;
         edtContagem_SistemaCod_Titleformat = 0;
         edtContagem_Deflator_Titleformat = 0;
         edtContagem_Fator_Titleformat = 0;
         edtContagem_Link_Titleformat = 0;
         edtContagem_Demanda_Titleformat = 0;
         cmbContagem_Status_Titleformat = 0;
         edtContagem_UsuarioContadorPessoaNom_Titleformat = 0;
         edtContagem_UsuarioContadorPessoaCod_Titleformat = 0;
         edtContagem_UsuarioContadorCod_Titleformat = 0;
         edtContagem_Divergencia_Titleformat = 0;
         edtContagem_PFL_Titleformat = 0;
         edtContagem_PFB_Titleformat = 0;
         edtContagem_Notas_Titleformat = 0;
         cmbContagem_Tipo_Titleformat = 0;
         cmbContagem_Tecnica_Titleformat = 0;
         edtContagem_AreaTrabalhoDes_Titleformat = 0;
         edtContagem_DataCriacao_Titleformat = 0;
         edtContagem_ContratadaCod_Titleformat = 0;
         edtContagem_AreaTrabalhoCod_Titleformat = 0;
         edtContagem_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContagem_usuariocontadorpessoanom3_Visible = 1;
         edtavContagem_areatrabalhodes3_Visible = 1;
         edtavContagem_contratadacod3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContagem_usuariocontadorpessoanom2_Visible = 1;
         edtavContagem_areatrabalhodes2_Visible = 1;
         edtavContagem_contratadacod2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContagem_usuariocontadorpessoanom1_Visible = 1;
         edtavContagem_areatrabalhodes1_Visible = 1;
         edtavContagem_contratadacod1_Visible = 1;
         edtContagem_SistemaSigla_Title = "Sistema";
         edtContagem_SistemaCod_Title = "Sistema";
         edtContagem_Deflator_Title = "Deflator";
         edtContagem_Fator_Title = "Fator";
         edtContagem_Link_Title = "Link";
         edtContagem_Demanda_Title = "Demanda";
         cmbContagem_Status.Title.Text = "Status";
         edtContagem_UsuarioContadorPessoaNom_Title = "do Contador";
         edtContagem_UsuarioContadorPessoaCod_Title = "Pessoa Contador";
         edtContagem_UsuarioContadorCod_Title = "Usu�rio Contador";
         edtContagem_Divergencia_Title = "Contagem_Divergencia";
         edtContagem_PFL_Title = "PFL";
         edtContagem_PFB_Title = "PFB";
         edtContagem_Notas_Title = "Notas";
         cmbContagem_Tipo.Title.Text = "Tipo de Contagem";
         cmbContagem_Tecnica.Title.Text = "T�cnica de Contagem";
         edtContagem_AreaTrabalhoDes_Title = "�rea de Trabalho";
         edtContagem_DataCriacao_Title = "Data da contagem";
         edtContagem_ContratadaCod_Title = "Contratada";
         edtContagem_AreaTrabalhoCod_Title = "Cod. �rea de Trabalho";
         edtContagem_Codigo_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Prompt Contagem";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtContagem_Codigo_Titleformat',ctrl:'CONTAGEM_CODIGO',prop:'Titleformat'},{av:'edtContagem_Codigo_Title',ctrl:'CONTAGEM_CODIGO',prop:'Title'},{av:'edtContagem_AreaTrabalhoCod_Titleformat',ctrl:'CONTAGEM_AREATRABALHOCOD',prop:'Titleformat'},{av:'edtContagem_AreaTrabalhoCod_Title',ctrl:'CONTAGEM_AREATRABALHOCOD',prop:'Title'},{av:'edtContagem_ContratadaCod_Titleformat',ctrl:'CONTAGEM_CONTRATADACOD',prop:'Titleformat'},{av:'edtContagem_ContratadaCod_Title',ctrl:'CONTAGEM_CONTRATADACOD',prop:'Title'},{av:'edtContagem_DataCriacao_Titleformat',ctrl:'CONTAGEM_DATACRIACAO',prop:'Titleformat'},{av:'edtContagem_DataCriacao_Title',ctrl:'CONTAGEM_DATACRIACAO',prop:'Title'},{av:'edtContagem_AreaTrabalhoDes_Titleformat',ctrl:'CONTAGEM_AREATRABALHODES',prop:'Titleformat'},{av:'edtContagem_AreaTrabalhoDes_Title',ctrl:'CONTAGEM_AREATRABALHODES',prop:'Title'},{av:'cmbContagem_Tecnica'},{av:'cmbContagem_Tipo'},{av:'edtContagem_Notas_Titleformat',ctrl:'CONTAGEM_NOTAS',prop:'Titleformat'},{av:'edtContagem_Notas_Title',ctrl:'CONTAGEM_NOTAS',prop:'Title'},{av:'edtContagem_PFB_Titleformat',ctrl:'CONTAGEM_PFB',prop:'Titleformat'},{av:'edtContagem_PFB_Title',ctrl:'CONTAGEM_PFB',prop:'Title'},{av:'edtContagem_PFL_Titleformat',ctrl:'CONTAGEM_PFL',prop:'Titleformat'},{av:'edtContagem_PFL_Title',ctrl:'CONTAGEM_PFL',prop:'Title'},{av:'edtContagem_Divergencia_Titleformat',ctrl:'CONTAGEM_DIVERGENCIA',prop:'Titleformat'},{av:'edtContagem_Divergencia_Title',ctrl:'CONTAGEM_DIVERGENCIA',prop:'Title'},{av:'edtContagem_UsuarioContadorCod_Titleformat',ctrl:'CONTAGEM_USUARIOCONTADORCOD',prop:'Titleformat'},{av:'edtContagem_UsuarioContadorCod_Title',ctrl:'CONTAGEM_USUARIOCONTADORCOD',prop:'Title'},{av:'edtContagem_UsuarioContadorPessoaCod_Titleformat',ctrl:'CONTAGEM_USUARIOCONTADORPESSOACOD',prop:'Titleformat'},{av:'edtContagem_UsuarioContadorPessoaCod_Title',ctrl:'CONTAGEM_USUARIOCONTADORPESSOACOD',prop:'Title'},{av:'edtContagem_UsuarioContadorPessoaNom_Titleformat',ctrl:'CONTAGEM_USUARIOCONTADORPESSOANOM',prop:'Titleformat'},{av:'edtContagem_UsuarioContadorPessoaNom_Title',ctrl:'CONTAGEM_USUARIOCONTADORPESSOANOM',prop:'Title'},{av:'cmbContagem_Status'},{av:'edtContagem_Demanda_Titleformat',ctrl:'CONTAGEM_DEMANDA',prop:'Titleformat'},{av:'edtContagem_Demanda_Title',ctrl:'CONTAGEM_DEMANDA',prop:'Title'},{av:'edtContagem_Link_Titleformat',ctrl:'CONTAGEM_LINK',prop:'Titleformat'},{av:'edtContagem_Link_Title',ctrl:'CONTAGEM_LINK',prop:'Title'},{av:'edtContagem_Fator_Titleformat',ctrl:'CONTAGEM_FATOR',prop:'Titleformat'},{av:'edtContagem_Fator_Title',ctrl:'CONTAGEM_FATOR',prop:'Title'},{av:'edtContagem_Deflator_Titleformat',ctrl:'CONTAGEM_DEFLATOR',prop:'Titleformat'},{av:'edtContagem_Deflator_Title',ctrl:'CONTAGEM_DEFLATOR',prop:'Title'},{av:'edtContagem_SistemaCod_Titleformat',ctrl:'CONTAGEM_SISTEMACOD',prop:'Titleformat'},{av:'edtContagem_SistemaCod_Title',ctrl:'CONTAGEM_SISTEMACOD',prop:'Title'},{av:'edtContagem_SistemaSigla_Titleformat',ctrl:'CONTAGEM_SISTEMASIGLA',prop:'Titleformat'},{av:'edtContagem_SistemaSigla_Title',ctrl:'CONTAGEM_SISTEMASIGLA',prop:'Title'},{av:'AV173GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV174GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11HZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E24HZ2',iparms:[],oparms:[{av:'AV34Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E25HZ2',iparms:[{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1118Contagem_ContratadaCod',fld:'CONTAGEM_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV7InOutContagem_Codigo',fld:'vINOUTCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContagem_ContratadaCod',fld:'vINOUTCONTAGEM_CONTRATADACOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E12HZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17HZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13HZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'edtavContagem_contratadacod2_Visible',ctrl:'vCONTAGEM_CONTRATADACOD2',prop:'Visible'},{av:'edtavContagem_areatrabalhodes2_Visible',ctrl:'vCONTAGEM_AREATRABALHODES2',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom2_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagem_contratadacod3_Visible',ctrl:'vCONTAGEM_CONTRATADACOD3',prop:'Visible'},{av:'edtavContagem_areatrabalhodes3_Visible',ctrl:'vCONTAGEM_AREATRABALHODES3',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom3_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagem_contratadacod1_Visible',ctrl:'vCONTAGEM_CONTRATADACOD1',prop:'Visible'},{av:'edtavContagem_areatrabalhodes1_Visible',ctrl:'vCONTAGEM_AREATRABALHODES1',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom1_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18HZ2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavContagem_contratadacod1_Visible',ctrl:'vCONTAGEM_CONTRATADACOD1',prop:'Visible'},{av:'edtavContagem_areatrabalhodes1_Visible',ctrl:'vCONTAGEM_AREATRABALHODES1',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom1_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19HZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14HZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'edtavContagem_contratadacod2_Visible',ctrl:'vCONTAGEM_CONTRATADACOD2',prop:'Visible'},{av:'edtavContagem_areatrabalhodes2_Visible',ctrl:'vCONTAGEM_AREATRABALHODES2',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom2_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagem_contratadacod3_Visible',ctrl:'vCONTAGEM_CONTRATADACOD3',prop:'Visible'},{av:'edtavContagem_areatrabalhodes3_Visible',ctrl:'vCONTAGEM_AREATRABALHODES3',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom3_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagem_contratadacod1_Visible',ctrl:'vCONTAGEM_CONTRATADACOD1',prop:'Visible'},{av:'edtavContagem_areatrabalhodes1_Visible',ctrl:'vCONTAGEM_AREATRABALHODES1',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom1_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20HZ2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavContagem_contratadacod2_Visible',ctrl:'vCONTAGEM_CONTRATADACOD2',prop:'Visible'},{av:'edtavContagem_areatrabalhodes2_Visible',ctrl:'vCONTAGEM_AREATRABALHODES2',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom2_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15HZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'edtavContagem_contratadacod2_Visible',ctrl:'vCONTAGEM_CONTRATADACOD2',prop:'Visible'},{av:'edtavContagem_areatrabalhodes2_Visible',ctrl:'vCONTAGEM_AREATRABALHODES2',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom2_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagem_contratadacod3_Visible',ctrl:'vCONTAGEM_CONTRATADACOD3',prop:'Visible'},{av:'edtavContagem_areatrabalhodes3_Visible',ctrl:'vCONTAGEM_AREATRABALHODES3',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom3_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagem_contratadacod1_Visible',ctrl:'vCONTAGEM_CONTRATADACOD1',prop:'Visible'},{av:'edtavContagem_areatrabalhodes1_Visible',ctrl:'vCONTAGEM_AREATRABALHODES1',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom1_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21HZ2',iparms:[{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavContagem_contratadacod3_Visible',ctrl:'vCONTAGEM_CONTRATADACOD3',prop:'Visible'},{av:'edtavContagem_areatrabalhodes3_Visible',ctrl:'vCONTAGEM_AREATRABALHODES3',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom3_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E16HZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contagem_ContratadaCod1',fld:'vCONTAGEM_CONTRATADACOD1',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContagem_contratadacod1_Visible',ctrl:'vCONTAGEM_CONTRATADACOD1',prop:'Visible'},{av:'edtavContagem_areatrabalhodes1_Visible',ctrl:'vCONTAGEM_AREATRABALHODES1',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom1_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contagem_ContratadaCod2',fld:'vCONTAGEM_CONTRATADACOD2',pic:'ZZZZZ9',nv:0},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contagem_ContratadaCod3',fld:'vCONTAGEM_CONTRATADACOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18Contagem_AreaTrabalhoDes1',fld:'vCONTAGEM_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contagem_UsuarioContadorPessoaNom1',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Contagem_AreaTrabalhoDes2',fld:'vCONTAGEM_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25Contagem_UsuarioContadorPessoaNom2',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',pic:'@!',nv:''},{av:'AV30Contagem_AreaTrabalhoDes3',fld:'vCONTAGEM_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV31Contagem_UsuarioContadorPessoaNom3',fld:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',pic:'@!',nv:''},{av:'edtavContagem_contratadacod2_Visible',ctrl:'vCONTAGEM_CONTRATADACOD2',prop:'Visible'},{av:'edtavContagem_areatrabalhodes2_Visible',ctrl:'vCONTAGEM_AREATRABALHODES2',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom2_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagem_contratadacod3_Visible',ctrl:'vCONTAGEM_CONTRATADACOD3',prop:'Visible'},{av:'edtavContagem_areatrabalhodes3_Visible',ctrl:'vCONTAGEM_AREATRABALHODES3',prop:'Visible'},{av:'edtavContagem_usuariocontadorpessoanom3_Visible',ctrl:'vCONTAGEM_USUARIOCONTADORPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV18Contagem_AreaTrabalhoDes1 = "";
         AV19Contagem_UsuarioContadorPessoaNom1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV24Contagem_AreaTrabalhoDes2 = "";
         AV25Contagem_UsuarioContadorPessoaNom2 = "";
         AV27DynamicFiltersSelector3 = "";
         AV30Contagem_AreaTrabalhoDes3 = "";
         AV31Contagem_UsuarioContadorPessoaNom3 = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         AV34Select = "";
         AV177Select_GXI = "";
         A197Contagem_DataCriacao = DateTime.MinValue;
         A194Contagem_AreaTrabalhoDes = "";
         A195Contagem_Tecnica = "";
         A196Contagem_Tipo = "";
         A1059Contagem_Notas = "";
         A215Contagem_UsuarioContadorPessoaNom = "";
         A262Contagem_Status = "";
         A945Contagem_Demanda = "";
         A946Contagem_Link = "";
         A941Contagem_SistemaSigla = "";
         A199Contagem_Proposito = "";
         A200Contagem_Escopo = "";
         A201Contagem_Fronteira = "";
         A202Contagem_Observacao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18Contagem_AreaTrabalhoDes1 = "";
         lV19Contagem_UsuarioContadorPessoaNom1 = "";
         lV24Contagem_AreaTrabalhoDes2 = "";
         lV25Contagem_UsuarioContadorPessoaNom2 = "";
         lV30Contagem_AreaTrabalhoDes3 = "";
         lV31Contagem_UsuarioContadorPessoaNom3 = "";
         H00HZ2_A199Contagem_Proposito = new String[] {""} ;
         H00HZ2_n199Contagem_Proposito = new bool[] {false} ;
         H00HZ2_A200Contagem_Escopo = new String[] {""} ;
         H00HZ2_n200Contagem_Escopo = new bool[] {false} ;
         H00HZ2_A201Contagem_Fronteira = new String[] {""} ;
         H00HZ2_n201Contagem_Fronteira = new bool[] {false} ;
         H00HZ2_A202Contagem_Observacao = new String[] {""} ;
         H00HZ2_n202Contagem_Observacao = new bool[] {false} ;
         H00HZ2_A941Contagem_SistemaSigla = new String[] {""} ;
         H00HZ2_n941Contagem_SistemaSigla = new bool[] {false} ;
         H00HZ2_A940Contagem_SistemaCod = new int[1] ;
         H00HZ2_n940Contagem_SistemaCod = new bool[] {false} ;
         H00HZ2_A1117Contagem_Deflator = new decimal[1] ;
         H00HZ2_n1117Contagem_Deflator = new bool[] {false} ;
         H00HZ2_A947Contagem_Fator = new decimal[1] ;
         H00HZ2_n947Contagem_Fator = new bool[] {false} ;
         H00HZ2_A946Contagem_Link = new String[] {""} ;
         H00HZ2_n946Contagem_Link = new bool[] {false} ;
         H00HZ2_A945Contagem_Demanda = new String[] {""} ;
         H00HZ2_n945Contagem_Demanda = new bool[] {false} ;
         H00HZ2_A262Contagem_Status = new String[] {""} ;
         H00HZ2_n262Contagem_Status = new bool[] {false} ;
         H00HZ2_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         H00HZ2_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         H00HZ2_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         H00HZ2_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         H00HZ2_A213Contagem_UsuarioContadorCod = new int[1] ;
         H00HZ2_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         H00HZ2_A1119Contagem_Divergencia = new decimal[1] ;
         H00HZ2_n1119Contagem_Divergencia = new bool[] {false} ;
         H00HZ2_A944Contagem_PFL = new decimal[1] ;
         H00HZ2_n944Contagem_PFL = new bool[] {false} ;
         H00HZ2_A943Contagem_PFB = new decimal[1] ;
         H00HZ2_n943Contagem_PFB = new bool[] {false} ;
         H00HZ2_A1059Contagem_Notas = new String[] {""} ;
         H00HZ2_n1059Contagem_Notas = new bool[] {false} ;
         H00HZ2_A196Contagem_Tipo = new String[] {""} ;
         H00HZ2_n196Contagem_Tipo = new bool[] {false} ;
         H00HZ2_A195Contagem_Tecnica = new String[] {""} ;
         H00HZ2_n195Contagem_Tecnica = new bool[] {false} ;
         H00HZ2_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         H00HZ2_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         H00HZ2_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         H00HZ2_A1118Contagem_ContratadaCod = new int[1] ;
         H00HZ2_n1118Contagem_ContratadaCod = new bool[] {false} ;
         H00HZ2_A193Contagem_AreaTrabalhoCod = new int[1] ;
         H00HZ2_A192Contagem_Codigo = new int[1] ;
         H00HZ3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontagem__default(),
            new Object[][] {
                new Object[] {
               H00HZ2_A199Contagem_Proposito, H00HZ2_n199Contagem_Proposito, H00HZ2_A200Contagem_Escopo, H00HZ2_n200Contagem_Escopo, H00HZ2_A201Contagem_Fronteira, H00HZ2_n201Contagem_Fronteira, H00HZ2_A202Contagem_Observacao, H00HZ2_n202Contagem_Observacao, H00HZ2_A941Contagem_SistemaSigla, H00HZ2_n941Contagem_SistemaSigla,
               H00HZ2_A940Contagem_SistemaCod, H00HZ2_n940Contagem_SistemaCod, H00HZ2_A1117Contagem_Deflator, H00HZ2_n1117Contagem_Deflator, H00HZ2_A947Contagem_Fator, H00HZ2_n947Contagem_Fator, H00HZ2_A946Contagem_Link, H00HZ2_n946Contagem_Link, H00HZ2_A945Contagem_Demanda, H00HZ2_n945Contagem_Demanda,
               H00HZ2_A262Contagem_Status, H00HZ2_n262Contagem_Status, H00HZ2_A215Contagem_UsuarioContadorPessoaNom, H00HZ2_n215Contagem_UsuarioContadorPessoaNom, H00HZ2_A214Contagem_UsuarioContadorPessoaCod, H00HZ2_n214Contagem_UsuarioContadorPessoaCod, H00HZ2_A213Contagem_UsuarioContadorCod, H00HZ2_n213Contagem_UsuarioContadorCod, H00HZ2_A1119Contagem_Divergencia, H00HZ2_n1119Contagem_Divergencia,
               H00HZ2_A944Contagem_PFL, H00HZ2_n944Contagem_PFL, H00HZ2_A943Contagem_PFB, H00HZ2_n943Contagem_PFB, H00HZ2_A1059Contagem_Notas, H00HZ2_n1059Contagem_Notas, H00HZ2_A196Contagem_Tipo, H00HZ2_n196Contagem_Tipo, H00HZ2_A195Contagem_Tecnica, H00HZ2_n195Contagem_Tecnica,
               H00HZ2_A194Contagem_AreaTrabalhoDes, H00HZ2_n194Contagem_AreaTrabalhoDes, H00HZ2_A197Contagem_DataCriacao, H00HZ2_A1118Contagem_ContratadaCod, H00HZ2_n1118Contagem_ContratadaCod, H00HZ2_A193Contagem_AreaTrabalhoCod, H00HZ2_A192Contagem_Codigo
               }
               , new Object[] {
               H00HZ3_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_86 ;
      private short nGXsfl_86_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV28DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_86_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagem_Codigo_Titleformat ;
      private short edtContagem_AreaTrabalhoCod_Titleformat ;
      private short edtContagem_ContratadaCod_Titleformat ;
      private short edtContagem_DataCriacao_Titleformat ;
      private short edtContagem_AreaTrabalhoDes_Titleformat ;
      private short cmbContagem_Tecnica_Titleformat ;
      private short cmbContagem_Tipo_Titleformat ;
      private short edtContagem_Notas_Titleformat ;
      private short edtContagem_PFB_Titleformat ;
      private short edtContagem_PFL_Titleformat ;
      private short edtContagem_Divergencia_Titleformat ;
      private short edtContagem_UsuarioContadorCod_Titleformat ;
      private short edtContagem_UsuarioContadorPessoaCod_Titleformat ;
      private short edtContagem_UsuarioContadorPessoaNom_Titleformat ;
      private short cmbContagem_Status_Titleformat ;
      private short edtContagem_Demanda_Titleformat ;
      private short edtContagem_Link_Titleformat ;
      private short edtContagem_Fator_Titleformat ;
      private short edtContagem_Deflator_Titleformat ;
      private short edtContagem_SistemaCod_Titleformat ;
      private short edtContagem_SistemaSigla_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContagem_Codigo ;
      private int AV8InOutContagem_ContratadaCod ;
      private int wcpOAV7InOutContagem_Codigo ;
      private int wcpOAV8InOutContagem_ContratadaCod ;
      private int subGrid_Rows ;
      private int AV17Contagem_ContratadaCod1 ;
      private int AV23Contagem_ContratadaCod2 ;
      private int AV29Contagem_ContratadaCod3 ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A192Contagem_Codigo ;
      private int A193Contagem_AreaTrabalhoCod ;
      private int A1118Contagem_ContratadaCod ;
      private int A213Contagem_UsuarioContadorCod ;
      private int A214Contagem_UsuarioContadorPessoaCod ;
      private int A940Contagem_SistemaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV172PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContagem_contratadacod1_Visible ;
      private int edtavContagem_areatrabalhodes1_Visible ;
      private int edtavContagem_usuariocontadorpessoanom1_Visible ;
      private int edtavContagem_contratadacod2_Visible ;
      private int edtavContagem_areatrabalhodes2_Visible ;
      private int edtavContagem_usuariocontadorpessoanom2_Visible ;
      private int edtavContagem_contratadacod3_Visible ;
      private int edtavContagem_areatrabalhodes3_Visible ;
      private int edtavContagem_usuariocontadorpessoanom3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV173GridCurrentPage ;
      private long AV174GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A943Contagem_PFB ;
      private decimal A944Contagem_PFL ;
      private decimal A1119Contagem_Divergencia ;
      private decimal A947Contagem_Fator ;
      private decimal A1117Contagem_Deflator ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_86_idx="0001" ;
      private String AV19Contagem_UsuarioContadorPessoaNom1 ;
      private String AV25Contagem_UsuarioContadorPessoaNom2 ;
      private String AV31Contagem_UsuarioContadorPessoaNom3 ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String Contagem_proposito_Internalname ;
      private String Contagem_escopo_Internalname ;
      private String Contagem_fronteira_Internalname ;
      private String Contagem_observacao_Internalname ;
      private String edtavSelect_Internalname ;
      private String edtContagem_Codigo_Internalname ;
      private String edtContagem_AreaTrabalhoCod_Internalname ;
      private String edtContagem_ContratadaCod_Internalname ;
      private String edtContagem_DataCriacao_Internalname ;
      private String edtContagem_AreaTrabalhoDes_Internalname ;
      private String cmbContagem_Tecnica_Internalname ;
      private String A195Contagem_Tecnica ;
      private String cmbContagem_Tipo_Internalname ;
      private String A196Contagem_Tipo ;
      private String edtContagem_Notas_Internalname ;
      private String edtContagem_PFB_Internalname ;
      private String edtContagem_PFL_Internalname ;
      private String edtContagem_Divergencia_Internalname ;
      private String edtContagem_UsuarioContadorCod_Internalname ;
      private String edtContagem_UsuarioContadorPessoaCod_Internalname ;
      private String A215Contagem_UsuarioContadorPessoaNom ;
      private String edtContagem_UsuarioContadorPessoaNom_Internalname ;
      private String cmbContagem_Status_Internalname ;
      private String A262Contagem_Status ;
      private String edtContagem_Demanda_Internalname ;
      private String edtContagem_Link_Internalname ;
      private String edtContagem_Fator_Internalname ;
      private String edtContagem_Deflator_Internalname ;
      private String edtContagem_SistemaCod_Internalname ;
      private String A941Contagem_SistemaSigla ;
      private String edtContagem_SistemaSigla_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV19Contagem_UsuarioContadorPessoaNom1 ;
      private String lV25Contagem_UsuarioContadorPessoaNom2 ;
      private String lV31Contagem_UsuarioContadorPessoaNom3 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContagem_contratadacod1_Internalname ;
      private String edtavContagem_areatrabalhodes1_Internalname ;
      private String edtavContagem_usuariocontadorpessoanom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContagem_contratadacod2_Internalname ;
      private String edtavContagem_areatrabalhodes2_Internalname ;
      private String edtavContagem_usuariocontadorpessoanom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContagem_contratadacod3_Internalname ;
      private String edtavContagem_areatrabalhodes3_Internalname ;
      private String edtavContagem_usuariocontadorpessoanom3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContagem_Codigo_Title ;
      private String edtContagem_AreaTrabalhoCod_Title ;
      private String edtContagem_ContratadaCod_Title ;
      private String edtContagem_DataCriacao_Title ;
      private String edtContagem_AreaTrabalhoDes_Title ;
      private String edtContagem_Notas_Title ;
      private String edtContagem_PFB_Title ;
      private String edtContagem_PFL_Title ;
      private String edtContagem_Divergencia_Title ;
      private String edtContagem_UsuarioContadorCod_Title ;
      private String edtContagem_UsuarioContadorPessoaCod_Title ;
      private String edtContagem_UsuarioContadorPessoaNom_Title ;
      private String edtContagem_Demanda_Title ;
      private String edtContagem_Link_Title ;
      private String edtContagem_Fator_Title ;
      private String edtContagem_Deflator_Title ;
      private String edtContagem_SistemaCod_Title ;
      private String edtContagem_SistemaSigla_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContagem_contratadacod3_Jsonclick ;
      private String edtavContagem_areatrabalhodes3_Jsonclick ;
      private String edtavContagem_usuariocontadorpessoanom3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContagem_contratadacod2_Jsonclick ;
      private String edtavContagem_areatrabalhodes2_Jsonclick ;
      private String edtavContagem_usuariocontadorpessoanom2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContagem_contratadacod1_Jsonclick ;
      private String edtavContagem_areatrabalhodes1_Jsonclick ;
      private String edtavContagem_usuariocontadorpessoanom1_Jsonclick ;
      private String sGXsfl_86_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContagem_Codigo_Jsonclick ;
      private String edtContagem_AreaTrabalhoCod_Jsonclick ;
      private String edtContagem_ContratadaCod_Jsonclick ;
      private String edtContagem_DataCriacao_Jsonclick ;
      private String edtContagem_AreaTrabalhoDes_Jsonclick ;
      private String cmbContagem_Tecnica_Jsonclick ;
      private String cmbContagem_Tipo_Jsonclick ;
      private String edtContagem_Notas_Jsonclick ;
      private String edtContagem_PFB_Jsonclick ;
      private String edtContagem_PFL_Jsonclick ;
      private String edtContagem_Divergencia_Jsonclick ;
      private String edtContagem_UsuarioContadorCod_Jsonclick ;
      private String edtContagem_UsuarioContadorPessoaCod_Jsonclick ;
      private String edtContagem_UsuarioContadorPessoaNom_Jsonclick ;
      private String cmbContagem_Status_Jsonclick ;
      private String edtContagem_Demanda_Jsonclick ;
      private String edtContagem_Link_Jsonclick ;
      private String edtContagem_Fator_Jsonclick ;
      private String edtContagem_Deflator_Jsonclick ;
      private String edtContagem_SistemaCod_Jsonclick ;
      private String edtContagem_SistemaSigla_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime A197Contagem_DataCriacao ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV26DynamicFiltersEnabled3 ;
      private bool toggleJsOutput ;
      private bool AV33DynamicFiltersIgnoreFirst ;
      private bool AV32DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool Contagem_proposito_Enabled ;
      private bool Contagem_escopo_Enabled ;
      private bool Contagem_fronteira_Enabled ;
      private bool Contagem_observacao_Enabled ;
      private bool n1118Contagem_ContratadaCod ;
      private bool n194Contagem_AreaTrabalhoDes ;
      private bool n195Contagem_Tecnica ;
      private bool n196Contagem_Tipo ;
      private bool n1059Contagem_Notas ;
      private bool n943Contagem_PFB ;
      private bool n944Contagem_PFL ;
      private bool n1119Contagem_Divergencia ;
      private bool n213Contagem_UsuarioContadorCod ;
      private bool n214Contagem_UsuarioContadorPessoaCod ;
      private bool n215Contagem_UsuarioContadorPessoaNom ;
      private bool n262Contagem_Status ;
      private bool n945Contagem_Demanda ;
      private bool n946Contagem_Link ;
      private bool n947Contagem_Fator ;
      private bool n1117Contagem_Deflator ;
      private bool n940Contagem_SistemaCod ;
      private bool n941Contagem_SistemaSigla ;
      private bool n199Contagem_Proposito ;
      private bool n200Contagem_Escopo ;
      private bool n201Contagem_Fronteira ;
      private bool n202Contagem_Observacao ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV34Select_IsBlob ;
      private String A1059Contagem_Notas ;
      private String A199Contagem_Proposito ;
      private String A200Contagem_Escopo ;
      private String A201Contagem_Fronteira ;
      private String A202Contagem_Observacao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV18Contagem_AreaTrabalhoDes1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV24Contagem_AreaTrabalhoDes2 ;
      private String AV27DynamicFiltersSelector3 ;
      private String AV30Contagem_AreaTrabalhoDes3 ;
      private String AV177Select_GXI ;
      private String A194Contagem_AreaTrabalhoDes ;
      private String A945Contagem_Demanda ;
      private String A946Contagem_Link ;
      private String lV18Contagem_AreaTrabalhoDes1 ;
      private String lV24Contagem_AreaTrabalhoDes2 ;
      private String lV30Contagem_AreaTrabalhoDes3 ;
      private String AV34Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContagem_Codigo ;
      private int aP1_InOutContagem_ContratadaCod ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbContagem_Tecnica ;
      private GXCombobox cmbContagem_Tipo ;
      private GXCombobox cmbContagem_Status ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00HZ2_A199Contagem_Proposito ;
      private bool[] H00HZ2_n199Contagem_Proposito ;
      private String[] H00HZ2_A200Contagem_Escopo ;
      private bool[] H00HZ2_n200Contagem_Escopo ;
      private String[] H00HZ2_A201Contagem_Fronteira ;
      private bool[] H00HZ2_n201Contagem_Fronteira ;
      private String[] H00HZ2_A202Contagem_Observacao ;
      private bool[] H00HZ2_n202Contagem_Observacao ;
      private String[] H00HZ2_A941Contagem_SistemaSigla ;
      private bool[] H00HZ2_n941Contagem_SistemaSigla ;
      private int[] H00HZ2_A940Contagem_SistemaCod ;
      private bool[] H00HZ2_n940Contagem_SistemaCod ;
      private decimal[] H00HZ2_A1117Contagem_Deflator ;
      private bool[] H00HZ2_n1117Contagem_Deflator ;
      private decimal[] H00HZ2_A947Contagem_Fator ;
      private bool[] H00HZ2_n947Contagem_Fator ;
      private String[] H00HZ2_A946Contagem_Link ;
      private bool[] H00HZ2_n946Contagem_Link ;
      private String[] H00HZ2_A945Contagem_Demanda ;
      private bool[] H00HZ2_n945Contagem_Demanda ;
      private String[] H00HZ2_A262Contagem_Status ;
      private bool[] H00HZ2_n262Contagem_Status ;
      private String[] H00HZ2_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] H00HZ2_n215Contagem_UsuarioContadorPessoaNom ;
      private int[] H00HZ2_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] H00HZ2_n214Contagem_UsuarioContadorPessoaCod ;
      private int[] H00HZ2_A213Contagem_UsuarioContadorCod ;
      private bool[] H00HZ2_n213Contagem_UsuarioContadorCod ;
      private decimal[] H00HZ2_A1119Contagem_Divergencia ;
      private bool[] H00HZ2_n1119Contagem_Divergencia ;
      private decimal[] H00HZ2_A944Contagem_PFL ;
      private bool[] H00HZ2_n944Contagem_PFL ;
      private decimal[] H00HZ2_A943Contagem_PFB ;
      private bool[] H00HZ2_n943Contagem_PFB ;
      private String[] H00HZ2_A1059Contagem_Notas ;
      private bool[] H00HZ2_n1059Contagem_Notas ;
      private String[] H00HZ2_A196Contagem_Tipo ;
      private bool[] H00HZ2_n196Contagem_Tipo ;
      private String[] H00HZ2_A195Contagem_Tecnica ;
      private bool[] H00HZ2_n195Contagem_Tecnica ;
      private String[] H00HZ2_A194Contagem_AreaTrabalhoDes ;
      private bool[] H00HZ2_n194Contagem_AreaTrabalhoDes ;
      private DateTime[] H00HZ2_A197Contagem_DataCriacao ;
      private int[] H00HZ2_A1118Contagem_ContratadaCod ;
      private bool[] H00HZ2_n1118Contagem_ContratadaCod ;
      private int[] H00HZ2_A193Contagem_AreaTrabalhoCod ;
      private int[] H00HZ2_A192Contagem_Codigo ;
      private long[] H00HZ3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
   }

   public class promptcontagem__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00HZ2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             int AV17Contagem_ContratadaCod1 ,
                                             String AV18Contagem_AreaTrabalhoDes1 ,
                                             String AV19Contagem_UsuarioContadorPessoaNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             int AV23Contagem_ContratadaCod2 ,
                                             String AV24Contagem_AreaTrabalhoDes2 ,
                                             String AV25Contagem_UsuarioContadorPessoaNom2 ,
                                             bool AV26DynamicFiltersEnabled3 ,
                                             String AV27DynamicFiltersSelector3 ,
                                             short AV28DynamicFiltersOperator3 ,
                                             int AV29Contagem_ContratadaCod3 ,
                                             String AV30Contagem_AreaTrabalhoDes3 ,
                                             String AV31Contagem_UsuarioContadorPessoaNom3 ,
                                             int A1118Contagem_ContratadaCod ,
                                             String A194Contagem_AreaTrabalhoDes ,
                                             String A215Contagem_UsuarioContadorPessoaNom ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [26] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Contagem_Proposito], T1.[Contagem_Escopo], T1.[Contagem_Fronteira], T1.[Contagem_Observacao], T2.[Sistema_Sigla] AS Contagem_SistemaSigla, T1.[Contagem_SistemaCod] AS Contagem_SistemaCod, T1.[Contagem_Deflator], T1.[Contagem_Fator], T1.[Contagem_Link], T1.[Contagem_Demanda], T1.[Contagem_Status], T4.[Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom, T3.[Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod, T1.[Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, T1.[Contagem_Divergencia], T1.[Contagem_PFL], T1.[Contagem_PFB], T1.[Contagem_Notas], T1.[Contagem_Tipo], T1.[Contagem_Tecnica], T5.[AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes, T1.[Contagem_DataCriacao], T1.[Contagem_ContratadaCod], T1.[Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, T1.[Contagem_Codigo]";
         sFromString = " FROM (((([Contagem] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Contagem_SistemaCod]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) INNER JOIN [AreaTrabalho] T5 WITH (NOLOCK) ON T5.[AreaTrabalho_Codigo] = T1.[Contagem_AreaTrabalhoCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! (0==AV17Contagem_ContratadaCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV17Contagem_ContratadaCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV17Contagem_ContratadaCod1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 4 ) ) && ( ! (0==AV17Contagem_ContratadaCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV17Contagem_ContratadaCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV17Contagem_ContratadaCod1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 2 ) || ( AV16DynamicFiltersOperator1 == 5 ) ) && ( ! (0==AV17Contagem_ContratadaCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV17Contagem_ContratadaCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV17Contagem_ContratadaCod1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contagem_AreaTrabalhoDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV18Contagem_AreaTrabalhoDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV18Contagem_AreaTrabalhoDes1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contagem_AreaTrabalhoDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV18Contagem_AreaTrabalhoDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV18Contagem_AreaTrabalhoDes1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contagem_UsuarioContadorPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV19Contagem_UsuarioContadorPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV19Contagem_UsuarioContadorPessoaNom1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contagem_UsuarioContadorPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV19Contagem_UsuarioContadorPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV19Contagem_UsuarioContadorPessoaNom1)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! (0==AV23Contagem_ContratadaCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV23Contagem_ContratadaCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV23Contagem_ContratadaCod2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 4 ) ) && ( ! (0==AV23Contagem_ContratadaCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV23Contagem_ContratadaCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV23Contagem_ContratadaCod2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 2 ) || ( AV22DynamicFiltersOperator2 == 5 ) ) && ( ! (0==AV23Contagem_ContratadaCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV23Contagem_ContratadaCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV23Contagem_ContratadaCod2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contagem_AreaTrabalhoDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV24Contagem_AreaTrabalhoDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV24Contagem_AreaTrabalhoDes2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contagem_AreaTrabalhoDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV24Contagem_AreaTrabalhoDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV24Contagem_AreaTrabalhoDes2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Contagem_UsuarioContadorPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV25Contagem_UsuarioContadorPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV25Contagem_UsuarioContadorPessoaNom2)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Contagem_UsuarioContadorPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV25Contagem_UsuarioContadorPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV25Contagem_UsuarioContadorPessoaNom2)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 0 ) || ( AV28DynamicFiltersOperator3 == 3 ) ) && ( ! (0==AV29Contagem_ContratadaCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV29Contagem_ContratadaCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV29Contagem_ContratadaCod3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 1 ) || ( AV28DynamicFiltersOperator3 == 4 ) ) && ( ! (0==AV29Contagem_ContratadaCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV29Contagem_ContratadaCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV29Contagem_ContratadaCod3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 2 ) || ( AV28DynamicFiltersOperator3 == 5 ) ) && ( ! (0==AV29Contagem_ContratadaCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV29Contagem_ContratadaCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV29Contagem_ContratadaCod3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 0 ) || ( AV28DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Contagem_AreaTrabalhoDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV30Contagem_AreaTrabalhoDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV30Contagem_AreaTrabalhoDes3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 1 ) || ( AV28DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Contagem_AreaTrabalhoDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV30Contagem_AreaTrabalhoDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV30Contagem_AreaTrabalhoDes3)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 0 ) || ( AV28DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Contagem_UsuarioContadorPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV31Contagem_UsuarioContadorPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV31Contagem_UsuarioContadorPessoaNom3)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 1 ) || ( AV28DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Contagem_UsuarioContadorPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV31Contagem_UsuarioContadorPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV31Contagem_UsuarioContadorPessoaNom3)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_ContratadaCod]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_ContratadaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_AreaTrabalhoCod]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_AreaTrabalhoCod] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_DataCriacao]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_DataCriacao] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[AreaTrabalho_Descricao]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[AreaTrabalho_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Tecnica]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Tecnica] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Tipo]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Notas]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Notas] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_PFB]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_PFB] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_PFL]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_PFL] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Divergencia]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Divergencia] DESC";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_UsuarioContadorCod]";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_UsuarioContadorCod] DESC";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Usuario_PessoaCod]";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Usuario_PessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 14 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 14 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 15 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Status]";
         }
         else if ( ( AV13OrderedBy == 15 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Status] DESC";
         }
         else if ( ( AV13OrderedBy == 16 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Demanda]";
         }
         else if ( ( AV13OrderedBy == 16 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Demanda] DESC";
         }
         else if ( ( AV13OrderedBy == 17 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Link]";
         }
         else if ( ( AV13OrderedBy == 17 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Link] DESC";
         }
         else if ( ( AV13OrderedBy == 18 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Fator]";
         }
         else if ( ( AV13OrderedBy == 18 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Fator] DESC";
         }
         else if ( ( AV13OrderedBy == 19 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Deflator]";
         }
         else if ( ( AV13OrderedBy == 19 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Deflator] DESC";
         }
         else if ( ( AV13OrderedBy == 20 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_SistemaCod]";
         }
         else if ( ( AV13OrderedBy == 20 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_SistemaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 21 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Sistema_Sigla]";
         }
         else if ( ( AV13OrderedBy == 21 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Sistema_Sigla] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00HZ3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             int AV17Contagem_ContratadaCod1 ,
                                             String AV18Contagem_AreaTrabalhoDes1 ,
                                             String AV19Contagem_UsuarioContadorPessoaNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             int AV23Contagem_ContratadaCod2 ,
                                             String AV24Contagem_AreaTrabalhoDes2 ,
                                             String AV25Contagem_UsuarioContadorPessoaNom2 ,
                                             bool AV26DynamicFiltersEnabled3 ,
                                             String AV27DynamicFiltersSelector3 ,
                                             short AV28DynamicFiltersOperator3 ,
                                             int AV29Contagem_ContratadaCod3 ,
                                             String AV30Contagem_AreaTrabalhoDes3 ,
                                             String AV31Contagem_UsuarioContadorPessoaNom3 ,
                                             int A1118Contagem_ContratadaCod ,
                                             String A194Contagem_AreaTrabalhoDes ,
                                             String A215Contagem_UsuarioContadorPessoaNom ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [21] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((([Contagem] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T1.[Contagem_SistemaCod]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contagem_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! (0==AV17Contagem_ContratadaCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV17Contagem_ContratadaCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV17Contagem_ContratadaCod1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 4 ) ) && ( ! (0==AV17Contagem_ContratadaCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV17Contagem_ContratadaCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV17Contagem_ContratadaCod1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 2 ) || ( AV16DynamicFiltersOperator1 == 5 ) ) && ( ! (0==AV17Contagem_ContratadaCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV17Contagem_ContratadaCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV17Contagem_ContratadaCod1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contagem_AreaTrabalhoDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV18Contagem_AreaTrabalhoDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV18Contagem_AreaTrabalhoDes1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contagem_AreaTrabalhoDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like '%' + @lV18Contagem_AreaTrabalhoDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like '%' + @lV18Contagem_AreaTrabalhoDes1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contagem_UsuarioContadorPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV19Contagem_UsuarioContadorPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV19Contagem_UsuarioContadorPessoaNom1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contagem_UsuarioContadorPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV19Contagem_UsuarioContadorPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV19Contagem_UsuarioContadorPessoaNom1)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! (0==AV23Contagem_ContratadaCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV23Contagem_ContratadaCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV23Contagem_ContratadaCod2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 4 ) ) && ( ! (0==AV23Contagem_ContratadaCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV23Contagem_ContratadaCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV23Contagem_ContratadaCod2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 2 ) || ( AV22DynamicFiltersOperator2 == 5 ) ) && ( ! (0==AV23Contagem_ContratadaCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV23Contagem_ContratadaCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV23Contagem_ContratadaCod2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contagem_AreaTrabalhoDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV24Contagem_AreaTrabalhoDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV24Contagem_AreaTrabalhoDes2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contagem_AreaTrabalhoDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like '%' + @lV24Contagem_AreaTrabalhoDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like '%' + @lV24Contagem_AreaTrabalhoDes2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Contagem_UsuarioContadorPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV25Contagem_UsuarioContadorPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV25Contagem_UsuarioContadorPessoaNom2)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Contagem_UsuarioContadorPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV25Contagem_UsuarioContadorPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV25Contagem_UsuarioContadorPessoaNom2)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 0 ) || ( AV28DynamicFiltersOperator3 == 3 ) ) && ( ! (0==AV29Contagem_ContratadaCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV29Contagem_ContratadaCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV29Contagem_ContratadaCod3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 1 ) || ( AV28DynamicFiltersOperator3 == 4 ) ) && ( ! (0==AV29Contagem_ContratadaCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV29Contagem_ContratadaCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV29Contagem_ContratadaCod3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 2 ) || ( AV28DynamicFiltersOperator3 == 5 ) ) && ( ! (0==AV29Contagem_ContratadaCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV29Contagem_ContratadaCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV29Contagem_ContratadaCod3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 0 ) || ( AV28DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Contagem_AreaTrabalhoDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV30Contagem_AreaTrabalhoDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV30Contagem_AreaTrabalhoDes3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 1 ) || ( AV28DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Contagem_AreaTrabalhoDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like '%' + @lV30Contagem_AreaTrabalhoDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like '%' + @lV30Contagem_AreaTrabalhoDes3)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 0 ) || ( AV28DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Contagem_UsuarioContadorPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV31Contagem_UsuarioContadorPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV31Contagem_UsuarioContadorPessoaNom3)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV28DynamicFiltersOperator3 == 1 ) || ( AV28DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Contagem_UsuarioContadorPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV31Contagem_UsuarioContadorPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV31Contagem_UsuarioContadorPessoaNom3)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 14 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 14 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 15 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 15 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 16 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 16 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 17 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 17 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 18 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 18 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 19 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 19 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 20 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 20 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 21 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 21 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00HZ2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_H00HZ3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00HZ2 ;
          prmH00HZ2 = new Object[] {
          new Object[] {"@AV17Contagem_ContratadaCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17Contagem_ContratadaCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17Contagem_ContratadaCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Contagem_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV18Contagem_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV19Contagem_UsuarioContadorPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19Contagem_UsuarioContadorPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV23Contagem_ContratadaCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23Contagem_ContratadaCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23Contagem_ContratadaCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV24Contagem_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV24Contagem_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV25Contagem_UsuarioContadorPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV25Contagem_UsuarioContadorPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV29Contagem_ContratadaCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV29Contagem_ContratadaCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV29Contagem_ContratadaCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV30Contagem_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV30Contagem_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV31Contagem_UsuarioContadorPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV31Contagem_UsuarioContadorPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00HZ3 ;
          prmH00HZ3 = new Object[] {
          new Object[] {"@AV17Contagem_ContratadaCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17Contagem_ContratadaCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17Contagem_ContratadaCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Contagem_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV18Contagem_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV19Contagem_UsuarioContadorPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19Contagem_UsuarioContadorPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV23Contagem_ContratadaCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23Contagem_ContratadaCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23Contagem_ContratadaCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV24Contagem_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV24Contagem_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV25Contagem_UsuarioContadorPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV25Contagem_UsuarioContadorPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV29Contagem_ContratadaCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV29Contagem_ContratadaCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV29Contagem_ContratadaCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV30Contagem_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV30Contagem_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV31Contagem_UsuarioContadorPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV31Contagem_UsuarioContadorPessoaNom3",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00HZ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HZ2,11,0,true,false )
             ,new CursorDef("H00HZ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HZ3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 25) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((String[]) buf[22])[0] = rslt.getString(12, 100) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((int[]) buf[24])[0] = rslt.getInt(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((int[]) buf[26])[0] = rslt.getInt(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((decimal[]) buf[28])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((decimal[]) buf[30])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(16);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(17);
                ((String[]) buf[34])[0] = rslt.getLongVarchar(18) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(18);
                ((String[]) buf[36])[0] = rslt.getString(19, 1) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(19);
                ((String[]) buf[38])[0] = rslt.getString(20, 1) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(20);
                ((String[]) buf[40])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(21);
                ((DateTime[]) buf[42])[0] = rslt.getGXDate(22) ;
                ((int[]) buf[43])[0] = rslt.getInt(23) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(23);
                ((int[]) buf[45])[0] = rslt.getInt(24) ;
                ((int[]) buf[46])[0] = rslt.getInt(25) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
       }
    }

 }

}
