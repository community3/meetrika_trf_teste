/*
               File: PromptProjeto
        Description: Selecione Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/15/2020 0:4:57.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptprojeto : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptprojeto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptprojeto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutProjeto_Codigo ,
                           ref String aP1_InOutProjeto_Sigla )
      {
         this.AV7InOutProjeto_Codigo = aP0_InOutProjeto_Codigo;
         this.AV30InOutProjeto_Sigla = aP1_InOutProjeto_Sigla;
         executePrivate();
         aP0_InOutProjeto_Codigo=this.AV7InOutProjeto_Codigo;
         aP1_InOutProjeto_Sigla=this.AV30InOutProjeto_Sigla;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavProjeto_areatrabalhocodigo = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavProjeto_tecnicacontagem1 = new GXCombobox();
         cmbavProjeto_status1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavProjeto_tecnicacontagem2 = new GXCombobox();
         cmbavProjeto_status2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavProjeto_tecnicacontagem3 = new GXCombobox();
         cmbavProjeto_status3 = new GXCombobox();
         cmbProjeto_Status = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_68 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_68_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_68_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV31Projeto_Sigla1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
               AV33Projeto_TecnicaContagem1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
               AV34Projeto_Status1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV35Projeto_Sigla2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
               AV37Projeto_TecnicaContagem2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
               AV38Projeto_Status2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV39Projeto_Sigla3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
               AV41Projeto_TecnicaContagem3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
               AV42Projeto_Status3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutProjeto_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutProjeto_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV30InOutProjeto_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30InOutProjeto_Sigla", AV30InOutProjeto_Sigla);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PADG2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSDG2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEDG2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020515045746");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptprojeto.aspx") + "?" + UrlEncode("" +AV7InOutProjeto_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV30InOutProjeto_Sigla))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_SIGLA1", StringUtil.RTrim( AV31Projeto_Sigla1));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_TECNICACONTAGEM1", StringUtil.RTrim( AV33Projeto_TecnicaContagem1));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_STATUS1", StringUtil.RTrim( AV34Projeto_Status1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_SIGLA2", StringUtil.RTrim( AV35Projeto_Sigla2));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_TECNICACONTAGEM2", StringUtil.RTrim( AV37Projeto_TecnicaContagem2));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_STATUS2", StringUtil.RTrim( AV38Projeto_Status2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_SIGLA3", StringUtil.RTrim( AV39Projeto_Sigla3));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_TECNICACONTAGEM3", StringUtil.RTrim( AV41Projeto_TecnicaContagem3));
         GxWebStd.gx_hidden_field( context, "GXH_vPROJETO_STATUS3", StringUtil.RTrim( AV42Projeto_Status3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_68", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_68), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTPROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutProjeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTPROJETO_SIGLA", StringUtil.RTrim( AV30InOutProjeto_Sigla));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormDG2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptProjeto" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Projeto" ;
      }

      protected void WBDG0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_DG2( true) ;
         }
         else
         {
            wb_table1_2_DG2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DG2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_68_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(78, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_68_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(79, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"");
         }
         wbLoad = true;
      }

      protected void STARTDG2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Projeto", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPDG0( ) ;
      }

      protected void WSDG2( )
      {
         STARTDG2( ) ;
         EVTDG2( ) ;
      }

      protected void EVTDG2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11DG2 */
                           E11DG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12DG2 */
                           E12DG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13DG2 */
                           E13DG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14DG2 */
                           E14DG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15DG2 */
                           E15DG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16DG2 */
                           E16DG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17DG2 */
                           E17DG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_68_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
                           SubsflControlProps_682( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV62Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtProjeto_Codigo_Internalname), ",", "."));
                           A650Projeto_Sigla = StringUtil.Upper( cgiGet( edtProjeto_Sigla_Internalname));
                           A656Projeto_Prazo = (short)(context.localUtil.CToN( cgiGet( edtProjeto_Prazo_Internalname), ",", "."));
                           n656Projeto_Prazo = false;
                           cmbProjeto_Status.Name = cmbProjeto_Status_Internalname;
                           cmbProjeto_Status.CurrentValue = cgiGet( cmbProjeto_Status_Internalname);
                           A658Projeto_Status = cgiGet( cmbProjeto_Status_Internalname);
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E18DG2 */
                                 E18DG2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E19DG2 */
                                 E19DG2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E20DG2 */
                                 E20DG2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Projeto_sigla1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA1"), AV31Projeto_Sigla1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Projeto_tecnicacontagem1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM1"), AV33Projeto_TecnicaContagem1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Projeto_status1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS1"), AV34Projeto_Status1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Projeto_sigla2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA2"), AV35Projeto_Sigla2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Projeto_tecnicacontagem2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM2"), AV37Projeto_TecnicaContagem2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Projeto_status2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS2"), AV38Projeto_Status2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Projeto_sigla3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA3"), AV39Projeto_Sigla3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Projeto_tecnicacontagem3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM3"), AV41Projeto_TecnicaContagem3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Projeto_status3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS3"), AV42Projeto_Status3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E21DG2 */
                                       E21DG2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEDG2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormDG2( ) ;
            }
         }
      }

      protected void PADG2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavProjeto_areatrabalhocodigo.Name = "vPROJETO_AREATRABALHOCODIGO";
            dynavProjeto_areatrabalhocodigo.WebTags = "";
            dynavProjeto_areatrabalhocodigo.removeAllItems();
            /* Using cursor H00DG2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavProjeto_areatrabalhocodigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00DG2_A5AreaTrabalho_Codigo[0]), 6, 0)), H00DG2_A6AreaTrabalho_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavProjeto_areatrabalhocodigo.ItemCount > 0 )
            {
               AV59Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( dynavProjeto_areatrabalhocodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59Projeto_AreaTrabalhoCodigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Projeto_AreaTrabalhoCodigo), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("PROJETO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector1.addItem("PROJETO_TECNICACONTAGEM", "T�cnica de Contagem", 0);
            cmbavDynamicfiltersselector1.addItem("PROJETO_STATUS", "Status", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavProjeto_tecnicacontagem1.Name = "vPROJETO_TECNICACONTAGEM1";
            cmbavProjeto_tecnicacontagem1.WebTags = "";
            cmbavProjeto_tecnicacontagem1.addItem("", "Todos", 0);
            cmbavProjeto_tecnicacontagem1.addItem("1", "Indicativa", 0);
            cmbavProjeto_tecnicacontagem1.addItem("2", "Estimada", 0);
            cmbavProjeto_tecnicacontagem1.addItem("3", "Detalhada", 0);
            if ( cmbavProjeto_tecnicacontagem1.ItemCount > 0 )
            {
               AV33Projeto_TecnicaContagem1 = cmbavProjeto_tecnicacontagem1.getValidValue(AV33Projeto_TecnicaContagem1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
            }
            cmbavProjeto_status1.Name = "vPROJETO_STATUS1";
            cmbavProjeto_status1.WebTags = "";
            cmbavProjeto_status1.addItem("", "Todos", 0);
            cmbavProjeto_status1.addItem("A", "Aberto", 0);
            cmbavProjeto_status1.addItem("E", "Em Contagem", 0);
            cmbavProjeto_status1.addItem("C", "Contado", 0);
            if ( cmbavProjeto_status1.ItemCount > 0 )
            {
               AV34Projeto_Status1 = cmbavProjeto_status1.getValidValue(AV34Projeto_Status1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("PROJETO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector2.addItem("PROJETO_TECNICACONTAGEM", "T�cnica de Contagem", 0);
            cmbavDynamicfiltersselector2.addItem("PROJETO_STATUS", "Status", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavProjeto_tecnicacontagem2.Name = "vPROJETO_TECNICACONTAGEM2";
            cmbavProjeto_tecnicacontagem2.WebTags = "";
            cmbavProjeto_tecnicacontagem2.addItem("", "Todos", 0);
            cmbavProjeto_tecnicacontagem2.addItem("1", "Indicativa", 0);
            cmbavProjeto_tecnicacontagem2.addItem("2", "Estimada", 0);
            cmbavProjeto_tecnicacontagem2.addItem("3", "Detalhada", 0);
            if ( cmbavProjeto_tecnicacontagem2.ItemCount > 0 )
            {
               AV37Projeto_TecnicaContagem2 = cmbavProjeto_tecnicacontagem2.getValidValue(AV37Projeto_TecnicaContagem2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
            }
            cmbavProjeto_status2.Name = "vPROJETO_STATUS2";
            cmbavProjeto_status2.WebTags = "";
            cmbavProjeto_status2.addItem("", "Todos", 0);
            cmbavProjeto_status2.addItem("A", "Aberto", 0);
            cmbavProjeto_status2.addItem("E", "Em Contagem", 0);
            cmbavProjeto_status2.addItem("C", "Contado", 0);
            if ( cmbavProjeto_status2.ItemCount > 0 )
            {
               AV38Projeto_Status2 = cmbavProjeto_status2.getValidValue(AV38Projeto_Status2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("PROJETO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector3.addItem("PROJETO_TECNICACONTAGEM", "T�cnica de Contagem", 0);
            cmbavDynamicfiltersselector3.addItem("PROJETO_STATUS", "Status", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavProjeto_tecnicacontagem3.Name = "vPROJETO_TECNICACONTAGEM3";
            cmbavProjeto_tecnicacontagem3.WebTags = "";
            cmbavProjeto_tecnicacontagem3.addItem("", "Todos", 0);
            cmbavProjeto_tecnicacontagem3.addItem("1", "Indicativa", 0);
            cmbavProjeto_tecnicacontagem3.addItem("2", "Estimada", 0);
            cmbavProjeto_tecnicacontagem3.addItem("3", "Detalhada", 0);
            if ( cmbavProjeto_tecnicacontagem3.ItemCount > 0 )
            {
               AV41Projeto_TecnicaContagem3 = cmbavProjeto_tecnicacontagem3.getValidValue(AV41Projeto_TecnicaContagem3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
            }
            cmbavProjeto_status3.Name = "vPROJETO_STATUS3";
            cmbavProjeto_status3.WebTags = "";
            cmbavProjeto_status3.addItem("", "Todos", 0);
            cmbavProjeto_status3.addItem("A", "Aberto", 0);
            cmbavProjeto_status3.addItem("E", "Em Contagem", 0);
            cmbavProjeto_status3.addItem("C", "Contado", 0);
            if ( cmbavProjeto_status3.ItemCount > 0 )
            {
               AV42Projeto_Status3 = cmbavProjeto_status3.getValidValue(AV42Projeto_Status3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
            }
            GXCCtl = "PROJETO_STATUS_" + sGXsfl_68_idx;
            cmbProjeto_Status.Name = GXCCtl;
            cmbProjeto_Status.WebTags = "";
            cmbProjeto_Status.addItem("A", "Aberto", 0);
            cmbProjeto_Status.addItem("E", "Em Contagem", 0);
            cmbProjeto_Status.addItem("C", "Contado", 0);
            if ( cmbProjeto_Status.ItemCount > 0 )
            {
               A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavProjeto_areatrabalhocodigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvPROJETO_AREATRABALHOCODIGODG1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvPROJETO_AREATRABALHOCODIGO_dataDG1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvPROJETO_AREATRABALHOCODIGO_htmlDG1( )
      {
         int gxdynajaxvalue ;
         GXDLVvPROJETO_AREATRABALHOCODIGO_dataDG1( ) ;
         gxdynajaxindex = 1;
         dynavProjeto_areatrabalhocodigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavProjeto_areatrabalhocodigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavProjeto_areatrabalhocodigo.ItemCount > 0 )
         {
            AV59Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( dynavProjeto_areatrabalhocodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59Projeto_AreaTrabalhoCodigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Projeto_AreaTrabalhoCodigo), 6, 0)));
         }
      }

      protected void GXDLVvPROJETO_AREATRABALHOCODIGO_dataDG1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00DG3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00DG3_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00DG3_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_682( ) ;
         while ( nGXsfl_68_idx <= nRC_GXsfl_68 )
         {
            sendrow_682( ) ;
            nGXsfl_68_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_68_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_68_idx+1));
            sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
            SubsflControlProps_682( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV31Projeto_Sigla1 ,
                                       String AV33Projeto_TecnicaContagem1 ,
                                       String AV34Projeto_Status1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV35Projeto_Sigla2 ,
                                       String AV37Projeto_TecnicaContagem2 ,
                                       String AV38Projeto_Status2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV39Projeto_Sigla3 ,
                                       String AV41Projeto_TecnicaContagem3 ,
                                       String AV42Projeto_Status3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFDG2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "PROJETO_SIGLA", StringUtil.RTrim( A650Projeto_Sigla));
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_STATUS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
         GxWebStd.gx_hidden_field( context, "PROJETO_STATUS", StringUtil.RTrim( A658Projeto_Status));
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavProjeto_areatrabalhocodigo.ItemCount > 0 )
         {
            AV59Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( dynavProjeto_areatrabalhocodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59Projeto_AreaTrabalhoCodigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Projeto_AreaTrabalhoCodigo), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavProjeto_tecnicacontagem1.ItemCount > 0 )
         {
            AV33Projeto_TecnicaContagem1 = cmbavProjeto_tecnicacontagem1.getValidValue(AV33Projeto_TecnicaContagem1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
         }
         if ( cmbavProjeto_status1.ItemCount > 0 )
         {
            AV34Projeto_Status1 = cmbavProjeto_status1.getValidValue(AV34Projeto_Status1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavProjeto_tecnicacontagem2.ItemCount > 0 )
         {
            AV37Projeto_TecnicaContagem2 = cmbavProjeto_tecnicacontagem2.getValidValue(AV37Projeto_TecnicaContagem2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
         }
         if ( cmbavProjeto_status2.ItemCount > 0 )
         {
            AV38Projeto_Status2 = cmbavProjeto_status2.getValidValue(AV38Projeto_Status2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavProjeto_tecnicacontagem3.ItemCount > 0 )
         {
            AV41Projeto_TecnicaContagem3 = cmbavProjeto_tecnicacontagem3.getValidValue(AV41Projeto_TecnicaContagem3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
         }
         if ( cmbavProjeto_status3.ItemCount > 0 )
         {
            AV42Projeto_Status3 = cmbavProjeto_status3.getValidValue(AV42Projeto_Status3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDG2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFDG2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 68;
         /* Execute user event: E19DG2 */
         E19DG2 ();
         nGXsfl_68_idx = 1;
         sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
         SubsflControlProps_682( ) ;
         nGXsfl_68_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_682( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV31Projeto_Sigla1 ,
                                                 AV33Projeto_TecnicaContagem1 ,
                                                 AV34Projeto_Status1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV35Projeto_Sigla2 ,
                                                 AV37Projeto_TecnicaContagem2 ,
                                                 AV38Projeto_Status2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV39Projeto_Sigla3 ,
                                                 AV41Projeto_TecnicaContagem3 ,
                                                 AV42Projeto_Status3 ,
                                                 A650Projeto_Sigla ,
                                                 A652Projeto_TecnicaContagem ,
                                                 A658Projeto_Status ,
                                                 A2151Projeto_AreaTrabalhoCodigo ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV31Projeto_Sigla1 = StringUtil.PadR( StringUtil.RTrim( AV31Projeto_Sigla1), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
            lV35Projeto_Sigla2 = StringUtil.PadR( StringUtil.RTrim( AV35Projeto_Sigla2), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
            lV39Projeto_Sigla3 = StringUtil.PadR( StringUtil.RTrim( AV39Projeto_Sigla3), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
            /* Using cursor H00DG5 */
            pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV31Projeto_Sigla1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, lV35Projeto_Sigla2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, lV39Projeto_Sigla3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_68_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2155Projeto_SistemaCodigo = H00DG5_A2155Projeto_SistemaCodigo[0];
               n2155Projeto_SistemaCodigo = H00DG5_n2155Projeto_SistemaCodigo[0];
               A2151Projeto_AreaTrabalhoCodigo = H00DG5_A2151Projeto_AreaTrabalhoCodigo[0];
               n2151Projeto_AreaTrabalhoCodigo = H00DG5_n2151Projeto_AreaTrabalhoCodigo[0];
               A652Projeto_TecnicaContagem = H00DG5_A652Projeto_TecnicaContagem[0];
               A658Projeto_Status = H00DG5_A658Projeto_Status[0];
               A650Projeto_Sigla = H00DG5_A650Projeto_Sigla[0];
               A648Projeto_Codigo = H00DG5_A648Projeto_Codigo[0];
               A656Projeto_Prazo = H00DG5_A656Projeto_Prazo[0];
               n656Projeto_Prazo = H00DG5_n656Projeto_Prazo[0];
               A656Projeto_Prazo = H00DG5_A656Projeto_Prazo[0];
               n656Projeto_Prazo = H00DG5_n656Projeto_Prazo[0];
               /* Execute user event: E20DG2 */
               E20DG2 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 68;
            WBDG0( ) ;
         }
         nGXsfl_68_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV31Projeto_Sigla1 ,
                                              AV33Projeto_TecnicaContagem1 ,
                                              AV34Projeto_Status1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV35Projeto_Sigla2 ,
                                              AV37Projeto_TecnicaContagem2 ,
                                              AV38Projeto_Status2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV39Projeto_Sigla3 ,
                                              AV41Projeto_TecnicaContagem3 ,
                                              AV42Projeto_Status3 ,
                                              A650Projeto_Sigla ,
                                              A652Projeto_TecnicaContagem ,
                                              A658Projeto_Status ,
                                              A2151Projeto_AreaTrabalhoCodigo ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV31Projeto_Sigla1 = StringUtil.PadR( StringUtil.RTrim( AV31Projeto_Sigla1), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
         lV35Projeto_Sigla2 = StringUtil.PadR( StringUtil.RTrim( AV35Projeto_Sigla2), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
         lV39Projeto_Sigla3 = StringUtil.PadR( StringUtil.RTrim( AV39Projeto_Sigla3), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
         /* Using cursor H00DG7 */
         pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV31Projeto_Sigla1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, lV35Projeto_Sigla2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, lV39Projeto_Sigla3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3});
         GRID_nRecordCount = H00DG7_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3) ;
         }
         return (int)(0) ;
      }

      protected void STRUPDG0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E18DG2 */
         E18DG2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavProjeto_areatrabalhocodigo.Name = dynavProjeto_areatrabalhocodigo_Internalname;
            dynavProjeto_areatrabalhocodigo.CurrentValue = cgiGet( dynavProjeto_areatrabalhocodigo_Internalname);
            AV59Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( cgiGet( dynavProjeto_areatrabalhocodigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Projeto_AreaTrabalhoCodigo), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV31Projeto_Sigla1 = StringUtil.Upper( cgiGet( edtavProjeto_sigla1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
            cmbavProjeto_tecnicacontagem1.Name = cmbavProjeto_tecnicacontagem1_Internalname;
            cmbavProjeto_tecnicacontagem1.CurrentValue = cgiGet( cmbavProjeto_tecnicacontagem1_Internalname);
            AV33Projeto_TecnicaContagem1 = cgiGet( cmbavProjeto_tecnicacontagem1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
            cmbavProjeto_status1.Name = cmbavProjeto_status1_Internalname;
            cmbavProjeto_status1.CurrentValue = cgiGet( cmbavProjeto_status1_Internalname);
            AV34Projeto_Status1 = cgiGet( cmbavProjeto_status1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV35Projeto_Sigla2 = StringUtil.Upper( cgiGet( edtavProjeto_sigla2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
            cmbavProjeto_tecnicacontagem2.Name = cmbavProjeto_tecnicacontagem2_Internalname;
            cmbavProjeto_tecnicacontagem2.CurrentValue = cgiGet( cmbavProjeto_tecnicacontagem2_Internalname);
            AV37Projeto_TecnicaContagem2 = cgiGet( cmbavProjeto_tecnicacontagem2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
            cmbavProjeto_status2.Name = cmbavProjeto_status2_Internalname;
            cmbavProjeto_status2.CurrentValue = cgiGet( cmbavProjeto_status2_Internalname);
            AV38Projeto_Status2 = cgiGet( cmbavProjeto_status2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV39Projeto_Sigla3 = StringUtil.Upper( cgiGet( edtavProjeto_sigla3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
            cmbavProjeto_tecnicacontagem3.Name = cmbavProjeto_tecnicacontagem3_Internalname;
            cmbavProjeto_tecnicacontagem3.CurrentValue = cgiGet( cmbavProjeto_tecnicacontagem3_Internalname);
            AV41Projeto_TecnicaContagem3 = cgiGet( cmbavProjeto_tecnicacontagem3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
            cmbavProjeto_status3.Name = cmbavProjeto_status3_Internalname;
            cmbavProjeto_status3.CurrentValue = cgiGet( cmbavProjeto_status3_Internalname);
            AV42Projeto_Status3 = cgiGet( cmbavProjeto_status3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_68 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_68"), ",", "."));
            AV57GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV58GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA1"), AV31Projeto_Sigla1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM1"), AV33Projeto_TecnicaContagem1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS1"), AV34Projeto_Status1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA2"), AV35Projeto_Sigla2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM2"), AV37Projeto_TecnicaContagem2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS2"), AV38Projeto_Status2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_SIGLA3"), AV39Projeto_Sigla3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_TECNICACONTAGEM3"), AV41Projeto_TecnicaContagem3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPROJETO_STATUS3"), AV42Projeto_Status3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E18DG2 */
         E18DG2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18DG2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV33Projeto_TecnicaContagem1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
         AV34Projeto_Status1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
         AV15DynamicFiltersSelector1 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV37Projeto_TecnicaContagem2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
         AV38Projeto_Status2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
         AV19DynamicFiltersSelector2 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV41Projeto_TecnicaContagem3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
         AV42Projeto_Status3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
         AV23DynamicFiltersSelector3 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = "Selecione Projeto";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E19DG2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         AV57GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57GridCurrentPage), 10, 0)));
         AV58GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
      }

      protected void E11DG2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV56PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV56PageToGo) ;
         }
      }

      private void E20DG2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV62Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 68;
         }
         sendrow_682( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_68_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(68, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E21DG2 */
         E21DG2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21DG2( )
      {
         /* Enter Routine */
         AV7InOutProjeto_Codigo = A648Projeto_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutProjeto_Codigo), 6, 0)));
         AV30InOutProjeto_Sigla = A650Projeto_Sigla;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30InOutProjeto_Sigla", AV30InOutProjeto_Sigla);
         context.setWebReturnParms(new Object[] {(int)AV7InOutProjeto_Codigo,(String)AV30InOutProjeto_Sigla});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E16DG2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E12DG2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", cmbavProjeto_tecnicacontagem1.ToJavascriptSource());
         cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", cmbavProjeto_status1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", cmbavProjeto_tecnicacontagem2.ToJavascriptSource());
         cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", cmbavProjeto_status2.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", cmbavProjeto_tecnicacontagem3.ToJavascriptSource());
         cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", cmbavProjeto_status3.ToJavascriptSource());
      }

      protected void E17DG2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E13DG2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", cmbavProjeto_tecnicacontagem1.ToJavascriptSource());
         cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", cmbavProjeto_status1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", cmbavProjeto_tecnicacontagem2.ToJavascriptSource());
         cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", cmbavProjeto_status2.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", cmbavProjeto_tecnicacontagem3.ToJavascriptSource());
         cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", cmbavProjeto_status3.ToJavascriptSource());
      }

      protected void E14DG2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV31Projeto_Sigla1, AV33Projeto_TecnicaContagem1, AV34Projeto_Status1, AV19DynamicFiltersSelector2, AV35Projeto_Sigla2, AV37Projeto_TecnicaContagem2, AV38Projeto_Status2, AV23DynamicFiltersSelector3, AV39Projeto_Sigla3, AV41Projeto_TecnicaContagem3, AV42Projeto_Status3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", cmbavProjeto_tecnicacontagem1.ToJavascriptSource());
         cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", cmbavProjeto_status1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", cmbavProjeto_tecnicacontagem2.ToJavascriptSource());
         cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", cmbavProjeto_status2.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", cmbavProjeto_tecnicacontagem3.ToJavascriptSource());
         cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", cmbavProjeto_status3.ToJavascriptSource());
      }

      protected void E15DG2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavProjeto_areatrabalhocodigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59Projeto_AreaTrabalhoCodigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavProjeto_areatrabalhocodigo_Internalname, "Values", dynavProjeto_areatrabalhocodigo.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", cmbavProjeto_tecnicacontagem1.ToJavascriptSource());
         cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", cmbavProjeto_status1.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", cmbavProjeto_tecnicacontagem2.ToJavascriptSource());
         cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", cmbavProjeto_status2.ToJavascriptSource());
         cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", cmbavProjeto_tecnicacontagem3.ToJavascriptSource());
         cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", cmbavProjeto_status3.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavProjeto_sigla1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla1_Visible), 5, 0)));
         cmbavProjeto_tecnicacontagem1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem1.Visible), 5, 0)));
         cmbavProjeto_status1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 )
         {
            edtavProjeto_sigla1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 )
         {
            cmbavProjeto_tecnicacontagem1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_STATUS") == 0 )
         {
            cmbavProjeto_status1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavProjeto_sigla2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla2_Visible), 5, 0)));
         cmbavProjeto_tecnicacontagem2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem2.Visible), 5, 0)));
         cmbavProjeto_status2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 )
         {
            edtavProjeto_sigla2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 )
         {
            cmbavProjeto_tecnicacontagem2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_STATUS") == 0 )
         {
            cmbavProjeto_status2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavProjeto_sigla3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla3_Visible), 5, 0)));
         cmbavProjeto_tecnicacontagem3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem3.Visible), 5, 0)));
         cmbavProjeto_status3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 )
         {
            edtavProjeto_sigla3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProjeto_sigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProjeto_sigla3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 )
         {
            cmbavProjeto_tecnicacontagem3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_tecnicacontagem3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_STATUS") == 0 )
         {
            cmbavProjeto_status3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProjeto_status3.Visible), 5, 0)));
         }
      }

      protected void S162( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV35Projeto_Sigla2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV39Projeto_Sigla3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'CLEANFILTERS' Routine */
         AV59Projeto_AreaTrabalhoCodigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Projeto_AreaTrabalhoCodigo), 6, 0)));
         AV15DynamicFiltersSelector1 = "PROJETO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV31Projeto_Sigla1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 )
            {
               AV31Projeto_Sigla1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Projeto_Sigla1", AV31Projeto_Sigla1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 )
            {
               AV33Projeto_TecnicaContagem1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Projeto_TecnicaContagem1", AV33Projeto_TecnicaContagem1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_STATUS") == 0 )
            {
               AV34Projeto_Status1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Projeto_Status1", AV34Projeto_Status1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 )
               {
                  AV35Projeto_Sigla2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Projeto_Sigla2", AV35Projeto_Sigla2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 )
               {
                  AV37Projeto_TecnicaContagem2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Projeto_TecnicaContagem2", AV37Projeto_TecnicaContagem2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_STATUS") == 0 )
               {
                  AV38Projeto_Status2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Projeto_Status2", AV38Projeto_Status2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 )
                  {
                     AV39Projeto_Sigla3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Projeto_Sigla3", AV39Projeto_Sigla3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 )
                  {
                     AV41Projeto_TecnicaContagem3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Projeto_TecnicaContagem3", AV41Projeto_TecnicaContagem3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_STATUS") == 0 )
                  {
                     AV42Projeto_Status3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Projeto_Status3", AV42Projeto_Status3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Projeto_Sigla1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31Projeto_Sigla1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Projeto_TecnicaContagem1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV33Projeto_TecnicaContagem1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_STATUS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Projeto_Status1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV34Projeto_Status1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Projeto_Sigla2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV35Projeto_Sigla2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Projeto_TecnicaContagem2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV37Projeto_TecnicaContagem2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_STATUS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Projeto_Status2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV38Projeto_Status2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Projeto_Sigla3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV39Projeto_Sigla3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Projeto_TecnicaContagem3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV41Projeto_TecnicaContagem3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_STATUS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Projeto_Status3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV42Projeto_Status3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_DG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_DG2( true) ;
         }
         else
         {
            wb_table2_5_DG2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_DG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_62_DG2( true) ;
         }
         else
         {
            wb_table3_62_DG2( false) ;
         }
         return  ;
      }

      protected void wb_table3_62_DG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DG2e( true) ;
         }
         else
         {
            wb_table1_2_DG2e( false) ;
         }
      }

      protected void wb_table3_62_DG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_65_DG2( true) ;
         }
         else
         {
            wb_table4_65_DG2( false) ;
         }
         return  ;
      }

      protected void wb_table4_65_DG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_62_DG2e( true) ;
         }
         else
         {
            wb_table3_62_DG2e( false) ;
         }
      }

      protected void wb_table4_65_DG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"68\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Projeto") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Sigla") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(34), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Prazo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Status") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A650Projeto_Sigla));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A658Projeto_Status));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 68 )
         {
            wbEnd = 0;
            nRC_GXsfl_68 = (short)(nGXsfl_68_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_65_DG2e( true) ;
         }
         else
         {
            wb_table4_65_DG2e( false) ;
         }
      }

      protected void wb_table2_5_DG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_8_DG2( true) ;
         }
         else
         {
            wb_table5_8_DG2( false) ;
         }
         return  ;
      }

      protected void wb_table5_8_DG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_DG2e( true) ;
         }
         else
         {
            wb_table2_5_DG2e( false) ;
         }
      }

      protected void wb_table5_8_DG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextprojeto_areatrabalhocodigo_Internalname, "�rea de Trabalhlho", "", "", lblFiltertextprojeto_areatrabalhocodigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_PromptProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavProjeto_areatrabalhocodigo, dynavProjeto_areatrabalhocodigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV59Projeto_AreaTrabalhoCodigo), 6, 0)), 1, dynavProjeto_areatrabalhocodigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "", true, "HLP_PromptProjeto.htm");
            dynavProjeto_areatrabalhocodigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59Projeto_AreaTrabalhoCodigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavProjeto_areatrabalhocodigo_Internalname, "Values", (String)(dynavProjeto_areatrabalhocodigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_17_DG2( true) ;
         }
         else
         {
            wb_table6_17_DG2( false) ;
         }
         return  ;
      }

      protected void wb_table6_17_DG2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_8_DG2e( true) ;
         }
         else
         {
            wb_table5_8_DG2e( false) ;
         }
      }

      protected void wb_table6_17_DG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e22dg1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "", true, "HLP_PromptProjeto.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_sigla1_Internalname, StringUtil.RTrim( AV31Projeto_Sigla1), StringUtil.RTrim( context.localUtil.Format( AV31Projeto_Sigla1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_sigla1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_sigla1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptProjeto.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_tecnicacontagem1, cmbavProjeto_tecnicacontagem1_Internalname, StringUtil.RTrim( AV33Projeto_TecnicaContagem1), 1, cmbavProjeto_tecnicacontagem1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_tecnicacontagem1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "", true, "HLP_PromptProjeto.htm");
            cmbavProjeto_tecnicacontagem1.CurrentValue = StringUtil.RTrim( AV33Projeto_TecnicaContagem1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem1_Internalname, "Values", (String)(cmbavProjeto_tecnicacontagem1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_status1, cmbavProjeto_status1_Internalname, StringUtil.RTrim( AV34Projeto_Status1), 1, cmbavProjeto_status1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_status1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_PromptProjeto.htm");
            cmbavProjeto_status1.CurrentValue = StringUtil.RTrim( AV34Projeto_Status1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status1_Internalname, "Values", (String)(cmbavProjeto_status1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptProjeto.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e23dg1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_PromptProjeto.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_sigla2_Internalname, StringUtil.RTrim( AV35Projeto_Sigla2), StringUtil.RTrim( context.localUtil.Format( AV35Projeto_Sigla2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_sigla2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_sigla2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptProjeto.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_tecnicacontagem2, cmbavProjeto_tecnicacontagem2_Internalname, StringUtil.RTrim( AV37Projeto_TecnicaContagem2), 1, cmbavProjeto_tecnicacontagem2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_tecnicacontagem2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptProjeto.htm");
            cmbavProjeto_tecnicacontagem2.CurrentValue = StringUtil.RTrim( AV37Projeto_TecnicaContagem2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem2_Internalname, "Values", (String)(cmbavProjeto_tecnicacontagem2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_status2, cmbavProjeto_status2_Internalname, StringUtil.RTrim( AV38Projeto_Status2), 1, cmbavProjeto_status2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_status2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_PromptProjeto.htm");
            cmbavProjeto_status2.CurrentValue = StringUtil.RTrim( AV38Projeto_Status2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status2_Internalname, "Values", (String)(cmbavProjeto_status2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptProjeto.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e24dg1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_PromptProjeto.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavProjeto_sigla3_Internalname, StringUtil.RTrim( AV39Projeto_Sigla3), StringUtil.RTrim( context.localUtil.Format( AV39Projeto_Sigla3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProjeto_sigla3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavProjeto_sigla3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptProjeto.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_tecnicacontagem3, cmbavProjeto_tecnicacontagem3_Internalname, StringUtil.RTrim( AV41Projeto_TecnicaContagem3), 1, cmbavProjeto_tecnicacontagem3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_tecnicacontagem3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_PromptProjeto.htm");
            cmbavProjeto_tecnicacontagem3.CurrentValue = StringUtil.RTrim( AV41Projeto_TecnicaContagem3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_tecnicacontagem3_Internalname, "Values", (String)(cmbavProjeto_tecnicacontagem3.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto_status3, cmbavProjeto_status3_Internalname, StringUtil.RTrim( AV42Projeto_Status3), 1, cmbavProjeto_status3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavProjeto_status3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_PromptProjeto.htm");
            cmbavProjeto_status3.CurrentValue = StringUtil.RTrim( AV42Projeto_Status3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_status3_Internalname, "Values", (String)(cmbavProjeto_status3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_17_DG2e( true) ;
         }
         else
         {
            wb_table6_17_DG2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutProjeto_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutProjeto_Codigo), 6, 0)));
         AV30InOutProjeto_Sigla = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30InOutProjeto_Sigla", AV30InOutProjeto_Sigla);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADG2( ) ;
         WSDG2( ) ;
         WEDG2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020515045920");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptprojeto.js", "?2020515045921");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_682( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_68_idx;
         edtProjeto_Codigo_Internalname = "PROJETO_CODIGO_"+sGXsfl_68_idx;
         edtProjeto_Sigla_Internalname = "PROJETO_SIGLA_"+sGXsfl_68_idx;
         edtProjeto_Prazo_Internalname = "PROJETO_PRAZO_"+sGXsfl_68_idx;
         cmbProjeto_Status_Internalname = "PROJETO_STATUS_"+sGXsfl_68_idx;
      }

      protected void SubsflControlProps_fel_682( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_68_fel_idx;
         edtProjeto_Codigo_Internalname = "PROJETO_CODIGO_"+sGXsfl_68_fel_idx;
         edtProjeto_Sigla_Internalname = "PROJETO_SIGLA_"+sGXsfl_68_fel_idx;
         edtProjeto_Prazo_Internalname = "PROJETO_PRAZO_"+sGXsfl_68_fel_idx;
         cmbProjeto_Status_Internalname = "PROJETO_STATUS_"+sGXsfl_68_fel_idx;
      }

      protected void sendrow_682( )
      {
         SubsflControlProps_682( ) ;
         WBDG0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_68_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_68_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_68_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 69,'',false,'',68)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV62Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV62Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_68_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Sigla_Internalname,StringUtil.RTrim( A650Projeto_Sigla),StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjeto_Prazo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A656Projeto_Prazo), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjeto_Prazo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)34,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_68_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "PROJETO_STATUS_" + sGXsfl_68_idx;
               cmbProjeto_Status.Name = GXCCtl;
               cmbProjeto_Status.WebTags = "";
               cmbProjeto_Status.addItem("A", "Aberto", 0);
               cmbProjeto_Status.addItem("E", "Em Contagem", 0);
               cmbProjeto_Status.addItem("C", "Contado", 0);
               if ( cmbProjeto_Status.ItemCount > 0 )
               {
                  A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbProjeto_Status,(String)cmbProjeto_Status_Internalname,StringUtil.RTrim( A658Projeto_Status),(short)1,(String)cmbProjeto_Status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbProjeto_Status.CurrentValue = StringUtil.RTrim( A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjeto_Status_Internalname, "Values", (String)(cmbProjeto_Status.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_CODIGO"+"_"+sGXsfl_68_idx, GetSecureSignedToken( sGXsfl_68_idx, context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_SIGLA"+"_"+sGXsfl_68_idx, GetSecureSignedToken( sGXsfl_68_idx, StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETO_STATUS"+"_"+sGXsfl_68_idx, GetSecureSignedToken( sGXsfl_68_idx, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_68_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_68_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_68_idx+1));
            sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
            SubsflControlProps_682( ) ;
         }
         /* End function sendrow_682 */
      }

      protected void init_default_properties( )
      {
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextprojeto_areatrabalhocodigo_Internalname = "FILTERTEXTPROJETO_AREATRABALHOCODIGO";
         dynavProjeto_areatrabalhocodigo_Internalname = "vPROJETO_AREATRABALHOCODIGO";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavProjeto_sigla1_Internalname = "vPROJETO_SIGLA1";
         cmbavProjeto_tecnicacontagem1_Internalname = "vPROJETO_TECNICACONTAGEM1";
         cmbavProjeto_status1_Internalname = "vPROJETO_STATUS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavProjeto_sigla2_Internalname = "vPROJETO_SIGLA2";
         cmbavProjeto_tecnicacontagem2_Internalname = "vPROJETO_TECNICACONTAGEM2";
         cmbavProjeto_status2_Internalname = "vPROJETO_STATUS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavProjeto_sigla3_Internalname = "vPROJETO_SIGLA3";
         cmbavProjeto_tecnicacontagem3_Internalname = "vPROJETO_TECNICACONTAGEM3";
         cmbavProjeto_status3_Internalname = "vPROJETO_STATUS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtProjeto_Codigo_Internalname = "PROJETO_CODIGO";
         edtProjeto_Sigla_Internalname = "PROJETO_SIGLA";
         edtProjeto_Prazo_Internalname = "PROJETO_PRAZO";
         cmbProjeto_Status_Internalname = "PROJETO_STATUS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbProjeto_Status_Jsonclick = "";
         edtProjeto_Prazo_Jsonclick = "";
         edtProjeto_Sigla_Jsonclick = "";
         edtProjeto_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         cmbavProjeto_status3_Jsonclick = "";
         cmbavProjeto_tecnicacontagem3_Jsonclick = "";
         edtavProjeto_sigla3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavProjeto_status2_Jsonclick = "";
         cmbavProjeto_tecnicacontagem2_Jsonclick = "";
         edtavProjeto_sigla2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavProjeto_status1_Jsonclick = "";
         cmbavProjeto_tecnicacontagem1_Jsonclick = "";
         edtavProjeto_sigla1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavProjeto_areatrabalhocodigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavProjeto_status3.Visible = 1;
         cmbavProjeto_tecnicacontagem3.Visible = 1;
         edtavProjeto_sigla3_Visible = 1;
         cmbavProjeto_status2.Visible = 1;
         cmbavProjeto_tecnicacontagem2.Visible = 1;
         edtavProjeto_sigla2_Visible = 1;
         cmbavProjeto_status1.Visible = 1;
         cmbavProjeto_tecnicacontagem1.Visible = 1;
         edtavProjeto_sigla1_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Projeto";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV57GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV58GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11DG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E20DG2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E21DG2',iparms:[{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A650Projeto_Sigla',fld:'PROJETO_SIGLA',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutProjeto_Codigo',fld:'vINOUTPROJETO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV30InOutProjeto_Sigla',fld:'vINOUTPROJETO_SIGLA',pic:'@!',nv:''}]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E16DG2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E12DG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'},{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'},{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22DG1',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E17DG2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E13DG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'},{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'},{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23DG1',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E14DG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'},{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'},{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24DG1',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E15DG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV59Projeto_AreaTrabalhoCodigo',fld:'vPROJETO_AREATRABALHOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31Projeto_Sigla1',fld:'vPROJETO_SIGLA1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavProjeto_sigla1_Visible',ctrl:'vPROJETO_SIGLA1',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem1'},{av:'cmbavProjeto_status1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV35Projeto_Sigla2',fld:'vPROJETO_SIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39Projeto_Sigla3',fld:'vPROJETO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV33Projeto_TecnicaContagem1',fld:'vPROJETO_TECNICACONTAGEM1',pic:'',nv:''},{av:'AV34Projeto_Status1',fld:'vPROJETO_STATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV37Projeto_TecnicaContagem2',fld:'vPROJETO_TECNICACONTAGEM2',pic:'',nv:''},{av:'AV38Projeto_Status2',fld:'vPROJETO_STATUS2',pic:'',nv:''},{av:'AV41Projeto_TecnicaContagem3',fld:'vPROJETO_TECNICACONTAGEM3',pic:'',nv:''},{av:'AV42Projeto_Status3',fld:'vPROJETO_STATUS3',pic:'',nv:''},{av:'edtavProjeto_sigla2_Visible',ctrl:'vPROJETO_SIGLA2',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem2'},{av:'cmbavProjeto_status2'},{av:'edtavProjeto_sigla3_Visible',ctrl:'vPROJETO_SIGLA3',prop:'Visible'},{av:'cmbavProjeto_tecnicacontagem3'},{av:'cmbavProjeto_status3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV30InOutProjeto_Sigla = "";
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV31Projeto_Sigla1 = "";
         AV33Projeto_TecnicaContagem1 = "";
         AV34Projeto_Status1 = "A";
         AV19DynamicFiltersSelector2 = "";
         AV35Projeto_Sigla2 = "";
         AV37Projeto_TecnicaContagem2 = "";
         AV38Projeto_Status2 = "A";
         AV23DynamicFiltersSelector3 = "";
         AV39Projeto_Sigla3 = "";
         AV41Projeto_TecnicaContagem3 = "";
         AV42Projeto_Status3 = "A";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV62Select_GXI = "";
         A650Projeto_Sigla = "";
         A658Projeto_Status = "";
         scmdbuf = "";
         H00DG2_A5AreaTrabalho_Codigo = new int[1] ;
         H00DG2_A6AreaTrabalho_Descricao = new String[] {""} ;
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00DG3_A5AreaTrabalho_Codigo = new int[1] ;
         H00DG3_A6AreaTrabalho_Descricao = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         lV31Projeto_Sigla1 = "";
         lV35Projeto_Sigla2 = "";
         lV39Projeto_Sigla3 = "";
         A652Projeto_TecnicaContagem = "";
         H00DG5_A2155Projeto_SistemaCodigo = new int[1] ;
         H00DG5_n2155Projeto_SistemaCodigo = new bool[] {false} ;
         H00DG5_A2151Projeto_AreaTrabalhoCodigo = new int[1] ;
         H00DG5_n2151Projeto_AreaTrabalhoCodigo = new bool[] {false} ;
         H00DG5_A652Projeto_TecnicaContagem = new String[] {""} ;
         H00DG5_A658Projeto_Status = new String[] {""} ;
         H00DG5_A650Projeto_Sigla = new String[] {""} ;
         H00DG5_A648Projeto_Codigo = new int[1] ;
         H00DG5_A656Projeto_Prazo = new short[1] ;
         H00DG5_n656Projeto_Prazo = new bool[] {false} ;
         H00DG7_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFiltertextprojeto_areatrabalhocodigo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptprojeto__default(),
            new Object[][] {
                new Object[] {
               H00DG2_A5AreaTrabalho_Codigo, H00DG2_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00DG3_A5AreaTrabalho_Codigo, H00DG3_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00DG5_A2155Projeto_SistemaCodigo, H00DG5_n2155Projeto_SistemaCodigo, H00DG5_A2151Projeto_AreaTrabalhoCodigo, H00DG5_n2151Projeto_AreaTrabalhoCodigo, H00DG5_A652Projeto_TecnicaContagem, H00DG5_A658Projeto_Status, H00DG5_A650Projeto_Sigla, H00DG5_A648Projeto_Codigo, H00DG5_A656Projeto_Prazo, H00DG5_n656Projeto_Prazo
               }
               , new Object[] {
               H00DG7_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_68 ;
      private short nGXsfl_68_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A656Projeto_Prazo ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_68_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutProjeto_Codigo ;
      private int wcpOAV7InOutProjeto_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A648Projeto_Codigo ;
      private int AV59Projeto_AreaTrabalhoCodigo ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A2151Projeto_AreaTrabalhoCodigo ;
      private int A2155Projeto_SistemaCodigo ;
      private int AV56PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavProjeto_sigla1_Visible ;
      private int edtavProjeto_sigla2_Visible ;
      private int edtavProjeto_sigla3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV57GridCurrentPage ;
      private long AV58GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV30InOutProjeto_Sigla ;
      private String wcpOAV30InOutProjeto_Sigla ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_68_idx="0001" ;
      private String AV31Projeto_Sigla1 ;
      private String AV33Projeto_TecnicaContagem1 ;
      private String AV34Projeto_Status1 ;
      private String AV35Projeto_Sigla2 ;
      private String AV37Projeto_TecnicaContagem2 ;
      private String AV38Projeto_Status2 ;
      private String AV39Projeto_Sigla3 ;
      private String AV41Projeto_TecnicaContagem3 ;
      private String AV42Projeto_Status3 ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtProjeto_Codigo_Internalname ;
      private String A650Projeto_Sigla ;
      private String edtProjeto_Sigla_Internalname ;
      private String edtProjeto_Prazo_Internalname ;
      private String cmbProjeto_Status_Internalname ;
      private String A658Projeto_Status ;
      private String scmdbuf ;
      private String GXCCtl ;
      private String dynavProjeto_areatrabalhocodigo_Internalname ;
      private String gxwrpcisep ;
      private String lV31Projeto_Sigla1 ;
      private String lV35Projeto_Sigla2 ;
      private String lV39Projeto_Sigla3 ;
      private String A652Projeto_TecnicaContagem ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavProjeto_sigla1_Internalname ;
      private String cmbavProjeto_tecnicacontagem1_Internalname ;
      private String cmbavProjeto_status1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavProjeto_sigla2_Internalname ;
      private String cmbavProjeto_tecnicacontagem2_Internalname ;
      private String cmbavProjeto_status2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavProjeto_sigla3_Internalname ;
      private String cmbavProjeto_tecnicacontagem3_Internalname ;
      private String cmbavProjeto_status3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextprojeto_areatrabalhocodigo_Internalname ;
      private String lblFiltertextprojeto_areatrabalhocodigo_Jsonclick ;
      private String dynavProjeto_areatrabalhocodigo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavProjeto_sigla1_Jsonclick ;
      private String cmbavProjeto_tecnicacontagem1_Jsonclick ;
      private String cmbavProjeto_status1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavProjeto_sigla2_Jsonclick ;
      private String cmbavProjeto_tecnicacontagem2_Jsonclick ;
      private String cmbavProjeto_status2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavProjeto_sigla3_Jsonclick ;
      private String cmbavProjeto_tecnicacontagem3_Jsonclick ;
      private String cmbavProjeto_status3_Jsonclick ;
      private String sGXsfl_68_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtProjeto_Codigo_Jsonclick ;
      private String edtProjeto_Sigla_Jsonclick ;
      private String edtProjeto_Prazo_Jsonclick ;
      private String cmbProjeto_Status_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool toggleJsOutput ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n656Projeto_Prazo ;
      private bool n2155Projeto_SistemaCodigo ;
      private bool n2151Projeto_AreaTrabalhoCodigo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV62Select_GXI ;
      private String AV28Select ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutProjeto_Codigo ;
      private String aP1_InOutProjeto_Sigla ;
      private GXCombobox dynavProjeto_areatrabalhocodigo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavProjeto_tecnicacontagem1 ;
      private GXCombobox cmbavProjeto_status1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavProjeto_tecnicacontagem2 ;
      private GXCombobox cmbavProjeto_status2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavProjeto_tecnicacontagem3 ;
      private GXCombobox cmbavProjeto_status3 ;
      private GXCombobox cmbProjeto_Status ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00DG2_A5AreaTrabalho_Codigo ;
      private String[] H00DG2_A6AreaTrabalho_Descricao ;
      private int[] H00DG3_A5AreaTrabalho_Codigo ;
      private String[] H00DG3_A6AreaTrabalho_Descricao ;
      private int[] H00DG5_A2155Projeto_SistemaCodigo ;
      private bool[] H00DG5_n2155Projeto_SistemaCodigo ;
      private int[] H00DG5_A2151Projeto_AreaTrabalhoCodigo ;
      private bool[] H00DG5_n2151Projeto_AreaTrabalhoCodigo ;
      private String[] H00DG5_A652Projeto_TecnicaContagem ;
      private String[] H00DG5_A658Projeto_Status ;
      private String[] H00DG5_A650Projeto_Sigla ;
      private int[] H00DG5_A648Projeto_Codigo ;
      private short[] H00DG5_A656Projeto_Prazo ;
      private bool[] H00DG5_n656Projeto_Prazo ;
      private long[] H00DG7_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class promptprojeto__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00DG5( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV31Projeto_Sigla1 ,
                                             String AV33Projeto_TecnicaContagem1 ,
                                             String AV34Projeto_Status1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             String AV35Projeto_Sigla2 ,
                                             String AV37Projeto_TecnicaContagem2 ,
                                             String AV38Projeto_Status2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             String AV39Projeto_Sigla3 ,
                                             String AV41Projeto_TecnicaContagem3 ,
                                             String AV42Projeto_Status3 ,
                                             String A650Projeto_Sigla ,
                                             String A652Projeto_TecnicaContagem ,
                                             String A658Projeto_Status ,
                                             int A2151Projeto_AreaTrabalhoCodigo ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [15] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Projeto_SistemaCodigo], T1.[Projeto_AreaTrabalhoCodigo], T1.[Projeto_TecnicaContagem], T1.[Projeto_Status], T1.[Projeto_Sigla], T1.[Projeto_Codigo], COALESCE( T2.[Projeto_Prazo], 0) AS Projeto_Prazo";
         sFromString = " FROM ([Projeto] T1 WITH (NOLOCK) LEFT JOIN (SELECT [Sistema_Prazo] AS Projeto_Prazo, [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) ) T2 ON T2.[Sistema_Codigo] = T1.[Projeto_SistemaCodigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Projeto_AreaTrabalhoCodigo] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Projeto_Sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV31Projeto_Sigla1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Projeto_TecnicaContagem1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV33Projeto_TecnicaContagem1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Projeto_Status1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV34Projeto_Status1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Projeto_Sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV35Projeto_Sigla2 + '%')";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Projeto_TecnicaContagem2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV37Projeto_TecnicaContagem2)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Projeto_Status2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV38Projeto_Status2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Projeto_Sigla3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV39Projeto_Sigla3 + '%')";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Projeto_TecnicaContagem3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV41Projeto_TecnicaContagem3)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Projeto_Status3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV42Projeto_Status3)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         sOrderString = sOrderString + " ORDER BY T1.[Projeto_Sigla]";
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00DG7( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV31Projeto_Sigla1 ,
                                             String AV33Projeto_TecnicaContagem1 ,
                                             String AV34Projeto_Status1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             String AV35Projeto_Sigla2 ,
                                             String AV37Projeto_TecnicaContagem2 ,
                                             String AV38Projeto_Status2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             String AV39Projeto_Sigla3 ,
                                             String AV41Projeto_TecnicaContagem3 ,
                                             String AV42Projeto_Status3 ,
                                             String A650Projeto_Sigla ,
                                             String A652Projeto_TecnicaContagem ,
                                             String A658Projeto_Status ,
                                             int A2151Projeto_AreaTrabalhoCodigo ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [10] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Projeto] T1 WITH (NOLOCK) LEFT JOIN (SELECT [Sistema_Prazo] AS Projeto_Prazo, [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) ) T2 ON T2.[Sistema_Codigo] = T1.[Projeto_SistemaCodigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Projeto_AreaTrabalhoCodigo] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Projeto_Sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV31Projeto_Sigla1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Projeto_TecnicaContagem1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV33Projeto_TecnicaContagem1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Projeto_Status1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV34Projeto_Status1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Projeto_Sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV35Projeto_Sigla2 + '%')";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Projeto_TecnicaContagem2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV37Projeto_TecnicaContagem2)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Projeto_Status2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV38Projeto_Status2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Projeto_Sigla3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV39Projeto_Sigla3 + '%')";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Projeto_TecnicaContagem3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV41Projeto_TecnicaContagem3)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Projeto_Status3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV42Projeto_Status3)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00DG5(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] );
               case 3 :
                     return conditional_H00DG7(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DG2 ;
          prmH00DG2 = new Object[] {
          } ;
          Object[] prmH00DG3 ;
          prmH00DG3 = new Object[] {
          } ;
          Object[] prmH00DG5 ;
          prmH00DG5 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV31Projeto_Sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV33Projeto_TecnicaContagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV34Projeto_Status1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV35Projeto_Sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV37Projeto_TecnicaContagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV38Projeto_Status2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV39Projeto_Sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV41Projeto_TecnicaContagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV42Projeto_Status3",SqlDbType.Char,1,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00DG7 ;
          prmH00DG7 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV31Projeto_Sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV33Projeto_TecnicaContagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV34Projeto_Status1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV35Projeto_Sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV37Projeto_TecnicaContagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV38Projeto_Status2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV39Projeto_Sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV41Projeto_TecnicaContagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV42Projeto_Status3",SqlDbType.Char,1,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DG2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DG2,0,0,true,false )
             ,new CursorDef("H00DG3", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DG3,0,0,true,false )
             ,new CursorDef("H00DG5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DG5,11,0,true,false )
             ,new CursorDef("H00DG7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DG7,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((short[]) buf[8])[0] = rslt.getShort(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

}
