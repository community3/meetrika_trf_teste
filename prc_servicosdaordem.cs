/*
               File: PRC_ServicosDaOrdem
        Description: Servicos Da Ordem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:29.84
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_servicosdaordem : GXProcedure
   {
      public prc_servicosdaordem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_servicosdaordem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ServicoFluxo_ServicoCod ,
                           ref short aP1_ServicoFluxo_Ordem ,
                           out bool aP2_TemOutros )
      {
         this.A1522ServicoFluxo_ServicoCod = aP0_ServicoFluxo_ServicoCod;
         this.A1532ServicoFluxo_Ordem = aP1_ServicoFluxo_Ordem;
         this.AV9TemOutros = false ;
         initialize();
         executePrivate();
         aP0_ServicoFluxo_ServicoCod=this.A1522ServicoFluxo_ServicoCod;
         aP1_ServicoFluxo_Ordem=this.A1532ServicoFluxo_Ordem;
         aP2_TemOutros=this.AV9TemOutros;
      }

      public bool executeUdp( ref int aP0_ServicoFluxo_ServicoCod ,
                              ref short aP1_ServicoFluxo_Ordem )
      {
         this.A1522ServicoFluxo_ServicoCod = aP0_ServicoFluxo_ServicoCod;
         this.A1532ServicoFluxo_Ordem = aP1_ServicoFluxo_Ordem;
         this.AV9TemOutros = false ;
         initialize();
         executePrivate();
         aP0_ServicoFluxo_ServicoCod=this.A1522ServicoFluxo_ServicoCod;
         aP1_ServicoFluxo_Ordem=this.A1532ServicoFluxo_Ordem;
         aP2_TemOutros=this.AV9TemOutros;
         return AV9TemOutros ;
      }

      public void executeSubmit( ref int aP0_ServicoFluxo_ServicoCod ,
                                 ref short aP1_ServicoFluxo_Ordem ,
                                 out bool aP2_TemOutros )
      {
         prc_servicosdaordem objprc_servicosdaordem;
         objprc_servicosdaordem = new prc_servicosdaordem();
         objprc_servicosdaordem.A1522ServicoFluxo_ServicoCod = aP0_ServicoFluxo_ServicoCod;
         objprc_servicosdaordem.A1532ServicoFluxo_Ordem = aP1_ServicoFluxo_Ordem;
         objprc_servicosdaordem.AV9TemOutros = false ;
         objprc_servicosdaordem.context.SetSubmitInitialConfig(context);
         objprc_servicosdaordem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_servicosdaordem);
         aP0_ServicoFluxo_ServicoCod=this.A1522ServicoFluxo_ServicoCod;
         aP1_ServicoFluxo_Ordem=this.A1532ServicoFluxo_Ordem;
         aP2_TemOutros=this.AV9TemOutros;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_servicosdaordem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00B42 */
         pr_default.execute(0, new Object[] {A1522ServicoFluxo_ServicoCod, n1532ServicoFluxo_Ordem, A1532ServicoFluxo_Ordem});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1528ServicoFluxo_Codigo = P00B42_A1528ServicoFluxo_Codigo[0];
            AV9TemOutros = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00B42_A1522ServicoFluxo_ServicoCod = new int[1] ;
         P00B42_A1532ServicoFluxo_Ordem = new short[1] ;
         P00B42_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         P00B42_A1528ServicoFluxo_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_servicosdaordem__default(),
            new Object[][] {
                new Object[] {
               P00B42_A1522ServicoFluxo_ServicoCod, P00B42_A1532ServicoFluxo_Ordem, P00B42_n1532ServicoFluxo_Ordem, P00B42_A1528ServicoFluxo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1532ServicoFluxo_Ordem ;
      private int A1522ServicoFluxo_ServicoCod ;
      private int A1528ServicoFluxo_Codigo ;
      private String scmdbuf ;
      private bool AV9TemOutros ;
      private bool n1532ServicoFluxo_Ordem ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ServicoFluxo_ServicoCod ;
      private short aP1_ServicoFluxo_Ordem ;
      private IDataStoreProvider pr_default ;
      private int[] P00B42_A1522ServicoFluxo_ServicoCod ;
      private short[] P00B42_A1532ServicoFluxo_Ordem ;
      private bool[] P00B42_n1532ServicoFluxo_Ordem ;
      private int[] P00B42_A1528ServicoFluxo_Codigo ;
      private bool aP2_TemOutros ;
   }

   public class prc_servicosdaordem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00B42 ;
          prmP00B42 = new Object[] {
          new Object[] {"@ServicoFluxo_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoFluxo_Ordem",SqlDbType.SmallInt,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00B42", "SELECT TOP 1 [ServicoFluxo_ServicoCod], [ServicoFluxo_Ordem], [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (NOLOCK) WHERE [ServicoFluxo_ServicoCod] = @ServicoFluxo_ServicoCod and [ServicoFluxo_Ordem] = @ServicoFluxo_Ordem ORDER BY [ServicoFluxo_ServicoCod], [ServicoFluxo_Ordem] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B42,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                return;
       }
    }

 }

}
