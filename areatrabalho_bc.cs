/*
               File: AreaTrabalho_BC
        Description: �rea de Trabalho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:27:15.51
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class areatrabalho_bc : GXHttpHandler, IGxSilentTrn
   {
      public areatrabalho_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public areatrabalho_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow04106( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey04106( ) ;
         standaloneModal( ) ;
         AddRow04106( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11042 */
            E11042 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_040( )
      {
         BeforeValidate04106( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls04106( ) ;
            }
            else
            {
               CheckExtendedTable04106( ) ;
               if ( AnyError == 0 )
               {
                  ZM04106( 11) ;
                  ZM04106( 12) ;
                  ZM04106( 13) ;
                  ZM04106( 14) ;
                  ZM04106( 15) ;
                  ZM04106( 16) ;
                  ZM04106( 17) ;
               }
               CloseExtendedTableCursors04106( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12042( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV40Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV41GXV1 = 1;
            while ( AV41GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV41GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "AreaTrabalho_OrganizacaoCod") == 0 )
               {
                  AV29Insert_AreaTrabalho_OrganizacaoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Contratante_Codigo") == 0 )
               {
                  AV11Insert_Contratante_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "AreaTrabalho_ServicoPadrao") == 0 )
               {
                  AV27Insert_AreaTrabalho_ServicoPadrao = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV41GXV1 = (int)(AV41GXV1+1);
            }
         }
         AV39Causa = "";
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            AV31ParametrosSistema.Load(1);
            AV32Contratante_EmailSdaHost = AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdahost;
            AV33Contratante_EmailSdaUser = AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdauser;
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdakey)) )
            {
               AV35Contratante_EmailSdaPass = Crypto.Decrypt64( AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdapass, AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdakey);
            }
            AV36Contratante_EmailSdaPort = AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdaport;
            AV37Contratante_EmailSdaAut = AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdaaut;
            AV34Contratante_EmailSdaSec = AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdasec;
         }
      }

      protected void E11042( )
      {
         /* After Trn Routine */
         AV8WWPContext.gxTpr_Servicopadrao = (short)(A830AreaTrabalho_ServicoPadrao);
         new wwpbaseobjects.setwwpcontext(context ).execute(  AV8WWPContext) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            new prc_newperfilgestor(context ).execute( ref  A5AreaTrabalho_Codigo) ;
         }
         new wwpbaseobjects.audittransaction(context ).execute(  AV30AuditingObject,  AV40Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV8WWPContext", AV8WWPContext);
      }

      protected void E13042( )
      {
         /* Contratante_cnpj_Isvalid Routine */
         if ( new prc_validadocumento(context).executeUdp(  AV15Contratante_CNPJ,  "J") )
         {
            GX_msglist.addItem("Documento inv�lido. (Poder� continuar com o teste)!");
         }
         GXt_int1 = AV26Contratante_Codigo;
         new prc_retornacontratantepelocnpj(context ).execute(  AV15Contratante_CNPJ, out  AV20Contratante_Email, out  AV23Contratante_Fax, out  AV18Contratante_IE, out  AV17Contratante_NomeFantasia, out  AV22Contratante_Ramal, out  AV16Contratante_RazaoSocial, out  AV21Contratante_Telefone, out  AV19Contratante_WebSite, ref  AV25Municipio_Codigo, ref  AV24Estado_UF, out  GXt_int1) ;
         AV26Contratante_Codigo = GXt_int1;
      }

      protected void ZM04106( short GX_JID )
      {
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            Z6AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
            Z642AreaTrabalho_CalculoPFinal = A642AreaTrabalho_CalculoPFinal;
            Z834AreaTrabalho_ValidaOSFM = A834AreaTrabalho_ValidaOSFM;
            Z855AreaTrabalho_DiasParaPagar = A855AreaTrabalho_DiasParaPagar;
            Z987AreaTrabalho_ContratadaUpdBslCod = A987AreaTrabalho_ContratadaUpdBslCod;
            Z1154AreaTrabalho_TipoPlanilha = A1154AreaTrabalho_TipoPlanilha;
            Z72AreaTrabalho_Ativo = A72AreaTrabalho_Ativo;
            Z1588AreaTrabalho_SS_Codigo = A1588AreaTrabalho_SS_Codigo;
            Z2081AreaTrabalho_VerTA = A2081AreaTrabalho_VerTA;
            Z2080AreaTrabalho_SelUsrPrestadora = A2080AreaTrabalho_SelUsrPrestadora;
            Z29Contratante_Codigo = A29Contratante_Codigo;
            Z830AreaTrabalho_ServicoPadrao = A830AreaTrabalho_ServicoPadrao;
            Z1216AreaTrabalho_OrganizacaoCod = A1216AreaTrabalho_OrganizacaoCod;
            Z272AreaTrabalho_ContagensQtdGeral = A272AreaTrabalho_ContagensQtdGeral;
         }
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            Z33Contratante_Fax = A33Contratante_Fax;
            Z32Contratante_Ramal = A32Contratante_Ramal;
            Z31Contratante_Telefone = A31Contratante_Telefone;
            Z14Contratante_Email = A14Contratante_Email;
            Z13Contratante_WebSite = A13Contratante_WebSite;
            Z11Contratante_IE = A11Contratante_IE;
            Z10Contratante_NomeFantasia = A10Contratante_NomeFantasia;
            Z547Contratante_EmailSdaHost = A547Contratante_EmailSdaHost;
            Z548Contratante_EmailSdaUser = A548Contratante_EmailSdaUser;
            Z549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
            Z550Contratante_EmailSdaKey = A550Contratante_EmailSdaKey;
            Z551Contratante_EmailSdaAut = A551Contratante_EmailSdaAut;
            Z552Contratante_EmailSdaPort = A552Contratante_EmailSdaPort;
            Z1048Contratante_EmailSdaSec = A1048Contratante_EmailSdaSec;
            Z25Municipio_Codigo = A25Municipio_Codigo;
            Z335Contratante_PessoaCod = A335Contratante_PessoaCod;
            Z272AreaTrabalho_ContagensQtdGeral = A272AreaTrabalho_ContagensQtdGeral;
         }
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            Z272AreaTrabalho_ContagensQtdGeral = A272AreaTrabalho_ContagensQtdGeral;
         }
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            Z1214Organizacao_Nome = A1214Organizacao_Nome;
            Z272AreaTrabalho_ContagensQtdGeral = A272AreaTrabalho_ContagensQtdGeral;
         }
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            Z26Municipio_Nome = A26Municipio_Nome;
            Z23Estado_UF = A23Estado_UF;
            Z272AreaTrabalho_ContagensQtdGeral = A272AreaTrabalho_ContagensQtdGeral;
         }
         if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
         {
            Z24Estado_Nome = A24Estado_Nome;
            Z272AreaTrabalho_ContagensQtdGeral = A272AreaTrabalho_ContagensQtdGeral;
         }
         if ( ( GX_JID == 16 ) || ( GX_JID == 0 ) )
         {
            Z12Contratante_CNPJ = A12Contratante_CNPJ;
            Z9Contratante_RazaoSocial = A9Contratante_RazaoSocial;
            Z272AreaTrabalho_ContagensQtdGeral = A272AreaTrabalho_ContagensQtdGeral;
         }
         if ( ( GX_JID == 17 ) || ( GX_JID == 0 ) )
         {
            Z272AreaTrabalho_ContagensQtdGeral = A272AreaTrabalho_ContagensQtdGeral;
         }
         if ( GX_JID == -10 )
         {
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z6AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
            Z642AreaTrabalho_CalculoPFinal = A642AreaTrabalho_CalculoPFinal;
            Z834AreaTrabalho_ValidaOSFM = A834AreaTrabalho_ValidaOSFM;
            Z855AreaTrabalho_DiasParaPagar = A855AreaTrabalho_DiasParaPagar;
            Z987AreaTrabalho_ContratadaUpdBslCod = A987AreaTrabalho_ContratadaUpdBslCod;
            Z1154AreaTrabalho_TipoPlanilha = A1154AreaTrabalho_TipoPlanilha;
            Z72AreaTrabalho_Ativo = A72AreaTrabalho_Ativo;
            Z1588AreaTrabalho_SS_Codigo = A1588AreaTrabalho_SS_Codigo;
            Z2081AreaTrabalho_VerTA = A2081AreaTrabalho_VerTA;
            Z2080AreaTrabalho_SelUsrPrestadora = A2080AreaTrabalho_SelUsrPrestadora;
            Z29Contratante_Codigo = A29Contratante_Codigo;
            Z830AreaTrabalho_ServicoPadrao = A830AreaTrabalho_ServicoPadrao;
            Z1216AreaTrabalho_OrganizacaoCod = A1216AreaTrabalho_OrganizacaoCod;
            Z272AreaTrabalho_ContagensQtdGeral = A272AreaTrabalho_ContagensQtdGeral;
            Z1214Organizacao_Nome = A1214Organizacao_Nome;
            Z33Contratante_Fax = A33Contratante_Fax;
            Z32Contratante_Ramal = A32Contratante_Ramal;
            Z31Contratante_Telefone = A31Contratante_Telefone;
            Z14Contratante_Email = A14Contratante_Email;
            Z13Contratante_WebSite = A13Contratante_WebSite;
            Z11Contratante_IE = A11Contratante_IE;
            Z10Contratante_NomeFantasia = A10Contratante_NomeFantasia;
            Z547Contratante_EmailSdaHost = A547Contratante_EmailSdaHost;
            Z548Contratante_EmailSdaUser = A548Contratante_EmailSdaUser;
            Z549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
            Z550Contratante_EmailSdaKey = A550Contratante_EmailSdaKey;
            Z551Contratante_EmailSdaAut = A551Contratante_EmailSdaAut;
            Z552Contratante_EmailSdaPort = A552Contratante_EmailSdaPort;
            Z1048Contratante_EmailSdaSec = A1048Contratante_EmailSdaSec;
            Z25Municipio_Codigo = A25Municipio_Codigo;
            Z335Contratante_PessoaCod = A335Contratante_PessoaCod;
            Z26Municipio_Nome = A26Municipio_Nome;
            Z23Estado_UF = A23Estado_UF;
            Z24Estado_Nome = A24Estado_Nome;
            Z12Contratante_CNPJ = A12Contratante_CNPJ;
            Z9Contratante_RazaoSocial = A9Contratante_RazaoSocial;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV40Pgmname = "AreaTrabalho_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A72AreaTrabalho_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A72AreaTrabalho_Ativo = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal)) && ( Gx_BScreen == 0 ) )
         {
            A642AreaTrabalho_CalculoPFinal = "MB";
         }
      }

      protected void Load04106( )
      {
         /* Using cursor BC000413 */
         pr_default.execute(9, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound106 = 1;
            A6AreaTrabalho_Descricao = BC000413_A6AreaTrabalho_Descricao[0];
            A1214Organizacao_Nome = BC000413_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = BC000413_n1214Organizacao_Nome[0];
            A24Estado_Nome = BC000413_A24Estado_Nome[0];
            A26Municipio_Nome = BC000413_A26Municipio_Nome[0];
            A33Contratante_Fax = BC000413_A33Contratante_Fax[0];
            n33Contratante_Fax = BC000413_n33Contratante_Fax[0];
            A32Contratante_Ramal = BC000413_A32Contratante_Ramal[0];
            n32Contratante_Ramal = BC000413_n32Contratante_Ramal[0];
            A31Contratante_Telefone = BC000413_A31Contratante_Telefone[0];
            A14Contratante_Email = BC000413_A14Contratante_Email[0];
            n14Contratante_Email = BC000413_n14Contratante_Email[0];
            A13Contratante_WebSite = BC000413_A13Contratante_WebSite[0];
            n13Contratante_WebSite = BC000413_n13Contratante_WebSite[0];
            A12Contratante_CNPJ = BC000413_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = BC000413_n12Contratante_CNPJ[0];
            A11Contratante_IE = BC000413_A11Contratante_IE[0];
            A10Contratante_NomeFantasia = BC000413_A10Contratante_NomeFantasia[0];
            A9Contratante_RazaoSocial = BC000413_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = BC000413_n9Contratante_RazaoSocial[0];
            A547Contratante_EmailSdaHost = BC000413_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = BC000413_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = BC000413_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = BC000413_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = BC000413_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = BC000413_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = BC000413_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = BC000413_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = BC000413_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = BC000413_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = BC000413_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = BC000413_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = BC000413_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = BC000413_n1048Contratante_EmailSdaSec[0];
            A642AreaTrabalho_CalculoPFinal = BC000413_A642AreaTrabalho_CalculoPFinal[0];
            A834AreaTrabalho_ValidaOSFM = BC000413_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = BC000413_n834AreaTrabalho_ValidaOSFM[0];
            A855AreaTrabalho_DiasParaPagar = BC000413_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = BC000413_n855AreaTrabalho_DiasParaPagar[0];
            A987AreaTrabalho_ContratadaUpdBslCod = BC000413_A987AreaTrabalho_ContratadaUpdBslCod[0];
            n987AreaTrabalho_ContratadaUpdBslCod = BC000413_n987AreaTrabalho_ContratadaUpdBslCod[0];
            A1154AreaTrabalho_TipoPlanilha = BC000413_A1154AreaTrabalho_TipoPlanilha[0];
            n1154AreaTrabalho_TipoPlanilha = BC000413_n1154AreaTrabalho_TipoPlanilha[0];
            A72AreaTrabalho_Ativo = BC000413_A72AreaTrabalho_Ativo[0];
            A1588AreaTrabalho_SS_Codigo = BC000413_A1588AreaTrabalho_SS_Codigo[0];
            n1588AreaTrabalho_SS_Codigo = BC000413_n1588AreaTrabalho_SS_Codigo[0];
            A2081AreaTrabalho_VerTA = BC000413_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = BC000413_n2081AreaTrabalho_VerTA[0];
            A2080AreaTrabalho_SelUsrPrestadora = BC000413_A2080AreaTrabalho_SelUsrPrestadora[0];
            n2080AreaTrabalho_SelUsrPrestadora = BC000413_n2080AreaTrabalho_SelUsrPrestadora[0];
            A29Contratante_Codigo = BC000413_A29Contratante_Codigo[0];
            n29Contratante_Codigo = BC000413_n29Contratante_Codigo[0];
            A830AreaTrabalho_ServicoPadrao = BC000413_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = BC000413_n830AreaTrabalho_ServicoPadrao[0];
            A1216AreaTrabalho_OrganizacaoCod = BC000413_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = BC000413_n1216AreaTrabalho_OrganizacaoCod[0];
            A25Municipio_Codigo = BC000413_A25Municipio_Codigo[0];
            n25Municipio_Codigo = BC000413_n25Municipio_Codigo[0];
            A23Estado_UF = BC000413_A23Estado_UF[0];
            A335Contratante_PessoaCod = BC000413_A335Contratante_PessoaCod[0];
            A272AreaTrabalho_ContagensQtdGeral = BC000413_A272AreaTrabalho_ContagensQtdGeral[0];
            n272AreaTrabalho_ContagensQtdGeral = BC000413_n272AreaTrabalho_ContagensQtdGeral[0];
            ZM04106( -10) ;
         }
         pr_default.close(9);
         OnLoadActions04106( ) ;
      }

      protected void OnLoadActions04106( )
      {
      }

      protected void CheckExtendedTable04106( )
      {
         standaloneModal( ) ;
         /* Using cursor BC000411 */
         pr_default.execute(8, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            A272AreaTrabalho_ContagensQtdGeral = BC000411_A272AreaTrabalho_ContagensQtdGeral[0];
            n272AreaTrabalho_ContagensQtdGeral = BC000411_n272AreaTrabalho_ContagensQtdGeral[0];
         }
         else
         {
            A272AreaTrabalho_ContagensQtdGeral = 0;
            n272AreaTrabalho_ContagensQtdGeral = false;
         }
         pr_default.close(8);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A6AreaTrabalho_Descricao)) )
         {
            GX_msglist.addItem("�rea de Trabalho � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00046 */
         pr_default.execute(4, new Object[] {n1216AreaTrabalho_OrganizacaoCod, A1216AreaTrabalho_OrganizacaoCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A1216AreaTrabalho_OrganizacaoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Area Trabalho_Organizacao'.", "ForeignKeyNotFound", 1, "AREATRABALHO_ORGANIZACAOCOD");
               AnyError = 1;
            }
         }
         A1214Organizacao_Nome = BC00046_A1214Organizacao_Nome[0];
         n1214Organizacao_Nome = BC00046_n1214Organizacao_Nome[0];
         pr_default.close(4);
         /* Using cursor BC00044 */
         pr_default.execute(2, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A29Contratante_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Contratante'.", "ForeignKeyNotFound", 1, "CONTRATANTE_CODIGO");
               AnyError = 1;
            }
         }
         A33Contratante_Fax = BC00044_A33Contratante_Fax[0];
         n33Contratante_Fax = BC00044_n33Contratante_Fax[0];
         A32Contratante_Ramal = BC00044_A32Contratante_Ramal[0];
         n32Contratante_Ramal = BC00044_n32Contratante_Ramal[0];
         A31Contratante_Telefone = BC00044_A31Contratante_Telefone[0];
         A14Contratante_Email = BC00044_A14Contratante_Email[0];
         n14Contratante_Email = BC00044_n14Contratante_Email[0];
         A13Contratante_WebSite = BC00044_A13Contratante_WebSite[0];
         n13Contratante_WebSite = BC00044_n13Contratante_WebSite[0];
         A11Contratante_IE = BC00044_A11Contratante_IE[0];
         A10Contratante_NomeFantasia = BC00044_A10Contratante_NomeFantasia[0];
         A547Contratante_EmailSdaHost = BC00044_A547Contratante_EmailSdaHost[0];
         n547Contratante_EmailSdaHost = BC00044_n547Contratante_EmailSdaHost[0];
         A548Contratante_EmailSdaUser = BC00044_A548Contratante_EmailSdaUser[0];
         n548Contratante_EmailSdaUser = BC00044_n548Contratante_EmailSdaUser[0];
         A549Contratante_EmailSdaPass = BC00044_A549Contratante_EmailSdaPass[0];
         n549Contratante_EmailSdaPass = BC00044_n549Contratante_EmailSdaPass[0];
         A550Contratante_EmailSdaKey = BC00044_A550Contratante_EmailSdaKey[0];
         n550Contratante_EmailSdaKey = BC00044_n550Contratante_EmailSdaKey[0];
         A551Contratante_EmailSdaAut = BC00044_A551Contratante_EmailSdaAut[0];
         n551Contratante_EmailSdaAut = BC00044_n551Contratante_EmailSdaAut[0];
         A552Contratante_EmailSdaPort = BC00044_A552Contratante_EmailSdaPort[0];
         n552Contratante_EmailSdaPort = BC00044_n552Contratante_EmailSdaPort[0];
         A1048Contratante_EmailSdaSec = BC00044_A1048Contratante_EmailSdaSec[0];
         n1048Contratante_EmailSdaSec = BC00044_n1048Contratante_EmailSdaSec[0];
         A25Municipio_Codigo = BC00044_A25Municipio_Codigo[0];
         n25Municipio_Codigo = BC00044_n25Municipio_Codigo[0];
         A335Contratante_PessoaCod = BC00044_A335Contratante_PessoaCod[0];
         pr_default.close(2);
         /* Using cursor BC00047 */
         pr_default.execute(5, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A25Municipio_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Municipio'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A26Municipio_Nome = BC00047_A26Municipio_Nome[0];
         A23Estado_UF = BC00047_A23Estado_UF[0];
         pr_default.close(5);
         /* Using cursor BC00048 */
         pr_default.execute(6, new Object[] {A23Estado_UF});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A23Estado_UF)) ) )
            {
               GX_msglist.addItem("N�o existe 'Estado'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A24Estado_Nome = BC00048_A24Estado_Nome[0];
         pr_default.close(6);
         /* Using cursor BC00049 */
         pr_default.execute(7, new Object[] {A335Contratante_PessoaCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A335Contratante_PessoaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratante_Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A12Contratante_CNPJ = BC00049_A12Contratante_CNPJ[0];
         n12Contratante_CNPJ = BC00049_n12Contratante_CNPJ[0];
         A9Contratante_RazaoSocial = BC00049_A9Contratante_RazaoSocial[0];
         n9Contratante_RazaoSocial = BC00049_n9Contratante_RazaoSocial[0];
         pr_default.close(7);
         /* Using cursor BC00045 */
         pr_default.execute(3, new Object[] {n830AreaTrabalho_ServicoPadrao, A830AreaTrabalho_ServicoPadrao});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A830AreaTrabalho_ServicoPadrao) ) )
            {
               GX_msglist.addItem("N�o existe 'Area Trabalho_Servicos'.", "ForeignKeyNotFound", 1, "AREATRABALHO_SERVICOPADRAO");
               AnyError = 1;
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors04106( )
      {
         pr_default.close(8);
         pr_default.close(4);
         pr_default.close(2);
         pr_default.close(5);
         pr_default.close(6);
         pr_default.close(7);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey04106( )
      {
         /* Using cursor BC000414 */
         pr_default.execute(10, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound106 = 1;
         }
         else
         {
            RcdFound106 = 0;
         }
         pr_default.close(10);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00043 */
         pr_default.execute(1, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM04106( 10) ;
            RcdFound106 = 1;
            A5AreaTrabalho_Codigo = BC00043_A5AreaTrabalho_Codigo[0];
            A6AreaTrabalho_Descricao = BC00043_A6AreaTrabalho_Descricao[0];
            A642AreaTrabalho_CalculoPFinal = BC00043_A642AreaTrabalho_CalculoPFinal[0];
            A834AreaTrabalho_ValidaOSFM = BC00043_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = BC00043_n834AreaTrabalho_ValidaOSFM[0];
            A855AreaTrabalho_DiasParaPagar = BC00043_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = BC00043_n855AreaTrabalho_DiasParaPagar[0];
            A987AreaTrabalho_ContratadaUpdBslCod = BC00043_A987AreaTrabalho_ContratadaUpdBslCod[0];
            n987AreaTrabalho_ContratadaUpdBslCod = BC00043_n987AreaTrabalho_ContratadaUpdBslCod[0];
            A1154AreaTrabalho_TipoPlanilha = BC00043_A1154AreaTrabalho_TipoPlanilha[0];
            n1154AreaTrabalho_TipoPlanilha = BC00043_n1154AreaTrabalho_TipoPlanilha[0];
            A72AreaTrabalho_Ativo = BC00043_A72AreaTrabalho_Ativo[0];
            A1588AreaTrabalho_SS_Codigo = BC00043_A1588AreaTrabalho_SS_Codigo[0];
            n1588AreaTrabalho_SS_Codigo = BC00043_n1588AreaTrabalho_SS_Codigo[0];
            A2081AreaTrabalho_VerTA = BC00043_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = BC00043_n2081AreaTrabalho_VerTA[0];
            A2080AreaTrabalho_SelUsrPrestadora = BC00043_A2080AreaTrabalho_SelUsrPrestadora[0];
            n2080AreaTrabalho_SelUsrPrestadora = BC00043_n2080AreaTrabalho_SelUsrPrestadora[0];
            A29Contratante_Codigo = BC00043_A29Contratante_Codigo[0];
            n29Contratante_Codigo = BC00043_n29Contratante_Codigo[0];
            A830AreaTrabalho_ServicoPadrao = BC00043_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = BC00043_n830AreaTrabalho_ServicoPadrao[0];
            A1216AreaTrabalho_OrganizacaoCod = BC00043_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = BC00043_n1216AreaTrabalho_OrganizacaoCod[0];
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            sMode106 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load04106( ) ;
            if ( AnyError == 1 )
            {
               RcdFound106 = 0;
               InitializeNonKey04106( ) ;
            }
            Gx_mode = sMode106;
         }
         else
         {
            RcdFound106 = 0;
            InitializeNonKey04106( ) ;
            sMode106 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode106;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey04106( ) ;
         if ( RcdFound106 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_040( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency04106( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00042 */
            pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AreaTrabalho"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z6AreaTrabalho_Descricao, BC00042_A6AreaTrabalho_Descricao[0]) != 0 ) || ( StringUtil.StrCmp(Z642AreaTrabalho_CalculoPFinal, BC00042_A642AreaTrabalho_CalculoPFinal[0]) != 0 ) || ( Z834AreaTrabalho_ValidaOSFM != BC00042_A834AreaTrabalho_ValidaOSFM[0] ) || ( Z855AreaTrabalho_DiasParaPagar != BC00042_A855AreaTrabalho_DiasParaPagar[0] ) || ( Z987AreaTrabalho_ContratadaUpdBslCod != BC00042_A987AreaTrabalho_ContratadaUpdBslCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1154AreaTrabalho_TipoPlanilha != BC00042_A1154AreaTrabalho_TipoPlanilha[0] ) || ( Z72AreaTrabalho_Ativo != BC00042_A72AreaTrabalho_Ativo[0] ) || ( Z1588AreaTrabalho_SS_Codigo != BC00042_A1588AreaTrabalho_SS_Codigo[0] ) || ( Z2081AreaTrabalho_VerTA != BC00042_A2081AreaTrabalho_VerTA[0] ) || ( Z2080AreaTrabalho_SelUsrPrestadora != BC00042_A2080AreaTrabalho_SelUsrPrestadora[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z29Contratante_Codigo != BC00042_A29Contratante_Codigo[0] ) || ( Z830AreaTrabalho_ServicoPadrao != BC00042_A830AreaTrabalho_ServicoPadrao[0] ) || ( Z1216AreaTrabalho_OrganizacaoCod != BC00042_A1216AreaTrabalho_OrganizacaoCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"AreaTrabalho"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert04106( )
      {
         BeforeValidate04106( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable04106( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM04106( 0) ;
            CheckOptimisticConcurrency04106( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm04106( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert04106( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000415 */
                     pr_default.execute(11, new Object[] {A6AreaTrabalho_Descricao, A642AreaTrabalho_CalculoPFinal, n834AreaTrabalho_ValidaOSFM, A834AreaTrabalho_ValidaOSFM, n855AreaTrabalho_DiasParaPagar, A855AreaTrabalho_DiasParaPagar, n987AreaTrabalho_ContratadaUpdBslCod, A987AreaTrabalho_ContratadaUpdBslCod, n1154AreaTrabalho_TipoPlanilha, A1154AreaTrabalho_TipoPlanilha, A72AreaTrabalho_Ativo, n1588AreaTrabalho_SS_Codigo, A1588AreaTrabalho_SS_Codigo, n2081AreaTrabalho_VerTA, A2081AreaTrabalho_VerTA, n2080AreaTrabalho_SelUsrPrestadora, A2080AreaTrabalho_SelUsrPrestadora, n29Contratante_Codigo, A29Contratante_Codigo, n830AreaTrabalho_ServicoPadrao, A830AreaTrabalho_ServicoPadrao, n1216AreaTrabalho_OrganizacaoCod, A1216AreaTrabalho_OrganizacaoCod});
                     A5AreaTrabalho_Codigo = BC000415_A5AreaTrabalho_Codigo[0];
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("AreaTrabalho") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load04106( ) ;
            }
            EndLevel04106( ) ;
         }
         CloseExtendedTableCursors04106( ) ;
      }

      protected void Update04106( )
      {
         BeforeValidate04106( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable04106( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency04106( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm04106( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate04106( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000416 */
                     pr_default.execute(12, new Object[] {A6AreaTrabalho_Descricao, A642AreaTrabalho_CalculoPFinal, n834AreaTrabalho_ValidaOSFM, A834AreaTrabalho_ValidaOSFM, n855AreaTrabalho_DiasParaPagar, A855AreaTrabalho_DiasParaPagar, n987AreaTrabalho_ContratadaUpdBslCod, A987AreaTrabalho_ContratadaUpdBslCod, n1154AreaTrabalho_TipoPlanilha, A1154AreaTrabalho_TipoPlanilha, A72AreaTrabalho_Ativo, n1588AreaTrabalho_SS_Codigo, A1588AreaTrabalho_SS_Codigo, n2081AreaTrabalho_VerTA, A2081AreaTrabalho_VerTA, n2080AreaTrabalho_SelUsrPrestadora, A2080AreaTrabalho_SelUsrPrestadora, n29Contratante_Codigo, A29Contratante_Codigo, n830AreaTrabalho_ServicoPadrao, A830AreaTrabalho_ServicoPadrao, n1216AreaTrabalho_OrganizacaoCod, A1216AreaTrabalho_OrganizacaoCod, A5AreaTrabalho_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("AreaTrabalho") ;
                     if ( (pr_default.getStatus(12) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AreaTrabalho"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate04106( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel04106( ) ;
         }
         CloseExtendedTableCursors04106( ) ;
      }

      protected void DeferredUpdate04106( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate04106( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency04106( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls04106( ) ;
            AfterConfirm04106( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete04106( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000417 */
                  pr_default.execute(13, new Object[] {A5AreaTrabalho_Codigo});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("AreaTrabalho") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode106 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel04106( ) ;
         Gx_mode = sMode106;
      }

      protected void OnDeleteControls04106( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000419 */
            pr_default.execute(14, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               A272AreaTrabalho_ContagensQtdGeral = BC000419_A272AreaTrabalho_ContagensQtdGeral[0];
               n272AreaTrabalho_ContagensQtdGeral = BC000419_n272AreaTrabalho_ContagensQtdGeral[0];
            }
            else
            {
               A272AreaTrabalho_ContagensQtdGeral = 0;
               n272AreaTrabalho_ContagensQtdGeral = false;
            }
            pr_default.close(14);
            /* Using cursor BC000420 */
            pr_default.execute(15, new Object[] {n1216AreaTrabalho_OrganizacaoCod, A1216AreaTrabalho_OrganizacaoCod});
            A1214Organizacao_Nome = BC000420_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = BC000420_n1214Organizacao_Nome[0];
            pr_default.close(15);
            /* Using cursor BC000421 */
            pr_default.execute(16, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            A33Contratante_Fax = BC000421_A33Contratante_Fax[0];
            n33Contratante_Fax = BC000421_n33Contratante_Fax[0];
            A32Contratante_Ramal = BC000421_A32Contratante_Ramal[0];
            n32Contratante_Ramal = BC000421_n32Contratante_Ramal[0];
            A31Contratante_Telefone = BC000421_A31Contratante_Telefone[0];
            A14Contratante_Email = BC000421_A14Contratante_Email[0];
            n14Contratante_Email = BC000421_n14Contratante_Email[0];
            A13Contratante_WebSite = BC000421_A13Contratante_WebSite[0];
            n13Contratante_WebSite = BC000421_n13Contratante_WebSite[0];
            A11Contratante_IE = BC000421_A11Contratante_IE[0];
            A10Contratante_NomeFantasia = BC000421_A10Contratante_NomeFantasia[0];
            A547Contratante_EmailSdaHost = BC000421_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = BC000421_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = BC000421_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = BC000421_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = BC000421_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = BC000421_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = BC000421_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = BC000421_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = BC000421_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = BC000421_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = BC000421_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = BC000421_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = BC000421_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = BC000421_n1048Contratante_EmailSdaSec[0];
            A25Municipio_Codigo = BC000421_A25Municipio_Codigo[0];
            n25Municipio_Codigo = BC000421_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = BC000421_A335Contratante_PessoaCod[0];
            pr_default.close(16);
            /* Using cursor BC000422 */
            pr_default.execute(17, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
            A26Municipio_Nome = BC000422_A26Municipio_Nome[0];
            A23Estado_UF = BC000422_A23Estado_UF[0];
            pr_default.close(17);
            /* Using cursor BC000423 */
            pr_default.execute(18, new Object[] {A23Estado_UF});
            A24Estado_Nome = BC000423_A24Estado_Nome[0];
            pr_default.close(18);
            /* Using cursor BC000424 */
            pr_default.execute(19, new Object[] {A335Contratante_PessoaCod});
            A12Contratante_CNPJ = BC000424_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = BC000424_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = BC000424_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = BC000424_n9Contratante_RazaoSocial[0];
            pr_default.close(19);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000425 */
            pr_default.execute(20, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Grupo de Func�es"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor BC000426 */
            pr_default.execute(21, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T236"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor BC000427 */
            pr_default.execute(22, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor BC000428 */
            pr_default.execute(23, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario Notifica"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor BC000429 */
            pr_default.execute(24, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Indicador"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor BC000430 */
            pr_default.execute(25, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Notifica��es da Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
            /* Using cursor BC000431 */
            pr_default.execute(26, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Cat�logo de Servi�os"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
            /* Using cursor BC000432 */
            pr_default.execute(27, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(27) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gest�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(27);
            /* Using cursor BC000433 */
            pr_default.execute(28, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(28) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gloss�rio"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(28);
            /* Using cursor BC000434 */
            pr_default.execute(29, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(29) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Vari�vel Cocomo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(29);
            /* Using cursor BC000435 */
            pr_default.execute(30, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(30) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Regras de Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(30);
            /* Using cursor BC000436 */
            pr_default.execute(31, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Par�metros das Planilhas"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(31);
            /* Using cursor BC000437 */
            pr_default.execute(32, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T62"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(32);
            /* Using cursor BC000438 */
            pr_default.execute(33, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Item N�o Mensuravel"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
            /* Using cursor BC000439 */
            pr_default.execute(34, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Referencia INM"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
            /* Using cursor BC000440 */
            pr_default.execute(35, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Lote"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
            /* Using cursor BC000441 */
            pr_default.execute(36, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(36) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"N�o Conformidade"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(36);
            /* Using cursor BC000442 */
            pr_default.execute(37, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(37) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Status"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(37);
            /* Using cursor BC000443 */
            pr_default.execute(38, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(38) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Guia"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(38);
            /* Using cursor BC000444 */
            pr_default.execute(39, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(39) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(39);
            /* Using cursor BC000445 */
            pr_default.execute(40, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(40) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistema"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(40);
            /* Using cursor BC000446 */
            pr_default.execute(41, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(41) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(41);
            /* Using cursor BC000447 */
            pr_default.execute(42, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(42) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratada"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(42);
            /* Using cursor BC000448 */
            pr_default.execute(43, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(43) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Perfil"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(43);
            /* Using cursor BC000449 */
            pr_default.execute(44, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(44) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Proposta"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(44);
            /* Using cursor BC000450 */
            pr_default.execute(45, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(45) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Configura��o de Fatores de Impacto na Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(45);
            /* Using cursor BC000451 */
            pr_default.execute(46, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(46) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Area Trabalho_Guias"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(46);
         }
      }

      protected void EndLevel04106( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete04106( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart04106( )
      {
         /* Scan By routine */
         /* Using cursor BC000453 */
         pr_default.execute(47, new Object[] {A5AreaTrabalho_Codigo});
         RcdFound106 = 0;
         if ( (pr_default.getStatus(47) != 101) )
         {
            RcdFound106 = 1;
            A5AreaTrabalho_Codigo = BC000453_A5AreaTrabalho_Codigo[0];
            A6AreaTrabalho_Descricao = BC000453_A6AreaTrabalho_Descricao[0];
            A1214Organizacao_Nome = BC000453_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = BC000453_n1214Organizacao_Nome[0];
            A24Estado_Nome = BC000453_A24Estado_Nome[0];
            A26Municipio_Nome = BC000453_A26Municipio_Nome[0];
            A33Contratante_Fax = BC000453_A33Contratante_Fax[0];
            n33Contratante_Fax = BC000453_n33Contratante_Fax[0];
            A32Contratante_Ramal = BC000453_A32Contratante_Ramal[0];
            n32Contratante_Ramal = BC000453_n32Contratante_Ramal[0];
            A31Contratante_Telefone = BC000453_A31Contratante_Telefone[0];
            A14Contratante_Email = BC000453_A14Contratante_Email[0];
            n14Contratante_Email = BC000453_n14Contratante_Email[0];
            A13Contratante_WebSite = BC000453_A13Contratante_WebSite[0];
            n13Contratante_WebSite = BC000453_n13Contratante_WebSite[0];
            A12Contratante_CNPJ = BC000453_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = BC000453_n12Contratante_CNPJ[0];
            A11Contratante_IE = BC000453_A11Contratante_IE[0];
            A10Contratante_NomeFantasia = BC000453_A10Contratante_NomeFantasia[0];
            A9Contratante_RazaoSocial = BC000453_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = BC000453_n9Contratante_RazaoSocial[0];
            A547Contratante_EmailSdaHost = BC000453_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = BC000453_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = BC000453_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = BC000453_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = BC000453_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = BC000453_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = BC000453_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = BC000453_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = BC000453_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = BC000453_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = BC000453_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = BC000453_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = BC000453_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = BC000453_n1048Contratante_EmailSdaSec[0];
            A642AreaTrabalho_CalculoPFinal = BC000453_A642AreaTrabalho_CalculoPFinal[0];
            A834AreaTrabalho_ValidaOSFM = BC000453_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = BC000453_n834AreaTrabalho_ValidaOSFM[0];
            A855AreaTrabalho_DiasParaPagar = BC000453_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = BC000453_n855AreaTrabalho_DiasParaPagar[0];
            A987AreaTrabalho_ContratadaUpdBslCod = BC000453_A987AreaTrabalho_ContratadaUpdBslCod[0];
            n987AreaTrabalho_ContratadaUpdBslCod = BC000453_n987AreaTrabalho_ContratadaUpdBslCod[0];
            A1154AreaTrabalho_TipoPlanilha = BC000453_A1154AreaTrabalho_TipoPlanilha[0];
            n1154AreaTrabalho_TipoPlanilha = BC000453_n1154AreaTrabalho_TipoPlanilha[0];
            A72AreaTrabalho_Ativo = BC000453_A72AreaTrabalho_Ativo[0];
            A1588AreaTrabalho_SS_Codigo = BC000453_A1588AreaTrabalho_SS_Codigo[0];
            n1588AreaTrabalho_SS_Codigo = BC000453_n1588AreaTrabalho_SS_Codigo[0];
            A2081AreaTrabalho_VerTA = BC000453_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = BC000453_n2081AreaTrabalho_VerTA[0];
            A2080AreaTrabalho_SelUsrPrestadora = BC000453_A2080AreaTrabalho_SelUsrPrestadora[0];
            n2080AreaTrabalho_SelUsrPrestadora = BC000453_n2080AreaTrabalho_SelUsrPrestadora[0];
            A29Contratante_Codigo = BC000453_A29Contratante_Codigo[0];
            n29Contratante_Codigo = BC000453_n29Contratante_Codigo[0];
            A830AreaTrabalho_ServicoPadrao = BC000453_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = BC000453_n830AreaTrabalho_ServicoPadrao[0];
            A1216AreaTrabalho_OrganizacaoCod = BC000453_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = BC000453_n1216AreaTrabalho_OrganizacaoCod[0];
            A25Municipio_Codigo = BC000453_A25Municipio_Codigo[0];
            n25Municipio_Codigo = BC000453_n25Municipio_Codigo[0];
            A23Estado_UF = BC000453_A23Estado_UF[0];
            A335Contratante_PessoaCod = BC000453_A335Contratante_PessoaCod[0];
            A272AreaTrabalho_ContagensQtdGeral = BC000453_A272AreaTrabalho_ContagensQtdGeral[0];
            n272AreaTrabalho_ContagensQtdGeral = BC000453_n272AreaTrabalho_ContagensQtdGeral[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext04106( )
      {
         /* Scan next routine */
         pr_default.readNext(47);
         RcdFound106 = 0;
         ScanKeyLoad04106( ) ;
      }

      protected void ScanKeyLoad04106( )
      {
         sMode106 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(47) != 101) )
         {
            RcdFound106 = 1;
            A5AreaTrabalho_Codigo = BC000453_A5AreaTrabalho_Codigo[0];
            A6AreaTrabalho_Descricao = BC000453_A6AreaTrabalho_Descricao[0];
            A1214Organizacao_Nome = BC000453_A1214Organizacao_Nome[0];
            n1214Organizacao_Nome = BC000453_n1214Organizacao_Nome[0];
            A24Estado_Nome = BC000453_A24Estado_Nome[0];
            A26Municipio_Nome = BC000453_A26Municipio_Nome[0];
            A33Contratante_Fax = BC000453_A33Contratante_Fax[0];
            n33Contratante_Fax = BC000453_n33Contratante_Fax[0];
            A32Contratante_Ramal = BC000453_A32Contratante_Ramal[0];
            n32Contratante_Ramal = BC000453_n32Contratante_Ramal[0];
            A31Contratante_Telefone = BC000453_A31Contratante_Telefone[0];
            A14Contratante_Email = BC000453_A14Contratante_Email[0];
            n14Contratante_Email = BC000453_n14Contratante_Email[0];
            A13Contratante_WebSite = BC000453_A13Contratante_WebSite[0];
            n13Contratante_WebSite = BC000453_n13Contratante_WebSite[0];
            A12Contratante_CNPJ = BC000453_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = BC000453_n12Contratante_CNPJ[0];
            A11Contratante_IE = BC000453_A11Contratante_IE[0];
            A10Contratante_NomeFantasia = BC000453_A10Contratante_NomeFantasia[0];
            A9Contratante_RazaoSocial = BC000453_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = BC000453_n9Contratante_RazaoSocial[0];
            A547Contratante_EmailSdaHost = BC000453_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = BC000453_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = BC000453_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = BC000453_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = BC000453_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = BC000453_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = BC000453_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = BC000453_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = BC000453_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = BC000453_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = BC000453_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = BC000453_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = BC000453_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = BC000453_n1048Contratante_EmailSdaSec[0];
            A642AreaTrabalho_CalculoPFinal = BC000453_A642AreaTrabalho_CalculoPFinal[0];
            A834AreaTrabalho_ValidaOSFM = BC000453_A834AreaTrabalho_ValidaOSFM[0];
            n834AreaTrabalho_ValidaOSFM = BC000453_n834AreaTrabalho_ValidaOSFM[0];
            A855AreaTrabalho_DiasParaPagar = BC000453_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = BC000453_n855AreaTrabalho_DiasParaPagar[0];
            A987AreaTrabalho_ContratadaUpdBslCod = BC000453_A987AreaTrabalho_ContratadaUpdBslCod[0];
            n987AreaTrabalho_ContratadaUpdBslCod = BC000453_n987AreaTrabalho_ContratadaUpdBslCod[0];
            A1154AreaTrabalho_TipoPlanilha = BC000453_A1154AreaTrabalho_TipoPlanilha[0];
            n1154AreaTrabalho_TipoPlanilha = BC000453_n1154AreaTrabalho_TipoPlanilha[0];
            A72AreaTrabalho_Ativo = BC000453_A72AreaTrabalho_Ativo[0];
            A1588AreaTrabalho_SS_Codigo = BC000453_A1588AreaTrabalho_SS_Codigo[0];
            n1588AreaTrabalho_SS_Codigo = BC000453_n1588AreaTrabalho_SS_Codigo[0];
            A2081AreaTrabalho_VerTA = BC000453_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = BC000453_n2081AreaTrabalho_VerTA[0];
            A2080AreaTrabalho_SelUsrPrestadora = BC000453_A2080AreaTrabalho_SelUsrPrestadora[0];
            n2080AreaTrabalho_SelUsrPrestadora = BC000453_n2080AreaTrabalho_SelUsrPrestadora[0];
            A29Contratante_Codigo = BC000453_A29Contratante_Codigo[0];
            n29Contratante_Codigo = BC000453_n29Contratante_Codigo[0];
            A830AreaTrabalho_ServicoPadrao = BC000453_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = BC000453_n830AreaTrabalho_ServicoPadrao[0];
            A1216AreaTrabalho_OrganizacaoCod = BC000453_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = BC000453_n1216AreaTrabalho_OrganizacaoCod[0];
            A25Municipio_Codigo = BC000453_A25Municipio_Codigo[0];
            n25Municipio_Codigo = BC000453_n25Municipio_Codigo[0];
            A23Estado_UF = BC000453_A23Estado_UF[0];
            A335Contratante_PessoaCod = BC000453_A335Contratante_PessoaCod[0];
            A272AreaTrabalho_ContagensQtdGeral = BC000453_A272AreaTrabalho_ContagensQtdGeral[0];
            n272AreaTrabalho_ContagensQtdGeral = BC000453_n272AreaTrabalho_ContagensQtdGeral[0];
         }
         Gx_mode = sMode106;
      }

      protected void ScanKeyEnd04106( )
      {
         pr_default.close(47);
      }

      protected void AfterConfirm04106( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert04106( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate04106( )
      {
         /* Before Update Rules */
         new loadauditareatrabalho(context ).execute(  "Y", ref  AV30AuditingObject,  A5AreaTrabalho_Codigo,  Gx_mode) ;
      }

      protected void BeforeDelete04106( )
      {
         /* Before Delete Rules */
         new loadauditareatrabalho(context ).execute(  "Y", ref  AV30AuditingObject,  A5AreaTrabalho_Codigo,  Gx_mode) ;
      }

      protected void BeforeComplete04106( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditareatrabalho(context ).execute(  "N", ref  AV30AuditingObject,  A5AreaTrabalho_Codigo,  Gx_mode) ;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditareatrabalho(context ).execute(  "N", ref  AV30AuditingObject,  A5AreaTrabalho_Codigo,  Gx_mode) ;
         }
      }

      protected void BeforeValidate04106( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes04106( )
      {
      }

      protected void AddRow04106( )
      {
         VarsToRow106( bcAreaTrabalho) ;
      }

      protected void ReadRow04106( )
      {
         RowToVars106( bcAreaTrabalho, 1) ;
      }

      protected void InitializeNonKey04106( )
      {
         AV30AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A6AreaTrabalho_Descricao = "";
         A1216AreaTrabalho_OrganizacaoCod = 0;
         n1216AreaTrabalho_OrganizacaoCod = false;
         A1214Organizacao_Nome = "";
         n1214Organizacao_Nome = false;
         A29Contratante_Codigo = 0;
         n29Contratante_Codigo = false;
         A335Contratante_PessoaCod = 0;
         A23Estado_UF = "";
         A24Estado_Nome = "";
         A25Municipio_Codigo = 0;
         n25Municipio_Codigo = false;
         A26Municipio_Nome = "";
         A33Contratante_Fax = "";
         n33Contratante_Fax = false;
         A32Contratante_Ramal = "";
         n32Contratante_Ramal = false;
         A31Contratante_Telefone = "";
         A14Contratante_Email = "";
         n14Contratante_Email = false;
         A13Contratante_WebSite = "";
         n13Contratante_WebSite = false;
         A12Contratante_CNPJ = "";
         n12Contratante_CNPJ = false;
         A11Contratante_IE = "";
         A10Contratante_NomeFantasia = "";
         A9Contratante_RazaoSocial = "";
         n9Contratante_RazaoSocial = false;
         A547Contratante_EmailSdaHost = "";
         n547Contratante_EmailSdaHost = false;
         A548Contratante_EmailSdaUser = "";
         n548Contratante_EmailSdaUser = false;
         A549Contratante_EmailSdaPass = "";
         n549Contratante_EmailSdaPass = false;
         A550Contratante_EmailSdaKey = "";
         n550Contratante_EmailSdaKey = false;
         A551Contratante_EmailSdaAut = false;
         n551Contratante_EmailSdaAut = false;
         A552Contratante_EmailSdaPort = 0;
         n552Contratante_EmailSdaPort = false;
         A1048Contratante_EmailSdaSec = 0;
         n1048Contratante_EmailSdaSec = false;
         A272AreaTrabalho_ContagensQtdGeral = 0;
         n272AreaTrabalho_ContagensQtdGeral = false;
         A830AreaTrabalho_ServicoPadrao = 0;
         n830AreaTrabalho_ServicoPadrao = false;
         A834AreaTrabalho_ValidaOSFM = false;
         n834AreaTrabalho_ValidaOSFM = false;
         A855AreaTrabalho_DiasParaPagar = 0;
         n855AreaTrabalho_DiasParaPagar = false;
         A987AreaTrabalho_ContratadaUpdBslCod = 0;
         n987AreaTrabalho_ContratadaUpdBslCod = false;
         A1154AreaTrabalho_TipoPlanilha = 0;
         n1154AreaTrabalho_TipoPlanilha = false;
         A1588AreaTrabalho_SS_Codigo = 0;
         n1588AreaTrabalho_SS_Codigo = false;
         A2081AreaTrabalho_VerTA = 0;
         n2081AreaTrabalho_VerTA = false;
         A2080AreaTrabalho_SelUsrPrestadora = false;
         n2080AreaTrabalho_SelUsrPrestadora = false;
         A642AreaTrabalho_CalculoPFinal = "MB";
         A72AreaTrabalho_Ativo = true;
         Z6AreaTrabalho_Descricao = "";
         Z642AreaTrabalho_CalculoPFinal = "";
         Z834AreaTrabalho_ValidaOSFM = false;
         Z855AreaTrabalho_DiasParaPagar = 0;
         Z987AreaTrabalho_ContratadaUpdBslCod = 0;
         Z1154AreaTrabalho_TipoPlanilha = 0;
         Z72AreaTrabalho_Ativo = false;
         Z1588AreaTrabalho_SS_Codigo = 0;
         Z2081AreaTrabalho_VerTA = 0;
         Z2080AreaTrabalho_SelUsrPrestadora = false;
         Z29Contratante_Codigo = 0;
         Z830AreaTrabalho_ServicoPadrao = 0;
         Z1216AreaTrabalho_OrganizacaoCod = 0;
      }

      protected void InitAll04106( )
      {
         A5AreaTrabalho_Codigo = 0;
         InitializeNonKey04106( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A72AreaTrabalho_Ativo = i72AreaTrabalho_Ativo;
         A642AreaTrabalho_CalculoPFinal = i642AreaTrabalho_CalculoPFinal;
      }

      public void VarsToRow106( SdtAreaTrabalho obj106 )
      {
         obj106.gxTpr_Mode = Gx_mode;
         obj106.gxTpr_Areatrabalho_descricao = A6AreaTrabalho_Descricao;
         obj106.gxTpr_Areatrabalho_organizacaocod = A1216AreaTrabalho_OrganizacaoCod;
         obj106.gxTpr_Organizacao_nome = A1214Organizacao_Nome;
         obj106.gxTpr_Contratante_codigo = A29Contratante_Codigo;
         obj106.gxTpr_Contratante_pessoacod = A335Contratante_PessoaCod;
         obj106.gxTpr_Estado_uf = A23Estado_UF;
         obj106.gxTpr_Estado_nome = A24Estado_Nome;
         obj106.gxTpr_Municipio_codigo = A25Municipio_Codigo;
         obj106.gxTpr_Municipio_nome = A26Municipio_Nome;
         obj106.gxTpr_Contratante_fax = A33Contratante_Fax;
         obj106.gxTpr_Contratante_ramal = A32Contratante_Ramal;
         obj106.gxTpr_Contratante_telefone = A31Contratante_Telefone;
         obj106.gxTpr_Contratante_email = A14Contratante_Email;
         obj106.gxTpr_Contratante_website = A13Contratante_WebSite;
         obj106.gxTpr_Contratante_cnpj = A12Contratante_CNPJ;
         obj106.gxTpr_Contratante_ie = A11Contratante_IE;
         obj106.gxTpr_Contratante_nomefantasia = A10Contratante_NomeFantasia;
         obj106.gxTpr_Contratante_razaosocial = A9Contratante_RazaoSocial;
         obj106.gxTpr_Contratante_emailsdahost = A547Contratante_EmailSdaHost;
         obj106.gxTpr_Contratante_emailsdauser = A548Contratante_EmailSdaUser;
         obj106.gxTpr_Contratante_emailsdapass = A549Contratante_EmailSdaPass;
         obj106.gxTpr_Contratante_emailsdakey = A550Contratante_EmailSdaKey;
         obj106.gxTpr_Contratante_emailsdaaut = A551Contratante_EmailSdaAut;
         obj106.gxTpr_Contratante_emailsdaport = A552Contratante_EmailSdaPort;
         obj106.gxTpr_Contratante_emailsdasec = A1048Contratante_EmailSdaSec;
         obj106.gxTpr_Areatrabalho_contagensqtdgeral = A272AreaTrabalho_ContagensQtdGeral;
         obj106.gxTpr_Areatrabalho_servicopadrao = A830AreaTrabalho_ServicoPadrao;
         obj106.gxTpr_Areatrabalho_validaosfm = A834AreaTrabalho_ValidaOSFM;
         obj106.gxTpr_Areatrabalho_diasparapagar = A855AreaTrabalho_DiasParaPagar;
         obj106.gxTpr_Areatrabalho_contratadaupdbslcod = A987AreaTrabalho_ContratadaUpdBslCod;
         obj106.gxTpr_Areatrabalho_tipoplanilha = A1154AreaTrabalho_TipoPlanilha;
         obj106.gxTpr_Areatrabalho_ss_codigo = A1588AreaTrabalho_SS_Codigo;
         obj106.gxTpr_Areatrabalho_verta = A2081AreaTrabalho_VerTA;
         obj106.gxTpr_Areatrabalho_selusrprestadora = A2080AreaTrabalho_SelUsrPrestadora;
         obj106.gxTpr_Areatrabalho_calculopfinal = A642AreaTrabalho_CalculoPFinal;
         obj106.gxTpr_Areatrabalho_ativo = A72AreaTrabalho_Ativo;
         obj106.gxTpr_Areatrabalho_codigo = A5AreaTrabalho_Codigo;
         obj106.gxTpr_Areatrabalho_codigo_Z = Z5AreaTrabalho_Codigo;
         obj106.gxTpr_Areatrabalho_descricao_Z = Z6AreaTrabalho_Descricao;
         obj106.gxTpr_Areatrabalho_organizacaocod_Z = Z1216AreaTrabalho_OrganizacaoCod;
         obj106.gxTpr_Organizacao_nome_Z = Z1214Organizacao_Nome;
         obj106.gxTpr_Contratante_codigo_Z = Z29Contratante_Codigo;
         obj106.gxTpr_Contratante_pessoacod_Z = Z335Contratante_PessoaCod;
         obj106.gxTpr_Estado_uf_Z = Z23Estado_UF;
         obj106.gxTpr_Estado_nome_Z = Z24Estado_Nome;
         obj106.gxTpr_Municipio_codigo_Z = Z25Municipio_Codigo;
         obj106.gxTpr_Municipio_nome_Z = Z26Municipio_Nome;
         obj106.gxTpr_Contratante_fax_Z = Z33Contratante_Fax;
         obj106.gxTpr_Contratante_ramal_Z = Z32Contratante_Ramal;
         obj106.gxTpr_Contratante_telefone_Z = Z31Contratante_Telefone;
         obj106.gxTpr_Contratante_email_Z = Z14Contratante_Email;
         obj106.gxTpr_Contratante_website_Z = Z13Contratante_WebSite;
         obj106.gxTpr_Contratante_cnpj_Z = Z12Contratante_CNPJ;
         obj106.gxTpr_Contratante_ie_Z = Z11Contratante_IE;
         obj106.gxTpr_Contratante_nomefantasia_Z = Z10Contratante_NomeFantasia;
         obj106.gxTpr_Contratante_razaosocial_Z = Z9Contratante_RazaoSocial;
         obj106.gxTpr_Contratante_emailsdahost_Z = Z547Contratante_EmailSdaHost;
         obj106.gxTpr_Contratante_emailsdauser_Z = Z548Contratante_EmailSdaUser;
         obj106.gxTpr_Contratante_emailsdapass_Z = Z549Contratante_EmailSdaPass;
         obj106.gxTpr_Contratante_emailsdakey_Z = Z550Contratante_EmailSdaKey;
         obj106.gxTpr_Contratante_emailsdaaut_Z = Z551Contratante_EmailSdaAut;
         obj106.gxTpr_Contratante_emailsdaport_Z = Z552Contratante_EmailSdaPort;
         obj106.gxTpr_Contratante_emailsdasec_Z = Z1048Contratante_EmailSdaSec;
         obj106.gxTpr_Areatrabalho_contagensqtdgeral_Z = Z272AreaTrabalho_ContagensQtdGeral;
         obj106.gxTpr_Areatrabalho_calculopfinal_Z = Z642AreaTrabalho_CalculoPFinal;
         obj106.gxTpr_Areatrabalho_servicopadrao_Z = Z830AreaTrabalho_ServicoPadrao;
         obj106.gxTpr_Areatrabalho_validaosfm_Z = Z834AreaTrabalho_ValidaOSFM;
         obj106.gxTpr_Areatrabalho_diasparapagar_Z = Z855AreaTrabalho_DiasParaPagar;
         obj106.gxTpr_Areatrabalho_contratadaupdbslcod_Z = Z987AreaTrabalho_ContratadaUpdBslCod;
         obj106.gxTpr_Areatrabalho_tipoplanilha_Z = Z1154AreaTrabalho_TipoPlanilha;
         obj106.gxTpr_Areatrabalho_ativo_Z = Z72AreaTrabalho_Ativo;
         obj106.gxTpr_Areatrabalho_ss_codigo_Z = Z1588AreaTrabalho_SS_Codigo;
         obj106.gxTpr_Areatrabalho_verta_Z = Z2081AreaTrabalho_VerTA;
         obj106.gxTpr_Areatrabalho_selusrprestadora_Z = Z2080AreaTrabalho_SelUsrPrestadora;
         obj106.gxTpr_Areatrabalho_organizacaocod_N = (short)(Convert.ToInt16(n1216AreaTrabalho_OrganizacaoCod));
         obj106.gxTpr_Organizacao_nome_N = (short)(Convert.ToInt16(n1214Organizacao_Nome));
         obj106.gxTpr_Contratante_codigo_N = (short)(Convert.ToInt16(n29Contratante_Codigo));
         obj106.gxTpr_Municipio_codigo_N = (short)(Convert.ToInt16(n25Municipio_Codigo));
         obj106.gxTpr_Contratante_fax_N = (short)(Convert.ToInt16(n33Contratante_Fax));
         obj106.gxTpr_Contratante_ramal_N = (short)(Convert.ToInt16(n32Contratante_Ramal));
         obj106.gxTpr_Contratante_email_N = (short)(Convert.ToInt16(n14Contratante_Email));
         obj106.gxTpr_Contratante_website_N = (short)(Convert.ToInt16(n13Contratante_WebSite));
         obj106.gxTpr_Contratante_cnpj_N = (short)(Convert.ToInt16(n12Contratante_CNPJ));
         obj106.gxTpr_Contratante_razaosocial_N = (short)(Convert.ToInt16(n9Contratante_RazaoSocial));
         obj106.gxTpr_Contratante_emailsdahost_N = (short)(Convert.ToInt16(n547Contratante_EmailSdaHost));
         obj106.gxTpr_Contratante_emailsdauser_N = (short)(Convert.ToInt16(n548Contratante_EmailSdaUser));
         obj106.gxTpr_Contratante_emailsdapass_N = (short)(Convert.ToInt16(n549Contratante_EmailSdaPass));
         obj106.gxTpr_Contratante_emailsdakey_N = (short)(Convert.ToInt16(n550Contratante_EmailSdaKey));
         obj106.gxTpr_Contratante_emailsdaaut_N = (short)(Convert.ToInt16(n551Contratante_EmailSdaAut));
         obj106.gxTpr_Contratante_emailsdaport_N = (short)(Convert.ToInt16(n552Contratante_EmailSdaPort));
         obj106.gxTpr_Contratante_emailsdasec_N = (short)(Convert.ToInt16(n1048Contratante_EmailSdaSec));
         obj106.gxTpr_Areatrabalho_contagensqtdgeral_N = (short)(Convert.ToInt16(n272AreaTrabalho_ContagensQtdGeral));
         obj106.gxTpr_Areatrabalho_servicopadrao_N = (short)(Convert.ToInt16(n830AreaTrabalho_ServicoPadrao));
         obj106.gxTpr_Areatrabalho_validaosfm_N = (short)(Convert.ToInt16(n834AreaTrabalho_ValidaOSFM));
         obj106.gxTpr_Areatrabalho_diasparapagar_N = (short)(Convert.ToInt16(n855AreaTrabalho_DiasParaPagar));
         obj106.gxTpr_Areatrabalho_contratadaupdbslcod_N = (short)(Convert.ToInt16(n987AreaTrabalho_ContratadaUpdBslCod));
         obj106.gxTpr_Areatrabalho_tipoplanilha_N = (short)(Convert.ToInt16(n1154AreaTrabalho_TipoPlanilha));
         obj106.gxTpr_Areatrabalho_ss_codigo_N = (short)(Convert.ToInt16(n1588AreaTrabalho_SS_Codigo));
         obj106.gxTpr_Areatrabalho_verta_N = (short)(Convert.ToInt16(n2081AreaTrabalho_VerTA));
         obj106.gxTpr_Areatrabalho_selusrprestadora_N = (short)(Convert.ToInt16(n2080AreaTrabalho_SelUsrPrestadora));
         obj106.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow106( SdtAreaTrabalho obj106 )
      {
         obj106.gxTpr_Areatrabalho_codigo = A5AreaTrabalho_Codigo;
         return  ;
      }

      public void RowToVars106( SdtAreaTrabalho obj106 ,
                                int forceLoad )
      {
         Gx_mode = obj106.gxTpr_Mode;
         A6AreaTrabalho_Descricao = obj106.gxTpr_Areatrabalho_descricao;
         A1216AreaTrabalho_OrganizacaoCod = obj106.gxTpr_Areatrabalho_organizacaocod;
         n1216AreaTrabalho_OrganizacaoCod = false;
         A1214Organizacao_Nome = obj106.gxTpr_Organizacao_nome;
         n1214Organizacao_Nome = false;
         A29Contratante_Codigo = obj106.gxTpr_Contratante_codigo;
         n29Contratante_Codigo = false;
         A335Contratante_PessoaCod = obj106.gxTpr_Contratante_pessoacod;
         A23Estado_UF = obj106.gxTpr_Estado_uf;
         A24Estado_Nome = obj106.gxTpr_Estado_nome;
         A25Municipio_Codigo = obj106.gxTpr_Municipio_codigo;
         n25Municipio_Codigo = false;
         A26Municipio_Nome = obj106.gxTpr_Municipio_nome;
         A33Contratante_Fax = obj106.gxTpr_Contratante_fax;
         n33Contratante_Fax = false;
         A32Contratante_Ramal = obj106.gxTpr_Contratante_ramal;
         n32Contratante_Ramal = false;
         A31Contratante_Telefone = obj106.gxTpr_Contratante_telefone;
         A14Contratante_Email = obj106.gxTpr_Contratante_email;
         n14Contratante_Email = false;
         A13Contratante_WebSite = obj106.gxTpr_Contratante_website;
         n13Contratante_WebSite = false;
         A12Contratante_CNPJ = obj106.gxTpr_Contratante_cnpj;
         n12Contratante_CNPJ = false;
         A11Contratante_IE = obj106.gxTpr_Contratante_ie;
         A10Contratante_NomeFantasia = obj106.gxTpr_Contratante_nomefantasia;
         A9Contratante_RazaoSocial = obj106.gxTpr_Contratante_razaosocial;
         n9Contratante_RazaoSocial = false;
         A547Contratante_EmailSdaHost = obj106.gxTpr_Contratante_emailsdahost;
         n547Contratante_EmailSdaHost = false;
         A548Contratante_EmailSdaUser = obj106.gxTpr_Contratante_emailsdauser;
         n548Contratante_EmailSdaUser = false;
         A549Contratante_EmailSdaPass = obj106.gxTpr_Contratante_emailsdapass;
         n549Contratante_EmailSdaPass = false;
         A550Contratante_EmailSdaKey = obj106.gxTpr_Contratante_emailsdakey;
         n550Contratante_EmailSdaKey = false;
         A551Contratante_EmailSdaAut = obj106.gxTpr_Contratante_emailsdaaut;
         n551Contratante_EmailSdaAut = false;
         A552Contratante_EmailSdaPort = obj106.gxTpr_Contratante_emailsdaport;
         n552Contratante_EmailSdaPort = false;
         A1048Contratante_EmailSdaSec = obj106.gxTpr_Contratante_emailsdasec;
         n1048Contratante_EmailSdaSec = false;
         A272AreaTrabalho_ContagensQtdGeral = obj106.gxTpr_Areatrabalho_contagensqtdgeral;
         n272AreaTrabalho_ContagensQtdGeral = false;
         A830AreaTrabalho_ServicoPadrao = obj106.gxTpr_Areatrabalho_servicopadrao;
         n830AreaTrabalho_ServicoPadrao = false;
         A834AreaTrabalho_ValidaOSFM = obj106.gxTpr_Areatrabalho_validaosfm;
         n834AreaTrabalho_ValidaOSFM = false;
         A855AreaTrabalho_DiasParaPagar = obj106.gxTpr_Areatrabalho_diasparapagar;
         n855AreaTrabalho_DiasParaPagar = false;
         A987AreaTrabalho_ContratadaUpdBslCod = obj106.gxTpr_Areatrabalho_contratadaupdbslcod;
         n987AreaTrabalho_ContratadaUpdBslCod = false;
         A1154AreaTrabalho_TipoPlanilha = obj106.gxTpr_Areatrabalho_tipoplanilha;
         n1154AreaTrabalho_TipoPlanilha = false;
         A1588AreaTrabalho_SS_Codigo = obj106.gxTpr_Areatrabalho_ss_codigo;
         n1588AreaTrabalho_SS_Codigo = false;
         A2081AreaTrabalho_VerTA = obj106.gxTpr_Areatrabalho_verta;
         n2081AreaTrabalho_VerTA = false;
         A2080AreaTrabalho_SelUsrPrestadora = obj106.gxTpr_Areatrabalho_selusrprestadora;
         n2080AreaTrabalho_SelUsrPrestadora = false;
         A642AreaTrabalho_CalculoPFinal = obj106.gxTpr_Areatrabalho_calculopfinal;
         A72AreaTrabalho_Ativo = obj106.gxTpr_Areatrabalho_ativo;
         A5AreaTrabalho_Codigo = obj106.gxTpr_Areatrabalho_codigo;
         Z5AreaTrabalho_Codigo = obj106.gxTpr_Areatrabalho_codigo_Z;
         Z6AreaTrabalho_Descricao = obj106.gxTpr_Areatrabalho_descricao_Z;
         Z1216AreaTrabalho_OrganizacaoCod = obj106.gxTpr_Areatrabalho_organizacaocod_Z;
         Z1214Organizacao_Nome = obj106.gxTpr_Organizacao_nome_Z;
         Z29Contratante_Codigo = obj106.gxTpr_Contratante_codigo_Z;
         Z335Contratante_PessoaCod = obj106.gxTpr_Contratante_pessoacod_Z;
         Z23Estado_UF = obj106.gxTpr_Estado_uf_Z;
         Z24Estado_Nome = obj106.gxTpr_Estado_nome_Z;
         Z25Municipio_Codigo = obj106.gxTpr_Municipio_codigo_Z;
         Z26Municipio_Nome = obj106.gxTpr_Municipio_nome_Z;
         Z33Contratante_Fax = obj106.gxTpr_Contratante_fax_Z;
         Z32Contratante_Ramal = obj106.gxTpr_Contratante_ramal_Z;
         Z31Contratante_Telefone = obj106.gxTpr_Contratante_telefone_Z;
         Z14Contratante_Email = obj106.gxTpr_Contratante_email_Z;
         Z13Contratante_WebSite = obj106.gxTpr_Contratante_website_Z;
         Z12Contratante_CNPJ = obj106.gxTpr_Contratante_cnpj_Z;
         Z11Contratante_IE = obj106.gxTpr_Contratante_ie_Z;
         Z10Contratante_NomeFantasia = obj106.gxTpr_Contratante_nomefantasia_Z;
         Z9Contratante_RazaoSocial = obj106.gxTpr_Contratante_razaosocial_Z;
         Z547Contratante_EmailSdaHost = obj106.gxTpr_Contratante_emailsdahost_Z;
         Z548Contratante_EmailSdaUser = obj106.gxTpr_Contratante_emailsdauser_Z;
         Z549Contratante_EmailSdaPass = obj106.gxTpr_Contratante_emailsdapass_Z;
         Z550Contratante_EmailSdaKey = obj106.gxTpr_Contratante_emailsdakey_Z;
         Z551Contratante_EmailSdaAut = obj106.gxTpr_Contratante_emailsdaaut_Z;
         Z552Contratante_EmailSdaPort = obj106.gxTpr_Contratante_emailsdaport_Z;
         Z1048Contratante_EmailSdaSec = obj106.gxTpr_Contratante_emailsdasec_Z;
         Z272AreaTrabalho_ContagensQtdGeral = obj106.gxTpr_Areatrabalho_contagensqtdgeral_Z;
         Z642AreaTrabalho_CalculoPFinal = obj106.gxTpr_Areatrabalho_calculopfinal_Z;
         Z830AreaTrabalho_ServicoPadrao = obj106.gxTpr_Areatrabalho_servicopadrao_Z;
         Z834AreaTrabalho_ValidaOSFM = obj106.gxTpr_Areatrabalho_validaosfm_Z;
         Z855AreaTrabalho_DiasParaPagar = obj106.gxTpr_Areatrabalho_diasparapagar_Z;
         Z987AreaTrabalho_ContratadaUpdBslCod = obj106.gxTpr_Areatrabalho_contratadaupdbslcod_Z;
         Z1154AreaTrabalho_TipoPlanilha = obj106.gxTpr_Areatrabalho_tipoplanilha_Z;
         Z72AreaTrabalho_Ativo = obj106.gxTpr_Areatrabalho_ativo_Z;
         Z1588AreaTrabalho_SS_Codigo = obj106.gxTpr_Areatrabalho_ss_codigo_Z;
         Z2081AreaTrabalho_VerTA = obj106.gxTpr_Areatrabalho_verta_Z;
         Z2080AreaTrabalho_SelUsrPrestadora = obj106.gxTpr_Areatrabalho_selusrprestadora_Z;
         n1216AreaTrabalho_OrganizacaoCod = (bool)(Convert.ToBoolean(obj106.gxTpr_Areatrabalho_organizacaocod_N));
         n1214Organizacao_Nome = (bool)(Convert.ToBoolean(obj106.gxTpr_Organizacao_nome_N));
         n29Contratante_Codigo = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_codigo_N));
         n25Municipio_Codigo = (bool)(Convert.ToBoolean(obj106.gxTpr_Municipio_codigo_N));
         n33Contratante_Fax = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_fax_N));
         n32Contratante_Ramal = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_ramal_N));
         n14Contratante_Email = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_email_N));
         n13Contratante_WebSite = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_website_N));
         n12Contratante_CNPJ = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_cnpj_N));
         n9Contratante_RazaoSocial = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_razaosocial_N));
         n547Contratante_EmailSdaHost = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_emailsdahost_N));
         n548Contratante_EmailSdaUser = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_emailsdauser_N));
         n549Contratante_EmailSdaPass = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_emailsdapass_N));
         n550Contratante_EmailSdaKey = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_emailsdakey_N));
         n551Contratante_EmailSdaAut = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_emailsdaaut_N));
         n552Contratante_EmailSdaPort = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_emailsdaport_N));
         n1048Contratante_EmailSdaSec = (bool)(Convert.ToBoolean(obj106.gxTpr_Contratante_emailsdasec_N));
         n272AreaTrabalho_ContagensQtdGeral = (bool)(Convert.ToBoolean(obj106.gxTpr_Areatrabalho_contagensqtdgeral_N));
         n830AreaTrabalho_ServicoPadrao = (bool)(Convert.ToBoolean(obj106.gxTpr_Areatrabalho_servicopadrao_N));
         n834AreaTrabalho_ValidaOSFM = (bool)(Convert.ToBoolean(obj106.gxTpr_Areatrabalho_validaosfm_N));
         n855AreaTrabalho_DiasParaPagar = (bool)(Convert.ToBoolean(obj106.gxTpr_Areatrabalho_diasparapagar_N));
         n987AreaTrabalho_ContratadaUpdBslCod = (bool)(Convert.ToBoolean(obj106.gxTpr_Areatrabalho_contratadaupdbslcod_N));
         n1154AreaTrabalho_TipoPlanilha = (bool)(Convert.ToBoolean(obj106.gxTpr_Areatrabalho_tipoplanilha_N));
         n1588AreaTrabalho_SS_Codigo = (bool)(Convert.ToBoolean(obj106.gxTpr_Areatrabalho_ss_codigo_N));
         n2081AreaTrabalho_VerTA = (bool)(Convert.ToBoolean(obj106.gxTpr_Areatrabalho_verta_N));
         n2080AreaTrabalho_SelUsrPrestadora = (bool)(Convert.ToBoolean(obj106.gxTpr_Areatrabalho_selusrprestadora_N));
         Gx_mode = obj106.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A5AreaTrabalho_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey04106( ) ;
         ScanKeyStart04106( ) ;
         if ( RcdFound106 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
         }
         ZM04106( -10) ;
         OnLoadActions04106( ) ;
         AddRow04106( ) ;
         ScanKeyEnd04106( ) ;
         if ( RcdFound106 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars106( bcAreaTrabalho, 0) ;
         ScanKeyStart04106( ) ;
         if ( RcdFound106 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
         }
         ZM04106( -10) ;
         OnLoadActions04106( ) ;
         AddRow04106( ) ;
         ScanKeyEnd04106( ) ;
         if ( RcdFound106 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars106( bcAreaTrabalho, 0) ;
         nKeyPressed = 1;
         GetKey04106( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert04106( ) ;
         }
         else
         {
            if ( RcdFound106 == 1 )
            {
               if ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo )
               {
                  A5AreaTrabalho_Codigo = Z5AreaTrabalho_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update04106( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert04106( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert04106( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow106( bcAreaTrabalho) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars106( bcAreaTrabalho, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey04106( ) ;
         if ( RcdFound106 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo )
            {
               A5AreaTrabalho_Codigo = Z5AreaTrabalho_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(15);
         pr_default.close(17);
         pr_default.close(18);
         pr_default.close(19);
         pr_default.close(14);
         context.RollbackDataStores( "AreaTrabalho_BC");
         VarsToRow106( bcAreaTrabalho) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcAreaTrabalho.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcAreaTrabalho.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcAreaTrabalho )
         {
            bcAreaTrabalho = (SdtAreaTrabalho)(sdt);
            if ( StringUtil.StrCmp(bcAreaTrabalho.gxTpr_Mode, "") == 0 )
            {
               bcAreaTrabalho.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow106( bcAreaTrabalho) ;
            }
            else
            {
               RowToVars106( bcAreaTrabalho, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcAreaTrabalho.gxTpr_Mode, "") == 0 )
            {
               bcAreaTrabalho.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars106( bcAreaTrabalho, 1) ;
         return  ;
      }

      public SdtAreaTrabalho AreaTrabalho_BC
      {
         get {
            return bcAreaTrabalho ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(15);
         pr_default.close(17);
         pr_default.close(18);
         pr_default.close(19);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV40Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV39Causa = "";
         AV31ParametrosSistema = new SdtParametrosSistema(context);
         AV32Contratante_EmailSdaHost = "";
         AV33Contratante_EmailSdaUser = "";
         AV35Contratante_EmailSdaPass = "";
         AV37Contratante_EmailSdaAut = false;
         AV30AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV15Contratante_CNPJ = "";
         AV20Contratante_Email = "";
         AV23Contratante_Fax = "";
         AV18Contratante_IE = "";
         AV17Contratante_NomeFantasia = "";
         AV22Contratante_Ramal = "";
         AV16Contratante_RazaoSocial = "";
         AV21Contratante_Telefone = "";
         AV19Contratante_WebSite = "";
         AV24Estado_UF = "";
         Z6AreaTrabalho_Descricao = "";
         A6AreaTrabalho_Descricao = "";
         Z642AreaTrabalho_CalculoPFinal = "";
         A642AreaTrabalho_CalculoPFinal = "";
         Z33Contratante_Fax = "";
         A33Contratante_Fax = "";
         Z32Contratante_Ramal = "";
         A32Contratante_Ramal = "";
         Z31Contratante_Telefone = "";
         A31Contratante_Telefone = "";
         Z14Contratante_Email = "";
         A14Contratante_Email = "";
         Z13Contratante_WebSite = "";
         A13Contratante_WebSite = "";
         Z11Contratante_IE = "";
         A11Contratante_IE = "";
         Z10Contratante_NomeFantasia = "";
         A10Contratante_NomeFantasia = "";
         Z547Contratante_EmailSdaHost = "";
         A547Contratante_EmailSdaHost = "";
         Z548Contratante_EmailSdaUser = "";
         A548Contratante_EmailSdaUser = "";
         Z549Contratante_EmailSdaPass = "";
         A549Contratante_EmailSdaPass = "";
         Z550Contratante_EmailSdaKey = "";
         A550Contratante_EmailSdaKey = "";
         Z1214Organizacao_Nome = "";
         A1214Organizacao_Nome = "";
         Z26Municipio_Nome = "";
         A26Municipio_Nome = "";
         Z23Estado_UF = "";
         A23Estado_UF = "";
         Z24Estado_Nome = "";
         A24Estado_Nome = "";
         Z12Contratante_CNPJ = "";
         A12Contratante_CNPJ = "";
         Z9Contratante_RazaoSocial = "";
         A9Contratante_RazaoSocial = "";
         BC000413_A5AreaTrabalho_Codigo = new int[1] ;
         BC000413_A6AreaTrabalho_Descricao = new String[] {""} ;
         BC000413_A1214Organizacao_Nome = new String[] {""} ;
         BC000413_n1214Organizacao_Nome = new bool[] {false} ;
         BC000413_A24Estado_Nome = new String[] {""} ;
         BC000413_A26Municipio_Nome = new String[] {""} ;
         BC000413_A33Contratante_Fax = new String[] {""} ;
         BC000413_n33Contratante_Fax = new bool[] {false} ;
         BC000413_A32Contratante_Ramal = new String[] {""} ;
         BC000413_n32Contratante_Ramal = new bool[] {false} ;
         BC000413_A31Contratante_Telefone = new String[] {""} ;
         BC000413_A14Contratante_Email = new String[] {""} ;
         BC000413_n14Contratante_Email = new bool[] {false} ;
         BC000413_A13Contratante_WebSite = new String[] {""} ;
         BC000413_n13Contratante_WebSite = new bool[] {false} ;
         BC000413_A12Contratante_CNPJ = new String[] {""} ;
         BC000413_n12Contratante_CNPJ = new bool[] {false} ;
         BC000413_A11Contratante_IE = new String[] {""} ;
         BC000413_A10Contratante_NomeFantasia = new String[] {""} ;
         BC000413_A9Contratante_RazaoSocial = new String[] {""} ;
         BC000413_n9Contratante_RazaoSocial = new bool[] {false} ;
         BC000413_A547Contratante_EmailSdaHost = new String[] {""} ;
         BC000413_n547Contratante_EmailSdaHost = new bool[] {false} ;
         BC000413_A548Contratante_EmailSdaUser = new String[] {""} ;
         BC000413_n548Contratante_EmailSdaUser = new bool[] {false} ;
         BC000413_A549Contratante_EmailSdaPass = new String[] {""} ;
         BC000413_n549Contratante_EmailSdaPass = new bool[] {false} ;
         BC000413_A550Contratante_EmailSdaKey = new String[] {""} ;
         BC000413_n550Contratante_EmailSdaKey = new bool[] {false} ;
         BC000413_A551Contratante_EmailSdaAut = new bool[] {false} ;
         BC000413_n551Contratante_EmailSdaAut = new bool[] {false} ;
         BC000413_A552Contratante_EmailSdaPort = new short[1] ;
         BC000413_n552Contratante_EmailSdaPort = new bool[] {false} ;
         BC000413_A1048Contratante_EmailSdaSec = new short[1] ;
         BC000413_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         BC000413_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         BC000413_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         BC000413_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         BC000413_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         BC000413_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         BC000413_A987AreaTrabalho_ContratadaUpdBslCod = new int[1] ;
         BC000413_n987AreaTrabalho_ContratadaUpdBslCod = new bool[] {false} ;
         BC000413_A1154AreaTrabalho_TipoPlanilha = new short[1] ;
         BC000413_n1154AreaTrabalho_TipoPlanilha = new bool[] {false} ;
         BC000413_A72AreaTrabalho_Ativo = new bool[] {false} ;
         BC000413_A1588AreaTrabalho_SS_Codigo = new int[1] ;
         BC000413_n1588AreaTrabalho_SS_Codigo = new bool[] {false} ;
         BC000413_A2081AreaTrabalho_VerTA = new short[1] ;
         BC000413_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         BC000413_A2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         BC000413_n2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         BC000413_A29Contratante_Codigo = new int[1] ;
         BC000413_n29Contratante_Codigo = new bool[] {false} ;
         BC000413_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         BC000413_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         BC000413_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         BC000413_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         BC000413_A25Municipio_Codigo = new int[1] ;
         BC000413_n25Municipio_Codigo = new bool[] {false} ;
         BC000413_A23Estado_UF = new String[] {""} ;
         BC000413_A335Contratante_PessoaCod = new int[1] ;
         BC000413_A272AreaTrabalho_ContagensQtdGeral = new short[1] ;
         BC000413_n272AreaTrabalho_ContagensQtdGeral = new bool[] {false} ;
         BC000411_A272AreaTrabalho_ContagensQtdGeral = new short[1] ;
         BC000411_n272AreaTrabalho_ContagensQtdGeral = new bool[] {false} ;
         BC00046_A1214Organizacao_Nome = new String[] {""} ;
         BC00046_n1214Organizacao_Nome = new bool[] {false} ;
         BC00044_A33Contratante_Fax = new String[] {""} ;
         BC00044_n33Contratante_Fax = new bool[] {false} ;
         BC00044_A32Contratante_Ramal = new String[] {""} ;
         BC00044_n32Contratante_Ramal = new bool[] {false} ;
         BC00044_A31Contratante_Telefone = new String[] {""} ;
         BC00044_A14Contratante_Email = new String[] {""} ;
         BC00044_n14Contratante_Email = new bool[] {false} ;
         BC00044_A13Contratante_WebSite = new String[] {""} ;
         BC00044_n13Contratante_WebSite = new bool[] {false} ;
         BC00044_A11Contratante_IE = new String[] {""} ;
         BC00044_A10Contratante_NomeFantasia = new String[] {""} ;
         BC00044_A547Contratante_EmailSdaHost = new String[] {""} ;
         BC00044_n547Contratante_EmailSdaHost = new bool[] {false} ;
         BC00044_A548Contratante_EmailSdaUser = new String[] {""} ;
         BC00044_n548Contratante_EmailSdaUser = new bool[] {false} ;
         BC00044_A549Contratante_EmailSdaPass = new String[] {""} ;
         BC00044_n549Contratante_EmailSdaPass = new bool[] {false} ;
         BC00044_A550Contratante_EmailSdaKey = new String[] {""} ;
         BC00044_n550Contratante_EmailSdaKey = new bool[] {false} ;
         BC00044_A551Contratante_EmailSdaAut = new bool[] {false} ;
         BC00044_n551Contratante_EmailSdaAut = new bool[] {false} ;
         BC00044_A552Contratante_EmailSdaPort = new short[1] ;
         BC00044_n552Contratante_EmailSdaPort = new bool[] {false} ;
         BC00044_A1048Contratante_EmailSdaSec = new short[1] ;
         BC00044_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         BC00044_A25Municipio_Codigo = new int[1] ;
         BC00044_n25Municipio_Codigo = new bool[] {false} ;
         BC00044_A335Contratante_PessoaCod = new int[1] ;
         BC00047_A26Municipio_Nome = new String[] {""} ;
         BC00047_A23Estado_UF = new String[] {""} ;
         BC00048_A24Estado_Nome = new String[] {""} ;
         BC00049_A12Contratante_CNPJ = new String[] {""} ;
         BC00049_n12Contratante_CNPJ = new bool[] {false} ;
         BC00049_A9Contratante_RazaoSocial = new String[] {""} ;
         BC00049_n9Contratante_RazaoSocial = new bool[] {false} ;
         BC00045_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         BC00045_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         BC000414_A5AreaTrabalho_Codigo = new int[1] ;
         BC00043_A5AreaTrabalho_Codigo = new int[1] ;
         BC00043_A6AreaTrabalho_Descricao = new String[] {""} ;
         BC00043_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         BC00043_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         BC00043_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         BC00043_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         BC00043_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         BC00043_A987AreaTrabalho_ContratadaUpdBslCod = new int[1] ;
         BC00043_n987AreaTrabalho_ContratadaUpdBslCod = new bool[] {false} ;
         BC00043_A1154AreaTrabalho_TipoPlanilha = new short[1] ;
         BC00043_n1154AreaTrabalho_TipoPlanilha = new bool[] {false} ;
         BC00043_A72AreaTrabalho_Ativo = new bool[] {false} ;
         BC00043_A1588AreaTrabalho_SS_Codigo = new int[1] ;
         BC00043_n1588AreaTrabalho_SS_Codigo = new bool[] {false} ;
         BC00043_A2081AreaTrabalho_VerTA = new short[1] ;
         BC00043_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         BC00043_A2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         BC00043_n2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         BC00043_A29Contratante_Codigo = new int[1] ;
         BC00043_n29Contratante_Codigo = new bool[] {false} ;
         BC00043_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         BC00043_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         BC00043_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         BC00043_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         sMode106 = "";
         BC00042_A5AreaTrabalho_Codigo = new int[1] ;
         BC00042_A6AreaTrabalho_Descricao = new String[] {""} ;
         BC00042_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         BC00042_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         BC00042_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         BC00042_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         BC00042_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         BC00042_A987AreaTrabalho_ContratadaUpdBslCod = new int[1] ;
         BC00042_n987AreaTrabalho_ContratadaUpdBslCod = new bool[] {false} ;
         BC00042_A1154AreaTrabalho_TipoPlanilha = new short[1] ;
         BC00042_n1154AreaTrabalho_TipoPlanilha = new bool[] {false} ;
         BC00042_A72AreaTrabalho_Ativo = new bool[] {false} ;
         BC00042_A1588AreaTrabalho_SS_Codigo = new int[1] ;
         BC00042_n1588AreaTrabalho_SS_Codigo = new bool[] {false} ;
         BC00042_A2081AreaTrabalho_VerTA = new short[1] ;
         BC00042_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         BC00042_A2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         BC00042_n2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         BC00042_A29Contratante_Codigo = new int[1] ;
         BC00042_n29Contratante_Codigo = new bool[] {false} ;
         BC00042_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         BC00042_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         BC00042_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         BC00042_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         BC000415_A5AreaTrabalho_Codigo = new int[1] ;
         BC000419_A272AreaTrabalho_ContagensQtdGeral = new short[1] ;
         BC000419_n272AreaTrabalho_ContagensQtdGeral = new bool[] {false} ;
         BC000420_A1214Organizacao_Nome = new String[] {""} ;
         BC000420_n1214Organizacao_Nome = new bool[] {false} ;
         BC000421_A33Contratante_Fax = new String[] {""} ;
         BC000421_n33Contratante_Fax = new bool[] {false} ;
         BC000421_A32Contratante_Ramal = new String[] {""} ;
         BC000421_n32Contratante_Ramal = new bool[] {false} ;
         BC000421_A31Contratante_Telefone = new String[] {""} ;
         BC000421_A14Contratante_Email = new String[] {""} ;
         BC000421_n14Contratante_Email = new bool[] {false} ;
         BC000421_A13Contratante_WebSite = new String[] {""} ;
         BC000421_n13Contratante_WebSite = new bool[] {false} ;
         BC000421_A11Contratante_IE = new String[] {""} ;
         BC000421_A10Contratante_NomeFantasia = new String[] {""} ;
         BC000421_A547Contratante_EmailSdaHost = new String[] {""} ;
         BC000421_n547Contratante_EmailSdaHost = new bool[] {false} ;
         BC000421_A548Contratante_EmailSdaUser = new String[] {""} ;
         BC000421_n548Contratante_EmailSdaUser = new bool[] {false} ;
         BC000421_A549Contratante_EmailSdaPass = new String[] {""} ;
         BC000421_n549Contratante_EmailSdaPass = new bool[] {false} ;
         BC000421_A550Contratante_EmailSdaKey = new String[] {""} ;
         BC000421_n550Contratante_EmailSdaKey = new bool[] {false} ;
         BC000421_A551Contratante_EmailSdaAut = new bool[] {false} ;
         BC000421_n551Contratante_EmailSdaAut = new bool[] {false} ;
         BC000421_A552Contratante_EmailSdaPort = new short[1] ;
         BC000421_n552Contratante_EmailSdaPort = new bool[] {false} ;
         BC000421_A1048Contratante_EmailSdaSec = new short[1] ;
         BC000421_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         BC000421_A25Municipio_Codigo = new int[1] ;
         BC000421_n25Municipio_Codigo = new bool[] {false} ;
         BC000421_A335Contratante_PessoaCod = new int[1] ;
         BC000422_A26Municipio_Nome = new String[] {""} ;
         BC000422_A23Estado_UF = new String[] {""} ;
         BC000423_A24Estado_Nome = new String[] {""} ;
         BC000424_A12Contratante_CNPJ = new String[] {""} ;
         BC000424_n12Contratante_CNPJ = new bool[] {false} ;
         BC000424_A9Contratante_RazaoSocial = new String[] {""} ;
         BC000424_n9Contratante_RazaoSocial = new bool[] {false} ;
         BC000425_A619GrupoFuncao_Codigo = new int[1] ;
         BC000426_A2175Equipe_Codigo = new int[1] ;
         BC000427_A648Projeto_Codigo = new int[1] ;
         BC000428_A2077UsuarioNotifica_Codigo = new int[1] ;
         BC000429_A2058Indicador_Codigo = new int[1] ;
         BC000430_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC000431_A1795CatalogoServico_Codigo = new int[1] ;
         BC000432_A1482Gestao_Codigo = new int[1] ;
         BC000433_A1347Glosario_Codigo = new int[1] ;
         BC000434_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         BC000434_A962VariavelCocomo_Sigla = new String[] {""} ;
         BC000434_A964VariavelCocomo_Tipo = new String[] {""} ;
         BC000434_A992VariavelCocomo_Sequencial = new short[1] ;
         BC000435_A860RegrasContagem_Regra = new String[] {""} ;
         BC000436_A848ParametrosPln_Codigo = new int[1] ;
         BC000437_A351AmbienteTecnologico_Codigo = new int[1] ;
         BC000438_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         BC000438_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         BC000439_A709ReferenciaINM_Codigo = new int[1] ;
         BC000440_A596Lote_Codigo = new int[1] ;
         BC000441_A426NaoConformidade_Codigo = new int[1] ;
         BC000442_A423Status_Codigo = new int[1] ;
         BC000443_A93Guia_Codigo = new int[1] ;
         BC000444_A192Contagem_Codigo = new int[1] ;
         BC000445_A127Sistema_Codigo = new int[1] ;
         BC000446_A74Contrato_Codigo = new int[1] ;
         BC000447_A39Contratada_Codigo = new int[1] ;
         BC000448_A3Perfil_Codigo = new int[1] ;
         BC000449_A1685Proposta_Codigo = new int[1] ;
         BC000450_A5AreaTrabalho_Codigo = new int[1] ;
         BC000450_A2127FatoresImpacto_Codigo = new int[1] ;
         BC000451_A5AreaTrabalho_Codigo = new int[1] ;
         BC000451_A93Guia_Codigo = new int[1] ;
         BC000453_A5AreaTrabalho_Codigo = new int[1] ;
         BC000453_A6AreaTrabalho_Descricao = new String[] {""} ;
         BC000453_A1214Organizacao_Nome = new String[] {""} ;
         BC000453_n1214Organizacao_Nome = new bool[] {false} ;
         BC000453_A24Estado_Nome = new String[] {""} ;
         BC000453_A26Municipio_Nome = new String[] {""} ;
         BC000453_A33Contratante_Fax = new String[] {""} ;
         BC000453_n33Contratante_Fax = new bool[] {false} ;
         BC000453_A32Contratante_Ramal = new String[] {""} ;
         BC000453_n32Contratante_Ramal = new bool[] {false} ;
         BC000453_A31Contratante_Telefone = new String[] {""} ;
         BC000453_A14Contratante_Email = new String[] {""} ;
         BC000453_n14Contratante_Email = new bool[] {false} ;
         BC000453_A13Contratante_WebSite = new String[] {""} ;
         BC000453_n13Contratante_WebSite = new bool[] {false} ;
         BC000453_A12Contratante_CNPJ = new String[] {""} ;
         BC000453_n12Contratante_CNPJ = new bool[] {false} ;
         BC000453_A11Contratante_IE = new String[] {""} ;
         BC000453_A10Contratante_NomeFantasia = new String[] {""} ;
         BC000453_A9Contratante_RazaoSocial = new String[] {""} ;
         BC000453_n9Contratante_RazaoSocial = new bool[] {false} ;
         BC000453_A547Contratante_EmailSdaHost = new String[] {""} ;
         BC000453_n547Contratante_EmailSdaHost = new bool[] {false} ;
         BC000453_A548Contratante_EmailSdaUser = new String[] {""} ;
         BC000453_n548Contratante_EmailSdaUser = new bool[] {false} ;
         BC000453_A549Contratante_EmailSdaPass = new String[] {""} ;
         BC000453_n549Contratante_EmailSdaPass = new bool[] {false} ;
         BC000453_A550Contratante_EmailSdaKey = new String[] {""} ;
         BC000453_n550Contratante_EmailSdaKey = new bool[] {false} ;
         BC000453_A551Contratante_EmailSdaAut = new bool[] {false} ;
         BC000453_n551Contratante_EmailSdaAut = new bool[] {false} ;
         BC000453_A552Contratante_EmailSdaPort = new short[1] ;
         BC000453_n552Contratante_EmailSdaPort = new bool[] {false} ;
         BC000453_A1048Contratante_EmailSdaSec = new short[1] ;
         BC000453_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         BC000453_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         BC000453_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         BC000453_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         BC000453_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         BC000453_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         BC000453_A987AreaTrabalho_ContratadaUpdBslCod = new int[1] ;
         BC000453_n987AreaTrabalho_ContratadaUpdBslCod = new bool[] {false} ;
         BC000453_A1154AreaTrabalho_TipoPlanilha = new short[1] ;
         BC000453_n1154AreaTrabalho_TipoPlanilha = new bool[] {false} ;
         BC000453_A72AreaTrabalho_Ativo = new bool[] {false} ;
         BC000453_A1588AreaTrabalho_SS_Codigo = new int[1] ;
         BC000453_n1588AreaTrabalho_SS_Codigo = new bool[] {false} ;
         BC000453_A2081AreaTrabalho_VerTA = new short[1] ;
         BC000453_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         BC000453_A2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         BC000453_n2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         BC000453_A29Contratante_Codigo = new int[1] ;
         BC000453_n29Contratante_Codigo = new bool[] {false} ;
         BC000453_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         BC000453_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         BC000453_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         BC000453_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         BC000453_A25Municipio_Codigo = new int[1] ;
         BC000453_n25Municipio_Codigo = new bool[] {false} ;
         BC000453_A23Estado_UF = new String[] {""} ;
         BC000453_A335Contratante_PessoaCod = new int[1] ;
         BC000453_A272AreaTrabalho_ContagensQtdGeral = new short[1] ;
         BC000453_n272AreaTrabalho_ContagensQtdGeral = new bool[] {false} ;
         i642AreaTrabalho_CalculoPFinal = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.areatrabalho_bc__default(),
            new Object[][] {
                new Object[] {
               BC00042_A5AreaTrabalho_Codigo, BC00042_A6AreaTrabalho_Descricao, BC00042_A642AreaTrabalho_CalculoPFinal, BC00042_A834AreaTrabalho_ValidaOSFM, BC00042_n834AreaTrabalho_ValidaOSFM, BC00042_A855AreaTrabalho_DiasParaPagar, BC00042_n855AreaTrabalho_DiasParaPagar, BC00042_A987AreaTrabalho_ContratadaUpdBslCod, BC00042_n987AreaTrabalho_ContratadaUpdBslCod, BC00042_A1154AreaTrabalho_TipoPlanilha,
               BC00042_n1154AreaTrabalho_TipoPlanilha, BC00042_A72AreaTrabalho_Ativo, BC00042_A1588AreaTrabalho_SS_Codigo, BC00042_n1588AreaTrabalho_SS_Codigo, BC00042_A2081AreaTrabalho_VerTA, BC00042_n2081AreaTrabalho_VerTA, BC00042_A2080AreaTrabalho_SelUsrPrestadora, BC00042_n2080AreaTrabalho_SelUsrPrestadora, BC00042_A29Contratante_Codigo, BC00042_n29Contratante_Codigo,
               BC00042_A830AreaTrabalho_ServicoPadrao, BC00042_n830AreaTrabalho_ServicoPadrao, BC00042_A1216AreaTrabalho_OrganizacaoCod, BC00042_n1216AreaTrabalho_OrganizacaoCod
               }
               , new Object[] {
               BC00043_A5AreaTrabalho_Codigo, BC00043_A6AreaTrabalho_Descricao, BC00043_A642AreaTrabalho_CalculoPFinal, BC00043_A834AreaTrabalho_ValidaOSFM, BC00043_n834AreaTrabalho_ValidaOSFM, BC00043_A855AreaTrabalho_DiasParaPagar, BC00043_n855AreaTrabalho_DiasParaPagar, BC00043_A987AreaTrabalho_ContratadaUpdBslCod, BC00043_n987AreaTrabalho_ContratadaUpdBslCod, BC00043_A1154AreaTrabalho_TipoPlanilha,
               BC00043_n1154AreaTrabalho_TipoPlanilha, BC00043_A72AreaTrabalho_Ativo, BC00043_A1588AreaTrabalho_SS_Codigo, BC00043_n1588AreaTrabalho_SS_Codigo, BC00043_A2081AreaTrabalho_VerTA, BC00043_n2081AreaTrabalho_VerTA, BC00043_A2080AreaTrabalho_SelUsrPrestadora, BC00043_n2080AreaTrabalho_SelUsrPrestadora, BC00043_A29Contratante_Codigo, BC00043_n29Contratante_Codigo,
               BC00043_A830AreaTrabalho_ServicoPadrao, BC00043_n830AreaTrabalho_ServicoPadrao, BC00043_A1216AreaTrabalho_OrganizacaoCod, BC00043_n1216AreaTrabalho_OrganizacaoCod
               }
               , new Object[] {
               BC00044_A33Contratante_Fax, BC00044_n33Contratante_Fax, BC00044_A32Contratante_Ramal, BC00044_n32Contratante_Ramal, BC00044_A31Contratante_Telefone, BC00044_A14Contratante_Email, BC00044_n14Contratante_Email, BC00044_A13Contratante_WebSite, BC00044_n13Contratante_WebSite, BC00044_A11Contratante_IE,
               BC00044_A10Contratante_NomeFantasia, BC00044_A547Contratante_EmailSdaHost, BC00044_n547Contratante_EmailSdaHost, BC00044_A548Contratante_EmailSdaUser, BC00044_n548Contratante_EmailSdaUser, BC00044_A549Contratante_EmailSdaPass, BC00044_n549Contratante_EmailSdaPass, BC00044_A550Contratante_EmailSdaKey, BC00044_n550Contratante_EmailSdaKey, BC00044_A551Contratante_EmailSdaAut,
               BC00044_n551Contratante_EmailSdaAut, BC00044_A552Contratante_EmailSdaPort, BC00044_n552Contratante_EmailSdaPort, BC00044_A1048Contratante_EmailSdaSec, BC00044_n1048Contratante_EmailSdaSec, BC00044_A25Municipio_Codigo, BC00044_n25Municipio_Codigo, BC00044_A335Contratante_PessoaCod
               }
               , new Object[] {
               BC00045_A830AreaTrabalho_ServicoPadrao
               }
               , new Object[] {
               BC00046_A1214Organizacao_Nome, BC00046_n1214Organizacao_Nome
               }
               , new Object[] {
               BC00047_A26Municipio_Nome, BC00047_A23Estado_UF
               }
               , new Object[] {
               BC00048_A24Estado_Nome
               }
               , new Object[] {
               BC00049_A12Contratante_CNPJ, BC00049_n12Contratante_CNPJ, BC00049_A9Contratante_RazaoSocial, BC00049_n9Contratante_RazaoSocial
               }
               , new Object[] {
               BC000411_A272AreaTrabalho_ContagensQtdGeral, BC000411_n272AreaTrabalho_ContagensQtdGeral
               }
               , new Object[] {
               BC000413_A5AreaTrabalho_Codigo, BC000413_A6AreaTrabalho_Descricao, BC000413_A1214Organizacao_Nome, BC000413_n1214Organizacao_Nome, BC000413_A24Estado_Nome, BC000413_A26Municipio_Nome, BC000413_A33Contratante_Fax, BC000413_n33Contratante_Fax, BC000413_A32Contratante_Ramal, BC000413_n32Contratante_Ramal,
               BC000413_A31Contratante_Telefone, BC000413_A14Contratante_Email, BC000413_n14Contratante_Email, BC000413_A13Contratante_WebSite, BC000413_n13Contratante_WebSite, BC000413_A12Contratante_CNPJ, BC000413_n12Contratante_CNPJ, BC000413_A11Contratante_IE, BC000413_A10Contratante_NomeFantasia, BC000413_A9Contratante_RazaoSocial,
               BC000413_n9Contratante_RazaoSocial, BC000413_A547Contratante_EmailSdaHost, BC000413_n547Contratante_EmailSdaHost, BC000413_A548Contratante_EmailSdaUser, BC000413_n548Contratante_EmailSdaUser, BC000413_A549Contratante_EmailSdaPass, BC000413_n549Contratante_EmailSdaPass, BC000413_A550Contratante_EmailSdaKey, BC000413_n550Contratante_EmailSdaKey, BC000413_A551Contratante_EmailSdaAut,
               BC000413_n551Contratante_EmailSdaAut, BC000413_A552Contratante_EmailSdaPort, BC000413_n552Contratante_EmailSdaPort, BC000413_A1048Contratante_EmailSdaSec, BC000413_n1048Contratante_EmailSdaSec, BC000413_A642AreaTrabalho_CalculoPFinal, BC000413_A834AreaTrabalho_ValidaOSFM, BC000413_n834AreaTrabalho_ValidaOSFM, BC000413_A855AreaTrabalho_DiasParaPagar, BC000413_n855AreaTrabalho_DiasParaPagar,
               BC000413_A987AreaTrabalho_ContratadaUpdBslCod, BC000413_n987AreaTrabalho_ContratadaUpdBslCod, BC000413_A1154AreaTrabalho_TipoPlanilha, BC000413_n1154AreaTrabalho_TipoPlanilha, BC000413_A72AreaTrabalho_Ativo, BC000413_A1588AreaTrabalho_SS_Codigo, BC000413_n1588AreaTrabalho_SS_Codigo, BC000413_A2081AreaTrabalho_VerTA, BC000413_n2081AreaTrabalho_VerTA, BC000413_A2080AreaTrabalho_SelUsrPrestadora,
               BC000413_n2080AreaTrabalho_SelUsrPrestadora, BC000413_A29Contratante_Codigo, BC000413_n29Contratante_Codigo, BC000413_A830AreaTrabalho_ServicoPadrao, BC000413_n830AreaTrabalho_ServicoPadrao, BC000413_A1216AreaTrabalho_OrganizacaoCod, BC000413_n1216AreaTrabalho_OrganizacaoCod, BC000413_A25Municipio_Codigo, BC000413_n25Municipio_Codigo, BC000413_A23Estado_UF,
               BC000413_A335Contratante_PessoaCod, BC000413_A272AreaTrabalho_ContagensQtdGeral, BC000413_n272AreaTrabalho_ContagensQtdGeral
               }
               , new Object[] {
               BC000414_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               BC000415_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000419_A272AreaTrabalho_ContagensQtdGeral, BC000419_n272AreaTrabalho_ContagensQtdGeral
               }
               , new Object[] {
               BC000420_A1214Organizacao_Nome, BC000420_n1214Organizacao_Nome
               }
               , new Object[] {
               BC000421_A33Contratante_Fax, BC000421_n33Contratante_Fax, BC000421_A32Contratante_Ramal, BC000421_n32Contratante_Ramal, BC000421_A31Contratante_Telefone, BC000421_A14Contratante_Email, BC000421_n14Contratante_Email, BC000421_A13Contratante_WebSite, BC000421_n13Contratante_WebSite, BC000421_A11Contratante_IE,
               BC000421_A10Contratante_NomeFantasia, BC000421_A547Contratante_EmailSdaHost, BC000421_n547Contratante_EmailSdaHost, BC000421_A548Contratante_EmailSdaUser, BC000421_n548Contratante_EmailSdaUser, BC000421_A549Contratante_EmailSdaPass, BC000421_n549Contratante_EmailSdaPass, BC000421_A550Contratante_EmailSdaKey, BC000421_n550Contratante_EmailSdaKey, BC000421_A551Contratante_EmailSdaAut,
               BC000421_n551Contratante_EmailSdaAut, BC000421_A552Contratante_EmailSdaPort, BC000421_n552Contratante_EmailSdaPort, BC000421_A1048Contratante_EmailSdaSec, BC000421_n1048Contratante_EmailSdaSec, BC000421_A25Municipio_Codigo, BC000421_n25Municipio_Codigo, BC000421_A335Contratante_PessoaCod
               }
               , new Object[] {
               BC000422_A26Municipio_Nome, BC000422_A23Estado_UF
               }
               , new Object[] {
               BC000423_A24Estado_Nome
               }
               , new Object[] {
               BC000424_A12Contratante_CNPJ, BC000424_n12Contratante_CNPJ, BC000424_A9Contratante_RazaoSocial, BC000424_n9Contratante_RazaoSocial
               }
               , new Object[] {
               BC000425_A619GrupoFuncao_Codigo
               }
               , new Object[] {
               BC000426_A2175Equipe_Codigo
               }
               , new Object[] {
               BC000427_A648Projeto_Codigo
               }
               , new Object[] {
               BC000428_A2077UsuarioNotifica_Codigo
               }
               , new Object[] {
               BC000429_A2058Indicador_Codigo
               }
               , new Object[] {
               BC000430_A1412ContagemResultadoNotificacao_Codigo
               }
               , new Object[] {
               BC000431_A1795CatalogoServico_Codigo
               }
               , new Object[] {
               BC000432_A1482Gestao_Codigo
               }
               , new Object[] {
               BC000433_A1347Glosario_Codigo
               }
               , new Object[] {
               BC000434_A961VariavelCocomo_AreaTrabalhoCod, BC000434_A962VariavelCocomo_Sigla, BC000434_A964VariavelCocomo_Tipo, BC000434_A992VariavelCocomo_Sequencial
               }
               , new Object[] {
               BC000435_A860RegrasContagem_Regra
               }
               , new Object[] {
               BC000436_A848ParametrosPln_Codigo
               }
               , new Object[] {
               BC000437_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               BC000438_A718ItemNaoMensuravel_AreaTrabalhoCod, BC000438_A715ItemNaoMensuravel_Codigo
               }
               , new Object[] {
               BC000439_A709ReferenciaINM_Codigo
               }
               , new Object[] {
               BC000440_A596Lote_Codigo
               }
               , new Object[] {
               BC000441_A426NaoConformidade_Codigo
               }
               , new Object[] {
               BC000442_A423Status_Codigo
               }
               , new Object[] {
               BC000443_A93Guia_Codigo
               }
               , new Object[] {
               BC000444_A192Contagem_Codigo
               }
               , new Object[] {
               BC000445_A127Sistema_Codigo
               }
               , new Object[] {
               BC000446_A74Contrato_Codigo
               }
               , new Object[] {
               BC000447_A39Contratada_Codigo
               }
               , new Object[] {
               BC000448_A3Perfil_Codigo
               }
               , new Object[] {
               BC000449_A1685Proposta_Codigo
               }
               , new Object[] {
               BC000450_A5AreaTrabalho_Codigo, BC000450_A2127FatoresImpacto_Codigo
               }
               , new Object[] {
               BC000451_A5AreaTrabalho_Codigo, BC000451_A93Guia_Codigo
               }
               , new Object[] {
               BC000453_A5AreaTrabalho_Codigo, BC000453_A6AreaTrabalho_Descricao, BC000453_A1214Organizacao_Nome, BC000453_n1214Organizacao_Nome, BC000453_A24Estado_Nome, BC000453_A26Municipio_Nome, BC000453_A33Contratante_Fax, BC000453_n33Contratante_Fax, BC000453_A32Contratante_Ramal, BC000453_n32Contratante_Ramal,
               BC000453_A31Contratante_Telefone, BC000453_A14Contratante_Email, BC000453_n14Contratante_Email, BC000453_A13Contratante_WebSite, BC000453_n13Contratante_WebSite, BC000453_A12Contratante_CNPJ, BC000453_n12Contratante_CNPJ, BC000453_A11Contratante_IE, BC000453_A10Contratante_NomeFantasia, BC000453_A9Contratante_RazaoSocial,
               BC000453_n9Contratante_RazaoSocial, BC000453_A547Contratante_EmailSdaHost, BC000453_n547Contratante_EmailSdaHost, BC000453_A548Contratante_EmailSdaUser, BC000453_n548Contratante_EmailSdaUser, BC000453_A549Contratante_EmailSdaPass, BC000453_n549Contratante_EmailSdaPass, BC000453_A550Contratante_EmailSdaKey, BC000453_n550Contratante_EmailSdaKey, BC000453_A551Contratante_EmailSdaAut,
               BC000453_n551Contratante_EmailSdaAut, BC000453_A552Contratante_EmailSdaPort, BC000453_n552Contratante_EmailSdaPort, BC000453_A1048Contratante_EmailSdaSec, BC000453_n1048Contratante_EmailSdaSec, BC000453_A642AreaTrabalho_CalculoPFinal, BC000453_A834AreaTrabalho_ValidaOSFM, BC000453_n834AreaTrabalho_ValidaOSFM, BC000453_A855AreaTrabalho_DiasParaPagar, BC000453_n855AreaTrabalho_DiasParaPagar,
               BC000453_A987AreaTrabalho_ContratadaUpdBslCod, BC000453_n987AreaTrabalho_ContratadaUpdBslCod, BC000453_A1154AreaTrabalho_TipoPlanilha, BC000453_n1154AreaTrabalho_TipoPlanilha, BC000453_A72AreaTrabalho_Ativo, BC000453_A1588AreaTrabalho_SS_Codigo, BC000453_n1588AreaTrabalho_SS_Codigo, BC000453_A2081AreaTrabalho_VerTA, BC000453_n2081AreaTrabalho_VerTA, BC000453_A2080AreaTrabalho_SelUsrPrestadora,
               BC000453_n2080AreaTrabalho_SelUsrPrestadora, BC000453_A29Contratante_Codigo, BC000453_n29Contratante_Codigo, BC000453_A830AreaTrabalho_ServicoPadrao, BC000453_n830AreaTrabalho_ServicoPadrao, BC000453_A1216AreaTrabalho_OrganizacaoCod, BC000453_n1216AreaTrabalho_OrganizacaoCod, BC000453_A25Municipio_Codigo, BC000453_n25Municipio_Codigo, BC000453_A23Estado_UF,
               BC000453_A335Contratante_PessoaCod, BC000453_A272AreaTrabalho_ContagensQtdGeral, BC000453_n272AreaTrabalho_ContagensQtdGeral
               }
            }
         );
         Z72AreaTrabalho_Ativo = true;
         A72AreaTrabalho_Ativo = true;
         i72AreaTrabalho_Ativo = true;
         Z642AreaTrabalho_CalculoPFinal = "MB";
         A642AreaTrabalho_CalculoPFinal = "MB";
         i642AreaTrabalho_CalculoPFinal = "MB";
         AV40Pgmname = "AreaTrabalho_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12042 */
         E12042 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short AV36Contratante_EmailSdaPort ;
      private short AV34Contratante_EmailSdaSec ;
      private short GX_JID ;
      private short Z855AreaTrabalho_DiasParaPagar ;
      private short A855AreaTrabalho_DiasParaPagar ;
      private short Z1154AreaTrabalho_TipoPlanilha ;
      private short A1154AreaTrabalho_TipoPlanilha ;
      private short Z2081AreaTrabalho_VerTA ;
      private short A2081AreaTrabalho_VerTA ;
      private short Z272AreaTrabalho_ContagensQtdGeral ;
      private short A272AreaTrabalho_ContagensQtdGeral ;
      private short Z552Contratante_EmailSdaPort ;
      private short A552Contratante_EmailSdaPort ;
      private short Z1048Contratante_EmailSdaSec ;
      private short A1048Contratante_EmailSdaSec ;
      private short Gx_BScreen ;
      private short RcdFound106 ;
      private int trnEnded ;
      private int Z5AreaTrabalho_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int AV41GXV1 ;
      private int AV29Insert_AreaTrabalho_OrganizacaoCod ;
      private int AV11Insert_Contratante_Codigo ;
      private int AV27Insert_AreaTrabalho_ServicoPadrao ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int AV26Contratante_Codigo ;
      private int GXt_int1 ;
      private int AV25Municipio_Codigo ;
      private int Z987AreaTrabalho_ContratadaUpdBslCod ;
      private int A987AreaTrabalho_ContratadaUpdBslCod ;
      private int Z1588AreaTrabalho_SS_Codigo ;
      private int A1588AreaTrabalho_SS_Codigo ;
      private int Z29Contratante_Codigo ;
      private int A29Contratante_Codigo ;
      private int Z830AreaTrabalho_ServicoPadrao ;
      private int Z1216AreaTrabalho_OrganizacaoCod ;
      private int A1216AreaTrabalho_OrganizacaoCod ;
      private int Z25Municipio_Codigo ;
      private int A25Municipio_Codigo ;
      private int Z335Contratante_PessoaCod ;
      private int A335Contratante_PessoaCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV40Pgmname ;
      private String AV39Causa ;
      private String AV23Contratante_Fax ;
      private String AV18Contratante_IE ;
      private String AV17Contratante_NomeFantasia ;
      private String AV22Contratante_Ramal ;
      private String AV16Contratante_RazaoSocial ;
      private String AV21Contratante_Telefone ;
      private String AV24Estado_UF ;
      private String Z642AreaTrabalho_CalculoPFinal ;
      private String A642AreaTrabalho_CalculoPFinal ;
      private String Z33Contratante_Fax ;
      private String A33Contratante_Fax ;
      private String Z32Contratante_Ramal ;
      private String A32Contratante_Ramal ;
      private String Z31Contratante_Telefone ;
      private String A31Contratante_Telefone ;
      private String Z11Contratante_IE ;
      private String A11Contratante_IE ;
      private String Z10Contratante_NomeFantasia ;
      private String A10Contratante_NomeFantasia ;
      private String Z550Contratante_EmailSdaKey ;
      private String A550Contratante_EmailSdaKey ;
      private String Z1214Organizacao_Nome ;
      private String A1214Organizacao_Nome ;
      private String Z26Municipio_Nome ;
      private String A26Municipio_Nome ;
      private String Z23Estado_UF ;
      private String A23Estado_UF ;
      private String Z24Estado_Nome ;
      private String A24Estado_Nome ;
      private String Z9Contratante_RazaoSocial ;
      private String A9Contratante_RazaoSocial ;
      private String sMode106 ;
      private String i642AreaTrabalho_CalculoPFinal ;
      private bool AV37Contratante_EmailSdaAut ;
      private bool Z834AreaTrabalho_ValidaOSFM ;
      private bool A834AreaTrabalho_ValidaOSFM ;
      private bool Z72AreaTrabalho_Ativo ;
      private bool A72AreaTrabalho_Ativo ;
      private bool Z2080AreaTrabalho_SelUsrPrestadora ;
      private bool A2080AreaTrabalho_SelUsrPrestadora ;
      private bool Z551Contratante_EmailSdaAut ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n1214Organizacao_Nome ;
      private bool n33Contratante_Fax ;
      private bool n32Contratante_Ramal ;
      private bool n14Contratante_Email ;
      private bool n13Contratante_WebSite ;
      private bool n12Contratante_CNPJ ;
      private bool n9Contratante_RazaoSocial ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n549Contratante_EmailSdaPass ;
      private bool n550Contratante_EmailSdaKey ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n552Contratante_EmailSdaPort ;
      private bool n1048Contratante_EmailSdaSec ;
      private bool n834AreaTrabalho_ValidaOSFM ;
      private bool n855AreaTrabalho_DiasParaPagar ;
      private bool n987AreaTrabalho_ContratadaUpdBslCod ;
      private bool n1154AreaTrabalho_TipoPlanilha ;
      private bool n1588AreaTrabalho_SS_Codigo ;
      private bool n2081AreaTrabalho_VerTA ;
      private bool n2080AreaTrabalho_SelUsrPrestadora ;
      private bool n29Contratante_Codigo ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool n1216AreaTrabalho_OrganizacaoCod ;
      private bool n25Municipio_Codigo ;
      private bool n272AreaTrabalho_ContagensQtdGeral ;
      private bool Gx_longc ;
      private bool i72AreaTrabalho_Ativo ;
      private String AV32Contratante_EmailSdaHost ;
      private String AV33Contratante_EmailSdaUser ;
      private String AV35Contratante_EmailSdaPass ;
      private String AV15Contratante_CNPJ ;
      private String AV20Contratante_Email ;
      private String AV19Contratante_WebSite ;
      private String Z6AreaTrabalho_Descricao ;
      private String A6AreaTrabalho_Descricao ;
      private String Z14Contratante_Email ;
      private String A14Contratante_Email ;
      private String Z13Contratante_WebSite ;
      private String A13Contratante_WebSite ;
      private String Z547Contratante_EmailSdaHost ;
      private String A547Contratante_EmailSdaHost ;
      private String Z548Contratante_EmailSdaUser ;
      private String A548Contratante_EmailSdaUser ;
      private String Z549Contratante_EmailSdaPass ;
      private String A549Contratante_EmailSdaPass ;
      private String Z12Contratante_CNPJ ;
      private String A12Contratante_CNPJ ;
      private IGxSession AV10WebSession ;
      private SdtAreaTrabalho bcAreaTrabalho ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC000413_A5AreaTrabalho_Codigo ;
      private String[] BC000413_A6AreaTrabalho_Descricao ;
      private String[] BC000413_A1214Organizacao_Nome ;
      private bool[] BC000413_n1214Organizacao_Nome ;
      private String[] BC000413_A24Estado_Nome ;
      private String[] BC000413_A26Municipio_Nome ;
      private String[] BC000413_A33Contratante_Fax ;
      private bool[] BC000413_n33Contratante_Fax ;
      private String[] BC000413_A32Contratante_Ramal ;
      private bool[] BC000413_n32Contratante_Ramal ;
      private String[] BC000413_A31Contratante_Telefone ;
      private String[] BC000413_A14Contratante_Email ;
      private bool[] BC000413_n14Contratante_Email ;
      private String[] BC000413_A13Contratante_WebSite ;
      private bool[] BC000413_n13Contratante_WebSite ;
      private String[] BC000413_A12Contratante_CNPJ ;
      private bool[] BC000413_n12Contratante_CNPJ ;
      private String[] BC000413_A11Contratante_IE ;
      private String[] BC000413_A10Contratante_NomeFantasia ;
      private String[] BC000413_A9Contratante_RazaoSocial ;
      private bool[] BC000413_n9Contratante_RazaoSocial ;
      private String[] BC000413_A547Contratante_EmailSdaHost ;
      private bool[] BC000413_n547Contratante_EmailSdaHost ;
      private String[] BC000413_A548Contratante_EmailSdaUser ;
      private bool[] BC000413_n548Contratante_EmailSdaUser ;
      private String[] BC000413_A549Contratante_EmailSdaPass ;
      private bool[] BC000413_n549Contratante_EmailSdaPass ;
      private String[] BC000413_A550Contratante_EmailSdaKey ;
      private bool[] BC000413_n550Contratante_EmailSdaKey ;
      private bool[] BC000413_A551Contratante_EmailSdaAut ;
      private bool[] BC000413_n551Contratante_EmailSdaAut ;
      private short[] BC000413_A552Contratante_EmailSdaPort ;
      private bool[] BC000413_n552Contratante_EmailSdaPort ;
      private short[] BC000413_A1048Contratante_EmailSdaSec ;
      private bool[] BC000413_n1048Contratante_EmailSdaSec ;
      private String[] BC000413_A642AreaTrabalho_CalculoPFinal ;
      private bool[] BC000413_A834AreaTrabalho_ValidaOSFM ;
      private bool[] BC000413_n834AreaTrabalho_ValidaOSFM ;
      private short[] BC000413_A855AreaTrabalho_DiasParaPagar ;
      private bool[] BC000413_n855AreaTrabalho_DiasParaPagar ;
      private int[] BC000413_A987AreaTrabalho_ContratadaUpdBslCod ;
      private bool[] BC000413_n987AreaTrabalho_ContratadaUpdBslCod ;
      private short[] BC000413_A1154AreaTrabalho_TipoPlanilha ;
      private bool[] BC000413_n1154AreaTrabalho_TipoPlanilha ;
      private bool[] BC000413_A72AreaTrabalho_Ativo ;
      private int[] BC000413_A1588AreaTrabalho_SS_Codigo ;
      private bool[] BC000413_n1588AreaTrabalho_SS_Codigo ;
      private short[] BC000413_A2081AreaTrabalho_VerTA ;
      private bool[] BC000413_n2081AreaTrabalho_VerTA ;
      private bool[] BC000413_A2080AreaTrabalho_SelUsrPrestadora ;
      private bool[] BC000413_n2080AreaTrabalho_SelUsrPrestadora ;
      private int[] BC000413_A29Contratante_Codigo ;
      private bool[] BC000413_n29Contratante_Codigo ;
      private int[] BC000413_A830AreaTrabalho_ServicoPadrao ;
      private bool[] BC000413_n830AreaTrabalho_ServicoPadrao ;
      private int[] BC000413_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] BC000413_n1216AreaTrabalho_OrganizacaoCod ;
      private int[] BC000413_A25Municipio_Codigo ;
      private bool[] BC000413_n25Municipio_Codigo ;
      private String[] BC000413_A23Estado_UF ;
      private int[] BC000413_A335Contratante_PessoaCod ;
      private short[] BC000413_A272AreaTrabalho_ContagensQtdGeral ;
      private bool[] BC000413_n272AreaTrabalho_ContagensQtdGeral ;
      private short[] BC000411_A272AreaTrabalho_ContagensQtdGeral ;
      private bool[] BC000411_n272AreaTrabalho_ContagensQtdGeral ;
      private String[] BC00046_A1214Organizacao_Nome ;
      private bool[] BC00046_n1214Organizacao_Nome ;
      private String[] BC00044_A33Contratante_Fax ;
      private bool[] BC00044_n33Contratante_Fax ;
      private String[] BC00044_A32Contratante_Ramal ;
      private bool[] BC00044_n32Contratante_Ramal ;
      private String[] BC00044_A31Contratante_Telefone ;
      private String[] BC00044_A14Contratante_Email ;
      private bool[] BC00044_n14Contratante_Email ;
      private String[] BC00044_A13Contratante_WebSite ;
      private bool[] BC00044_n13Contratante_WebSite ;
      private String[] BC00044_A11Contratante_IE ;
      private String[] BC00044_A10Contratante_NomeFantasia ;
      private String[] BC00044_A547Contratante_EmailSdaHost ;
      private bool[] BC00044_n547Contratante_EmailSdaHost ;
      private String[] BC00044_A548Contratante_EmailSdaUser ;
      private bool[] BC00044_n548Contratante_EmailSdaUser ;
      private String[] BC00044_A549Contratante_EmailSdaPass ;
      private bool[] BC00044_n549Contratante_EmailSdaPass ;
      private String[] BC00044_A550Contratante_EmailSdaKey ;
      private bool[] BC00044_n550Contratante_EmailSdaKey ;
      private bool[] BC00044_A551Contratante_EmailSdaAut ;
      private bool[] BC00044_n551Contratante_EmailSdaAut ;
      private short[] BC00044_A552Contratante_EmailSdaPort ;
      private bool[] BC00044_n552Contratante_EmailSdaPort ;
      private short[] BC00044_A1048Contratante_EmailSdaSec ;
      private bool[] BC00044_n1048Contratante_EmailSdaSec ;
      private int[] BC00044_A25Municipio_Codigo ;
      private bool[] BC00044_n25Municipio_Codigo ;
      private int[] BC00044_A335Contratante_PessoaCod ;
      private String[] BC00047_A26Municipio_Nome ;
      private String[] BC00047_A23Estado_UF ;
      private String[] BC00048_A24Estado_Nome ;
      private String[] BC00049_A12Contratante_CNPJ ;
      private bool[] BC00049_n12Contratante_CNPJ ;
      private String[] BC00049_A9Contratante_RazaoSocial ;
      private bool[] BC00049_n9Contratante_RazaoSocial ;
      private int[] BC00045_A830AreaTrabalho_ServicoPadrao ;
      private bool[] BC00045_n830AreaTrabalho_ServicoPadrao ;
      private int[] BC000414_A5AreaTrabalho_Codigo ;
      private int[] BC00043_A5AreaTrabalho_Codigo ;
      private String[] BC00043_A6AreaTrabalho_Descricao ;
      private String[] BC00043_A642AreaTrabalho_CalculoPFinal ;
      private bool[] BC00043_A834AreaTrabalho_ValidaOSFM ;
      private bool[] BC00043_n834AreaTrabalho_ValidaOSFM ;
      private short[] BC00043_A855AreaTrabalho_DiasParaPagar ;
      private bool[] BC00043_n855AreaTrabalho_DiasParaPagar ;
      private int[] BC00043_A987AreaTrabalho_ContratadaUpdBslCod ;
      private bool[] BC00043_n987AreaTrabalho_ContratadaUpdBslCod ;
      private short[] BC00043_A1154AreaTrabalho_TipoPlanilha ;
      private bool[] BC00043_n1154AreaTrabalho_TipoPlanilha ;
      private bool[] BC00043_A72AreaTrabalho_Ativo ;
      private int[] BC00043_A1588AreaTrabalho_SS_Codigo ;
      private bool[] BC00043_n1588AreaTrabalho_SS_Codigo ;
      private short[] BC00043_A2081AreaTrabalho_VerTA ;
      private bool[] BC00043_n2081AreaTrabalho_VerTA ;
      private bool[] BC00043_A2080AreaTrabalho_SelUsrPrestadora ;
      private bool[] BC00043_n2080AreaTrabalho_SelUsrPrestadora ;
      private int[] BC00043_A29Contratante_Codigo ;
      private bool[] BC00043_n29Contratante_Codigo ;
      private int[] BC00043_A830AreaTrabalho_ServicoPadrao ;
      private bool[] BC00043_n830AreaTrabalho_ServicoPadrao ;
      private int[] BC00043_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] BC00043_n1216AreaTrabalho_OrganizacaoCod ;
      private int[] BC00042_A5AreaTrabalho_Codigo ;
      private String[] BC00042_A6AreaTrabalho_Descricao ;
      private String[] BC00042_A642AreaTrabalho_CalculoPFinal ;
      private bool[] BC00042_A834AreaTrabalho_ValidaOSFM ;
      private bool[] BC00042_n834AreaTrabalho_ValidaOSFM ;
      private short[] BC00042_A855AreaTrabalho_DiasParaPagar ;
      private bool[] BC00042_n855AreaTrabalho_DiasParaPagar ;
      private int[] BC00042_A987AreaTrabalho_ContratadaUpdBslCod ;
      private bool[] BC00042_n987AreaTrabalho_ContratadaUpdBslCod ;
      private short[] BC00042_A1154AreaTrabalho_TipoPlanilha ;
      private bool[] BC00042_n1154AreaTrabalho_TipoPlanilha ;
      private bool[] BC00042_A72AreaTrabalho_Ativo ;
      private int[] BC00042_A1588AreaTrabalho_SS_Codigo ;
      private bool[] BC00042_n1588AreaTrabalho_SS_Codigo ;
      private short[] BC00042_A2081AreaTrabalho_VerTA ;
      private bool[] BC00042_n2081AreaTrabalho_VerTA ;
      private bool[] BC00042_A2080AreaTrabalho_SelUsrPrestadora ;
      private bool[] BC00042_n2080AreaTrabalho_SelUsrPrestadora ;
      private int[] BC00042_A29Contratante_Codigo ;
      private bool[] BC00042_n29Contratante_Codigo ;
      private int[] BC00042_A830AreaTrabalho_ServicoPadrao ;
      private bool[] BC00042_n830AreaTrabalho_ServicoPadrao ;
      private int[] BC00042_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] BC00042_n1216AreaTrabalho_OrganizacaoCod ;
      private int[] BC000415_A5AreaTrabalho_Codigo ;
      private short[] BC000419_A272AreaTrabalho_ContagensQtdGeral ;
      private bool[] BC000419_n272AreaTrabalho_ContagensQtdGeral ;
      private String[] BC000420_A1214Organizacao_Nome ;
      private bool[] BC000420_n1214Organizacao_Nome ;
      private String[] BC000421_A33Contratante_Fax ;
      private bool[] BC000421_n33Contratante_Fax ;
      private String[] BC000421_A32Contratante_Ramal ;
      private bool[] BC000421_n32Contratante_Ramal ;
      private String[] BC000421_A31Contratante_Telefone ;
      private String[] BC000421_A14Contratante_Email ;
      private bool[] BC000421_n14Contratante_Email ;
      private String[] BC000421_A13Contratante_WebSite ;
      private bool[] BC000421_n13Contratante_WebSite ;
      private String[] BC000421_A11Contratante_IE ;
      private String[] BC000421_A10Contratante_NomeFantasia ;
      private String[] BC000421_A547Contratante_EmailSdaHost ;
      private bool[] BC000421_n547Contratante_EmailSdaHost ;
      private String[] BC000421_A548Contratante_EmailSdaUser ;
      private bool[] BC000421_n548Contratante_EmailSdaUser ;
      private String[] BC000421_A549Contratante_EmailSdaPass ;
      private bool[] BC000421_n549Contratante_EmailSdaPass ;
      private String[] BC000421_A550Contratante_EmailSdaKey ;
      private bool[] BC000421_n550Contratante_EmailSdaKey ;
      private bool[] BC000421_A551Contratante_EmailSdaAut ;
      private bool[] BC000421_n551Contratante_EmailSdaAut ;
      private short[] BC000421_A552Contratante_EmailSdaPort ;
      private bool[] BC000421_n552Contratante_EmailSdaPort ;
      private short[] BC000421_A1048Contratante_EmailSdaSec ;
      private bool[] BC000421_n1048Contratante_EmailSdaSec ;
      private int[] BC000421_A25Municipio_Codigo ;
      private bool[] BC000421_n25Municipio_Codigo ;
      private int[] BC000421_A335Contratante_PessoaCod ;
      private String[] BC000422_A26Municipio_Nome ;
      private String[] BC000422_A23Estado_UF ;
      private String[] BC000423_A24Estado_Nome ;
      private String[] BC000424_A12Contratante_CNPJ ;
      private bool[] BC000424_n12Contratante_CNPJ ;
      private String[] BC000424_A9Contratante_RazaoSocial ;
      private bool[] BC000424_n9Contratante_RazaoSocial ;
      private int[] BC000425_A619GrupoFuncao_Codigo ;
      private int[] BC000426_A2175Equipe_Codigo ;
      private int[] BC000427_A648Projeto_Codigo ;
      private int[] BC000428_A2077UsuarioNotifica_Codigo ;
      private int[] BC000429_A2058Indicador_Codigo ;
      private long[] BC000430_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] BC000431_A1795CatalogoServico_Codigo ;
      private int[] BC000432_A1482Gestao_Codigo ;
      private int[] BC000433_A1347Glosario_Codigo ;
      private int[] BC000434_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] BC000434_A962VariavelCocomo_Sigla ;
      private String[] BC000434_A964VariavelCocomo_Tipo ;
      private short[] BC000434_A992VariavelCocomo_Sequencial ;
      private String[] BC000435_A860RegrasContagem_Regra ;
      private int[] BC000436_A848ParametrosPln_Codigo ;
      private int[] BC000437_A351AmbienteTecnologico_Codigo ;
      private int[] BC000438_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] BC000438_A715ItemNaoMensuravel_Codigo ;
      private int[] BC000439_A709ReferenciaINM_Codigo ;
      private int[] BC000440_A596Lote_Codigo ;
      private int[] BC000441_A426NaoConformidade_Codigo ;
      private int[] BC000442_A423Status_Codigo ;
      private int[] BC000443_A93Guia_Codigo ;
      private int[] BC000444_A192Contagem_Codigo ;
      private int[] BC000445_A127Sistema_Codigo ;
      private int[] BC000446_A74Contrato_Codigo ;
      private int[] BC000447_A39Contratada_Codigo ;
      private int[] BC000448_A3Perfil_Codigo ;
      private int[] BC000449_A1685Proposta_Codigo ;
      private int[] BC000450_A5AreaTrabalho_Codigo ;
      private int[] BC000450_A2127FatoresImpacto_Codigo ;
      private int[] BC000451_A5AreaTrabalho_Codigo ;
      private int[] BC000451_A93Guia_Codigo ;
      private int[] BC000453_A5AreaTrabalho_Codigo ;
      private String[] BC000453_A6AreaTrabalho_Descricao ;
      private String[] BC000453_A1214Organizacao_Nome ;
      private bool[] BC000453_n1214Organizacao_Nome ;
      private String[] BC000453_A24Estado_Nome ;
      private String[] BC000453_A26Municipio_Nome ;
      private String[] BC000453_A33Contratante_Fax ;
      private bool[] BC000453_n33Contratante_Fax ;
      private String[] BC000453_A32Contratante_Ramal ;
      private bool[] BC000453_n32Contratante_Ramal ;
      private String[] BC000453_A31Contratante_Telefone ;
      private String[] BC000453_A14Contratante_Email ;
      private bool[] BC000453_n14Contratante_Email ;
      private String[] BC000453_A13Contratante_WebSite ;
      private bool[] BC000453_n13Contratante_WebSite ;
      private String[] BC000453_A12Contratante_CNPJ ;
      private bool[] BC000453_n12Contratante_CNPJ ;
      private String[] BC000453_A11Contratante_IE ;
      private String[] BC000453_A10Contratante_NomeFantasia ;
      private String[] BC000453_A9Contratante_RazaoSocial ;
      private bool[] BC000453_n9Contratante_RazaoSocial ;
      private String[] BC000453_A547Contratante_EmailSdaHost ;
      private bool[] BC000453_n547Contratante_EmailSdaHost ;
      private String[] BC000453_A548Contratante_EmailSdaUser ;
      private bool[] BC000453_n548Contratante_EmailSdaUser ;
      private String[] BC000453_A549Contratante_EmailSdaPass ;
      private bool[] BC000453_n549Contratante_EmailSdaPass ;
      private String[] BC000453_A550Contratante_EmailSdaKey ;
      private bool[] BC000453_n550Contratante_EmailSdaKey ;
      private bool[] BC000453_A551Contratante_EmailSdaAut ;
      private bool[] BC000453_n551Contratante_EmailSdaAut ;
      private short[] BC000453_A552Contratante_EmailSdaPort ;
      private bool[] BC000453_n552Contratante_EmailSdaPort ;
      private short[] BC000453_A1048Contratante_EmailSdaSec ;
      private bool[] BC000453_n1048Contratante_EmailSdaSec ;
      private String[] BC000453_A642AreaTrabalho_CalculoPFinal ;
      private bool[] BC000453_A834AreaTrabalho_ValidaOSFM ;
      private bool[] BC000453_n834AreaTrabalho_ValidaOSFM ;
      private short[] BC000453_A855AreaTrabalho_DiasParaPagar ;
      private bool[] BC000453_n855AreaTrabalho_DiasParaPagar ;
      private int[] BC000453_A987AreaTrabalho_ContratadaUpdBslCod ;
      private bool[] BC000453_n987AreaTrabalho_ContratadaUpdBslCod ;
      private short[] BC000453_A1154AreaTrabalho_TipoPlanilha ;
      private bool[] BC000453_n1154AreaTrabalho_TipoPlanilha ;
      private bool[] BC000453_A72AreaTrabalho_Ativo ;
      private int[] BC000453_A1588AreaTrabalho_SS_Codigo ;
      private bool[] BC000453_n1588AreaTrabalho_SS_Codigo ;
      private short[] BC000453_A2081AreaTrabalho_VerTA ;
      private bool[] BC000453_n2081AreaTrabalho_VerTA ;
      private bool[] BC000453_A2080AreaTrabalho_SelUsrPrestadora ;
      private bool[] BC000453_n2080AreaTrabalho_SelUsrPrestadora ;
      private int[] BC000453_A29Contratante_Codigo ;
      private bool[] BC000453_n29Contratante_Codigo ;
      private int[] BC000453_A830AreaTrabalho_ServicoPadrao ;
      private bool[] BC000453_n830AreaTrabalho_ServicoPadrao ;
      private int[] BC000453_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] BC000453_n1216AreaTrabalho_OrganizacaoCod ;
      private int[] BC000453_A25Municipio_Codigo ;
      private bool[] BC000453_n25Municipio_Codigo ;
      private String[] BC000453_A23Estado_UF ;
      private int[] BC000453_A335Contratante_PessoaCod ;
      private short[] BC000453_A272AreaTrabalho_ContagensQtdGeral ;
      private bool[] BC000453_n272AreaTrabalho_ContagensQtdGeral ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtAuditingObject AV30AuditingObject ;
      private SdtParametrosSistema AV31ParametrosSistema ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class areatrabalho_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new ForEachCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC000413 ;
          prmBC000413 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC000413 ;
          cmdBufferBC000413=" SELECT TM1.[AreaTrabalho_Codigo], TM1.[AreaTrabalho_Descricao], T3.[Organizacao_Nome], T6.[Estado_Nome], T5.[Municipio_Nome], T4.[Contratante_Fax], T4.[Contratante_Ramal], T4.[Contratante_Telefone], T4.[Contratante_Email], T4.[Contratante_WebSite], T7.[Pessoa_Docto] AS Contratante_CNPJ, T4.[Contratante_IE], T4.[Contratante_NomeFantasia], T7.[Pessoa_Nome] AS Contratante_RazaoSocial, T4.[Contratante_EmailSdaHost], T4.[Contratante_EmailSdaUser], T4.[Contratante_EmailSdaPass], T4.[Contratante_EmailSdaKey], T4.[Contratante_EmailSdaAut], T4.[Contratante_EmailSdaPort], T4.[Contratante_EmailSdaSec], TM1.[AreaTrabalho_CalculoPFinal], TM1.[AreaTrabalho_ValidaOSFM], TM1.[AreaTrabalho_DiasParaPagar], TM1.[AreaTrabalho_ContratadaUpdBslCod], TM1.[AreaTrabalho_TipoPlanilha], TM1.[AreaTrabalho_Ativo], TM1.[AreaTrabalho_SS_Codigo], TM1.[AreaTrabalho_VerTA], TM1.[AreaTrabalho_SelUsrPrestadora], TM1.[Contratante_Codigo], TM1.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, TM1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T4.[Municipio_Codigo], T5.[Estado_UF], T4.[Contratante_PessoaCod] AS Contratante_PessoaCod, COALESCE( T2.[AreaTrabalho_ContagensQtdGeral], 0) AS AreaTrabalho_ContagensQtdGeral FROM (((((([AreaTrabalho] TM1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] ) T2 ON T2.[Contagem_AreaTrabalhoCod] = TM1.[AreaTrabalho_Codigo]) LEFT JOIN [Organizacao] T3 WITH (NOLOCK) ON T3.[Organizacao_Codigo] = TM1.[AreaTrabalho_OrganizacaoCod]) LEFT JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = TM1.[Contratante_Codigo]) LEFT JOIN [Municipio] T5 WITH (NOLOCK) ON T5.[Municipio_Codigo] = T4.[Municipio_Codigo]) LEFT JOIN [Estado] T6 WITH "
          + " (NOLOCK) ON T6.[Estado_UF] = T5.[Estado_UF]) LEFT JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T4.[Contratante_PessoaCod]) WHERE TM1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY TM1.[AreaTrabalho_Codigo]  OPTION (FAST 100)" ;
          Object[] prmBC000411 ;
          prmBC000411 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00046 ;
          prmBC00046 = new Object[] {
          new Object[] {"@AreaTrabalho_OrganizacaoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00044 ;
          prmBC00044 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00047 ;
          prmBC00047 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00048 ;
          prmBC00048 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmBC00049 ;
          prmBC00049 = new Object[] {
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00045 ;
          prmBC00045 = new Object[] {
          new Object[] {"@AreaTrabalho_ServicoPadrao",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000414 ;
          prmBC000414 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00043 ;
          prmBC00043 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00042 ;
          prmBC00042 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000415 ;
          prmBC000415 = new Object[] {
          new Object[] {"@AreaTrabalho_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AreaTrabalho_CalculoPFinal",SqlDbType.Char,2,0} ,
          new Object[] {"@AreaTrabalho_ValidaOSFM",SqlDbType.Bit,4,0} ,
          new Object[] {"@AreaTrabalho_DiasParaPagar",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AreaTrabalho_ContratadaUpdBslCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_TipoPlanilha",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AreaTrabalho_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AreaTrabalho_SS_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_VerTA",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AreaTrabalho_SelUsrPrestadora",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_ServicoPadrao",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_OrganizacaoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000416 ;
          prmBC000416 = new Object[] {
          new Object[] {"@AreaTrabalho_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AreaTrabalho_CalculoPFinal",SqlDbType.Char,2,0} ,
          new Object[] {"@AreaTrabalho_ValidaOSFM",SqlDbType.Bit,4,0} ,
          new Object[] {"@AreaTrabalho_DiasParaPagar",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AreaTrabalho_ContratadaUpdBslCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_TipoPlanilha",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AreaTrabalho_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AreaTrabalho_SS_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_VerTA",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AreaTrabalho_SelUsrPrestadora",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_ServicoPadrao",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_OrganizacaoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000417 ;
          prmBC000417 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000419 ;
          prmBC000419 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000420 ;
          prmBC000420 = new Object[] {
          new Object[] {"@AreaTrabalho_OrganizacaoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000421 ;
          prmBC000421 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000422 ;
          prmBC000422 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000423 ;
          prmBC000423 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmBC000424 ;
          prmBC000424 = new Object[] {
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000425 ;
          prmBC000425 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000426 ;
          prmBC000426 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000427 ;
          prmBC000427 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000428 ;
          prmBC000428 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000429 ;
          prmBC000429 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000430 ;
          prmBC000430 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000431 ;
          prmBC000431 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000432 ;
          prmBC000432 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000433 ;
          prmBC000433 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000434 ;
          prmBC000434 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000435 ;
          prmBC000435 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000436 ;
          prmBC000436 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000437 ;
          prmBC000437 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000438 ;
          prmBC000438 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000439 ;
          prmBC000439 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000440 ;
          prmBC000440 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000441 ;
          prmBC000441 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000442 ;
          prmBC000442 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000443 ;
          prmBC000443 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000444 ;
          prmBC000444 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000445 ;
          prmBC000445 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000446 ;
          prmBC000446 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000447 ;
          prmBC000447 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000448 ;
          prmBC000448 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000449 ;
          prmBC000449 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000450 ;
          prmBC000450 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000451 ;
          prmBC000451 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000453 ;
          prmBC000453 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC000453 ;
          cmdBufferBC000453=" SELECT TM1.[AreaTrabalho_Codigo], TM1.[AreaTrabalho_Descricao], T3.[Organizacao_Nome], T6.[Estado_Nome], T5.[Municipio_Nome], T4.[Contratante_Fax], T4.[Contratante_Ramal], T4.[Contratante_Telefone], T4.[Contratante_Email], T4.[Contratante_WebSite], T7.[Pessoa_Docto] AS Contratante_CNPJ, T4.[Contratante_IE], T4.[Contratante_NomeFantasia], T7.[Pessoa_Nome] AS Contratante_RazaoSocial, T4.[Contratante_EmailSdaHost], T4.[Contratante_EmailSdaUser], T4.[Contratante_EmailSdaPass], T4.[Contratante_EmailSdaKey], T4.[Contratante_EmailSdaAut], T4.[Contratante_EmailSdaPort], T4.[Contratante_EmailSdaSec], TM1.[AreaTrabalho_CalculoPFinal], TM1.[AreaTrabalho_ValidaOSFM], TM1.[AreaTrabalho_DiasParaPagar], TM1.[AreaTrabalho_ContratadaUpdBslCod], TM1.[AreaTrabalho_TipoPlanilha], TM1.[AreaTrabalho_Ativo], TM1.[AreaTrabalho_SS_Codigo], TM1.[AreaTrabalho_VerTA], TM1.[AreaTrabalho_SelUsrPrestadora], TM1.[Contratante_Codigo], TM1.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, TM1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T4.[Municipio_Codigo], T5.[Estado_UF], T4.[Contratante_PessoaCod] AS Contratante_PessoaCod, COALESCE( T2.[AreaTrabalho_ContagensQtdGeral], 0) AS AreaTrabalho_ContagensQtdGeral FROM (((((([AreaTrabalho] TM1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] ) T2 ON T2.[Contagem_AreaTrabalhoCod] = TM1.[AreaTrabalho_Codigo]) LEFT JOIN [Organizacao] T3 WITH (NOLOCK) ON T3.[Organizacao_Codigo] = TM1.[AreaTrabalho_OrganizacaoCod]) LEFT JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = TM1.[Contratante_Codigo]) LEFT JOIN [Municipio] T5 WITH (NOLOCK) ON T5.[Municipio_Codigo] = T4.[Municipio_Codigo]) LEFT JOIN [Estado] T6 WITH "
          + " (NOLOCK) ON T6.[Estado_UF] = T5.[Estado_UF]) LEFT JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T4.[Contratante_PessoaCod]) WHERE TM1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY TM1.[AreaTrabalho_Codigo]  OPTION (FAST 100)" ;
          def= new CursorDef[] {
              new CursorDef("BC00042", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao], [AreaTrabalho_CalculoPFinal], [AreaTrabalho_ValidaOSFM], [AreaTrabalho_DiasParaPagar], [AreaTrabalho_ContratadaUpdBslCod], [AreaTrabalho_TipoPlanilha], [AreaTrabalho_Ativo], [AreaTrabalho_SS_Codigo], [AreaTrabalho_VerTA], [AreaTrabalho_SelUsrPrestadora], [Contratante_Codigo], [AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, [AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod FROM [AreaTrabalho] WITH (UPDLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00042,1,0,true,false )
             ,new CursorDef("BC00043", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao], [AreaTrabalho_CalculoPFinal], [AreaTrabalho_ValidaOSFM], [AreaTrabalho_DiasParaPagar], [AreaTrabalho_ContratadaUpdBslCod], [AreaTrabalho_TipoPlanilha], [AreaTrabalho_Ativo], [AreaTrabalho_SS_Codigo], [AreaTrabalho_VerTA], [AreaTrabalho_SelUsrPrestadora], [Contratante_Codigo], [AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, [AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00043,1,0,true,false )
             ,new CursorDef("BC00044", "SELECT [Contratante_Fax], [Contratante_Ramal], [Contratante_Telefone], [Contratante_Email], [Contratante_WebSite], [Contratante_IE], [Contratante_NomeFantasia], [Contratante_EmailSdaHost], [Contratante_EmailSdaUser], [Contratante_EmailSdaPass], [Contratante_EmailSdaKey], [Contratante_EmailSdaAut], [Contratante_EmailSdaPort], [Contratante_EmailSdaSec], [Municipio_Codigo], [Contratante_PessoaCod] AS Contratante_PessoaCod FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00044,1,0,true,false )
             ,new CursorDef("BC00045", "SELECT [Servico_Codigo] AS AreaTrabalho_ServicoPadrao FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AreaTrabalho_ServicoPadrao ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00045,1,0,true,false )
             ,new CursorDef("BC00046", "SELECT [Organizacao_Nome] FROM [Organizacao] WITH (NOLOCK) WHERE [Organizacao_Codigo] = @AreaTrabalho_OrganizacaoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00046,1,0,true,false )
             ,new CursorDef("BC00047", "SELECT [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00047,1,0,true,false )
             ,new CursorDef("BC00048", "SELECT [Estado_Nome] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00048,1,0,true,false )
             ,new CursorDef("BC00049", "SELECT [Pessoa_Docto] AS Contratante_CNPJ, [Pessoa_Nome] AS Contratante_RazaoSocial FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratante_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00049,1,0,true,false )
             ,new CursorDef("BC000411", "SELECT COALESCE( T1.[AreaTrabalho_ContagensQtdGeral], 0) AS AreaTrabalho_ContagensQtdGeral FROM (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] ) T1 WHERE T1.[Contagem_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000411,1,0,true,false )
             ,new CursorDef("BC000413", cmdBufferBC000413,true, GxErrorMask.GX_NOMASK, false, this,prmBC000413,100,0,true,false )
             ,new CursorDef("BC000414", "SELECT [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000414,1,0,true,false )
             ,new CursorDef("BC000415", "INSERT INTO [AreaTrabalho]([AreaTrabalho_Descricao], [AreaTrabalho_CalculoPFinal], [AreaTrabalho_ValidaOSFM], [AreaTrabalho_DiasParaPagar], [AreaTrabalho_ContratadaUpdBslCod], [AreaTrabalho_TipoPlanilha], [AreaTrabalho_Ativo], [AreaTrabalho_SS_Codigo], [AreaTrabalho_VerTA], [AreaTrabalho_SelUsrPrestadora], [Contratante_Codigo], [AreaTrabalho_ServicoPadrao], [AreaTrabalho_OrganizacaoCod]) VALUES(@AreaTrabalho_Descricao, @AreaTrabalho_CalculoPFinal, @AreaTrabalho_ValidaOSFM, @AreaTrabalho_DiasParaPagar, @AreaTrabalho_ContratadaUpdBslCod, @AreaTrabalho_TipoPlanilha, @AreaTrabalho_Ativo, @AreaTrabalho_SS_Codigo, @AreaTrabalho_VerTA, @AreaTrabalho_SelUsrPrestadora, @Contratante_Codigo, @AreaTrabalho_ServicoPadrao, @AreaTrabalho_OrganizacaoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC000415)
             ,new CursorDef("BC000416", "UPDATE [AreaTrabalho] SET [AreaTrabalho_Descricao]=@AreaTrabalho_Descricao, [AreaTrabalho_CalculoPFinal]=@AreaTrabalho_CalculoPFinal, [AreaTrabalho_ValidaOSFM]=@AreaTrabalho_ValidaOSFM, [AreaTrabalho_DiasParaPagar]=@AreaTrabalho_DiasParaPagar, [AreaTrabalho_ContratadaUpdBslCod]=@AreaTrabalho_ContratadaUpdBslCod, [AreaTrabalho_TipoPlanilha]=@AreaTrabalho_TipoPlanilha, [AreaTrabalho_Ativo]=@AreaTrabalho_Ativo, [AreaTrabalho_SS_Codigo]=@AreaTrabalho_SS_Codigo, [AreaTrabalho_VerTA]=@AreaTrabalho_VerTA, [AreaTrabalho_SelUsrPrestadora]=@AreaTrabalho_SelUsrPrestadora, [Contratante_Codigo]=@Contratante_Codigo, [AreaTrabalho_ServicoPadrao]=@AreaTrabalho_ServicoPadrao, [AreaTrabalho_OrganizacaoCod]=@AreaTrabalho_OrganizacaoCod  WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo", GxErrorMask.GX_NOMASK,prmBC000416)
             ,new CursorDef("BC000417", "DELETE FROM [AreaTrabalho]  WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo", GxErrorMask.GX_NOMASK,prmBC000417)
             ,new CursorDef("BC000419", "SELECT COALESCE( T1.[AreaTrabalho_ContagensQtdGeral], 0) AS AreaTrabalho_ContagensQtdGeral FROM (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] ) T1 WHERE T1.[Contagem_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000419,1,0,true,false )
             ,new CursorDef("BC000420", "SELECT [Organizacao_Nome] FROM [Organizacao] WITH (NOLOCK) WHERE [Organizacao_Codigo] = @AreaTrabalho_OrganizacaoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000420,1,0,true,false )
             ,new CursorDef("BC000421", "SELECT [Contratante_Fax], [Contratante_Ramal], [Contratante_Telefone], [Contratante_Email], [Contratante_WebSite], [Contratante_IE], [Contratante_NomeFantasia], [Contratante_EmailSdaHost], [Contratante_EmailSdaUser], [Contratante_EmailSdaPass], [Contratante_EmailSdaKey], [Contratante_EmailSdaAut], [Contratante_EmailSdaPort], [Contratante_EmailSdaSec], [Municipio_Codigo], [Contratante_PessoaCod] AS Contratante_PessoaCod FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000421,1,0,true,false )
             ,new CursorDef("BC000422", "SELECT [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000422,1,0,true,false )
             ,new CursorDef("BC000423", "SELECT [Estado_Nome] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000423,1,0,true,false )
             ,new CursorDef("BC000424", "SELECT [Pessoa_Docto] AS Contratante_CNPJ, [Pessoa_Nome] AS Contratante_RazaoSocial FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratante_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000424,1,0,true,false )
             ,new CursorDef("BC000425", "SELECT TOP 1 [GrupoFuncao_Codigo] FROM [Geral_Grupo_Funcao] WITH (NOLOCK) WHERE [GrupoFuncao_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000425,1,0,true,true )
             ,new CursorDef("BC000426", "SELECT TOP 1 [Equipe_Codigo] FROM [Equipe] WITH (NOLOCK) WHERE [Equipe_AreaTrabalhoCodigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000426,1,0,true,true )
             ,new CursorDef("BC000427", "SELECT TOP 1 [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_AreaTrabalhoCodigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000427,1,0,true,true )
             ,new CursorDef("BC000428", "SELECT TOP 1 [UsuarioNotifica_Codigo] FROM [UsuarioNotifica] WITH (NOLOCK) WHERE [UsuarioNotifica_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000428,1,0,true,true )
             ,new CursorDef("BC000429", "SELECT TOP 1 [Indicador_Codigo] FROM [Indicador] WITH (NOLOCK) WHERE [Indicador_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000429,1,0,true,true )
             ,new CursorDef("BC000430", "SELECT TOP 1 [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000430,1,0,true,true )
             ,new CursorDef("BC000431", "SELECT TOP 1 [CatalogoServico_Codigo] FROM [CatalogoServicos] WITH (NOLOCK) WHERE [CatalogoServico_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000431,1,0,true,true )
             ,new CursorDef("BC000432", "SELECT TOP 1 [Gestao_Codigo] FROM [Gestao] WITH (NOLOCK) WHERE [Gestao_AreaCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000432,1,0,true,true )
             ,new CursorDef("BC000433", "SELECT TOP 1 [Glosario_Codigo] FROM [Glosario] WITH (NOLOCK) WHERE [Glosario_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000433,1,0,true,true )
             ,new CursorDef("BC000434", "SELECT TOP 1 [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE [VariavelCocomo_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000434,1,0,true,true )
             ,new CursorDef("BC000435", "SELECT TOP 1 [RegrasContagem_Regra] FROM [RegrasContagem] WITH (NOLOCK) WHERE [RegrasContagem_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000435,1,0,true,true )
             ,new CursorDef("BC000436", "SELECT TOP 1 [ParametrosPln_Codigo] FROM [ParametrosPlanilhas] WITH (NOLOCK) WHERE [ParametrosPln_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000436,1,0,true,true )
             ,new CursorDef("BC000437", "SELECT TOP 1 [AmbienteTecnologico_Codigo] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000437,1,0,true,true )
             ,new CursorDef("BC000438", "SELECT TOP 1 [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Codigo] FROM [ItemNaoMensuravel] WITH (NOLOCK) WHERE [ItemNaoMensuravel_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000438,1,0,true,true )
             ,new CursorDef("BC000439", "SELECT TOP 1 [ReferenciaINM_Codigo] FROM [ReferenciaINM] WITH (NOLOCK) WHERE [ReferenciaINM_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000439,1,0,true,true )
             ,new CursorDef("BC000440", "SELECT TOP 1 [Lote_Codigo] FROM [Lote] WITH (NOLOCK) WHERE [Lote_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000440,1,0,true,true )
             ,new CursorDef("BC000441", "SELECT TOP 1 [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000441,1,0,true,true )
             ,new CursorDef("BC000442", "SELECT TOP 1 [Status_Codigo] FROM [Status] WITH (NOLOCK) WHERE [Status_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000442,1,0,true,true )
             ,new CursorDef("BC000443", "SELECT TOP 1 [Guia_Codigo] FROM [Guia] WITH (NOLOCK) WHERE [Guia_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000443,1,0,true,true )
             ,new CursorDef("BC000444", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000444,1,0,true,true )
             ,new CursorDef("BC000445", "SELECT TOP 1 [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000445,1,0,true,true )
             ,new CursorDef("BC000446", "SELECT TOP 1 [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000446,1,0,true,true )
             ,new CursorDef("BC000447", "SELECT TOP 1 [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000447,1,0,true,true )
             ,new CursorDef("BC000448", "SELECT TOP 1 [Perfil_Codigo] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000448,1,0,true,true )
             ,new CursorDef("BC000449", "SELECT TOP 1 [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000449,1,0,true,true )
             ,new CursorDef("BC000450", "SELECT TOP 1 [AreaTrabalho_Codigo], [FatoresImpacto_Codigo] FROM [FatoresImpacto] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000450,1,0,true,true )
             ,new CursorDef("BC000451", "SELECT TOP 1 [AreaTrabalho_Codigo], [Guia_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000451,1,0,true,true )
             ,new CursorDef("BC000453", cmdBufferBC000453,true, GxErrorMask.GX_NOMASK, false, this,prmBC000453,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((short[]) buf[14])[0] = rslt.getShort(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((int[]) buf[22])[0] = rslt.getInt(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((short[]) buf[14])[0] = rslt.getShort(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((int[]) buf[22])[0] = rslt.getInt(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 32) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 8 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 20) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 20) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 15) ;
                ((String[]) buf[18])[0] = rslt.getString(13, 100) ;
                ((String[]) buf[19])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((String[]) buf[21])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((String[]) buf[23])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((String[]) buf[25])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((String[]) buf[27])[0] = rslt.getString(18, 32) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(18);
                ((bool[]) buf[29])[0] = rslt.getBool(19) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(19);
                ((short[]) buf[31])[0] = rslt.getShort(20) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(20);
                ((short[]) buf[33])[0] = rslt.getShort(21) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(21);
                ((String[]) buf[35])[0] = rslt.getString(22, 2) ;
                ((bool[]) buf[36])[0] = rslt.getBool(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                ((short[]) buf[38])[0] = rslt.getShort(24) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(24);
                ((int[]) buf[40])[0] = rslt.getInt(25) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(25);
                ((short[]) buf[42])[0] = rslt.getShort(26) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(26);
                ((bool[]) buf[44])[0] = rslt.getBool(27) ;
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(28);
                ((short[]) buf[47])[0] = rslt.getShort(29) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(29);
                ((bool[]) buf[49])[0] = rslt.getBool(30) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(30);
                ((int[]) buf[51])[0] = rslt.getInt(31) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(31);
                ((int[]) buf[53])[0] = rslt.getInt(32) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(32);
                ((int[]) buf[55])[0] = rslt.getInt(33) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(33);
                ((int[]) buf[57])[0] = rslt.getInt(34) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(34);
                ((String[]) buf[59])[0] = rslt.getString(35, 2) ;
                ((int[]) buf[60])[0] = rslt.getInt(36) ;
                ((short[]) buf[61])[0] = rslt.getShort(37) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(37);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 32) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 45 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 46 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 47 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 20) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 20) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 15) ;
                ((String[]) buf[18])[0] = rslt.getString(13, 100) ;
                ((String[]) buf[19])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((String[]) buf[21])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((String[]) buf[23])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((String[]) buf[25])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((String[]) buf[27])[0] = rslt.getString(18, 32) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(18);
                ((bool[]) buf[29])[0] = rslt.getBool(19) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(19);
                ((short[]) buf[31])[0] = rslt.getShort(20) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(20);
                ((short[]) buf[33])[0] = rslt.getShort(21) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(21);
                ((String[]) buf[35])[0] = rslt.getString(22, 2) ;
                ((bool[]) buf[36])[0] = rslt.getBool(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                ((short[]) buf[38])[0] = rslt.getShort(24) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(24);
                ((int[]) buf[40])[0] = rslt.getInt(25) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(25);
                ((short[]) buf[42])[0] = rslt.getShort(26) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(26);
                ((bool[]) buf[44])[0] = rslt.getBool(27) ;
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(28);
                ((short[]) buf[47])[0] = rslt.getShort(29) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(29);
                ((bool[]) buf[49])[0] = rslt.getBool(30) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(30);
                ((int[]) buf[51])[0] = rslt.getInt(31) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(31);
                ((int[]) buf[53])[0] = rslt.getInt(32) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(32);
                ((int[]) buf[55])[0] = rslt.getInt(33) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(33);
                ((int[]) buf[57])[0] = rslt.getInt(34) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(34);
                ((String[]) buf[59])[0] = rslt.getString(35, 2) ;
                ((int[]) buf[60])[0] = rslt.getInt(36) ;
                ((short[]) buf[61])[0] = rslt.getShort(37) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(37);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[9]);
                }
                stmt.SetParameter(7, (bool)parms[10]);
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[22]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[9]);
                }
                stmt.SetParameter(7, (bool)parms[10]);
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[22]);
                }
                stmt.SetParameter(14, (int)parms[23]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 41 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 42 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 43 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 44 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 45 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 46 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 47 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
