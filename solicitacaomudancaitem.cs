/*
               File: SolicitacaoMudancaItem
        Description: Item da Mudan�a
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/3/2020 22:40:36.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacaomudancaitem : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"SOLICITACAOMUDANCAITEM_FUNCAOAPF") == 0 )
         {
            A993SolicitacaoMudanca_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n993SolicitacaoMudanca_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
            A166FuncaoAPF_Nome = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXSGASOLICITACAOMUDANCAITEM_FUNCAOAPF300( A993SolicitacaoMudanca_SistemaCod, A166FuncaoAPF_Nome) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"SOLICITACAOMUDANCAITEM_FUNCAOAPF") == 0 )
         {
            A993SolicitacaoMudanca_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n993SolicitacaoMudanca_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
            A166FuncaoAPF_Nome = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXSGASOLICITACAOMUDANCAITEM_FUNCAOAPF300( A993SolicitacaoMudanca_SistemaCod, A166FuncaoAPF_Nome) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxHideCode"+"_"+"SOLICITACAOMUDANCAITEM_FUNCAOAPF") == 0 )
         {
            A993SolicitacaoMudanca_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n993SolicitacaoMudanca_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
            h995SolicitacaoMudancaItem_FuncaoAPF = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXHCASOLICITACAOMUDANCAITEM_FUNCAOAPF30122( A993SolicitacaoMudanca_SistemaCod, h995SolicitacaoMudancaItem_FuncaoAPF) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A996SolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A996SolicitacaoMudanca_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A995SolicitacaoMudancaItem_FuncaoAPF = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A995SolicitacaoMudancaItem_FuncaoAPF) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7SolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSOLICITACAOMUDANCA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7SolicitacaoMudanca_Codigo), "ZZZZZ9")));
               AV8SolicitacaoMudancaItem_FuncaoAPF = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSOLICITACAOMUDANCAITEM_FUNCAOAPF", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8SolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Item da Mudan�a", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtSolicitacaoMudancaItem_FuncaoAPF_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public solicitacaomudancaitem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public solicitacaomudancaitem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_SolicitacaoMudanca_Codigo ,
                           int aP2_SolicitacaoMudancaItem_FuncaoAPF )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7SolicitacaoMudanca_Codigo = aP1_SolicitacaoMudanca_Codigo;
         this.AV8SolicitacaoMudancaItem_FuncaoAPF = aP2_SolicitacaoMudancaItem_FuncaoAPF;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_30122( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_30122e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSolicitacaoMudanca_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacaoMudanca_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtSolicitacaoMudanca_Codigo_Visible, edtSolicitacaoMudanca_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoMudancaItem.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_30122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_30122( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_30122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_30122( true) ;
         }
         return  ;
      }

      protected void wb_table3_31_30122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_30122e( true) ;
         }
         else
         {
            wb_table1_2_30122e( false) ;
         }
      }

      protected void wb_table3_31_30122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoMudancaItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoMudancaItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoMudancaItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_30122e( true) ;
         }
         else
         {
            wb_table3_31_30122e( false) ;
         }
      }

      protected void wb_table2_5_30122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_30122( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_30122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_30122e( true) ;
         }
         else
         {
            wb_table2_5_30122e( false) ;
         }
      }

      protected void wb_table4_13_30122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacaomudanca_sistemacod_Internalname, "Sistema", "", "", lblTextblocksolicitacaomudanca_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoMudancaItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSolicitacaoMudanca_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0, ",", "")), ((edtSolicitacaoMudanca_SistemaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A993SolicitacaoMudanca_SistemaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A993SolicitacaoMudanca_SistemaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacaoMudanca_SistemaCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSolicitacaoMudanca_SistemaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoMudancaItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacaomudancaitem_funcaoapf_Internalname, "Item", "", "", lblTextblocksolicitacaomudancaitem_funcaoapf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoMudancaItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSolicitacaoMudancaItem_FuncaoAPF_Internalname, h995SolicitacaoMudancaItem_FuncaoAPF, StringUtil.RTrim( context.localUtil.Format( h995SolicitacaoMudancaItem_FuncaoAPF, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacaoMudancaItem_FuncaoAPF_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSolicitacaoMudancaItem_FuncaoAPF_Enabled, 1, "text", "", 6, "chr", 1, "row", 200, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_SolicitacaoMudancaItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacaomudancaitem_descricao_Internalname, "", "", "", lblTextblocksolicitacaomudancaitem_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoMudancaItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"SOLICITACAOMUDANCAITEM_DESCRICAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_30122e( true) ;
         }
         else
         {
            wb_table4_13_30122e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11302 */
         E11302 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A993SolicitacaoMudanca_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_SistemaCod_Internalname), ",", "."));
               n993SolicitacaoMudanca_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
               h995SolicitacaoMudancaItem_FuncaoAPF = cgiGet( edtSolicitacaoMudancaItem_FuncaoAPF_Internalname);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SOLICITACAOMUDANCA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacaoMudanca_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A996SolicitacaoMudanca_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
               }
               else
               {
                  A996SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
               }
               /* Read saved values. */
               A995SolicitacaoMudancaItem_FuncaoAPF = (int)(context.localUtil.CToN( cgiGet( "GXHCSOLICITACAOMUDANCAITEM_FUNCAOAPF"), ",", "."));
               Z996SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z996SolicitacaoMudanca_Codigo"), ",", "."));
               Z995SolicitacaoMudancaItem_FuncaoAPF = (int)(context.localUtil.CToN( cgiGet( "Z995SolicitacaoMudancaItem_FuncaoAPF"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSOLICITACAOMUDANCA_CODIGO"), ",", "."));
               AV8SolicitacaoMudancaItem_FuncaoAPF = (int)(context.localUtil.CToN( cgiGet( "vSOLICITACAOMUDANCAITEM_FUNCAOAPF"), ",", "."));
               A998SolicitacaoMudancaItem_Descricao = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO");
               Gx_mode = cgiGet( "vMODE");
               Solicitacaomudancaitem_descricao_Width = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Width");
               Solicitacaomudancaitem_descricao_Height = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Height");
               Solicitacaomudancaitem_descricao_Skin = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Skin");
               Solicitacaomudancaitem_descricao_Toolbar = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Toolbar");
               Solicitacaomudancaitem_descricao_Color = (int)(context.localUtil.CToN( cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Color"), ",", "."));
               Solicitacaomudancaitem_descricao_Enabled = StringUtil.StrToBool( cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Enabled"));
               Solicitacaomudancaitem_descricao_Class = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Class");
               Solicitacaomudancaitem_descricao_Customtoolbar = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Customtoolbar");
               Solicitacaomudancaitem_descricao_Customconfiguration = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Customconfiguration");
               Solicitacaomudancaitem_descricao_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Toolbarcancollapse"));
               Solicitacaomudancaitem_descricao_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Toolbarexpanded"));
               Solicitacaomudancaitem_descricao_Buttonpressedid = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Buttonpressedid");
               Solicitacaomudancaitem_descricao_Captionvalue = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Captionvalue");
               Solicitacaomudancaitem_descricao_Captionclass = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Captionclass");
               Solicitacaomudancaitem_descricao_Captionposition = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Captionposition");
               Solicitacaomudancaitem_descricao_Coltitle = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Coltitle");
               Solicitacaomudancaitem_descricao_Coltitlefont = cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Coltitlefont");
               Solicitacaomudancaitem_descricao_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Coltitlecolor"), ",", "."));
               Solicitacaomudancaitem_descricao_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Usercontroliscolumn"));
               Solicitacaomudancaitem_descricao_Visible = StringUtil.StrToBool( cgiGet( "SOLICITACAOMUDANCAITEM_DESCRICAO_Visible"));
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "SolicitacaoMudancaItem";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A996SolicitacaoMudanca_Codigo != Z996SolicitacaoMudanca_Codigo ) || ( A995SolicitacaoMudancaItem_FuncaoAPF != Z995SolicitacaoMudancaItem_FuncaoAPF ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("solicitacaomudancaitem:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A996SolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
                  A995SolicitacaoMudancaItem_FuncaoAPF = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode122 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode122;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound122 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_300( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "SOLICITACAOMUDANCA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtSolicitacaoMudanca_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11302 */
                           E11302 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12302 */
                           E12302 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12302 */
            E12302 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll30122( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes30122( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_300( )
      {
         BeforeValidate30122( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls30122( ) ;
            }
            else
            {
               CheckExtendedTable30122( ) ;
               CloseExtendedTableCursors30122( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption300( )
      {
      }

      protected void E11302( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         edtSolicitacaoMudanca_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudanca_Codigo_Visible), 5, 0)));
      }

      protected void E12302( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV10TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwsolicitacaomudancaitem.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM30122( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -8 )
         {
            Z998SolicitacaoMudancaItem_Descricao = A998SolicitacaoMudancaItem_Descricao;
            Z996SolicitacaoMudanca_Codigo = A996SolicitacaoMudanca_Codigo;
            Z995SolicitacaoMudancaItem_FuncaoAPF = A995SolicitacaoMudancaItem_FuncaoAPF;
            Z993SolicitacaoMudanca_SistemaCod = A993SolicitacaoMudanca_SistemaCod;
         }
      }

      protected void standaloneNotModal( )
      {
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7SolicitacaoMudanca_Codigo) )
         {
            A996SolicitacaoMudanca_Codigo = AV7SolicitacaoMudanca_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
         }
         if ( ! (0==AV7SolicitacaoMudanca_Codigo) )
         {
            edtSolicitacaoMudanca_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudanca_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtSolicitacaoMudanca_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudanca_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV7SolicitacaoMudanca_Codigo) )
         {
            edtSolicitacaoMudanca_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudanca_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV8SolicitacaoMudancaItem_FuncaoAPF) )
         {
            A995SolicitacaoMudancaItem_FuncaoAPF = AV8SolicitacaoMudancaItem_FuncaoAPF;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
            /* Using cursor T00306 */
            pr_default.execute(4, new Object[] {n993SolicitacaoMudanca_SistemaCod, A993SolicitacaoMudanca_SistemaCod, A995SolicitacaoMudancaItem_FuncaoAPF});
            h995SolicitacaoMudancaItem_FuncaoAPF = "";
            while ( (pr_default.getStatus(4) != 101) )
            {
               A166FuncaoAPF_Nome = T00306_A166FuncaoAPF_Nome[0];
               A183FuncaoAPF_Ativo = T00306_A183FuncaoAPF_Ativo[0];
               A360FuncaoAPF_SistemaCod = T00306_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = T00306_n360FuncaoAPF_SistemaCod[0];
               A165FuncaoAPF_Codigo = T00306_A165FuncaoAPF_Codigo[0];
               h995SolicitacaoMudancaItem_FuncaoAPF = A166FuncaoAPF_Nome;
               if (true) break;
            }
            pr_default.close(4);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "h995SolicitacaoMudancaItem_FuncaoAPF", h995SolicitacaoMudancaItem_FuncaoAPF);
         }
         if ( ! (0==AV8SolicitacaoMudancaItem_FuncaoAPF) )
         {
            edtSolicitacaoMudancaItem_FuncaoAPF_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudancaItem_FuncaoAPF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudancaItem_FuncaoAPF_Enabled), 5, 0)));
         }
         else
         {
            edtSolicitacaoMudancaItem_FuncaoAPF_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudancaItem_FuncaoAPF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudancaItem_FuncaoAPF_Enabled), 5, 0)));
         }
         if ( ! (0==AV8SolicitacaoMudancaItem_FuncaoAPF) )
         {
            edtSolicitacaoMudancaItem_FuncaoAPF_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudancaItem_FuncaoAPF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudancaItem_FuncaoAPF_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00304 */
            pr_default.execute(2, new Object[] {A996SolicitacaoMudanca_Codigo});
            A993SolicitacaoMudanca_SistemaCod = T00304_A993SolicitacaoMudanca_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
            n993SolicitacaoMudanca_SistemaCod = T00304_n993SolicitacaoMudanca_SistemaCod[0];
            pr_default.close(2);
         }
      }

      protected void Load30122( )
      {
         /* Using cursor T00307 */
         pr_default.execute(5, new Object[] {A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound122 = 1;
            A998SolicitacaoMudancaItem_Descricao = T00307_A998SolicitacaoMudancaItem_Descricao[0];
            A993SolicitacaoMudanca_SistemaCod = T00307_A993SolicitacaoMudanca_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
            n993SolicitacaoMudanca_SistemaCod = T00307_n993SolicitacaoMudanca_SistemaCod[0];
            ZM30122( -8) ;
         }
         pr_default.close(5);
         OnLoadActions30122( ) ;
      }

      protected void OnLoadActions30122( )
      {
         /* Using cursor T00308 */
         pr_default.execute(6, new Object[] {n993SolicitacaoMudanca_SistemaCod, A993SolicitacaoMudanca_SistemaCod, A995SolicitacaoMudancaItem_FuncaoAPF});
         h995SolicitacaoMudancaItem_FuncaoAPF = "";
         while ( (pr_default.getStatus(6) != 101) )
         {
            A166FuncaoAPF_Nome = T00308_A166FuncaoAPF_Nome[0];
            A183FuncaoAPF_Ativo = T00308_A183FuncaoAPF_Ativo[0];
            A360FuncaoAPF_SistemaCod = T00308_A360FuncaoAPF_SistemaCod[0];
            n360FuncaoAPF_SistemaCod = T00308_n360FuncaoAPF_SistemaCod[0];
            A165FuncaoAPF_Codigo = T00308_A165FuncaoAPF_Codigo[0];
            h995SolicitacaoMudancaItem_FuncaoAPF = A166FuncaoAPF_Nome;
            if (true) break;
         }
         pr_default.close(6);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "h995SolicitacaoMudancaItem_FuncaoAPF", h995SolicitacaoMudancaItem_FuncaoAPF);
      }

      protected void CheckExtendedTable30122( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00304 */
         pr_default.execute(2, new Object[] {A996SolicitacaoMudanca_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicita��o de Mudan�a'.", "ForeignKeyNotFound", 1, "SOLICITACAOMUDANCA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacaoMudanca_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A993SolicitacaoMudanca_SistemaCod = T00304_A993SolicitacaoMudanca_SistemaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
         n993SolicitacaoMudanca_SistemaCod = T00304_n993SolicitacaoMudanca_SistemaCod[0];
         pr_default.close(2);
         /* Using cursor T00305 */
         pr_default.execute(3, new Object[] {A995SolicitacaoMudancaItem_FuncaoAPF});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacao Mudanca Item_Funcao APF'.", "ForeignKeyNotFound", 1, "SOLICITACAOMUDANCAITEM_FUNCAOAPF");
            AnyError = 1;
            GX_FocusControl = edtSolicitacaoMudancaItem_FuncaoAPF_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors30122( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_9( int A996SolicitacaoMudanca_Codigo )
      {
         /* Using cursor T00309 */
         pr_default.execute(7, new Object[] {A996SolicitacaoMudanca_Codigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicita��o de Mudan�a'.", "ForeignKeyNotFound", 1, "SOLICITACAOMUDANCA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacaoMudanca_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A993SolicitacaoMudanca_SistemaCod = T00309_A993SolicitacaoMudanca_SistemaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
         n993SolicitacaoMudanca_SistemaCod = T00309_n993SolicitacaoMudanca_SistemaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_10( int A995SolicitacaoMudancaItem_FuncaoAPF )
      {
         /* Using cursor T003010 */
         pr_default.execute(8, new Object[] {A995SolicitacaoMudancaItem_FuncaoAPF});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacao Mudanca Item_Funcao APF'.", "ForeignKeyNotFound", 1, "SOLICITACAOMUDANCAITEM_FUNCAOAPF");
            AnyError = 1;
            GX_FocusControl = edtSolicitacaoMudancaItem_FuncaoAPF_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void GetKey30122( )
      {
         /* Using cursor T003011 */
         pr_default.execute(9, new Object[] {A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound122 = 1;
         }
         else
         {
            RcdFound122 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00303 */
         pr_default.execute(1, new Object[] {A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM30122( 8) ;
            RcdFound122 = 1;
            A998SolicitacaoMudancaItem_Descricao = T00303_A998SolicitacaoMudancaItem_Descricao[0];
            A996SolicitacaoMudanca_Codigo = T00303_A996SolicitacaoMudanca_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
            A995SolicitacaoMudancaItem_FuncaoAPF = T00303_A995SolicitacaoMudancaItem_FuncaoAPF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
            Z996SolicitacaoMudanca_Codigo = A996SolicitacaoMudanca_Codigo;
            Z995SolicitacaoMudancaItem_FuncaoAPF = A995SolicitacaoMudancaItem_FuncaoAPF;
            sMode122 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load30122( ) ;
            if ( AnyError == 1 )
            {
               RcdFound122 = 0;
               InitializeNonKey30122( ) ;
            }
            Gx_mode = sMode122;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound122 = 0;
            InitializeNonKey30122( ) ;
            sMode122 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode122;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey30122( ) ;
         if ( RcdFound122 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound122 = 0;
         /* Using cursor T003012 */
         pr_default.execute(10, new Object[] {A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T003012_A996SolicitacaoMudanca_Codigo[0] < A996SolicitacaoMudanca_Codigo ) || ( T003012_A996SolicitacaoMudanca_Codigo[0] == A996SolicitacaoMudanca_Codigo ) && ( T003012_A995SolicitacaoMudancaItem_FuncaoAPF[0] < A995SolicitacaoMudancaItem_FuncaoAPF ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T003012_A996SolicitacaoMudanca_Codigo[0] > A996SolicitacaoMudanca_Codigo ) || ( T003012_A996SolicitacaoMudanca_Codigo[0] == A996SolicitacaoMudanca_Codigo ) && ( T003012_A995SolicitacaoMudancaItem_FuncaoAPF[0] > A995SolicitacaoMudancaItem_FuncaoAPF ) ) )
            {
               A996SolicitacaoMudanca_Codigo = T003012_A996SolicitacaoMudanca_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
               A995SolicitacaoMudancaItem_FuncaoAPF = T003012_A995SolicitacaoMudancaItem_FuncaoAPF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
               RcdFound122 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound122 = 0;
         /* Using cursor T003013 */
         pr_default.execute(11, new Object[] {A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T003013_A996SolicitacaoMudanca_Codigo[0] > A996SolicitacaoMudanca_Codigo ) || ( T003013_A996SolicitacaoMudanca_Codigo[0] == A996SolicitacaoMudanca_Codigo ) && ( T003013_A995SolicitacaoMudancaItem_FuncaoAPF[0] > A995SolicitacaoMudancaItem_FuncaoAPF ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T003013_A996SolicitacaoMudanca_Codigo[0] < A996SolicitacaoMudanca_Codigo ) || ( T003013_A996SolicitacaoMudanca_Codigo[0] == A996SolicitacaoMudanca_Codigo ) && ( T003013_A995SolicitacaoMudancaItem_FuncaoAPF[0] < A995SolicitacaoMudancaItem_FuncaoAPF ) ) )
            {
               A996SolicitacaoMudanca_Codigo = T003013_A996SolicitacaoMudanca_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
               A995SolicitacaoMudancaItem_FuncaoAPF = T003013_A995SolicitacaoMudancaItem_FuncaoAPF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
               RcdFound122 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey30122( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtSolicitacaoMudancaItem_FuncaoAPF_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert30122( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound122 == 1 )
            {
               if ( ( A996SolicitacaoMudanca_Codigo != Z996SolicitacaoMudanca_Codigo ) || ( A995SolicitacaoMudancaItem_FuncaoAPF != Z995SolicitacaoMudancaItem_FuncaoAPF ) )
               {
                  A996SolicitacaoMudanca_Codigo = Z996SolicitacaoMudanca_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
                  A995SolicitacaoMudancaItem_FuncaoAPF = Z995SolicitacaoMudancaItem_FuncaoAPF;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SOLICITACAOMUDANCA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacaoMudanca_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtSolicitacaoMudancaItem_FuncaoAPF_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update30122( ) ;
                  GX_FocusControl = edtSolicitacaoMudancaItem_FuncaoAPF_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A996SolicitacaoMudanca_Codigo != Z996SolicitacaoMudanca_Codigo ) || ( A995SolicitacaoMudancaItem_FuncaoAPF != Z995SolicitacaoMudancaItem_FuncaoAPF ) )
               {
                  /* Insert record */
                  GX_FocusControl = edtSolicitacaoMudancaItem_FuncaoAPF_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert30122( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SOLICITACAOMUDANCA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtSolicitacaoMudanca_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtSolicitacaoMudancaItem_FuncaoAPF_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert30122( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A996SolicitacaoMudanca_Codigo != Z996SolicitacaoMudanca_Codigo ) || ( A995SolicitacaoMudancaItem_FuncaoAPF != Z995SolicitacaoMudancaItem_FuncaoAPF ) )
         {
            A996SolicitacaoMudanca_Codigo = Z996SolicitacaoMudanca_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
            A995SolicitacaoMudancaItem_FuncaoAPF = Z995SolicitacaoMudancaItem_FuncaoAPF;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SOLICITACAOMUDANCA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacaoMudanca_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtSolicitacaoMudancaItem_FuncaoAPF_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency30122( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00302 */
            pr_default.execute(0, new Object[] {A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SolicitacaoMudancaItem"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"SolicitacaoMudancaItem"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert30122( )
      {
         BeforeValidate30122( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable30122( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM30122( 0) ;
            CheckOptimisticConcurrency30122( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm30122( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert30122( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003014 */
                     pr_default.execute(12, new Object[] {A998SolicitacaoMudancaItem_Descricao, A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoMudancaItem") ;
                     if ( (pr_default.getStatus(12) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption300( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load30122( ) ;
            }
            EndLevel30122( ) ;
         }
         CloseExtendedTableCursors30122( ) ;
      }

      protected void Update30122( )
      {
         BeforeValidate30122( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable30122( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency30122( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm30122( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate30122( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003015 */
                     pr_default.execute(13, new Object[] {A998SolicitacaoMudancaItem_Descricao, A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoMudancaItem") ;
                     if ( (pr_default.getStatus(13) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SolicitacaoMudancaItem"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate30122( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel30122( ) ;
         }
         CloseExtendedTableCursors30122( ) ;
      }

      protected void DeferredUpdate30122( )
      {
      }

      protected void delete( )
      {
         BeforeValidate30122( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency30122( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls30122( ) ;
            AfterConfirm30122( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete30122( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003016 */
                  pr_default.execute(14, new Object[] {A996SolicitacaoMudanca_Codigo, A995SolicitacaoMudancaItem_FuncaoAPF});
                  pr_default.close(14);
                  dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoMudancaItem") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode122 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel30122( ) ;
         Gx_mode = sMode122;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls30122( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003017 */
            pr_default.execute(15, new Object[] {A996SolicitacaoMudanca_Codigo});
            A993SolicitacaoMudanca_SistemaCod = T003017_A993SolicitacaoMudanca_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
            n993SolicitacaoMudanca_SistemaCod = T003017_n993SolicitacaoMudanca_SistemaCod[0];
            pr_default.close(15);
         }
      }

      protected void EndLevel30122( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete30122( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(15);
            context.CommitDataStores( "SolicitacaoMudancaItem");
            if ( AnyError == 0 )
            {
               ConfirmValues300( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(15);
            context.RollbackDataStores( "SolicitacaoMudancaItem");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart30122( )
      {
         /* Scan By routine */
         /* Using cursor T003018 */
         pr_default.execute(16);
         RcdFound122 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound122 = 1;
            A996SolicitacaoMudanca_Codigo = T003018_A996SolicitacaoMudanca_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
            A995SolicitacaoMudancaItem_FuncaoAPF = T003018_A995SolicitacaoMudancaItem_FuncaoAPF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext30122( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound122 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound122 = 1;
            A996SolicitacaoMudanca_Codigo = T003018_A996SolicitacaoMudanca_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
            A995SolicitacaoMudancaItem_FuncaoAPF = T003018_A995SolicitacaoMudancaItem_FuncaoAPF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
         }
      }

      protected void ScanEnd30122( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm30122( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert30122( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate30122( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete30122( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete30122( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate30122( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes30122( )
      {
         edtSolicitacaoMudanca_SistemaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudanca_SistemaCod_Enabled), 5, 0)));
         edtSolicitacaoMudancaItem_FuncaoAPF_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudancaItem_FuncaoAPF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudancaItem_FuncaoAPF_Enabled), 5, 0)));
         edtSolicitacaoMudanca_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudanca_Codigo_Enabled), 5, 0)));
         Solicitacaomudancaitem_descricao_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Solicitacaomudancaitem_descricao_Internalname, "Enabled", StringUtil.BoolToStr( Solicitacaomudancaitem_descricao_Enabled));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues300( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205322403685");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("solicitacaomudancaitem.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7SolicitacaoMudanca_Codigo) + "," + UrlEncode("" +AV8SolicitacaoMudancaItem_FuncaoAPF)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "GXHCSOLICITACAOMUDANCAITEM_FUNCAOAPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z996SolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z995SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV10TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV10TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vSOLICITACAOMUDANCA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSOLICITACAOMUDANCAITEM_FUNCAOAPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SOLICITACAOMUDANCAITEM_DESCRICAO", A998SolicitacaoMudancaItem_Descricao);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSOLICITACAOMUDANCA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7SolicitacaoMudanca_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSOLICITACAOMUDANCAITEM_FUNCAOAPF", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8SolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SOLICITACAOMUDANCAITEM_DESCRICAO_Enabled", StringUtil.BoolToStr( Solicitacaomudancaitem_descricao_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "SolicitacaoMudancaItem";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("solicitacaomudancaitem:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("solicitacaomudancaitem.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7SolicitacaoMudanca_Codigo) + "," + UrlEncode("" +AV8SolicitacaoMudancaItem_FuncaoAPF) ;
      }

      public override String GetPgmname( )
      {
         return "SolicitacaoMudancaItem" ;
      }

      public override String GetPgmdesc( )
      {
         return "Item da Mudan�a" ;
      }

      protected void InitializeNonKey30122( )
      {
         A993SolicitacaoMudanca_SistemaCod = 0;
         n993SolicitacaoMudanca_SistemaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
         A998SolicitacaoMudancaItem_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A998SolicitacaoMudancaItem_Descricao", A998SolicitacaoMudancaItem_Descricao);
      }

      protected void InitAll30122( )
      {
         A996SolicitacaoMudanca_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
         A995SolicitacaoMudancaItem_FuncaoAPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A995SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
         InitializeNonKey30122( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205322403698");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("solicitacaomudancaitem.js", "?20205322403698");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocksolicitacaomudanca_sistemacod_Internalname = "TEXTBLOCKSOLICITACAOMUDANCA_SISTEMACOD";
         edtSolicitacaoMudanca_SistemaCod_Internalname = "SOLICITACAOMUDANCA_SISTEMACOD";
         lblTextblocksolicitacaomudancaitem_funcaoapf_Internalname = "TEXTBLOCKSOLICITACAOMUDANCAITEM_FUNCAOAPF";
         edtSolicitacaoMudancaItem_FuncaoAPF_Internalname = "SOLICITACAOMUDANCAITEM_FUNCAOAPF";
         lblTextblocksolicitacaomudancaitem_descricao_Internalname = "TEXTBLOCKSOLICITACAOMUDANCAITEM_DESCRICAO";
         Solicitacaomudancaitem_descricao_Internalname = "SOLICITACAOMUDANCAITEM_DESCRICAO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtSolicitacaoMudanca_Codigo_Internalname = "SOLICITACAOMUDANCA_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Item da Mudan�a";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Item da Mudan�a";
         Solicitacaomudancaitem_descricao_Enabled = Convert.ToBoolean( 1);
         edtSolicitacaoMudancaItem_FuncaoAPF_Jsonclick = "";
         edtSolicitacaoMudancaItem_FuncaoAPF_Enabled = 1;
         edtSolicitacaoMudanca_SistemaCod_Jsonclick = "";
         edtSolicitacaoMudanca_SistemaCod_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtSolicitacaoMudanca_Codigo_Jsonclick = "";
         edtSolicitacaoMudanca_Codigo_Enabled = 1;
         edtSolicitacaoMudanca_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXSGASOLICITACAOMUDANCAITEM_FUNCAOAPF300( int A993SolicitacaoMudanca_SistemaCod ,
                                                               String A166FuncaoAPF_Nome )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXSGASOLICITACAOMUDANCAITEM_FUNCAOAPF_data300( A993SolicitacaoMudanca_SistemaCod, A166FuncaoAPF_Nome) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXSGASOLICITACAOMUDANCAITEM_FUNCAOAPF_data300( int A993SolicitacaoMudanca_SistemaCod ,
                                                                    String A166FuncaoAPF_Nome )
      {
         l166FuncaoAPF_Nome = StringUtil.Concat( StringUtil.RTrim( A166FuncaoAPF_Nome), "%", "");
         /* Using cursor T003019 */
         pr_default.execute(17, new Object[] {l166FuncaoAPF_Nome, n993SolicitacaoMudanca_SistemaCod, A993SolicitacaoMudanca_SistemaCod});
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         while ( (pr_default.getStatus(17) != 101) )
         {
            gxdynajaxctrlcodr.Add(T003019_A166FuncaoAPF_Nome[0]);
            gxdynajaxctrldescr.Add(T003019_A166FuncaoAPF_Nome[0]);
            pr_default.readNext(17);
         }
         pr_default.close(17);
      }

      protected void GXHCASOLICITACAOMUDANCAITEM_FUNCAOAPF30122( int A993SolicitacaoMudanca_SistemaCod ,
                                                                 String A166FuncaoAPF_Nome )
      {
         /* Using cursor T003020 */
         pr_default.execute(18, new Object[] {A166FuncaoAPF_Nome, n993SolicitacaoMudanca_SistemaCod, A993SolicitacaoMudanca_SistemaCod});
         gxhchits = 0;
         while ( (pr_default.getStatus(18) != 101) )
         {
            gxhchits = (short)(gxhchits+1);
            if ( gxhchits > 1 )
            {
               if (true) break;
            }
            A166FuncaoAPF_Nome = T003020_A166FuncaoAPF_Nome[0];
            A183FuncaoAPF_Ativo = T003020_A183FuncaoAPF_Ativo[0];
            A360FuncaoAPF_SistemaCod = T003020_A360FuncaoAPF_SistemaCod[0];
            n360FuncaoAPF_SistemaCod = T003020_n360FuncaoAPF_SistemaCod[0];
            A165FuncaoAPF_Codigo = T003020_A165FuncaoAPF_Codigo[0];
            pr_default.readNext(18);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( gxhchits > 1 )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("\"ambiguousck\"");
         }
         if ( gxhchits == 0 )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(18);
      }

      public void Valid_Solicitacaomudancaitem_funcaoapf( String GX_Parm1 ,
                                                          int GX_Parm2 )
      {
         h995SolicitacaoMudancaItem_FuncaoAPF = GX_Parm1;
         A995SolicitacaoMudancaItem_FuncaoAPF = GX_Parm2;
         /* Using cursor T003021 */
         pr_default.execute(19, new Object[] {A995SolicitacaoMudancaItem_FuncaoAPF});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacao Mudanca Item_Funcao APF'.", "ForeignKeyNotFound", 1, "SOLICITACAOMUDANCAITEM_FUNCAOAPF");
            AnyError = 1;
            GX_FocusControl = edtSolicitacaoMudancaItem_FuncaoAPF_Internalname;
         }
         pr_default.close(19);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Solicitacaomudanca_codigo( String GX_Parm1 ,
                                                   int GX_Parm2 ,
                                                   int GX_Parm3 ,
                                                   int GX_Parm4 )
      {
         h995SolicitacaoMudancaItem_FuncaoAPF = GX_Parm1;
         A996SolicitacaoMudanca_Codigo = GX_Parm2;
         A995SolicitacaoMudancaItem_FuncaoAPF = GX_Parm3;
         A993SolicitacaoMudanca_SistemaCod = GX_Parm4;
         n993SolicitacaoMudanca_SistemaCod = false;
         /* Using cursor T003022 */
         pr_default.execute(20, new Object[] {A996SolicitacaoMudanca_Codigo});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicita��o de Mudan�a'.", "ForeignKeyNotFound", 1, "SOLICITACAOMUDANCA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacaoMudanca_Codigo_Internalname;
         }
         A993SolicitacaoMudanca_SistemaCod = T003022_A993SolicitacaoMudanca_SistemaCod[0];
         n993SolicitacaoMudanca_SistemaCod = T003022_n993SolicitacaoMudanca_SistemaCod[0];
         pr_default.close(20);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( h995SolicitacaoMudancaItem_FuncaoAPF)) )
         {
            A995SolicitacaoMudancaItem_FuncaoAPF = 0;
         }
         else
         {
            A166FuncaoAPF_Nome = h995SolicitacaoMudancaItem_FuncaoAPF;
            /* Using cursor T003023 */
            pr_default.execute(21, new Object[] {A166FuncaoAPF_Nome, n993SolicitacaoMudanca_SistemaCod, A993SolicitacaoMudanca_SistemaCod});
            A995SolicitacaoMudancaItem_FuncaoAPF = T003023_A165FuncaoAPF_Codigo[0];
            if ( ! ( (pr_default.getStatus(21) == 101) ) )
            {
               pr_default.readNext(21);
               if ( ! ( (pr_default.getStatus(21) == 101) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_ambiguousck", new   object[]  {"Fun��o de Transa��o"}), 1, "SOLICITACAOMUDANCAITEM_FUNCAOAPF");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacaoMudancaItem_FuncaoAPF_Internalname;
               }
            }
            else
            {
            }
            pr_default.close(21);
         }
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "h995SolicitacaoMudancaItem_FuncaoAPF", h995SolicitacaoMudancaItem_FuncaoAPF);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A993SolicitacaoMudanca_SistemaCod = 0;
            n993SolicitacaoMudanca_SistemaCod = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ".", "")));
         isValidOutput.Add(h995SolicitacaoMudancaItem_FuncaoAPF);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7SolicitacaoMudanca_Codigo',fld:'vSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8SolicitacaoMudancaItem_FuncaoAPF',fld:'vSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12302',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(20);
         pr_default.close(15);
         pr_default.close(19);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A166FuncaoAPF_Nome = "";
         h995SolicitacaoMudancaItem_FuncaoAPF = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocksolicitacaomudanca_sistemacod_Jsonclick = "";
         lblTextblocksolicitacaomudancaitem_funcaoapf_Jsonclick = "";
         lblTextblocksolicitacaomudancaitem_descricao_Jsonclick = "";
         A998SolicitacaoMudancaItem_Descricao = "";
         Solicitacaomudancaitem_descricao_Width = "";
         Solicitacaomudancaitem_descricao_Height = "";
         Solicitacaomudancaitem_descricao_Skin = "";
         Solicitacaomudancaitem_descricao_Toolbar = "";
         Solicitacaomudancaitem_descricao_Class = "";
         Solicitacaomudancaitem_descricao_Customtoolbar = "";
         Solicitacaomudancaitem_descricao_Customconfiguration = "";
         Solicitacaomudancaitem_descricao_Buttonpressedid = "";
         Solicitacaomudancaitem_descricao_Captionvalue = "";
         Solicitacaomudancaitem_descricao_Captionclass = "";
         Solicitacaomudancaitem_descricao_Captionposition = "";
         Solicitacaomudancaitem_descricao_Coltitle = "";
         Solicitacaomudancaitem_descricao_Coltitlefont = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode122 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         Z998SolicitacaoMudancaItem_Descricao = "";
         T00306_A166FuncaoAPF_Nome = new String[] {""} ;
         T00306_A183FuncaoAPF_Ativo = new String[] {""} ;
         T00306_A360FuncaoAPF_SistemaCod = new int[1] ;
         T00306_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         T00306_A165FuncaoAPF_Codigo = new int[1] ;
         A183FuncaoAPF_Ativo = "";
         T00304_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         T00304_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         T00307_A998SolicitacaoMudancaItem_Descricao = new String[] {""} ;
         T00307_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T00307_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         T00307_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         T00307_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         T00308_A166FuncaoAPF_Nome = new String[] {""} ;
         T00308_A183FuncaoAPF_Ativo = new String[] {""} ;
         T00308_A360FuncaoAPF_SistemaCod = new int[1] ;
         T00308_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         T00308_A165FuncaoAPF_Codigo = new int[1] ;
         T00305_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         T00309_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         T00309_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         T003010_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         T003011_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T003011_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         T00303_A998SolicitacaoMudancaItem_Descricao = new String[] {""} ;
         T00303_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T00303_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         T003012_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T003012_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         T003013_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T003013_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         T00302_A998SolicitacaoMudancaItem_Descricao = new String[] {""} ;
         T00302_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T00302_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         T003017_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         T003017_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         T003018_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T003018_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         l166FuncaoAPF_Nome = "";
         T003019_A166FuncaoAPF_Nome = new String[] {""} ;
         T003020_A166FuncaoAPF_Nome = new String[] {""} ;
         T003020_A183FuncaoAPF_Ativo = new String[] {""} ;
         T003020_A360FuncaoAPF_SistemaCod = new int[1] ;
         T003020_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         T003020_A165FuncaoAPF_Codigo = new int[1] ;
         T003021_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         T003022_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         T003022_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         T003023_A166FuncaoAPF_Nome = new String[] {""} ;
         T003023_A183FuncaoAPF_Ativo = new String[] {""} ;
         T003023_A360FuncaoAPF_SistemaCod = new int[1] ;
         T003023_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         T003023_A165FuncaoAPF_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacaomudancaitem__default(),
            new Object[][] {
                new Object[] {
               T00302_A998SolicitacaoMudancaItem_Descricao, T00302_A996SolicitacaoMudanca_Codigo, T00302_A995SolicitacaoMudancaItem_FuncaoAPF
               }
               , new Object[] {
               T00303_A998SolicitacaoMudancaItem_Descricao, T00303_A996SolicitacaoMudanca_Codigo, T00303_A995SolicitacaoMudancaItem_FuncaoAPF
               }
               , new Object[] {
               T00304_A993SolicitacaoMudanca_SistemaCod, T00304_n993SolicitacaoMudanca_SistemaCod
               }
               , new Object[] {
               T00305_A995SolicitacaoMudancaItem_FuncaoAPF
               }
               , new Object[] {
               T00306_A166FuncaoAPF_Nome, T00306_A183FuncaoAPF_Ativo, T00306_A360FuncaoAPF_SistemaCod, T00306_n360FuncaoAPF_SistemaCod, T00306_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T00307_A998SolicitacaoMudancaItem_Descricao, T00307_A996SolicitacaoMudanca_Codigo, T00307_A995SolicitacaoMudancaItem_FuncaoAPF, T00307_A993SolicitacaoMudanca_SistemaCod, T00307_n993SolicitacaoMudanca_SistemaCod
               }
               , new Object[] {
               T00308_A166FuncaoAPF_Nome, T00308_A183FuncaoAPF_Ativo, T00308_A360FuncaoAPF_SistemaCod, T00308_n360FuncaoAPF_SistemaCod, T00308_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T00309_A993SolicitacaoMudanca_SistemaCod, T00309_n993SolicitacaoMudanca_SistemaCod
               }
               , new Object[] {
               T003010_A995SolicitacaoMudancaItem_FuncaoAPF
               }
               , new Object[] {
               T003011_A996SolicitacaoMudanca_Codigo, T003011_A995SolicitacaoMudancaItem_FuncaoAPF
               }
               , new Object[] {
               T003012_A996SolicitacaoMudanca_Codigo, T003012_A995SolicitacaoMudancaItem_FuncaoAPF
               }
               , new Object[] {
               T003013_A996SolicitacaoMudanca_Codigo, T003013_A995SolicitacaoMudancaItem_FuncaoAPF
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003017_A993SolicitacaoMudanca_SistemaCod, T003017_n993SolicitacaoMudanca_SistemaCod
               }
               , new Object[] {
               T003018_A996SolicitacaoMudanca_Codigo, T003018_A995SolicitacaoMudancaItem_FuncaoAPF
               }
               , new Object[] {
               T003019_A166FuncaoAPF_Nome
               }
               , new Object[] {
               T003020_A166FuncaoAPF_Nome, T003020_A183FuncaoAPF_Ativo, T003020_A360FuncaoAPF_SistemaCod, T003020_n360FuncaoAPF_SistemaCod, T003020_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T003021_A995SolicitacaoMudancaItem_FuncaoAPF
               }
               , new Object[] {
               T003022_A993SolicitacaoMudanca_SistemaCod, T003022_n993SolicitacaoMudanca_SistemaCod
               }
               , new Object[] {
               T003023_A166FuncaoAPF_Nome, T003023_A183FuncaoAPF_Ativo, T003023_A360FuncaoAPF_SistemaCod, T003023_n360FuncaoAPF_SistemaCod, T003023_A165FuncaoAPF_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound122 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short gxhchits ;
      private short wbTemp ;
      private int wcpOAV7SolicitacaoMudanca_Codigo ;
      private int wcpOAV8SolicitacaoMudancaItem_FuncaoAPF ;
      private int Z996SolicitacaoMudanca_Codigo ;
      private int Z995SolicitacaoMudancaItem_FuncaoAPF ;
      private int A993SolicitacaoMudanca_SistemaCod ;
      private int A996SolicitacaoMudanca_Codigo ;
      private int A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int AV7SolicitacaoMudanca_Codigo ;
      private int AV8SolicitacaoMudancaItem_FuncaoAPF ;
      private int trnEnded ;
      private int edtSolicitacaoMudanca_Codigo_Visible ;
      private int edtSolicitacaoMudanca_Codigo_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtSolicitacaoMudanca_SistemaCod_Enabled ;
      private int edtSolicitacaoMudancaItem_FuncaoAPF_Enabled ;
      private int Solicitacaomudancaitem_descricao_Color ;
      private int Solicitacaomudancaitem_descricao_Coltitlecolor ;
      private int Z993SolicitacaoMudanca_SistemaCod ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A165FuncaoAPF_Codigo ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtSolicitacaoMudancaItem_FuncaoAPF_Internalname ;
      private String TempTags ;
      private String edtSolicitacaoMudanca_Codigo_Internalname ;
      private String edtSolicitacaoMudanca_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocksolicitacaomudanca_sistemacod_Internalname ;
      private String lblTextblocksolicitacaomudanca_sistemacod_Jsonclick ;
      private String edtSolicitacaoMudanca_SistemaCod_Internalname ;
      private String edtSolicitacaoMudanca_SistemaCod_Jsonclick ;
      private String lblTextblocksolicitacaomudancaitem_funcaoapf_Internalname ;
      private String lblTextblocksolicitacaomudancaitem_funcaoapf_Jsonclick ;
      private String edtSolicitacaoMudancaItem_FuncaoAPF_Jsonclick ;
      private String lblTextblocksolicitacaomudancaitem_descricao_Internalname ;
      private String lblTextblocksolicitacaomudancaitem_descricao_Jsonclick ;
      private String Solicitacaomudancaitem_descricao_Width ;
      private String Solicitacaomudancaitem_descricao_Height ;
      private String Solicitacaomudancaitem_descricao_Skin ;
      private String Solicitacaomudancaitem_descricao_Toolbar ;
      private String Solicitacaomudancaitem_descricao_Class ;
      private String Solicitacaomudancaitem_descricao_Customtoolbar ;
      private String Solicitacaomudancaitem_descricao_Customconfiguration ;
      private String Solicitacaomudancaitem_descricao_Buttonpressedid ;
      private String Solicitacaomudancaitem_descricao_Captionvalue ;
      private String Solicitacaomudancaitem_descricao_Captionclass ;
      private String Solicitacaomudancaitem_descricao_Captionposition ;
      private String Solicitacaomudancaitem_descricao_Coltitle ;
      private String Solicitacaomudancaitem_descricao_Coltitlefont ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode122 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String A183FuncaoAPF_Ativo ;
      private String Solicitacaomudancaitem_descricao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool n993SolicitacaoMudanca_SistemaCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Solicitacaomudancaitem_descricao_Enabled ;
      private bool Solicitacaomudancaitem_descricao_Toolbarcancollapse ;
      private bool Solicitacaomudancaitem_descricao_Toolbarexpanded ;
      private bool Solicitacaomudancaitem_descricao_Usercontroliscolumn ;
      private bool Solicitacaomudancaitem_descricao_Visible ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool n360FuncaoAPF_SistemaCod ;
      private String A998SolicitacaoMudancaItem_Descricao ;
      private String Z998SolicitacaoMudancaItem_Descricao ;
      private String A166FuncaoAPF_Nome ;
      private String h995SolicitacaoMudancaItem_FuncaoAPF ;
      private String l166FuncaoAPF_Nome ;
      private IGxSession AV11WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T00306_A166FuncaoAPF_Nome ;
      private String[] T00306_A183FuncaoAPF_Ativo ;
      private int[] T00306_A360FuncaoAPF_SistemaCod ;
      private bool[] T00306_n360FuncaoAPF_SistemaCod ;
      private int[] T00306_A165FuncaoAPF_Codigo ;
      private int[] T00304_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] T00304_n993SolicitacaoMudanca_SistemaCod ;
      private String[] T00307_A998SolicitacaoMudancaItem_Descricao ;
      private int[] T00307_A996SolicitacaoMudanca_Codigo ;
      private int[] T00307_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int[] T00307_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] T00307_n993SolicitacaoMudanca_SistemaCod ;
      private String[] T00308_A166FuncaoAPF_Nome ;
      private String[] T00308_A183FuncaoAPF_Ativo ;
      private int[] T00308_A360FuncaoAPF_SistemaCod ;
      private bool[] T00308_n360FuncaoAPF_SistemaCod ;
      private int[] T00308_A165FuncaoAPF_Codigo ;
      private int[] T00305_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int[] T00309_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] T00309_n993SolicitacaoMudanca_SistemaCod ;
      private int[] T003010_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int[] T003011_A996SolicitacaoMudanca_Codigo ;
      private int[] T003011_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private String[] T00303_A998SolicitacaoMudancaItem_Descricao ;
      private int[] T00303_A996SolicitacaoMudanca_Codigo ;
      private int[] T00303_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int[] T003012_A996SolicitacaoMudanca_Codigo ;
      private int[] T003012_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int[] T003013_A996SolicitacaoMudanca_Codigo ;
      private int[] T003013_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private String[] T00302_A998SolicitacaoMudancaItem_Descricao ;
      private int[] T00302_A996SolicitacaoMudanca_Codigo ;
      private int[] T00302_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int[] T003017_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] T003017_n993SolicitacaoMudanca_SistemaCod ;
      private int[] T003018_A996SolicitacaoMudanca_Codigo ;
      private int[] T003018_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private String[] T003019_A166FuncaoAPF_Nome ;
      private String[] T003020_A166FuncaoAPF_Nome ;
      private String[] T003020_A183FuncaoAPF_Ativo ;
      private int[] T003020_A360FuncaoAPF_SistemaCod ;
      private bool[] T003020_n360FuncaoAPF_SistemaCod ;
      private int[] T003020_A165FuncaoAPF_Codigo ;
      private int[] T003021_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int[] T003022_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] T003022_n993SolicitacaoMudanca_SistemaCod ;
      private String[] T003023_A166FuncaoAPF_Nome ;
      private String[] T003023_A183FuncaoAPF_Ativo ;
      private int[] T003023_A360FuncaoAPF_SistemaCod ;
      private bool[] T003023_n360FuncaoAPF_SistemaCod ;
      private int[] T003023_A165FuncaoAPF_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
   }

   public class solicitacaomudancaitem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00306 ;
          prmT00306 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00307 ;
          prmT00307 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00308 ;
          prmT00308 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00304 ;
          prmT00304 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00305 ;
          prmT00305 = new Object[] {
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00309 ;
          prmT00309 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003010 ;
          prmT003010 = new Object[] {
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003011 ;
          prmT003011 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00303 ;
          prmT00303 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003012 ;
          prmT003012 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003013 ;
          prmT003013 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00302 ;
          prmT00302 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003014 ;
          prmT003014 = new Object[] {
          new Object[] {"@SolicitacaoMudancaItem_Descricao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003015 ;
          prmT003015 = new Object[] {
          new Object[] {"@SolicitacaoMudancaItem_Descricao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003016 ;
          prmT003016 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003017 ;
          prmT003017 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003018 ;
          prmT003018 = new Object[] {
          } ;
          Object[] prmT003019 ;
          prmT003019 = new Object[] {
          new Object[] {"@l166FuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@SolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003020 ;
          prmT003020 = new Object[] {
          new Object[] {"@FuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@SolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003021 ;
          prmT003021 = new Object[] {
          new Object[] {"@SolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003022 ;
          prmT003022 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003023 ;
          prmT003023 = new Object[] {
          new Object[] {"@FuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@SolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00302", "SELECT [SolicitacaoMudancaItem_Descricao], [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF] AS SolicitacaoMudancaItem_FuncaoAPF FROM [SolicitacaoMudancaItem] WITH (UPDLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo AND [SolicitacaoMudancaItem_FuncaoAPF] = @SolicitacaoMudancaItem_FuncaoAPF ",true, GxErrorMask.GX_NOMASK, false, this,prmT00302,1,0,true,false )
             ,new CursorDef("T00303", "SELECT [SolicitacaoMudancaItem_Descricao], [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF] AS SolicitacaoMudancaItem_FuncaoAPF FROM [SolicitacaoMudancaItem] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo AND [SolicitacaoMudancaItem_FuncaoAPF] = @SolicitacaoMudancaItem_FuncaoAPF ",true, GxErrorMask.GX_NOMASK, false, this,prmT00303,1,0,true,false )
             ,new CursorDef("T00304", "SELECT [SolicitacaoMudanca_SistemaCod] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00304,1,0,true,false )
             ,new CursorDef("T00305", "SELECT [FuncaoAPF_Codigo] AS SolicitacaoMudancaItem_FuncaoAPF FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @SolicitacaoMudancaItem_FuncaoAPF ",true, GxErrorMask.GX_NOMASK, false, this,prmT00305,1,0,true,false )
             ,new CursorDef("T00306", "SELECT [FuncaoAPF_Nome], [FuncaoAPF_Ativo], [FuncaoAPF_SistemaCod], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ([FuncaoAPF_Ativo] = 1) AND ([FuncaoAPF_SistemaCod] = @SolicitacaoMudanca_SistemaCod) AND ([FuncaoAPF_Codigo] = @SolicitacaoMudancaItem_FuncaoAPF) ",true, GxErrorMask.GX_NOMASK, false, this,prmT00306,0,0,true,false )
             ,new CursorDef("T00307", "SELECT TM1.[SolicitacaoMudancaItem_Descricao], TM1.[SolicitacaoMudanca_Codigo], TM1.[SolicitacaoMudancaItem_FuncaoAPF] AS SolicitacaoMudancaItem_FuncaoAPF, T2.[SolicitacaoMudanca_SistemaCod] FROM ([SolicitacaoMudancaItem] TM1 WITH (NOLOCK) INNER JOIN [SolicitacaoMudanca] T2 WITH (NOLOCK) ON T2.[SolicitacaoMudanca_Codigo] = TM1.[SolicitacaoMudanca_Codigo]) WHERE TM1.[SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo and TM1.[SolicitacaoMudancaItem_FuncaoAPF] = @SolicitacaoMudancaItem_FuncaoAPF ORDER BY TM1.[SolicitacaoMudanca_Codigo], TM1.[SolicitacaoMudancaItem_FuncaoAPF]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00307,100,0,true,false )
             ,new CursorDef("T00308", "SELECT [FuncaoAPF_Nome], [FuncaoAPF_Ativo], [FuncaoAPF_SistemaCod], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ([FuncaoAPF_Ativo] = 1) AND ([FuncaoAPF_SistemaCod] = @SolicitacaoMudanca_SistemaCod) AND ([FuncaoAPF_Codigo] = @SolicitacaoMudancaItem_FuncaoAPF) ",true, GxErrorMask.GX_NOMASK, false, this,prmT00308,0,0,true,false )
             ,new CursorDef("T00309", "SELECT [SolicitacaoMudanca_SistemaCod] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00309,1,0,true,false )
             ,new CursorDef("T003010", "SELECT [FuncaoAPF_Codigo] AS SolicitacaoMudancaItem_FuncaoAPF FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @SolicitacaoMudancaItem_FuncaoAPF ",true, GxErrorMask.GX_NOMASK, false, this,prmT003010,1,0,true,false )
             ,new CursorDef("T003011", "SELECT [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF] AS SolicitacaoMudancaItem_FuncaoAPF FROM [SolicitacaoMudancaItem] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo AND [SolicitacaoMudancaItem_FuncaoAPF] = @SolicitacaoMudancaItem_FuncaoAPF  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003011,1,0,true,false )
             ,new CursorDef("T003012", "SELECT TOP 1 [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF] AS SolicitacaoMudancaItem_FuncaoAPF FROM [SolicitacaoMudancaItem] WITH (NOLOCK) WHERE ( [SolicitacaoMudanca_Codigo] > @SolicitacaoMudanca_Codigo or [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo and [SolicitacaoMudancaItem_FuncaoAPF] > @SolicitacaoMudancaItem_FuncaoAPF) ORDER BY [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003012,1,0,true,true )
             ,new CursorDef("T003013", "SELECT TOP 1 [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF] AS SolicitacaoMudancaItem_FuncaoAPF FROM [SolicitacaoMudancaItem] WITH (NOLOCK) WHERE ( [SolicitacaoMudanca_Codigo] < @SolicitacaoMudanca_Codigo or [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo and [SolicitacaoMudancaItem_FuncaoAPF] < @SolicitacaoMudancaItem_FuncaoAPF) ORDER BY [SolicitacaoMudanca_Codigo] DESC, [SolicitacaoMudancaItem_FuncaoAPF] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003013,1,0,true,true )
             ,new CursorDef("T003014", "INSERT INTO [SolicitacaoMudancaItem]([SolicitacaoMudancaItem_Descricao], [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF]) VALUES(@SolicitacaoMudancaItem_Descricao, @SolicitacaoMudanca_Codigo, @SolicitacaoMudancaItem_FuncaoAPF)", GxErrorMask.GX_NOMASK,prmT003014)
             ,new CursorDef("T003015", "UPDATE [SolicitacaoMudancaItem] SET [SolicitacaoMudancaItem_Descricao]=@SolicitacaoMudancaItem_Descricao  WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo AND [SolicitacaoMudancaItem_FuncaoAPF] = @SolicitacaoMudancaItem_FuncaoAPF", GxErrorMask.GX_NOMASK,prmT003015)
             ,new CursorDef("T003016", "DELETE FROM [SolicitacaoMudancaItem]  WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo AND [SolicitacaoMudancaItem_FuncaoAPF] = @SolicitacaoMudancaItem_FuncaoAPF", GxErrorMask.GX_NOMASK,prmT003016)
             ,new CursorDef("T003017", "SELECT [SolicitacaoMudanca_SistemaCod] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003017,1,0,true,false )
             ,new CursorDef("T003018", "SELECT [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF] AS SolicitacaoMudancaItem_FuncaoAPF FROM [SolicitacaoMudancaItem] WITH (NOLOCK) ORDER BY [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003018,100,0,true,false )
             ,new CursorDef("T003019", "SELECT DISTINCT TOP 10 [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE (UPPER([FuncaoAPF_Nome]) like '%' + UPPER(@l166FuncaoAPF_Nome)) AND ([FuncaoAPF_Ativo] = 1) AND ([FuncaoAPF_SistemaCod] = @SolicitacaoMudanca_SistemaCod) ORDER BY [FuncaoAPF_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003019,0,0,true,false )
             ,new CursorDef("T003020", "SELECT [FuncaoAPF_Nome], [FuncaoAPF_Ativo], [FuncaoAPF_SistemaCod], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ([FuncaoAPF_Nome] = @FuncaoAPF_Nome) AND ([FuncaoAPF_Ativo] = 1) AND ([FuncaoAPF_SistemaCod] = @SolicitacaoMudanca_SistemaCod) ",true, GxErrorMask.GX_NOMASK, false, this,prmT003020,0,0,true,false )
             ,new CursorDef("T003021", "SELECT [FuncaoAPF_Codigo] AS SolicitacaoMudancaItem_FuncaoAPF FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @SolicitacaoMudancaItem_FuncaoAPF ",true, GxErrorMask.GX_NOMASK, false, this,prmT003021,1,0,true,false )
             ,new CursorDef("T003022", "SELECT [SolicitacaoMudanca_SistemaCod] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003022,1,0,true,false )
             ,new CursorDef("T003023", "SELECT [FuncaoAPF_Nome], [FuncaoAPF_Ativo], [FuncaoAPF_SistemaCod], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ([FuncaoAPF_Nome] = @FuncaoAPF_Nome) AND ([FuncaoAPF_Ativo] = 1) AND ([FuncaoAPF_SistemaCod] = @SolicitacaoMudanca_SistemaCod)  OPTION (FAST 0)",true, GxErrorMask.GX_NOMASK, false, this,prmT003023,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 13 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                return;
             case 18 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                return;
       }
    }

 }

}
