/*
               File: PRC_TemIndicadorPnt
        Description: Tem Indicador de Pontualidade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:9.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_temindicadorpnt : GXProcedure
   {
      public prc_temindicadorpnt( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_temindicadorpnt( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                           out bool aP1_TemIndicadorDePontoalidade ,
                           out bool aP2_TemIndicadorPntLote )
      {
         this.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         this.AV8TemIndicadorDePontoalidade = false ;
         this.AV9TemIndicadorPntLote = false ;
         initialize();
         executePrivate();
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP1_TemIndicadorDePontoalidade=this.AV8TemIndicadorDePontoalidade;
         aP2_TemIndicadorPntLote=this.AV9TemIndicadorPntLote;
      }

      public bool executeUdp( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                              out bool aP1_TemIndicadorDePontoalidade )
      {
         this.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         this.AV8TemIndicadorDePontoalidade = false ;
         this.AV9TemIndicadorPntLote = false ;
         initialize();
         executePrivate();
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP1_TemIndicadorDePontoalidade=this.AV8TemIndicadorDePontoalidade;
         aP2_TemIndicadorPntLote=this.AV9TemIndicadorPntLote;
         return AV9TemIndicadorPntLote ;
      }

      public void executeSubmit( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                                 out bool aP1_TemIndicadorDePontoalidade ,
                                 out bool aP2_TemIndicadorPntLote )
      {
         prc_temindicadorpnt objprc_temindicadorpnt;
         objprc_temindicadorpnt = new prc_temindicadorpnt();
         objprc_temindicadorpnt.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         objprc_temindicadorpnt.AV8TemIndicadorDePontoalidade = false ;
         objprc_temindicadorpnt.AV9TemIndicadorPntLote = false ;
         objprc_temindicadorpnt.context.SetSubmitInitialConfig(context);
         objprc_temindicadorpnt.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_temindicadorpnt);
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP1_TemIndicadorDePontoalidade=this.AV8TemIndicadorDePontoalidade;
         aP2_TemIndicadorPntLote=this.AV9TemIndicadorPntLote;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_temindicadorpnt)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00WJ2 */
         pr_default.execute(0, new Object[] {A1270ContratoServicosIndicador_CntSrvCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1308ContratoServicosIndicador_Tipo = P00WJ2_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P00WJ2_n1308ContratoServicosIndicador_Tipo[0];
            A1269ContratoServicosIndicador_Codigo = P00WJ2_A1269ContratoServicosIndicador_Codigo[0];
            if ( StringUtil.StrCmp(A1308ContratoServicosIndicador_Tipo, "P") == 0 )
            {
               AV8TemIndicadorDePontoalidade = true;
            }
            else if ( StringUtil.StrCmp(A1308ContratoServicosIndicador_Tipo, "PL") == 0 )
            {
               AV9TemIndicadorPntLote = true;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WJ2_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P00WJ2_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P00WJ2_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P00WJ2_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         A1308ContratoServicosIndicador_Tipo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_temindicadorpnt__default(),
            new Object[][] {
                new Object[] {
               P00WJ2_A1270ContratoServicosIndicador_CntSrvCod, P00WJ2_A1308ContratoServicosIndicador_Tipo, P00WJ2_n1308ContratoServicosIndicador_Tipo, P00WJ2_A1269ContratoServicosIndicador_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private String scmdbuf ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private bool AV9TemIndicadorPntLote ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private bool AV8TemIndicadorDePontoalidade ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicosIndicador_CntSrvCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00WJ2_A1270ContratoServicosIndicador_CntSrvCod ;
      private String[] P00WJ2_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P00WJ2_n1308ContratoServicosIndicador_Tipo ;
      private int[] P00WJ2_A1269ContratoServicosIndicador_Codigo ;
      private bool aP1_TemIndicadorDePontoalidade ;
      private bool aP2_TemIndicadorPntLote ;
   }

   public class prc_temindicadorpnt__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WJ2 ;
          prmP00WJ2 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WJ2", "SELECT [ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE ([ContratoServicosIndicador_CntSrvCod] = @ContratoServicosIndicador_CntSrvCod) AND ([ContratoServicosIndicador_Tipo] = 'P' or [ContratoServicosIndicador_Tipo] = 'PL') ORDER BY [ContratoServicosIndicador_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WJ2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
