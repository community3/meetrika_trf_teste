/*
               File: GetWWPessoaFilterData
        Description: Get WWPessoa Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:11.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwpessoafilterdata : GXProcedure
   {
      public getwwpessoafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwpessoafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
         return AV28OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwpessoafilterdata objgetwwpessoafilterdata;
         objgetwwpessoafilterdata = new getwwpessoafilterdata();
         objgetwwpessoafilterdata.AV19DDOName = aP0_DDOName;
         objgetwwpessoafilterdata.AV17SearchTxt = aP1_SearchTxt;
         objgetwwpessoafilterdata.AV18SearchTxtTo = aP2_SearchTxtTo;
         objgetwwpessoafilterdata.AV23OptionsJson = "" ;
         objgetwwpessoafilterdata.AV26OptionsDescJson = "" ;
         objgetwwpessoafilterdata.AV28OptionIndexesJson = "" ;
         objgetwwpessoafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwpessoafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwpessoafilterdata);
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwpessoafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV22Options = (IGxCollection)(new GxSimpleCollection());
         AV25OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV27OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_PESSOA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADPESSOA_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_PESSOA_DOCTO") == 0 )
         {
            /* Execute user subroutine: 'LOADPESSOA_DOCTOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_PESSOA_IE") == 0 )
         {
            /* Execute user subroutine: 'LOADPESSOA_IEOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_PESSOA_ENDERECO") == 0 )
         {
            /* Execute user subroutine: 'LOADPESSOA_ENDERECOOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_PESSOA_UF") == 0 )
         {
            /* Execute user subroutine: 'LOADPESSOA_UFOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_PESSOA_CEP") == 0 )
         {
            /* Execute user subroutine: 'LOADPESSOA_CEPOPTIONS' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_PESSOA_TELEFONE") == 0 )
         {
            /* Execute user subroutine: 'LOADPESSOA_TELEFONEOPTIONS' */
            S181 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_PESSOA_FAX") == 0 )
         {
            /* Execute user subroutine: 'LOADPESSOA_FAXOPTIONS' */
            S191 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV23OptionsJson = AV22Options.ToJSonString(false);
         AV26OptionsDescJson = AV25OptionsDesc.ToJSonString(false);
         AV28OptionIndexesJson = AV27OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get("WWPessoaGridState"), "") == 0 )
         {
            AV32GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWPessoaGridState"), "");
         }
         else
         {
            AV32GridState.FromXml(AV30Session.Get("WWPessoaGridState"), "");
         }
         AV68GXV1 = 1;
         while ( AV68GXV1 <= AV32GridState.gxTpr_Filtervalues.Count )
         {
            AV33GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV32GridState.gxTpr_Filtervalues.Item(AV68GXV1));
            if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_NOME") == 0 )
            {
               AV10TFPessoa_Nome = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_NOME_SEL") == 0 )
            {
               AV11TFPessoa_Nome_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_TIPOPESSOA_SEL") == 0 )
            {
               AV12TFPessoa_TipoPessoa_SelsJson = AV33GridStateFilterValue.gxTpr_Value;
               AV13TFPessoa_TipoPessoa_Sels.FromJSonString(AV12TFPessoa_TipoPessoa_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_DOCTO") == 0 )
            {
               AV14TFPessoa_Docto = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_DOCTO_SEL") == 0 )
            {
               AV15TFPessoa_Docto_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_IE") == 0 )
            {
               AV52TFPessoa_IE = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_IE_SEL") == 0 )
            {
               AV53TFPessoa_IE_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_ENDERECO") == 0 )
            {
               AV54TFPessoa_Endereco = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_ENDERECO_SEL") == 0 )
            {
               AV55TFPessoa_Endereco_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_MUNICIPIOCOD") == 0 )
            {
               AV56TFPessoa_MunicipioCod = (int)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
               AV57TFPessoa_MunicipioCod_To = (int)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_UF") == 0 )
            {
               AV58TFPessoa_UF = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_UF_SEL") == 0 )
            {
               AV59TFPessoa_UF_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_CEP") == 0 )
            {
               AV60TFPessoa_CEP = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_CEP_SEL") == 0 )
            {
               AV61TFPessoa_CEP_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_TELEFONE") == 0 )
            {
               AV62TFPessoa_Telefone = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_TELEFONE_SEL") == 0 )
            {
               AV63TFPessoa_Telefone_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_FAX") == 0 )
            {
               AV64TFPessoa_Fax = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_FAX_SEL") == 0 )
            {
               AV65TFPessoa_Fax_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFPESSOA_ATIVO_SEL") == 0 )
            {
               AV16TFPessoa_Ativo_Sel = (short)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
            }
            AV68GXV1 = (int)(AV68GXV1+1);
         }
         if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(1));
            AV35DynamicFiltersSelector1 = AV34GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "PESSOA_NOME") == 0 )
            {
               AV36DynamicFiltersOperator1 = AV34GridStateDynamicFilter.gxTpr_Operator;
               AV37Pessoa_Nome1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "PESSOA_TIPOPESSOA") == 0 )
            {
               AV38Pessoa_TipoPessoa1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "PESSOA_DOCTO") == 0 )
            {
               AV36DynamicFiltersOperator1 = AV34GridStateDynamicFilter.gxTpr_Operator;
               AV39Pessoa_Docto1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV40DynamicFiltersEnabled2 = true;
               AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(2));
               AV41DynamicFiltersSelector2 = AV34GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "PESSOA_NOME") == 0 )
               {
                  AV42DynamicFiltersOperator2 = AV34GridStateDynamicFilter.gxTpr_Operator;
                  AV43Pessoa_Nome2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "PESSOA_TIPOPESSOA") == 0 )
               {
                  AV44Pessoa_TipoPessoa2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "PESSOA_DOCTO") == 0 )
               {
                  AV42DynamicFiltersOperator2 = AV34GridStateDynamicFilter.gxTpr_Operator;
                  AV45Pessoa_Docto2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV46DynamicFiltersEnabled3 = true;
                  AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(3));
                  AV47DynamicFiltersSelector3 = AV34GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV47DynamicFiltersSelector3, "PESSOA_NOME") == 0 )
                  {
                     AV48DynamicFiltersOperator3 = AV34GridStateDynamicFilter.gxTpr_Operator;
                     AV49Pessoa_Nome3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV47DynamicFiltersSelector3, "PESSOA_TIPOPESSOA") == 0 )
                  {
                     AV50Pessoa_TipoPessoa3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV47DynamicFiltersSelector3, "PESSOA_DOCTO") == 0 )
                  {
                     AV48DynamicFiltersOperator3 = AV34GridStateDynamicFilter.gxTpr_Operator;
                     AV51Pessoa_Docto3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADPESSOA_NOMEOPTIONS' Routine */
         AV10TFPessoa_Nome = AV17SearchTxt;
         AV11TFPessoa_Nome_Sel = "";
         AV70WWPessoaDS_1_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV71WWPessoaDS_2_Dynamicfiltersoperator1 = AV36DynamicFiltersOperator1;
         AV72WWPessoaDS_3_Pessoa_nome1 = AV37Pessoa_Nome1;
         AV73WWPessoaDS_4_Pessoa_tipopessoa1 = AV38Pessoa_TipoPessoa1;
         AV74WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV75WWPessoaDS_6_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV76WWPessoaDS_7_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV77WWPessoaDS_8_Dynamicfiltersoperator2 = AV42DynamicFiltersOperator2;
         AV78WWPessoaDS_9_Pessoa_nome2 = AV43Pessoa_Nome2;
         AV79WWPessoaDS_10_Pessoa_tipopessoa2 = AV44Pessoa_TipoPessoa2;
         AV80WWPessoaDS_11_Pessoa_docto2 = AV45Pessoa_Docto2;
         AV81WWPessoaDS_12_Dynamicfiltersenabled3 = AV46DynamicFiltersEnabled3;
         AV82WWPessoaDS_13_Dynamicfiltersselector3 = AV47DynamicFiltersSelector3;
         AV83WWPessoaDS_14_Dynamicfiltersoperator3 = AV48DynamicFiltersOperator3;
         AV84WWPessoaDS_15_Pessoa_nome3 = AV49Pessoa_Nome3;
         AV85WWPessoaDS_16_Pessoa_tipopessoa3 = AV50Pessoa_TipoPessoa3;
         AV86WWPessoaDS_17_Pessoa_docto3 = AV51Pessoa_Docto3;
         AV87WWPessoaDS_18_Tfpessoa_nome = AV10TFPessoa_Nome;
         AV88WWPessoaDS_19_Tfpessoa_nome_sel = AV11TFPessoa_Nome_Sel;
         AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV13TFPessoa_TipoPessoa_Sels;
         AV90WWPessoaDS_21_Tfpessoa_docto = AV14TFPessoa_Docto;
         AV91WWPessoaDS_22_Tfpessoa_docto_sel = AV15TFPessoa_Docto_Sel;
         AV92WWPessoaDS_23_Tfpessoa_ie = AV52TFPessoa_IE;
         AV93WWPessoaDS_24_Tfpessoa_ie_sel = AV53TFPessoa_IE_Sel;
         AV94WWPessoaDS_25_Tfpessoa_endereco = AV54TFPessoa_Endereco;
         AV95WWPessoaDS_26_Tfpessoa_endereco_sel = AV55TFPessoa_Endereco_Sel;
         AV96WWPessoaDS_27_Tfpessoa_municipiocod = AV56TFPessoa_MunicipioCod;
         AV97WWPessoaDS_28_Tfpessoa_municipiocod_to = AV57TFPessoa_MunicipioCod_To;
         AV98WWPessoaDS_29_Tfpessoa_uf = AV58TFPessoa_UF;
         AV99WWPessoaDS_30_Tfpessoa_uf_sel = AV59TFPessoa_UF_Sel;
         AV100WWPessoaDS_31_Tfpessoa_cep = AV60TFPessoa_CEP;
         AV101WWPessoaDS_32_Tfpessoa_cep_sel = AV61TFPessoa_CEP_Sel;
         AV102WWPessoaDS_33_Tfpessoa_telefone = AV62TFPessoa_Telefone;
         AV103WWPessoaDS_34_Tfpessoa_telefone_sel = AV63TFPessoa_Telefone_Sel;
         AV104WWPessoaDS_35_Tfpessoa_fax = AV64TFPessoa_Fax;
         AV105WWPessoaDS_36_Tfpessoa_fax_sel = AV65TFPessoa_Fax_Sel;
         AV106WWPessoaDS_37_Tfpessoa_ativo_sel = AV16TFPessoa_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A36Pessoa_TipoPessoa ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                              AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                              AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWPessoaDS_3_Pessoa_nome1 ,
                                              AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                              AV74WWPessoaDS_5_Pessoa_docto1 ,
                                              AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                              AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                              AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                              AV78WWPessoaDS_9_Pessoa_nome2 ,
                                              AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                              AV80WWPessoaDS_11_Pessoa_docto2 ,
                                              AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                              AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                              AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                              AV84WWPessoaDS_15_Pessoa_nome3 ,
                                              AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                              AV86WWPessoaDS_17_Pessoa_docto3 ,
                                              AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                              AV87WWPessoaDS_18_Tfpessoa_nome ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels.Count ,
                                              AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                              AV90WWPessoaDS_21_Tfpessoa_docto ,
                                              AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                              AV92WWPessoaDS_23_Tfpessoa_ie ,
                                              AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                              AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                              AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                              AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                              AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                              AV98WWPessoaDS_29_Tfpessoa_uf ,
                                              AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                              AV100WWPessoaDS_31_Tfpessoa_cep ,
                                              AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                              AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                              AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                              AV104WWPessoaDS_35_Tfpessoa_fax ,
                                              AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                              AV107Usuario_pessoacod ,
                                              A35Pessoa_Nome ,
                                              A37Pessoa_Docto ,
                                              A518Pessoa_IE ,
                                              A519Pessoa_Endereco ,
                                              A503Pessoa_MunicipioCod ,
                                              A520Pessoa_UF ,
                                              A521Pessoa_CEP ,
                                              A522Pessoa_Telefone ,
                                              A523Pessoa_Fax ,
                                              A38Pessoa_Ativo ,
                                              A34Pessoa_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV87WWPessoaDS_18_Tfpessoa_nome = StringUtil.PadR( StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome), 100, "%");
         lV90WWPessoaDS_21_Tfpessoa_docto = StringUtil.Concat( StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto), "%", "");
         lV92WWPessoaDS_23_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie), 15, "%");
         lV94WWPessoaDS_25_Tfpessoa_endereco = StringUtil.Concat( StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco), "%", "");
         lV98WWPessoaDS_29_Tfpessoa_uf = StringUtil.PadR( StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf), 2, "%");
         lV100WWPessoaDS_31_Tfpessoa_cep = StringUtil.PadR( StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep), 10, "%");
         lV102WWPessoaDS_33_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone), 15, "%");
         lV104WWPessoaDS_35_Tfpessoa_fax = StringUtil.PadR( StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax), 15, "%");
         /* Using cursor P00F22 */
         pr_default.execute(0, new Object[] {lV72WWPessoaDS_3_Pessoa_nome1, lV72WWPessoaDS_3_Pessoa_nome1, AV73WWPessoaDS_4_Pessoa_tipopessoa1, lV74WWPessoaDS_5_Pessoa_docto1, lV74WWPessoaDS_5_Pessoa_docto1, lV78WWPessoaDS_9_Pessoa_nome2, lV78WWPessoaDS_9_Pessoa_nome2, AV79WWPessoaDS_10_Pessoa_tipopessoa2, lV80WWPessoaDS_11_Pessoa_docto2, lV80WWPessoaDS_11_Pessoa_docto2, lV84WWPessoaDS_15_Pessoa_nome3, lV84WWPessoaDS_15_Pessoa_nome3, AV85WWPessoaDS_16_Pessoa_tipopessoa3, lV86WWPessoaDS_17_Pessoa_docto3, lV86WWPessoaDS_17_Pessoa_docto3, lV87WWPessoaDS_18_Tfpessoa_nome, AV88WWPessoaDS_19_Tfpessoa_nome_sel, lV90WWPessoaDS_21_Tfpessoa_docto, AV91WWPessoaDS_22_Tfpessoa_docto_sel, lV92WWPessoaDS_23_Tfpessoa_ie, AV93WWPessoaDS_24_Tfpessoa_ie_sel, lV94WWPessoaDS_25_Tfpessoa_endereco, AV95WWPessoaDS_26_Tfpessoa_endereco_sel, AV96WWPessoaDS_27_Tfpessoa_municipiocod, AV97WWPessoaDS_28_Tfpessoa_municipiocod_to, lV98WWPessoaDS_29_Tfpessoa_uf, AV99WWPessoaDS_30_Tfpessoa_uf_sel, lV100WWPessoaDS_31_Tfpessoa_cep, AV101WWPessoaDS_32_Tfpessoa_cep_sel, lV102WWPessoaDS_33_Tfpessoa_telefone, AV103WWPessoaDS_34_Tfpessoa_telefone_sel, lV104WWPessoaDS_35_Tfpessoa_fax, AV105WWPessoaDS_36_Tfpessoa_fax_sel, AV107Usuario_pessoacod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKF22 = false;
            A35Pessoa_Nome = P00F22_A35Pessoa_Nome[0];
            A34Pessoa_Codigo = P00F22_A34Pessoa_Codigo[0];
            A38Pessoa_Ativo = P00F22_A38Pessoa_Ativo[0];
            A523Pessoa_Fax = P00F22_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00F22_n523Pessoa_Fax[0];
            A522Pessoa_Telefone = P00F22_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00F22_n522Pessoa_Telefone[0];
            A521Pessoa_CEP = P00F22_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00F22_n521Pessoa_CEP[0];
            A520Pessoa_UF = P00F22_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F22_n520Pessoa_UF[0];
            A503Pessoa_MunicipioCod = P00F22_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P00F22_n503Pessoa_MunicipioCod[0];
            A519Pessoa_Endereco = P00F22_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00F22_n519Pessoa_Endereco[0];
            A518Pessoa_IE = P00F22_A518Pessoa_IE[0];
            n518Pessoa_IE = P00F22_n518Pessoa_IE[0];
            A37Pessoa_Docto = P00F22_A37Pessoa_Docto[0];
            A36Pessoa_TipoPessoa = P00F22_A36Pessoa_TipoPessoa[0];
            A520Pessoa_UF = P00F22_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F22_n520Pessoa_UF[0];
            AV29count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00F22_A35Pessoa_Nome[0], A35Pessoa_Nome) == 0 ) )
            {
               BRKF22 = false;
               A34Pessoa_Codigo = P00F22_A34Pessoa_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKF22 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A35Pessoa_Nome)) )
            {
               AV21Option = A35Pessoa_Nome;
               AV22Options.Add(AV21Option, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKF22 )
            {
               BRKF22 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADPESSOA_DOCTOOPTIONS' Routine */
         AV14TFPessoa_Docto = AV17SearchTxt;
         AV15TFPessoa_Docto_Sel = "";
         AV70WWPessoaDS_1_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV71WWPessoaDS_2_Dynamicfiltersoperator1 = AV36DynamicFiltersOperator1;
         AV72WWPessoaDS_3_Pessoa_nome1 = AV37Pessoa_Nome1;
         AV73WWPessoaDS_4_Pessoa_tipopessoa1 = AV38Pessoa_TipoPessoa1;
         AV74WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV75WWPessoaDS_6_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV76WWPessoaDS_7_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV77WWPessoaDS_8_Dynamicfiltersoperator2 = AV42DynamicFiltersOperator2;
         AV78WWPessoaDS_9_Pessoa_nome2 = AV43Pessoa_Nome2;
         AV79WWPessoaDS_10_Pessoa_tipopessoa2 = AV44Pessoa_TipoPessoa2;
         AV80WWPessoaDS_11_Pessoa_docto2 = AV45Pessoa_Docto2;
         AV81WWPessoaDS_12_Dynamicfiltersenabled3 = AV46DynamicFiltersEnabled3;
         AV82WWPessoaDS_13_Dynamicfiltersselector3 = AV47DynamicFiltersSelector3;
         AV83WWPessoaDS_14_Dynamicfiltersoperator3 = AV48DynamicFiltersOperator3;
         AV84WWPessoaDS_15_Pessoa_nome3 = AV49Pessoa_Nome3;
         AV85WWPessoaDS_16_Pessoa_tipopessoa3 = AV50Pessoa_TipoPessoa3;
         AV86WWPessoaDS_17_Pessoa_docto3 = AV51Pessoa_Docto3;
         AV87WWPessoaDS_18_Tfpessoa_nome = AV10TFPessoa_Nome;
         AV88WWPessoaDS_19_Tfpessoa_nome_sel = AV11TFPessoa_Nome_Sel;
         AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV13TFPessoa_TipoPessoa_Sels;
         AV90WWPessoaDS_21_Tfpessoa_docto = AV14TFPessoa_Docto;
         AV91WWPessoaDS_22_Tfpessoa_docto_sel = AV15TFPessoa_Docto_Sel;
         AV92WWPessoaDS_23_Tfpessoa_ie = AV52TFPessoa_IE;
         AV93WWPessoaDS_24_Tfpessoa_ie_sel = AV53TFPessoa_IE_Sel;
         AV94WWPessoaDS_25_Tfpessoa_endereco = AV54TFPessoa_Endereco;
         AV95WWPessoaDS_26_Tfpessoa_endereco_sel = AV55TFPessoa_Endereco_Sel;
         AV96WWPessoaDS_27_Tfpessoa_municipiocod = AV56TFPessoa_MunicipioCod;
         AV97WWPessoaDS_28_Tfpessoa_municipiocod_to = AV57TFPessoa_MunicipioCod_To;
         AV98WWPessoaDS_29_Tfpessoa_uf = AV58TFPessoa_UF;
         AV99WWPessoaDS_30_Tfpessoa_uf_sel = AV59TFPessoa_UF_Sel;
         AV100WWPessoaDS_31_Tfpessoa_cep = AV60TFPessoa_CEP;
         AV101WWPessoaDS_32_Tfpessoa_cep_sel = AV61TFPessoa_CEP_Sel;
         AV102WWPessoaDS_33_Tfpessoa_telefone = AV62TFPessoa_Telefone;
         AV103WWPessoaDS_34_Tfpessoa_telefone_sel = AV63TFPessoa_Telefone_Sel;
         AV104WWPessoaDS_35_Tfpessoa_fax = AV64TFPessoa_Fax;
         AV105WWPessoaDS_36_Tfpessoa_fax_sel = AV65TFPessoa_Fax_Sel;
         AV106WWPessoaDS_37_Tfpessoa_ativo_sel = AV16TFPessoa_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A36Pessoa_TipoPessoa ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                              AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                              AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWPessoaDS_3_Pessoa_nome1 ,
                                              AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                              AV74WWPessoaDS_5_Pessoa_docto1 ,
                                              AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                              AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                              AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                              AV78WWPessoaDS_9_Pessoa_nome2 ,
                                              AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                              AV80WWPessoaDS_11_Pessoa_docto2 ,
                                              AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                              AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                              AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                              AV84WWPessoaDS_15_Pessoa_nome3 ,
                                              AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                              AV86WWPessoaDS_17_Pessoa_docto3 ,
                                              AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                              AV87WWPessoaDS_18_Tfpessoa_nome ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels.Count ,
                                              AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                              AV90WWPessoaDS_21_Tfpessoa_docto ,
                                              AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                              AV92WWPessoaDS_23_Tfpessoa_ie ,
                                              AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                              AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                              AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                              AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                              AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                              AV98WWPessoaDS_29_Tfpessoa_uf ,
                                              AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                              AV100WWPessoaDS_31_Tfpessoa_cep ,
                                              AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                              AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                              AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                              AV104WWPessoaDS_35_Tfpessoa_fax ,
                                              AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                              AV107Usuario_pessoacod ,
                                              A35Pessoa_Nome ,
                                              A37Pessoa_Docto ,
                                              A518Pessoa_IE ,
                                              A519Pessoa_Endereco ,
                                              A503Pessoa_MunicipioCod ,
                                              A520Pessoa_UF ,
                                              A521Pessoa_CEP ,
                                              A522Pessoa_Telefone ,
                                              A523Pessoa_Fax ,
                                              A38Pessoa_Ativo ,
                                              A34Pessoa_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV87WWPessoaDS_18_Tfpessoa_nome = StringUtil.PadR( StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome), 100, "%");
         lV90WWPessoaDS_21_Tfpessoa_docto = StringUtil.Concat( StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto), "%", "");
         lV92WWPessoaDS_23_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie), 15, "%");
         lV94WWPessoaDS_25_Tfpessoa_endereco = StringUtil.Concat( StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco), "%", "");
         lV98WWPessoaDS_29_Tfpessoa_uf = StringUtil.PadR( StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf), 2, "%");
         lV100WWPessoaDS_31_Tfpessoa_cep = StringUtil.PadR( StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep), 10, "%");
         lV102WWPessoaDS_33_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone), 15, "%");
         lV104WWPessoaDS_35_Tfpessoa_fax = StringUtil.PadR( StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax), 15, "%");
         /* Using cursor P00F23 */
         pr_default.execute(1, new Object[] {lV72WWPessoaDS_3_Pessoa_nome1, lV72WWPessoaDS_3_Pessoa_nome1, AV73WWPessoaDS_4_Pessoa_tipopessoa1, lV74WWPessoaDS_5_Pessoa_docto1, lV74WWPessoaDS_5_Pessoa_docto1, lV78WWPessoaDS_9_Pessoa_nome2, lV78WWPessoaDS_9_Pessoa_nome2, AV79WWPessoaDS_10_Pessoa_tipopessoa2, lV80WWPessoaDS_11_Pessoa_docto2, lV80WWPessoaDS_11_Pessoa_docto2, lV84WWPessoaDS_15_Pessoa_nome3, lV84WWPessoaDS_15_Pessoa_nome3, AV85WWPessoaDS_16_Pessoa_tipopessoa3, lV86WWPessoaDS_17_Pessoa_docto3, lV86WWPessoaDS_17_Pessoa_docto3, lV87WWPessoaDS_18_Tfpessoa_nome, AV88WWPessoaDS_19_Tfpessoa_nome_sel, lV90WWPessoaDS_21_Tfpessoa_docto, AV91WWPessoaDS_22_Tfpessoa_docto_sel, lV92WWPessoaDS_23_Tfpessoa_ie, AV93WWPessoaDS_24_Tfpessoa_ie_sel, lV94WWPessoaDS_25_Tfpessoa_endereco, AV95WWPessoaDS_26_Tfpessoa_endereco_sel, AV96WWPessoaDS_27_Tfpessoa_municipiocod, AV97WWPessoaDS_28_Tfpessoa_municipiocod_to, lV98WWPessoaDS_29_Tfpessoa_uf, AV99WWPessoaDS_30_Tfpessoa_uf_sel, lV100WWPessoaDS_31_Tfpessoa_cep, AV101WWPessoaDS_32_Tfpessoa_cep_sel, lV102WWPessoaDS_33_Tfpessoa_telefone, AV103WWPessoaDS_34_Tfpessoa_telefone_sel, lV104WWPessoaDS_35_Tfpessoa_fax, AV105WWPessoaDS_36_Tfpessoa_fax_sel, AV107Usuario_pessoacod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKF24 = false;
            A37Pessoa_Docto = P00F23_A37Pessoa_Docto[0];
            A34Pessoa_Codigo = P00F23_A34Pessoa_Codigo[0];
            A38Pessoa_Ativo = P00F23_A38Pessoa_Ativo[0];
            A523Pessoa_Fax = P00F23_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00F23_n523Pessoa_Fax[0];
            A522Pessoa_Telefone = P00F23_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00F23_n522Pessoa_Telefone[0];
            A521Pessoa_CEP = P00F23_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00F23_n521Pessoa_CEP[0];
            A520Pessoa_UF = P00F23_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F23_n520Pessoa_UF[0];
            A503Pessoa_MunicipioCod = P00F23_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P00F23_n503Pessoa_MunicipioCod[0];
            A519Pessoa_Endereco = P00F23_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00F23_n519Pessoa_Endereco[0];
            A518Pessoa_IE = P00F23_A518Pessoa_IE[0];
            n518Pessoa_IE = P00F23_n518Pessoa_IE[0];
            A36Pessoa_TipoPessoa = P00F23_A36Pessoa_TipoPessoa[0];
            A35Pessoa_Nome = P00F23_A35Pessoa_Nome[0];
            A520Pessoa_UF = P00F23_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F23_n520Pessoa_UF[0];
            AV29count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00F23_A37Pessoa_Docto[0], A37Pessoa_Docto) == 0 ) )
            {
               BRKF24 = false;
               A34Pessoa_Codigo = P00F23_A34Pessoa_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKF24 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A37Pessoa_Docto)) )
            {
               AV21Option = A37Pessoa_Docto;
               AV22Options.Add(AV21Option, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKF24 )
            {
               BRKF24 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADPESSOA_IEOPTIONS' Routine */
         AV52TFPessoa_IE = AV17SearchTxt;
         AV53TFPessoa_IE_Sel = "";
         AV70WWPessoaDS_1_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV71WWPessoaDS_2_Dynamicfiltersoperator1 = AV36DynamicFiltersOperator1;
         AV72WWPessoaDS_3_Pessoa_nome1 = AV37Pessoa_Nome1;
         AV73WWPessoaDS_4_Pessoa_tipopessoa1 = AV38Pessoa_TipoPessoa1;
         AV74WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV75WWPessoaDS_6_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV76WWPessoaDS_7_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV77WWPessoaDS_8_Dynamicfiltersoperator2 = AV42DynamicFiltersOperator2;
         AV78WWPessoaDS_9_Pessoa_nome2 = AV43Pessoa_Nome2;
         AV79WWPessoaDS_10_Pessoa_tipopessoa2 = AV44Pessoa_TipoPessoa2;
         AV80WWPessoaDS_11_Pessoa_docto2 = AV45Pessoa_Docto2;
         AV81WWPessoaDS_12_Dynamicfiltersenabled3 = AV46DynamicFiltersEnabled3;
         AV82WWPessoaDS_13_Dynamicfiltersselector3 = AV47DynamicFiltersSelector3;
         AV83WWPessoaDS_14_Dynamicfiltersoperator3 = AV48DynamicFiltersOperator3;
         AV84WWPessoaDS_15_Pessoa_nome3 = AV49Pessoa_Nome3;
         AV85WWPessoaDS_16_Pessoa_tipopessoa3 = AV50Pessoa_TipoPessoa3;
         AV86WWPessoaDS_17_Pessoa_docto3 = AV51Pessoa_Docto3;
         AV87WWPessoaDS_18_Tfpessoa_nome = AV10TFPessoa_Nome;
         AV88WWPessoaDS_19_Tfpessoa_nome_sel = AV11TFPessoa_Nome_Sel;
         AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV13TFPessoa_TipoPessoa_Sels;
         AV90WWPessoaDS_21_Tfpessoa_docto = AV14TFPessoa_Docto;
         AV91WWPessoaDS_22_Tfpessoa_docto_sel = AV15TFPessoa_Docto_Sel;
         AV92WWPessoaDS_23_Tfpessoa_ie = AV52TFPessoa_IE;
         AV93WWPessoaDS_24_Tfpessoa_ie_sel = AV53TFPessoa_IE_Sel;
         AV94WWPessoaDS_25_Tfpessoa_endereco = AV54TFPessoa_Endereco;
         AV95WWPessoaDS_26_Tfpessoa_endereco_sel = AV55TFPessoa_Endereco_Sel;
         AV96WWPessoaDS_27_Tfpessoa_municipiocod = AV56TFPessoa_MunicipioCod;
         AV97WWPessoaDS_28_Tfpessoa_municipiocod_to = AV57TFPessoa_MunicipioCod_To;
         AV98WWPessoaDS_29_Tfpessoa_uf = AV58TFPessoa_UF;
         AV99WWPessoaDS_30_Tfpessoa_uf_sel = AV59TFPessoa_UF_Sel;
         AV100WWPessoaDS_31_Tfpessoa_cep = AV60TFPessoa_CEP;
         AV101WWPessoaDS_32_Tfpessoa_cep_sel = AV61TFPessoa_CEP_Sel;
         AV102WWPessoaDS_33_Tfpessoa_telefone = AV62TFPessoa_Telefone;
         AV103WWPessoaDS_34_Tfpessoa_telefone_sel = AV63TFPessoa_Telefone_Sel;
         AV104WWPessoaDS_35_Tfpessoa_fax = AV64TFPessoa_Fax;
         AV105WWPessoaDS_36_Tfpessoa_fax_sel = AV65TFPessoa_Fax_Sel;
         AV106WWPessoaDS_37_Tfpessoa_ativo_sel = AV16TFPessoa_Ativo_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A36Pessoa_TipoPessoa ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                              AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                              AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWPessoaDS_3_Pessoa_nome1 ,
                                              AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                              AV74WWPessoaDS_5_Pessoa_docto1 ,
                                              AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                              AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                              AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                              AV78WWPessoaDS_9_Pessoa_nome2 ,
                                              AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                              AV80WWPessoaDS_11_Pessoa_docto2 ,
                                              AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                              AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                              AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                              AV84WWPessoaDS_15_Pessoa_nome3 ,
                                              AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                              AV86WWPessoaDS_17_Pessoa_docto3 ,
                                              AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                              AV87WWPessoaDS_18_Tfpessoa_nome ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels.Count ,
                                              AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                              AV90WWPessoaDS_21_Tfpessoa_docto ,
                                              AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                              AV92WWPessoaDS_23_Tfpessoa_ie ,
                                              AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                              AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                              AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                              AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                              AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                              AV98WWPessoaDS_29_Tfpessoa_uf ,
                                              AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                              AV100WWPessoaDS_31_Tfpessoa_cep ,
                                              AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                              AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                              AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                              AV104WWPessoaDS_35_Tfpessoa_fax ,
                                              AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                              AV107Usuario_pessoacod ,
                                              A35Pessoa_Nome ,
                                              A37Pessoa_Docto ,
                                              A518Pessoa_IE ,
                                              A519Pessoa_Endereco ,
                                              A503Pessoa_MunicipioCod ,
                                              A520Pessoa_UF ,
                                              A521Pessoa_CEP ,
                                              A522Pessoa_Telefone ,
                                              A523Pessoa_Fax ,
                                              A38Pessoa_Ativo ,
                                              A34Pessoa_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV87WWPessoaDS_18_Tfpessoa_nome = StringUtil.PadR( StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome), 100, "%");
         lV90WWPessoaDS_21_Tfpessoa_docto = StringUtil.Concat( StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto), "%", "");
         lV92WWPessoaDS_23_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie), 15, "%");
         lV94WWPessoaDS_25_Tfpessoa_endereco = StringUtil.Concat( StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco), "%", "");
         lV98WWPessoaDS_29_Tfpessoa_uf = StringUtil.PadR( StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf), 2, "%");
         lV100WWPessoaDS_31_Tfpessoa_cep = StringUtil.PadR( StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep), 10, "%");
         lV102WWPessoaDS_33_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone), 15, "%");
         lV104WWPessoaDS_35_Tfpessoa_fax = StringUtil.PadR( StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax), 15, "%");
         /* Using cursor P00F24 */
         pr_default.execute(2, new Object[] {lV72WWPessoaDS_3_Pessoa_nome1, lV72WWPessoaDS_3_Pessoa_nome1, AV73WWPessoaDS_4_Pessoa_tipopessoa1, lV74WWPessoaDS_5_Pessoa_docto1, lV74WWPessoaDS_5_Pessoa_docto1, lV78WWPessoaDS_9_Pessoa_nome2, lV78WWPessoaDS_9_Pessoa_nome2, AV79WWPessoaDS_10_Pessoa_tipopessoa2, lV80WWPessoaDS_11_Pessoa_docto2, lV80WWPessoaDS_11_Pessoa_docto2, lV84WWPessoaDS_15_Pessoa_nome3, lV84WWPessoaDS_15_Pessoa_nome3, AV85WWPessoaDS_16_Pessoa_tipopessoa3, lV86WWPessoaDS_17_Pessoa_docto3, lV86WWPessoaDS_17_Pessoa_docto3, lV87WWPessoaDS_18_Tfpessoa_nome, AV88WWPessoaDS_19_Tfpessoa_nome_sel, lV90WWPessoaDS_21_Tfpessoa_docto, AV91WWPessoaDS_22_Tfpessoa_docto_sel, lV92WWPessoaDS_23_Tfpessoa_ie, AV93WWPessoaDS_24_Tfpessoa_ie_sel, lV94WWPessoaDS_25_Tfpessoa_endereco, AV95WWPessoaDS_26_Tfpessoa_endereco_sel, AV96WWPessoaDS_27_Tfpessoa_municipiocod, AV97WWPessoaDS_28_Tfpessoa_municipiocod_to, lV98WWPessoaDS_29_Tfpessoa_uf, AV99WWPessoaDS_30_Tfpessoa_uf_sel, lV100WWPessoaDS_31_Tfpessoa_cep, AV101WWPessoaDS_32_Tfpessoa_cep_sel, lV102WWPessoaDS_33_Tfpessoa_telefone, AV103WWPessoaDS_34_Tfpessoa_telefone_sel, lV104WWPessoaDS_35_Tfpessoa_fax, AV105WWPessoaDS_36_Tfpessoa_fax_sel, AV107Usuario_pessoacod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKF26 = false;
            A38Pessoa_Ativo = P00F24_A38Pessoa_Ativo[0];
            A518Pessoa_IE = P00F24_A518Pessoa_IE[0];
            n518Pessoa_IE = P00F24_n518Pessoa_IE[0];
            A34Pessoa_Codigo = P00F24_A34Pessoa_Codigo[0];
            A523Pessoa_Fax = P00F24_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00F24_n523Pessoa_Fax[0];
            A522Pessoa_Telefone = P00F24_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00F24_n522Pessoa_Telefone[0];
            A521Pessoa_CEP = P00F24_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00F24_n521Pessoa_CEP[0];
            A520Pessoa_UF = P00F24_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F24_n520Pessoa_UF[0];
            A503Pessoa_MunicipioCod = P00F24_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P00F24_n503Pessoa_MunicipioCod[0];
            A519Pessoa_Endereco = P00F24_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00F24_n519Pessoa_Endereco[0];
            A37Pessoa_Docto = P00F24_A37Pessoa_Docto[0];
            A36Pessoa_TipoPessoa = P00F24_A36Pessoa_TipoPessoa[0];
            A35Pessoa_Nome = P00F24_A35Pessoa_Nome[0];
            A520Pessoa_UF = P00F24_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F24_n520Pessoa_UF[0];
            AV29count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00F24_A518Pessoa_IE[0], A518Pessoa_IE) == 0 ) )
            {
               BRKF26 = false;
               A34Pessoa_Codigo = P00F24_A34Pessoa_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKF26 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A518Pessoa_IE)) )
            {
               AV21Option = A518Pessoa_IE;
               AV22Options.Add(AV21Option, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKF26 )
            {
               BRKF26 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADPESSOA_ENDERECOOPTIONS' Routine */
         AV54TFPessoa_Endereco = AV17SearchTxt;
         AV55TFPessoa_Endereco_Sel = "";
         AV70WWPessoaDS_1_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV71WWPessoaDS_2_Dynamicfiltersoperator1 = AV36DynamicFiltersOperator1;
         AV72WWPessoaDS_3_Pessoa_nome1 = AV37Pessoa_Nome1;
         AV73WWPessoaDS_4_Pessoa_tipopessoa1 = AV38Pessoa_TipoPessoa1;
         AV74WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV75WWPessoaDS_6_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV76WWPessoaDS_7_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV77WWPessoaDS_8_Dynamicfiltersoperator2 = AV42DynamicFiltersOperator2;
         AV78WWPessoaDS_9_Pessoa_nome2 = AV43Pessoa_Nome2;
         AV79WWPessoaDS_10_Pessoa_tipopessoa2 = AV44Pessoa_TipoPessoa2;
         AV80WWPessoaDS_11_Pessoa_docto2 = AV45Pessoa_Docto2;
         AV81WWPessoaDS_12_Dynamicfiltersenabled3 = AV46DynamicFiltersEnabled3;
         AV82WWPessoaDS_13_Dynamicfiltersselector3 = AV47DynamicFiltersSelector3;
         AV83WWPessoaDS_14_Dynamicfiltersoperator3 = AV48DynamicFiltersOperator3;
         AV84WWPessoaDS_15_Pessoa_nome3 = AV49Pessoa_Nome3;
         AV85WWPessoaDS_16_Pessoa_tipopessoa3 = AV50Pessoa_TipoPessoa3;
         AV86WWPessoaDS_17_Pessoa_docto3 = AV51Pessoa_Docto3;
         AV87WWPessoaDS_18_Tfpessoa_nome = AV10TFPessoa_Nome;
         AV88WWPessoaDS_19_Tfpessoa_nome_sel = AV11TFPessoa_Nome_Sel;
         AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV13TFPessoa_TipoPessoa_Sels;
         AV90WWPessoaDS_21_Tfpessoa_docto = AV14TFPessoa_Docto;
         AV91WWPessoaDS_22_Tfpessoa_docto_sel = AV15TFPessoa_Docto_Sel;
         AV92WWPessoaDS_23_Tfpessoa_ie = AV52TFPessoa_IE;
         AV93WWPessoaDS_24_Tfpessoa_ie_sel = AV53TFPessoa_IE_Sel;
         AV94WWPessoaDS_25_Tfpessoa_endereco = AV54TFPessoa_Endereco;
         AV95WWPessoaDS_26_Tfpessoa_endereco_sel = AV55TFPessoa_Endereco_Sel;
         AV96WWPessoaDS_27_Tfpessoa_municipiocod = AV56TFPessoa_MunicipioCod;
         AV97WWPessoaDS_28_Tfpessoa_municipiocod_to = AV57TFPessoa_MunicipioCod_To;
         AV98WWPessoaDS_29_Tfpessoa_uf = AV58TFPessoa_UF;
         AV99WWPessoaDS_30_Tfpessoa_uf_sel = AV59TFPessoa_UF_Sel;
         AV100WWPessoaDS_31_Tfpessoa_cep = AV60TFPessoa_CEP;
         AV101WWPessoaDS_32_Tfpessoa_cep_sel = AV61TFPessoa_CEP_Sel;
         AV102WWPessoaDS_33_Tfpessoa_telefone = AV62TFPessoa_Telefone;
         AV103WWPessoaDS_34_Tfpessoa_telefone_sel = AV63TFPessoa_Telefone_Sel;
         AV104WWPessoaDS_35_Tfpessoa_fax = AV64TFPessoa_Fax;
         AV105WWPessoaDS_36_Tfpessoa_fax_sel = AV65TFPessoa_Fax_Sel;
         AV106WWPessoaDS_37_Tfpessoa_ativo_sel = AV16TFPessoa_Ativo_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A36Pessoa_TipoPessoa ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                              AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                              AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWPessoaDS_3_Pessoa_nome1 ,
                                              AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                              AV74WWPessoaDS_5_Pessoa_docto1 ,
                                              AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                              AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                              AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                              AV78WWPessoaDS_9_Pessoa_nome2 ,
                                              AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                              AV80WWPessoaDS_11_Pessoa_docto2 ,
                                              AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                              AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                              AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                              AV84WWPessoaDS_15_Pessoa_nome3 ,
                                              AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                              AV86WWPessoaDS_17_Pessoa_docto3 ,
                                              AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                              AV87WWPessoaDS_18_Tfpessoa_nome ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels.Count ,
                                              AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                              AV90WWPessoaDS_21_Tfpessoa_docto ,
                                              AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                              AV92WWPessoaDS_23_Tfpessoa_ie ,
                                              AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                              AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                              AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                              AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                              AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                              AV98WWPessoaDS_29_Tfpessoa_uf ,
                                              AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                              AV100WWPessoaDS_31_Tfpessoa_cep ,
                                              AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                              AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                              AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                              AV104WWPessoaDS_35_Tfpessoa_fax ,
                                              AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                              AV107Usuario_pessoacod ,
                                              A35Pessoa_Nome ,
                                              A37Pessoa_Docto ,
                                              A518Pessoa_IE ,
                                              A519Pessoa_Endereco ,
                                              A503Pessoa_MunicipioCod ,
                                              A520Pessoa_UF ,
                                              A521Pessoa_CEP ,
                                              A522Pessoa_Telefone ,
                                              A523Pessoa_Fax ,
                                              A38Pessoa_Ativo ,
                                              A34Pessoa_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV87WWPessoaDS_18_Tfpessoa_nome = StringUtil.PadR( StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome), 100, "%");
         lV90WWPessoaDS_21_Tfpessoa_docto = StringUtil.Concat( StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto), "%", "");
         lV92WWPessoaDS_23_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie), 15, "%");
         lV94WWPessoaDS_25_Tfpessoa_endereco = StringUtil.Concat( StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco), "%", "");
         lV98WWPessoaDS_29_Tfpessoa_uf = StringUtil.PadR( StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf), 2, "%");
         lV100WWPessoaDS_31_Tfpessoa_cep = StringUtil.PadR( StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep), 10, "%");
         lV102WWPessoaDS_33_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone), 15, "%");
         lV104WWPessoaDS_35_Tfpessoa_fax = StringUtil.PadR( StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax), 15, "%");
         /* Using cursor P00F25 */
         pr_default.execute(3, new Object[] {lV72WWPessoaDS_3_Pessoa_nome1, lV72WWPessoaDS_3_Pessoa_nome1, AV73WWPessoaDS_4_Pessoa_tipopessoa1, lV74WWPessoaDS_5_Pessoa_docto1, lV74WWPessoaDS_5_Pessoa_docto1, lV78WWPessoaDS_9_Pessoa_nome2, lV78WWPessoaDS_9_Pessoa_nome2, AV79WWPessoaDS_10_Pessoa_tipopessoa2, lV80WWPessoaDS_11_Pessoa_docto2, lV80WWPessoaDS_11_Pessoa_docto2, lV84WWPessoaDS_15_Pessoa_nome3, lV84WWPessoaDS_15_Pessoa_nome3, AV85WWPessoaDS_16_Pessoa_tipopessoa3, lV86WWPessoaDS_17_Pessoa_docto3, lV86WWPessoaDS_17_Pessoa_docto3, lV87WWPessoaDS_18_Tfpessoa_nome, AV88WWPessoaDS_19_Tfpessoa_nome_sel, lV90WWPessoaDS_21_Tfpessoa_docto, AV91WWPessoaDS_22_Tfpessoa_docto_sel, lV92WWPessoaDS_23_Tfpessoa_ie, AV93WWPessoaDS_24_Tfpessoa_ie_sel, lV94WWPessoaDS_25_Tfpessoa_endereco, AV95WWPessoaDS_26_Tfpessoa_endereco_sel, AV96WWPessoaDS_27_Tfpessoa_municipiocod, AV97WWPessoaDS_28_Tfpessoa_municipiocod_to, lV98WWPessoaDS_29_Tfpessoa_uf, AV99WWPessoaDS_30_Tfpessoa_uf_sel, lV100WWPessoaDS_31_Tfpessoa_cep, AV101WWPessoaDS_32_Tfpessoa_cep_sel, lV102WWPessoaDS_33_Tfpessoa_telefone, AV103WWPessoaDS_34_Tfpessoa_telefone_sel, lV104WWPessoaDS_35_Tfpessoa_fax, AV105WWPessoaDS_36_Tfpessoa_fax_sel, AV107Usuario_pessoacod});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKF28 = false;
            A38Pessoa_Ativo = P00F25_A38Pessoa_Ativo[0];
            A519Pessoa_Endereco = P00F25_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00F25_n519Pessoa_Endereco[0];
            A34Pessoa_Codigo = P00F25_A34Pessoa_Codigo[0];
            A523Pessoa_Fax = P00F25_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00F25_n523Pessoa_Fax[0];
            A522Pessoa_Telefone = P00F25_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00F25_n522Pessoa_Telefone[0];
            A521Pessoa_CEP = P00F25_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00F25_n521Pessoa_CEP[0];
            A520Pessoa_UF = P00F25_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F25_n520Pessoa_UF[0];
            A503Pessoa_MunicipioCod = P00F25_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P00F25_n503Pessoa_MunicipioCod[0];
            A518Pessoa_IE = P00F25_A518Pessoa_IE[0];
            n518Pessoa_IE = P00F25_n518Pessoa_IE[0];
            A37Pessoa_Docto = P00F25_A37Pessoa_Docto[0];
            A36Pessoa_TipoPessoa = P00F25_A36Pessoa_TipoPessoa[0];
            A35Pessoa_Nome = P00F25_A35Pessoa_Nome[0];
            A520Pessoa_UF = P00F25_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F25_n520Pessoa_UF[0];
            AV29count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00F25_A519Pessoa_Endereco[0], A519Pessoa_Endereco) == 0 ) )
            {
               BRKF28 = false;
               A34Pessoa_Codigo = P00F25_A34Pessoa_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKF28 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A519Pessoa_Endereco)) )
            {
               AV21Option = A519Pessoa_Endereco;
               AV22Options.Add(AV21Option, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKF28 )
            {
               BRKF28 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADPESSOA_UFOPTIONS' Routine */
         AV58TFPessoa_UF = AV17SearchTxt;
         AV59TFPessoa_UF_Sel = "";
         AV70WWPessoaDS_1_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV71WWPessoaDS_2_Dynamicfiltersoperator1 = AV36DynamicFiltersOperator1;
         AV72WWPessoaDS_3_Pessoa_nome1 = AV37Pessoa_Nome1;
         AV73WWPessoaDS_4_Pessoa_tipopessoa1 = AV38Pessoa_TipoPessoa1;
         AV74WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV75WWPessoaDS_6_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV76WWPessoaDS_7_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV77WWPessoaDS_8_Dynamicfiltersoperator2 = AV42DynamicFiltersOperator2;
         AV78WWPessoaDS_9_Pessoa_nome2 = AV43Pessoa_Nome2;
         AV79WWPessoaDS_10_Pessoa_tipopessoa2 = AV44Pessoa_TipoPessoa2;
         AV80WWPessoaDS_11_Pessoa_docto2 = AV45Pessoa_Docto2;
         AV81WWPessoaDS_12_Dynamicfiltersenabled3 = AV46DynamicFiltersEnabled3;
         AV82WWPessoaDS_13_Dynamicfiltersselector3 = AV47DynamicFiltersSelector3;
         AV83WWPessoaDS_14_Dynamicfiltersoperator3 = AV48DynamicFiltersOperator3;
         AV84WWPessoaDS_15_Pessoa_nome3 = AV49Pessoa_Nome3;
         AV85WWPessoaDS_16_Pessoa_tipopessoa3 = AV50Pessoa_TipoPessoa3;
         AV86WWPessoaDS_17_Pessoa_docto3 = AV51Pessoa_Docto3;
         AV87WWPessoaDS_18_Tfpessoa_nome = AV10TFPessoa_Nome;
         AV88WWPessoaDS_19_Tfpessoa_nome_sel = AV11TFPessoa_Nome_Sel;
         AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV13TFPessoa_TipoPessoa_Sels;
         AV90WWPessoaDS_21_Tfpessoa_docto = AV14TFPessoa_Docto;
         AV91WWPessoaDS_22_Tfpessoa_docto_sel = AV15TFPessoa_Docto_Sel;
         AV92WWPessoaDS_23_Tfpessoa_ie = AV52TFPessoa_IE;
         AV93WWPessoaDS_24_Tfpessoa_ie_sel = AV53TFPessoa_IE_Sel;
         AV94WWPessoaDS_25_Tfpessoa_endereco = AV54TFPessoa_Endereco;
         AV95WWPessoaDS_26_Tfpessoa_endereco_sel = AV55TFPessoa_Endereco_Sel;
         AV96WWPessoaDS_27_Tfpessoa_municipiocod = AV56TFPessoa_MunicipioCod;
         AV97WWPessoaDS_28_Tfpessoa_municipiocod_to = AV57TFPessoa_MunicipioCod_To;
         AV98WWPessoaDS_29_Tfpessoa_uf = AV58TFPessoa_UF;
         AV99WWPessoaDS_30_Tfpessoa_uf_sel = AV59TFPessoa_UF_Sel;
         AV100WWPessoaDS_31_Tfpessoa_cep = AV60TFPessoa_CEP;
         AV101WWPessoaDS_32_Tfpessoa_cep_sel = AV61TFPessoa_CEP_Sel;
         AV102WWPessoaDS_33_Tfpessoa_telefone = AV62TFPessoa_Telefone;
         AV103WWPessoaDS_34_Tfpessoa_telefone_sel = AV63TFPessoa_Telefone_Sel;
         AV104WWPessoaDS_35_Tfpessoa_fax = AV64TFPessoa_Fax;
         AV105WWPessoaDS_36_Tfpessoa_fax_sel = AV65TFPessoa_Fax_Sel;
         AV106WWPessoaDS_37_Tfpessoa_ativo_sel = AV16TFPessoa_Ativo_Sel;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A36Pessoa_TipoPessoa ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                              AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                              AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWPessoaDS_3_Pessoa_nome1 ,
                                              AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                              AV74WWPessoaDS_5_Pessoa_docto1 ,
                                              AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                              AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                              AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                              AV78WWPessoaDS_9_Pessoa_nome2 ,
                                              AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                              AV80WWPessoaDS_11_Pessoa_docto2 ,
                                              AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                              AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                              AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                              AV84WWPessoaDS_15_Pessoa_nome3 ,
                                              AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                              AV86WWPessoaDS_17_Pessoa_docto3 ,
                                              AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                              AV87WWPessoaDS_18_Tfpessoa_nome ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels.Count ,
                                              AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                              AV90WWPessoaDS_21_Tfpessoa_docto ,
                                              AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                              AV92WWPessoaDS_23_Tfpessoa_ie ,
                                              AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                              AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                              AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                              AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                              AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                              AV98WWPessoaDS_29_Tfpessoa_uf ,
                                              AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                              AV100WWPessoaDS_31_Tfpessoa_cep ,
                                              AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                              AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                              AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                              AV104WWPessoaDS_35_Tfpessoa_fax ,
                                              AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                              AV107Usuario_pessoacod ,
                                              A35Pessoa_Nome ,
                                              A37Pessoa_Docto ,
                                              A518Pessoa_IE ,
                                              A519Pessoa_Endereco ,
                                              A503Pessoa_MunicipioCod ,
                                              A520Pessoa_UF ,
                                              A521Pessoa_CEP ,
                                              A522Pessoa_Telefone ,
                                              A523Pessoa_Fax ,
                                              A38Pessoa_Ativo ,
                                              A34Pessoa_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV87WWPessoaDS_18_Tfpessoa_nome = StringUtil.PadR( StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome), 100, "%");
         lV90WWPessoaDS_21_Tfpessoa_docto = StringUtil.Concat( StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto), "%", "");
         lV92WWPessoaDS_23_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie), 15, "%");
         lV94WWPessoaDS_25_Tfpessoa_endereco = StringUtil.Concat( StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco), "%", "");
         lV98WWPessoaDS_29_Tfpessoa_uf = StringUtil.PadR( StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf), 2, "%");
         lV100WWPessoaDS_31_Tfpessoa_cep = StringUtil.PadR( StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep), 10, "%");
         lV102WWPessoaDS_33_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone), 15, "%");
         lV104WWPessoaDS_35_Tfpessoa_fax = StringUtil.PadR( StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax), 15, "%");
         /* Using cursor P00F26 */
         pr_default.execute(4, new Object[] {lV72WWPessoaDS_3_Pessoa_nome1, lV72WWPessoaDS_3_Pessoa_nome1, AV73WWPessoaDS_4_Pessoa_tipopessoa1, lV74WWPessoaDS_5_Pessoa_docto1, lV74WWPessoaDS_5_Pessoa_docto1, lV78WWPessoaDS_9_Pessoa_nome2, lV78WWPessoaDS_9_Pessoa_nome2, AV79WWPessoaDS_10_Pessoa_tipopessoa2, lV80WWPessoaDS_11_Pessoa_docto2, lV80WWPessoaDS_11_Pessoa_docto2, lV84WWPessoaDS_15_Pessoa_nome3, lV84WWPessoaDS_15_Pessoa_nome3, AV85WWPessoaDS_16_Pessoa_tipopessoa3, lV86WWPessoaDS_17_Pessoa_docto3, lV86WWPessoaDS_17_Pessoa_docto3, lV87WWPessoaDS_18_Tfpessoa_nome, AV88WWPessoaDS_19_Tfpessoa_nome_sel, lV90WWPessoaDS_21_Tfpessoa_docto, AV91WWPessoaDS_22_Tfpessoa_docto_sel, lV92WWPessoaDS_23_Tfpessoa_ie, AV93WWPessoaDS_24_Tfpessoa_ie_sel, lV94WWPessoaDS_25_Tfpessoa_endereco, AV95WWPessoaDS_26_Tfpessoa_endereco_sel, AV96WWPessoaDS_27_Tfpessoa_municipiocod, AV97WWPessoaDS_28_Tfpessoa_municipiocod_to, lV98WWPessoaDS_29_Tfpessoa_uf, AV99WWPessoaDS_30_Tfpessoa_uf_sel, lV100WWPessoaDS_31_Tfpessoa_cep, AV101WWPessoaDS_32_Tfpessoa_cep_sel, lV102WWPessoaDS_33_Tfpessoa_telefone, AV103WWPessoaDS_34_Tfpessoa_telefone_sel, lV104WWPessoaDS_35_Tfpessoa_fax, AV105WWPessoaDS_36_Tfpessoa_fax_sel, AV107Usuario_pessoacod});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKF210 = false;
            A38Pessoa_Ativo = P00F26_A38Pessoa_Ativo[0];
            A520Pessoa_UF = P00F26_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F26_n520Pessoa_UF[0];
            A34Pessoa_Codigo = P00F26_A34Pessoa_Codigo[0];
            A523Pessoa_Fax = P00F26_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00F26_n523Pessoa_Fax[0];
            A522Pessoa_Telefone = P00F26_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00F26_n522Pessoa_Telefone[0];
            A521Pessoa_CEP = P00F26_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00F26_n521Pessoa_CEP[0];
            A503Pessoa_MunicipioCod = P00F26_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P00F26_n503Pessoa_MunicipioCod[0];
            A519Pessoa_Endereco = P00F26_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00F26_n519Pessoa_Endereco[0];
            A518Pessoa_IE = P00F26_A518Pessoa_IE[0];
            n518Pessoa_IE = P00F26_n518Pessoa_IE[0];
            A37Pessoa_Docto = P00F26_A37Pessoa_Docto[0];
            A36Pessoa_TipoPessoa = P00F26_A36Pessoa_TipoPessoa[0];
            A35Pessoa_Nome = P00F26_A35Pessoa_Nome[0];
            A520Pessoa_UF = P00F26_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F26_n520Pessoa_UF[0];
            AV29count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00F26_A520Pessoa_UF[0], A520Pessoa_UF) == 0 ) )
            {
               BRKF210 = false;
               A34Pessoa_Codigo = P00F26_A34Pessoa_Codigo[0];
               A503Pessoa_MunicipioCod = P00F26_A503Pessoa_MunicipioCod[0];
               n503Pessoa_MunicipioCod = P00F26_n503Pessoa_MunicipioCod[0];
               AV29count = (long)(AV29count+1);
               BRKF210 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A520Pessoa_UF)) )
            {
               AV21Option = A520Pessoa_UF;
               AV22Options.Add(AV21Option, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKF210 )
            {
               BRKF210 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      protected void S171( )
      {
         /* 'LOADPESSOA_CEPOPTIONS' Routine */
         AV60TFPessoa_CEP = AV17SearchTxt;
         AV61TFPessoa_CEP_Sel = "";
         AV70WWPessoaDS_1_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV71WWPessoaDS_2_Dynamicfiltersoperator1 = AV36DynamicFiltersOperator1;
         AV72WWPessoaDS_3_Pessoa_nome1 = AV37Pessoa_Nome1;
         AV73WWPessoaDS_4_Pessoa_tipopessoa1 = AV38Pessoa_TipoPessoa1;
         AV74WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV75WWPessoaDS_6_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV76WWPessoaDS_7_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV77WWPessoaDS_8_Dynamicfiltersoperator2 = AV42DynamicFiltersOperator2;
         AV78WWPessoaDS_9_Pessoa_nome2 = AV43Pessoa_Nome2;
         AV79WWPessoaDS_10_Pessoa_tipopessoa2 = AV44Pessoa_TipoPessoa2;
         AV80WWPessoaDS_11_Pessoa_docto2 = AV45Pessoa_Docto2;
         AV81WWPessoaDS_12_Dynamicfiltersenabled3 = AV46DynamicFiltersEnabled3;
         AV82WWPessoaDS_13_Dynamicfiltersselector3 = AV47DynamicFiltersSelector3;
         AV83WWPessoaDS_14_Dynamicfiltersoperator3 = AV48DynamicFiltersOperator3;
         AV84WWPessoaDS_15_Pessoa_nome3 = AV49Pessoa_Nome3;
         AV85WWPessoaDS_16_Pessoa_tipopessoa3 = AV50Pessoa_TipoPessoa3;
         AV86WWPessoaDS_17_Pessoa_docto3 = AV51Pessoa_Docto3;
         AV87WWPessoaDS_18_Tfpessoa_nome = AV10TFPessoa_Nome;
         AV88WWPessoaDS_19_Tfpessoa_nome_sel = AV11TFPessoa_Nome_Sel;
         AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV13TFPessoa_TipoPessoa_Sels;
         AV90WWPessoaDS_21_Tfpessoa_docto = AV14TFPessoa_Docto;
         AV91WWPessoaDS_22_Tfpessoa_docto_sel = AV15TFPessoa_Docto_Sel;
         AV92WWPessoaDS_23_Tfpessoa_ie = AV52TFPessoa_IE;
         AV93WWPessoaDS_24_Tfpessoa_ie_sel = AV53TFPessoa_IE_Sel;
         AV94WWPessoaDS_25_Tfpessoa_endereco = AV54TFPessoa_Endereco;
         AV95WWPessoaDS_26_Tfpessoa_endereco_sel = AV55TFPessoa_Endereco_Sel;
         AV96WWPessoaDS_27_Tfpessoa_municipiocod = AV56TFPessoa_MunicipioCod;
         AV97WWPessoaDS_28_Tfpessoa_municipiocod_to = AV57TFPessoa_MunicipioCod_To;
         AV98WWPessoaDS_29_Tfpessoa_uf = AV58TFPessoa_UF;
         AV99WWPessoaDS_30_Tfpessoa_uf_sel = AV59TFPessoa_UF_Sel;
         AV100WWPessoaDS_31_Tfpessoa_cep = AV60TFPessoa_CEP;
         AV101WWPessoaDS_32_Tfpessoa_cep_sel = AV61TFPessoa_CEP_Sel;
         AV102WWPessoaDS_33_Tfpessoa_telefone = AV62TFPessoa_Telefone;
         AV103WWPessoaDS_34_Tfpessoa_telefone_sel = AV63TFPessoa_Telefone_Sel;
         AV104WWPessoaDS_35_Tfpessoa_fax = AV64TFPessoa_Fax;
         AV105WWPessoaDS_36_Tfpessoa_fax_sel = AV65TFPessoa_Fax_Sel;
         AV106WWPessoaDS_37_Tfpessoa_ativo_sel = AV16TFPessoa_Ativo_Sel;
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              A36Pessoa_TipoPessoa ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                              AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                              AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWPessoaDS_3_Pessoa_nome1 ,
                                              AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                              AV74WWPessoaDS_5_Pessoa_docto1 ,
                                              AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                              AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                              AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                              AV78WWPessoaDS_9_Pessoa_nome2 ,
                                              AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                              AV80WWPessoaDS_11_Pessoa_docto2 ,
                                              AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                              AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                              AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                              AV84WWPessoaDS_15_Pessoa_nome3 ,
                                              AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                              AV86WWPessoaDS_17_Pessoa_docto3 ,
                                              AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                              AV87WWPessoaDS_18_Tfpessoa_nome ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels.Count ,
                                              AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                              AV90WWPessoaDS_21_Tfpessoa_docto ,
                                              AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                              AV92WWPessoaDS_23_Tfpessoa_ie ,
                                              AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                              AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                              AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                              AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                              AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                              AV98WWPessoaDS_29_Tfpessoa_uf ,
                                              AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                              AV100WWPessoaDS_31_Tfpessoa_cep ,
                                              AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                              AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                              AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                              AV104WWPessoaDS_35_Tfpessoa_fax ,
                                              AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                              AV107Usuario_pessoacod ,
                                              A35Pessoa_Nome ,
                                              A37Pessoa_Docto ,
                                              A518Pessoa_IE ,
                                              A519Pessoa_Endereco ,
                                              A503Pessoa_MunicipioCod ,
                                              A520Pessoa_UF ,
                                              A521Pessoa_CEP ,
                                              A522Pessoa_Telefone ,
                                              A523Pessoa_Fax ,
                                              A38Pessoa_Ativo ,
                                              A34Pessoa_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV87WWPessoaDS_18_Tfpessoa_nome = StringUtil.PadR( StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome), 100, "%");
         lV90WWPessoaDS_21_Tfpessoa_docto = StringUtil.Concat( StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto), "%", "");
         lV92WWPessoaDS_23_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie), 15, "%");
         lV94WWPessoaDS_25_Tfpessoa_endereco = StringUtil.Concat( StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco), "%", "");
         lV98WWPessoaDS_29_Tfpessoa_uf = StringUtil.PadR( StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf), 2, "%");
         lV100WWPessoaDS_31_Tfpessoa_cep = StringUtil.PadR( StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep), 10, "%");
         lV102WWPessoaDS_33_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone), 15, "%");
         lV104WWPessoaDS_35_Tfpessoa_fax = StringUtil.PadR( StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax), 15, "%");
         /* Using cursor P00F27 */
         pr_default.execute(5, new Object[] {lV72WWPessoaDS_3_Pessoa_nome1, lV72WWPessoaDS_3_Pessoa_nome1, AV73WWPessoaDS_4_Pessoa_tipopessoa1, lV74WWPessoaDS_5_Pessoa_docto1, lV74WWPessoaDS_5_Pessoa_docto1, lV78WWPessoaDS_9_Pessoa_nome2, lV78WWPessoaDS_9_Pessoa_nome2, AV79WWPessoaDS_10_Pessoa_tipopessoa2, lV80WWPessoaDS_11_Pessoa_docto2, lV80WWPessoaDS_11_Pessoa_docto2, lV84WWPessoaDS_15_Pessoa_nome3, lV84WWPessoaDS_15_Pessoa_nome3, AV85WWPessoaDS_16_Pessoa_tipopessoa3, lV86WWPessoaDS_17_Pessoa_docto3, lV86WWPessoaDS_17_Pessoa_docto3, lV87WWPessoaDS_18_Tfpessoa_nome, AV88WWPessoaDS_19_Tfpessoa_nome_sel, lV90WWPessoaDS_21_Tfpessoa_docto, AV91WWPessoaDS_22_Tfpessoa_docto_sel, lV92WWPessoaDS_23_Tfpessoa_ie, AV93WWPessoaDS_24_Tfpessoa_ie_sel, lV94WWPessoaDS_25_Tfpessoa_endereco, AV95WWPessoaDS_26_Tfpessoa_endereco_sel, AV96WWPessoaDS_27_Tfpessoa_municipiocod, AV97WWPessoaDS_28_Tfpessoa_municipiocod_to, lV98WWPessoaDS_29_Tfpessoa_uf, AV99WWPessoaDS_30_Tfpessoa_uf_sel, lV100WWPessoaDS_31_Tfpessoa_cep, AV101WWPessoaDS_32_Tfpessoa_cep_sel, lV102WWPessoaDS_33_Tfpessoa_telefone, AV103WWPessoaDS_34_Tfpessoa_telefone_sel, lV104WWPessoaDS_35_Tfpessoa_fax, AV105WWPessoaDS_36_Tfpessoa_fax_sel, AV107Usuario_pessoacod});
         while ( (pr_default.getStatus(5) != 101) )
         {
            BRKF212 = false;
            A38Pessoa_Ativo = P00F27_A38Pessoa_Ativo[0];
            A521Pessoa_CEP = P00F27_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00F27_n521Pessoa_CEP[0];
            A34Pessoa_Codigo = P00F27_A34Pessoa_Codigo[0];
            A523Pessoa_Fax = P00F27_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00F27_n523Pessoa_Fax[0];
            A522Pessoa_Telefone = P00F27_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00F27_n522Pessoa_Telefone[0];
            A520Pessoa_UF = P00F27_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F27_n520Pessoa_UF[0];
            A503Pessoa_MunicipioCod = P00F27_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P00F27_n503Pessoa_MunicipioCod[0];
            A519Pessoa_Endereco = P00F27_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00F27_n519Pessoa_Endereco[0];
            A518Pessoa_IE = P00F27_A518Pessoa_IE[0];
            n518Pessoa_IE = P00F27_n518Pessoa_IE[0];
            A37Pessoa_Docto = P00F27_A37Pessoa_Docto[0];
            A36Pessoa_TipoPessoa = P00F27_A36Pessoa_TipoPessoa[0];
            A35Pessoa_Nome = P00F27_A35Pessoa_Nome[0];
            A520Pessoa_UF = P00F27_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F27_n520Pessoa_UF[0];
            AV29count = 0;
            while ( (pr_default.getStatus(5) != 101) && ( StringUtil.StrCmp(P00F27_A521Pessoa_CEP[0], A521Pessoa_CEP) == 0 ) )
            {
               BRKF212 = false;
               A34Pessoa_Codigo = P00F27_A34Pessoa_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKF212 = true;
               pr_default.readNext(5);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A521Pessoa_CEP)) )
            {
               AV21Option = A521Pessoa_CEP;
               AV22Options.Add(AV21Option, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKF212 )
            {
               BRKF212 = true;
               pr_default.readNext(5);
            }
         }
         pr_default.close(5);
      }

      protected void S181( )
      {
         /* 'LOADPESSOA_TELEFONEOPTIONS' Routine */
         AV62TFPessoa_Telefone = AV17SearchTxt;
         AV63TFPessoa_Telefone_Sel = "";
         AV70WWPessoaDS_1_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV71WWPessoaDS_2_Dynamicfiltersoperator1 = AV36DynamicFiltersOperator1;
         AV72WWPessoaDS_3_Pessoa_nome1 = AV37Pessoa_Nome1;
         AV73WWPessoaDS_4_Pessoa_tipopessoa1 = AV38Pessoa_TipoPessoa1;
         AV74WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV75WWPessoaDS_6_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV76WWPessoaDS_7_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV77WWPessoaDS_8_Dynamicfiltersoperator2 = AV42DynamicFiltersOperator2;
         AV78WWPessoaDS_9_Pessoa_nome2 = AV43Pessoa_Nome2;
         AV79WWPessoaDS_10_Pessoa_tipopessoa2 = AV44Pessoa_TipoPessoa2;
         AV80WWPessoaDS_11_Pessoa_docto2 = AV45Pessoa_Docto2;
         AV81WWPessoaDS_12_Dynamicfiltersenabled3 = AV46DynamicFiltersEnabled3;
         AV82WWPessoaDS_13_Dynamicfiltersselector3 = AV47DynamicFiltersSelector3;
         AV83WWPessoaDS_14_Dynamicfiltersoperator3 = AV48DynamicFiltersOperator3;
         AV84WWPessoaDS_15_Pessoa_nome3 = AV49Pessoa_Nome3;
         AV85WWPessoaDS_16_Pessoa_tipopessoa3 = AV50Pessoa_TipoPessoa3;
         AV86WWPessoaDS_17_Pessoa_docto3 = AV51Pessoa_Docto3;
         AV87WWPessoaDS_18_Tfpessoa_nome = AV10TFPessoa_Nome;
         AV88WWPessoaDS_19_Tfpessoa_nome_sel = AV11TFPessoa_Nome_Sel;
         AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV13TFPessoa_TipoPessoa_Sels;
         AV90WWPessoaDS_21_Tfpessoa_docto = AV14TFPessoa_Docto;
         AV91WWPessoaDS_22_Tfpessoa_docto_sel = AV15TFPessoa_Docto_Sel;
         AV92WWPessoaDS_23_Tfpessoa_ie = AV52TFPessoa_IE;
         AV93WWPessoaDS_24_Tfpessoa_ie_sel = AV53TFPessoa_IE_Sel;
         AV94WWPessoaDS_25_Tfpessoa_endereco = AV54TFPessoa_Endereco;
         AV95WWPessoaDS_26_Tfpessoa_endereco_sel = AV55TFPessoa_Endereco_Sel;
         AV96WWPessoaDS_27_Tfpessoa_municipiocod = AV56TFPessoa_MunicipioCod;
         AV97WWPessoaDS_28_Tfpessoa_municipiocod_to = AV57TFPessoa_MunicipioCod_To;
         AV98WWPessoaDS_29_Tfpessoa_uf = AV58TFPessoa_UF;
         AV99WWPessoaDS_30_Tfpessoa_uf_sel = AV59TFPessoa_UF_Sel;
         AV100WWPessoaDS_31_Tfpessoa_cep = AV60TFPessoa_CEP;
         AV101WWPessoaDS_32_Tfpessoa_cep_sel = AV61TFPessoa_CEP_Sel;
         AV102WWPessoaDS_33_Tfpessoa_telefone = AV62TFPessoa_Telefone;
         AV103WWPessoaDS_34_Tfpessoa_telefone_sel = AV63TFPessoa_Telefone_Sel;
         AV104WWPessoaDS_35_Tfpessoa_fax = AV64TFPessoa_Fax;
         AV105WWPessoaDS_36_Tfpessoa_fax_sel = AV65TFPessoa_Fax_Sel;
         AV106WWPessoaDS_37_Tfpessoa_ativo_sel = AV16TFPessoa_Ativo_Sel;
         pr_default.dynParam(6, new Object[]{ new Object[]{
                                              A36Pessoa_TipoPessoa ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                              AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                              AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWPessoaDS_3_Pessoa_nome1 ,
                                              AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                              AV74WWPessoaDS_5_Pessoa_docto1 ,
                                              AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                              AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                              AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                              AV78WWPessoaDS_9_Pessoa_nome2 ,
                                              AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                              AV80WWPessoaDS_11_Pessoa_docto2 ,
                                              AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                              AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                              AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                              AV84WWPessoaDS_15_Pessoa_nome3 ,
                                              AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                              AV86WWPessoaDS_17_Pessoa_docto3 ,
                                              AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                              AV87WWPessoaDS_18_Tfpessoa_nome ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels.Count ,
                                              AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                              AV90WWPessoaDS_21_Tfpessoa_docto ,
                                              AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                              AV92WWPessoaDS_23_Tfpessoa_ie ,
                                              AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                              AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                              AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                              AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                              AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                              AV98WWPessoaDS_29_Tfpessoa_uf ,
                                              AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                              AV100WWPessoaDS_31_Tfpessoa_cep ,
                                              AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                              AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                              AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                              AV104WWPessoaDS_35_Tfpessoa_fax ,
                                              AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                              AV107Usuario_pessoacod ,
                                              A35Pessoa_Nome ,
                                              A37Pessoa_Docto ,
                                              A518Pessoa_IE ,
                                              A519Pessoa_Endereco ,
                                              A503Pessoa_MunicipioCod ,
                                              A520Pessoa_UF ,
                                              A521Pessoa_CEP ,
                                              A522Pessoa_Telefone ,
                                              A523Pessoa_Fax ,
                                              A38Pessoa_Ativo ,
                                              A34Pessoa_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV87WWPessoaDS_18_Tfpessoa_nome = StringUtil.PadR( StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome), 100, "%");
         lV90WWPessoaDS_21_Tfpessoa_docto = StringUtil.Concat( StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto), "%", "");
         lV92WWPessoaDS_23_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie), 15, "%");
         lV94WWPessoaDS_25_Tfpessoa_endereco = StringUtil.Concat( StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco), "%", "");
         lV98WWPessoaDS_29_Tfpessoa_uf = StringUtil.PadR( StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf), 2, "%");
         lV100WWPessoaDS_31_Tfpessoa_cep = StringUtil.PadR( StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep), 10, "%");
         lV102WWPessoaDS_33_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone), 15, "%");
         lV104WWPessoaDS_35_Tfpessoa_fax = StringUtil.PadR( StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax), 15, "%");
         /* Using cursor P00F28 */
         pr_default.execute(6, new Object[] {lV72WWPessoaDS_3_Pessoa_nome1, lV72WWPessoaDS_3_Pessoa_nome1, AV73WWPessoaDS_4_Pessoa_tipopessoa1, lV74WWPessoaDS_5_Pessoa_docto1, lV74WWPessoaDS_5_Pessoa_docto1, lV78WWPessoaDS_9_Pessoa_nome2, lV78WWPessoaDS_9_Pessoa_nome2, AV79WWPessoaDS_10_Pessoa_tipopessoa2, lV80WWPessoaDS_11_Pessoa_docto2, lV80WWPessoaDS_11_Pessoa_docto2, lV84WWPessoaDS_15_Pessoa_nome3, lV84WWPessoaDS_15_Pessoa_nome3, AV85WWPessoaDS_16_Pessoa_tipopessoa3, lV86WWPessoaDS_17_Pessoa_docto3, lV86WWPessoaDS_17_Pessoa_docto3, lV87WWPessoaDS_18_Tfpessoa_nome, AV88WWPessoaDS_19_Tfpessoa_nome_sel, lV90WWPessoaDS_21_Tfpessoa_docto, AV91WWPessoaDS_22_Tfpessoa_docto_sel, lV92WWPessoaDS_23_Tfpessoa_ie, AV93WWPessoaDS_24_Tfpessoa_ie_sel, lV94WWPessoaDS_25_Tfpessoa_endereco, AV95WWPessoaDS_26_Tfpessoa_endereco_sel, AV96WWPessoaDS_27_Tfpessoa_municipiocod, AV97WWPessoaDS_28_Tfpessoa_municipiocod_to, lV98WWPessoaDS_29_Tfpessoa_uf, AV99WWPessoaDS_30_Tfpessoa_uf_sel, lV100WWPessoaDS_31_Tfpessoa_cep, AV101WWPessoaDS_32_Tfpessoa_cep_sel, lV102WWPessoaDS_33_Tfpessoa_telefone, AV103WWPessoaDS_34_Tfpessoa_telefone_sel, lV104WWPessoaDS_35_Tfpessoa_fax, AV105WWPessoaDS_36_Tfpessoa_fax_sel, AV107Usuario_pessoacod});
         while ( (pr_default.getStatus(6) != 101) )
         {
            BRKF214 = false;
            A38Pessoa_Ativo = P00F28_A38Pessoa_Ativo[0];
            A522Pessoa_Telefone = P00F28_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00F28_n522Pessoa_Telefone[0];
            A34Pessoa_Codigo = P00F28_A34Pessoa_Codigo[0];
            A523Pessoa_Fax = P00F28_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00F28_n523Pessoa_Fax[0];
            A521Pessoa_CEP = P00F28_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00F28_n521Pessoa_CEP[0];
            A520Pessoa_UF = P00F28_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F28_n520Pessoa_UF[0];
            A503Pessoa_MunicipioCod = P00F28_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P00F28_n503Pessoa_MunicipioCod[0];
            A519Pessoa_Endereco = P00F28_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00F28_n519Pessoa_Endereco[0];
            A518Pessoa_IE = P00F28_A518Pessoa_IE[0];
            n518Pessoa_IE = P00F28_n518Pessoa_IE[0];
            A37Pessoa_Docto = P00F28_A37Pessoa_Docto[0];
            A36Pessoa_TipoPessoa = P00F28_A36Pessoa_TipoPessoa[0];
            A35Pessoa_Nome = P00F28_A35Pessoa_Nome[0];
            A520Pessoa_UF = P00F28_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F28_n520Pessoa_UF[0];
            AV29count = 0;
            while ( (pr_default.getStatus(6) != 101) && ( StringUtil.StrCmp(P00F28_A522Pessoa_Telefone[0], A522Pessoa_Telefone) == 0 ) )
            {
               BRKF214 = false;
               A34Pessoa_Codigo = P00F28_A34Pessoa_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKF214 = true;
               pr_default.readNext(6);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A522Pessoa_Telefone)) )
            {
               AV21Option = A522Pessoa_Telefone;
               AV22Options.Add(AV21Option, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKF214 )
            {
               BRKF214 = true;
               pr_default.readNext(6);
            }
         }
         pr_default.close(6);
      }

      protected void S191( )
      {
         /* 'LOADPESSOA_FAXOPTIONS' Routine */
         AV64TFPessoa_Fax = AV17SearchTxt;
         AV65TFPessoa_Fax_Sel = "";
         AV70WWPessoaDS_1_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV71WWPessoaDS_2_Dynamicfiltersoperator1 = AV36DynamicFiltersOperator1;
         AV72WWPessoaDS_3_Pessoa_nome1 = AV37Pessoa_Nome1;
         AV73WWPessoaDS_4_Pessoa_tipopessoa1 = AV38Pessoa_TipoPessoa1;
         AV74WWPessoaDS_5_Pessoa_docto1 = AV39Pessoa_Docto1;
         AV75WWPessoaDS_6_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV76WWPessoaDS_7_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV77WWPessoaDS_8_Dynamicfiltersoperator2 = AV42DynamicFiltersOperator2;
         AV78WWPessoaDS_9_Pessoa_nome2 = AV43Pessoa_Nome2;
         AV79WWPessoaDS_10_Pessoa_tipopessoa2 = AV44Pessoa_TipoPessoa2;
         AV80WWPessoaDS_11_Pessoa_docto2 = AV45Pessoa_Docto2;
         AV81WWPessoaDS_12_Dynamicfiltersenabled3 = AV46DynamicFiltersEnabled3;
         AV82WWPessoaDS_13_Dynamicfiltersselector3 = AV47DynamicFiltersSelector3;
         AV83WWPessoaDS_14_Dynamicfiltersoperator3 = AV48DynamicFiltersOperator3;
         AV84WWPessoaDS_15_Pessoa_nome3 = AV49Pessoa_Nome3;
         AV85WWPessoaDS_16_Pessoa_tipopessoa3 = AV50Pessoa_TipoPessoa3;
         AV86WWPessoaDS_17_Pessoa_docto3 = AV51Pessoa_Docto3;
         AV87WWPessoaDS_18_Tfpessoa_nome = AV10TFPessoa_Nome;
         AV88WWPessoaDS_19_Tfpessoa_nome_sel = AV11TFPessoa_Nome_Sel;
         AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels = AV13TFPessoa_TipoPessoa_Sels;
         AV90WWPessoaDS_21_Tfpessoa_docto = AV14TFPessoa_Docto;
         AV91WWPessoaDS_22_Tfpessoa_docto_sel = AV15TFPessoa_Docto_Sel;
         AV92WWPessoaDS_23_Tfpessoa_ie = AV52TFPessoa_IE;
         AV93WWPessoaDS_24_Tfpessoa_ie_sel = AV53TFPessoa_IE_Sel;
         AV94WWPessoaDS_25_Tfpessoa_endereco = AV54TFPessoa_Endereco;
         AV95WWPessoaDS_26_Tfpessoa_endereco_sel = AV55TFPessoa_Endereco_Sel;
         AV96WWPessoaDS_27_Tfpessoa_municipiocod = AV56TFPessoa_MunicipioCod;
         AV97WWPessoaDS_28_Tfpessoa_municipiocod_to = AV57TFPessoa_MunicipioCod_To;
         AV98WWPessoaDS_29_Tfpessoa_uf = AV58TFPessoa_UF;
         AV99WWPessoaDS_30_Tfpessoa_uf_sel = AV59TFPessoa_UF_Sel;
         AV100WWPessoaDS_31_Tfpessoa_cep = AV60TFPessoa_CEP;
         AV101WWPessoaDS_32_Tfpessoa_cep_sel = AV61TFPessoa_CEP_Sel;
         AV102WWPessoaDS_33_Tfpessoa_telefone = AV62TFPessoa_Telefone;
         AV103WWPessoaDS_34_Tfpessoa_telefone_sel = AV63TFPessoa_Telefone_Sel;
         AV104WWPessoaDS_35_Tfpessoa_fax = AV64TFPessoa_Fax;
         AV105WWPessoaDS_36_Tfpessoa_fax_sel = AV65TFPessoa_Fax_Sel;
         AV106WWPessoaDS_37_Tfpessoa_ativo_sel = AV16TFPessoa_Ativo_Sel;
         pr_default.dynParam(7, new Object[]{ new Object[]{
                                              A36Pessoa_TipoPessoa ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                              AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                              AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWPessoaDS_3_Pessoa_nome1 ,
                                              AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                              AV74WWPessoaDS_5_Pessoa_docto1 ,
                                              AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                              AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                              AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                              AV78WWPessoaDS_9_Pessoa_nome2 ,
                                              AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                              AV80WWPessoaDS_11_Pessoa_docto2 ,
                                              AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                              AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                              AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                              AV84WWPessoaDS_15_Pessoa_nome3 ,
                                              AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                              AV86WWPessoaDS_17_Pessoa_docto3 ,
                                              AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                              AV87WWPessoaDS_18_Tfpessoa_nome ,
                                              AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels.Count ,
                                              AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                              AV90WWPessoaDS_21_Tfpessoa_docto ,
                                              AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                              AV92WWPessoaDS_23_Tfpessoa_ie ,
                                              AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                              AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                              AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                              AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                              AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                              AV98WWPessoaDS_29_Tfpessoa_uf ,
                                              AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                              AV100WWPessoaDS_31_Tfpessoa_cep ,
                                              AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                              AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                              AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                              AV104WWPessoaDS_35_Tfpessoa_fax ,
                                              AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                              AV107Usuario_pessoacod ,
                                              A35Pessoa_Nome ,
                                              A37Pessoa_Docto ,
                                              A518Pessoa_IE ,
                                              A519Pessoa_Endereco ,
                                              A503Pessoa_MunicipioCod ,
                                              A520Pessoa_UF ,
                                              A521Pessoa_CEP ,
                                              A522Pessoa_Telefone ,
                                              A523Pessoa_Fax ,
                                              A38Pessoa_Ativo ,
                                              A34Pessoa_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV72WWPessoaDS_3_Pessoa_nome1 = StringUtil.PadR( StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1), 100, "%");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV74WWPessoaDS_5_Pessoa_docto1 = StringUtil.Concat( StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1), "%", "");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV78WWPessoaDS_9_Pessoa_nome2 = StringUtil.PadR( StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2), 100, "%");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV80WWPessoaDS_11_Pessoa_docto2 = StringUtil.Concat( StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2), "%", "");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV84WWPessoaDS_15_Pessoa_nome3 = StringUtil.PadR( StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3), 100, "%");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV86WWPessoaDS_17_Pessoa_docto3 = StringUtil.Concat( StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3), "%", "");
         lV87WWPessoaDS_18_Tfpessoa_nome = StringUtil.PadR( StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome), 100, "%");
         lV90WWPessoaDS_21_Tfpessoa_docto = StringUtil.Concat( StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto), "%", "");
         lV92WWPessoaDS_23_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie), 15, "%");
         lV94WWPessoaDS_25_Tfpessoa_endereco = StringUtil.Concat( StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco), "%", "");
         lV98WWPessoaDS_29_Tfpessoa_uf = StringUtil.PadR( StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf), 2, "%");
         lV100WWPessoaDS_31_Tfpessoa_cep = StringUtil.PadR( StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep), 10, "%");
         lV102WWPessoaDS_33_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone), 15, "%");
         lV104WWPessoaDS_35_Tfpessoa_fax = StringUtil.PadR( StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax), 15, "%");
         /* Using cursor P00F29 */
         pr_default.execute(7, new Object[] {lV72WWPessoaDS_3_Pessoa_nome1, lV72WWPessoaDS_3_Pessoa_nome1, AV73WWPessoaDS_4_Pessoa_tipopessoa1, lV74WWPessoaDS_5_Pessoa_docto1, lV74WWPessoaDS_5_Pessoa_docto1, lV78WWPessoaDS_9_Pessoa_nome2, lV78WWPessoaDS_9_Pessoa_nome2, AV79WWPessoaDS_10_Pessoa_tipopessoa2, lV80WWPessoaDS_11_Pessoa_docto2, lV80WWPessoaDS_11_Pessoa_docto2, lV84WWPessoaDS_15_Pessoa_nome3, lV84WWPessoaDS_15_Pessoa_nome3, AV85WWPessoaDS_16_Pessoa_tipopessoa3, lV86WWPessoaDS_17_Pessoa_docto3, lV86WWPessoaDS_17_Pessoa_docto3, lV87WWPessoaDS_18_Tfpessoa_nome, AV88WWPessoaDS_19_Tfpessoa_nome_sel, lV90WWPessoaDS_21_Tfpessoa_docto, AV91WWPessoaDS_22_Tfpessoa_docto_sel, lV92WWPessoaDS_23_Tfpessoa_ie, AV93WWPessoaDS_24_Tfpessoa_ie_sel, lV94WWPessoaDS_25_Tfpessoa_endereco, AV95WWPessoaDS_26_Tfpessoa_endereco_sel, AV96WWPessoaDS_27_Tfpessoa_municipiocod, AV97WWPessoaDS_28_Tfpessoa_municipiocod_to, lV98WWPessoaDS_29_Tfpessoa_uf, AV99WWPessoaDS_30_Tfpessoa_uf_sel, lV100WWPessoaDS_31_Tfpessoa_cep, AV101WWPessoaDS_32_Tfpessoa_cep_sel, lV102WWPessoaDS_33_Tfpessoa_telefone, AV103WWPessoaDS_34_Tfpessoa_telefone_sel, lV104WWPessoaDS_35_Tfpessoa_fax, AV105WWPessoaDS_36_Tfpessoa_fax_sel, AV107Usuario_pessoacod});
         while ( (pr_default.getStatus(7) != 101) )
         {
            BRKF216 = false;
            A38Pessoa_Ativo = P00F29_A38Pessoa_Ativo[0];
            A523Pessoa_Fax = P00F29_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00F29_n523Pessoa_Fax[0];
            A34Pessoa_Codigo = P00F29_A34Pessoa_Codigo[0];
            A522Pessoa_Telefone = P00F29_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00F29_n522Pessoa_Telefone[0];
            A521Pessoa_CEP = P00F29_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00F29_n521Pessoa_CEP[0];
            A520Pessoa_UF = P00F29_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F29_n520Pessoa_UF[0];
            A503Pessoa_MunicipioCod = P00F29_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P00F29_n503Pessoa_MunicipioCod[0];
            A519Pessoa_Endereco = P00F29_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00F29_n519Pessoa_Endereco[0];
            A518Pessoa_IE = P00F29_A518Pessoa_IE[0];
            n518Pessoa_IE = P00F29_n518Pessoa_IE[0];
            A37Pessoa_Docto = P00F29_A37Pessoa_Docto[0];
            A36Pessoa_TipoPessoa = P00F29_A36Pessoa_TipoPessoa[0];
            A35Pessoa_Nome = P00F29_A35Pessoa_Nome[0];
            A520Pessoa_UF = P00F29_A520Pessoa_UF[0];
            n520Pessoa_UF = P00F29_n520Pessoa_UF[0];
            AV29count = 0;
            while ( (pr_default.getStatus(7) != 101) && ( StringUtil.StrCmp(P00F29_A523Pessoa_Fax[0], A523Pessoa_Fax) == 0 ) )
            {
               BRKF216 = false;
               A34Pessoa_Codigo = P00F29_A34Pessoa_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKF216 = true;
               pr_default.readNext(7);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A523Pessoa_Fax)) )
            {
               AV21Option = A523Pessoa_Fax;
               AV22Options.Add(AV21Option, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKF216 )
            {
               BRKF216 = true;
               pr_default.readNext(7);
            }
         }
         pr_default.close(7);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV22Options = new GxSimpleCollection();
         AV25OptionsDesc = new GxSimpleCollection();
         AV27OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV30Session = context.GetSession();
         AV32GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV33GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFPessoa_Nome = "";
         AV11TFPessoa_Nome_Sel = "";
         AV12TFPessoa_TipoPessoa_SelsJson = "";
         AV13TFPessoa_TipoPessoa_Sels = new GxSimpleCollection();
         AV14TFPessoa_Docto = "";
         AV15TFPessoa_Docto_Sel = "";
         AV52TFPessoa_IE = "";
         AV53TFPessoa_IE_Sel = "";
         AV54TFPessoa_Endereco = "";
         AV55TFPessoa_Endereco_Sel = "";
         AV58TFPessoa_UF = "";
         AV59TFPessoa_UF_Sel = "";
         AV60TFPessoa_CEP = "";
         AV61TFPessoa_CEP_Sel = "";
         AV62TFPessoa_Telefone = "";
         AV63TFPessoa_Telefone_Sel = "";
         AV64TFPessoa_Fax = "";
         AV65TFPessoa_Fax_Sel = "";
         AV34GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV35DynamicFiltersSelector1 = "";
         AV37Pessoa_Nome1 = "";
         AV38Pessoa_TipoPessoa1 = "";
         AV39Pessoa_Docto1 = "";
         AV41DynamicFiltersSelector2 = "";
         AV43Pessoa_Nome2 = "";
         AV44Pessoa_TipoPessoa2 = "";
         AV45Pessoa_Docto2 = "";
         AV47DynamicFiltersSelector3 = "";
         AV49Pessoa_Nome3 = "";
         AV50Pessoa_TipoPessoa3 = "";
         AV51Pessoa_Docto3 = "";
         AV70WWPessoaDS_1_Dynamicfiltersselector1 = "";
         AV72WWPessoaDS_3_Pessoa_nome1 = "";
         AV73WWPessoaDS_4_Pessoa_tipopessoa1 = "";
         AV74WWPessoaDS_5_Pessoa_docto1 = "";
         AV76WWPessoaDS_7_Dynamicfiltersselector2 = "";
         AV78WWPessoaDS_9_Pessoa_nome2 = "";
         AV79WWPessoaDS_10_Pessoa_tipopessoa2 = "";
         AV80WWPessoaDS_11_Pessoa_docto2 = "";
         AV82WWPessoaDS_13_Dynamicfiltersselector3 = "";
         AV84WWPessoaDS_15_Pessoa_nome3 = "";
         AV85WWPessoaDS_16_Pessoa_tipopessoa3 = "";
         AV86WWPessoaDS_17_Pessoa_docto3 = "";
         AV87WWPessoaDS_18_Tfpessoa_nome = "";
         AV88WWPessoaDS_19_Tfpessoa_nome_sel = "";
         AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels = new GxSimpleCollection();
         AV90WWPessoaDS_21_Tfpessoa_docto = "";
         AV91WWPessoaDS_22_Tfpessoa_docto_sel = "";
         AV92WWPessoaDS_23_Tfpessoa_ie = "";
         AV93WWPessoaDS_24_Tfpessoa_ie_sel = "";
         AV94WWPessoaDS_25_Tfpessoa_endereco = "";
         AV95WWPessoaDS_26_Tfpessoa_endereco_sel = "";
         AV98WWPessoaDS_29_Tfpessoa_uf = "";
         AV99WWPessoaDS_30_Tfpessoa_uf_sel = "";
         AV100WWPessoaDS_31_Tfpessoa_cep = "";
         AV101WWPessoaDS_32_Tfpessoa_cep_sel = "";
         AV102WWPessoaDS_33_Tfpessoa_telefone = "";
         AV103WWPessoaDS_34_Tfpessoa_telefone_sel = "";
         AV104WWPessoaDS_35_Tfpessoa_fax = "";
         AV105WWPessoaDS_36_Tfpessoa_fax_sel = "";
         scmdbuf = "";
         lV72WWPessoaDS_3_Pessoa_nome1 = "";
         lV74WWPessoaDS_5_Pessoa_docto1 = "";
         lV78WWPessoaDS_9_Pessoa_nome2 = "";
         lV80WWPessoaDS_11_Pessoa_docto2 = "";
         lV84WWPessoaDS_15_Pessoa_nome3 = "";
         lV86WWPessoaDS_17_Pessoa_docto3 = "";
         lV87WWPessoaDS_18_Tfpessoa_nome = "";
         lV90WWPessoaDS_21_Tfpessoa_docto = "";
         lV92WWPessoaDS_23_Tfpessoa_ie = "";
         lV94WWPessoaDS_25_Tfpessoa_endereco = "";
         lV98WWPessoaDS_29_Tfpessoa_uf = "";
         lV100WWPessoaDS_31_Tfpessoa_cep = "";
         lV102WWPessoaDS_33_Tfpessoa_telefone = "";
         lV104WWPessoaDS_35_Tfpessoa_fax = "";
         A36Pessoa_TipoPessoa = "";
         A35Pessoa_Nome = "";
         A37Pessoa_Docto = "";
         A518Pessoa_IE = "";
         A519Pessoa_Endereco = "";
         A520Pessoa_UF = "";
         A521Pessoa_CEP = "";
         A522Pessoa_Telefone = "";
         A523Pessoa_Fax = "";
         P00F22_A35Pessoa_Nome = new String[] {""} ;
         P00F22_A34Pessoa_Codigo = new int[1] ;
         P00F22_A38Pessoa_Ativo = new bool[] {false} ;
         P00F22_A523Pessoa_Fax = new String[] {""} ;
         P00F22_n523Pessoa_Fax = new bool[] {false} ;
         P00F22_A522Pessoa_Telefone = new String[] {""} ;
         P00F22_n522Pessoa_Telefone = new bool[] {false} ;
         P00F22_A521Pessoa_CEP = new String[] {""} ;
         P00F22_n521Pessoa_CEP = new bool[] {false} ;
         P00F22_A520Pessoa_UF = new String[] {""} ;
         P00F22_n520Pessoa_UF = new bool[] {false} ;
         P00F22_A503Pessoa_MunicipioCod = new int[1] ;
         P00F22_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P00F22_A519Pessoa_Endereco = new String[] {""} ;
         P00F22_n519Pessoa_Endereco = new bool[] {false} ;
         P00F22_A518Pessoa_IE = new String[] {""} ;
         P00F22_n518Pessoa_IE = new bool[] {false} ;
         P00F22_A37Pessoa_Docto = new String[] {""} ;
         P00F22_A36Pessoa_TipoPessoa = new String[] {""} ;
         AV21Option = "";
         P00F23_A37Pessoa_Docto = new String[] {""} ;
         P00F23_A34Pessoa_Codigo = new int[1] ;
         P00F23_A38Pessoa_Ativo = new bool[] {false} ;
         P00F23_A523Pessoa_Fax = new String[] {""} ;
         P00F23_n523Pessoa_Fax = new bool[] {false} ;
         P00F23_A522Pessoa_Telefone = new String[] {""} ;
         P00F23_n522Pessoa_Telefone = new bool[] {false} ;
         P00F23_A521Pessoa_CEP = new String[] {""} ;
         P00F23_n521Pessoa_CEP = new bool[] {false} ;
         P00F23_A520Pessoa_UF = new String[] {""} ;
         P00F23_n520Pessoa_UF = new bool[] {false} ;
         P00F23_A503Pessoa_MunicipioCod = new int[1] ;
         P00F23_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P00F23_A519Pessoa_Endereco = new String[] {""} ;
         P00F23_n519Pessoa_Endereco = new bool[] {false} ;
         P00F23_A518Pessoa_IE = new String[] {""} ;
         P00F23_n518Pessoa_IE = new bool[] {false} ;
         P00F23_A36Pessoa_TipoPessoa = new String[] {""} ;
         P00F23_A35Pessoa_Nome = new String[] {""} ;
         P00F24_A38Pessoa_Ativo = new bool[] {false} ;
         P00F24_A518Pessoa_IE = new String[] {""} ;
         P00F24_n518Pessoa_IE = new bool[] {false} ;
         P00F24_A34Pessoa_Codigo = new int[1] ;
         P00F24_A523Pessoa_Fax = new String[] {""} ;
         P00F24_n523Pessoa_Fax = new bool[] {false} ;
         P00F24_A522Pessoa_Telefone = new String[] {""} ;
         P00F24_n522Pessoa_Telefone = new bool[] {false} ;
         P00F24_A521Pessoa_CEP = new String[] {""} ;
         P00F24_n521Pessoa_CEP = new bool[] {false} ;
         P00F24_A520Pessoa_UF = new String[] {""} ;
         P00F24_n520Pessoa_UF = new bool[] {false} ;
         P00F24_A503Pessoa_MunicipioCod = new int[1] ;
         P00F24_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P00F24_A519Pessoa_Endereco = new String[] {""} ;
         P00F24_n519Pessoa_Endereco = new bool[] {false} ;
         P00F24_A37Pessoa_Docto = new String[] {""} ;
         P00F24_A36Pessoa_TipoPessoa = new String[] {""} ;
         P00F24_A35Pessoa_Nome = new String[] {""} ;
         P00F25_A38Pessoa_Ativo = new bool[] {false} ;
         P00F25_A519Pessoa_Endereco = new String[] {""} ;
         P00F25_n519Pessoa_Endereco = new bool[] {false} ;
         P00F25_A34Pessoa_Codigo = new int[1] ;
         P00F25_A523Pessoa_Fax = new String[] {""} ;
         P00F25_n523Pessoa_Fax = new bool[] {false} ;
         P00F25_A522Pessoa_Telefone = new String[] {""} ;
         P00F25_n522Pessoa_Telefone = new bool[] {false} ;
         P00F25_A521Pessoa_CEP = new String[] {""} ;
         P00F25_n521Pessoa_CEP = new bool[] {false} ;
         P00F25_A520Pessoa_UF = new String[] {""} ;
         P00F25_n520Pessoa_UF = new bool[] {false} ;
         P00F25_A503Pessoa_MunicipioCod = new int[1] ;
         P00F25_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P00F25_A518Pessoa_IE = new String[] {""} ;
         P00F25_n518Pessoa_IE = new bool[] {false} ;
         P00F25_A37Pessoa_Docto = new String[] {""} ;
         P00F25_A36Pessoa_TipoPessoa = new String[] {""} ;
         P00F25_A35Pessoa_Nome = new String[] {""} ;
         P00F26_A38Pessoa_Ativo = new bool[] {false} ;
         P00F26_A520Pessoa_UF = new String[] {""} ;
         P00F26_n520Pessoa_UF = new bool[] {false} ;
         P00F26_A34Pessoa_Codigo = new int[1] ;
         P00F26_A523Pessoa_Fax = new String[] {""} ;
         P00F26_n523Pessoa_Fax = new bool[] {false} ;
         P00F26_A522Pessoa_Telefone = new String[] {""} ;
         P00F26_n522Pessoa_Telefone = new bool[] {false} ;
         P00F26_A521Pessoa_CEP = new String[] {""} ;
         P00F26_n521Pessoa_CEP = new bool[] {false} ;
         P00F26_A503Pessoa_MunicipioCod = new int[1] ;
         P00F26_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P00F26_A519Pessoa_Endereco = new String[] {""} ;
         P00F26_n519Pessoa_Endereco = new bool[] {false} ;
         P00F26_A518Pessoa_IE = new String[] {""} ;
         P00F26_n518Pessoa_IE = new bool[] {false} ;
         P00F26_A37Pessoa_Docto = new String[] {""} ;
         P00F26_A36Pessoa_TipoPessoa = new String[] {""} ;
         P00F26_A35Pessoa_Nome = new String[] {""} ;
         P00F27_A38Pessoa_Ativo = new bool[] {false} ;
         P00F27_A521Pessoa_CEP = new String[] {""} ;
         P00F27_n521Pessoa_CEP = new bool[] {false} ;
         P00F27_A34Pessoa_Codigo = new int[1] ;
         P00F27_A523Pessoa_Fax = new String[] {""} ;
         P00F27_n523Pessoa_Fax = new bool[] {false} ;
         P00F27_A522Pessoa_Telefone = new String[] {""} ;
         P00F27_n522Pessoa_Telefone = new bool[] {false} ;
         P00F27_A520Pessoa_UF = new String[] {""} ;
         P00F27_n520Pessoa_UF = new bool[] {false} ;
         P00F27_A503Pessoa_MunicipioCod = new int[1] ;
         P00F27_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P00F27_A519Pessoa_Endereco = new String[] {""} ;
         P00F27_n519Pessoa_Endereco = new bool[] {false} ;
         P00F27_A518Pessoa_IE = new String[] {""} ;
         P00F27_n518Pessoa_IE = new bool[] {false} ;
         P00F27_A37Pessoa_Docto = new String[] {""} ;
         P00F27_A36Pessoa_TipoPessoa = new String[] {""} ;
         P00F27_A35Pessoa_Nome = new String[] {""} ;
         P00F28_A38Pessoa_Ativo = new bool[] {false} ;
         P00F28_A522Pessoa_Telefone = new String[] {""} ;
         P00F28_n522Pessoa_Telefone = new bool[] {false} ;
         P00F28_A34Pessoa_Codigo = new int[1] ;
         P00F28_A523Pessoa_Fax = new String[] {""} ;
         P00F28_n523Pessoa_Fax = new bool[] {false} ;
         P00F28_A521Pessoa_CEP = new String[] {""} ;
         P00F28_n521Pessoa_CEP = new bool[] {false} ;
         P00F28_A520Pessoa_UF = new String[] {""} ;
         P00F28_n520Pessoa_UF = new bool[] {false} ;
         P00F28_A503Pessoa_MunicipioCod = new int[1] ;
         P00F28_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P00F28_A519Pessoa_Endereco = new String[] {""} ;
         P00F28_n519Pessoa_Endereco = new bool[] {false} ;
         P00F28_A518Pessoa_IE = new String[] {""} ;
         P00F28_n518Pessoa_IE = new bool[] {false} ;
         P00F28_A37Pessoa_Docto = new String[] {""} ;
         P00F28_A36Pessoa_TipoPessoa = new String[] {""} ;
         P00F28_A35Pessoa_Nome = new String[] {""} ;
         P00F29_A38Pessoa_Ativo = new bool[] {false} ;
         P00F29_A523Pessoa_Fax = new String[] {""} ;
         P00F29_n523Pessoa_Fax = new bool[] {false} ;
         P00F29_A34Pessoa_Codigo = new int[1] ;
         P00F29_A522Pessoa_Telefone = new String[] {""} ;
         P00F29_n522Pessoa_Telefone = new bool[] {false} ;
         P00F29_A521Pessoa_CEP = new String[] {""} ;
         P00F29_n521Pessoa_CEP = new bool[] {false} ;
         P00F29_A520Pessoa_UF = new String[] {""} ;
         P00F29_n520Pessoa_UF = new bool[] {false} ;
         P00F29_A503Pessoa_MunicipioCod = new int[1] ;
         P00F29_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P00F29_A519Pessoa_Endereco = new String[] {""} ;
         P00F29_n519Pessoa_Endereco = new bool[] {false} ;
         P00F29_A518Pessoa_IE = new String[] {""} ;
         P00F29_n518Pessoa_IE = new bool[] {false} ;
         P00F29_A37Pessoa_Docto = new String[] {""} ;
         P00F29_A36Pessoa_TipoPessoa = new String[] {""} ;
         P00F29_A35Pessoa_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwpessoafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00F22_A35Pessoa_Nome, P00F22_A34Pessoa_Codigo, P00F22_A38Pessoa_Ativo, P00F22_A523Pessoa_Fax, P00F22_n523Pessoa_Fax, P00F22_A522Pessoa_Telefone, P00F22_n522Pessoa_Telefone, P00F22_A521Pessoa_CEP, P00F22_n521Pessoa_CEP, P00F22_A520Pessoa_UF,
               P00F22_n520Pessoa_UF, P00F22_A503Pessoa_MunicipioCod, P00F22_n503Pessoa_MunicipioCod, P00F22_A519Pessoa_Endereco, P00F22_n519Pessoa_Endereco, P00F22_A518Pessoa_IE, P00F22_n518Pessoa_IE, P00F22_A37Pessoa_Docto, P00F22_A36Pessoa_TipoPessoa
               }
               , new Object[] {
               P00F23_A37Pessoa_Docto, P00F23_A34Pessoa_Codigo, P00F23_A38Pessoa_Ativo, P00F23_A523Pessoa_Fax, P00F23_n523Pessoa_Fax, P00F23_A522Pessoa_Telefone, P00F23_n522Pessoa_Telefone, P00F23_A521Pessoa_CEP, P00F23_n521Pessoa_CEP, P00F23_A520Pessoa_UF,
               P00F23_n520Pessoa_UF, P00F23_A503Pessoa_MunicipioCod, P00F23_n503Pessoa_MunicipioCod, P00F23_A519Pessoa_Endereco, P00F23_n519Pessoa_Endereco, P00F23_A518Pessoa_IE, P00F23_n518Pessoa_IE, P00F23_A36Pessoa_TipoPessoa, P00F23_A35Pessoa_Nome
               }
               , new Object[] {
               P00F24_A38Pessoa_Ativo, P00F24_A518Pessoa_IE, P00F24_n518Pessoa_IE, P00F24_A34Pessoa_Codigo, P00F24_A523Pessoa_Fax, P00F24_n523Pessoa_Fax, P00F24_A522Pessoa_Telefone, P00F24_n522Pessoa_Telefone, P00F24_A521Pessoa_CEP, P00F24_n521Pessoa_CEP,
               P00F24_A520Pessoa_UF, P00F24_n520Pessoa_UF, P00F24_A503Pessoa_MunicipioCod, P00F24_n503Pessoa_MunicipioCod, P00F24_A519Pessoa_Endereco, P00F24_n519Pessoa_Endereco, P00F24_A37Pessoa_Docto, P00F24_A36Pessoa_TipoPessoa, P00F24_A35Pessoa_Nome
               }
               , new Object[] {
               P00F25_A38Pessoa_Ativo, P00F25_A519Pessoa_Endereco, P00F25_n519Pessoa_Endereco, P00F25_A34Pessoa_Codigo, P00F25_A523Pessoa_Fax, P00F25_n523Pessoa_Fax, P00F25_A522Pessoa_Telefone, P00F25_n522Pessoa_Telefone, P00F25_A521Pessoa_CEP, P00F25_n521Pessoa_CEP,
               P00F25_A520Pessoa_UF, P00F25_n520Pessoa_UF, P00F25_A503Pessoa_MunicipioCod, P00F25_n503Pessoa_MunicipioCod, P00F25_A518Pessoa_IE, P00F25_n518Pessoa_IE, P00F25_A37Pessoa_Docto, P00F25_A36Pessoa_TipoPessoa, P00F25_A35Pessoa_Nome
               }
               , new Object[] {
               P00F26_A38Pessoa_Ativo, P00F26_A520Pessoa_UF, P00F26_n520Pessoa_UF, P00F26_A34Pessoa_Codigo, P00F26_A523Pessoa_Fax, P00F26_n523Pessoa_Fax, P00F26_A522Pessoa_Telefone, P00F26_n522Pessoa_Telefone, P00F26_A521Pessoa_CEP, P00F26_n521Pessoa_CEP,
               P00F26_A503Pessoa_MunicipioCod, P00F26_n503Pessoa_MunicipioCod, P00F26_A519Pessoa_Endereco, P00F26_n519Pessoa_Endereco, P00F26_A518Pessoa_IE, P00F26_n518Pessoa_IE, P00F26_A37Pessoa_Docto, P00F26_A36Pessoa_TipoPessoa, P00F26_A35Pessoa_Nome
               }
               , new Object[] {
               P00F27_A38Pessoa_Ativo, P00F27_A521Pessoa_CEP, P00F27_n521Pessoa_CEP, P00F27_A34Pessoa_Codigo, P00F27_A523Pessoa_Fax, P00F27_n523Pessoa_Fax, P00F27_A522Pessoa_Telefone, P00F27_n522Pessoa_Telefone, P00F27_A520Pessoa_UF, P00F27_n520Pessoa_UF,
               P00F27_A503Pessoa_MunicipioCod, P00F27_n503Pessoa_MunicipioCod, P00F27_A519Pessoa_Endereco, P00F27_n519Pessoa_Endereco, P00F27_A518Pessoa_IE, P00F27_n518Pessoa_IE, P00F27_A37Pessoa_Docto, P00F27_A36Pessoa_TipoPessoa, P00F27_A35Pessoa_Nome
               }
               , new Object[] {
               P00F28_A38Pessoa_Ativo, P00F28_A522Pessoa_Telefone, P00F28_n522Pessoa_Telefone, P00F28_A34Pessoa_Codigo, P00F28_A523Pessoa_Fax, P00F28_n523Pessoa_Fax, P00F28_A521Pessoa_CEP, P00F28_n521Pessoa_CEP, P00F28_A520Pessoa_UF, P00F28_n520Pessoa_UF,
               P00F28_A503Pessoa_MunicipioCod, P00F28_n503Pessoa_MunicipioCod, P00F28_A519Pessoa_Endereco, P00F28_n519Pessoa_Endereco, P00F28_A518Pessoa_IE, P00F28_n518Pessoa_IE, P00F28_A37Pessoa_Docto, P00F28_A36Pessoa_TipoPessoa, P00F28_A35Pessoa_Nome
               }
               , new Object[] {
               P00F29_A38Pessoa_Ativo, P00F29_A523Pessoa_Fax, P00F29_n523Pessoa_Fax, P00F29_A34Pessoa_Codigo, P00F29_A522Pessoa_Telefone, P00F29_n522Pessoa_Telefone, P00F29_A521Pessoa_CEP, P00F29_n521Pessoa_CEP, P00F29_A520Pessoa_UF, P00F29_n520Pessoa_UF,
               P00F29_A503Pessoa_MunicipioCod, P00F29_n503Pessoa_MunicipioCod, P00F29_A519Pessoa_Endereco, P00F29_n519Pessoa_Endereco, P00F29_A518Pessoa_IE, P00F29_n518Pessoa_IE, P00F29_A37Pessoa_Docto, P00F29_A36Pessoa_TipoPessoa, P00F29_A35Pessoa_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFPessoa_Ativo_Sel ;
      private short AV36DynamicFiltersOperator1 ;
      private short AV42DynamicFiltersOperator2 ;
      private short AV48DynamicFiltersOperator3 ;
      private short AV71WWPessoaDS_2_Dynamicfiltersoperator1 ;
      private short AV77WWPessoaDS_8_Dynamicfiltersoperator2 ;
      private short AV83WWPessoaDS_14_Dynamicfiltersoperator3 ;
      private short AV106WWPessoaDS_37_Tfpessoa_ativo_sel ;
      private int AV68GXV1 ;
      private int AV56TFPessoa_MunicipioCod ;
      private int AV57TFPessoa_MunicipioCod_To ;
      private int AV96WWPessoaDS_27_Tfpessoa_municipiocod ;
      private int AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ;
      private int AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count ;
      private int AV107Usuario_pessoacod ;
      private int A503Pessoa_MunicipioCod ;
      private int A34Pessoa_Codigo ;
      private long AV29count ;
      private String AV10TFPessoa_Nome ;
      private String AV11TFPessoa_Nome_Sel ;
      private String AV52TFPessoa_IE ;
      private String AV53TFPessoa_IE_Sel ;
      private String AV58TFPessoa_UF ;
      private String AV59TFPessoa_UF_Sel ;
      private String AV60TFPessoa_CEP ;
      private String AV61TFPessoa_CEP_Sel ;
      private String AV62TFPessoa_Telefone ;
      private String AV63TFPessoa_Telefone_Sel ;
      private String AV64TFPessoa_Fax ;
      private String AV65TFPessoa_Fax_Sel ;
      private String AV37Pessoa_Nome1 ;
      private String AV38Pessoa_TipoPessoa1 ;
      private String AV43Pessoa_Nome2 ;
      private String AV44Pessoa_TipoPessoa2 ;
      private String AV49Pessoa_Nome3 ;
      private String AV50Pessoa_TipoPessoa3 ;
      private String AV72WWPessoaDS_3_Pessoa_nome1 ;
      private String AV73WWPessoaDS_4_Pessoa_tipopessoa1 ;
      private String AV78WWPessoaDS_9_Pessoa_nome2 ;
      private String AV79WWPessoaDS_10_Pessoa_tipopessoa2 ;
      private String AV84WWPessoaDS_15_Pessoa_nome3 ;
      private String AV85WWPessoaDS_16_Pessoa_tipopessoa3 ;
      private String AV87WWPessoaDS_18_Tfpessoa_nome ;
      private String AV88WWPessoaDS_19_Tfpessoa_nome_sel ;
      private String AV92WWPessoaDS_23_Tfpessoa_ie ;
      private String AV93WWPessoaDS_24_Tfpessoa_ie_sel ;
      private String AV98WWPessoaDS_29_Tfpessoa_uf ;
      private String AV99WWPessoaDS_30_Tfpessoa_uf_sel ;
      private String AV100WWPessoaDS_31_Tfpessoa_cep ;
      private String AV101WWPessoaDS_32_Tfpessoa_cep_sel ;
      private String AV102WWPessoaDS_33_Tfpessoa_telefone ;
      private String AV103WWPessoaDS_34_Tfpessoa_telefone_sel ;
      private String AV104WWPessoaDS_35_Tfpessoa_fax ;
      private String AV105WWPessoaDS_36_Tfpessoa_fax_sel ;
      private String scmdbuf ;
      private String lV72WWPessoaDS_3_Pessoa_nome1 ;
      private String lV78WWPessoaDS_9_Pessoa_nome2 ;
      private String lV84WWPessoaDS_15_Pessoa_nome3 ;
      private String lV87WWPessoaDS_18_Tfpessoa_nome ;
      private String lV92WWPessoaDS_23_Tfpessoa_ie ;
      private String lV98WWPessoaDS_29_Tfpessoa_uf ;
      private String lV100WWPessoaDS_31_Tfpessoa_cep ;
      private String lV102WWPessoaDS_33_Tfpessoa_telefone ;
      private String lV104WWPessoaDS_35_Tfpessoa_fax ;
      private String A36Pessoa_TipoPessoa ;
      private String A35Pessoa_Nome ;
      private String A518Pessoa_IE ;
      private String A520Pessoa_UF ;
      private String A521Pessoa_CEP ;
      private String A522Pessoa_Telefone ;
      private String A523Pessoa_Fax ;
      private bool returnInSub ;
      private bool AV40DynamicFiltersEnabled2 ;
      private bool AV46DynamicFiltersEnabled3 ;
      private bool AV75WWPessoaDS_6_Dynamicfiltersenabled2 ;
      private bool AV81WWPessoaDS_12_Dynamicfiltersenabled3 ;
      private bool A38Pessoa_Ativo ;
      private bool BRKF22 ;
      private bool n523Pessoa_Fax ;
      private bool n522Pessoa_Telefone ;
      private bool n521Pessoa_CEP ;
      private bool n520Pessoa_UF ;
      private bool n503Pessoa_MunicipioCod ;
      private bool n519Pessoa_Endereco ;
      private bool n518Pessoa_IE ;
      private bool BRKF24 ;
      private bool BRKF26 ;
      private bool BRKF28 ;
      private bool BRKF210 ;
      private bool BRKF212 ;
      private bool BRKF214 ;
      private bool BRKF216 ;
      private String AV28OptionIndexesJson ;
      private String AV23OptionsJson ;
      private String AV26OptionsDescJson ;
      private String AV12TFPessoa_TipoPessoa_SelsJson ;
      private String AV19DDOName ;
      private String AV17SearchTxt ;
      private String AV18SearchTxtTo ;
      private String AV14TFPessoa_Docto ;
      private String AV15TFPessoa_Docto_Sel ;
      private String AV54TFPessoa_Endereco ;
      private String AV55TFPessoa_Endereco_Sel ;
      private String AV35DynamicFiltersSelector1 ;
      private String AV39Pessoa_Docto1 ;
      private String AV41DynamicFiltersSelector2 ;
      private String AV45Pessoa_Docto2 ;
      private String AV47DynamicFiltersSelector3 ;
      private String AV51Pessoa_Docto3 ;
      private String AV70WWPessoaDS_1_Dynamicfiltersselector1 ;
      private String AV74WWPessoaDS_5_Pessoa_docto1 ;
      private String AV76WWPessoaDS_7_Dynamicfiltersselector2 ;
      private String AV80WWPessoaDS_11_Pessoa_docto2 ;
      private String AV82WWPessoaDS_13_Dynamicfiltersselector3 ;
      private String AV86WWPessoaDS_17_Pessoa_docto3 ;
      private String AV90WWPessoaDS_21_Tfpessoa_docto ;
      private String AV91WWPessoaDS_22_Tfpessoa_docto_sel ;
      private String AV94WWPessoaDS_25_Tfpessoa_endereco ;
      private String AV95WWPessoaDS_26_Tfpessoa_endereco_sel ;
      private String lV74WWPessoaDS_5_Pessoa_docto1 ;
      private String lV80WWPessoaDS_11_Pessoa_docto2 ;
      private String lV86WWPessoaDS_17_Pessoa_docto3 ;
      private String lV90WWPessoaDS_21_Tfpessoa_docto ;
      private String lV94WWPessoaDS_25_Tfpessoa_endereco ;
      private String A37Pessoa_Docto ;
      private String A519Pessoa_Endereco ;
      private String AV21Option ;
      private IGxSession AV30Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00F22_A35Pessoa_Nome ;
      private int[] P00F22_A34Pessoa_Codigo ;
      private bool[] P00F22_A38Pessoa_Ativo ;
      private String[] P00F22_A523Pessoa_Fax ;
      private bool[] P00F22_n523Pessoa_Fax ;
      private String[] P00F22_A522Pessoa_Telefone ;
      private bool[] P00F22_n522Pessoa_Telefone ;
      private String[] P00F22_A521Pessoa_CEP ;
      private bool[] P00F22_n521Pessoa_CEP ;
      private String[] P00F22_A520Pessoa_UF ;
      private bool[] P00F22_n520Pessoa_UF ;
      private int[] P00F22_A503Pessoa_MunicipioCod ;
      private bool[] P00F22_n503Pessoa_MunicipioCod ;
      private String[] P00F22_A519Pessoa_Endereco ;
      private bool[] P00F22_n519Pessoa_Endereco ;
      private String[] P00F22_A518Pessoa_IE ;
      private bool[] P00F22_n518Pessoa_IE ;
      private String[] P00F22_A37Pessoa_Docto ;
      private String[] P00F22_A36Pessoa_TipoPessoa ;
      private String[] P00F23_A37Pessoa_Docto ;
      private int[] P00F23_A34Pessoa_Codigo ;
      private bool[] P00F23_A38Pessoa_Ativo ;
      private String[] P00F23_A523Pessoa_Fax ;
      private bool[] P00F23_n523Pessoa_Fax ;
      private String[] P00F23_A522Pessoa_Telefone ;
      private bool[] P00F23_n522Pessoa_Telefone ;
      private String[] P00F23_A521Pessoa_CEP ;
      private bool[] P00F23_n521Pessoa_CEP ;
      private String[] P00F23_A520Pessoa_UF ;
      private bool[] P00F23_n520Pessoa_UF ;
      private int[] P00F23_A503Pessoa_MunicipioCod ;
      private bool[] P00F23_n503Pessoa_MunicipioCod ;
      private String[] P00F23_A519Pessoa_Endereco ;
      private bool[] P00F23_n519Pessoa_Endereco ;
      private String[] P00F23_A518Pessoa_IE ;
      private bool[] P00F23_n518Pessoa_IE ;
      private String[] P00F23_A36Pessoa_TipoPessoa ;
      private String[] P00F23_A35Pessoa_Nome ;
      private bool[] P00F24_A38Pessoa_Ativo ;
      private String[] P00F24_A518Pessoa_IE ;
      private bool[] P00F24_n518Pessoa_IE ;
      private int[] P00F24_A34Pessoa_Codigo ;
      private String[] P00F24_A523Pessoa_Fax ;
      private bool[] P00F24_n523Pessoa_Fax ;
      private String[] P00F24_A522Pessoa_Telefone ;
      private bool[] P00F24_n522Pessoa_Telefone ;
      private String[] P00F24_A521Pessoa_CEP ;
      private bool[] P00F24_n521Pessoa_CEP ;
      private String[] P00F24_A520Pessoa_UF ;
      private bool[] P00F24_n520Pessoa_UF ;
      private int[] P00F24_A503Pessoa_MunicipioCod ;
      private bool[] P00F24_n503Pessoa_MunicipioCod ;
      private String[] P00F24_A519Pessoa_Endereco ;
      private bool[] P00F24_n519Pessoa_Endereco ;
      private String[] P00F24_A37Pessoa_Docto ;
      private String[] P00F24_A36Pessoa_TipoPessoa ;
      private String[] P00F24_A35Pessoa_Nome ;
      private bool[] P00F25_A38Pessoa_Ativo ;
      private String[] P00F25_A519Pessoa_Endereco ;
      private bool[] P00F25_n519Pessoa_Endereco ;
      private int[] P00F25_A34Pessoa_Codigo ;
      private String[] P00F25_A523Pessoa_Fax ;
      private bool[] P00F25_n523Pessoa_Fax ;
      private String[] P00F25_A522Pessoa_Telefone ;
      private bool[] P00F25_n522Pessoa_Telefone ;
      private String[] P00F25_A521Pessoa_CEP ;
      private bool[] P00F25_n521Pessoa_CEP ;
      private String[] P00F25_A520Pessoa_UF ;
      private bool[] P00F25_n520Pessoa_UF ;
      private int[] P00F25_A503Pessoa_MunicipioCod ;
      private bool[] P00F25_n503Pessoa_MunicipioCod ;
      private String[] P00F25_A518Pessoa_IE ;
      private bool[] P00F25_n518Pessoa_IE ;
      private String[] P00F25_A37Pessoa_Docto ;
      private String[] P00F25_A36Pessoa_TipoPessoa ;
      private String[] P00F25_A35Pessoa_Nome ;
      private bool[] P00F26_A38Pessoa_Ativo ;
      private String[] P00F26_A520Pessoa_UF ;
      private bool[] P00F26_n520Pessoa_UF ;
      private int[] P00F26_A34Pessoa_Codigo ;
      private String[] P00F26_A523Pessoa_Fax ;
      private bool[] P00F26_n523Pessoa_Fax ;
      private String[] P00F26_A522Pessoa_Telefone ;
      private bool[] P00F26_n522Pessoa_Telefone ;
      private String[] P00F26_A521Pessoa_CEP ;
      private bool[] P00F26_n521Pessoa_CEP ;
      private int[] P00F26_A503Pessoa_MunicipioCod ;
      private bool[] P00F26_n503Pessoa_MunicipioCod ;
      private String[] P00F26_A519Pessoa_Endereco ;
      private bool[] P00F26_n519Pessoa_Endereco ;
      private String[] P00F26_A518Pessoa_IE ;
      private bool[] P00F26_n518Pessoa_IE ;
      private String[] P00F26_A37Pessoa_Docto ;
      private String[] P00F26_A36Pessoa_TipoPessoa ;
      private String[] P00F26_A35Pessoa_Nome ;
      private bool[] P00F27_A38Pessoa_Ativo ;
      private String[] P00F27_A521Pessoa_CEP ;
      private bool[] P00F27_n521Pessoa_CEP ;
      private int[] P00F27_A34Pessoa_Codigo ;
      private String[] P00F27_A523Pessoa_Fax ;
      private bool[] P00F27_n523Pessoa_Fax ;
      private String[] P00F27_A522Pessoa_Telefone ;
      private bool[] P00F27_n522Pessoa_Telefone ;
      private String[] P00F27_A520Pessoa_UF ;
      private bool[] P00F27_n520Pessoa_UF ;
      private int[] P00F27_A503Pessoa_MunicipioCod ;
      private bool[] P00F27_n503Pessoa_MunicipioCod ;
      private String[] P00F27_A519Pessoa_Endereco ;
      private bool[] P00F27_n519Pessoa_Endereco ;
      private String[] P00F27_A518Pessoa_IE ;
      private bool[] P00F27_n518Pessoa_IE ;
      private String[] P00F27_A37Pessoa_Docto ;
      private String[] P00F27_A36Pessoa_TipoPessoa ;
      private String[] P00F27_A35Pessoa_Nome ;
      private bool[] P00F28_A38Pessoa_Ativo ;
      private String[] P00F28_A522Pessoa_Telefone ;
      private bool[] P00F28_n522Pessoa_Telefone ;
      private int[] P00F28_A34Pessoa_Codigo ;
      private String[] P00F28_A523Pessoa_Fax ;
      private bool[] P00F28_n523Pessoa_Fax ;
      private String[] P00F28_A521Pessoa_CEP ;
      private bool[] P00F28_n521Pessoa_CEP ;
      private String[] P00F28_A520Pessoa_UF ;
      private bool[] P00F28_n520Pessoa_UF ;
      private int[] P00F28_A503Pessoa_MunicipioCod ;
      private bool[] P00F28_n503Pessoa_MunicipioCod ;
      private String[] P00F28_A519Pessoa_Endereco ;
      private bool[] P00F28_n519Pessoa_Endereco ;
      private String[] P00F28_A518Pessoa_IE ;
      private bool[] P00F28_n518Pessoa_IE ;
      private String[] P00F28_A37Pessoa_Docto ;
      private String[] P00F28_A36Pessoa_TipoPessoa ;
      private String[] P00F28_A35Pessoa_Nome ;
      private bool[] P00F29_A38Pessoa_Ativo ;
      private String[] P00F29_A523Pessoa_Fax ;
      private bool[] P00F29_n523Pessoa_Fax ;
      private int[] P00F29_A34Pessoa_Codigo ;
      private String[] P00F29_A522Pessoa_Telefone ;
      private bool[] P00F29_n522Pessoa_Telefone ;
      private String[] P00F29_A521Pessoa_CEP ;
      private bool[] P00F29_n521Pessoa_CEP ;
      private String[] P00F29_A520Pessoa_UF ;
      private bool[] P00F29_n520Pessoa_UF ;
      private int[] P00F29_A503Pessoa_MunicipioCod ;
      private bool[] P00F29_n503Pessoa_MunicipioCod ;
      private String[] P00F29_A519Pessoa_Endereco ;
      private bool[] P00F29_n519Pessoa_Endereco ;
      private String[] P00F29_A518Pessoa_IE ;
      private bool[] P00F29_n518Pessoa_IE ;
      private String[] P00F29_A37Pessoa_Docto ;
      private String[] P00F29_A36Pessoa_TipoPessoa ;
      private String[] P00F29_A35Pessoa_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFPessoa_TipoPessoa_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV32GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV33GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV34GridStateDynamicFilter ;
   }

   public class getwwpessoafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00F22( IGxContext context ,
                                             String A36Pessoa_TipoPessoa ,
                                             IGxCollection AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                             String AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                             short AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWPessoaDS_3_Pessoa_nome1 ,
                                             String AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                             String AV74WWPessoaDS_5_Pessoa_docto1 ,
                                             bool AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                             String AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                             short AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                             String AV78WWPessoaDS_9_Pessoa_nome2 ,
                                             String AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                             String AV80WWPessoaDS_11_Pessoa_docto2 ,
                                             bool AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                             String AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                             short AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                             String AV84WWPessoaDS_15_Pessoa_nome3 ,
                                             String AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                             String AV86WWPessoaDS_17_Pessoa_docto3 ,
                                             String AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                             String AV87WWPessoaDS_18_Tfpessoa_nome ,
                                             int AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count ,
                                             String AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                             String AV90WWPessoaDS_21_Tfpessoa_docto ,
                                             String AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                             String AV92WWPessoaDS_23_Tfpessoa_ie ,
                                             String AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                             String AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                             int AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                             int AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                             String AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                             String AV98WWPessoaDS_29_Tfpessoa_uf ,
                                             String AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                             String AV100WWPessoaDS_31_Tfpessoa_cep ,
                                             String AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                             String AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                             String AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                             String AV104WWPessoaDS_35_Tfpessoa_fax ,
                                             short AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                             int AV107Usuario_pessoacod ,
                                             String A35Pessoa_Nome ,
                                             String A37Pessoa_Docto ,
                                             String A518Pessoa_IE ,
                                             String A519Pessoa_Endereco ,
                                             int A503Pessoa_MunicipioCod ,
                                             String A520Pessoa_UF ,
                                             String A521Pessoa_CEP ,
                                             String A522Pessoa_Telefone ,
                                             String A523Pessoa_Fax ,
                                             bool A38Pessoa_Ativo ,
                                             int A34Pessoa_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [34] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Pessoa_Nome], T1.[Pessoa_Codigo], T1.[Pessoa_Ativo], T1.[Pessoa_Fax], T1.[Pessoa_Telefone], T1.[Pessoa_CEP], T2.[Estado_UF] AS Pessoa_UF, T1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Pessoa_Endereco], T1.[Pessoa_IE], T1.[Pessoa_Docto], T1.[Pessoa_TipoPessoa] FROM ([Pessoa] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Pessoa_MunicipioCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Pessoa_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWPessoaDS_4_Pessoa_tipopessoa1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV73WWPessoaDS_4_Pessoa_tipopessoa1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWPessoaDS_10_Pessoa_tipopessoa2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV79WWPessoaDS_10_Pessoa_tipopessoa2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWPessoaDS_16_Pessoa_tipopessoa3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV85WWPessoaDS_16_Pessoa_tipopessoa3)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV87WWPessoaDS_18_Tfpessoa_nome)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] = @AV88WWPessoaDS_19_Tfpessoa_nome_sel)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels, "T1.[Pessoa_TipoPessoa] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV90WWPessoaDS_21_Tfpessoa_docto)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] = @AV91WWPessoaDS_22_Tfpessoa_docto_sel)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] like @lV92WWPessoaDS_23_Tfpessoa_ie)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] = @AV93WWPessoaDS_24_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] like @lV94WWPessoaDS_25_Tfpessoa_endereco)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] = @AV95WWPessoaDS_26_Tfpessoa_endereco_sel)";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (0==AV96WWPessoaDS_27_Tfpessoa_municipiocod) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] >= @AV96WWPessoaDS_27_Tfpessoa_municipiocod)";
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (0==AV97WWPessoaDS_28_Tfpessoa_municipiocod_to) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] <= @AV97WWPessoaDS_28_Tfpessoa_municipiocod_to)";
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] like @lV98WWPessoaDS_29_Tfpessoa_uf)";
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] = @AV99WWPessoaDS_30_Tfpessoa_uf_sel)";
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] like @lV100WWPessoaDS_31_Tfpessoa_cep)";
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] = @AV101WWPessoaDS_32_Tfpessoa_cep_sel)";
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] like @lV102WWPessoaDS_33_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] = @AV103WWPessoaDS_34_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] like @lV104WWPessoaDS_35_Tfpessoa_fax)";
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] = @AV105WWPessoaDS_36_Tfpessoa_fax_sel)";
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 1)";
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 0)";
         }
         if ( AV107Usuario_pessoacod > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Codigo] = @AV107Usuario_pessoacod)";
         }
         else
         {
            GXv_int1[33] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Pessoa_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00F23( IGxContext context ,
                                             String A36Pessoa_TipoPessoa ,
                                             IGxCollection AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                             String AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                             short AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWPessoaDS_3_Pessoa_nome1 ,
                                             String AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                             String AV74WWPessoaDS_5_Pessoa_docto1 ,
                                             bool AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                             String AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                             short AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                             String AV78WWPessoaDS_9_Pessoa_nome2 ,
                                             String AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                             String AV80WWPessoaDS_11_Pessoa_docto2 ,
                                             bool AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                             String AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                             short AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                             String AV84WWPessoaDS_15_Pessoa_nome3 ,
                                             String AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                             String AV86WWPessoaDS_17_Pessoa_docto3 ,
                                             String AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                             String AV87WWPessoaDS_18_Tfpessoa_nome ,
                                             int AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count ,
                                             String AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                             String AV90WWPessoaDS_21_Tfpessoa_docto ,
                                             String AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                             String AV92WWPessoaDS_23_Tfpessoa_ie ,
                                             String AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                             String AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                             int AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                             int AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                             String AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                             String AV98WWPessoaDS_29_Tfpessoa_uf ,
                                             String AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                             String AV100WWPessoaDS_31_Tfpessoa_cep ,
                                             String AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                             String AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                             String AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                             String AV104WWPessoaDS_35_Tfpessoa_fax ,
                                             short AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                             int AV107Usuario_pessoacod ,
                                             String A35Pessoa_Nome ,
                                             String A37Pessoa_Docto ,
                                             String A518Pessoa_IE ,
                                             String A519Pessoa_Endereco ,
                                             int A503Pessoa_MunicipioCod ,
                                             String A520Pessoa_UF ,
                                             String A521Pessoa_CEP ,
                                             String A522Pessoa_Telefone ,
                                             String A523Pessoa_Fax ,
                                             bool A38Pessoa_Ativo ,
                                             int A34Pessoa_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [34] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Pessoa_Docto], T1.[Pessoa_Codigo], T1.[Pessoa_Ativo], T1.[Pessoa_Fax], T1.[Pessoa_Telefone], T1.[Pessoa_CEP], T2.[Estado_UF] AS Pessoa_UF, T1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Pessoa_Endereco], T1.[Pessoa_IE], T1.[Pessoa_TipoPessoa], T1.[Pessoa_Nome] FROM ([Pessoa] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Pessoa_MunicipioCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Pessoa_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWPessoaDS_4_Pessoa_tipopessoa1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV73WWPessoaDS_4_Pessoa_tipopessoa1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWPessoaDS_10_Pessoa_tipopessoa2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV79WWPessoaDS_10_Pessoa_tipopessoa2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWPessoaDS_16_Pessoa_tipopessoa3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV85WWPessoaDS_16_Pessoa_tipopessoa3)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV87WWPessoaDS_18_Tfpessoa_nome)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] = @AV88WWPessoaDS_19_Tfpessoa_nome_sel)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels, "T1.[Pessoa_TipoPessoa] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV90WWPessoaDS_21_Tfpessoa_docto)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] = @AV91WWPessoaDS_22_Tfpessoa_docto_sel)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] like @lV92WWPessoaDS_23_Tfpessoa_ie)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] = @AV93WWPessoaDS_24_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] like @lV94WWPessoaDS_25_Tfpessoa_endereco)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] = @AV95WWPessoaDS_26_Tfpessoa_endereco_sel)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (0==AV96WWPessoaDS_27_Tfpessoa_municipiocod) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] >= @AV96WWPessoaDS_27_Tfpessoa_municipiocod)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! (0==AV97WWPessoaDS_28_Tfpessoa_municipiocod_to) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] <= @AV97WWPessoaDS_28_Tfpessoa_municipiocod_to)";
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] like @lV98WWPessoaDS_29_Tfpessoa_uf)";
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] = @AV99WWPessoaDS_30_Tfpessoa_uf_sel)";
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] like @lV100WWPessoaDS_31_Tfpessoa_cep)";
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] = @AV101WWPessoaDS_32_Tfpessoa_cep_sel)";
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] like @lV102WWPessoaDS_33_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] = @AV103WWPessoaDS_34_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] like @lV104WWPessoaDS_35_Tfpessoa_fax)";
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] = @AV105WWPessoaDS_36_Tfpessoa_fax_sel)";
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 1)";
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 0)";
         }
         if ( AV107Usuario_pessoacod > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Codigo] = @AV107Usuario_pessoacod)";
         }
         else
         {
            GXv_int3[33] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Pessoa_Docto]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00F24( IGxContext context ,
                                             String A36Pessoa_TipoPessoa ,
                                             IGxCollection AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                             String AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                             short AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWPessoaDS_3_Pessoa_nome1 ,
                                             String AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                             String AV74WWPessoaDS_5_Pessoa_docto1 ,
                                             bool AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                             String AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                             short AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                             String AV78WWPessoaDS_9_Pessoa_nome2 ,
                                             String AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                             String AV80WWPessoaDS_11_Pessoa_docto2 ,
                                             bool AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                             String AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                             short AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                             String AV84WWPessoaDS_15_Pessoa_nome3 ,
                                             String AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                             String AV86WWPessoaDS_17_Pessoa_docto3 ,
                                             String AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                             String AV87WWPessoaDS_18_Tfpessoa_nome ,
                                             int AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count ,
                                             String AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                             String AV90WWPessoaDS_21_Tfpessoa_docto ,
                                             String AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                             String AV92WWPessoaDS_23_Tfpessoa_ie ,
                                             String AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                             String AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                             int AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                             int AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                             String AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                             String AV98WWPessoaDS_29_Tfpessoa_uf ,
                                             String AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                             String AV100WWPessoaDS_31_Tfpessoa_cep ,
                                             String AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                             String AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                             String AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                             String AV104WWPessoaDS_35_Tfpessoa_fax ,
                                             short AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                             int AV107Usuario_pessoacod ,
                                             String A35Pessoa_Nome ,
                                             String A37Pessoa_Docto ,
                                             String A518Pessoa_IE ,
                                             String A519Pessoa_Endereco ,
                                             int A503Pessoa_MunicipioCod ,
                                             String A520Pessoa_UF ,
                                             String A521Pessoa_CEP ,
                                             String A522Pessoa_Telefone ,
                                             String A523Pessoa_Fax ,
                                             bool A38Pessoa_Ativo ,
                                             int A34Pessoa_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [34] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Pessoa_Ativo], T1.[Pessoa_IE], T1.[Pessoa_Codigo], T1.[Pessoa_Fax], T1.[Pessoa_Telefone], T1.[Pessoa_CEP], T2.[Estado_UF] AS Pessoa_UF, T1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Pessoa_Endereco], T1.[Pessoa_Docto], T1.[Pessoa_TipoPessoa], T1.[Pessoa_Nome] FROM ([Pessoa] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Pessoa_MunicipioCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Pessoa_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWPessoaDS_4_Pessoa_tipopessoa1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV73WWPessoaDS_4_Pessoa_tipopessoa1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWPessoaDS_10_Pessoa_tipopessoa2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV79WWPessoaDS_10_Pessoa_tipopessoa2)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWPessoaDS_16_Pessoa_tipopessoa3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV85WWPessoaDS_16_Pessoa_tipopessoa3)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV87WWPessoaDS_18_Tfpessoa_nome)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] = @AV88WWPessoaDS_19_Tfpessoa_nome_sel)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels, "T1.[Pessoa_TipoPessoa] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV90WWPessoaDS_21_Tfpessoa_docto)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] = @AV91WWPessoaDS_22_Tfpessoa_docto_sel)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] like @lV92WWPessoaDS_23_Tfpessoa_ie)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] = @AV93WWPessoaDS_24_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] like @lV94WWPessoaDS_25_Tfpessoa_endereco)";
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] = @AV95WWPessoaDS_26_Tfpessoa_endereco_sel)";
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (0==AV96WWPessoaDS_27_Tfpessoa_municipiocod) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] >= @AV96WWPessoaDS_27_Tfpessoa_municipiocod)";
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( ! (0==AV97WWPessoaDS_28_Tfpessoa_municipiocod_to) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] <= @AV97WWPessoaDS_28_Tfpessoa_municipiocod_to)";
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] like @lV98WWPessoaDS_29_Tfpessoa_uf)";
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] = @AV99WWPessoaDS_30_Tfpessoa_uf_sel)";
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] like @lV100WWPessoaDS_31_Tfpessoa_cep)";
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] = @AV101WWPessoaDS_32_Tfpessoa_cep_sel)";
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] like @lV102WWPessoaDS_33_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] = @AV103WWPessoaDS_34_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] like @lV104WWPessoaDS_35_Tfpessoa_fax)";
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] = @AV105WWPessoaDS_36_Tfpessoa_fax_sel)";
         }
         else
         {
            GXv_int5[32] = 1;
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 1)";
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 0)";
         }
         if ( AV107Usuario_pessoacod > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Codigo] = @AV107Usuario_pessoacod)";
         }
         else
         {
            GXv_int5[33] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Pessoa_IE]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00F25( IGxContext context ,
                                             String A36Pessoa_TipoPessoa ,
                                             IGxCollection AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                             String AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                             short AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWPessoaDS_3_Pessoa_nome1 ,
                                             String AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                             String AV74WWPessoaDS_5_Pessoa_docto1 ,
                                             bool AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                             String AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                             short AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                             String AV78WWPessoaDS_9_Pessoa_nome2 ,
                                             String AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                             String AV80WWPessoaDS_11_Pessoa_docto2 ,
                                             bool AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                             String AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                             short AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                             String AV84WWPessoaDS_15_Pessoa_nome3 ,
                                             String AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                             String AV86WWPessoaDS_17_Pessoa_docto3 ,
                                             String AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                             String AV87WWPessoaDS_18_Tfpessoa_nome ,
                                             int AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count ,
                                             String AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                             String AV90WWPessoaDS_21_Tfpessoa_docto ,
                                             String AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                             String AV92WWPessoaDS_23_Tfpessoa_ie ,
                                             String AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                             String AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                             int AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                             int AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                             String AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                             String AV98WWPessoaDS_29_Tfpessoa_uf ,
                                             String AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                             String AV100WWPessoaDS_31_Tfpessoa_cep ,
                                             String AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                             String AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                             String AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                             String AV104WWPessoaDS_35_Tfpessoa_fax ,
                                             short AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                             int AV107Usuario_pessoacod ,
                                             String A35Pessoa_Nome ,
                                             String A37Pessoa_Docto ,
                                             String A518Pessoa_IE ,
                                             String A519Pessoa_Endereco ,
                                             int A503Pessoa_MunicipioCod ,
                                             String A520Pessoa_UF ,
                                             String A521Pessoa_CEP ,
                                             String A522Pessoa_Telefone ,
                                             String A523Pessoa_Fax ,
                                             bool A38Pessoa_Ativo ,
                                             int A34Pessoa_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [34] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Pessoa_Ativo], T1.[Pessoa_Endereco], T1.[Pessoa_Codigo], T1.[Pessoa_Fax], T1.[Pessoa_Telefone], T1.[Pessoa_CEP], T2.[Estado_UF] AS Pessoa_UF, T1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Pessoa_IE], T1.[Pessoa_Docto], T1.[Pessoa_TipoPessoa], T1.[Pessoa_Nome] FROM ([Pessoa] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Pessoa_MunicipioCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Pessoa_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWPessoaDS_4_Pessoa_tipopessoa1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV73WWPessoaDS_4_Pessoa_tipopessoa1)";
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWPessoaDS_10_Pessoa_tipopessoa2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV79WWPessoaDS_10_Pessoa_tipopessoa2)";
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWPessoaDS_16_Pessoa_tipopessoa3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV85WWPessoaDS_16_Pessoa_tipopessoa3)";
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV87WWPessoaDS_18_Tfpessoa_nome)";
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] = @AV88WWPessoaDS_19_Tfpessoa_nome_sel)";
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels, "T1.[Pessoa_TipoPessoa] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV90WWPessoaDS_21_Tfpessoa_docto)";
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] = @AV91WWPessoaDS_22_Tfpessoa_docto_sel)";
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] like @lV92WWPessoaDS_23_Tfpessoa_ie)";
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] = @AV93WWPessoaDS_24_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] like @lV94WWPessoaDS_25_Tfpessoa_endereco)";
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] = @AV95WWPessoaDS_26_Tfpessoa_endereco_sel)";
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( ! (0==AV96WWPessoaDS_27_Tfpessoa_municipiocod) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] >= @AV96WWPessoaDS_27_Tfpessoa_municipiocod)";
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( ! (0==AV97WWPessoaDS_28_Tfpessoa_municipiocod_to) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] <= @AV97WWPessoaDS_28_Tfpessoa_municipiocod_to)";
         }
         else
         {
            GXv_int7[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] like @lV98WWPessoaDS_29_Tfpessoa_uf)";
         }
         else
         {
            GXv_int7[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] = @AV99WWPessoaDS_30_Tfpessoa_uf_sel)";
         }
         else
         {
            GXv_int7[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] like @lV100WWPessoaDS_31_Tfpessoa_cep)";
         }
         else
         {
            GXv_int7[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] = @AV101WWPessoaDS_32_Tfpessoa_cep_sel)";
         }
         else
         {
            GXv_int7[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] like @lV102WWPessoaDS_33_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int7[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] = @AV103WWPessoaDS_34_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int7[30] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] like @lV104WWPessoaDS_35_Tfpessoa_fax)";
         }
         else
         {
            GXv_int7[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] = @AV105WWPessoaDS_36_Tfpessoa_fax_sel)";
         }
         else
         {
            GXv_int7[32] = 1;
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 1)";
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 0)";
         }
         if ( AV107Usuario_pessoacod > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Codigo] = @AV107Usuario_pessoacod)";
         }
         else
         {
            GXv_int7[33] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Pessoa_Endereco]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00F26( IGxContext context ,
                                             String A36Pessoa_TipoPessoa ,
                                             IGxCollection AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                             String AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                             short AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWPessoaDS_3_Pessoa_nome1 ,
                                             String AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                             String AV74WWPessoaDS_5_Pessoa_docto1 ,
                                             bool AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                             String AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                             short AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                             String AV78WWPessoaDS_9_Pessoa_nome2 ,
                                             String AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                             String AV80WWPessoaDS_11_Pessoa_docto2 ,
                                             bool AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                             String AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                             short AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                             String AV84WWPessoaDS_15_Pessoa_nome3 ,
                                             String AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                             String AV86WWPessoaDS_17_Pessoa_docto3 ,
                                             String AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                             String AV87WWPessoaDS_18_Tfpessoa_nome ,
                                             int AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count ,
                                             String AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                             String AV90WWPessoaDS_21_Tfpessoa_docto ,
                                             String AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                             String AV92WWPessoaDS_23_Tfpessoa_ie ,
                                             String AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                             String AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                             int AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                             int AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                             String AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                             String AV98WWPessoaDS_29_Tfpessoa_uf ,
                                             String AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                             String AV100WWPessoaDS_31_Tfpessoa_cep ,
                                             String AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                             String AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                             String AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                             String AV104WWPessoaDS_35_Tfpessoa_fax ,
                                             short AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                             int AV107Usuario_pessoacod ,
                                             String A35Pessoa_Nome ,
                                             String A37Pessoa_Docto ,
                                             String A518Pessoa_IE ,
                                             String A519Pessoa_Endereco ,
                                             int A503Pessoa_MunicipioCod ,
                                             String A520Pessoa_UF ,
                                             String A521Pessoa_CEP ,
                                             String A522Pessoa_Telefone ,
                                             String A523Pessoa_Fax ,
                                             bool A38Pessoa_Ativo ,
                                             int A34Pessoa_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [34] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[Pessoa_Ativo], T2.[Estado_UF] AS Pessoa_UF, T1.[Pessoa_Codigo], T1.[Pessoa_Fax], T1.[Pessoa_Telefone], T1.[Pessoa_CEP], T1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Pessoa_Endereco], T1.[Pessoa_IE], T1.[Pessoa_Docto], T1.[Pessoa_TipoPessoa], T1.[Pessoa_Nome] FROM ([Pessoa] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Pessoa_MunicipioCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Pessoa_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int9[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int9[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWPessoaDS_4_Pessoa_tipopessoa1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV73WWPessoaDS_4_Pessoa_tipopessoa1)";
         }
         else
         {
            GXv_int9[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int9[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int9[4] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWPessoaDS_10_Pessoa_tipopessoa2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV79WWPessoaDS_10_Pessoa_tipopessoa2)";
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWPessoaDS_16_Pessoa_tipopessoa3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV85WWPessoaDS_16_Pessoa_tipopessoa3)";
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int9[13] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int9[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV87WWPessoaDS_18_Tfpessoa_nome)";
         }
         else
         {
            GXv_int9[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] = @AV88WWPessoaDS_19_Tfpessoa_nome_sel)";
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels, "T1.[Pessoa_TipoPessoa] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV90WWPessoaDS_21_Tfpessoa_docto)";
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] = @AV91WWPessoaDS_22_Tfpessoa_docto_sel)";
         }
         else
         {
            GXv_int9[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] like @lV92WWPessoaDS_23_Tfpessoa_ie)";
         }
         else
         {
            GXv_int9[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] = @AV93WWPessoaDS_24_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int9[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] like @lV94WWPessoaDS_25_Tfpessoa_endereco)";
         }
         else
         {
            GXv_int9[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] = @AV95WWPessoaDS_26_Tfpessoa_endereco_sel)";
         }
         else
         {
            GXv_int9[22] = 1;
         }
         if ( ! (0==AV96WWPessoaDS_27_Tfpessoa_municipiocod) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] >= @AV96WWPessoaDS_27_Tfpessoa_municipiocod)";
         }
         else
         {
            GXv_int9[23] = 1;
         }
         if ( ! (0==AV97WWPessoaDS_28_Tfpessoa_municipiocod_to) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] <= @AV97WWPessoaDS_28_Tfpessoa_municipiocod_to)";
         }
         else
         {
            GXv_int9[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] like @lV98WWPessoaDS_29_Tfpessoa_uf)";
         }
         else
         {
            GXv_int9[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] = @AV99WWPessoaDS_30_Tfpessoa_uf_sel)";
         }
         else
         {
            GXv_int9[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] like @lV100WWPessoaDS_31_Tfpessoa_cep)";
         }
         else
         {
            GXv_int9[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] = @AV101WWPessoaDS_32_Tfpessoa_cep_sel)";
         }
         else
         {
            GXv_int9[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] like @lV102WWPessoaDS_33_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int9[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] = @AV103WWPessoaDS_34_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int9[30] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] like @lV104WWPessoaDS_35_Tfpessoa_fax)";
         }
         else
         {
            GXv_int9[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] = @AV105WWPessoaDS_36_Tfpessoa_fax_sel)";
         }
         else
         {
            GXv_int9[32] = 1;
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 1)";
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 0)";
         }
         if ( AV107Usuario_pessoacod > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Codigo] = @AV107Usuario_pessoacod)";
         }
         else
         {
            GXv_int9[33] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Estado_UF]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      protected Object[] conditional_P00F27( IGxContext context ,
                                             String A36Pessoa_TipoPessoa ,
                                             IGxCollection AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                             String AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                             short AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWPessoaDS_3_Pessoa_nome1 ,
                                             String AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                             String AV74WWPessoaDS_5_Pessoa_docto1 ,
                                             bool AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                             String AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                             short AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                             String AV78WWPessoaDS_9_Pessoa_nome2 ,
                                             String AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                             String AV80WWPessoaDS_11_Pessoa_docto2 ,
                                             bool AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                             String AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                             short AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                             String AV84WWPessoaDS_15_Pessoa_nome3 ,
                                             String AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                             String AV86WWPessoaDS_17_Pessoa_docto3 ,
                                             String AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                             String AV87WWPessoaDS_18_Tfpessoa_nome ,
                                             int AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count ,
                                             String AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                             String AV90WWPessoaDS_21_Tfpessoa_docto ,
                                             String AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                             String AV92WWPessoaDS_23_Tfpessoa_ie ,
                                             String AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                             String AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                             int AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                             int AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                             String AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                             String AV98WWPessoaDS_29_Tfpessoa_uf ,
                                             String AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                             String AV100WWPessoaDS_31_Tfpessoa_cep ,
                                             String AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                             String AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                             String AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                             String AV104WWPessoaDS_35_Tfpessoa_fax ,
                                             short AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                             int AV107Usuario_pessoacod ,
                                             String A35Pessoa_Nome ,
                                             String A37Pessoa_Docto ,
                                             String A518Pessoa_IE ,
                                             String A519Pessoa_Endereco ,
                                             int A503Pessoa_MunicipioCod ,
                                             String A520Pessoa_UF ,
                                             String A521Pessoa_CEP ,
                                             String A522Pessoa_Telefone ,
                                             String A523Pessoa_Fax ,
                                             bool A38Pessoa_Ativo ,
                                             int A34Pessoa_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int11 ;
         GXv_int11 = new short [34] ;
         Object[] GXv_Object12 ;
         GXv_Object12 = new Object [2] ;
         scmdbuf = "SELECT T1.[Pessoa_Ativo], T1.[Pessoa_CEP], T1.[Pessoa_Codigo], T1.[Pessoa_Fax], T1.[Pessoa_Telefone], T2.[Estado_UF] AS Pessoa_UF, T1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Pessoa_Endereco], T1.[Pessoa_IE], T1.[Pessoa_Docto], T1.[Pessoa_TipoPessoa], T1.[Pessoa_Nome] FROM ([Pessoa] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Pessoa_MunicipioCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Pessoa_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int11[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int11[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWPessoaDS_4_Pessoa_tipopessoa1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV73WWPessoaDS_4_Pessoa_tipopessoa1)";
         }
         else
         {
            GXv_int11[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int11[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int11[4] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int11[5] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int11[6] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWPessoaDS_10_Pessoa_tipopessoa2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV79WWPessoaDS_10_Pessoa_tipopessoa2)";
         }
         else
         {
            GXv_int11[7] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int11[8] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int11[9] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int11[10] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int11[11] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWPessoaDS_16_Pessoa_tipopessoa3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV85WWPessoaDS_16_Pessoa_tipopessoa3)";
         }
         else
         {
            GXv_int11[12] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int11[13] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int11[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV87WWPessoaDS_18_Tfpessoa_nome)";
         }
         else
         {
            GXv_int11[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] = @AV88WWPessoaDS_19_Tfpessoa_nome_sel)";
         }
         else
         {
            GXv_int11[16] = 1;
         }
         if ( AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels, "T1.[Pessoa_TipoPessoa] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV90WWPessoaDS_21_Tfpessoa_docto)";
         }
         else
         {
            GXv_int11[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] = @AV91WWPessoaDS_22_Tfpessoa_docto_sel)";
         }
         else
         {
            GXv_int11[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] like @lV92WWPessoaDS_23_Tfpessoa_ie)";
         }
         else
         {
            GXv_int11[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] = @AV93WWPessoaDS_24_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int11[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] like @lV94WWPessoaDS_25_Tfpessoa_endereco)";
         }
         else
         {
            GXv_int11[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] = @AV95WWPessoaDS_26_Tfpessoa_endereco_sel)";
         }
         else
         {
            GXv_int11[22] = 1;
         }
         if ( ! (0==AV96WWPessoaDS_27_Tfpessoa_municipiocod) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] >= @AV96WWPessoaDS_27_Tfpessoa_municipiocod)";
         }
         else
         {
            GXv_int11[23] = 1;
         }
         if ( ! (0==AV97WWPessoaDS_28_Tfpessoa_municipiocod_to) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] <= @AV97WWPessoaDS_28_Tfpessoa_municipiocod_to)";
         }
         else
         {
            GXv_int11[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] like @lV98WWPessoaDS_29_Tfpessoa_uf)";
         }
         else
         {
            GXv_int11[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] = @AV99WWPessoaDS_30_Tfpessoa_uf_sel)";
         }
         else
         {
            GXv_int11[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] like @lV100WWPessoaDS_31_Tfpessoa_cep)";
         }
         else
         {
            GXv_int11[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] = @AV101WWPessoaDS_32_Tfpessoa_cep_sel)";
         }
         else
         {
            GXv_int11[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] like @lV102WWPessoaDS_33_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int11[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] = @AV103WWPessoaDS_34_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int11[30] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] like @lV104WWPessoaDS_35_Tfpessoa_fax)";
         }
         else
         {
            GXv_int11[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] = @AV105WWPessoaDS_36_Tfpessoa_fax_sel)";
         }
         else
         {
            GXv_int11[32] = 1;
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 1)";
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 0)";
         }
         if ( AV107Usuario_pessoacod > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Codigo] = @AV107Usuario_pessoacod)";
         }
         else
         {
            GXv_int11[33] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Pessoa_CEP]";
         GXv_Object12[0] = scmdbuf;
         GXv_Object12[1] = GXv_int11;
         return GXv_Object12 ;
      }

      protected Object[] conditional_P00F28( IGxContext context ,
                                             String A36Pessoa_TipoPessoa ,
                                             IGxCollection AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                             String AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                             short AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWPessoaDS_3_Pessoa_nome1 ,
                                             String AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                             String AV74WWPessoaDS_5_Pessoa_docto1 ,
                                             bool AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                             String AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                             short AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                             String AV78WWPessoaDS_9_Pessoa_nome2 ,
                                             String AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                             String AV80WWPessoaDS_11_Pessoa_docto2 ,
                                             bool AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                             String AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                             short AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                             String AV84WWPessoaDS_15_Pessoa_nome3 ,
                                             String AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                             String AV86WWPessoaDS_17_Pessoa_docto3 ,
                                             String AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                             String AV87WWPessoaDS_18_Tfpessoa_nome ,
                                             int AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count ,
                                             String AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                             String AV90WWPessoaDS_21_Tfpessoa_docto ,
                                             String AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                             String AV92WWPessoaDS_23_Tfpessoa_ie ,
                                             String AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                             String AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                             int AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                             int AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                             String AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                             String AV98WWPessoaDS_29_Tfpessoa_uf ,
                                             String AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                             String AV100WWPessoaDS_31_Tfpessoa_cep ,
                                             String AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                             String AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                             String AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                             String AV104WWPessoaDS_35_Tfpessoa_fax ,
                                             short AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                             int AV107Usuario_pessoacod ,
                                             String A35Pessoa_Nome ,
                                             String A37Pessoa_Docto ,
                                             String A518Pessoa_IE ,
                                             String A519Pessoa_Endereco ,
                                             int A503Pessoa_MunicipioCod ,
                                             String A520Pessoa_UF ,
                                             String A521Pessoa_CEP ,
                                             String A522Pessoa_Telefone ,
                                             String A523Pessoa_Fax ,
                                             bool A38Pessoa_Ativo ,
                                             int A34Pessoa_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int13 ;
         GXv_int13 = new short [34] ;
         Object[] GXv_Object14 ;
         GXv_Object14 = new Object [2] ;
         scmdbuf = "SELECT T1.[Pessoa_Ativo], T1.[Pessoa_Telefone], T1.[Pessoa_Codigo], T1.[Pessoa_Fax], T1.[Pessoa_CEP], T2.[Estado_UF] AS Pessoa_UF, T1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Pessoa_Endereco], T1.[Pessoa_IE], T1.[Pessoa_Docto], T1.[Pessoa_TipoPessoa], T1.[Pessoa_Nome] FROM ([Pessoa] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Pessoa_MunicipioCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Pessoa_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int13[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int13[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWPessoaDS_4_Pessoa_tipopessoa1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV73WWPessoaDS_4_Pessoa_tipopessoa1)";
         }
         else
         {
            GXv_int13[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int13[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int13[4] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int13[5] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int13[6] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWPessoaDS_10_Pessoa_tipopessoa2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV79WWPessoaDS_10_Pessoa_tipopessoa2)";
         }
         else
         {
            GXv_int13[7] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int13[8] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int13[9] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int13[10] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int13[11] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWPessoaDS_16_Pessoa_tipopessoa3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV85WWPessoaDS_16_Pessoa_tipopessoa3)";
         }
         else
         {
            GXv_int13[12] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int13[13] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int13[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV87WWPessoaDS_18_Tfpessoa_nome)";
         }
         else
         {
            GXv_int13[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] = @AV88WWPessoaDS_19_Tfpessoa_nome_sel)";
         }
         else
         {
            GXv_int13[16] = 1;
         }
         if ( AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels, "T1.[Pessoa_TipoPessoa] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV90WWPessoaDS_21_Tfpessoa_docto)";
         }
         else
         {
            GXv_int13[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] = @AV91WWPessoaDS_22_Tfpessoa_docto_sel)";
         }
         else
         {
            GXv_int13[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] like @lV92WWPessoaDS_23_Tfpessoa_ie)";
         }
         else
         {
            GXv_int13[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] = @AV93WWPessoaDS_24_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int13[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] like @lV94WWPessoaDS_25_Tfpessoa_endereco)";
         }
         else
         {
            GXv_int13[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] = @AV95WWPessoaDS_26_Tfpessoa_endereco_sel)";
         }
         else
         {
            GXv_int13[22] = 1;
         }
         if ( ! (0==AV96WWPessoaDS_27_Tfpessoa_municipiocod) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] >= @AV96WWPessoaDS_27_Tfpessoa_municipiocod)";
         }
         else
         {
            GXv_int13[23] = 1;
         }
         if ( ! (0==AV97WWPessoaDS_28_Tfpessoa_municipiocod_to) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] <= @AV97WWPessoaDS_28_Tfpessoa_municipiocod_to)";
         }
         else
         {
            GXv_int13[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] like @lV98WWPessoaDS_29_Tfpessoa_uf)";
         }
         else
         {
            GXv_int13[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] = @AV99WWPessoaDS_30_Tfpessoa_uf_sel)";
         }
         else
         {
            GXv_int13[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] like @lV100WWPessoaDS_31_Tfpessoa_cep)";
         }
         else
         {
            GXv_int13[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] = @AV101WWPessoaDS_32_Tfpessoa_cep_sel)";
         }
         else
         {
            GXv_int13[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] like @lV102WWPessoaDS_33_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int13[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] = @AV103WWPessoaDS_34_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int13[30] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] like @lV104WWPessoaDS_35_Tfpessoa_fax)";
         }
         else
         {
            GXv_int13[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] = @AV105WWPessoaDS_36_Tfpessoa_fax_sel)";
         }
         else
         {
            GXv_int13[32] = 1;
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 1)";
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 0)";
         }
         if ( AV107Usuario_pessoacod > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Codigo] = @AV107Usuario_pessoacod)";
         }
         else
         {
            GXv_int13[33] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Pessoa_Telefone]";
         GXv_Object14[0] = scmdbuf;
         GXv_Object14[1] = GXv_int13;
         return GXv_Object14 ;
      }

      protected Object[] conditional_P00F29( IGxContext context ,
                                             String A36Pessoa_TipoPessoa ,
                                             IGxCollection AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels ,
                                             String AV70WWPessoaDS_1_Dynamicfiltersselector1 ,
                                             short AV71WWPessoaDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWPessoaDS_3_Pessoa_nome1 ,
                                             String AV73WWPessoaDS_4_Pessoa_tipopessoa1 ,
                                             String AV74WWPessoaDS_5_Pessoa_docto1 ,
                                             bool AV75WWPessoaDS_6_Dynamicfiltersenabled2 ,
                                             String AV76WWPessoaDS_7_Dynamicfiltersselector2 ,
                                             short AV77WWPessoaDS_8_Dynamicfiltersoperator2 ,
                                             String AV78WWPessoaDS_9_Pessoa_nome2 ,
                                             String AV79WWPessoaDS_10_Pessoa_tipopessoa2 ,
                                             String AV80WWPessoaDS_11_Pessoa_docto2 ,
                                             bool AV81WWPessoaDS_12_Dynamicfiltersenabled3 ,
                                             String AV82WWPessoaDS_13_Dynamicfiltersselector3 ,
                                             short AV83WWPessoaDS_14_Dynamicfiltersoperator3 ,
                                             String AV84WWPessoaDS_15_Pessoa_nome3 ,
                                             String AV85WWPessoaDS_16_Pessoa_tipopessoa3 ,
                                             String AV86WWPessoaDS_17_Pessoa_docto3 ,
                                             String AV88WWPessoaDS_19_Tfpessoa_nome_sel ,
                                             String AV87WWPessoaDS_18_Tfpessoa_nome ,
                                             int AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count ,
                                             String AV91WWPessoaDS_22_Tfpessoa_docto_sel ,
                                             String AV90WWPessoaDS_21_Tfpessoa_docto ,
                                             String AV93WWPessoaDS_24_Tfpessoa_ie_sel ,
                                             String AV92WWPessoaDS_23_Tfpessoa_ie ,
                                             String AV95WWPessoaDS_26_Tfpessoa_endereco_sel ,
                                             String AV94WWPessoaDS_25_Tfpessoa_endereco ,
                                             int AV96WWPessoaDS_27_Tfpessoa_municipiocod ,
                                             int AV97WWPessoaDS_28_Tfpessoa_municipiocod_to ,
                                             String AV99WWPessoaDS_30_Tfpessoa_uf_sel ,
                                             String AV98WWPessoaDS_29_Tfpessoa_uf ,
                                             String AV101WWPessoaDS_32_Tfpessoa_cep_sel ,
                                             String AV100WWPessoaDS_31_Tfpessoa_cep ,
                                             String AV103WWPessoaDS_34_Tfpessoa_telefone_sel ,
                                             String AV102WWPessoaDS_33_Tfpessoa_telefone ,
                                             String AV105WWPessoaDS_36_Tfpessoa_fax_sel ,
                                             String AV104WWPessoaDS_35_Tfpessoa_fax ,
                                             short AV106WWPessoaDS_37_Tfpessoa_ativo_sel ,
                                             int AV107Usuario_pessoacod ,
                                             String A35Pessoa_Nome ,
                                             String A37Pessoa_Docto ,
                                             String A518Pessoa_IE ,
                                             String A519Pessoa_Endereco ,
                                             int A503Pessoa_MunicipioCod ,
                                             String A520Pessoa_UF ,
                                             String A521Pessoa_CEP ,
                                             String A522Pessoa_Telefone ,
                                             String A523Pessoa_Fax ,
                                             bool A38Pessoa_Ativo ,
                                             int A34Pessoa_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int15 ;
         GXv_int15 = new short [34] ;
         Object[] GXv_Object16 ;
         GXv_Object16 = new Object [2] ;
         scmdbuf = "SELECT T1.[Pessoa_Ativo], T1.[Pessoa_Fax], T1.[Pessoa_Codigo], T1.[Pessoa_Telefone], T1.[Pessoa_CEP], T2.[Estado_UF] AS Pessoa_UF, T1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Pessoa_Endereco], T1.[Pessoa_IE], T1.[Pessoa_Docto], T1.[Pessoa_TipoPessoa], T1.[Pessoa_Nome] FROM ([Pessoa] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Pessoa_MunicipioCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Pessoa_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int15[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_NOME") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWPessoaDS_3_Pessoa_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV72WWPessoaDS_3_Pessoa_nome1)";
         }
         else
         {
            GXv_int15[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWPessoaDS_4_Pessoa_tipopessoa1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV73WWPessoaDS_4_Pessoa_tipopessoa1)";
         }
         else
         {
            GXv_int15[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int15[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWPessoaDS_1_Dynamicfiltersselector1, "PESSOA_DOCTO") == 0 ) && ( AV71WWPessoaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWPessoaDS_5_Pessoa_docto1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV74WWPessoaDS_5_Pessoa_docto1)";
         }
         else
         {
            GXv_int15[4] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int15[5] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_NOME") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWPessoaDS_9_Pessoa_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV78WWPessoaDS_9_Pessoa_nome2)";
         }
         else
         {
            GXv_int15[6] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWPessoaDS_10_Pessoa_tipopessoa2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV79WWPessoaDS_10_Pessoa_tipopessoa2)";
         }
         else
         {
            GXv_int15[7] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int15[8] = 1;
         }
         if ( AV75WWPessoaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWPessoaDS_7_Dynamicfiltersselector2, "PESSOA_DOCTO") == 0 ) && ( AV77WWPessoaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWPessoaDS_11_Pessoa_docto2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV80WWPessoaDS_11_Pessoa_docto2)";
         }
         else
         {
            GXv_int15[9] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int15[10] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_NOME") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWPessoaDS_15_Pessoa_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like '%' + @lV84WWPessoaDS_15_Pessoa_nome3)";
         }
         else
         {
            GXv_int15[11] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_TIPOPESSOA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWPessoaDS_16_Pessoa_tipopessoa3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_TipoPessoa] = @AV85WWPessoaDS_16_Pessoa_tipopessoa3)";
         }
         else
         {
            GXv_int15[12] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int15[13] = 1;
         }
         if ( AV81WWPessoaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWPessoaDS_13_Dynamicfiltersselector3, "PESSOA_DOCTO") == 0 ) && ( AV83WWPessoaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPessoaDS_17_Pessoa_docto3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like '%' + @lV86WWPessoaDS_17_Pessoa_docto3)";
         }
         else
         {
            GXv_int15[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWPessoaDS_18_Tfpessoa_nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] like @lV87WWPessoaDS_18_Tfpessoa_nome)";
         }
         else
         {
            GXv_int15[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPessoaDS_19_Tfpessoa_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Nome] = @AV88WWPessoaDS_19_Tfpessoa_nome_sel)";
         }
         else
         {
            GXv_int15[16] = 1;
         }
         if ( AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89WWPessoaDS_20_Tfpessoa_tipopessoa_sels, "T1.[Pessoa_TipoPessoa] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWPessoaDS_21_Tfpessoa_docto)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] like @lV90WWPessoaDS_21_Tfpessoa_docto)";
         }
         else
         {
            GXv_int15[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWPessoaDS_22_Tfpessoa_docto_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Docto] = @AV91WWPessoaDS_22_Tfpessoa_docto_sel)";
         }
         else
         {
            GXv_int15[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWPessoaDS_23_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] like @lV92WWPessoaDS_23_Tfpessoa_ie)";
         }
         else
         {
            GXv_int15[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWPessoaDS_24_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_IE] = @AV93WWPessoaDS_24_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int15[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWPessoaDS_25_Tfpessoa_endereco)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] like @lV94WWPessoaDS_25_Tfpessoa_endereco)";
         }
         else
         {
            GXv_int15[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWPessoaDS_26_Tfpessoa_endereco_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Endereco] = @AV95WWPessoaDS_26_Tfpessoa_endereco_sel)";
         }
         else
         {
            GXv_int15[22] = 1;
         }
         if ( ! (0==AV96WWPessoaDS_27_Tfpessoa_municipiocod) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] >= @AV96WWPessoaDS_27_Tfpessoa_municipiocod)";
         }
         else
         {
            GXv_int15[23] = 1;
         }
         if ( ! (0==AV97WWPessoaDS_28_Tfpessoa_municipiocod_to) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_MunicipioCod] <= @AV97WWPessoaDS_28_Tfpessoa_municipiocod_to)";
         }
         else
         {
            GXv_int15[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWPessoaDS_29_Tfpessoa_uf)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] like @lV98WWPessoaDS_29_Tfpessoa_uf)";
         }
         else
         {
            GXv_int15[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaDS_30_Tfpessoa_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Estado_UF] = @AV99WWPessoaDS_30_Tfpessoa_uf_sel)";
         }
         else
         {
            GXv_int15[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWPessoaDS_31_Tfpessoa_cep)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] like @lV100WWPessoaDS_31_Tfpessoa_cep)";
         }
         else
         {
            GXv_int15[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWPessoaDS_32_Tfpessoa_cep_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_CEP] = @AV101WWPessoaDS_32_Tfpessoa_cep_sel)";
         }
         else
         {
            GXv_int15[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWPessoaDS_33_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] like @lV102WWPessoaDS_33_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int15[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWPessoaDS_34_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Telefone] = @AV103WWPessoaDS_34_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int15[30] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWPessoaDS_35_Tfpessoa_fax)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] like @lV104WWPessoaDS_35_Tfpessoa_fax)";
         }
         else
         {
            GXv_int15[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWPessoaDS_36_Tfpessoa_fax_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Fax] = @AV105WWPessoaDS_36_Tfpessoa_fax_sel)";
         }
         else
         {
            GXv_int15[32] = 1;
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 1)";
         }
         if ( AV106WWPessoaDS_37_Tfpessoa_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Ativo] = 0)";
         }
         if ( AV107Usuario_pessoacod > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Pessoa_Codigo] = @AV107Usuario_pessoacod)";
         }
         else
         {
            GXv_int15[33] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Pessoa_Fax]";
         GXv_Object16[0] = scmdbuf;
         GXv_Object16[1] = GXv_int15;
         return GXv_Object16 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00F22(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (int)dynConstraints[50] );
               case 1 :
                     return conditional_P00F23(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (int)dynConstraints[50] );
               case 2 :
                     return conditional_P00F24(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (int)dynConstraints[50] );
               case 3 :
                     return conditional_P00F25(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (int)dynConstraints[50] );
               case 4 :
                     return conditional_P00F26(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (int)dynConstraints[50] );
               case 5 :
                     return conditional_P00F27(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (int)dynConstraints[50] );
               case 6 :
                     return conditional_P00F28(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (int)dynConstraints[50] );
               case 7 :
                     return conditional_P00F29(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (bool)dynConstraints[49] , (int)dynConstraints[50] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00F22 ;
          prmP00F22 = new Object[] {
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV73WWPessoaDS_4_Pessoa_tipopessoa1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV79WWPessoaDS_10_Pessoa_tipopessoa2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV85WWPessoaDS_16_Pessoa_tipopessoa3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV87WWPessoaDS_18_Tfpessoa_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV88WWPessoaDS_19_Tfpessoa_nome_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV90WWPessoaDS_21_Tfpessoa_docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV91WWPessoaDS_22_Tfpessoa_docto_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV92WWPessoaDS_23_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV93WWPessoaDS_24_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV94WWPessoaDS_25_Tfpessoa_endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV95WWPessoaDS_26_Tfpessoa_endereco_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV96WWPessoaDS_27_Tfpessoa_municipiocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV97WWPessoaDS_28_Tfpessoa_municipiocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV98WWPessoaDS_29_Tfpessoa_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV99WWPessoaDS_30_Tfpessoa_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV100WWPessoaDS_31_Tfpessoa_cep",SqlDbType.Char,10,0} ,
          new Object[] {"@AV101WWPessoaDS_32_Tfpessoa_cep_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWPessoaDS_33_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV103WWPessoaDS_34_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV104WWPessoaDS_35_Tfpessoa_fax",SqlDbType.Char,15,0} ,
          new Object[] {"@AV105WWPessoaDS_36_Tfpessoa_fax_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV107Usuario_pessoacod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00F23 ;
          prmP00F23 = new Object[] {
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV73WWPessoaDS_4_Pessoa_tipopessoa1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV79WWPessoaDS_10_Pessoa_tipopessoa2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV85WWPessoaDS_16_Pessoa_tipopessoa3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV87WWPessoaDS_18_Tfpessoa_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV88WWPessoaDS_19_Tfpessoa_nome_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV90WWPessoaDS_21_Tfpessoa_docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV91WWPessoaDS_22_Tfpessoa_docto_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV92WWPessoaDS_23_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV93WWPessoaDS_24_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV94WWPessoaDS_25_Tfpessoa_endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV95WWPessoaDS_26_Tfpessoa_endereco_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV96WWPessoaDS_27_Tfpessoa_municipiocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV97WWPessoaDS_28_Tfpessoa_municipiocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV98WWPessoaDS_29_Tfpessoa_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV99WWPessoaDS_30_Tfpessoa_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV100WWPessoaDS_31_Tfpessoa_cep",SqlDbType.Char,10,0} ,
          new Object[] {"@AV101WWPessoaDS_32_Tfpessoa_cep_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWPessoaDS_33_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV103WWPessoaDS_34_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV104WWPessoaDS_35_Tfpessoa_fax",SqlDbType.Char,15,0} ,
          new Object[] {"@AV105WWPessoaDS_36_Tfpessoa_fax_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV107Usuario_pessoacod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00F24 ;
          prmP00F24 = new Object[] {
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV73WWPessoaDS_4_Pessoa_tipopessoa1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV79WWPessoaDS_10_Pessoa_tipopessoa2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV85WWPessoaDS_16_Pessoa_tipopessoa3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV87WWPessoaDS_18_Tfpessoa_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV88WWPessoaDS_19_Tfpessoa_nome_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV90WWPessoaDS_21_Tfpessoa_docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV91WWPessoaDS_22_Tfpessoa_docto_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV92WWPessoaDS_23_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV93WWPessoaDS_24_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV94WWPessoaDS_25_Tfpessoa_endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV95WWPessoaDS_26_Tfpessoa_endereco_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV96WWPessoaDS_27_Tfpessoa_municipiocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV97WWPessoaDS_28_Tfpessoa_municipiocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV98WWPessoaDS_29_Tfpessoa_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV99WWPessoaDS_30_Tfpessoa_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV100WWPessoaDS_31_Tfpessoa_cep",SqlDbType.Char,10,0} ,
          new Object[] {"@AV101WWPessoaDS_32_Tfpessoa_cep_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWPessoaDS_33_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV103WWPessoaDS_34_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV104WWPessoaDS_35_Tfpessoa_fax",SqlDbType.Char,15,0} ,
          new Object[] {"@AV105WWPessoaDS_36_Tfpessoa_fax_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV107Usuario_pessoacod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00F25 ;
          prmP00F25 = new Object[] {
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV73WWPessoaDS_4_Pessoa_tipopessoa1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV79WWPessoaDS_10_Pessoa_tipopessoa2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV85WWPessoaDS_16_Pessoa_tipopessoa3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV87WWPessoaDS_18_Tfpessoa_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV88WWPessoaDS_19_Tfpessoa_nome_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV90WWPessoaDS_21_Tfpessoa_docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV91WWPessoaDS_22_Tfpessoa_docto_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV92WWPessoaDS_23_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV93WWPessoaDS_24_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV94WWPessoaDS_25_Tfpessoa_endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV95WWPessoaDS_26_Tfpessoa_endereco_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV96WWPessoaDS_27_Tfpessoa_municipiocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV97WWPessoaDS_28_Tfpessoa_municipiocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV98WWPessoaDS_29_Tfpessoa_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV99WWPessoaDS_30_Tfpessoa_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV100WWPessoaDS_31_Tfpessoa_cep",SqlDbType.Char,10,0} ,
          new Object[] {"@AV101WWPessoaDS_32_Tfpessoa_cep_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWPessoaDS_33_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV103WWPessoaDS_34_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV104WWPessoaDS_35_Tfpessoa_fax",SqlDbType.Char,15,0} ,
          new Object[] {"@AV105WWPessoaDS_36_Tfpessoa_fax_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV107Usuario_pessoacod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00F26 ;
          prmP00F26 = new Object[] {
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV73WWPessoaDS_4_Pessoa_tipopessoa1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV79WWPessoaDS_10_Pessoa_tipopessoa2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV85WWPessoaDS_16_Pessoa_tipopessoa3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV87WWPessoaDS_18_Tfpessoa_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV88WWPessoaDS_19_Tfpessoa_nome_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV90WWPessoaDS_21_Tfpessoa_docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV91WWPessoaDS_22_Tfpessoa_docto_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV92WWPessoaDS_23_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV93WWPessoaDS_24_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV94WWPessoaDS_25_Tfpessoa_endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV95WWPessoaDS_26_Tfpessoa_endereco_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV96WWPessoaDS_27_Tfpessoa_municipiocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV97WWPessoaDS_28_Tfpessoa_municipiocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV98WWPessoaDS_29_Tfpessoa_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV99WWPessoaDS_30_Tfpessoa_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV100WWPessoaDS_31_Tfpessoa_cep",SqlDbType.Char,10,0} ,
          new Object[] {"@AV101WWPessoaDS_32_Tfpessoa_cep_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWPessoaDS_33_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV103WWPessoaDS_34_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV104WWPessoaDS_35_Tfpessoa_fax",SqlDbType.Char,15,0} ,
          new Object[] {"@AV105WWPessoaDS_36_Tfpessoa_fax_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV107Usuario_pessoacod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00F27 ;
          prmP00F27 = new Object[] {
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV73WWPessoaDS_4_Pessoa_tipopessoa1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV79WWPessoaDS_10_Pessoa_tipopessoa2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV85WWPessoaDS_16_Pessoa_tipopessoa3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV87WWPessoaDS_18_Tfpessoa_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV88WWPessoaDS_19_Tfpessoa_nome_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV90WWPessoaDS_21_Tfpessoa_docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV91WWPessoaDS_22_Tfpessoa_docto_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV92WWPessoaDS_23_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV93WWPessoaDS_24_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV94WWPessoaDS_25_Tfpessoa_endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV95WWPessoaDS_26_Tfpessoa_endereco_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV96WWPessoaDS_27_Tfpessoa_municipiocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV97WWPessoaDS_28_Tfpessoa_municipiocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV98WWPessoaDS_29_Tfpessoa_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV99WWPessoaDS_30_Tfpessoa_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV100WWPessoaDS_31_Tfpessoa_cep",SqlDbType.Char,10,0} ,
          new Object[] {"@AV101WWPessoaDS_32_Tfpessoa_cep_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWPessoaDS_33_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV103WWPessoaDS_34_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV104WWPessoaDS_35_Tfpessoa_fax",SqlDbType.Char,15,0} ,
          new Object[] {"@AV105WWPessoaDS_36_Tfpessoa_fax_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV107Usuario_pessoacod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00F28 ;
          prmP00F28 = new Object[] {
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV73WWPessoaDS_4_Pessoa_tipopessoa1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV79WWPessoaDS_10_Pessoa_tipopessoa2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV85WWPessoaDS_16_Pessoa_tipopessoa3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV87WWPessoaDS_18_Tfpessoa_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV88WWPessoaDS_19_Tfpessoa_nome_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV90WWPessoaDS_21_Tfpessoa_docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV91WWPessoaDS_22_Tfpessoa_docto_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV92WWPessoaDS_23_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV93WWPessoaDS_24_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV94WWPessoaDS_25_Tfpessoa_endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV95WWPessoaDS_26_Tfpessoa_endereco_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV96WWPessoaDS_27_Tfpessoa_municipiocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV97WWPessoaDS_28_Tfpessoa_municipiocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV98WWPessoaDS_29_Tfpessoa_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV99WWPessoaDS_30_Tfpessoa_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV100WWPessoaDS_31_Tfpessoa_cep",SqlDbType.Char,10,0} ,
          new Object[] {"@AV101WWPessoaDS_32_Tfpessoa_cep_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWPessoaDS_33_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV103WWPessoaDS_34_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV104WWPessoaDS_35_Tfpessoa_fax",SqlDbType.Char,15,0} ,
          new Object[] {"@AV105WWPessoaDS_36_Tfpessoa_fax_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV107Usuario_pessoacod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00F29 ;
          prmP00F29 = new Object[] {
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV72WWPessoaDS_3_Pessoa_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV73WWPessoaDS_4_Pessoa_tipopessoa1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV74WWPessoaDS_5_Pessoa_docto1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78WWPessoaDS_9_Pessoa_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV79WWPessoaDS_10_Pessoa_tipopessoa2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV80WWPessoaDS_11_Pessoa_docto2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV84WWPessoaDS_15_Pessoa_nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV85WWPessoaDS_16_Pessoa_tipopessoa3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV86WWPessoaDS_17_Pessoa_docto3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV87WWPessoaDS_18_Tfpessoa_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV88WWPessoaDS_19_Tfpessoa_nome_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV90WWPessoaDS_21_Tfpessoa_docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV91WWPessoaDS_22_Tfpessoa_docto_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV92WWPessoaDS_23_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV93WWPessoaDS_24_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV94WWPessoaDS_25_Tfpessoa_endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV95WWPessoaDS_26_Tfpessoa_endereco_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV96WWPessoaDS_27_Tfpessoa_municipiocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV97WWPessoaDS_28_Tfpessoa_municipiocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV98WWPessoaDS_29_Tfpessoa_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV99WWPessoaDS_30_Tfpessoa_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV100WWPessoaDS_31_Tfpessoa_cep",SqlDbType.Char,10,0} ,
          new Object[] {"@AV101WWPessoaDS_32_Tfpessoa_cep_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWPessoaDS_33_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV103WWPessoaDS_34_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV104WWPessoaDS_35_Tfpessoa_fax",SqlDbType.Char,15,0} ,
          new Object[] {"@AV105WWPessoaDS_36_Tfpessoa_fax_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV107Usuario_pessoacod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00F22", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00F22,100,0,true,false )
             ,new CursorDef("P00F23", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00F23,100,0,true,false )
             ,new CursorDef("P00F24", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00F24,100,0,true,false )
             ,new CursorDef("P00F25", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00F25,100,0,true,false )
             ,new CursorDef("P00F26", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00F26,100,0,true,false )
             ,new CursorDef("P00F27", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00F27,100,0,true,false )
             ,new CursorDef("P00F28", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00F28,100,0,true,false )
             ,new CursorDef("P00F29", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00F29,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 2) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getVarchar(11) ;
                ((String[]) buf[18])[0] = rslt.getString(12, 1) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 2) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((String[]) buf[18])[0] = rslt.getString(12, 100) ;
                return;
             case 2 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 2) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((String[]) buf[18])[0] = rslt.getString(12, 100) ;
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 2) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((String[]) buf[18])[0] = rslt.getString(12, 100) ;
                return;
             case 4 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((String[]) buf[18])[0] = rslt.getString(12, 100) ;
                return;
             case 5 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 2) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((String[]) buf[18])[0] = rslt.getString(12, 100) ;
                return;
             case 6 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 2) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((String[]) buf[18])[0] = rslt.getString(12, 100) ;
                return;
             case 7 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 2) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((String[]) buf[18])[0] = rslt.getString(12, 100) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                return;
             case 7 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwpessoafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwpessoafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwpessoafilterdata") )
          {
             return  ;
          }
          getwwpessoafilterdata worker = new getwwpessoafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
