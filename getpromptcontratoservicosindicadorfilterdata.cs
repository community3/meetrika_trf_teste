/*
               File: GetPromptContratoServicosIndicadorFilterData
        Description: Get Prompt Contrato Servicos Indicador Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:5:3.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratoservicosindicadorfilterdata : GXProcedure
   {
      public getpromptcontratoservicosindicadorfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratoservicosindicadorfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV38DDOName = aP0_DDOName;
         this.AV36SearchTxt = aP1_SearchTxt;
         this.AV37SearchTxtTo = aP2_SearchTxtTo;
         this.AV42OptionsJson = "" ;
         this.AV45OptionsDescJson = "" ;
         this.AV47OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV42OptionsJson;
         aP4_OptionsDescJson=this.AV45OptionsDescJson;
         aP5_OptionIndexesJson=this.AV47OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV38DDOName = aP0_DDOName;
         this.AV36SearchTxt = aP1_SearchTxt;
         this.AV37SearchTxtTo = aP2_SearchTxtTo;
         this.AV42OptionsJson = "" ;
         this.AV45OptionsDescJson = "" ;
         this.AV47OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV42OptionsJson;
         aP4_OptionsDescJson=this.AV45OptionsDescJson;
         aP5_OptionIndexesJson=this.AV47OptionIndexesJson;
         return AV47OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratoservicosindicadorfilterdata objgetpromptcontratoservicosindicadorfilterdata;
         objgetpromptcontratoservicosindicadorfilterdata = new getpromptcontratoservicosindicadorfilterdata();
         objgetpromptcontratoservicosindicadorfilterdata.AV38DDOName = aP0_DDOName;
         objgetpromptcontratoservicosindicadorfilterdata.AV36SearchTxt = aP1_SearchTxt;
         objgetpromptcontratoservicosindicadorfilterdata.AV37SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratoservicosindicadorfilterdata.AV42OptionsJson = "" ;
         objgetpromptcontratoservicosindicadorfilterdata.AV45OptionsDescJson = "" ;
         objgetpromptcontratoservicosindicadorfilterdata.AV47OptionIndexesJson = "" ;
         objgetpromptcontratoservicosindicadorfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratoservicosindicadorfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratoservicosindicadorfilterdata);
         aP3_OptionsJson=this.AV42OptionsJson;
         aP4_OptionsDescJson=this.AV45OptionsDescJson;
         aP5_OptionIndexesJson=this.AV47OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratoservicosindicadorfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV41Options = (IGxCollection)(new GxSimpleCollection());
         AV44OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV46OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV38DDOName), "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSINDICADOR_INDICADOROPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV38DDOName), "DDO_CONTRATOSERVICOSINDICADOR_FINALIDADE") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSINDICADOR_FINALIDADEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV38DDOName), "DDO_CONTRATOSERVICOSINDICADOR_META") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSINDICADOR_METAOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV38DDOName), "DDO_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV38DDOName), "DDO_CONTRATOSERVICOSINDICADOR_VIGENCIA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSINDICADOR_VIGENCIAOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV42OptionsJson = AV41Options.ToJSonString(false);
         AV45OptionsDescJson = AV44OptionsDesc.ToJSonString(false);
         AV47OptionIndexesJson = AV46OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV49Session.Get("PromptContratoServicosIndicadorGridState"), "") == 0 )
         {
            AV51GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoServicosIndicadorGridState"), "");
         }
         else
         {
            AV51GridState.FromXml(AV49Session.Get("PromptContratoServicosIndicadorGridState"), "");
         }
         AV67GXV1 = 1;
         while ( AV67GXV1 <= AV51GridState.gxTpr_Filtervalues.Count )
         {
            AV52GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV51GridState.gxTpr_Filtervalues.Item(AV67GXV1));
            if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_CODIGO") == 0 )
            {
               AV10TFContratoServicosIndicador_Codigo = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContratoServicosIndicador_Codigo_To = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_CNTSRVCOD") == 0 )
            {
               AV12TFContratoServicosIndicador_CntSrvCod = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContratoServicosIndicador_CntSrvCod_To = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_CONTRATOCOD") == 0 )
            {
               AV14TFContratoServicosIndicador_ContratoCod = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, "."));
               AV15TFContratoServicosIndicador_ContratoCod_To = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_AREATRABALHOCOD") == 0 )
            {
               AV16TFContratoServicosIndicador_AreaTrabalhoCod = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, "."));
               AV17TFContratoServicosIndicador_AreaTrabalhoCod_To = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_NUMERO") == 0 )
            {
               AV18TFContratoServicosIndicador_Numero = (short)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, "."));
               AV19TFContratoServicosIndicador_Numero_To = (short)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
            {
               AV20TFContratoServicosIndicador_Indicador = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL") == 0 )
            {
               AV21TFContratoServicosIndicador_Indicador_Sel = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_FINALIDADE") == 0 )
            {
               AV22TFContratoServicosIndicador_Finalidade = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_FINALIDADE_SEL") == 0 )
            {
               AV23TFContratoServicosIndicador_Finalidade_Sel = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_META") == 0 )
            {
               AV24TFContratoServicosIndicador_Meta = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_META_SEL") == 0 )
            {
               AV25TFContratoServicosIndicador_Meta_Sel = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO") == 0 )
            {
               AV26TFContratoServicosIndicador_InstrumentoMedicao = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO_SEL") == 0 )
            {
               AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_TIPO_SEL") == 0 )
            {
               AV28TFContratoServicosIndicador_Tipo_SelsJson = AV52GridStateFilterValue.gxTpr_Value;
               AV29TFContratoServicosIndicador_Tipo_Sels.FromJSonString(AV28TFContratoServicosIndicador_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_PERIODICIDADE_SEL") == 0 )
            {
               AV30TFContratoServicosIndicador_Periodicidade_SelsJson = AV52GridStateFilterValue.gxTpr_Value;
               AV31TFContratoServicosIndicador_Periodicidade_Sels.FromJSonString(AV30TFContratoServicosIndicador_Periodicidade_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_VIGENCIA") == 0 )
            {
               AV32TFContratoServicosIndicador_Vigencia = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_VIGENCIA_SEL") == 0 )
            {
               AV33TFContratoServicosIndicador_Vigencia_Sel = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS") == 0 )
            {
               AV34TFContratoServicosIndicador_QtdeFaixas = (short)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, "."));
               AV35TFContratoServicosIndicador_QtdeFaixas_To = (short)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, "."));
            }
            AV67GXV1 = (int)(AV67GXV1+1);
         }
         if ( AV51GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV53GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV51GridState.gxTpr_Dynamicfilters.Item(1));
            AV54DynamicFiltersSelector1 = AV53GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
            {
               AV55DynamicFiltersOperator1 = AV53GridStateDynamicFilter.gxTpr_Operator;
               AV56ContratoServicosIndicador_Indicador1 = AV53GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV51GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV57DynamicFiltersEnabled2 = true;
               AV53GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV51GridState.gxTpr_Dynamicfilters.Item(2));
               AV58DynamicFiltersSelector2 = AV53GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
               {
                  AV59DynamicFiltersOperator2 = AV53GridStateDynamicFilter.gxTpr_Operator;
                  AV60ContratoServicosIndicador_Indicador2 = AV53GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV51GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV61DynamicFiltersEnabled3 = true;
                  AV53GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV51GridState.gxTpr_Dynamicfilters.Item(3));
                  AV62DynamicFiltersSelector3 = AV53GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
                  {
                     AV63DynamicFiltersOperator3 = AV53GridStateDynamicFilter.gxTpr_Operator;
                     AV64ContratoServicosIndicador_Indicador3 = AV53GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOSERVICOSINDICADOR_INDICADOROPTIONS' Routine */
         AV20TFContratoServicosIndicador_Indicador = AV36SearchTxt;
         AV21TFContratoServicosIndicador_Indicador_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1308ContratoServicosIndicador_Tipo ,
                                              AV29TFContratoServicosIndicador_Tipo_Sels ,
                                              A1309ContratoServicosIndicador_Periodicidade ,
                                              AV31TFContratoServicosIndicador_Periodicidade_Sels ,
                                              AV54DynamicFiltersSelector1 ,
                                              AV55DynamicFiltersOperator1 ,
                                              AV56ContratoServicosIndicador_Indicador1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV59DynamicFiltersOperator2 ,
                                              AV60ContratoServicosIndicador_Indicador2 ,
                                              AV61DynamicFiltersEnabled3 ,
                                              AV62DynamicFiltersSelector3 ,
                                              AV63DynamicFiltersOperator3 ,
                                              AV64ContratoServicosIndicador_Indicador3 ,
                                              AV10TFContratoServicosIndicador_Codigo ,
                                              AV11TFContratoServicosIndicador_Codigo_To ,
                                              AV12TFContratoServicosIndicador_CntSrvCod ,
                                              AV13TFContratoServicosIndicador_CntSrvCod_To ,
                                              AV14TFContratoServicosIndicador_ContratoCod ,
                                              AV15TFContratoServicosIndicador_ContratoCod_To ,
                                              AV16TFContratoServicosIndicador_AreaTrabalhoCod ,
                                              AV17TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                              AV18TFContratoServicosIndicador_Numero ,
                                              AV19TFContratoServicosIndicador_Numero_To ,
                                              AV21TFContratoServicosIndicador_Indicador_Sel ,
                                              AV20TFContratoServicosIndicador_Indicador ,
                                              AV23TFContratoServicosIndicador_Finalidade_Sel ,
                                              AV22TFContratoServicosIndicador_Finalidade ,
                                              AV25TFContratoServicosIndicador_Meta_Sel ,
                                              AV24TFContratoServicosIndicador_Meta ,
                                              AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                              AV26TFContratoServicosIndicador_InstrumentoMedicao ,
                                              AV29TFContratoServicosIndicador_Tipo_Sels.Count ,
                                              AV31TFContratoServicosIndicador_Periodicidade_Sels.Count ,
                                              AV33TFContratoServicosIndicador_Vigencia_Sel ,
                                              AV32TFContratoServicosIndicador_Vigencia ,
                                              A1274ContratoServicosIndicador_Indicador ,
                                              A1269ContratoServicosIndicador_Codigo ,
                                              A1270ContratoServicosIndicador_CntSrvCod ,
                                              A1296ContratoServicosIndicador_ContratoCod ,
                                              A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                              A1271ContratoServicosIndicador_Numero ,
                                              A1305ContratoServicosIndicador_Finalidade ,
                                              A1306ContratoServicosIndicador_Meta ,
                                              A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                              A1310ContratoServicosIndicador_Vigencia ,
                                              AV34TFContratoServicosIndicador_QtdeFaixas ,
                                              A1298ContratoServicosIndicador_QtdeFaixas ,
                                              AV35TFContratoServicosIndicador_QtdeFaixas_To },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV56ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1), "%", "");
         lV56ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1), "%", "");
         lV60ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2), "%", "");
         lV60ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2), "%", "");
         lV64ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3), "%", "");
         lV64ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3), "%", "");
         lV20TFContratoServicosIndicador_Indicador = StringUtil.Concat( StringUtil.RTrim( AV20TFContratoServicosIndicador_Indicador), "%", "");
         lV22TFContratoServicosIndicador_Finalidade = StringUtil.Concat( StringUtil.RTrim( AV22TFContratoServicosIndicador_Finalidade), "%", "");
         lV24TFContratoServicosIndicador_Meta = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoServicosIndicador_Meta), "%", "");
         lV26TFContratoServicosIndicador_InstrumentoMedicao = StringUtil.Concat( StringUtil.RTrim( AV26TFContratoServicosIndicador_InstrumentoMedicao), "%", "");
         lV32TFContratoServicosIndicador_Vigencia = StringUtil.Concat( StringUtil.RTrim( AV32TFContratoServicosIndicador_Vigencia), "%", "");
         /* Using cursor P00QL3 */
         pr_default.execute(0, new Object[] {AV34TFContratoServicosIndicador_QtdeFaixas, AV34TFContratoServicosIndicador_QtdeFaixas, AV35TFContratoServicosIndicador_QtdeFaixas_To, AV35TFContratoServicosIndicador_QtdeFaixas_To, lV56ContratoServicosIndicador_Indicador1, lV56ContratoServicosIndicador_Indicador1, lV60ContratoServicosIndicador_Indicador2, lV60ContratoServicosIndicador_Indicador2, lV64ContratoServicosIndicador_Indicador3, lV64ContratoServicosIndicador_Indicador3, AV10TFContratoServicosIndicador_Codigo, AV11TFContratoServicosIndicador_Codigo_To, AV12TFContratoServicosIndicador_CntSrvCod, AV13TFContratoServicosIndicador_CntSrvCod_To, AV14TFContratoServicosIndicador_ContratoCod, AV15TFContratoServicosIndicador_ContratoCod_To, AV16TFContratoServicosIndicador_AreaTrabalhoCod, AV17TFContratoServicosIndicador_AreaTrabalhoCod_To, AV18TFContratoServicosIndicador_Numero, AV19TFContratoServicosIndicador_Numero_To, lV20TFContratoServicosIndicador_Indicador, AV21TFContratoServicosIndicador_Indicador_Sel, lV22TFContratoServicosIndicador_Finalidade, AV23TFContratoServicosIndicador_Finalidade_Sel, lV24TFContratoServicosIndicador_Meta, AV25TFContratoServicosIndicador_Meta_Sel, lV26TFContratoServicosIndicador_InstrumentoMedicao, AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel, lV32TFContratoServicosIndicador_Vigencia, AV33TFContratoServicosIndicador_Vigencia_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKQL2 = false;
            A1274ContratoServicosIndicador_Indicador = P00QL3_A1274ContratoServicosIndicador_Indicador[0];
            A1310ContratoServicosIndicador_Vigencia = P00QL3_A1310ContratoServicosIndicador_Vigencia[0];
            n1310ContratoServicosIndicador_Vigencia = P00QL3_n1310ContratoServicosIndicador_Vigencia[0];
            A1309ContratoServicosIndicador_Periodicidade = P00QL3_A1309ContratoServicosIndicador_Periodicidade[0];
            n1309ContratoServicosIndicador_Periodicidade = P00QL3_n1309ContratoServicosIndicador_Periodicidade[0];
            A1308ContratoServicosIndicador_Tipo = P00QL3_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P00QL3_n1308ContratoServicosIndicador_Tipo[0];
            A1307ContratoServicosIndicador_InstrumentoMedicao = P00QL3_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
            n1307ContratoServicosIndicador_InstrumentoMedicao = P00QL3_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
            A1306ContratoServicosIndicador_Meta = P00QL3_A1306ContratoServicosIndicador_Meta[0];
            n1306ContratoServicosIndicador_Meta = P00QL3_n1306ContratoServicosIndicador_Meta[0];
            A1305ContratoServicosIndicador_Finalidade = P00QL3_A1305ContratoServicosIndicador_Finalidade[0];
            n1305ContratoServicosIndicador_Finalidade = P00QL3_n1305ContratoServicosIndicador_Finalidade[0];
            A1271ContratoServicosIndicador_Numero = P00QL3_A1271ContratoServicosIndicador_Numero[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL3_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL3_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1296ContratoServicosIndicador_ContratoCod = P00QL3_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00QL3_n1296ContratoServicosIndicador_ContratoCod[0];
            A1270ContratoServicosIndicador_CntSrvCod = P00QL3_A1270ContratoServicosIndicador_CntSrvCod[0];
            A1269ContratoServicosIndicador_Codigo = P00QL3_A1269ContratoServicosIndicador_Codigo[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00QL3_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = P00QL3_n1298ContratoServicosIndicador_QtdeFaixas[0];
            A1296ContratoServicosIndicador_ContratoCod = P00QL3_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00QL3_n1296ContratoServicosIndicador_ContratoCod[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL3_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL3_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00QL3_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = P00QL3_n1298ContratoServicosIndicador_QtdeFaixas[0];
            AV48count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00QL3_A1274ContratoServicosIndicador_Indicador[0], A1274ContratoServicosIndicador_Indicador) == 0 ) )
            {
               BRKQL2 = false;
               A1269ContratoServicosIndicador_Codigo = P00QL3_A1269ContratoServicosIndicador_Codigo[0];
               AV48count = (long)(AV48count+1);
               BRKQL2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1274ContratoServicosIndicador_Indicador)) )
            {
               AV40Option = A1274ContratoServicosIndicador_Indicador;
               AV41Options.Add(AV40Option, 0);
               AV46OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV48count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV41Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQL2 )
            {
               BRKQL2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOSERVICOSINDICADOR_FINALIDADEOPTIONS' Routine */
         AV22TFContratoServicosIndicador_Finalidade = AV36SearchTxt;
         AV23TFContratoServicosIndicador_Finalidade_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1308ContratoServicosIndicador_Tipo ,
                                              AV29TFContratoServicosIndicador_Tipo_Sels ,
                                              A1309ContratoServicosIndicador_Periodicidade ,
                                              AV31TFContratoServicosIndicador_Periodicidade_Sels ,
                                              AV54DynamicFiltersSelector1 ,
                                              AV55DynamicFiltersOperator1 ,
                                              AV56ContratoServicosIndicador_Indicador1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV59DynamicFiltersOperator2 ,
                                              AV60ContratoServicosIndicador_Indicador2 ,
                                              AV61DynamicFiltersEnabled3 ,
                                              AV62DynamicFiltersSelector3 ,
                                              AV63DynamicFiltersOperator3 ,
                                              AV64ContratoServicosIndicador_Indicador3 ,
                                              AV10TFContratoServicosIndicador_Codigo ,
                                              AV11TFContratoServicosIndicador_Codigo_To ,
                                              AV12TFContratoServicosIndicador_CntSrvCod ,
                                              AV13TFContratoServicosIndicador_CntSrvCod_To ,
                                              AV14TFContratoServicosIndicador_ContratoCod ,
                                              AV15TFContratoServicosIndicador_ContratoCod_To ,
                                              AV16TFContratoServicosIndicador_AreaTrabalhoCod ,
                                              AV17TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                              AV18TFContratoServicosIndicador_Numero ,
                                              AV19TFContratoServicosIndicador_Numero_To ,
                                              AV21TFContratoServicosIndicador_Indicador_Sel ,
                                              AV20TFContratoServicosIndicador_Indicador ,
                                              AV23TFContratoServicosIndicador_Finalidade_Sel ,
                                              AV22TFContratoServicosIndicador_Finalidade ,
                                              AV25TFContratoServicosIndicador_Meta_Sel ,
                                              AV24TFContratoServicosIndicador_Meta ,
                                              AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                              AV26TFContratoServicosIndicador_InstrumentoMedicao ,
                                              AV29TFContratoServicosIndicador_Tipo_Sels.Count ,
                                              AV31TFContratoServicosIndicador_Periodicidade_Sels.Count ,
                                              AV33TFContratoServicosIndicador_Vigencia_Sel ,
                                              AV32TFContratoServicosIndicador_Vigencia ,
                                              A1274ContratoServicosIndicador_Indicador ,
                                              A1269ContratoServicosIndicador_Codigo ,
                                              A1270ContratoServicosIndicador_CntSrvCod ,
                                              A1296ContratoServicosIndicador_ContratoCod ,
                                              A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                              A1271ContratoServicosIndicador_Numero ,
                                              A1305ContratoServicosIndicador_Finalidade ,
                                              A1306ContratoServicosIndicador_Meta ,
                                              A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                              A1310ContratoServicosIndicador_Vigencia ,
                                              AV34TFContratoServicosIndicador_QtdeFaixas ,
                                              A1298ContratoServicosIndicador_QtdeFaixas ,
                                              AV35TFContratoServicosIndicador_QtdeFaixas_To },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV56ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1), "%", "");
         lV56ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1), "%", "");
         lV60ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2), "%", "");
         lV60ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2), "%", "");
         lV64ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3), "%", "");
         lV64ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3), "%", "");
         lV20TFContratoServicosIndicador_Indicador = StringUtil.Concat( StringUtil.RTrim( AV20TFContratoServicosIndicador_Indicador), "%", "");
         lV22TFContratoServicosIndicador_Finalidade = StringUtil.Concat( StringUtil.RTrim( AV22TFContratoServicosIndicador_Finalidade), "%", "");
         lV24TFContratoServicosIndicador_Meta = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoServicosIndicador_Meta), "%", "");
         lV26TFContratoServicosIndicador_InstrumentoMedicao = StringUtil.Concat( StringUtil.RTrim( AV26TFContratoServicosIndicador_InstrumentoMedicao), "%", "");
         lV32TFContratoServicosIndicador_Vigencia = StringUtil.Concat( StringUtil.RTrim( AV32TFContratoServicosIndicador_Vigencia), "%", "");
         /* Using cursor P00QL5 */
         pr_default.execute(1, new Object[] {AV34TFContratoServicosIndicador_QtdeFaixas, AV34TFContratoServicosIndicador_QtdeFaixas, AV35TFContratoServicosIndicador_QtdeFaixas_To, AV35TFContratoServicosIndicador_QtdeFaixas_To, lV56ContratoServicosIndicador_Indicador1, lV56ContratoServicosIndicador_Indicador1, lV60ContratoServicosIndicador_Indicador2, lV60ContratoServicosIndicador_Indicador2, lV64ContratoServicosIndicador_Indicador3, lV64ContratoServicosIndicador_Indicador3, AV10TFContratoServicosIndicador_Codigo, AV11TFContratoServicosIndicador_Codigo_To, AV12TFContratoServicosIndicador_CntSrvCod, AV13TFContratoServicosIndicador_CntSrvCod_To, AV14TFContratoServicosIndicador_ContratoCod, AV15TFContratoServicosIndicador_ContratoCod_To, AV16TFContratoServicosIndicador_AreaTrabalhoCod, AV17TFContratoServicosIndicador_AreaTrabalhoCod_To, AV18TFContratoServicosIndicador_Numero, AV19TFContratoServicosIndicador_Numero_To, lV20TFContratoServicosIndicador_Indicador, AV21TFContratoServicosIndicador_Indicador_Sel, lV22TFContratoServicosIndicador_Finalidade, AV23TFContratoServicosIndicador_Finalidade_Sel, lV24TFContratoServicosIndicador_Meta, AV25TFContratoServicosIndicador_Meta_Sel, lV26TFContratoServicosIndicador_InstrumentoMedicao, AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel, lV32TFContratoServicosIndicador_Vigencia, AV33TFContratoServicosIndicador_Vigencia_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKQL4 = false;
            A1305ContratoServicosIndicador_Finalidade = P00QL5_A1305ContratoServicosIndicador_Finalidade[0];
            n1305ContratoServicosIndicador_Finalidade = P00QL5_n1305ContratoServicosIndicador_Finalidade[0];
            A1310ContratoServicosIndicador_Vigencia = P00QL5_A1310ContratoServicosIndicador_Vigencia[0];
            n1310ContratoServicosIndicador_Vigencia = P00QL5_n1310ContratoServicosIndicador_Vigencia[0];
            A1309ContratoServicosIndicador_Periodicidade = P00QL5_A1309ContratoServicosIndicador_Periodicidade[0];
            n1309ContratoServicosIndicador_Periodicidade = P00QL5_n1309ContratoServicosIndicador_Periodicidade[0];
            A1308ContratoServicosIndicador_Tipo = P00QL5_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P00QL5_n1308ContratoServicosIndicador_Tipo[0];
            A1307ContratoServicosIndicador_InstrumentoMedicao = P00QL5_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
            n1307ContratoServicosIndicador_InstrumentoMedicao = P00QL5_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
            A1306ContratoServicosIndicador_Meta = P00QL5_A1306ContratoServicosIndicador_Meta[0];
            n1306ContratoServicosIndicador_Meta = P00QL5_n1306ContratoServicosIndicador_Meta[0];
            A1271ContratoServicosIndicador_Numero = P00QL5_A1271ContratoServicosIndicador_Numero[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL5_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL5_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1296ContratoServicosIndicador_ContratoCod = P00QL5_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00QL5_n1296ContratoServicosIndicador_ContratoCod[0];
            A1270ContratoServicosIndicador_CntSrvCod = P00QL5_A1270ContratoServicosIndicador_CntSrvCod[0];
            A1269ContratoServicosIndicador_Codigo = P00QL5_A1269ContratoServicosIndicador_Codigo[0];
            A1274ContratoServicosIndicador_Indicador = P00QL5_A1274ContratoServicosIndicador_Indicador[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00QL5_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = P00QL5_n1298ContratoServicosIndicador_QtdeFaixas[0];
            A1296ContratoServicosIndicador_ContratoCod = P00QL5_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00QL5_n1296ContratoServicosIndicador_ContratoCod[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL5_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL5_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00QL5_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = P00QL5_n1298ContratoServicosIndicador_QtdeFaixas[0];
            AV48count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00QL5_A1305ContratoServicosIndicador_Finalidade[0], A1305ContratoServicosIndicador_Finalidade) == 0 ) )
            {
               BRKQL4 = false;
               A1269ContratoServicosIndicador_Codigo = P00QL5_A1269ContratoServicosIndicador_Codigo[0];
               AV48count = (long)(AV48count+1);
               BRKQL4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1305ContratoServicosIndicador_Finalidade)) )
            {
               AV40Option = A1305ContratoServicosIndicador_Finalidade;
               AV41Options.Add(AV40Option, 0);
               AV46OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV48count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV41Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQL4 )
            {
               BRKQL4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOSERVICOSINDICADOR_METAOPTIONS' Routine */
         AV24TFContratoServicosIndicador_Meta = AV36SearchTxt;
         AV25TFContratoServicosIndicador_Meta_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A1308ContratoServicosIndicador_Tipo ,
                                              AV29TFContratoServicosIndicador_Tipo_Sels ,
                                              A1309ContratoServicosIndicador_Periodicidade ,
                                              AV31TFContratoServicosIndicador_Periodicidade_Sels ,
                                              AV54DynamicFiltersSelector1 ,
                                              AV55DynamicFiltersOperator1 ,
                                              AV56ContratoServicosIndicador_Indicador1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV59DynamicFiltersOperator2 ,
                                              AV60ContratoServicosIndicador_Indicador2 ,
                                              AV61DynamicFiltersEnabled3 ,
                                              AV62DynamicFiltersSelector3 ,
                                              AV63DynamicFiltersOperator3 ,
                                              AV64ContratoServicosIndicador_Indicador3 ,
                                              AV10TFContratoServicosIndicador_Codigo ,
                                              AV11TFContratoServicosIndicador_Codigo_To ,
                                              AV12TFContratoServicosIndicador_CntSrvCod ,
                                              AV13TFContratoServicosIndicador_CntSrvCod_To ,
                                              AV14TFContratoServicosIndicador_ContratoCod ,
                                              AV15TFContratoServicosIndicador_ContratoCod_To ,
                                              AV16TFContratoServicosIndicador_AreaTrabalhoCod ,
                                              AV17TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                              AV18TFContratoServicosIndicador_Numero ,
                                              AV19TFContratoServicosIndicador_Numero_To ,
                                              AV21TFContratoServicosIndicador_Indicador_Sel ,
                                              AV20TFContratoServicosIndicador_Indicador ,
                                              AV23TFContratoServicosIndicador_Finalidade_Sel ,
                                              AV22TFContratoServicosIndicador_Finalidade ,
                                              AV25TFContratoServicosIndicador_Meta_Sel ,
                                              AV24TFContratoServicosIndicador_Meta ,
                                              AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                              AV26TFContratoServicosIndicador_InstrumentoMedicao ,
                                              AV29TFContratoServicosIndicador_Tipo_Sels.Count ,
                                              AV31TFContratoServicosIndicador_Periodicidade_Sels.Count ,
                                              AV33TFContratoServicosIndicador_Vigencia_Sel ,
                                              AV32TFContratoServicosIndicador_Vigencia ,
                                              A1274ContratoServicosIndicador_Indicador ,
                                              A1269ContratoServicosIndicador_Codigo ,
                                              A1270ContratoServicosIndicador_CntSrvCod ,
                                              A1296ContratoServicosIndicador_ContratoCod ,
                                              A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                              A1271ContratoServicosIndicador_Numero ,
                                              A1305ContratoServicosIndicador_Finalidade ,
                                              A1306ContratoServicosIndicador_Meta ,
                                              A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                              A1310ContratoServicosIndicador_Vigencia ,
                                              AV34TFContratoServicosIndicador_QtdeFaixas ,
                                              A1298ContratoServicosIndicador_QtdeFaixas ,
                                              AV35TFContratoServicosIndicador_QtdeFaixas_To },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV56ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1), "%", "");
         lV56ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1), "%", "");
         lV60ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2), "%", "");
         lV60ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2), "%", "");
         lV64ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3), "%", "");
         lV64ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3), "%", "");
         lV20TFContratoServicosIndicador_Indicador = StringUtil.Concat( StringUtil.RTrim( AV20TFContratoServicosIndicador_Indicador), "%", "");
         lV22TFContratoServicosIndicador_Finalidade = StringUtil.Concat( StringUtil.RTrim( AV22TFContratoServicosIndicador_Finalidade), "%", "");
         lV24TFContratoServicosIndicador_Meta = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoServicosIndicador_Meta), "%", "");
         lV26TFContratoServicosIndicador_InstrumentoMedicao = StringUtil.Concat( StringUtil.RTrim( AV26TFContratoServicosIndicador_InstrumentoMedicao), "%", "");
         lV32TFContratoServicosIndicador_Vigencia = StringUtil.Concat( StringUtil.RTrim( AV32TFContratoServicosIndicador_Vigencia), "%", "");
         /* Using cursor P00QL7 */
         pr_default.execute(2, new Object[] {AV34TFContratoServicosIndicador_QtdeFaixas, AV34TFContratoServicosIndicador_QtdeFaixas, AV35TFContratoServicosIndicador_QtdeFaixas_To, AV35TFContratoServicosIndicador_QtdeFaixas_To, lV56ContratoServicosIndicador_Indicador1, lV56ContratoServicosIndicador_Indicador1, lV60ContratoServicosIndicador_Indicador2, lV60ContratoServicosIndicador_Indicador2, lV64ContratoServicosIndicador_Indicador3, lV64ContratoServicosIndicador_Indicador3, AV10TFContratoServicosIndicador_Codigo, AV11TFContratoServicosIndicador_Codigo_To, AV12TFContratoServicosIndicador_CntSrvCod, AV13TFContratoServicosIndicador_CntSrvCod_To, AV14TFContratoServicosIndicador_ContratoCod, AV15TFContratoServicosIndicador_ContratoCod_To, AV16TFContratoServicosIndicador_AreaTrabalhoCod, AV17TFContratoServicosIndicador_AreaTrabalhoCod_To, AV18TFContratoServicosIndicador_Numero, AV19TFContratoServicosIndicador_Numero_To, lV20TFContratoServicosIndicador_Indicador, AV21TFContratoServicosIndicador_Indicador_Sel, lV22TFContratoServicosIndicador_Finalidade, AV23TFContratoServicosIndicador_Finalidade_Sel, lV24TFContratoServicosIndicador_Meta, AV25TFContratoServicosIndicador_Meta_Sel, lV26TFContratoServicosIndicador_InstrumentoMedicao, AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel, lV32TFContratoServicosIndicador_Vigencia, AV33TFContratoServicosIndicador_Vigencia_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKQL6 = false;
            A1306ContratoServicosIndicador_Meta = P00QL7_A1306ContratoServicosIndicador_Meta[0];
            n1306ContratoServicosIndicador_Meta = P00QL7_n1306ContratoServicosIndicador_Meta[0];
            A1310ContratoServicosIndicador_Vigencia = P00QL7_A1310ContratoServicosIndicador_Vigencia[0];
            n1310ContratoServicosIndicador_Vigencia = P00QL7_n1310ContratoServicosIndicador_Vigencia[0];
            A1309ContratoServicosIndicador_Periodicidade = P00QL7_A1309ContratoServicosIndicador_Periodicidade[0];
            n1309ContratoServicosIndicador_Periodicidade = P00QL7_n1309ContratoServicosIndicador_Periodicidade[0];
            A1308ContratoServicosIndicador_Tipo = P00QL7_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P00QL7_n1308ContratoServicosIndicador_Tipo[0];
            A1307ContratoServicosIndicador_InstrumentoMedicao = P00QL7_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
            n1307ContratoServicosIndicador_InstrumentoMedicao = P00QL7_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
            A1305ContratoServicosIndicador_Finalidade = P00QL7_A1305ContratoServicosIndicador_Finalidade[0];
            n1305ContratoServicosIndicador_Finalidade = P00QL7_n1305ContratoServicosIndicador_Finalidade[0];
            A1271ContratoServicosIndicador_Numero = P00QL7_A1271ContratoServicosIndicador_Numero[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL7_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL7_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1296ContratoServicosIndicador_ContratoCod = P00QL7_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00QL7_n1296ContratoServicosIndicador_ContratoCod[0];
            A1270ContratoServicosIndicador_CntSrvCod = P00QL7_A1270ContratoServicosIndicador_CntSrvCod[0];
            A1269ContratoServicosIndicador_Codigo = P00QL7_A1269ContratoServicosIndicador_Codigo[0];
            A1274ContratoServicosIndicador_Indicador = P00QL7_A1274ContratoServicosIndicador_Indicador[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00QL7_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = P00QL7_n1298ContratoServicosIndicador_QtdeFaixas[0];
            A1296ContratoServicosIndicador_ContratoCod = P00QL7_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00QL7_n1296ContratoServicosIndicador_ContratoCod[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL7_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL7_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00QL7_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = P00QL7_n1298ContratoServicosIndicador_QtdeFaixas[0];
            AV48count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00QL7_A1306ContratoServicosIndicador_Meta[0], A1306ContratoServicosIndicador_Meta) == 0 ) )
            {
               BRKQL6 = false;
               A1269ContratoServicosIndicador_Codigo = P00QL7_A1269ContratoServicosIndicador_Codigo[0];
               AV48count = (long)(AV48count+1);
               BRKQL6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1306ContratoServicosIndicador_Meta)) )
            {
               AV40Option = A1306ContratoServicosIndicador_Meta;
               AV41Options.Add(AV40Option, 0);
               AV46OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV48count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV41Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQL6 )
            {
               BRKQL6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAOOPTIONS' Routine */
         AV26TFContratoServicosIndicador_InstrumentoMedicao = AV36SearchTxt;
         AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel = "";
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A1308ContratoServicosIndicador_Tipo ,
                                              AV29TFContratoServicosIndicador_Tipo_Sels ,
                                              A1309ContratoServicosIndicador_Periodicidade ,
                                              AV31TFContratoServicosIndicador_Periodicidade_Sels ,
                                              AV54DynamicFiltersSelector1 ,
                                              AV55DynamicFiltersOperator1 ,
                                              AV56ContratoServicosIndicador_Indicador1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV59DynamicFiltersOperator2 ,
                                              AV60ContratoServicosIndicador_Indicador2 ,
                                              AV61DynamicFiltersEnabled3 ,
                                              AV62DynamicFiltersSelector3 ,
                                              AV63DynamicFiltersOperator3 ,
                                              AV64ContratoServicosIndicador_Indicador3 ,
                                              AV10TFContratoServicosIndicador_Codigo ,
                                              AV11TFContratoServicosIndicador_Codigo_To ,
                                              AV12TFContratoServicosIndicador_CntSrvCod ,
                                              AV13TFContratoServicosIndicador_CntSrvCod_To ,
                                              AV14TFContratoServicosIndicador_ContratoCod ,
                                              AV15TFContratoServicosIndicador_ContratoCod_To ,
                                              AV16TFContratoServicosIndicador_AreaTrabalhoCod ,
                                              AV17TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                              AV18TFContratoServicosIndicador_Numero ,
                                              AV19TFContratoServicosIndicador_Numero_To ,
                                              AV21TFContratoServicosIndicador_Indicador_Sel ,
                                              AV20TFContratoServicosIndicador_Indicador ,
                                              AV23TFContratoServicosIndicador_Finalidade_Sel ,
                                              AV22TFContratoServicosIndicador_Finalidade ,
                                              AV25TFContratoServicosIndicador_Meta_Sel ,
                                              AV24TFContratoServicosIndicador_Meta ,
                                              AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                              AV26TFContratoServicosIndicador_InstrumentoMedicao ,
                                              AV29TFContratoServicosIndicador_Tipo_Sels.Count ,
                                              AV31TFContratoServicosIndicador_Periodicidade_Sels.Count ,
                                              AV33TFContratoServicosIndicador_Vigencia_Sel ,
                                              AV32TFContratoServicosIndicador_Vigencia ,
                                              A1274ContratoServicosIndicador_Indicador ,
                                              A1269ContratoServicosIndicador_Codigo ,
                                              A1270ContratoServicosIndicador_CntSrvCod ,
                                              A1296ContratoServicosIndicador_ContratoCod ,
                                              A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                              A1271ContratoServicosIndicador_Numero ,
                                              A1305ContratoServicosIndicador_Finalidade ,
                                              A1306ContratoServicosIndicador_Meta ,
                                              A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                              A1310ContratoServicosIndicador_Vigencia ,
                                              AV34TFContratoServicosIndicador_QtdeFaixas ,
                                              A1298ContratoServicosIndicador_QtdeFaixas ,
                                              AV35TFContratoServicosIndicador_QtdeFaixas_To },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV56ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1), "%", "");
         lV56ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1), "%", "");
         lV60ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2), "%", "");
         lV60ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2), "%", "");
         lV64ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3), "%", "");
         lV64ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3), "%", "");
         lV20TFContratoServicosIndicador_Indicador = StringUtil.Concat( StringUtil.RTrim( AV20TFContratoServicosIndicador_Indicador), "%", "");
         lV22TFContratoServicosIndicador_Finalidade = StringUtil.Concat( StringUtil.RTrim( AV22TFContratoServicosIndicador_Finalidade), "%", "");
         lV24TFContratoServicosIndicador_Meta = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoServicosIndicador_Meta), "%", "");
         lV26TFContratoServicosIndicador_InstrumentoMedicao = StringUtil.Concat( StringUtil.RTrim( AV26TFContratoServicosIndicador_InstrumentoMedicao), "%", "");
         lV32TFContratoServicosIndicador_Vigencia = StringUtil.Concat( StringUtil.RTrim( AV32TFContratoServicosIndicador_Vigencia), "%", "");
         /* Using cursor P00QL9 */
         pr_default.execute(3, new Object[] {AV34TFContratoServicosIndicador_QtdeFaixas, AV34TFContratoServicosIndicador_QtdeFaixas, AV35TFContratoServicosIndicador_QtdeFaixas_To, AV35TFContratoServicosIndicador_QtdeFaixas_To, lV56ContratoServicosIndicador_Indicador1, lV56ContratoServicosIndicador_Indicador1, lV60ContratoServicosIndicador_Indicador2, lV60ContratoServicosIndicador_Indicador2, lV64ContratoServicosIndicador_Indicador3, lV64ContratoServicosIndicador_Indicador3, AV10TFContratoServicosIndicador_Codigo, AV11TFContratoServicosIndicador_Codigo_To, AV12TFContratoServicosIndicador_CntSrvCod, AV13TFContratoServicosIndicador_CntSrvCod_To, AV14TFContratoServicosIndicador_ContratoCod, AV15TFContratoServicosIndicador_ContratoCod_To, AV16TFContratoServicosIndicador_AreaTrabalhoCod, AV17TFContratoServicosIndicador_AreaTrabalhoCod_To, AV18TFContratoServicosIndicador_Numero, AV19TFContratoServicosIndicador_Numero_To, lV20TFContratoServicosIndicador_Indicador, AV21TFContratoServicosIndicador_Indicador_Sel, lV22TFContratoServicosIndicador_Finalidade, AV23TFContratoServicosIndicador_Finalidade_Sel, lV24TFContratoServicosIndicador_Meta, AV25TFContratoServicosIndicador_Meta_Sel, lV26TFContratoServicosIndicador_InstrumentoMedicao, AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel, lV32TFContratoServicosIndicador_Vigencia, AV33TFContratoServicosIndicador_Vigencia_Sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKQL8 = false;
            A1307ContratoServicosIndicador_InstrumentoMedicao = P00QL9_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
            n1307ContratoServicosIndicador_InstrumentoMedicao = P00QL9_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
            A1310ContratoServicosIndicador_Vigencia = P00QL9_A1310ContratoServicosIndicador_Vigencia[0];
            n1310ContratoServicosIndicador_Vigencia = P00QL9_n1310ContratoServicosIndicador_Vigencia[0];
            A1309ContratoServicosIndicador_Periodicidade = P00QL9_A1309ContratoServicosIndicador_Periodicidade[0];
            n1309ContratoServicosIndicador_Periodicidade = P00QL9_n1309ContratoServicosIndicador_Periodicidade[0];
            A1308ContratoServicosIndicador_Tipo = P00QL9_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P00QL9_n1308ContratoServicosIndicador_Tipo[0];
            A1306ContratoServicosIndicador_Meta = P00QL9_A1306ContratoServicosIndicador_Meta[0];
            n1306ContratoServicosIndicador_Meta = P00QL9_n1306ContratoServicosIndicador_Meta[0];
            A1305ContratoServicosIndicador_Finalidade = P00QL9_A1305ContratoServicosIndicador_Finalidade[0];
            n1305ContratoServicosIndicador_Finalidade = P00QL9_n1305ContratoServicosIndicador_Finalidade[0];
            A1271ContratoServicosIndicador_Numero = P00QL9_A1271ContratoServicosIndicador_Numero[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL9_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL9_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1296ContratoServicosIndicador_ContratoCod = P00QL9_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00QL9_n1296ContratoServicosIndicador_ContratoCod[0];
            A1270ContratoServicosIndicador_CntSrvCod = P00QL9_A1270ContratoServicosIndicador_CntSrvCod[0];
            A1269ContratoServicosIndicador_Codigo = P00QL9_A1269ContratoServicosIndicador_Codigo[0];
            A1274ContratoServicosIndicador_Indicador = P00QL9_A1274ContratoServicosIndicador_Indicador[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00QL9_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = P00QL9_n1298ContratoServicosIndicador_QtdeFaixas[0];
            A1296ContratoServicosIndicador_ContratoCod = P00QL9_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00QL9_n1296ContratoServicosIndicador_ContratoCod[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL9_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL9_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00QL9_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = P00QL9_n1298ContratoServicosIndicador_QtdeFaixas[0];
            AV48count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00QL9_A1307ContratoServicosIndicador_InstrumentoMedicao[0], A1307ContratoServicosIndicador_InstrumentoMedicao) == 0 ) )
            {
               BRKQL8 = false;
               A1269ContratoServicosIndicador_Codigo = P00QL9_A1269ContratoServicosIndicador_Codigo[0];
               AV48count = (long)(AV48count+1);
               BRKQL8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1307ContratoServicosIndicador_InstrumentoMedicao)) )
            {
               AV40Option = A1307ContratoServicosIndicador_InstrumentoMedicao;
               AV41Options.Add(AV40Option, 0);
               AV46OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV48count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV41Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQL8 )
            {
               BRKQL8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADCONTRATOSERVICOSINDICADOR_VIGENCIAOPTIONS' Routine */
         AV32TFContratoServicosIndicador_Vigencia = AV36SearchTxt;
         AV33TFContratoServicosIndicador_Vigencia_Sel = "";
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A1308ContratoServicosIndicador_Tipo ,
                                              AV29TFContratoServicosIndicador_Tipo_Sels ,
                                              A1309ContratoServicosIndicador_Periodicidade ,
                                              AV31TFContratoServicosIndicador_Periodicidade_Sels ,
                                              AV54DynamicFiltersSelector1 ,
                                              AV55DynamicFiltersOperator1 ,
                                              AV56ContratoServicosIndicador_Indicador1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV59DynamicFiltersOperator2 ,
                                              AV60ContratoServicosIndicador_Indicador2 ,
                                              AV61DynamicFiltersEnabled3 ,
                                              AV62DynamicFiltersSelector3 ,
                                              AV63DynamicFiltersOperator3 ,
                                              AV64ContratoServicosIndicador_Indicador3 ,
                                              AV10TFContratoServicosIndicador_Codigo ,
                                              AV11TFContratoServicosIndicador_Codigo_To ,
                                              AV12TFContratoServicosIndicador_CntSrvCod ,
                                              AV13TFContratoServicosIndicador_CntSrvCod_To ,
                                              AV14TFContratoServicosIndicador_ContratoCod ,
                                              AV15TFContratoServicosIndicador_ContratoCod_To ,
                                              AV16TFContratoServicosIndicador_AreaTrabalhoCod ,
                                              AV17TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                              AV18TFContratoServicosIndicador_Numero ,
                                              AV19TFContratoServicosIndicador_Numero_To ,
                                              AV21TFContratoServicosIndicador_Indicador_Sel ,
                                              AV20TFContratoServicosIndicador_Indicador ,
                                              AV23TFContratoServicosIndicador_Finalidade_Sel ,
                                              AV22TFContratoServicosIndicador_Finalidade ,
                                              AV25TFContratoServicosIndicador_Meta_Sel ,
                                              AV24TFContratoServicosIndicador_Meta ,
                                              AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                              AV26TFContratoServicosIndicador_InstrumentoMedicao ,
                                              AV29TFContratoServicosIndicador_Tipo_Sels.Count ,
                                              AV31TFContratoServicosIndicador_Periodicidade_Sels.Count ,
                                              AV33TFContratoServicosIndicador_Vigencia_Sel ,
                                              AV32TFContratoServicosIndicador_Vigencia ,
                                              A1274ContratoServicosIndicador_Indicador ,
                                              A1269ContratoServicosIndicador_Codigo ,
                                              A1270ContratoServicosIndicador_CntSrvCod ,
                                              A1296ContratoServicosIndicador_ContratoCod ,
                                              A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                              A1271ContratoServicosIndicador_Numero ,
                                              A1305ContratoServicosIndicador_Finalidade ,
                                              A1306ContratoServicosIndicador_Meta ,
                                              A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                              A1310ContratoServicosIndicador_Vigencia ,
                                              AV34TFContratoServicosIndicador_QtdeFaixas ,
                                              A1298ContratoServicosIndicador_QtdeFaixas ,
                                              AV35TFContratoServicosIndicador_QtdeFaixas_To },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV56ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1), "%", "");
         lV56ContratoServicosIndicador_Indicador1 = StringUtil.Concat( StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1), "%", "");
         lV60ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2), "%", "");
         lV60ContratoServicosIndicador_Indicador2 = StringUtil.Concat( StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2), "%", "");
         lV64ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3), "%", "");
         lV64ContratoServicosIndicador_Indicador3 = StringUtil.Concat( StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3), "%", "");
         lV20TFContratoServicosIndicador_Indicador = StringUtil.Concat( StringUtil.RTrim( AV20TFContratoServicosIndicador_Indicador), "%", "");
         lV22TFContratoServicosIndicador_Finalidade = StringUtil.Concat( StringUtil.RTrim( AV22TFContratoServicosIndicador_Finalidade), "%", "");
         lV24TFContratoServicosIndicador_Meta = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoServicosIndicador_Meta), "%", "");
         lV26TFContratoServicosIndicador_InstrumentoMedicao = StringUtil.Concat( StringUtil.RTrim( AV26TFContratoServicosIndicador_InstrumentoMedicao), "%", "");
         lV32TFContratoServicosIndicador_Vigencia = StringUtil.Concat( StringUtil.RTrim( AV32TFContratoServicosIndicador_Vigencia), "%", "");
         /* Using cursor P00QL11 */
         pr_default.execute(4, new Object[] {AV34TFContratoServicosIndicador_QtdeFaixas, AV34TFContratoServicosIndicador_QtdeFaixas, AV35TFContratoServicosIndicador_QtdeFaixas_To, AV35TFContratoServicosIndicador_QtdeFaixas_To, lV56ContratoServicosIndicador_Indicador1, lV56ContratoServicosIndicador_Indicador1, lV60ContratoServicosIndicador_Indicador2, lV60ContratoServicosIndicador_Indicador2, lV64ContratoServicosIndicador_Indicador3, lV64ContratoServicosIndicador_Indicador3, AV10TFContratoServicosIndicador_Codigo, AV11TFContratoServicosIndicador_Codigo_To, AV12TFContratoServicosIndicador_CntSrvCod, AV13TFContratoServicosIndicador_CntSrvCod_To, AV14TFContratoServicosIndicador_ContratoCod, AV15TFContratoServicosIndicador_ContratoCod_To, AV16TFContratoServicosIndicador_AreaTrabalhoCod, AV17TFContratoServicosIndicador_AreaTrabalhoCod_To, AV18TFContratoServicosIndicador_Numero, AV19TFContratoServicosIndicador_Numero_To, lV20TFContratoServicosIndicador_Indicador, AV21TFContratoServicosIndicador_Indicador_Sel, lV22TFContratoServicosIndicador_Finalidade, AV23TFContratoServicosIndicador_Finalidade_Sel, lV24TFContratoServicosIndicador_Meta, AV25TFContratoServicosIndicador_Meta_Sel, lV26TFContratoServicosIndicador_InstrumentoMedicao, AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel, lV32TFContratoServicosIndicador_Vigencia, AV33TFContratoServicosIndicador_Vigencia_Sel});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKQL10 = false;
            A1310ContratoServicosIndicador_Vigencia = P00QL11_A1310ContratoServicosIndicador_Vigencia[0];
            n1310ContratoServicosIndicador_Vigencia = P00QL11_n1310ContratoServicosIndicador_Vigencia[0];
            A1309ContratoServicosIndicador_Periodicidade = P00QL11_A1309ContratoServicosIndicador_Periodicidade[0];
            n1309ContratoServicosIndicador_Periodicidade = P00QL11_n1309ContratoServicosIndicador_Periodicidade[0];
            A1308ContratoServicosIndicador_Tipo = P00QL11_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P00QL11_n1308ContratoServicosIndicador_Tipo[0];
            A1307ContratoServicosIndicador_InstrumentoMedicao = P00QL11_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
            n1307ContratoServicosIndicador_InstrumentoMedicao = P00QL11_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
            A1306ContratoServicosIndicador_Meta = P00QL11_A1306ContratoServicosIndicador_Meta[0];
            n1306ContratoServicosIndicador_Meta = P00QL11_n1306ContratoServicosIndicador_Meta[0];
            A1305ContratoServicosIndicador_Finalidade = P00QL11_A1305ContratoServicosIndicador_Finalidade[0];
            n1305ContratoServicosIndicador_Finalidade = P00QL11_n1305ContratoServicosIndicador_Finalidade[0];
            A1271ContratoServicosIndicador_Numero = P00QL11_A1271ContratoServicosIndicador_Numero[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL11_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL11_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1296ContratoServicosIndicador_ContratoCod = P00QL11_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00QL11_n1296ContratoServicosIndicador_ContratoCod[0];
            A1270ContratoServicosIndicador_CntSrvCod = P00QL11_A1270ContratoServicosIndicador_CntSrvCod[0];
            A1269ContratoServicosIndicador_Codigo = P00QL11_A1269ContratoServicosIndicador_Codigo[0];
            A1274ContratoServicosIndicador_Indicador = P00QL11_A1274ContratoServicosIndicador_Indicador[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00QL11_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = P00QL11_n1298ContratoServicosIndicador_QtdeFaixas[0];
            A1296ContratoServicosIndicador_ContratoCod = P00QL11_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00QL11_n1296ContratoServicosIndicador_ContratoCod[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL11_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00QL11_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00QL11_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = P00QL11_n1298ContratoServicosIndicador_QtdeFaixas[0];
            AV48count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00QL11_A1310ContratoServicosIndicador_Vigencia[0], A1310ContratoServicosIndicador_Vigencia) == 0 ) )
            {
               BRKQL10 = false;
               A1269ContratoServicosIndicador_Codigo = P00QL11_A1269ContratoServicosIndicador_Codigo[0];
               AV48count = (long)(AV48count+1);
               BRKQL10 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1310ContratoServicosIndicador_Vigencia)) )
            {
               AV40Option = A1310ContratoServicosIndicador_Vigencia;
               AV41Options.Add(AV40Option, 0);
               AV46OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV48count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV41Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQL10 )
            {
               BRKQL10 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV41Options = new GxSimpleCollection();
         AV44OptionsDesc = new GxSimpleCollection();
         AV46OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV49Session = context.GetSession();
         AV51GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV52GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV20TFContratoServicosIndicador_Indicador = "";
         AV21TFContratoServicosIndicador_Indicador_Sel = "";
         AV22TFContratoServicosIndicador_Finalidade = "";
         AV23TFContratoServicosIndicador_Finalidade_Sel = "";
         AV24TFContratoServicosIndicador_Meta = "";
         AV25TFContratoServicosIndicador_Meta_Sel = "";
         AV26TFContratoServicosIndicador_InstrumentoMedicao = "";
         AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel = "";
         AV28TFContratoServicosIndicador_Tipo_SelsJson = "";
         AV29TFContratoServicosIndicador_Tipo_Sels = new GxSimpleCollection();
         AV30TFContratoServicosIndicador_Periodicidade_SelsJson = "";
         AV31TFContratoServicosIndicador_Periodicidade_Sels = new GxSimpleCollection();
         AV32TFContratoServicosIndicador_Vigencia = "";
         AV33TFContratoServicosIndicador_Vigencia_Sel = "";
         AV53GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV54DynamicFiltersSelector1 = "";
         AV56ContratoServicosIndicador_Indicador1 = "";
         AV58DynamicFiltersSelector2 = "";
         AV60ContratoServicosIndicador_Indicador2 = "";
         AV62DynamicFiltersSelector3 = "";
         AV64ContratoServicosIndicador_Indicador3 = "";
         scmdbuf = "";
         lV56ContratoServicosIndicador_Indicador1 = "";
         lV60ContratoServicosIndicador_Indicador2 = "";
         lV64ContratoServicosIndicador_Indicador3 = "";
         lV20TFContratoServicosIndicador_Indicador = "";
         lV22TFContratoServicosIndicador_Finalidade = "";
         lV24TFContratoServicosIndicador_Meta = "";
         lV26TFContratoServicosIndicador_InstrumentoMedicao = "";
         lV32TFContratoServicosIndicador_Vigencia = "";
         A1308ContratoServicosIndicador_Tipo = "";
         A1309ContratoServicosIndicador_Periodicidade = "";
         A1274ContratoServicosIndicador_Indicador = "";
         A1305ContratoServicosIndicador_Finalidade = "";
         A1306ContratoServicosIndicador_Meta = "";
         A1307ContratoServicosIndicador_InstrumentoMedicao = "";
         A1310ContratoServicosIndicador_Vigencia = "";
         P00QL3_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         P00QL3_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         P00QL3_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         P00QL3_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         P00QL3_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         P00QL3_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P00QL3_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P00QL3_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         P00QL3_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         P00QL3_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         P00QL3_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         P00QL3_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         P00QL3_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         P00QL3_A1271ContratoServicosIndicador_Numero = new short[1] ;
         P00QL3_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         P00QL3_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         P00QL3_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         P00QL3_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         P00QL3_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P00QL3_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00QL3_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         P00QL3_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         AV40Option = "";
         P00QL5_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         P00QL5_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         P00QL5_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         P00QL5_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         P00QL5_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         P00QL5_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         P00QL5_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P00QL5_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P00QL5_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         P00QL5_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         P00QL5_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         P00QL5_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         P00QL5_A1271ContratoServicosIndicador_Numero = new short[1] ;
         P00QL5_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         P00QL5_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         P00QL5_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         P00QL5_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         P00QL5_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P00QL5_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00QL5_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         P00QL5_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         P00QL5_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         P00QL7_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         P00QL7_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         P00QL7_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         P00QL7_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         P00QL7_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         P00QL7_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         P00QL7_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P00QL7_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P00QL7_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         P00QL7_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         P00QL7_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         P00QL7_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         P00QL7_A1271ContratoServicosIndicador_Numero = new short[1] ;
         P00QL7_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         P00QL7_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         P00QL7_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         P00QL7_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         P00QL7_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P00QL7_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00QL7_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         P00QL7_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         P00QL7_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         P00QL9_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         P00QL9_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         P00QL9_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         P00QL9_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         P00QL9_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         P00QL9_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         P00QL9_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P00QL9_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P00QL9_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         P00QL9_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         P00QL9_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         P00QL9_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         P00QL9_A1271ContratoServicosIndicador_Numero = new short[1] ;
         P00QL9_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         P00QL9_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         P00QL9_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         P00QL9_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         P00QL9_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P00QL9_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00QL9_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         P00QL9_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         P00QL9_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         P00QL11_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         P00QL11_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         P00QL11_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         P00QL11_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         P00QL11_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P00QL11_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P00QL11_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         P00QL11_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         P00QL11_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         P00QL11_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         P00QL11_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         P00QL11_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         P00QL11_A1271ContratoServicosIndicador_Numero = new short[1] ;
         P00QL11_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         P00QL11_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         P00QL11_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         P00QL11_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         P00QL11_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P00QL11_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00QL11_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         P00QL11_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         P00QL11_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratoservicosindicadorfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00QL3_A1274ContratoServicosIndicador_Indicador, P00QL3_A1310ContratoServicosIndicador_Vigencia, P00QL3_n1310ContratoServicosIndicador_Vigencia, P00QL3_A1309ContratoServicosIndicador_Periodicidade, P00QL3_n1309ContratoServicosIndicador_Periodicidade, P00QL3_A1308ContratoServicosIndicador_Tipo, P00QL3_n1308ContratoServicosIndicador_Tipo, P00QL3_A1307ContratoServicosIndicador_InstrumentoMedicao, P00QL3_n1307ContratoServicosIndicador_InstrumentoMedicao, P00QL3_A1306ContratoServicosIndicador_Meta,
               P00QL3_n1306ContratoServicosIndicador_Meta, P00QL3_A1305ContratoServicosIndicador_Finalidade, P00QL3_n1305ContratoServicosIndicador_Finalidade, P00QL3_A1271ContratoServicosIndicador_Numero, P00QL3_A1295ContratoServicosIndicador_AreaTrabalhoCod, P00QL3_n1295ContratoServicosIndicador_AreaTrabalhoCod, P00QL3_A1296ContratoServicosIndicador_ContratoCod, P00QL3_n1296ContratoServicosIndicador_ContratoCod, P00QL3_A1270ContratoServicosIndicador_CntSrvCod, P00QL3_A1269ContratoServicosIndicador_Codigo,
               P00QL3_A1298ContratoServicosIndicador_QtdeFaixas, P00QL3_n1298ContratoServicosIndicador_QtdeFaixas
               }
               , new Object[] {
               P00QL5_A1305ContratoServicosIndicador_Finalidade, P00QL5_n1305ContratoServicosIndicador_Finalidade, P00QL5_A1310ContratoServicosIndicador_Vigencia, P00QL5_n1310ContratoServicosIndicador_Vigencia, P00QL5_A1309ContratoServicosIndicador_Periodicidade, P00QL5_n1309ContratoServicosIndicador_Periodicidade, P00QL5_A1308ContratoServicosIndicador_Tipo, P00QL5_n1308ContratoServicosIndicador_Tipo, P00QL5_A1307ContratoServicosIndicador_InstrumentoMedicao, P00QL5_n1307ContratoServicosIndicador_InstrumentoMedicao,
               P00QL5_A1306ContratoServicosIndicador_Meta, P00QL5_n1306ContratoServicosIndicador_Meta, P00QL5_A1271ContratoServicosIndicador_Numero, P00QL5_A1295ContratoServicosIndicador_AreaTrabalhoCod, P00QL5_n1295ContratoServicosIndicador_AreaTrabalhoCod, P00QL5_A1296ContratoServicosIndicador_ContratoCod, P00QL5_n1296ContratoServicosIndicador_ContratoCod, P00QL5_A1270ContratoServicosIndicador_CntSrvCod, P00QL5_A1269ContratoServicosIndicador_Codigo, P00QL5_A1274ContratoServicosIndicador_Indicador,
               P00QL5_A1298ContratoServicosIndicador_QtdeFaixas, P00QL5_n1298ContratoServicosIndicador_QtdeFaixas
               }
               , new Object[] {
               P00QL7_A1306ContratoServicosIndicador_Meta, P00QL7_n1306ContratoServicosIndicador_Meta, P00QL7_A1310ContratoServicosIndicador_Vigencia, P00QL7_n1310ContratoServicosIndicador_Vigencia, P00QL7_A1309ContratoServicosIndicador_Periodicidade, P00QL7_n1309ContratoServicosIndicador_Periodicidade, P00QL7_A1308ContratoServicosIndicador_Tipo, P00QL7_n1308ContratoServicosIndicador_Tipo, P00QL7_A1307ContratoServicosIndicador_InstrumentoMedicao, P00QL7_n1307ContratoServicosIndicador_InstrumentoMedicao,
               P00QL7_A1305ContratoServicosIndicador_Finalidade, P00QL7_n1305ContratoServicosIndicador_Finalidade, P00QL7_A1271ContratoServicosIndicador_Numero, P00QL7_A1295ContratoServicosIndicador_AreaTrabalhoCod, P00QL7_n1295ContratoServicosIndicador_AreaTrabalhoCod, P00QL7_A1296ContratoServicosIndicador_ContratoCod, P00QL7_n1296ContratoServicosIndicador_ContratoCod, P00QL7_A1270ContratoServicosIndicador_CntSrvCod, P00QL7_A1269ContratoServicosIndicador_Codigo, P00QL7_A1274ContratoServicosIndicador_Indicador,
               P00QL7_A1298ContratoServicosIndicador_QtdeFaixas, P00QL7_n1298ContratoServicosIndicador_QtdeFaixas
               }
               , new Object[] {
               P00QL9_A1307ContratoServicosIndicador_InstrumentoMedicao, P00QL9_n1307ContratoServicosIndicador_InstrumentoMedicao, P00QL9_A1310ContratoServicosIndicador_Vigencia, P00QL9_n1310ContratoServicosIndicador_Vigencia, P00QL9_A1309ContratoServicosIndicador_Periodicidade, P00QL9_n1309ContratoServicosIndicador_Periodicidade, P00QL9_A1308ContratoServicosIndicador_Tipo, P00QL9_n1308ContratoServicosIndicador_Tipo, P00QL9_A1306ContratoServicosIndicador_Meta, P00QL9_n1306ContratoServicosIndicador_Meta,
               P00QL9_A1305ContratoServicosIndicador_Finalidade, P00QL9_n1305ContratoServicosIndicador_Finalidade, P00QL9_A1271ContratoServicosIndicador_Numero, P00QL9_A1295ContratoServicosIndicador_AreaTrabalhoCod, P00QL9_n1295ContratoServicosIndicador_AreaTrabalhoCod, P00QL9_A1296ContratoServicosIndicador_ContratoCod, P00QL9_n1296ContratoServicosIndicador_ContratoCod, P00QL9_A1270ContratoServicosIndicador_CntSrvCod, P00QL9_A1269ContratoServicosIndicador_Codigo, P00QL9_A1274ContratoServicosIndicador_Indicador,
               P00QL9_A1298ContratoServicosIndicador_QtdeFaixas, P00QL9_n1298ContratoServicosIndicador_QtdeFaixas
               }
               , new Object[] {
               P00QL11_A1310ContratoServicosIndicador_Vigencia, P00QL11_n1310ContratoServicosIndicador_Vigencia, P00QL11_A1309ContratoServicosIndicador_Periodicidade, P00QL11_n1309ContratoServicosIndicador_Periodicidade, P00QL11_A1308ContratoServicosIndicador_Tipo, P00QL11_n1308ContratoServicosIndicador_Tipo, P00QL11_A1307ContratoServicosIndicador_InstrumentoMedicao, P00QL11_n1307ContratoServicosIndicador_InstrumentoMedicao, P00QL11_A1306ContratoServicosIndicador_Meta, P00QL11_n1306ContratoServicosIndicador_Meta,
               P00QL11_A1305ContratoServicosIndicador_Finalidade, P00QL11_n1305ContratoServicosIndicador_Finalidade, P00QL11_A1271ContratoServicosIndicador_Numero, P00QL11_A1295ContratoServicosIndicador_AreaTrabalhoCod, P00QL11_n1295ContratoServicosIndicador_AreaTrabalhoCod, P00QL11_A1296ContratoServicosIndicador_ContratoCod, P00QL11_n1296ContratoServicosIndicador_ContratoCod, P00QL11_A1270ContratoServicosIndicador_CntSrvCod, P00QL11_A1269ContratoServicosIndicador_Codigo, P00QL11_A1274ContratoServicosIndicador_Indicador,
               P00QL11_A1298ContratoServicosIndicador_QtdeFaixas, P00QL11_n1298ContratoServicosIndicador_QtdeFaixas
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18TFContratoServicosIndicador_Numero ;
      private short AV19TFContratoServicosIndicador_Numero_To ;
      private short AV34TFContratoServicosIndicador_QtdeFaixas ;
      private short AV35TFContratoServicosIndicador_QtdeFaixas_To ;
      private short AV55DynamicFiltersOperator1 ;
      private short AV59DynamicFiltersOperator2 ;
      private short AV63DynamicFiltersOperator3 ;
      private short A1271ContratoServicosIndicador_Numero ;
      private short A1298ContratoServicosIndicador_QtdeFaixas ;
      private int AV67GXV1 ;
      private int AV10TFContratoServicosIndicador_Codigo ;
      private int AV11TFContratoServicosIndicador_Codigo_To ;
      private int AV12TFContratoServicosIndicador_CntSrvCod ;
      private int AV13TFContratoServicosIndicador_CntSrvCod_To ;
      private int AV14TFContratoServicosIndicador_ContratoCod ;
      private int AV15TFContratoServicosIndicador_ContratoCod_To ;
      private int AV16TFContratoServicosIndicador_AreaTrabalhoCod ;
      private int AV17TFContratoServicosIndicador_AreaTrabalhoCod_To ;
      private int AV29TFContratoServicosIndicador_Tipo_Sels_Count ;
      private int AV31TFContratoServicosIndicador_Periodicidade_Sels_Count ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A1296ContratoServicosIndicador_ContratoCod ;
      private int A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private long AV48count ;
      private String scmdbuf ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private String A1309ContratoServicosIndicador_Periodicidade ;
      private bool returnInSub ;
      private bool AV57DynamicFiltersEnabled2 ;
      private bool AV61DynamicFiltersEnabled3 ;
      private bool BRKQL2 ;
      private bool n1310ContratoServicosIndicador_Vigencia ;
      private bool n1309ContratoServicosIndicador_Periodicidade ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private bool n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool n1306ContratoServicosIndicador_Meta ;
      private bool n1305ContratoServicosIndicador_Finalidade ;
      private bool n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool n1296ContratoServicosIndicador_ContratoCod ;
      private bool n1298ContratoServicosIndicador_QtdeFaixas ;
      private bool BRKQL4 ;
      private bool BRKQL6 ;
      private bool BRKQL8 ;
      private bool BRKQL10 ;
      private String AV47OptionIndexesJson ;
      private String AV42OptionsJson ;
      private String AV45OptionsDescJson ;
      private String AV28TFContratoServicosIndicador_Tipo_SelsJson ;
      private String AV30TFContratoServicosIndicador_Periodicidade_SelsJson ;
      private String AV56ContratoServicosIndicador_Indicador1 ;
      private String AV60ContratoServicosIndicador_Indicador2 ;
      private String AV64ContratoServicosIndicador_Indicador3 ;
      private String lV56ContratoServicosIndicador_Indicador1 ;
      private String lV60ContratoServicosIndicador_Indicador2 ;
      private String lV64ContratoServicosIndicador_Indicador3 ;
      private String A1274ContratoServicosIndicador_Indicador ;
      private String A1305ContratoServicosIndicador_Finalidade ;
      private String A1306ContratoServicosIndicador_Meta ;
      private String A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String AV38DDOName ;
      private String AV36SearchTxt ;
      private String AV37SearchTxtTo ;
      private String AV20TFContratoServicosIndicador_Indicador ;
      private String AV21TFContratoServicosIndicador_Indicador_Sel ;
      private String AV22TFContratoServicosIndicador_Finalidade ;
      private String AV23TFContratoServicosIndicador_Finalidade_Sel ;
      private String AV24TFContratoServicosIndicador_Meta ;
      private String AV25TFContratoServicosIndicador_Meta_Sel ;
      private String AV26TFContratoServicosIndicador_InstrumentoMedicao ;
      private String AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel ;
      private String AV32TFContratoServicosIndicador_Vigencia ;
      private String AV33TFContratoServicosIndicador_Vigencia_Sel ;
      private String AV54DynamicFiltersSelector1 ;
      private String AV58DynamicFiltersSelector2 ;
      private String AV62DynamicFiltersSelector3 ;
      private String lV20TFContratoServicosIndicador_Indicador ;
      private String lV22TFContratoServicosIndicador_Finalidade ;
      private String lV24TFContratoServicosIndicador_Meta ;
      private String lV26TFContratoServicosIndicador_InstrumentoMedicao ;
      private String lV32TFContratoServicosIndicador_Vigencia ;
      private String A1310ContratoServicosIndicador_Vigencia ;
      private String AV40Option ;
      private IGxSession AV49Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00QL3_A1274ContratoServicosIndicador_Indicador ;
      private String[] P00QL3_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] P00QL3_n1310ContratoServicosIndicador_Vigencia ;
      private String[] P00QL3_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] P00QL3_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] P00QL3_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P00QL3_n1308ContratoServicosIndicador_Tipo ;
      private String[] P00QL3_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] P00QL3_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] P00QL3_A1306ContratoServicosIndicador_Meta ;
      private bool[] P00QL3_n1306ContratoServicosIndicador_Meta ;
      private String[] P00QL3_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] P00QL3_n1305ContratoServicosIndicador_Finalidade ;
      private short[] P00QL3_A1271ContratoServicosIndicador_Numero ;
      private int[] P00QL3_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] P00QL3_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int[] P00QL3_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] P00QL3_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] P00QL3_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] P00QL3_A1269ContratoServicosIndicador_Codigo ;
      private short[] P00QL3_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] P00QL3_n1298ContratoServicosIndicador_QtdeFaixas ;
      private String[] P00QL5_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] P00QL5_n1305ContratoServicosIndicador_Finalidade ;
      private String[] P00QL5_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] P00QL5_n1310ContratoServicosIndicador_Vigencia ;
      private String[] P00QL5_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] P00QL5_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] P00QL5_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P00QL5_n1308ContratoServicosIndicador_Tipo ;
      private String[] P00QL5_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] P00QL5_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] P00QL5_A1306ContratoServicosIndicador_Meta ;
      private bool[] P00QL5_n1306ContratoServicosIndicador_Meta ;
      private short[] P00QL5_A1271ContratoServicosIndicador_Numero ;
      private int[] P00QL5_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] P00QL5_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int[] P00QL5_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] P00QL5_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] P00QL5_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] P00QL5_A1269ContratoServicosIndicador_Codigo ;
      private String[] P00QL5_A1274ContratoServicosIndicador_Indicador ;
      private short[] P00QL5_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] P00QL5_n1298ContratoServicosIndicador_QtdeFaixas ;
      private String[] P00QL7_A1306ContratoServicosIndicador_Meta ;
      private bool[] P00QL7_n1306ContratoServicosIndicador_Meta ;
      private String[] P00QL7_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] P00QL7_n1310ContratoServicosIndicador_Vigencia ;
      private String[] P00QL7_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] P00QL7_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] P00QL7_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P00QL7_n1308ContratoServicosIndicador_Tipo ;
      private String[] P00QL7_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] P00QL7_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] P00QL7_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] P00QL7_n1305ContratoServicosIndicador_Finalidade ;
      private short[] P00QL7_A1271ContratoServicosIndicador_Numero ;
      private int[] P00QL7_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] P00QL7_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int[] P00QL7_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] P00QL7_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] P00QL7_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] P00QL7_A1269ContratoServicosIndicador_Codigo ;
      private String[] P00QL7_A1274ContratoServicosIndicador_Indicador ;
      private short[] P00QL7_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] P00QL7_n1298ContratoServicosIndicador_QtdeFaixas ;
      private String[] P00QL9_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] P00QL9_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] P00QL9_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] P00QL9_n1310ContratoServicosIndicador_Vigencia ;
      private String[] P00QL9_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] P00QL9_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] P00QL9_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P00QL9_n1308ContratoServicosIndicador_Tipo ;
      private String[] P00QL9_A1306ContratoServicosIndicador_Meta ;
      private bool[] P00QL9_n1306ContratoServicosIndicador_Meta ;
      private String[] P00QL9_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] P00QL9_n1305ContratoServicosIndicador_Finalidade ;
      private short[] P00QL9_A1271ContratoServicosIndicador_Numero ;
      private int[] P00QL9_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] P00QL9_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int[] P00QL9_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] P00QL9_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] P00QL9_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] P00QL9_A1269ContratoServicosIndicador_Codigo ;
      private String[] P00QL9_A1274ContratoServicosIndicador_Indicador ;
      private short[] P00QL9_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] P00QL9_n1298ContratoServicosIndicador_QtdeFaixas ;
      private String[] P00QL11_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] P00QL11_n1310ContratoServicosIndicador_Vigencia ;
      private String[] P00QL11_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] P00QL11_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] P00QL11_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P00QL11_n1308ContratoServicosIndicador_Tipo ;
      private String[] P00QL11_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] P00QL11_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] P00QL11_A1306ContratoServicosIndicador_Meta ;
      private bool[] P00QL11_n1306ContratoServicosIndicador_Meta ;
      private String[] P00QL11_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] P00QL11_n1305ContratoServicosIndicador_Finalidade ;
      private short[] P00QL11_A1271ContratoServicosIndicador_Numero ;
      private int[] P00QL11_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] P00QL11_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int[] P00QL11_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] P00QL11_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] P00QL11_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] P00QL11_A1269ContratoServicosIndicador_Codigo ;
      private String[] P00QL11_A1274ContratoServicosIndicador_Indicador ;
      private short[] P00QL11_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] P00QL11_n1298ContratoServicosIndicador_QtdeFaixas ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29TFContratoServicosIndicador_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31TFContratoServicosIndicador_Periodicidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV41Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV44OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV46OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV51GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV52GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV53GridStateDynamicFilter ;
   }

   public class getpromptcontratoservicosindicadorfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00QL3( IGxContext context ,
                                             String A1308ContratoServicosIndicador_Tipo ,
                                             IGxCollection AV29TFContratoServicosIndicador_Tipo_Sels ,
                                             String A1309ContratoServicosIndicador_Periodicidade ,
                                             IGxCollection AV31TFContratoServicosIndicador_Periodicidade_Sels ,
                                             String AV54DynamicFiltersSelector1 ,
                                             short AV55DynamicFiltersOperator1 ,
                                             String AV56ContratoServicosIndicador_Indicador1 ,
                                             bool AV57DynamicFiltersEnabled2 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             short AV59DynamicFiltersOperator2 ,
                                             String AV60ContratoServicosIndicador_Indicador2 ,
                                             bool AV61DynamicFiltersEnabled3 ,
                                             String AV62DynamicFiltersSelector3 ,
                                             short AV63DynamicFiltersOperator3 ,
                                             String AV64ContratoServicosIndicador_Indicador3 ,
                                             int AV10TFContratoServicosIndicador_Codigo ,
                                             int AV11TFContratoServicosIndicador_Codigo_To ,
                                             int AV12TFContratoServicosIndicador_CntSrvCod ,
                                             int AV13TFContratoServicosIndicador_CntSrvCod_To ,
                                             int AV14TFContratoServicosIndicador_ContratoCod ,
                                             int AV15TFContratoServicosIndicador_ContratoCod_To ,
                                             int AV16TFContratoServicosIndicador_AreaTrabalhoCod ,
                                             int AV17TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                             short AV18TFContratoServicosIndicador_Numero ,
                                             short AV19TFContratoServicosIndicador_Numero_To ,
                                             String AV21TFContratoServicosIndicador_Indicador_Sel ,
                                             String AV20TFContratoServicosIndicador_Indicador ,
                                             String AV23TFContratoServicosIndicador_Finalidade_Sel ,
                                             String AV22TFContratoServicosIndicador_Finalidade ,
                                             String AV25TFContratoServicosIndicador_Meta_Sel ,
                                             String AV24TFContratoServicosIndicador_Meta ,
                                             String AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                             String AV26TFContratoServicosIndicador_InstrumentoMedicao ,
                                             int AV29TFContratoServicosIndicador_Tipo_Sels_Count ,
                                             int AV31TFContratoServicosIndicador_Periodicidade_Sels_Count ,
                                             String AV33TFContratoServicosIndicador_Vigencia_Sel ,
                                             String AV32TFContratoServicosIndicador_Vigencia ,
                                             String A1274ContratoServicosIndicador_Indicador ,
                                             int A1269ContratoServicosIndicador_Codigo ,
                                             int A1270ContratoServicosIndicador_CntSrvCod ,
                                             int A1296ContratoServicosIndicador_ContratoCod ,
                                             int A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                             short A1271ContratoServicosIndicador_Numero ,
                                             String A1305ContratoServicosIndicador_Finalidade ,
                                             String A1306ContratoServicosIndicador_Meta ,
                                             String A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                             String A1310ContratoServicosIndicador_Vigencia ,
                                             short AV34TFContratoServicosIndicador_QtdeFaixas ,
                                             short A1298ContratoServicosIndicador_QtdeFaixas ,
                                             short AV35TFContratoServicosIndicador_QtdeFaixas_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [30] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosIndicador_Indicador], T1.[ContratoServicosIndicador_Vigencia], T1.[ContratoServicosIndicador_Periodicidade], T1.[ContratoServicosIndicador_Tipo], T1.[ContratoServicosIndicador_InstrumentoMedicao], T1.[ContratoServicosIndicador_Meta], T1.[ContratoServicosIndicador_Finalidade], T1.[ContratoServicosIndicador_Numero], T3.[Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod, T2.[Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod, T1.[ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod, T1.[ContratoServicosIndicador_Codigo], COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas FROM ((([ContratoServicosIndicador] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosIndicador_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T4 ON T4.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         scmdbuf = scmdbuf + " WHERE ((@AV34TFContratoServicosIndicador_QtdeFaixas = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) >= @AV34TFContratoServicosIndicador_QtdeFaixas))";
         scmdbuf = scmdbuf + " and ((@AV35TFContratoServicosIndicador_QtdeFaixas_To = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) <= @AV35TFContratoServicosIndicador_QtdeFaixas_To))";
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV55DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV56ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV55DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV56ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV59DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV60ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV59DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV60ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV63DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV64ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV63DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV64ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (0==AV10TFContratoServicosIndicador_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] >= @AV10TFContratoServicosIndicador_Codigo)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (0==AV11TFContratoServicosIndicador_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] <= @AV11TFContratoServicosIndicador_Codigo_To)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV12TFContratoServicosIndicador_CntSrvCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] >= @AV12TFContratoServicosIndicador_CntSrvCod)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV13TFContratoServicosIndicador_CntSrvCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] <= @AV13TFContratoServicosIndicador_CntSrvCod_To)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV14TFContratoServicosIndicador_ContratoCod) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] >= @AV14TFContratoServicosIndicador_ContratoCod)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV15TFContratoServicosIndicador_ContratoCod_To) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] <= @AV15TFContratoServicosIndicador_ContratoCod_To)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (0==AV16TFContratoServicosIndicador_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] >= @AV16TFContratoServicosIndicador_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (0==AV17TFContratoServicosIndicador_AreaTrabalhoCod_To) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] <= @AV17TFContratoServicosIndicador_AreaTrabalhoCod_To)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (0==AV18TFContratoServicosIndicador_Numero) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] >= @AV18TFContratoServicosIndicador_Numero)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (0==AV19TFContratoServicosIndicador_Numero_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] <= @AV19TFContratoServicosIndicador_Numero_To)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosIndicador_Indicador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoServicosIndicador_Indicador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV20TFContratoServicosIndicador_Indicador)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosIndicador_Indicador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] = @AV21TFContratoServicosIndicador_Indicador_Sel)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosIndicador_Finalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoServicosIndicador_Finalidade)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] like @lV22TFContratoServicosIndicador_Finalidade)";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosIndicador_Finalidade_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] = @AV23TFContratoServicosIndicador_Finalidade_Sel)";
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoServicosIndicador_Meta_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoServicosIndicador_Meta)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] like @lV24TFContratoServicosIndicador_Meta)";
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoServicosIndicador_Meta_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] = @AV25TFContratoServicosIndicador_Meta_Sel)";
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContratoServicosIndicador_InstrumentoMedicao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] like @lV26TFContratoServicosIndicador_InstrumentoMedicao)";
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] = @AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)";
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( AV29TFContratoServicosIndicador_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV29TFContratoServicosIndicador_Tipo_Sels, "T1.[ContratoServicosIndicador_Tipo] IN (", ")") + ")";
         }
         if ( AV31TFContratoServicosIndicador_Periodicidade_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV31TFContratoServicosIndicador_Periodicidade_Sels, "T1.[ContratoServicosIndicador_Periodicidade] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Vigencia_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoServicosIndicador_Vigencia)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] like @lV32TFContratoServicosIndicador_Vigencia)";
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Vigencia_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] = @AV33TFContratoServicosIndicador_Vigencia_Sel)";
         }
         else
         {
            GXv_int1[29] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosIndicador_Indicador]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00QL5( IGxContext context ,
                                             String A1308ContratoServicosIndicador_Tipo ,
                                             IGxCollection AV29TFContratoServicosIndicador_Tipo_Sels ,
                                             String A1309ContratoServicosIndicador_Periodicidade ,
                                             IGxCollection AV31TFContratoServicosIndicador_Periodicidade_Sels ,
                                             String AV54DynamicFiltersSelector1 ,
                                             short AV55DynamicFiltersOperator1 ,
                                             String AV56ContratoServicosIndicador_Indicador1 ,
                                             bool AV57DynamicFiltersEnabled2 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             short AV59DynamicFiltersOperator2 ,
                                             String AV60ContratoServicosIndicador_Indicador2 ,
                                             bool AV61DynamicFiltersEnabled3 ,
                                             String AV62DynamicFiltersSelector3 ,
                                             short AV63DynamicFiltersOperator3 ,
                                             String AV64ContratoServicosIndicador_Indicador3 ,
                                             int AV10TFContratoServicosIndicador_Codigo ,
                                             int AV11TFContratoServicosIndicador_Codigo_To ,
                                             int AV12TFContratoServicosIndicador_CntSrvCod ,
                                             int AV13TFContratoServicosIndicador_CntSrvCod_To ,
                                             int AV14TFContratoServicosIndicador_ContratoCod ,
                                             int AV15TFContratoServicosIndicador_ContratoCod_To ,
                                             int AV16TFContratoServicosIndicador_AreaTrabalhoCod ,
                                             int AV17TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                             short AV18TFContratoServicosIndicador_Numero ,
                                             short AV19TFContratoServicosIndicador_Numero_To ,
                                             String AV21TFContratoServicosIndicador_Indicador_Sel ,
                                             String AV20TFContratoServicosIndicador_Indicador ,
                                             String AV23TFContratoServicosIndicador_Finalidade_Sel ,
                                             String AV22TFContratoServicosIndicador_Finalidade ,
                                             String AV25TFContratoServicosIndicador_Meta_Sel ,
                                             String AV24TFContratoServicosIndicador_Meta ,
                                             String AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                             String AV26TFContratoServicosIndicador_InstrumentoMedicao ,
                                             int AV29TFContratoServicosIndicador_Tipo_Sels_Count ,
                                             int AV31TFContratoServicosIndicador_Periodicidade_Sels_Count ,
                                             String AV33TFContratoServicosIndicador_Vigencia_Sel ,
                                             String AV32TFContratoServicosIndicador_Vigencia ,
                                             String A1274ContratoServicosIndicador_Indicador ,
                                             int A1269ContratoServicosIndicador_Codigo ,
                                             int A1270ContratoServicosIndicador_CntSrvCod ,
                                             int A1296ContratoServicosIndicador_ContratoCod ,
                                             int A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                             short A1271ContratoServicosIndicador_Numero ,
                                             String A1305ContratoServicosIndicador_Finalidade ,
                                             String A1306ContratoServicosIndicador_Meta ,
                                             String A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                             String A1310ContratoServicosIndicador_Vigencia ,
                                             short AV34TFContratoServicosIndicador_QtdeFaixas ,
                                             short A1298ContratoServicosIndicador_QtdeFaixas ,
                                             short AV35TFContratoServicosIndicador_QtdeFaixas_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [30] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosIndicador_Finalidade], T1.[ContratoServicosIndicador_Vigencia], T1.[ContratoServicosIndicador_Periodicidade], T1.[ContratoServicosIndicador_Tipo], T1.[ContratoServicosIndicador_InstrumentoMedicao], T1.[ContratoServicosIndicador_Meta], T1.[ContratoServicosIndicador_Numero], T3.[Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod, T2.[Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod, T1.[ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod, T1.[ContratoServicosIndicador_Codigo], T1.[ContratoServicosIndicador_Indicador], COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas FROM ((([ContratoServicosIndicador] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosIndicador_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T4 ON T4.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         scmdbuf = scmdbuf + " WHERE ((@AV34TFContratoServicosIndicador_QtdeFaixas = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) >= @AV34TFContratoServicosIndicador_QtdeFaixas))";
         scmdbuf = scmdbuf + " and ((@AV35TFContratoServicosIndicador_QtdeFaixas_To = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) <= @AV35TFContratoServicosIndicador_QtdeFaixas_To))";
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV55DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV56ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV55DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV56ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV59DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV60ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV59DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV60ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV63DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV64ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV63DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV64ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (0==AV10TFContratoServicosIndicador_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] >= @AV10TFContratoServicosIndicador_Codigo)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (0==AV11TFContratoServicosIndicador_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] <= @AV11TFContratoServicosIndicador_Codigo_To)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV12TFContratoServicosIndicador_CntSrvCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] >= @AV12TFContratoServicosIndicador_CntSrvCod)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV13TFContratoServicosIndicador_CntSrvCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] <= @AV13TFContratoServicosIndicador_CntSrvCod_To)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV14TFContratoServicosIndicador_ContratoCod) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] >= @AV14TFContratoServicosIndicador_ContratoCod)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV15TFContratoServicosIndicador_ContratoCod_To) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] <= @AV15TFContratoServicosIndicador_ContratoCod_To)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (0==AV16TFContratoServicosIndicador_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] >= @AV16TFContratoServicosIndicador_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (0==AV17TFContratoServicosIndicador_AreaTrabalhoCod_To) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] <= @AV17TFContratoServicosIndicador_AreaTrabalhoCod_To)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (0==AV18TFContratoServicosIndicador_Numero) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] >= @AV18TFContratoServicosIndicador_Numero)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (0==AV19TFContratoServicosIndicador_Numero_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] <= @AV19TFContratoServicosIndicador_Numero_To)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosIndicador_Indicador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoServicosIndicador_Indicador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV20TFContratoServicosIndicador_Indicador)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosIndicador_Indicador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] = @AV21TFContratoServicosIndicador_Indicador_Sel)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosIndicador_Finalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoServicosIndicador_Finalidade)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] like @lV22TFContratoServicosIndicador_Finalidade)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosIndicador_Finalidade_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] = @AV23TFContratoServicosIndicador_Finalidade_Sel)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoServicosIndicador_Meta_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoServicosIndicador_Meta)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] like @lV24TFContratoServicosIndicador_Meta)";
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoServicosIndicador_Meta_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] = @AV25TFContratoServicosIndicador_Meta_Sel)";
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContratoServicosIndicador_InstrumentoMedicao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] like @lV26TFContratoServicosIndicador_InstrumentoMedicao)";
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] = @AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)";
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( AV29TFContratoServicosIndicador_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV29TFContratoServicosIndicador_Tipo_Sels, "T1.[ContratoServicosIndicador_Tipo] IN (", ")") + ")";
         }
         if ( AV31TFContratoServicosIndicador_Periodicidade_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV31TFContratoServicosIndicador_Periodicidade_Sels, "T1.[ContratoServicosIndicador_Periodicidade] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Vigencia_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoServicosIndicador_Vigencia)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] like @lV32TFContratoServicosIndicador_Vigencia)";
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Vigencia_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] = @AV33TFContratoServicosIndicador_Vigencia_Sel)";
         }
         else
         {
            GXv_int3[29] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosIndicador_Finalidade]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00QL7( IGxContext context ,
                                             String A1308ContratoServicosIndicador_Tipo ,
                                             IGxCollection AV29TFContratoServicosIndicador_Tipo_Sels ,
                                             String A1309ContratoServicosIndicador_Periodicidade ,
                                             IGxCollection AV31TFContratoServicosIndicador_Periodicidade_Sels ,
                                             String AV54DynamicFiltersSelector1 ,
                                             short AV55DynamicFiltersOperator1 ,
                                             String AV56ContratoServicosIndicador_Indicador1 ,
                                             bool AV57DynamicFiltersEnabled2 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             short AV59DynamicFiltersOperator2 ,
                                             String AV60ContratoServicosIndicador_Indicador2 ,
                                             bool AV61DynamicFiltersEnabled3 ,
                                             String AV62DynamicFiltersSelector3 ,
                                             short AV63DynamicFiltersOperator3 ,
                                             String AV64ContratoServicosIndicador_Indicador3 ,
                                             int AV10TFContratoServicosIndicador_Codigo ,
                                             int AV11TFContratoServicosIndicador_Codigo_To ,
                                             int AV12TFContratoServicosIndicador_CntSrvCod ,
                                             int AV13TFContratoServicosIndicador_CntSrvCod_To ,
                                             int AV14TFContratoServicosIndicador_ContratoCod ,
                                             int AV15TFContratoServicosIndicador_ContratoCod_To ,
                                             int AV16TFContratoServicosIndicador_AreaTrabalhoCod ,
                                             int AV17TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                             short AV18TFContratoServicosIndicador_Numero ,
                                             short AV19TFContratoServicosIndicador_Numero_To ,
                                             String AV21TFContratoServicosIndicador_Indicador_Sel ,
                                             String AV20TFContratoServicosIndicador_Indicador ,
                                             String AV23TFContratoServicosIndicador_Finalidade_Sel ,
                                             String AV22TFContratoServicosIndicador_Finalidade ,
                                             String AV25TFContratoServicosIndicador_Meta_Sel ,
                                             String AV24TFContratoServicosIndicador_Meta ,
                                             String AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                             String AV26TFContratoServicosIndicador_InstrumentoMedicao ,
                                             int AV29TFContratoServicosIndicador_Tipo_Sels_Count ,
                                             int AV31TFContratoServicosIndicador_Periodicidade_Sels_Count ,
                                             String AV33TFContratoServicosIndicador_Vigencia_Sel ,
                                             String AV32TFContratoServicosIndicador_Vigencia ,
                                             String A1274ContratoServicosIndicador_Indicador ,
                                             int A1269ContratoServicosIndicador_Codigo ,
                                             int A1270ContratoServicosIndicador_CntSrvCod ,
                                             int A1296ContratoServicosIndicador_ContratoCod ,
                                             int A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                             short A1271ContratoServicosIndicador_Numero ,
                                             String A1305ContratoServicosIndicador_Finalidade ,
                                             String A1306ContratoServicosIndicador_Meta ,
                                             String A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                             String A1310ContratoServicosIndicador_Vigencia ,
                                             short AV34TFContratoServicosIndicador_QtdeFaixas ,
                                             short A1298ContratoServicosIndicador_QtdeFaixas ,
                                             short AV35TFContratoServicosIndicador_QtdeFaixas_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [30] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosIndicador_Meta], T1.[ContratoServicosIndicador_Vigencia], T1.[ContratoServicosIndicador_Periodicidade], T1.[ContratoServicosIndicador_Tipo], T1.[ContratoServicosIndicador_InstrumentoMedicao], T1.[ContratoServicosIndicador_Finalidade], T1.[ContratoServicosIndicador_Numero], T3.[Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod, T2.[Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod, T1.[ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod, T1.[ContratoServicosIndicador_Codigo], T1.[ContratoServicosIndicador_Indicador], COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas FROM ((([ContratoServicosIndicador] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosIndicador_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T4 ON T4.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         scmdbuf = scmdbuf + " WHERE ((@AV34TFContratoServicosIndicador_QtdeFaixas = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) >= @AV34TFContratoServicosIndicador_QtdeFaixas))";
         scmdbuf = scmdbuf + " and ((@AV35TFContratoServicosIndicador_QtdeFaixas_To = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) <= @AV35TFContratoServicosIndicador_QtdeFaixas_To))";
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV55DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV56ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV55DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV56ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV59DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV60ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV59DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV60ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV63DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV64ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV63DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV64ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! (0==AV10TFContratoServicosIndicador_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] >= @AV10TFContratoServicosIndicador_Codigo)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! (0==AV11TFContratoServicosIndicador_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] <= @AV11TFContratoServicosIndicador_Codigo_To)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV12TFContratoServicosIndicador_CntSrvCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] >= @AV12TFContratoServicosIndicador_CntSrvCod)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (0==AV13TFContratoServicosIndicador_CntSrvCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] <= @AV13TFContratoServicosIndicador_CntSrvCod_To)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (0==AV14TFContratoServicosIndicador_ContratoCod) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] >= @AV14TFContratoServicosIndicador_ContratoCod)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV15TFContratoServicosIndicador_ContratoCod_To) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] <= @AV15TFContratoServicosIndicador_ContratoCod_To)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (0==AV16TFContratoServicosIndicador_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] >= @AV16TFContratoServicosIndicador_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (0==AV17TFContratoServicosIndicador_AreaTrabalhoCod_To) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] <= @AV17TFContratoServicosIndicador_AreaTrabalhoCod_To)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (0==AV18TFContratoServicosIndicador_Numero) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] >= @AV18TFContratoServicosIndicador_Numero)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (0==AV19TFContratoServicosIndicador_Numero_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] <= @AV19TFContratoServicosIndicador_Numero_To)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosIndicador_Indicador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoServicosIndicador_Indicador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV20TFContratoServicosIndicador_Indicador)";
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosIndicador_Indicador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] = @AV21TFContratoServicosIndicador_Indicador_Sel)";
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosIndicador_Finalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoServicosIndicador_Finalidade)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] like @lV22TFContratoServicosIndicador_Finalidade)";
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosIndicador_Finalidade_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] = @AV23TFContratoServicosIndicador_Finalidade_Sel)";
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoServicosIndicador_Meta_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoServicosIndicador_Meta)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] like @lV24TFContratoServicosIndicador_Meta)";
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoServicosIndicador_Meta_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] = @AV25TFContratoServicosIndicador_Meta_Sel)";
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContratoServicosIndicador_InstrumentoMedicao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] like @lV26TFContratoServicosIndicador_InstrumentoMedicao)";
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] = @AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)";
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( AV29TFContratoServicosIndicador_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV29TFContratoServicosIndicador_Tipo_Sels, "T1.[ContratoServicosIndicador_Tipo] IN (", ")") + ")";
         }
         if ( AV31TFContratoServicosIndicador_Periodicidade_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV31TFContratoServicosIndicador_Periodicidade_Sels, "T1.[ContratoServicosIndicador_Periodicidade] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Vigencia_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoServicosIndicador_Vigencia)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] like @lV32TFContratoServicosIndicador_Vigencia)";
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Vigencia_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] = @AV33TFContratoServicosIndicador_Vigencia_Sel)";
         }
         else
         {
            GXv_int5[29] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosIndicador_Meta]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00QL9( IGxContext context ,
                                             String A1308ContratoServicosIndicador_Tipo ,
                                             IGxCollection AV29TFContratoServicosIndicador_Tipo_Sels ,
                                             String A1309ContratoServicosIndicador_Periodicidade ,
                                             IGxCollection AV31TFContratoServicosIndicador_Periodicidade_Sels ,
                                             String AV54DynamicFiltersSelector1 ,
                                             short AV55DynamicFiltersOperator1 ,
                                             String AV56ContratoServicosIndicador_Indicador1 ,
                                             bool AV57DynamicFiltersEnabled2 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             short AV59DynamicFiltersOperator2 ,
                                             String AV60ContratoServicosIndicador_Indicador2 ,
                                             bool AV61DynamicFiltersEnabled3 ,
                                             String AV62DynamicFiltersSelector3 ,
                                             short AV63DynamicFiltersOperator3 ,
                                             String AV64ContratoServicosIndicador_Indicador3 ,
                                             int AV10TFContratoServicosIndicador_Codigo ,
                                             int AV11TFContratoServicosIndicador_Codigo_To ,
                                             int AV12TFContratoServicosIndicador_CntSrvCod ,
                                             int AV13TFContratoServicosIndicador_CntSrvCod_To ,
                                             int AV14TFContratoServicosIndicador_ContratoCod ,
                                             int AV15TFContratoServicosIndicador_ContratoCod_To ,
                                             int AV16TFContratoServicosIndicador_AreaTrabalhoCod ,
                                             int AV17TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                             short AV18TFContratoServicosIndicador_Numero ,
                                             short AV19TFContratoServicosIndicador_Numero_To ,
                                             String AV21TFContratoServicosIndicador_Indicador_Sel ,
                                             String AV20TFContratoServicosIndicador_Indicador ,
                                             String AV23TFContratoServicosIndicador_Finalidade_Sel ,
                                             String AV22TFContratoServicosIndicador_Finalidade ,
                                             String AV25TFContratoServicosIndicador_Meta_Sel ,
                                             String AV24TFContratoServicosIndicador_Meta ,
                                             String AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                             String AV26TFContratoServicosIndicador_InstrumentoMedicao ,
                                             int AV29TFContratoServicosIndicador_Tipo_Sels_Count ,
                                             int AV31TFContratoServicosIndicador_Periodicidade_Sels_Count ,
                                             String AV33TFContratoServicosIndicador_Vigencia_Sel ,
                                             String AV32TFContratoServicosIndicador_Vigencia ,
                                             String A1274ContratoServicosIndicador_Indicador ,
                                             int A1269ContratoServicosIndicador_Codigo ,
                                             int A1270ContratoServicosIndicador_CntSrvCod ,
                                             int A1296ContratoServicosIndicador_ContratoCod ,
                                             int A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                             short A1271ContratoServicosIndicador_Numero ,
                                             String A1305ContratoServicosIndicador_Finalidade ,
                                             String A1306ContratoServicosIndicador_Meta ,
                                             String A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                             String A1310ContratoServicosIndicador_Vigencia ,
                                             short AV34TFContratoServicosIndicador_QtdeFaixas ,
                                             short A1298ContratoServicosIndicador_QtdeFaixas ,
                                             short AV35TFContratoServicosIndicador_QtdeFaixas_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [30] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosIndicador_InstrumentoMedicao], T1.[ContratoServicosIndicador_Vigencia], T1.[ContratoServicosIndicador_Periodicidade], T1.[ContratoServicosIndicador_Tipo], T1.[ContratoServicosIndicador_Meta], T1.[ContratoServicosIndicador_Finalidade], T1.[ContratoServicosIndicador_Numero], T3.[Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod, T2.[Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod, T1.[ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod, T1.[ContratoServicosIndicador_Codigo], T1.[ContratoServicosIndicador_Indicador], COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas FROM ((([ContratoServicosIndicador] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosIndicador_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T4 ON T4.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         scmdbuf = scmdbuf + " WHERE ((@AV34TFContratoServicosIndicador_QtdeFaixas = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) >= @AV34TFContratoServicosIndicador_QtdeFaixas))";
         scmdbuf = scmdbuf + " and ((@AV35TFContratoServicosIndicador_QtdeFaixas_To = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) <= @AV35TFContratoServicosIndicador_QtdeFaixas_To))";
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV55DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV56ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV55DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV56ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV59DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV60ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV59DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV60ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV63DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV64ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV63DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV64ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ! (0==AV10TFContratoServicosIndicador_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] >= @AV10TFContratoServicosIndicador_Codigo)";
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( ! (0==AV11TFContratoServicosIndicador_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] <= @AV11TFContratoServicosIndicador_Codigo_To)";
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! (0==AV12TFContratoServicosIndicador_CntSrvCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] >= @AV12TFContratoServicosIndicador_CntSrvCod)";
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( ! (0==AV13TFContratoServicosIndicador_CntSrvCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] <= @AV13TFContratoServicosIndicador_CntSrvCod_To)";
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! (0==AV14TFContratoServicosIndicador_ContratoCod) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] >= @AV14TFContratoServicosIndicador_ContratoCod)";
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ! (0==AV15TFContratoServicosIndicador_ContratoCod_To) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] <= @AV15TFContratoServicosIndicador_ContratoCod_To)";
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! (0==AV16TFContratoServicosIndicador_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] >= @AV16TFContratoServicosIndicador_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( ! (0==AV17TFContratoServicosIndicador_AreaTrabalhoCod_To) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] <= @AV17TFContratoServicosIndicador_AreaTrabalhoCod_To)";
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( ! (0==AV18TFContratoServicosIndicador_Numero) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] >= @AV18TFContratoServicosIndicador_Numero)";
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! (0==AV19TFContratoServicosIndicador_Numero_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] <= @AV19TFContratoServicosIndicador_Numero_To)";
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosIndicador_Indicador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoServicosIndicador_Indicador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV20TFContratoServicosIndicador_Indicador)";
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosIndicador_Indicador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] = @AV21TFContratoServicosIndicador_Indicador_Sel)";
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosIndicador_Finalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoServicosIndicador_Finalidade)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] like @lV22TFContratoServicosIndicador_Finalidade)";
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosIndicador_Finalidade_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] = @AV23TFContratoServicosIndicador_Finalidade_Sel)";
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoServicosIndicador_Meta_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoServicosIndicador_Meta)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] like @lV24TFContratoServicosIndicador_Meta)";
         }
         else
         {
            GXv_int7[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoServicosIndicador_Meta_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] = @AV25TFContratoServicosIndicador_Meta_Sel)";
         }
         else
         {
            GXv_int7[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContratoServicosIndicador_InstrumentoMedicao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] like @lV26TFContratoServicosIndicador_InstrumentoMedicao)";
         }
         else
         {
            GXv_int7[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] = @AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)";
         }
         else
         {
            GXv_int7[27] = 1;
         }
         if ( AV29TFContratoServicosIndicador_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV29TFContratoServicosIndicador_Tipo_Sels, "T1.[ContratoServicosIndicador_Tipo] IN (", ")") + ")";
         }
         if ( AV31TFContratoServicosIndicador_Periodicidade_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV31TFContratoServicosIndicador_Periodicidade_Sels, "T1.[ContratoServicosIndicador_Periodicidade] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Vigencia_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoServicosIndicador_Vigencia)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] like @lV32TFContratoServicosIndicador_Vigencia)";
         }
         else
         {
            GXv_int7[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Vigencia_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] = @AV33TFContratoServicosIndicador_Vigencia_Sel)";
         }
         else
         {
            GXv_int7[29] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosIndicador_InstrumentoMedicao]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00QL11( IGxContext context ,
                                              String A1308ContratoServicosIndicador_Tipo ,
                                              IGxCollection AV29TFContratoServicosIndicador_Tipo_Sels ,
                                              String A1309ContratoServicosIndicador_Periodicidade ,
                                              IGxCollection AV31TFContratoServicosIndicador_Periodicidade_Sels ,
                                              String AV54DynamicFiltersSelector1 ,
                                              short AV55DynamicFiltersOperator1 ,
                                              String AV56ContratoServicosIndicador_Indicador1 ,
                                              bool AV57DynamicFiltersEnabled2 ,
                                              String AV58DynamicFiltersSelector2 ,
                                              short AV59DynamicFiltersOperator2 ,
                                              String AV60ContratoServicosIndicador_Indicador2 ,
                                              bool AV61DynamicFiltersEnabled3 ,
                                              String AV62DynamicFiltersSelector3 ,
                                              short AV63DynamicFiltersOperator3 ,
                                              String AV64ContratoServicosIndicador_Indicador3 ,
                                              int AV10TFContratoServicosIndicador_Codigo ,
                                              int AV11TFContratoServicosIndicador_Codigo_To ,
                                              int AV12TFContratoServicosIndicador_CntSrvCod ,
                                              int AV13TFContratoServicosIndicador_CntSrvCod_To ,
                                              int AV14TFContratoServicosIndicador_ContratoCod ,
                                              int AV15TFContratoServicosIndicador_ContratoCod_To ,
                                              int AV16TFContratoServicosIndicador_AreaTrabalhoCod ,
                                              int AV17TFContratoServicosIndicador_AreaTrabalhoCod_To ,
                                              short AV18TFContratoServicosIndicador_Numero ,
                                              short AV19TFContratoServicosIndicador_Numero_To ,
                                              String AV21TFContratoServicosIndicador_Indicador_Sel ,
                                              String AV20TFContratoServicosIndicador_Indicador ,
                                              String AV23TFContratoServicosIndicador_Finalidade_Sel ,
                                              String AV22TFContratoServicosIndicador_Finalidade ,
                                              String AV25TFContratoServicosIndicador_Meta_Sel ,
                                              String AV24TFContratoServicosIndicador_Meta ,
                                              String AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel ,
                                              String AV26TFContratoServicosIndicador_InstrumentoMedicao ,
                                              int AV29TFContratoServicosIndicador_Tipo_Sels_Count ,
                                              int AV31TFContratoServicosIndicador_Periodicidade_Sels_Count ,
                                              String AV33TFContratoServicosIndicador_Vigencia_Sel ,
                                              String AV32TFContratoServicosIndicador_Vigencia ,
                                              String A1274ContratoServicosIndicador_Indicador ,
                                              int A1269ContratoServicosIndicador_Codigo ,
                                              int A1270ContratoServicosIndicador_CntSrvCod ,
                                              int A1296ContratoServicosIndicador_ContratoCod ,
                                              int A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                              short A1271ContratoServicosIndicador_Numero ,
                                              String A1305ContratoServicosIndicador_Finalidade ,
                                              String A1306ContratoServicosIndicador_Meta ,
                                              String A1307ContratoServicosIndicador_InstrumentoMedicao ,
                                              String A1310ContratoServicosIndicador_Vigencia ,
                                              short AV34TFContratoServicosIndicador_QtdeFaixas ,
                                              short A1298ContratoServicosIndicador_QtdeFaixas ,
                                              short AV35TFContratoServicosIndicador_QtdeFaixas_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [30] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosIndicador_Vigencia], T1.[ContratoServicosIndicador_Periodicidade], T1.[ContratoServicosIndicador_Tipo], T1.[ContratoServicosIndicador_InstrumentoMedicao], T1.[ContratoServicosIndicador_Meta], T1.[ContratoServicosIndicador_Finalidade], T1.[ContratoServicosIndicador_Numero], T3.[Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod, T2.[Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod, T1.[ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod, T1.[ContratoServicosIndicador_Codigo], T1.[ContratoServicosIndicador_Indicador], COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas FROM ((([ContratoServicosIndicador] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosIndicador_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T4 ON T4.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         scmdbuf = scmdbuf + " WHERE ((@AV34TFContratoServicosIndicador_QtdeFaixas = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) >= @AV34TFContratoServicosIndicador_QtdeFaixas))";
         scmdbuf = scmdbuf + " and ((@AV35TFContratoServicosIndicador_QtdeFaixas_To = convert(int, 0)) or ( COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) <= @AV35TFContratoServicosIndicador_QtdeFaixas_To))";
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV55DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV56ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int9[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV55DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoServicosIndicador_Indicador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV56ContratoServicosIndicador_Indicador1)";
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV59DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV60ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV59DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContratoServicosIndicador_Indicador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV60ContratoServicosIndicador_Indicador2)";
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV63DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV64ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 ) && ( AV63DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContratoServicosIndicador_Indicador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like '%' + @lV64ContratoServicosIndicador_Indicador3)";
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( ! (0==AV10TFContratoServicosIndicador_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] >= @AV10TFContratoServicosIndicador_Codigo)";
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( ! (0==AV11TFContratoServicosIndicador_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Codigo] <= @AV11TFContratoServicosIndicador_Codigo_To)";
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( ! (0==AV12TFContratoServicosIndicador_CntSrvCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] >= @AV12TFContratoServicosIndicador_CntSrvCod)";
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( ! (0==AV13TFContratoServicosIndicador_CntSrvCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_CntSrvCod] <= @AV13TFContratoServicosIndicador_CntSrvCod_To)";
         }
         else
         {
            GXv_int9[13] = 1;
         }
         if ( ! (0==AV14TFContratoServicosIndicador_ContratoCod) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] >= @AV14TFContratoServicosIndicador_ContratoCod)";
         }
         else
         {
            GXv_int9[14] = 1;
         }
         if ( ! (0==AV15TFContratoServicosIndicador_ContratoCod_To) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] <= @AV15TFContratoServicosIndicador_ContratoCod_To)";
         }
         else
         {
            GXv_int9[15] = 1;
         }
         if ( ! (0==AV16TFContratoServicosIndicador_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] >= @AV16TFContratoServicosIndicador_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( ! (0==AV17TFContratoServicosIndicador_AreaTrabalhoCod_To) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] <= @AV17TFContratoServicosIndicador_AreaTrabalhoCod_To)";
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( ! (0==AV18TFContratoServicosIndicador_Numero) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] >= @AV18TFContratoServicosIndicador_Numero)";
         }
         else
         {
            GXv_int9[18] = 1;
         }
         if ( ! (0==AV19TFContratoServicosIndicador_Numero_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] <= @AV19TFContratoServicosIndicador_Numero_To)";
         }
         else
         {
            GXv_int9[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosIndicador_Indicador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoServicosIndicador_Indicador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV20TFContratoServicosIndicador_Indicador)";
         }
         else
         {
            GXv_int9[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosIndicador_Indicador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] = @AV21TFContratoServicosIndicador_Indicador_Sel)";
         }
         else
         {
            GXv_int9[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosIndicador_Finalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoServicosIndicador_Finalidade)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] like @lV22TFContratoServicosIndicador_Finalidade)";
         }
         else
         {
            GXv_int9[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosIndicador_Finalidade_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Finalidade] = @AV23TFContratoServicosIndicador_Finalidade_Sel)";
         }
         else
         {
            GXv_int9[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoServicosIndicador_Meta_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoServicosIndicador_Meta)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] like @lV24TFContratoServicosIndicador_Meta)";
         }
         else
         {
            GXv_int9[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoServicosIndicador_Meta_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Meta] = @AV25TFContratoServicosIndicador_Meta_Sel)";
         }
         else
         {
            GXv_int9[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContratoServicosIndicador_InstrumentoMedicao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] like @lV26TFContratoServicosIndicador_InstrumentoMedicao)";
         }
         else
         {
            GXv_int9[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_InstrumentoMedicao] = @AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel)";
         }
         else
         {
            GXv_int9[27] = 1;
         }
         if ( AV29TFContratoServicosIndicador_Tipo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV29TFContratoServicosIndicador_Tipo_Sels, "T1.[ContratoServicosIndicador_Tipo] IN (", ")") + ")";
         }
         if ( AV31TFContratoServicosIndicador_Periodicidade_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV31TFContratoServicosIndicador_Periodicidade_Sels, "T1.[ContratoServicosIndicador_Periodicidade] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Vigencia_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoServicosIndicador_Vigencia)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] like @lV32TFContratoServicosIndicador_Vigencia)";
         }
         else
         {
            GXv_int9[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Vigencia_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Vigencia] = @AV33TFContratoServicosIndicador_Vigencia_Sel)";
         }
         else
         {
            GXv_int9[29] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosIndicador_Vigencia]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00QL3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (short)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (short)dynConstraints[48] , (short)dynConstraints[49] );
               case 1 :
                     return conditional_P00QL5(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (short)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (short)dynConstraints[48] , (short)dynConstraints[49] );
               case 2 :
                     return conditional_P00QL7(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (short)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (short)dynConstraints[48] , (short)dynConstraints[49] );
               case 3 :
                     return conditional_P00QL9(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (short)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (short)dynConstraints[48] , (short)dynConstraints[49] );
               case 4 :
                     return conditional_P00QL11(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (short)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (short)dynConstraints[48] , (short)dynConstraints[49] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00QL3 ;
          prmP00QL3 = new Object[] {
          new Object[] {"@AV34TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV34TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV56ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV60ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV60ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV64ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV64ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV10TFContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoServicosIndicador_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratoServicosIndicador_CntSrvCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContratoServicosIndicador_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContratoServicosIndicador_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContratoServicosIndicador_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratoServicosIndicador_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV19TFContratoServicosIndicador_Numero_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV20TFContratoServicosIndicador_Indicador",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV21TFContratoServicosIndicador_Indicador_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV22TFContratoServicosIndicador_Finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV23TFContratoServicosIndicador_Finalidade_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV24TFContratoServicosIndicador_Meta",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV25TFContratoServicosIndicador_Meta_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV26TFContratoServicosIndicador_InstrumentoMedicao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV32TFContratoServicosIndicador_Vigencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV33TFContratoServicosIndicador_Vigencia_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00QL5 ;
          prmP00QL5 = new Object[] {
          new Object[] {"@AV34TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV34TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV56ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV60ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV60ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV64ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV64ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV10TFContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoServicosIndicador_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratoServicosIndicador_CntSrvCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContratoServicosIndicador_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContratoServicosIndicador_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContratoServicosIndicador_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratoServicosIndicador_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV19TFContratoServicosIndicador_Numero_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV20TFContratoServicosIndicador_Indicador",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV21TFContratoServicosIndicador_Indicador_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV22TFContratoServicosIndicador_Finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV23TFContratoServicosIndicador_Finalidade_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV24TFContratoServicosIndicador_Meta",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV25TFContratoServicosIndicador_Meta_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV26TFContratoServicosIndicador_InstrumentoMedicao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV32TFContratoServicosIndicador_Vigencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV33TFContratoServicosIndicador_Vigencia_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00QL7 ;
          prmP00QL7 = new Object[] {
          new Object[] {"@AV34TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV34TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV56ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV60ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV60ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV64ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV64ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV10TFContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoServicosIndicador_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratoServicosIndicador_CntSrvCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContratoServicosIndicador_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContratoServicosIndicador_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContratoServicosIndicador_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratoServicosIndicador_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV19TFContratoServicosIndicador_Numero_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV20TFContratoServicosIndicador_Indicador",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV21TFContratoServicosIndicador_Indicador_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV22TFContratoServicosIndicador_Finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV23TFContratoServicosIndicador_Finalidade_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV24TFContratoServicosIndicador_Meta",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV25TFContratoServicosIndicador_Meta_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV26TFContratoServicosIndicador_InstrumentoMedicao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV32TFContratoServicosIndicador_Vigencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV33TFContratoServicosIndicador_Vigencia_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00QL9 ;
          prmP00QL9 = new Object[] {
          new Object[] {"@AV34TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV34TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV56ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV60ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV60ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV64ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV64ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV10TFContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoServicosIndicador_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratoServicosIndicador_CntSrvCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContratoServicosIndicador_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContratoServicosIndicador_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContratoServicosIndicador_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratoServicosIndicador_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV19TFContratoServicosIndicador_Numero_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV20TFContratoServicosIndicador_Indicador",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV21TFContratoServicosIndicador_Indicador_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV22TFContratoServicosIndicador_Finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV23TFContratoServicosIndicador_Finalidade_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV24TFContratoServicosIndicador_Meta",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV25TFContratoServicosIndicador_Meta_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV26TFContratoServicosIndicador_InstrumentoMedicao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV32TFContratoServicosIndicador_Vigencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV33TFContratoServicosIndicador_Vigencia_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00QL11 ;
          prmP00QL11 = new Object[] {
          new Object[] {"@AV34TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV34TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV56ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV56ContratoServicosIndicador_Indicador1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV60ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV60ContratoServicosIndicador_Indicador2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV64ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV64ContratoServicosIndicador_Indicador3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV10TFContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoServicosIndicador_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratoServicosIndicador_CntSrvCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContratoServicosIndicador_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContratoServicosIndicador_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFContratoServicosIndicador_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratoServicosIndicador_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV19TFContratoServicosIndicador_Numero_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV20TFContratoServicosIndicador_Indicador",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV21TFContratoServicosIndicador_Indicador_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV22TFContratoServicosIndicador_Finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV23TFContratoServicosIndicador_Finalidade_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV24TFContratoServicosIndicador_Meta",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV25TFContratoServicosIndicador_Meta_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV26TFContratoServicosIndicador_InstrumentoMedicao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV27TFContratoServicosIndicador_InstrumentoMedicao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV32TFContratoServicosIndicador_Vigencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV33TFContratoServicosIndicador_Vigencia_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00QL3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QL3,100,0,true,false )
             ,new CursorDef("P00QL5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QL5,100,0,true,false )
             ,new CursorDef("P00QL7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QL7,100,0,true,false )
             ,new CursorDef("P00QL9", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QL9,100,0,true,false )
             ,new CursorDef("P00QL11", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QL11,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 2) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 2) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((short[]) buf[12])[0] = rslt.getShort(7) ;
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((String[]) buf[19])[0] = rslt.getLongVarchar(12) ;
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 2) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((short[]) buf[12])[0] = rslt.getShort(7) ;
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((String[]) buf[19])[0] = rslt.getLongVarchar(12) ;
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 2) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((short[]) buf[12])[0] = rslt.getShort(7) ;
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((String[]) buf[19])[0] = rslt.getLongVarchar(12) ;
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 2) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((short[]) buf[12])[0] = rslt.getShort(7) ;
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((String[]) buf[19])[0] = rslt.getLongVarchar(12) ;
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratoservicosindicadorfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratoservicosindicadorfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratoservicosindicadorfilterdata") )
          {
             return  ;
          }
          getpromptcontratoservicosindicadorfilterdata worker = new getpromptcontratoservicosindicadorfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
