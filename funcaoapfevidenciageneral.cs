/*
               File: FuncaoAPFEvidenciaGeneral
        Description: Funcao APFEvidencia General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:1:15.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaoapfevidenciageneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public funcaoapfevidenciageneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public funcaoapfevidenciageneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPFEvidencia_Codigo ,
                           int aP1_FuncaoAPF_Codigo ,
                           int aP2_FuncaoAPF_SistemaCod )
      {
         this.A406FuncaoAPFEvidencia_Codigo = aP0_FuncaoAPFEvidencia_Codigo;
         this.A165FuncaoAPF_Codigo = aP1_FuncaoAPF_Codigo;
         this.A360FuncaoAPF_SistemaCod = aP2_FuncaoAPF_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A406FuncaoAPFEvidencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
                  A360FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A406FuncaoAPFEvidencia_Codigo,(int)A165FuncaoAPF_Codigo,(int)A360FuncaoAPF_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAAF2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV16Pgmname = "FuncaoAPFEvidenciaGeneral";
               context.Gx_err = 0;
               WSAF2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Funcao APFEvidencia General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282311545");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaoapfevidenciageneral.aspx") + "?" + UrlEncode("" +A406FuncaoAPFEvidencia_Codigo) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A360FuncaoAPF_SistemaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA406FuncaoAPFEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_DESCRICAO", GetSecureSignedToken( sPrefix, A407FuncaoAPFEvidencia_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_NOMEARQ", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A409FuncaoAPFEvidencia_NomeArq, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_TIPOARQ", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A410FuncaoAPFEvidencia_TipoArq, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A411FuncaoAPFEvidencia_Data, "99/99/99 99:99")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormAF2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("funcaoapfevidenciageneral.js", "?20204282311547");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "FuncaoAPFEvidenciaGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcao APFEvidencia General" ;
      }

      protected void WBAF0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "funcaoapfevidenciageneral.aspx");
            }
            wb_table1_2_AF2( true) ;
         }
         else
         {
            wb_table1_2_AF2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AF2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTAF2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Funcao APFEvidencia General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPAF0( ) ;
            }
         }
      }

      protected void WSAF2( )
      {
         STARTAF2( ) ;
         EVTAF2( ) ;
      }

      protected void EVTAF2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11AF2 */
                                    E11AF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12AF2 */
                                    E12AF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13AF2 */
                                    E13AF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14AF2 */
                                    E14AF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPAF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEAF2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormAF2( ) ;
            }
         }
      }

      protected void PAAF2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAF2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV16Pgmname = "FuncaoAPFEvidenciaGeneral";
         context.Gx_err = 0;
      }

      protected void RFAF2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00AF2 */
            pr_default.execute(0, new Object[] {A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A411FuncaoAPFEvidencia_Data = H00AF2_A411FuncaoAPFEvidencia_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A411FuncaoAPFEvidencia_Data", context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPFEVIDENCIA_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A411FuncaoAPFEvidencia_Data, "99/99/99 99:99")));
               A410FuncaoAPFEvidencia_TipoArq = H00AF2_A410FuncaoAPFEvidencia_TipoArq[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A410FuncaoAPFEvidencia_TipoArq", A410FuncaoAPFEvidencia_TipoArq);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPFEVIDENCIA_TIPOARQ", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A410FuncaoAPFEvidencia_TipoArq, ""))));
               n410FuncaoAPFEvidencia_TipoArq = H00AF2_n410FuncaoAPFEvidencia_TipoArq[0];
               A409FuncaoAPFEvidencia_NomeArq = H00AF2_A409FuncaoAPFEvidencia_NomeArq[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A409FuncaoAPFEvidencia_NomeArq", A409FuncaoAPFEvidencia_NomeArq);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPFEVIDENCIA_NOMEARQ", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A409FuncaoAPFEvidencia_NomeArq, ""))));
               n409FuncaoAPFEvidencia_NomeArq = H00AF2_n409FuncaoAPFEvidencia_NomeArq[0];
               A407FuncaoAPFEvidencia_Descricao = H00AF2_A407FuncaoAPFEvidencia_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A407FuncaoAPFEvidencia_Descricao", A407FuncaoAPFEvidencia_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPFEVIDENCIA_DESCRICAO", GetSecureSignedToken( sPrefix, A407FuncaoAPFEvidencia_Descricao));
               n407FuncaoAPFEvidencia_Descricao = H00AF2_n407FuncaoAPFEvidencia_Descricao[0];
               A408FuncaoAPFEvidencia_Arquivo = H00AF2_A408FuncaoAPFEvidencia_Arquivo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A408FuncaoAPFEvidencia_Arquivo", A408FuncaoAPFEvidencia_Arquivo);
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A408FuncaoAPFEvidencia_Arquivo));
               n408FuncaoAPFEvidencia_Arquivo = H00AF2_n408FuncaoAPFEvidencia_Arquivo[0];
               /* Execute user event: E12AF2 */
               E12AF2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBAF0( ) ;
         }
      }

      protected void STRUPAF0( )
      {
         /* Before Start, stand alone formulas. */
         AV16Pgmname = "FuncaoAPFEvidenciaGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11AF2 */
         E11AF2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A407FuncaoAPFEvidencia_Descricao = cgiGet( edtFuncaoAPFEvidencia_Descricao_Internalname);
            n407FuncaoAPFEvidencia_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A407FuncaoAPFEvidencia_Descricao", A407FuncaoAPFEvidencia_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPFEVIDENCIA_DESCRICAO", GetSecureSignedToken( sPrefix, A407FuncaoAPFEvidencia_Descricao));
            A409FuncaoAPFEvidencia_NomeArq = cgiGet( edtFuncaoAPFEvidencia_NomeArq_Internalname);
            n409FuncaoAPFEvidencia_NomeArq = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A409FuncaoAPFEvidencia_NomeArq", A409FuncaoAPFEvidencia_NomeArq);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPFEVIDENCIA_NOMEARQ", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A409FuncaoAPFEvidencia_NomeArq, ""))));
            A408FuncaoAPFEvidencia_Arquivo = cgiGet( edtFuncaoAPFEvidencia_Arquivo_Internalname);
            n408FuncaoAPFEvidencia_Arquivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A408FuncaoAPFEvidencia_Arquivo", A408FuncaoAPFEvidencia_Arquivo);
            A410FuncaoAPFEvidencia_TipoArq = cgiGet( edtFuncaoAPFEvidencia_TipoArq_Internalname);
            n410FuncaoAPFEvidencia_TipoArq = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A410FuncaoAPFEvidencia_TipoArq", A410FuncaoAPFEvidencia_TipoArq);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPFEVIDENCIA_TIPOARQ", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A410FuncaoAPFEvidencia_TipoArq, ""))));
            A411FuncaoAPFEvidencia_Data = context.localUtil.CToT( cgiGet( edtFuncaoAPFEvidencia_Data_Internalname), 0);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A411FuncaoAPFEvidencia_Data", context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPFEVIDENCIA_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A411FuncaoAPFEvidencia_Data, "99/99/99 99:99")));
            /* Read saved values. */
            wcpOA406FuncaoAPFEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA406FuncaoAPFEvidencia_Codigo"), ",", "."));
            wcpOA165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA165FuncaoAPF_Codigo"), ",", "."));
            wcpOA360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA360FuncaoAPF_SistemaCod"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11AF2 */
         E11AF2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11AF2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12AF2( )
      {
         /* Load Routine */
      }

      protected void E13AF2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("funcaoapfevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A406FuncaoAPFEvidencia_Codigo) + "," + UrlEncode("" +A165FuncaoAPF_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14AF2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("funcaoapfevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A406FuncaoAPFEvidencia_Codigo) + "," + UrlEncode("" +A165FuncaoAPF_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV16Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "FuncaoAPFEvidencia";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "FuncaoAPFEvidencia_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7FuncaoAPFEvidencia_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "FuncaoAPF_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV12FuncaoAPF_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "FuncaoAPF_SistemaCod";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV13FuncaoAPF_SistemaCod), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_AF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_AF2( true) ;
         }
         else
         {
            wb_table2_8_AF2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_AF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_35_AF2( true) ;
         }
         else
         {
            wb_table3_35_AF2( false) ;
         }
         return  ;
      }

      protected void wb_table3_35_AF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AF2e( true) ;
         }
         else
         {
            wb_table1_2_AF2e( false) ;
         }
      }

      protected void wb_table3_35_AF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoAPFEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoAPFEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_35_AF2e( true) ;
         }
         else
         {
            wb_table3_35_AF2e( false) ;
         }
      }

      protected void wb_table2_8_AF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfevidencia_descricao_Internalname, "Descri��o", "", "", lblTextblockfuncaoapfevidencia_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPFEvidencia_Descricao_Internalname, A407FuncaoAPFEvidencia_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_FuncaoAPFEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfevidencia_nomearq_Internalname, "Nome", "", "", lblTextblockfuncaoapfevidencia_nomearq_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFEvidencia_NomeArq_Internalname, StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq), StringUtil.RTrim( context.localUtil.Format( A409FuncaoAPFEvidencia_NomeArq, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFEvidencia_NomeArq_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "NomeArq", "left", true, "HLP_FuncaoAPFEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfevidencia_arquivo_Internalname, "Arquivo", "", "", lblTextblockfuncaoapfevidencia_arquivo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataContentCellView'>") ;
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            edtFuncaoAPFEvidencia_Arquivo_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "Filetype", edtFuncaoAPFEvidencia_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo)) )
            {
               gxblobfileaux.Source = A408FuncaoAPFEvidencia_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtFuncaoAPFEvidencia_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtFuncaoAPFEvidencia_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A408FuncaoAPFEvidencia_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n408FuncaoAPFEvidencia_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A408FuncaoAPFEvidencia_Arquivo", A408FuncaoAPFEvidencia_Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A408FuncaoAPFEvidencia_Arquivo));
                  edtFuncaoAPFEvidencia_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "Filetype", edtFuncaoAPFEvidencia_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A408FuncaoAPFEvidencia_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtFuncaoAPFEvidencia_Arquivo_Internalname, StringUtil.RTrim( A408FuncaoAPFEvidencia_Arquivo), context.PathToRelativeUrl( A408FuncaoAPFEvidencia_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtFuncaoAPFEvidencia_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtFuncaoAPFEvidencia_Arquivo_Filetype)) ? A408FuncaoAPFEvidencia_Arquivo : edtFuncaoAPFEvidencia_Arquivo_Filetype)) : edtFuncaoAPFEvidencia_Arquivo_Contenttype), false, "", edtFuncaoAPFEvidencia_Arquivo_Parameters, 0, 0, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtFuncaoAPFEvidencia_Arquivo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+"", "", "", "HLP_FuncaoAPFEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfevidencia_tipoarq_Internalname, "Tipo", "", "", lblTextblockfuncaoapfevidencia_tipoarq_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFEvidencia_TipoArq_Internalname, StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq), StringUtil.RTrim( context.localUtil.Format( A410FuncaoAPFEvidencia_TipoArq, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFEvidencia_TipoArq_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "TipoArq", "left", true, "HLP_FuncaoAPFEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfevidencia_data_Internalname, "Hora Upload", "", "", lblTextblockfuncaoapfevidencia_data_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFEvidenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtFuncaoAPFEvidencia_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFEvidencia_Data_Internalname, context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A411FuncaoAPFEvidencia_Data, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFEvidencia_Data_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_FuncaoAPFEvidenciaGeneral.htm");
            GxWebStd.gx_bitmap( context, edtFuncaoAPFEvidencia_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_FuncaoAPFEvidenciaGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_AF2e( true) ;
         }
         else
         {
            wb_table2_8_AF2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A406FuncaoAPFEvidencia_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
         A165FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A360FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAF2( ) ;
         WSAF2( ) ;
         WEAF2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA406FuncaoAPFEvidencia_Codigo = (String)((String)getParm(obj,0));
         sCtrlA165FuncaoAPF_Codigo = (String)((String)getParm(obj,1));
         sCtrlA360FuncaoAPF_SistemaCod = (String)((String)getParm(obj,2));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAAF2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "funcaoapfevidenciageneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAAF2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A406FuncaoAPFEvidencia_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
            A165FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A360FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,4));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
         }
         wcpOA406FuncaoAPFEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA406FuncaoAPFEvidencia_Codigo"), ",", "."));
         wcpOA165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA165FuncaoAPF_Codigo"), ",", "."));
         wcpOA360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA360FuncaoAPF_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A406FuncaoAPFEvidencia_Codigo != wcpOA406FuncaoAPFEvidencia_Codigo ) || ( A165FuncaoAPF_Codigo != wcpOA165FuncaoAPF_Codigo ) || ( A360FuncaoAPF_SistemaCod != wcpOA360FuncaoAPF_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOA406FuncaoAPFEvidencia_Codigo = A406FuncaoAPFEvidencia_Codigo;
         wcpOA165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
         wcpOA360FuncaoAPF_SistemaCod = A360FuncaoAPF_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA406FuncaoAPFEvidencia_Codigo = cgiGet( sPrefix+"A406FuncaoAPFEvidencia_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA406FuncaoAPFEvidencia_Codigo) > 0 )
         {
            A406FuncaoAPFEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA406FuncaoAPFEvidencia_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A406FuncaoAPFEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0)));
         }
         else
         {
            A406FuncaoAPFEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A406FuncaoAPFEvidencia_Codigo_PARM"), ",", "."));
         }
         sCtrlA165FuncaoAPF_Codigo = cgiGet( sPrefix+"A165FuncaoAPF_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA165FuncaoAPF_Codigo) > 0 )
         {
            A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA165FuncaoAPF_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         }
         else
         {
            A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A165FuncaoAPF_Codigo_PARM"), ",", "."));
         }
         sCtrlA360FuncaoAPF_SistemaCod = cgiGet( sPrefix+"A360FuncaoAPF_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlA360FuncaoAPF_SistemaCod) > 0 )
         {
            A360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA360FuncaoAPF_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
         }
         else
         {
            A360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A360FuncaoAPF_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAAF2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSAF2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSAF2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A406FuncaoAPFEvidencia_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA406FuncaoAPFEvidencia_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A406FuncaoAPFEvidencia_Codigo_CTRL", StringUtil.RTrim( sCtrlA406FuncaoAPFEvidencia_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A165FuncaoAPF_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA165FuncaoAPF_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A165FuncaoAPF_Codigo_CTRL", StringUtil.RTrim( sCtrlA165FuncaoAPF_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A360FuncaoAPF_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA360FuncaoAPF_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A360FuncaoAPF_SistemaCod_CTRL", StringUtil.RTrim( sCtrlA360FuncaoAPF_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEAF2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282311581");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("funcaoapfevidenciageneral.js", "?20204282311581");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockfuncaoapfevidencia_descricao_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPFEVIDENCIA_DESCRICAO";
         edtFuncaoAPFEvidencia_Descricao_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_DESCRICAO";
         lblTextblockfuncaoapfevidencia_nomearq_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPFEVIDENCIA_NOMEARQ";
         edtFuncaoAPFEvidencia_NomeArq_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_NOMEARQ";
         lblTextblockfuncaoapfevidencia_arquivo_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPFEVIDENCIA_ARQUIVO";
         edtFuncaoAPFEvidencia_Arquivo_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_ARQUIVO";
         lblTextblockfuncaoapfevidencia_tipoarq_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPFEVIDENCIA_TIPOARQ";
         edtFuncaoAPFEvidencia_TipoArq_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_TIPOARQ";
         lblTextblockfuncaoapfevidencia_data_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPFEVIDENCIA_DATA";
         edtFuncaoAPFEvidencia_Data_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_DATA";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtFuncaoAPFEvidencia_Data_Jsonclick = "";
         edtFuncaoAPFEvidencia_TipoArq_Jsonclick = "";
         edtFuncaoAPFEvidencia_Arquivo_Jsonclick = "";
         edtFuncaoAPFEvidencia_Arquivo_Parameters = "";
         edtFuncaoAPFEvidencia_Arquivo_Contenttype = "";
         edtFuncaoAPFEvidencia_Arquivo_Filetype = "";
         edtFuncaoAPFEvidencia_NomeArq_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13AF2',iparms:[{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14AF2',iparms:[{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A407FuncaoAPFEvidencia_Descricao = "";
         A409FuncaoAPFEvidencia_NomeArq = "";
         A410FuncaoAPFEvidencia_TipoArq = "";
         A411FuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00AF2_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         H00AF2_A165FuncaoAPF_Codigo = new int[1] ;
         H00AF2_A411FuncaoAPFEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         H00AF2_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         H00AF2_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         H00AF2_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         H00AF2_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         H00AF2_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         H00AF2_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         H00AF2_A408FuncaoAPFEvidencia_Arquivo = new String[] {""} ;
         H00AF2_n408FuncaoAPFEvidencia_Arquivo = new bool[] {false} ;
         A408FuncaoAPFEvidencia_Arquivo = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockfuncaoapfevidencia_descricao_Jsonclick = "";
         lblTextblockfuncaoapfevidencia_nomearq_Jsonclick = "";
         lblTextblockfuncaoapfevidencia_arquivo_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblockfuncaoapfevidencia_tipoarq_Jsonclick = "";
         lblTextblockfuncaoapfevidencia_data_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA406FuncaoAPFEvidencia_Codigo = "";
         sCtrlA165FuncaoAPF_Codigo = "";
         sCtrlA360FuncaoAPF_SistemaCod = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaoapfevidenciageneral__default(),
            new Object[][] {
                new Object[] {
               H00AF2_A406FuncaoAPFEvidencia_Codigo, H00AF2_A165FuncaoAPF_Codigo, H00AF2_A411FuncaoAPFEvidencia_Data, H00AF2_A410FuncaoAPFEvidencia_TipoArq, H00AF2_n410FuncaoAPFEvidencia_TipoArq, H00AF2_A409FuncaoAPFEvidencia_NomeArq, H00AF2_n409FuncaoAPFEvidencia_NomeArq, H00AF2_A407FuncaoAPFEvidencia_Descricao, H00AF2_n407FuncaoAPFEvidencia_Descricao, H00AF2_A408FuncaoAPFEvidencia_Arquivo,
               H00AF2_n408FuncaoAPFEvidencia_Arquivo
               }
            }
         );
         AV16Pgmname = "FuncaoAPFEvidenciaGeneral";
         /* GeneXus formulas. */
         AV16Pgmname = "FuncaoAPFEvidenciaGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A406FuncaoAPFEvidencia_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int A360FuncaoAPF_SistemaCod ;
      private int wcpOA406FuncaoAPFEvidencia_Codigo ;
      private int wcpOA165FuncaoAPF_Codigo ;
      private int wcpOA360FuncaoAPF_SistemaCod ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7FuncaoAPFEvidencia_Codigo ;
      private int AV12FuncaoAPF_Codigo ;
      private int AV13FuncaoAPF_SistemaCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV16Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A409FuncaoAPFEvidencia_NomeArq ;
      private String A410FuncaoAPFEvidencia_TipoArq ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtFuncaoAPFEvidencia_Arquivo_Internalname ;
      private String edtFuncaoAPFEvidencia_Descricao_Internalname ;
      private String edtFuncaoAPFEvidencia_NomeArq_Internalname ;
      private String edtFuncaoAPFEvidencia_TipoArq_Internalname ;
      private String edtFuncaoAPFEvidencia_Data_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockfuncaoapfevidencia_descricao_Internalname ;
      private String lblTextblockfuncaoapfevidencia_descricao_Jsonclick ;
      private String lblTextblockfuncaoapfevidencia_nomearq_Internalname ;
      private String lblTextblockfuncaoapfevidencia_nomearq_Jsonclick ;
      private String edtFuncaoAPFEvidencia_NomeArq_Jsonclick ;
      private String lblTextblockfuncaoapfevidencia_arquivo_Internalname ;
      private String lblTextblockfuncaoapfevidencia_arquivo_Jsonclick ;
      private String edtFuncaoAPFEvidencia_Arquivo_Filetype ;
      private String edtFuncaoAPFEvidencia_Arquivo_Contenttype ;
      private String edtFuncaoAPFEvidencia_Arquivo_Parameters ;
      private String edtFuncaoAPFEvidencia_Arquivo_Jsonclick ;
      private String lblTextblockfuncaoapfevidencia_tipoarq_Internalname ;
      private String lblTextblockfuncaoapfevidencia_tipoarq_Jsonclick ;
      private String edtFuncaoAPFEvidencia_TipoArq_Jsonclick ;
      private String lblTextblockfuncaoapfevidencia_data_Internalname ;
      private String lblTextblockfuncaoapfevidencia_data_Jsonclick ;
      private String edtFuncaoAPFEvidencia_Data_Jsonclick ;
      private String sCtrlA406FuncaoAPFEvidencia_Codigo ;
      private String sCtrlA165FuncaoAPF_Codigo ;
      private String sCtrlA360FuncaoAPF_SistemaCod ;
      private DateTime A411FuncaoAPFEvidencia_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n410FuncaoAPFEvidencia_TipoArq ;
      private bool n409FuncaoAPFEvidencia_NomeArq ;
      private bool n407FuncaoAPFEvidencia_Descricao ;
      private bool n408FuncaoAPFEvidencia_Arquivo ;
      private bool returnInSub ;
      private String A407FuncaoAPFEvidencia_Descricao ;
      private String A408FuncaoAPFEvidencia_Arquivo ;
      private GxFile gxblobfileaux ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00AF2_A406FuncaoAPFEvidencia_Codigo ;
      private int[] H00AF2_A165FuncaoAPF_Codigo ;
      private DateTime[] H00AF2_A411FuncaoAPFEvidencia_Data ;
      private String[] H00AF2_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] H00AF2_n410FuncaoAPFEvidencia_TipoArq ;
      private String[] H00AF2_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] H00AF2_n409FuncaoAPFEvidencia_NomeArq ;
      private String[] H00AF2_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] H00AF2_n407FuncaoAPFEvidencia_Descricao ;
      private String[] H00AF2_A408FuncaoAPFEvidencia_Arquivo ;
      private bool[] H00AF2_n408FuncaoAPFEvidencia_Arquivo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class funcaoapfevidenciageneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AF2 ;
          prmH00AF2 = new Object[] {
          new Object[] {"@FuncaoAPFEvidencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AF2", "SELECT [FuncaoAPFEvidencia_Codigo], [FuncaoAPF_Codigo], [FuncaoAPFEvidencia_Data], [FuncaoAPFEvidencia_TipoArq], [FuncaoAPFEvidencia_NomeArq], [FuncaoAPFEvidencia_Descricao], [FuncaoAPFEvidencia_Arquivo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE ([FuncaoAPFEvidencia_Codigo] = @FuncaoAPFEvidencia_Codigo) AND ([FuncaoAPF_Codigo] = @FuncaoAPF_Codigo) ORDER BY [FuncaoAPFEvidencia_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AF2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getBLOBFile(7, "tmp", "") ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
