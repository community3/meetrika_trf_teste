/*
               File: PRC_UpdOSSistema
        Description: Upd OS Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:14.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updossistema : GXProcedure
   {
      public prc_updossistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updossistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_OldSistema_Codigo ,
                           ref int aP1_NewSistema_Codigo )
      {
         this.AV8OldSistema_Codigo = aP0_OldSistema_Codigo;
         this.AV9NewSistema_Codigo = aP1_NewSistema_Codigo;
         initialize();
         executePrivate();
         aP0_OldSistema_Codigo=this.AV8OldSistema_Codigo;
         aP1_NewSistema_Codigo=this.AV9NewSistema_Codigo;
      }

      public int executeUdp( ref int aP0_OldSistema_Codigo )
      {
         this.AV8OldSistema_Codigo = aP0_OldSistema_Codigo;
         this.AV9NewSistema_Codigo = aP1_NewSistema_Codigo;
         initialize();
         executePrivate();
         aP0_OldSistema_Codigo=this.AV8OldSistema_Codigo;
         aP1_NewSistema_Codigo=this.AV9NewSistema_Codigo;
         return AV9NewSistema_Codigo ;
      }

      public void executeSubmit( ref int aP0_OldSistema_Codigo ,
                                 ref int aP1_NewSistema_Codigo )
      {
         prc_updossistema objprc_updossistema;
         objprc_updossistema = new prc_updossistema();
         objprc_updossistema.AV8OldSistema_Codigo = aP0_OldSistema_Codigo;
         objprc_updossistema.AV9NewSistema_Codigo = aP1_NewSistema_Codigo;
         objprc_updossistema.context.SetSubmitInitialConfig(context);
         objprc_updossistema.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updossistema);
         aP0_OldSistema_Codigo=this.AV8OldSistema_Codigo;
         aP1_NewSistema_Codigo=this.AV9NewSistema_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updossistema)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00582 */
         pr_default.execute(0, new Object[] {AV8OldSistema_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A484ContagemResultado_StatusDmn = P00582_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00582_n484ContagemResultado_StatusDmn[0];
            A489ContagemResultado_SistemaCod = P00582_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00582_n489ContagemResultado_SistemaCod[0];
            A456ContagemResultado_Codigo = P00582_A456ContagemResultado_Codigo[0];
            A489ContagemResultado_SistemaCod = AV9NewSistema_Codigo;
            n489ContagemResultado_SistemaCod = false;
            BatchSize = 200;
            pr_default.initializeBatch( 1, BatchSize, this, "Executebatchp00583");
            /* Using cursor P00583 */
            pr_default.addRecord(1, new Object[] {n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, A456ContagemResultado_Codigo});
            if ( pr_default.recordCount(1) == pr_default.getBatchSize(1) )
            {
               Executebatchp00583( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(0);
         }
         if ( pr_default.getBatchSize(1) > 0 )
         {
            Executebatchp00583( ) ;
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void Executebatchp00583( )
      {
         /* Using cursor P00583 */
         pr_default.executeBatch(1);
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UpdOSSistema");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00582_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00582_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00582_A489ContagemResultado_SistemaCod = new int[1] ;
         P00582_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00582_A456ContagemResultado_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         P00583_A489ContagemResultado_SistemaCod = new int[1] ;
         P00583_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00583_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updossistema__default(),
            new Object[][] {
                new Object[] {
               P00582_A484ContagemResultado_StatusDmn, P00582_n484ContagemResultado_StatusDmn, P00582_A489ContagemResultado_SistemaCod, P00582_n489ContagemResultado_SistemaCod, P00582_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8OldSistema_Codigo ;
      private int AV9NewSistema_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int A456ContagemResultado_Codigo ;
      private int BatchSize ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n489ContagemResultado_SistemaCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_OldSistema_Codigo ;
      private int aP1_NewSistema_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] P00582_A484ContagemResultado_StatusDmn ;
      private bool[] P00582_n484ContagemResultado_StatusDmn ;
      private int[] P00582_A489ContagemResultado_SistemaCod ;
      private bool[] P00582_n489ContagemResultado_SistemaCod ;
      private int[] P00582_A456ContagemResultado_Codigo ;
      private int[] P00583_A489ContagemResultado_SistemaCod ;
      private bool[] P00583_n489ContagemResultado_SistemaCod ;
      private int[] P00583_A456ContagemResultado_Codigo ;
   }

   public class prc_updossistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new BatchUpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00582 ;
          prmP00582 = new Object[] {
          new Object[] {"@AV8OldSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00583 ;
          prmP00583 = new Object[] {
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00582", "SELECT [ContagemResultado_StatusDmn], [ContagemResultado_SistemaCod], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (UPDLOCK) WHERE ([ContagemResultado_SistemaCod] = @AV8OldSistema_Codigo) AND ([ContagemResultado_StatusDmn] = 'A' or [ContagemResultado_StatusDmn] = 'C' or [ContagemResultado_StatusDmn] = 'R' or [ContagemResultado_StatusDmn] = 'D' or [ContagemResultado_StatusDmn] = 'S') ORDER BY [ContagemResultado_SistemaCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00582,1,0,true,false )
             ,new CursorDef("P00583", "UPDATE [ContagemResultado] SET [ContagemResultado_SistemaCod]=@ContagemResultado_SistemaCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00583)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
