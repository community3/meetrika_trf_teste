/*
               File: PromptContratante
        Description: Selecione Contratante
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/31/2020 1:13:52.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratante : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratante( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratante( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratante_Codigo ,
                           ref String aP1_InOutContratante_CNPJ )
      {
         this.AV7InOutContratante_Codigo = aP0_InOutContratante_Codigo;
         this.AV63InOutContratante_CNPJ = aP1_InOutContratante_CNPJ;
         executePrivate();
         aP0_InOutContratante_Codigo=this.AV7InOutContratante_Codigo;
         aP1_InOutContratante_CNPJ=this.AV63InOutContratante_CNPJ;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         chkContratante_Ativo = new GXCheckbox();
         cmbContratante_EmailSdaAut = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_29 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_29_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_29_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratante_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratante_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV63InOutContratante_CNPJ = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63InOutContratante_CNPJ", AV63InOutContratante_CNPJ);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA0Z2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS0Z2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE0Z2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205311135240");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratante.aspx") + "?" + UrlEncode("" +AV7InOutContratante_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV63InOutContratante_CNPJ))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_29", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_29), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV114GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV115GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATANTE_CNPJ", AV63InOutContratante_CNPJ);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm0Z2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratante" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contratante" ;
      }

      protected void WB0Z0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_0Z2( true) ;
         }
         else
         {
            wb_table1_2_0Z2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_0Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
         }
         wbLoad = true;
      }

      protected void START0Z2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contratante", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0Z0( ) ;
      }

      protected void WS0Z2( )
      {
         START0Z2( ) ;
         EVT0Z2( ) ;
      }

      protected void EVT0Z2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110Z2 */
                           E110Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120Z2 */
                           E120Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_29_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
                           SubsflControlProps_292( ) ;
                           AV31Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV118Select_GXI : context.convertURL( context.PathToRelativeUrl( AV31Select))));
                           A29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratante_Codigo_Internalname), ",", "."));
                           A335Contratante_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratante_PessoaCod_Internalname), ",", "."));
                           A12Contratante_CNPJ = cgiGet( edtContratante_CNPJ_Internalname);
                           n12Contratante_CNPJ = false;
                           A9Contratante_RazaoSocial = StringUtil.Upper( cgiGet( edtContratante_RazaoSocial_Internalname));
                           n9Contratante_RazaoSocial = false;
                           A10Contratante_NomeFantasia = StringUtil.Upper( cgiGet( edtContratante_NomeFantasia_Internalname));
                           A11Contratante_IE = cgiGet( edtContratante_IE_Internalname);
                           A336Contratante_AgenciaNome = StringUtil.Upper( cgiGet( edtContratante_AgenciaNome_Internalname));
                           n336Contratante_AgenciaNome = false;
                           A337Contratante_AgenciaNro = cgiGet( edtContratante_AgenciaNro_Internalname);
                           n337Contratante_AgenciaNro = false;
                           A338Contratante_BancoNome = StringUtil.Upper( cgiGet( edtContratante_BancoNome_Internalname));
                           n338Contratante_BancoNome = false;
                           A339Contratante_BancoNro = cgiGet( edtContratante_BancoNro_Internalname);
                           n339Contratante_BancoNro = false;
                           A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
                           A23Estado_UF = StringUtil.Upper( cgiGet( edtEstado_UF_Internalname));
                           A30Contratante_Ativo = StringUtil.StrToBool( cgiGet( chkContratante_Ativo_Internalname));
                           A547Contratante_EmailSdaHost = cgiGet( edtContratante_EmailSdaHost_Internalname);
                           n547Contratante_EmailSdaHost = false;
                           A548Contratante_EmailSdaUser = cgiGet( edtContratante_EmailSdaUser_Internalname);
                           n548Contratante_EmailSdaUser = false;
                           A549Contratante_EmailSdaPass = cgiGet( edtContratante_EmailSdaPass_Internalname);
                           n549Contratante_EmailSdaPass = false;
                           A550Contratante_EmailSdaKey = cgiGet( edtContratante_EmailSdaKey_Internalname);
                           n550Contratante_EmailSdaKey = false;
                           cmbContratante_EmailSdaAut.Name = cmbContratante_EmailSdaAut_Internalname;
                           cmbContratante_EmailSdaAut.CurrentValue = cgiGet( cmbContratante_EmailSdaAut_Internalname);
                           A551Contratante_EmailSdaAut = StringUtil.StrToBool( cgiGet( cmbContratante_EmailSdaAut_Internalname));
                           n551Contratante_EmailSdaAut = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E130Z2 */
                                 E130Z2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E140Z2 */
                                 E140Z2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E150Z2 */
                                 E150Z2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E160Z2 */
                                       E160Z2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE0Z2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm0Z2( ) ;
            }
         }
      }

      protected void PA0Z2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            GXCCtl = "CONTRATANTE_ATIVO_" + sGXsfl_29_idx;
            chkContratante_Ativo.Name = GXCCtl;
            chkContratante_Ativo.WebTags = "";
            chkContratante_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratante_Ativo_Internalname, "TitleCaption", chkContratante_Ativo.Caption);
            chkContratante_Ativo.CheckedValue = "false";
            GXCCtl = "CONTRATANTE_EMAILSDAAUT_" + sGXsfl_29_idx;
            cmbContratante_EmailSdaAut.Name = GXCCtl;
            cmbContratante_EmailSdaAut.WebTags = "";
            cmbContratante_EmailSdaAut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_EmailSdaAut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_EmailSdaAut.ItemCount > 0 )
            {
               A551Contratante_EmailSdaAut = StringUtil.StrToBool( cmbContratante_EmailSdaAut.getValidValue(StringUtil.BoolToStr( A551Contratante_EmailSdaAut)));
               n551Contratante_EmailSdaAut = false;
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_292( ) ;
         while ( nGXsfl_29_idx <= nRC_GXsfl_29 )
         {
            sendrow_292( ) ;
            nGXsfl_29_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_29_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_29_idx+1));
            sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
            SubsflControlProps_292( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF0Z2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_PESSOACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_NOMEFANTASIA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A10Contratante_NomeFantasia, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_NOMEFANTASIA", StringUtil.RTrim( A10Contratante_NomeFantasia));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_IE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A11Contratante_IE, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_IE", StringUtil.RTrim( A11Contratante_IE));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_AGENCIANOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A336Contratante_AgenciaNome, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_AGENCIANOME", StringUtil.RTrim( A336Contratante_AgenciaNome));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_AGENCIANRO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A337Contratante_AgenciaNro, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_AGENCIANRO", StringUtil.RTrim( A337Contratante_AgenciaNro));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_BANCONOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A338Contratante_BancoNome, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_BANCONOME", StringUtil.RTrim( A338Contratante_BancoNome));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_BANCONRO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A339Contratante_BancoNro, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_BANCONRO", StringUtil.RTrim( A339Contratante_BancoNro));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_ATIVO", GetSecureSignedToken( "", A30Contratante_Ativo));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_ATIVO", StringUtil.BoolToStr( A30Contratante_Ativo));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_EMAILSDAHOST", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A547Contratante_EmailSdaHost, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAHOST", A547Contratante_EmailSdaHost);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_EMAILSDAUSER", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A548Contratante_EmailSdaUser, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAUSER", A548Contratante_EmailSdaUser);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_EMAILSDAPASS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A549Contratante_EmailSdaPass, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAPASS", A549Contratante_EmailSdaPass);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_EMAILSDAKEY", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A550Contratante_EmailSdaKey, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAKEY", StringUtil.RTrim( A550Contratante_EmailSdaKey));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_EMAILSDAAUT", GetSecureSignedToken( "", A551Contratante_EmailSdaAut));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAAUT", StringUtil.BoolToStr( A551Contratante_EmailSdaAut));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0Z2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF0Z2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 29;
         /* Execute user event: E140Z2 */
         E140Z2 ();
         nGXsfl_29_idx = 1;
         sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
         SubsflControlProps_292( ) ;
         nGXsfl_29_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_292( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H000Z2 */
            pr_default.execute(0, new Object[] {GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_29_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A25Municipio_Codigo = H000Z2_A25Municipio_Codigo[0];
               n25Municipio_Codigo = H000Z2_n25Municipio_Codigo[0];
               A551Contratante_EmailSdaAut = H000Z2_A551Contratante_EmailSdaAut[0];
               n551Contratante_EmailSdaAut = H000Z2_n551Contratante_EmailSdaAut[0];
               A550Contratante_EmailSdaKey = H000Z2_A550Contratante_EmailSdaKey[0];
               n550Contratante_EmailSdaKey = H000Z2_n550Contratante_EmailSdaKey[0];
               A549Contratante_EmailSdaPass = H000Z2_A549Contratante_EmailSdaPass[0];
               n549Contratante_EmailSdaPass = H000Z2_n549Contratante_EmailSdaPass[0];
               A548Contratante_EmailSdaUser = H000Z2_A548Contratante_EmailSdaUser[0];
               n548Contratante_EmailSdaUser = H000Z2_n548Contratante_EmailSdaUser[0];
               A547Contratante_EmailSdaHost = H000Z2_A547Contratante_EmailSdaHost[0];
               n547Contratante_EmailSdaHost = H000Z2_n547Contratante_EmailSdaHost[0];
               A30Contratante_Ativo = H000Z2_A30Contratante_Ativo[0];
               A23Estado_UF = H000Z2_A23Estado_UF[0];
               A26Municipio_Nome = H000Z2_A26Municipio_Nome[0];
               A339Contratante_BancoNro = H000Z2_A339Contratante_BancoNro[0];
               n339Contratante_BancoNro = H000Z2_n339Contratante_BancoNro[0];
               A338Contratante_BancoNome = H000Z2_A338Contratante_BancoNome[0];
               n338Contratante_BancoNome = H000Z2_n338Contratante_BancoNome[0];
               A337Contratante_AgenciaNro = H000Z2_A337Contratante_AgenciaNro[0];
               n337Contratante_AgenciaNro = H000Z2_n337Contratante_AgenciaNro[0];
               A336Contratante_AgenciaNome = H000Z2_A336Contratante_AgenciaNome[0];
               n336Contratante_AgenciaNome = H000Z2_n336Contratante_AgenciaNome[0];
               A11Contratante_IE = H000Z2_A11Contratante_IE[0];
               A10Contratante_NomeFantasia = H000Z2_A10Contratante_NomeFantasia[0];
               A9Contratante_RazaoSocial = H000Z2_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = H000Z2_n9Contratante_RazaoSocial[0];
               A12Contratante_CNPJ = H000Z2_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = H000Z2_n12Contratante_CNPJ[0];
               A335Contratante_PessoaCod = H000Z2_A335Contratante_PessoaCod[0];
               A29Contratante_Codigo = H000Z2_A29Contratante_Codigo[0];
               A23Estado_UF = H000Z2_A23Estado_UF[0];
               A26Municipio_Nome = H000Z2_A26Municipio_Nome[0];
               A9Contratante_RazaoSocial = H000Z2_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = H000Z2_n9Contratante_RazaoSocial[0];
               A12Contratante_CNPJ = H000Z2_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = H000Z2_n12Contratante_CNPJ[0];
               /* Execute user event: E150Z2 */
               E150Z2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 29;
            WB0Z0( ) ;
         }
         nGXsfl_29_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H000Z3 */
         pr_default.execute(1);
         GRID_nRecordCount = H000Z3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc) ;
         }
         return (int)(0) ;
      }

      protected void STRUP0Z0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E130Z2 */
         E130Z2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAREATRABALHO_CODIGO");
               GX_FocusControl = edtavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64AreaTrabalho_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64AreaTrabalho_Codigo), 6, 0)));
            }
            else
            {
               AV64AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64AreaTrabalho_Codigo), 6, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_29 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_29"), ",", "."));
            AV114GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV115GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E130Z2 */
         E130Z2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E130Z2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         Form.Caption = "Select Contratante";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "CNPJ", 0);
         cmbavOrderedby.addItem("2", "Jur�dica", 0);
         cmbavOrderedby.addItem("3", "Raz�o Social", 0);
         cmbavOrderedby.addItem("4", "Nome Fantasia", 0);
         cmbavOrderedby.addItem("5", "Insc. Estadual", 0);
         cmbavOrderedby.addItem("6", "Nome", 0);
         cmbavOrderedby.addItem("7", "Nro", 0);
         cmbavOrderedby.addItem("8", "Nome", 0);
         cmbavOrderedby.addItem("9", "Nro", 0);
         cmbavOrderedby.addItem("10", "Munic�pio", 0);
         cmbavOrderedby.addItem("11", "UF", 0);
         cmbavOrderedby.addItem("12", "Ativo?", 0);
         cmbavOrderedby.addItem("13", "SMTP", 0);
         cmbavOrderedby.addItem("14", "Usu�rio", 0);
         cmbavOrderedby.addItem("15", "Senha", 0);
         cmbavOrderedby.addItem("16", "Key", 0);
         cmbavOrderedby.addItem("17", "Autentica��o", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E140Z2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         edtContratante_PessoaCod_Titleformat = 2;
         edtContratante_PessoaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Jur�dica", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_PessoaCod_Internalname, "Title", edtContratante_PessoaCod_Title);
         edtContratante_CNPJ_Titleformat = 2;
         edtContratante_CNPJ_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "CNPJ", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_CNPJ_Internalname, "Title", edtContratante_CNPJ_Title);
         edtContratante_RazaoSocial_Titleformat = 2;
         edtContratante_RazaoSocial_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Raz�o Social", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_RazaoSocial_Internalname, "Title", edtContratante_RazaoSocial_Title);
         edtContratante_NomeFantasia_Titleformat = 2;
         edtContratante_NomeFantasia_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome Fantasia", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_NomeFantasia_Internalname, "Title", edtContratante_NomeFantasia_Title);
         edtContratante_IE_Titleformat = 2;
         edtContratante_IE_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Insc. Estadual", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_IE_Internalname, "Title", edtContratante_IE_Title);
         edtContratante_AgenciaNome_Titleformat = 2;
         edtContratante_AgenciaNome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV13OrderedBy==6) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_AgenciaNome_Internalname, "Title", edtContratante_AgenciaNome_Title);
         edtContratante_AgenciaNro_Titleformat = 2;
         edtContratante_AgenciaNro_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV13OrderedBy==7) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nro", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_AgenciaNro_Internalname, "Title", edtContratante_AgenciaNro_Title);
         edtContratante_BancoNome_Titleformat = 2;
         edtContratante_BancoNome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV13OrderedBy==8) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_BancoNome_Internalname, "Title", edtContratante_BancoNome_Title);
         edtContratante_BancoNro_Titleformat = 2;
         edtContratante_BancoNro_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV13OrderedBy==9) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nro", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_BancoNro_Internalname, "Title", edtContratante_BancoNro_Title);
         edtMunicipio_Nome_Titleformat = 2;
         edtMunicipio_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 10);' >%5</span>", ((AV13OrderedBy==10) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Munic�pio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Nome_Internalname, "Title", edtMunicipio_Nome_Title);
         edtEstado_UF_Titleformat = 2;
         edtEstado_UF_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 11);' >%5</span>", ((AV13OrderedBy==11) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "UF", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEstado_UF_Internalname, "Title", edtEstado_UF_Title);
         chkContratante_Ativo_Titleformat = 2;
         chkContratante_Ativo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 12);' >%5</span>", ((AV13OrderedBy==12) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ativo?", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratante_Ativo_Internalname, "Title", chkContratante_Ativo.Title.Text);
         edtContratante_EmailSdaHost_Titleformat = 2;
         edtContratante_EmailSdaHost_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 13);' >%5</span>", ((AV13OrderedBy==13) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "SMTP", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_EmailSdaHost_Internalname, "Title", edtContratante_EmailSdaHost_Title);
         edtContratante_EmailSdaUser_Titleformat = 2;
         edtContratante_EmailSdaUser_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 14);' >%5</span>", ((AV13OrderedBy==14) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Usu�rio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_EmailSdaUser_Internalname, "Title", edtContratante_EmailSdaUser_Title);
         edtContratante_EmailSdaPass_Titleformat = 2;
         edtContratante_EmailSdaPass_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 15);' >%5</span>", ((AV13OrderedBy==15) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Senha", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_EmailSdaPass_Internalname, "Title", edtContratante_EmailSdaPass_Title);
         edtContratante_EmailSdaKey_Titleformat = 2;
         edtContratante_EmailSdaKey_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 16);' >%5</span>", ((AV13OrderedBy==16) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Key", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_EmailSdaKey_Internalname, "Title", edtContratante_EmailSdaKey_Title);
         cmbContratante_EmailSdaAut_Titleformat = 2;
         cmbContratante_EmailSdaAut.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 17);' >%5</span>", ((AV13OrderedBy==17) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Autentica��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_EmailSdaAut_Internalname, "Title", cmbContratante_EmailSdaAut.Title.Text);
         AV114GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV114GridCurrentPage), 10, 0)));
         AV115GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV115GridPageCount), 10, 0)));
      }

      protected void E110Z2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV113PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV113PageToGo) ;
         }
      }

      private void E150Z2( )
      {
         /* Grid_Load Routine */
         AV31Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV31Select);
         AV118Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 29;
         }
         sendrow_292( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_29_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(29, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E160Z2 */
         E160Z2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E160Z2( )
      {
         /* Enter Routine */
         AV7InOutContratante_Codigo = A29Contratante_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratante_Codigo), 6, 0)));
         AV63InOutContratante_CNPJ = A12Contratante_CNPJ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63InOutContratante_CNPJ", AV63InOutContratante_CNPJ);
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratante_Codigo,(String)AV63InOutContratante_CNPJ});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E120Z2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void wb_table1_2_0Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_0Z2( true) ;
         }
         else
         {
            wb_table2_5_0Z2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_0Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_23_0Z2( true) ;
         }
         else
         {
            wb_table3_23_0Z2( false) ;
         }
         return  ;
      }

      protected void wb_table3_23_0Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0Z2e( true) ;
         }
         else
         {
            wb_table1_2_0Z2e( false) ;
         }
      }

      protected void wb_table3_23_0Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_26_0Z2( true) ;
         }
         else
         {
            wb_table4_26_0Z2( false) ;
         }
         return  ;
      }

      protected void wb_table4_26_0Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_23_0Z2e( true) ;
         }
         else
         {
            wb_table3_23_0Z2e( false) ;
         }
      }

      protected void wb_table4_26_0Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"29\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_PessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_PessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_PessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_CNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_CNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_CNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_RazaoSocial_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_RazaoSocial_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_RazaoSocial_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_NomeFantasia_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_NomeFantasia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_NomeFantasia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_IE_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_IE_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_IE_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_AgenciaNome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_AgenciaNome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_AgenciaNome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(60), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_AgenciaNro_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_AgenciaNro_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_AgenciaNro_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_BancoNome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_BancoNome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_BancoNome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(76), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_BancoNro_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_BancoNro_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_BancoNro_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMunicipio_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtMunicipio_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMunicipio_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtEstado_UF_Titleformat == 0 )
               {
                  context.SendWebValue( edtEstado_UF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtEstado_UF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContratante_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkContratante_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContratante_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_EmailSdaHost_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_EmailSdaHost_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_EmailSdaHost_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_EmailSdaUser_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_EmailSdaUser_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_EmailSdaUser_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_EmailSdaPass_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_EmailSdaPass_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_EmailSdaPass_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_EmailSdaKey_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_EmailSdaKey_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_EmailSdaKey_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratante_EmailSdaAut_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratante_EmailSdaAut.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratante_EmailSdaAut.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_PessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_PessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A12Contratante_CNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_CNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_CNPJ_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A9Contratante_RazaoSocial));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_RazaoSocial_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_RazaoSocial_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A10Contratante_NomeFantasia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_NomeFantasia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_NomeFantasia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A11Contratante_IE));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_IE_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_IE_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A336Contratante_AgenciaNome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_AgenciaNome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_AgenciaNome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A337Contratante_AgenciaNro));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_AgenciaNro_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_AgenciaNro_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A338Contratante_BancoNome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_BancoNome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_BancoNome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A339Contratante_BancoNro));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_BancoNro_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_BancoNro_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A26Municipio_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMunicipio_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMunicipio_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A23Estado_UF));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtEstado_UF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtEstado_UF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A30Contratante_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContratante_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContratante_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A547Contratante_EmailSdaHost);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_EmailSdaHost_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_EmailSdaHost_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A548Contratante_EmailSdaUser);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_EmailSdaUser_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_EmailSdaUser_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A549Contratante_EmailSdaPass);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_EmailSdaPass_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_EmailSdaPass_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A550Contratante_EmailSdaKey));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_EmailSdaKey_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_EmailSdaKey_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A551Contratante_EmailSdaAut));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratante_EmailSdaAut.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratante_EmailSdaAut_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 29 )
         {
            wbEnd = 0;
            nRC_GXsfl_29 = (short)(nGXsfl_29_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_26_0Z2e( true) ;
         }
         else
         {
            wb_table4_26_0Z2e( false) ;
         }
      }

      protected void wb_table2_5_0Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_29_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratante.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_14_0Z2( true) ;
         }
         else
         {
            wb_table5_14_0Z2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_0Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0Z2e( true) ;
         }
         else
         {
            wb_table2_5_0Z2e( false) ;
         }
      }

      protected void wb_table5_14_0Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextareatrabalho_codigo_Internalname, "C�digo Area de Trabalho", "", "", lblFiltertextareatrabalho_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_PromptContratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64AreaTrabalho_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV64AreaTrabalho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_0Z2e( true) ;
         }
         else
         {
            wb_table5_14_0Z2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratante_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratante_Codigo), 6, 0)));
         AV63InOutContratante_CNPJ = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63InOutContratante_CNPJ", AV63InOutContratante_CNPJ);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0Z2( ) ;
         WS0Z2( ) ;
         WE0Z2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205311135360");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratante.js", "?20205311135360");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_292( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_29_idx;
         edtContratante_Codigo_Internalname = "CONTRATANTE_CODIGO_"+sGXsfl_29_idx;
         edtContratante_PessoaCod_Internalname = "CONTRATANTE_PESSOACOD_"+sGXsfl_29_idx;
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ_"+sGXsfl_29_idx;
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL_"+sGXsfl_29_idx;
         edtContratante_NomeFantasia_Internalname = "CONTRATANTE_NOMEFANTASIA_"+sGXsfl_29_idx;
         edtContratante_IE_Internalname = "CONTRATANTE_IE_"+sGXsfl_29_idx;
         edtContratante_AgenciaNome_Internalname = "CONTRATANTE_AGENCIANOME_"+sGXsfl_29_idx;
         edtContratante_AgenciaNro_Internalname = "CONTRATANTE_AGENCIANRO_"+sGXsfl_29_idx;
         edtContratante_BancoNome_Internalname = "CONTRATANTE_BANCONOME_"+sGXsfl_29_idx;
         edtContratante_BancoNro_Internalname = "CONTRATANTE_BANCONRO_"+sGXsfl_29_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_29_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_29_idx;
         chkContratante_Ativo_Internalname = "CONTRATANTE_ATIVO_"+sGXsfl_29_idx;
         edtContratante_EmailSdaHost_Internalname = "CONTRATANTE_EMAILSDAHOST_"+sGXsfl_29_idx;
         edtContratante_EmailSdaUser_Internalname = "CONTRATANTE_EMAILSDAUSER_"+sGXsfl_29_idx;
         edtContratante_EmailSdaPass_Internalname = "CONTRATANTE_EMAILSDAPASS_"+sGXsfl_29_idx;
         edtContratante_EmailSdaKey_Internalname = "CONTRATANTE_EMAILSDAKEY_"+sGXsfl_29_idx;
         cmbContratante_EmailSdaAut_Internalname = "CONTRATANTE_EMAILSDAAUT_"+sGXsfl_29_idx;
      }

      protected void SubsflControlProps_fel_292( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_29_fel_idx;
         edtContratante_Codigo_Internalname = "CONTRATANTE_CODIGO_"+sGXsfl_29_fel_idx;
         edtContratante_PessoaCod_Internalname = "CONTRATANTE_PESSOACOD_"+sGXsfl_29_fel_idx;
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ_"+sGXsfl_29_fel_idx;
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL_"+sGXsfl_29_fel_idx;
         edtContratante_NomeFantasia_Internalname = "CONTRATANTE_NOMEFANTASIA_"+sGXsfl_29_fel_idx;
         edtContratante_IE_Internalname = "CONTRATANTE_IE_"+sGXsfl_29_fel_idx;
         edtContratante_AgenciaNome_Internalname = "CONTRATANTE_AGENCIANOME_"+sGXsfl_29_fel_idx;
         edtContratante_AgenciaNro_Internalname = "CONTRATANTE_AGENCIANRO_"+sGXsfl_29_fel_idx;
         edtContratante_BancoNome_Internalname = "CONTRATANTE_BANCONOME_"+sGXsfl_29_fel_idx;
         edtContratante_BancoNro_Internalname = "CONTRATANTE_BANCONRO_"+sGXsfl_29_fel_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_29_fel_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_29_fel_idx;
         chkContratante_Ativo_Internalname = "CONTRATANTE_ATIVO_"+sGXsfl_29_fel_idx;
         edtContratante_EmailSdaHost_Internalname = "CONTRATANTE_EMAILSDAHOST_"+sGXsfl_29_fel_idx;
         edtContratante_EmailSdaUser_Internalname = "CONTRATANTE_EMAILSDAUSER_"+sGXsfl_29_fel_idx;
         edtContratante_EmailSdaPass_Internalname = "CONTRATANTE_EMAILSDAPASS_"+sGXsfl_29_fel_idx;
         edtContratante_EmailSdaKey_Internalname = "CONTRATANTE_EMAILSDAKEY_"+sGXsfl_29_fel_idx;
         cmbContratante_EmailSdaAut_Internalname = "CONTRATANTE_EMAILSDAAUT_"+sGXsfl_29_fel_idx;
      }

      protected void sendrow_292( )
      {
         SubsflControlProps_292( ) ;
         WB0Z0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_29_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_29_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_29_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 30,'',false,'',29)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV31Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV118Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV118Select_GXI : context.PathToRelativeUrl( AV31Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_29_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV31Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_CNPJ_Internalname,(String)A12Contratante_CNPJ,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_CNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_RazaoSocial_Internalname,StringUtil.RTrim( A9Contratante_RazaoSocial),StringUtil.RTrim( context.localUtil.Format( A9Contratante_RazaoSocial, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_RazaoSocial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_NomeFantasia_Internalname,StringUtil.RTrim( A10Contratante_NomeFantasia),StringUtil.RTrim( context.localUtil.Format( A10Contratante_NomeFantasia, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_NomeFantasia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_IE_Internalname,StringUtil.RTrim( A11Contratante_IE),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_IE_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"IE",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_AgenciaNome_Internalname,StringUtil.RTrim( A336Contratante_AgenciaNome),StringUtil.RTrim( context.localUtil.Format( A336Contratante_AgenciaNome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_AgenciaNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_AgenciaNro_Internalname,StringUtil.RTrim( A337Contratante_AgenciaNro),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_AgenciaNro_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)60,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"Agencia",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_BancoNome_Internalname,StringUtil.RTrim( A338Contratante_BancoNome),StringUtil.RTrim( context.localUtil.Format( A338Contratante_BancoNome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_BancoNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_BancoNro_Internalname,StringUtil.RTrim( A339Contratante_BancoNro),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_BancoNro_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)76,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroBanco",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMunicipio_Nome_Internalname,StringUtil.RTrim( A26Municipio_Nome),StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMunicipio_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEstado_UF_Internalname,StringUtil.RTrim( A23Estado_UF),StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEstado_UF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"UF",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContratante_Ativo_Internalname,StringUtil.BoolToStr( A30Contratante_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_EmailSdaHost_Internalname,(String)A547Contratante_EmailSdaHost,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_EmailSdaHost_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_EmailSdaUser_Internalname,(String)A548Contratante_EmailSdaUser,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_EmailSdaUser_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)29,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_EmailSdaPass_Internalname,(String)A549Contratante_EmailSdaPass,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_EmailSdaPass_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)-1,(short)0,(short)29,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_EmailSdaKey_Internalname,StringUtil.RTrim( A550Contratante_EmailSdaKey),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_EmailSdaKey_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)32,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"KeyEnc",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_29_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATANTE_EMAILSDAAUT_" + sGXsfl_29_idx;
               cmbContratante_EmailSdaAut.Name = GXCCtl;
               cmbContratante_EmailSdaAut.WebTags = "";
               cmbContratante_EmailSdaAut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbContratante_EmailSdaAut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbContratante_EmailSdaAut.ItemCount > 0 )
               {
                  A551Contratante_EmailSdaAut = StringUtil.StrToBool( cmbContratante_EmailSdaAut.getValidValue(StringUtil.BoolToStr( A551Contratante_EmailSdaAut)));
                  n551Contratante_EmailSdaAut = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratante_EmailSdaAut,(String)cmbContratante_EmailSdaAut_Internalname,StringUtil.BoolToStr( A551Contratante_EmailSdaAut),(short)1,(String)cmbContratante_EmailSdaAut_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratante_EmailSdaAut.CurrentValue = StringUtil.BoolToStr( A551Contratante_EmailSdaAut);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_EmailSdaAut_Internalname, "Values", (String)(cmbContratante_EmailSdaAut.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_CODIGO"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_PESSOACOD"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_NOMEFANTASIA"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A10Contratante_NomeFantasia, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_IE"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A11Contratante_IE, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_AGENCIANOME"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A336Contratante_AgenciaNome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_AGENCIANRO"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A337Contratante_AgenciaNro, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_BANCONOME"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A338Contratante_BancoNome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_BANCONRO"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A339Contratante_BancoNro, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_ATIVO"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, A30Contratante_Ativo));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_EMAILSDAHOST"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A547Contratante_EmailSdaHost, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_EMAILSDAUSER"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A548Contratante_EmailSdaUser, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_EMAILSDAPASS"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A549Contratante_EmailSdaPass, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_EMAILSDAKEY"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A550Contratante_EmailSdaKey, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_EMAILSDAAUT"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, A551Contratante_EmailSdaAut));
            GridContainer.AddRow(GridRow);
            nGXsfl_29_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_29_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_29_idx+1));
            sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
            SubsflControlProps_292( ) ;
         }
         /* End function sendrow_292 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         lblFiltertextareatrabalho_codigo_Internalname = "FILTERTEXTAREATRABALHO_CODIGO";
         edtavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratante_Codigo_Internalname = "CONTRATANTE_CODIGO";
         edtContratante_PessoaCod_Internalname = "CONTRATANTE_PESSOACOD";
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ";
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL";
         edtContratante_NomeFantasia_Internalname = "CONTRATANTE_NOMEFANTASIA";
         edtContratante_IE_Internalname = "CONTRATANTE_IE";
         edtContratante_AgenciaNome_Internalname = "CONTRATANTE_AGENCIANOME";
         edtContratante_AgenciaNro_Internalname = "CONTRATANTE_AGENCIANRO";
         edtContratante_BancoNome_Internalname = "CONTRATANTE_BANCONOME";
         edtContratante_BancoNro_Internalname = "CONTRATANTE_BANCONRO";
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME";
         edtEstado_UF_Internalname = "ESTADO_UF";
         chkContratante_Ativo_Internalname = "CONTRATANTE_ATIVO";
         edtContratante_EmailSdaHost_Internalname = "CONTRATANTE_EMAILSDAHOST";
         edtContratante_EmailSdaUser_Internalname = "CONTRATANTE_EMAILSDAUSER";
         edtContratante_EmailSdaPass_Internalname = "CONTRATANTE_EMAILSDAPASS";
         edtContratante_EmailSdaKey_Internalname = "CONTRATANTE_EMAILSDAKEY";
         cmbContratante_EmailSdaAut_Internalname = "CONTRATANTE_EMAILSDAAUT";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbContratante_EmailSdaAut_Jsonclick = "";
         edtContratante_EmailSdaKey_Jsonclick = "";
         edtContratante_EmailSdaPass_Jsonclick = "";
         edtContratante_EmailSdaUser_Jsonclick = "";
         edtContratante_EmailSdaHost_Jsonclick = "";
         edtEstado_UF_Jsonclick = "";
         edtMunicipio_Nome_Jsonclick = "";
         edtContratante_BancoNro_Jsonclick = "";
         edtContratante_BancoNome_Jsonclick = "";
         edtContratante_AgenciaNro_Jsonclick = "";
         edtContratante_AgenciaNome_Jsonclick = "";
         edtContratante_IE_Jsonclick = "";
         edtContratante_NomeFantasia_Jsonclick = "";
         edtContratante_RazaoSocial_Jsonclick = "";
         edtContratante_CNPJ_Jsonclick = "";
         edtContratante_PessoaCod_Jsonclick = "";
         edtContratante_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavAreatrabalho_codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         cmbContratante_EmailSdaAut_Titleformat = 0;
         edtContratante_EmailSdaKey_Titleformat = 0;
         edtContratante_EmailSdaPass_Titleformat = 0;
         edtContratante_EmailSdaUser_Titleformat = 0;
         edtContratante_EmailSdaHost_Titleformat = 0;
         chkContratante_Ativo_Titleformat = 0;
         edtEstado_UF_Titleformat = 0;
         edtMunicipio_Nome_Titleformat = 0;
         edtContratante_BancoNro_Titleformat = 0;
         edtContratante_BancoNome_Titleformat = 0;
         edtContratante_AgenciaNro_Titleformat = 0;
         edtContratante_AgenciaNome_Titleformat = 0;
         edtContratante_IE_Titleformat = 0;
         edtContratante_NomeFantasia_Titleformat = 0;
         edtContratante_RazaoSocial_Titleformat = 0;
         edtContratante_CNPJ_Titleformat = 0;
         edtContratante_PessoaCod_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbContratante_EmailSdaAut.Title.Text = "Autentica��o";
         edtContratante_EmailSdaKey_Title = "Key";
         edtContratante_EmailSdaPass_Title = "Senha";
         edtContratante_EmailSdaUser_Title = "Usu�rio";
         edtContratante_EmailSdaHost_Title = "SMTP";
         chkContratante_Ativo.Title.Text = "Ativo?";
         edtEstado_UF_Title = "UF";
         edtMunicipio_Nome_Title = "Munic�pio";
         edtContratante_BancoNro_Title = "Nro";
         edtContratante_BancoNome_Title = "Nome";
         edtContratante_AgenciaNro_Title = "Nro";
         edtContratante_AgenciaNome_Title = "Nome";
         edtContratante_IE_Title = "Insc. Estadual";
         edtContratante_NomeFantasia_Title = "Nome Fantasia";
         edtContratante_RazaoSocial_Title = "Raz�o Social";
         edtContratante_CNPJ_Title = "CNPJ";
         edtContratante_PessoaCod_Title = "Jur�dica";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkContratante_Ativo.Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contratante";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'edtContratante_PessoaCod_Titleformat',ctrl:'CONTRATANTE_PESSOACOD',prop:'Titleformat'},{av:'edtContratante_PessoaCod_Title',ctrl:'CONTRATANTE_PESSOACOD',prop:'Title'},{av:'edtContratante_CNPJ_Titleformat',ctrl:'CONTRATANTE_CNPJ',prop:'Titleformat'},{av:'edtContratante_CNPJ_Title',ctrl:'CONTRATANTE_CNPJ',prop:'Title'},{av:'edtContratante_RazaoSocial_Titleformat',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Titleformat'},{av:'edtContratante_RazaoSocial_Title',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Title'},{av:'edtContratante_NomeFantasia_Titleformat',ctrl:'CONTRATANTE_NOMEFANTASIA',prop:'Titleformat'},{av:'edtContratante_NomeFantasia_Title',ctrl:'CONTRATANTE_NOMEFANTASIA',prop:'Title'},{av:'edtContratante_IE_Titleformat',ctrl:'CONTRATANTE_IE',prop:'Titleformat'},{av:'edtContratante_IE_Title',ctrl:'CONTRATANTE_IE',prop:'Title'},{av:'edtContratante_AgenciaNome_Titleformat',ctrl:'CONTRATANTE_AGENCIANOME',prop:'Titleformat'},{av:'edtContratante_AgenciaNome_Title',ctrl:'CONTRATANTE_AGENCIANOME',prop:'Title'},{av:'edtContratante_AgenciaNro_Titleformat',ctrl:'CONTRATANTE_AGENCIANRO',prop:'Titleformat'},{av:'edtContratante_AgenciaNro_Title',ctrl:'CONTRATANTE_AGENCIANRO',prop:'Title'},{av:'edtContratante_BancoNome_Titleformat',ctrl:'CONTRATANTE_BANCONOME',prop:'Titleformat'},{av:'edtContratante_BancoNome_Title',ctrl:'CONTRATANTE_BANCONOME',prop:'Title'},{av:'edtContratante_BancoNro_Titleformat',ctrl:'CONTRATANTE_BANCONRO',prop:'Titleformat'},{av:'edtContratante_BancoNro_Title',ctrl:'CONTRATANTE_BANCONRO',prop:'Title'},{av:'edtMunicipio_Nome_Titleformat',ctrl:'MUNICIPIO_NOME',prop:'Titleformat'},{av:'edtMunicipio_Nome_Title',ctrl:'MUNICIPIO_NOME',prop:'Title'},{av:'edtEstado_UF_Titleformat',ctrl:'ESTADO_UF',prop:'Titleformat'},{av:'edtEstado_UF_Title',ctrl:'ESTADO_UF',prop:'Title'},{av:'chkContratante_Ativo_Titleformat',ctrl:'CONTRATANTE_ATIVO',prop:'Titleformat'},{av:'chkContratante_Ativo.Title.Text',ctrl:'CONTRATANTE_ATIVO',prop:'Title'},{av:'edtContratante_EmailSdaHost_Titleformat',ctrl:'CONTRATANTE_EMAILSDAHOST',prop:'Titleformat'},{av:'edtContratante_EmailSdaHost_Title',ctrl:'CONTRATANTE_EMAILSDAHOST',prop:'Title'},{av:'edtContratante_EmailSdaUser_Titleformat',ctrl:'CONTRATANTE_EMAILSDAUSER',prop:'Titleformat'},{av:'edtContratante_EmailSdaUser_Title',ctrl:'CONTRATANTE_EMAILSDAUSER',prop:'Title'},{av:'edtContratante_EmailSdaPass_Titleformat',ctrl:'CONTRATANTE_EMAILSDAPASS',prop:'Titleformat'},{av:'edtContratante_EmailSdaPass_Title',ctrl:'CONTRATANTE_EMAILSDAPASS',prop:'Title'},{av:'edtContratante_EmailSdaKey_Titleformat',ctrl:'CONTRATANTE_EMAILSDAKEY',prop:'Titleformat'},{av:'edtContratante_EmailSdaKey_Title',ctrl:'CONTRATANTE_EMAILSDAKEY',prop:'Title'},{av:'cmbContratante_EmailSdaAut'},{av:'AV114GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV115GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E110Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E150Z2',iparms:[],oparms:[{av:'AV31Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E160Z2',iparms:[{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A12Contratante_CNPJ',fld:'CONTRATANTE_CNPJ',pic:'',nv:''}],oparms:[{av:'AV7InOutContratante_Codigo',fld:'vINOUTCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV63InOutContratante_CNPJ',fld:'vINOUTCONTRATANTE_CNPJ',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E120Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV63InOutContratante_CNPJ = "";
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Select = "";
         AV118Select_GXI = "";
         A12Contratante_CNPJ = "";
         A9Contratante_RazaoSocial = "";
         A10Contratante_NomeFantasia = "";
         A11Contratante_IE = "";
         A336Contratante_AgenciaNome = "";
         A337Contratante_AgenciaNro = "";
         A338Contratante_BancoNome = "";
         A339Contratante_BancoNro = "";
         A26Municipio_Nome = "";
         A23Estado_UF = "";
         A547Contratante_EmailSdaHost = "";
         A548Contratante_EmailSdaUser = "";
         A549Contratante_EmailSdaPass = "";
         A550Contratante_EmailSdaKey = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H000Z2_A25Municipio_Codigo = new int[1] ;
         H000Z2_n25Municipio_Codigo = new bool[] {false} ;
         H000Z2_A551Contratante_EmailSdaAut = new bool[] {false} ;
         H000Z2_n551Contratante_EmailSdaAut = new bool[] {false} ;
         H000Z2_A550Contratante_EmailSdaKey = new String[] {""} ;
         H000Z2_n550Contratante_EmailSdaKey = new bool[] {false} ;
         H000Z2_A549Contratante_EmailSdaPass = new String[] {""} ;
         H000Z2_n549Contratante_EmailSdaPass = new bool[] {false} ;
         H000Z2_A548Contratante_EmailSdaUser = new String[] {""} ;
         H000Z2_n548Contratante_EmailSdaUser = new bool[] {false} ;
         H000Z2_A547Contratante_EmailSdaHost = new String[] {""} ;
         H000Z2_n547Contratante_EmailSdaHost = new bool[] {false} ;
         H000Z2_A30Contratante_Ativo = new bool[] {false} ;
         H000Z2_A23Estado_UF = new String[] {""} ;
         H000Z2_A26Municipio_Nome = new String[] {""} ;
         H000Z2_A339Contratante_BancoNro = new String[] {""} ;
         H000Z2_n339Contratante_BancoNro = new bool[] {false} ;
         H000Z2_A338Contratante_BancoNome = new String[] {""} ;
         H000Z2_n338Contratante_BancoNome = new bool[] {false} ;
         H000Z2_A337Contratante_AgenciaNro = new String[] {""} ;
         H000Z2_n337Contratante_AgenciaNro = new bool[] {false} ;
         H000Z2_A336Contratante_AgenciaNome = new String[] {""} ;
         H000Z2_n336Contratante_AgenciaNome = new bool[] {false} ;
         H000Z2_A11Contratante_IE = new String[] {""} ;
         H000Z2_A10Contratante_NomeFantasia = new String[] {""} ;
         H000Z2_A9Contratante_RazaoSocial = new String[] {""} ;
         H000Z2_n9Contratante_RazaoSocial = new bool[] {false} ;
         H000Z2_A12Contratante_CNPJ = new String[] {""} ;
         H000Z2_n12Contratante_CNPJ = new bool[] {false} ;
         H000Z2_A335Contratante_PessoaCod = new int[1] ;
         H000Z2_A29Contratante_Codigo = new int[1] ;
         H000Z3_AGRID_nRecordCount = new long[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         TempTags = "";
         lblFiltertextareatrabalho_codigo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ClassString = "";
         StyleString = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratante__default(),
            new Object[][] {
                new Object[] {
               H000Z2_A25Municipio_Codigo, H000Z2_n25Municipio_Codigo, H000Z2_A551Contratante_EmailSdaAut, H000Z2_n551Contratante_EmailSdaAut, H000Z2_A550Contratante_EmailSdaKey, H000Z2_n550Contratante_EmailSdaKey, H000Z2_A549Contratante_EmailSdaPass, H000Z2_n549Contratante_EmailSdaPass, H000Z2_A548Contratante_EmailSdaUser, H000Z2_n548Contratante_EmailSdaUser,
               H000Z2_A547Contratante_EmailSdaHost, H000Z2_n547Contratante_EmailSdaHost, H000Z2_A30Contratante_Ativo, H000Z2_A23Estado_UF, H000Z2_A26Municipio_Nome, H000Z2_A339Contratante_BancoNro, H000Z2_n339Contratante_BancoNro, H000Z2_A338Contratante_BancoNome, H000Z2_n338Contratante_BancoNome, H000Z2_A337Contratante_AgenciaNro,
               H000Z2_n337Contratante_AgenciaNro, H000Z2_A336Contratante_AgenciaNome, H000Z2_n336Contratante_AgenciaNome, H000Z2_A11Contratante_IE, H000Z2_A10Contratante_NomeFantasia, H000Z2_A9Contratante_RazaoSocial, H000Z2_n9Contratante_RazaoSocial, H000Z2_A12Contratante_CNPJ, H000Z2_n12Contratante_CNPJ, H000Z2_A335Contratante_PessoaCod,
               H000Z2_A29Contratante_Codigo
               }
               , new Object[] {
               H000Z3_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_29 ;
      private short nGXsfl_29_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_29_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratante_PessoaCod_Titleformat ;
      private short edtContratante_CNPJ_Titleformat ;
      private short edtContratante_RazaoSocial_Titleformat ;
      private short edtContratante_NomeFantasia_Titleformat ;
      private short edtContratante_IE_Titleformat ;
      private short edtContratante_AgenciaNome_Titleformat ;
      private short edtContratante_AgenciaNro_Titleformat ;
      private short edtContratante_BancoNome_Titleformat ;
      private short edtContratante_BancoNro_Titleformat ;
      private short edtMunicipio_Nome_Titleformat ;
      private short edtEstado_UF_Titleformat ;
      private short chkContratante_Ativo_Titleformat ;
      private short edtContratante_EmailSdaHost_Titleformat ;
      private short edtContratante_EmailSdaUser_Titleformat ;
      private short edtContratante_EmailSdaPass_Titleformat ;
      private short edtContratante_EmailSdaKey_Titleformat ;
      private short cmbContratante_EmailSdaAut_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratante_Codigo ;
      private int wcpOAV7InOutContratante_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A29Contratante_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A25Municipio_Codigo ;
      private int AV64AreaTrabalho_Codigo ;
      private int edtavOrdereddsc_Visible ;
      private int AV113PageToGo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV114GridCurrentPage ;
      private long AV115GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_29_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratante_Codigo_Internalname ;
      private String edtContratante_PessoaCod_Internalname ;
      private String edtContratante_CNPJ_Internalname ;
      private String A9Contratante_RazaoSocial ;
      private String edtContratante_RazaoSocial_Internalname ;
      private String A10Contratante_NomeFantasia ;
      private String edtContratante_NomeFantasia_Internalname ;
      private String A11Contratante_IE ;
      private String edtContratante_IE_Internalname ;
      private String A336Contratante_AgenciaNome ;
      private String edtContratante_AgenciaNome_Internalname ;
      private String A337Contratante_AgenciaNro ;
      private String edtContratante_AgenciaNro_Internalname ;
      private String A338Contratante_BancoNome ;
      private String edtContratante_BancoNome_Internalname ;
      private String A339Contratante_BancoNro ;
      private String edtContratante_BancoNro_Internalname ;
      private String A26Municipio_Nome ;
      private String edtMunicipio_Nome_Internalname ;
      private String A23Estado_UF ;
      private String edtEstado_UF_Internalname ;
      private String chkContratante_Ativo_Internalname ;
      private String edtContratante_EmailSdaHost_Internalname ;
      private String edtContratante_EmailSdaUser_Internalname ;
      private String edtContratante_EmailSdaPass_Internalname ;
      private String A550Contratante_EmailSdaKey ;
      private String edtContratante_EmailSdaKey_Internalname ;
      private String cmbContratante_EmailSdaAut_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavAreatrabalho_codigo_Internalname ;
      private String edtContratante_PessoaCod_Title ;
      private String edtContratante_CNPJ_Title ;
      private String edtContratante_RazaoSocial_Title ;
      private String edtContratante_NomeFantasia_Title ;
      private String edtContratante_IE_Title ;
      private String edtContratante_AgenciaNome_Title ;
      private String edtContratante_AgenciaNro_Title ;
      private String edtContratante_BancoNome_Title ;
      private String edtContratante_BancoNro_Title ;
      private String edtMunicipio_Nome_Title ;
      private String edtEstado_UF_Title ;
      private String edtContratante_EmailSdaHost_Title ;
      private String edtContratante_EmailSdaUser_Title ;
      private String edtContratante_EmailSdaPass_Title ;
      private String edtContratante_EmailSdaKey_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String TempTags ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextareatrabalho_codigo_Internalname ;
      private String lblFiltertextareatrabalho_codigo_Jsonclick ;
      private String edtavAreatrabalho_codigo_Jsonclick ;
      private String sGXsfl_29_fel_idx="0001" ;
      private String ClassString ;
      private String StyleString ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratante_Codigo_Jsonclick ;
      private String edtContratante_PessoaCod_Jsonclick ;
      private String edtContratante_CNPJ_Jsonclick ;
      private String edtContratante_RazaoSocial_Jsonclick ;
      private String edtContratante_NomeFantasia_Jsonclick ;
      private String edtContratante_IE_Jsonclick ;
      private String edtContratante_AgenciaNome_Jsonclick ;
      private String edtContratante_AgenciaNro_Jsonclick ;
      private String edtContratante_BancoNome_Jsonclick ;
      private String edtContratante_BancoNro_Jsonclick ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String edtEstado_UF_Jsonclick ;
      private String edtContratante_EmailSdaHost_Jsonclick ;
      private String edtContratante_EmailSdaUser_Jsonclick ;
      private String edtContratante_EmailSdaPass_Jsonclick ;
      private String edtContratante_EmailSdaKey_Jsonclick ;
      private String cmbContratante_EmailSdaAut_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n12Contratante_CNPJ ;
      private bool n9Contratante_RazaoSocial ;
      private bool n336Contratante_AgenciaNome ;
      private bool n337Contratante_AgenciaNro ;
      private bool n338Contratante_BancoNome ;
      private bool n339Contratante_BancoNro ;
      private bool A30Contratante_Ativo ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n549Contratante_EmailSdaPass ;
      private bool n550Contratante_EmailSdaKey ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n25Municipio_Codigo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Select_IsBlob ;
      private String AV63InOutContratante_CNPJ ;
      private String wcpOAV63InOutContratante_CNPJ ;
      private String AV118Select_GXI ;
      private String A12Contratante_CNPJ ;
      private String A547Contratante_EmailSdaHost ;
      private String A548Contratante_EmailSdaUser ;
      private String A549Contratante_EmailSdaPass ;
      private String AV31Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratante_Codigo ;
      private String aP1_InOutContratante_CNPJ ;
      private GXCombobox cmbavOrderedby ;
      private GXCheckbox chkContratante_Ativo ;
      private GXCombobox cmbContratante_EmailSdaAut ;
      private IDataStoreProvider pr_default ;
      private int[] H000Z2_A25Municipio_Codigo ;
      private bool[] H000Z2_n25Municipio_Codigo ;
      private bool[] H000Z2_A551Contratante_EmailSdaAut ;
      private bool[] H000Z2_n551Contratante_EmailSdaAut ;
      private String[] H000Z2_A550Contratante_EmailSdaKey ;
      private bool[] H000Z2_n550Contratante_EmailSdaKey ;
      private String[] H000Z2_A549Contratante_EmailSdaPass ;
      private bool[] H000Z2_n549Contratante_EmailSdaPass ;
      private String[] H000Z2_A548Contratante_EmailSdaUser ;
      private bool[] H000Z2_n548Contratante_EmailSdaUser ;
      private String[] H000Z2_A547Contratante_EmailSdaHost ;
      private bool[] H000Z2_n547Contratante_EmailSdaHost ;
      private bool[] H000Z2_A30Contratante_Ativo ;
      private String[] H000Z2_A23Estado_UF ;
      private String[] H000Z2_A26Municipio_Nome ;
      private String[] H000Z2_A339Contratante_BancoNro ;
      private bool[] H000Z2_n339Contratante_BancoNro ;
      private String[] H000Z2_A338Contratante_BancoNome ;
      private bool[] H000Z2_n338Contratante_BancoNome ;
      private String[] H000Z2_A337Contratante_AgenciaNro ;
      private bool[] H000Z2_n337Contratante_AgenciaNro ;
      private String[] H000Z2_A336Contratante_AgenciaNome ;
      private bool[] H000Z2_n336Contratante_AgenciaNome ;
      private String[] H000Z2_A11Contratante_IE ;
      private String[] H000Z2_A10Contratante_NomeFantasia ;
      private String[] H000Z2_A9Contratante_RazaoSocial ;
      private bool[] H000Z2_n9Contratante_RazaoSocial ;
      private String[] H000Z2_A12Contratante_CNPJ ;
      private bool[] H000Z2_n12Contratante_CNPJ ;
      private int[] H000Z2_A335Contratante_PessoaCod ;
      private int[] H000Z2_A29Contratante_Codigo ;
      private long[] H000Z3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class promptcontratante__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H000Z2( IGxContext context ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Municipio_Codigo], T1.[Contratante_EmailSdaAut], T1.[Contratante_EmailSdaKey], T1.[Contratante_EmailSdaPass], T1.[Contratante_EmailSdaUser], T1.[Contratante_EmailSdaHost], T1.[Contratante_Ativo], T2.[Estado_UF], T2.[Municipio_Nome], T1.[Contratante_BancoNro], T1.[Contratante_BancoNome], T1.[Contratante_AgenciaNro], T1.[Contratante_AgenciaNome], T1.[Contratante_IE], T1.[Contratante_NomeFantasia], T3.[Pessoa_Nome] AS Contratante_RazaoSocial, T3.[Pessoa_Docto] AS Contratante_CNPJ, T1.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[Contratante_Codigo]";
         sFromString = " FROM (([Contratante] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Municipio_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T1.[Contratante_PessoaCod])";
         sOrderString = "";
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_PessoaCod]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_PessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_NomeFantasia]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_NomeFantasia] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_IE]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_IE] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_AgenciaNome]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_AgenciaNome] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_AgenciaNro]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_AgenciaNro] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_BancoNome]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_BancoNome] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_BancoNro]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_BancoNro] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Municipio_Nome]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Municipio_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Estado_UF]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Estado_UF] DESC";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_Ativo]";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_Ativo] DESC";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_EmailSdaHost]";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_EmailSdaHost] DESC";
         }
         else if ( ( AV13OrderedBy == 14 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_EmailSdaUser]";
         }
         else if ( ( AV13OrderedBy == 14 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_EmailSdaUser] DESC";
         }
         else if ( ( AV13OrderedBy == 15 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_EmailSdaPass]";
         }
         else if ( ( AV13OrderedBy == 15 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_EmailSdaPass] DESC";
         }
         else if ( ( AV13OrderedBy == 16 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_EmailSdaKey]";
         }
         else if ( ( AV13OrderedBy == 16 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_EmailSdaKey] DESC";
         }
         else if ( ( AV13OrderedBy == 17 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_EmailSdaAut]";
         }
         else if ( ( AV13OrderedBy == 17 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_EmailSdaAut] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H000Z3( IGxContext context ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([Contratante] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Municipio_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T1.[Contratante_PessoaCod])";
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 14 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 14 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 15 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 15 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 16 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 16 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 17 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 17 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object3[0] = scmdbuf;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H000Z2(context, (short)dynConstraints[0] , (bool)dynConstraints[1] );
               case 1 :
                     return conditional_H000Z3(context, (short)dynConstraints[0] , (bool)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000Z2 ;
          prmH000Z2 = new Object[] {
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH000Z3 ;
          prmH000Z3 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H000Z2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000Z2,11,0,true,false )
             ,new CursorDef("H000Z3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000Z3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 32) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((bool[]) buf[12])[0] = rslt.getBool(7) ;
                ((String[]) buf[13])[0] = rslt.getString(8, 2) ;
                ((String[]) buf[14])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[15])[0] = rslt.getString(10, 6) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 10) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 15) ;
                ((String[]) buf[24])[0] = rslt.getString(15, 100) ;
                ((String[]) buf[25])[0] = rslt.getString(16, 100) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((int[]) buf[29])[0] = rslt.getInt(18) ;
                ((int[]) buf[30])[0] = rslt.getInt(19) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                return;
       }
    }

 }

}
