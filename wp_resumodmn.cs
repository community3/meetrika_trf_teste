/*
               File: WP_ResumoDmn
        Description: Resumo geral
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:24:3.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_resumodmn : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_resumodmn( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_resumodmn( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavTodos = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_35 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_35_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_35_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV14Context);
               A63ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
               A60ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
               AV24Todos = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Todos", AV24Todos);
               AV5Solicitadas = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSolicitadas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Solicitadas), 4, 0)));
               AV6Rejeitadas = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRejeitadas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Rejeitadas), 4, 0)));
               AV7EmAnalise = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmanalise_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7EmAnalise), 4, 0)));
               AV8EmExecucao = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmexecucao_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV8EmExecucao), 4, 0)));
               AV9EmStandBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmstandby_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9EmStandBy), 4, 0)));
               AV10Divergencias = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDivergencias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Divergencias), 4, 0)));
               AV23TotalGeral = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TotalGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TotalGeral), 4, 0)));
               A1013Contrato_PrepostoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1013Contrato_PrepostoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
               A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               A1079ContratoGestor_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
               A1136ContratoGestor_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1136ContratoGestor_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1136ContratoGestor_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0)));
               A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
               A66ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
               A29Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n29Contratante_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
               AV28Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Contratante_Codigo), 6, 0)));
               A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               A6AreaTrabalho_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n52Contratada_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               A531ContagemResultado_StatusUltCnt = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n531ContagemResultado_StatusUltCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A531ContagemResultado_StatusUltCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0)));
               AV20AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20AreaTrabalho_Codigo), 6, 0)));
               A890ContagemResultado_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n890ContagemResultado_Responsavel = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
               A1229ContagemResultado_ContratadaDoResponsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1229ContagemResultado_ContratadaDoResponsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1229ContagemResultado_ContratadaDoResponsavel), 6, 0)));
               AV27Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contratada_Codigo), 6, 0)));
               A1236ContagemResultado_ContratanteDoResponsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1236ContagemResultado_ContratanteDoResponsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1236ContagemResultado_ContratanteDoResponsavel), 6, 0)));
               A53Contratada_AreaTrabalhoDes = GetNextPar( );
               n53Contratada_AreaTrabalhoDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A53Contratada_AreaTrabalhoDes", A53Contratada_AreaTrabalhoDes);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV14Context, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, AV24Todos, AV5Solicitadas, AV6Rejeitadas, AV7EmAnalise, AV8EmExecucao, AV9EmStandBy, AV10Divergencias, AV23TotalGeral, A1013Contrato_PrepostoCod, A39Contratada_Codigo, A1079ContratoGestor_UsuarioCod, A1136ContratoGestor_ContratadaCod, A69ContratadaUsuario_UsuarioCod, A66ContratadaUsuario_ContratadaCod, A29Contratante_Codigo, AV28Contratante_Codigo, A5AreaTrabalho_Codigo, A6AreaTrabalho_Descricao, A52Contratada_AreaTrabalhoCod, A484ContagemResultado_StatusDmn, A531ContagemResultado_StatusUltCnt, AV20AreaTrabalho_Codigo, A890ContagemResultado_Responsavel, A1229ContagemResultado_ContratadaDoResponsavel, AV27Contratada_Codigo, A1236ContagemResultado_ContratanteDoResponsavel, A53Contratada_AreaTrabalhoDes) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridsematriuir") == 0 )
            {
               nRC_GXsfl_45 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_45_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_45_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridsematriuir_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridsematriuir") == 0 )
            {
               subGridsematriuir_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV14Context);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV29Contratadas);
               AV23TotalGeral = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TotalGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TotalGeral), 4, 0)));
               AV31SemAtribuir = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSematribuir_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV31SemAtribuir), 4, 0)));
               A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               AV27Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contratada_Codigo), 6, 0)));
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n52Contratada_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               A53Contratada_AreaTrabalhoDes = GetNextPar( );
               n53Contratada_AreaTrabalhoDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A53Contratada_AreaTrabalhoDes", A53Contratada_AreaTrabalhoDes);
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               AV20AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20AreaTrabalho_Codigo), 6, 0)));
               A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n490ContagemResultado_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
               A890ContagemResultado_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n890ContagemResultado_Responsavel = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridsematriuir_refresh( subGridsematriuir_Rows, AV14Context, AV29Contratadas, AV23TotalGeral, AV31SemAtribuir, A39Contratada_Codigo, AV27Contratada_Codigo, A52Contratada_AreaTrabalhoCod, A53Contratada_AreaTrabalhoDes, A484ContagemResultado_StatusDmn, AV20AreaTrabalho_Codigo, A490ContagemResultado_ContratadaCod, A890ContagemResultado_Responsavel) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAJN2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTJN2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020621624365");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_resumodmn.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            GxWebStd.gx_hidden_field( context, "_GxRefreshTimeout", "{\"Type\":\"always\",\"Time\":\"300\"}");
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_35", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_35), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_45", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_45), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTEXT", AV14Context);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTEXT", AV14Context);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTOTALGERAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23TotalGeral), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_PREPOSTOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1013Contrato_PrepostoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_DESCRICAO", A6AreaTrabalho_Descricao);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSULTCNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A531ContagemResultado_StatusUltCnt), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A890ContagemResultado_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADADORESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1229ContagemResultado_ContratadaDoResponsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATANTEDORESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1236ContagemResultado_ContratanteDoResponsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHODES", A53Contratada_AreaTrabalhoDes);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADAS", AV29Contratadas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADAS", AV29Contratadas);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSEMATRIUIR_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSEMATRIUIR_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEJN2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTJN2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_resumodmn.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ResumoDmn" ;
      }

      public override String GetPgmdesc( )
      {
         return "Resumo geral" ;
      }

      protected void WBJN0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_JN2( true) ;
         }
         else
         {
            wb_table1_2_JN2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_JN2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTJN2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Resumo geral", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPJN0( ) ;
      }

      protected void WSJN2( )
      {
         STARTJN2( ) ;
         EVTJN2( ) ;
      }

      protected void EVTJN2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VTODOS.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11JN2 */
                              E11JN2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_35_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
                              SubsflControlProps_352( ) ;
                              AV11AreaDeTrabalho = cgiGet( edtavAreadetrabalho_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho_Internalname, AV11AreaDeTrabalho);
                              AV5Solicitadas = (short)(context.localUtil.CToN( cgiGet( edtavSolicitadas_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSolicitadas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Solicitadas), 4, 0)));
                              AV6Rejeitadas = (short)(context.localUtil.CToN( cgiGet( edtavRejeitadas_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRejeitadas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Rejeitadas), 4, 0)));
                              AV7EmAnalise = (short)(context.localUtil.CToN( cgiGet( edtavEmanalise_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmanalise_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7EmAnalise), 4, 0)));
                              AV8EmExecucao = (short)(context.localUtil.CToN( cgiGet( edtavEmexecucao_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmexecucao_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV8EmExecucao), 4, 0)));
                              AV9EmStandBy = (short)(context.localUtil.CToN( cgiGet( edtavEmstandby_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmstandby_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9EmStandBy), 4, 0)));
                              AV10Divergencias = (short)(context.localUtil.CToN( cgiGet( edtavDivergencias_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDivergencias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Divergencias), 4, 0)));
                              AV22Total = (short)(context.localUtil.CToN( cgiGet( edtavTotal_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Total), 4, 0)));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12JN2 */
                                    E12JN2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13JN2 */
                                    E13JN2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14JN2 */
                                    E14JN2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Left( sEvt, 19), "GRIDSEMATRIUIR.LOAD") == 0 )
                           {
                              nGXsfl_45_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_45_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_45_idx), 4, 0)), 4, "0");
                              SubsflControlProps_457( ) ;
                              AV30AreaDeTrabalho2 = cgiGet( edtavAreadetrabalho2_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho2_Internalname, AV30AreaDeTrabalho2);
                              AV31SemAtribuir = (short)(context.localUtil.CToN( cgiGet( edtavSematribuir_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSematribuir_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV31SemAtribuir), 4, 0)));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "GRIDSEMATRIUIR.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15JN7 */
                                    E15JN7 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEJN2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAJN2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            chkavTodos.Name = "vTODOS";
            chkavTodos.WebTags = "";
            chkavTodos.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavTodos_Internalname, "TitleCaption", chkavTodos.Caption);
            chkavTodos.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = chkavTodos_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_352( ) ;
         while ( nGXsfl_35_idx <= nRC_GXsfl_35 )
         {
            sendrow_352( ) ;
            nGXsfl_35_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_35_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_35_idx+1));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxnrGridsematriuir_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_457( ) ;
         while ( nGXsfl_45_idx <= nRC_GXsfl_45 )
         {
            sendrow_457( ) ;
            nGXsfl_45_idx = (short)(((subGridsematriuir_Islastpage==1)&&(nGXsfl_45_idx+1>subGridsematriuir_Recordsperpage( )) ? 1 : nGXsfl_45_idx+1));
            sGXsfl_45_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_45_idx), 4, 0)), 4, "0");
            SubsflControlProps_457( ) ;
         }
         context.GX_webresponse.AddString(GridsematriuirContainer.ToJavascriptSource());
         /* End function gxnrGridsematriuir_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       wwpbaseobjects.SdtWWPContext AV14Context ,
                                       int A63ContratanteUsuario_ContratanteCod ,
                                       int A60ContratanteUsuario_UsuarioCod ,
                                       bool AV24Todos ,
                                       short AV5Solicitadas ,
                                       short AV6Rejeitadas ,
                                       short AV7EmAnalise ,
                                       short AV8EmExecucao ,
                                       short AV9EmStandBy ,
                                       short AV10Divergencias ,
                                       short AV23TotalGeral ,
                                       int A1013Contrato_PrepostoCod ,
                                       int A39Contratada_Codigo ,
                                       int A1079ContratoGestor_UsuarioCod ,
                                       int A1136ContratoGestor_ContratadaCod ,
                                       int A69ContratadaUsuario_UsuarioCod ,
                                       int A66ContratadaUsuario_ContratadaCod ,
                                       int A29Contratante_Codigo ,
                                       int AV28Contratante_Codigo ,
                                       int A5AreaTrabalho_Codigo ,
                                       String A6AreaTrabalho_Descricao ,
                                       int A52Contratada_AreaTrabalhoCod ,
                                       String A484ContagemResultado_StatusDmn ,
                                       short A531ContagemResultado_StatusUltCnt ,
                                       int AV20AreaTrabalho_Codigo ,
                                       int A890ContagemResultado_Responsavel ,
                                       int A1229ContagemResultado_ContratadaDoResponsavel ,
                                       int AV27Contratada_Codigo ,
                                       int A1236ContagemResultado_ContratanteDoResponsavel ,
                                       String A53Contratada_AreaTrabalhoDes )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFJN2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void gxgrGridsematriuir_refresh( int subGridsematriuir_Rows ,
                                                 wwpbaseobjects.SdtWWPContext AV14Context ,
                                                 IGxCollection AV29Contratadas ,
                                                 short AV23TotalGeral ,
                                                 short AV31SemAtribuir ,
                                                 int A39Contratada_Codigo ,
                                                 int AV27Contratada_Codigo ,
                                                 int A52Contratada_AreaTrabalhoCod ,
                                                 String A53Contratada_AreaTrabalhoDes ,
                                                 String A484ContagemResultado_StatusDmn ,
                                                 int AV20AreaTrabalho_Codigo ,
                                                 int A490ContagemResultado_ContratadaCod ,
                                                 int A890ContagemResultado_Responsavel )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Rows), 6, 0, ".", "")));
         /* Execute user event: E14JN2 */
         E14JN2 ();
         GRIDSEMATRIUIR_nCurrentRecord = 0;
         RFJN7( ) ;
         /* End function gxgrGridsematriuir_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJN2( ) ;
         RFJN7( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavAreadetrabalho_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreadetrabalho_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreadetrabalho_Enabled), 5, 0)));
         edtavSolicitadas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSolicitadas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitadas_Enabled), 5, 0)));
         edtavRejeitadas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRejeitadas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRejeitadas_Enabled), 5, 0)));
         edtavEmanalise_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmanalise_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmanalise_Enabled), 5, 0)));
         edtavEmexecucao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmexecucao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmexecucao_Enabled), 5, 0)));
         edtavEmstandby_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmstandby_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmstandby_Enabled), 5, 0)));
         edtavDivergencias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDivergencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDivergencias_Enabled), 5, 0)));
         edtavTotal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotal_Enabled), 5, 0)));
         edtavAreadetrabalho2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreadetrabalho2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreadetrabalho2_Enabled), 5, 0)));
         edtavSematribuir_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSematribuir_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSematribuir_Enabled), 5, 0)));
      }

      protected void RFJN2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 35;
         /* Execute user event: E14JN2 */
         E14JN2 ();
         nGXsfl_35_idx = 1;
         sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
         SubsflControlProps_352( ) ;
         nGXsfl_35_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_352( ) ;
            /* Execute user event: E13JN2 */
            E13JN2 ();
            if ( ( GRID_nCurrentRecord > 0 ) && ( GRID_nGridOutOfScope == 0 ) && ( nGXsfl_35_idx == 1 ) )
            {
               GRID_nCurrentRecord = 0;
               GRID_nGridOutOfScope = 1;
               subgrid_firstpage( ) ;
               /* Execute user event: E13JN2 */
               E13JN2 ();
            }
            wbEnd = 35;
            WBJN0( ) ;
         }
         nGXsfl_35_Refreshing = 0;
      }

      protected void RFJN7( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridsematriuirContainer.ClearRows();
         }
         wbStart = 45;
         nGXsfl_45_idx = 1;
         sGXsfl_45_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_45_idx), 4, 0)), 4, "0");
         SubsflControlProps_457( ) ;
         nGXsfl_45_Refreshing = 1;
         GridsematriuirContainer.AddObjectProperty("GridName", "Gridsematriuir");
         GridsematriuirContainer.AddObjectProperty("CmpContext", "");
         GridsematriuirContainer.AddObjectProperty("InMasterPage", "false");
         GridsematriuirContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridsematriuirContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridsematriuirContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridsematriuirContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Backcolorstyle), 1, 0, ".", "")));
         GridsematriuirContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Visible), 5, 0, ".", "")));
         GridsematriuirContainer.PageSize = subGridsematriuir_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_457( ) ;
            /* Execute user event: E15JN7 */
            E15JN7 ();
            if ( ( GRIDSEMATRIUIR_nCurrentRecord > 0 ) && ( GRIDSEMATRIUIR_nGridOutOfScope == 0 ) && ( nGXsfl_45_idx == 1 ) )
            {
               GRIDSEMATRIUIR_nCurrentRecord = 0;
               GRIDSEMATRIUIR_nGridOutOfScope = 1;
               subgridsematriuir_firstpage( ) ;
               /* Execute user event: E15JN7 */
               E15JN7 ();
            }
            wbEnd = 45;
            WBJN0( ) ;
         }
         nGXsfl_45_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14Context, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, AV24Todos, AV5Solicitadas, AV6Rejeitadas, AV7EmAnalise, AV8EmExecucao, AV9EmStandBy, AV10Divergencias, AV23TotalGeral, A1013Contrato_PrepostoCod, A39Contratada_Codigo, A1079ContratoGestor_UsuarioCod, A1136ContratoGestor_ContratadaCod, A69ContratadaUsuario_UsuarioCod, A66ContratadaUsuario_ContratadaCod, A29Contratante_Codigo, AV28Contratante_Codigo, A5AreaTrabalho_Codigo, A6AreaTrabalho_Descricao, A52Contratada_AreaTrabalhoCod, A484ContagemResultado_StatusDmn, A531ContagemResultado_StatusUltCnt, AV20AreaTrabalho_Codigo, A890ContagemResultado_Responsavel, A1229ContagemResultado_ContratadaDoResponsavel, AV27Contratada_Codigo, A1236ContagemResultado_ContratanteDoResponsavel, A53Contratada_AreaTrabalhoDes) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14Context, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, AV24Todos, AV5Solicitadas, AV6Rejeitadas, AV7EmAnalise, AV8EmExecucao, AV9EmStandBy, AV10Divergencias, AV23TotalGeral, A1013Contrato_PrepostoCod, A39Contratada_Codigo, A1079ContratoGestor_UsuarioCod, A1136ContratoGestor_ContratadaCod, A69ContratadaUsuario_UsuarioCod, A66ContratadaUsuario_ContratadaCod, A29Contratante_Codigo, AV28Contratante_Codigo, A5AreaTrabalho_Codigo, A6AreaTrabalho_Descricao, A52Contratada_AreaTrabalhoCod, A484ContagemResultado_StatusDmn, A531ContagemResultado_StatusUltCnt, AV20AreaTrabalho_Codigo, A890ContagemResultado_Responsavel, A1229ContagemResultado_ContratadaDoResponsavel, AV27Contratada_Codigo, A1236ContagemResultado_ContratanteDoResponsavel, A53Contratada_AreaTrabalhoDes) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14Context, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, AV24Todos, AV5Solicitadas, AV6Rejeitadas, AV7EmAnalise, AV8EmExecucao, AV9EmStandBy, AV10Divergencias, AV23TotalGeral, A1013Contrato_PrepostoCod, A39Contratada_Codigo, A1079ContratoGestor_UsuarioCod, A1136ContratoGestor_ContratadaCod, A69ContratadaUsuario_UsuarioCod, A66ContratadaUsuario_ContratadaCod, A29Contratante_Codigo, AV28Contratante_Codigo, A5AreaTrabalho_Codigo, A6AreaTrabalho_Descricao, A52Contratada_AreaTrabalhoCod, A484ContagemResultado_StatusDmn, A531ContagemResultado_StatusUltCnt, AV20AreaTrabalho_Codigo, A890ContagemResultado_Responsavel, A1229ContagemResultado_ContratadaDoResponsavel, AV27Contratada_Codigo, A1236ContagemResultado_ContratanteDoResponsavel, A53Contratada_AreaTrabalhoDes) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14Context, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, AV24Todos, AV5Solicitadas, AV6Rejeitadas, AV7EmAnalise, AV8EmExecucao, AV9EmStandBy, AV10Divergencias, AV23TotalGeral, A1013Contrato_PrepostoCod, A39Contratada_Codigo, A1079ContratoGestor_UsuarioCod, A1136ContratoGestor_ContratadaCod, A69ContratadaUsuario_UsuarioCod, A66ContratadaUsuario_ContratadaCod, A29Contratante_Codigo, AV28Contratante_Codigo, A5AreaTrabalho_Codigo, A6AreaTrabalho_Descricao, A52Contratada_AreaTrabalhoCod, A484ContagemResultado_StatusDmn, A531ContagemResultado_StatusUltCnt, AV20AreaTrabalho_Codigo, A890ContagemResultado_Responsavel, A1229ContagemResultado_ContratadaDoResponsavel, AV27Contratada_Codigo, A1236ContagemResultado_ContratanteDoResponsavel, A53Contratada_AreaTrabalhoDes) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14Context, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, AV24Todos, AV5Solicitadas, AV6Rejeitadas, AV7EmAnalise, AV8EmExecucao, AV9EmStandBy, AV10Divergencias, AV23TotalGeral, A1013Contrato_PrepostoCod, A39Contratada_Codigo, A1079ContratoGestor_UsuarioCod, A1136ContratoGestor_ContratadaCod, A69ContratadaUsuario_UsuarioCod, A66ContratadaUsuario_ContratadaCod, A29Contratante_Codigo, AV28Contratante_Codigo, A5AreaTrabalho_Codigo, A6AreaTrabalho_Descricao, A52Contratada_AreaTrabalhoCod, A484ContagemResultado_StatusDmn, A531ContagemResultado_StatusUltCnt, AV20AreaTrabalho_Codigo, A890ContagemResultado_Responsavel, A1229ContagemResultado_ContratadaDoResponsavel, AV27Contratada_Codigo, A1236ContagemResultado_ContratanteDoResponsavel, A53Contratada_AreaTrabalhoDes) ;
         }
         return (int)(0) ;
      }

      protected int subGridsematriuir_Pagecount( )
      {
         GRIDSEMATRIUIR_nRecordCount = subGridsematriuir_Recordcount( );
         if ( ((int)((GRIDSEMATRIUIR_nRecordCount) % (subGridsematriuir_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDSEMATRIUIR_nRecordCount/ (decimal)(subGridsematriuir_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDSEMATRIUIR_nRecordCount/ (decimal)(subGridsematriuir_Recordsperpage( ))))+1) ;
      }

      protected int subGridsematriuir_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridsematriuir_Recordsperpage( )
      {
         if ( subGridsematriuir_Rows > 0 )
         {
            return subGridsematriuir_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridsematriuir_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgridsematriuir_firstpage( )
      {
         GRIDSEMATRIUIR_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSEMATRIUIR_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridsematriuir_refresh( subGridsematriuir_Rows, AV14Context, AV29Contratadas, AV23TotalGeral, AV31SemAtribuir, A39Contratada_Codigo, AV27Contratada_Codigo, A52Contratada_AreaTrabalhoCod, A53Contratada_AreaTrabalhoDes, A484ContagemResultado_StatusDmn, AV20AreaTrabalho_Codigo, A490ContagemResultado_ContratadaCod, A890ContagemResultado_Responsavel) ;
         }
         return 0 ;
      }

      protected short subgridsematriuir_nextpage( )
      {
         if ( GRIDSEMATRIUIR_nEOF == 0 )
         {
            GRIDSEMATRIUIR_nFirstRecordOnPage = (long)(GRIDSEMATRIUIR_nFirstRecordOnPage+subGridsematriuir_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSEMATRIUIR_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridsematriuir_refresh( subGridsematriuir_Rows, AV14Context, AV29Contratadas, AV23TotalGeral, AV31SemAtribuir, A39Contratada_Codigo, AV27Contratada_Codigo, A52Contratada_AreaTrabalhoCod, A53Contratada_AreaTrabalhoDes, A484ContagemResultado_StatusDmn, AV20AreaTrabalho_Codigo, A490ContagemResultado_ContratadaCod, A890ContagemResultado_Responsavel) ;
         }
         return (short)(((GRIDSEMATRIUIR_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridsematriuir_previouspage( )
      {
         if ( GRIDSEMATRIUIR_nFirstRecordOnPage >= subGridsematriuir_Recordsperpage( ) )
         {
            GRIDSEMATRIUIR_nFirstRecordOnPage = (long)(GRIDSEMATRIUIR_nFirstRecordOnPage-subGridsematriuir_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSEMATRIUIR_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridsematriuir_refresh( subGridsematriuir_Rows, AV14Context, AV29Contratadas, AV23TotalGeral, AV31SemAtribuir, A39Contratada_Codigo, AV27Contratada_Codigo, A52Contratada_AreaTrabalhoCod, A53Contratada_AreaTrabalhoDes, A484ContagemResultado_StatusDmn, AV20AreaTrabalho_Codigo, A490ContagemResultado_ContratadaCod, A890ContagemResultado_Responsavel) ;
         }
         return 0 ;
      }

      protected short subgridsematriuir_lastpage( )
      {
         subGridsematriuir_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGridsematriuir_refresh( subGridsematriuir_Rows, AV14Context, AV29Contratadas, AV23TotalGeral, AV31SemAtribuir, A39Contratada_Codigo, AV27Contratada_Codigo, A52Contratada_AreaTrabalhoCod, A53Contratada_AreaTrabalhoDes, A484ContagemResultado_StatusDmn, AV20AreaTrabalho_Codigo, A490ContagemResultado_ContratadaCod, A890ContagemResultado_Responsavel) ;
         }
         return 0 ;
      }

      protected int subgridsematriuir_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDSEMATRIUIR_nFirstRecordOnPage = (long)(subGridsematriuir_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDSEMATRIUIR_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSEMATRIUIR_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridsematriuir_refresh( subGridsematriuir_Rows, AV14Context, AV29Contratadas, AV23TotalGeral, AV31SemAtribuir, A39Contratada_Codigo, AV27Contratada_Codigo, A52Contratada_AreaTrabalhoCod, A53Contratada_AreaTrabalhoDes, A484ContagemResultado_StatusDmn, AV20AreaTrabalho_Codigo, A490ContagemResultado_ContratadaCod, A890ContagemResultado_Responsavel) ;
         }
         return (int)(0) ;
      }

      protected void STRUPJN0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavAreadetrabalho_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreadetrabalho_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreadetrabalho_Enabled), 5, 0)));
         edtavSolicitadas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSolicitadas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitadas_Enabled), 5, 0)));
         edtavRejeitadas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRejeitadas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRejeitadas_Enabled), 5, 0)));
         edtavEmanalise_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmanalise_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmanalise_Enabled), 5, 0)));
         edtavEmexecucao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmexecucao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmexecucao_Enabled), 5, 0)));
         edtavEmstandby_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmstandby_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmstandby_Enabled), 5, 0)));
         edtavDivergencias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDivergencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDivergencias_Enabled), 5, 0)));
         edtavTotal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotal_Enabled), 5, 0)));
         edtavAreadetrabalho2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreadetrabalho2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreadetrabalho2_Enabled), 5, 0)));
         edtavSematribuir_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSematribuir_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSematribuir_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12JN2 */
         E12JN2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV24Todos = StringUtil.StrToBool( cgiGet( chkavTodos_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Todos", AV24Todos);
            /* Read saved values. */
            nRC_GXsfl_35 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_35"), ",", "."));
            nRC_GXsfl_45 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_45"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRIDSEMATRIUIR_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDSEMATRIUIR_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            GRIDSEMATRIUIR_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDSEMATRIUIR_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            subGridsematriuir_Rows = (int)(context.localUtil.CToN( cgiGet( "GRIDSEMATRIUIR_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            nGXsfl_35_idx = (short)(context.localUtil.CToN( cgiGet( subGrid_Internalname+"_ROW"), ",", "."));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
            if ( nGXsfl_35_idx > 0 )
            {
               AV11AreaDeTrabalho = cgiGet( edtavAreadetrabalho_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho_Internalname, AV11AreaDeTrabalho);
               AV5Solicitadas = (short)(context.localUtil.CToN( cgiGet( edtavSolicitadas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSolicitadas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Solicitadas), 4, 0)));
               AV6Rejeitadas = (short)(context.localUtil.CToN( cgiGet( edtavRejeitadas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRejeitadas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Rejeitadas), 4, 0)));
               AV7EmAnalise = (short)(context.localUtil.CToN( cgiGet( edtavEmanalise_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmanalise_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7EmAnalise), 4, 0)));
               AV8EmExecucao = (short)(context.localUtil.CToN( cgiGet( edtavEmexecucao_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmexecucao_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV8EmExecucao), 4, 0)));
               AV9EmStandBy = (short)(context.localUtil.CToN( cgiGet( edtavEmstandby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmstandby_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9EmStandBy), 4, 0)));
               AV10Divergencias = (short)(context.localUtil.CToN( cgiGet( edtavDivergencias_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDivergencias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Divergencias), 4, 0)));
               AV22Total = (short)(context.localUtil.CToN( cgiGet( edtavTotal_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Total), 4, 0)));
            }
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12JN2 */
         E12JN2 ();
         if (returnInSub) return;
      }

      protected void E12JN2( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         subGridsematriuir_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Rows), 6, 0, ".", "")));
         subgrid_gotopage( 1) ;
         subgridsematriuir_gotopage( 1) ;
      }

      private void E13JN2( )
      {
         /* Grid_Load Routine */
         if ( AV14Context.gxTpr_Userehcontratante )
         {
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV24Todos ,
                                                 A63ContratanteUsuario_ContratanteCod ,
                                                 AV14Context.gxTpr_Contratante_codigo ,
                                                 A60ContratanteUsuario_UsuarioCod ,
                                                 AV14Context.gxTpr_Userid },
                                                 new int[] {
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT
                                                 }
            });
            /* Using cursor H00JN2 */
            pr_default.execute(0, new Object[] {AV14Context.gxTpr_Userid, AV14Context.gxTpr_Contratante_codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A60ContratanteUsuario_UsuarioCod = H00JN2_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = H00JN2_A63ContratanteUsuario_ContratanteCod[0];
               AV28Contratante_Codigo = A63ContratanteUsuario_ContratanteCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Contratante_Codigo), 6, 0)));
               /* Execute user subroutine: 'AREADACONTRATANTE' */
               S113 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'RESUMODAAREA' */
               S123 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  returnInSub = true;
                  if (true) return;
               }
               AV22Total = (short)(AV5Solicitadas+AV6Rejeitadas+AV7EmAnalise+AV8EmExecucao+AV9EmStandBy+AV10Divergencias);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Total), 4, 0)));
               AV23TotalGeral = (short)(AV23TotalGeral+AV22Total);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TotalGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TotalGeral), 4, 0)));
               if ( AV22Total > 0 )
               {
                  edtavSolicitadas_Visible = ((AV5Solicitadas>0) ? 1 : 0);
                  edtavRejeitadas_Visible = ((AV6Rejeitadas>0) ? 1 : 0);
                  edtavEmanalise_Visible = ((AV7EmAnalise>0) ? 1 : 0);
                  edtavEmexecucao_Visible = ((AV8EmExecucao>0) ? 1 : 0);
                  edtavEmstandby_Visible = ((AV9EmStandBy>0) ? 1 : 0);
                  edtavDivergencias_Visible = ((AV10Divergencias>0) ? 1 : 0);
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 35;
                  }
                  if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
                  {
                     sendrow_352( ) ;
                     GRID_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
                     {
                        GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
                     }
                  }
                  if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
                  {
                     GRID_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                  }
                  GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_35_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(35, GridRow);
                  }
               }
               pr_default.readNext(0);
            }
            pr_default.close(0);
         }
         else
         {
            if ( AV24Todos )
            {
               AV29Contratadas.Clear();
               /* Using cursor H00JN3 */
               pr_default.execute(1, new Object[] {AV14Context.gxTpr_Userid});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A1013Contrato_PrepostoCod = H00JN3_A1013Contrato_PrepostoCod[0];
                  n1013Contrato_PrepostoCod = H00JN3_n1013Contrato_PrepostoCod[0];
                  A39Contratada_Codigo = H00JN3_A39Contratada_Codigo[0];
                  AV29Contratadas.Add(A39Contratada_Codigo, 0);
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               pr_default.dynParam(2, new Object[]{ new Object[]{
                                                    A1136ContratoGestor_ContratadaCod ,
                                                    AV29Contratadas ,
                                                    AV14Context.gxTpr_Userid ,
                                                    A1079ContratoGestor_UsuarioCod },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.INT
                                                    }
               });
               /* Using cursor H00JN4 */
               pr_default.execute(2, new Object[] {AV14Context.gxTpr_Userid});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = H00JN4_A1078ContratoGestor_ContratoCod[0];
                  A1136ContratoGestor_ContratadaCod = H00JN4_A1136ContratoGestor_ContratadaCod[0];
                  n1136ContratoGestor_ContratadaCod = H00JN4_n1136ContratoGestor_ContratadaCod[0];
                  A1079ContratoGestor_UsuarioCod = H00JN4_A1079ContratoGestor_UsuarioCod[0];
                  A1136ContratoGestor_ContratadaCod = H00JN4_A1136ContratoGestor_ContratadaCod[0];
                  n1136ContratoGestor_ContratadaCod = H00JN4_n1136ContratoGestor_ContratadaCod[0];
                  AV29Contratadas.Add(A1136ContratoGestor_ContratadaCod, 0);
                  pr_default.readNext(2);
               }
               pr_default.close(2);
               AV37GXV1 = 1;
               while ( AV37GXV1 <= AV29Contratadas.Count )
               {
                  AV27Contratada_Codigo = (int)(AV29Contratadas.GetNumeric(AV37GXV1));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contratada_Codigo), 6, 0)));
                  /* Execute user subroutine: 'AREADACONTRATADA' */
                  S136 ();
                  if (returnInSub) return;
                  /* Execute user subroutine: 'RESUMODAAREA' */
                  S123 ();
                  if (returnInSub) return;
                  AV22Total = (short)(AV5Solicitadas+AV6Rejeitadas+AV7EmAnalise+AV8EmExecucao+AV9EmStandBy+AV10Divergencias);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Total), 4, 0)));
                  AV23TotalGeral = (short)(AV23TotalGeral+AV22Total);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TotalGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TotalGeral), 4, 0)));
                  if ( AV22Total > 0 )
                  {
                     edtavSolicitadas_Visible = ((AV5Solicitadas>0) ? 1 : 0);
                     edtavRejeitadas_Visible = ((AV6Rejeitadas>0) ? 1 : 0);
                     edtavEmanalise_Visible = ((AV7EmAnalise>0) ? 1 : 0);
                     edtavEmexecucao_Visible = ((AV8EmExecucao>0) ? 1 : 0);
                     edtavEmstandby_Visible = ((AV9EmStandBy>0) ? 1 : 0);
                     edtavDivergencias_Visible = ((AV10Divergencias>0) ? 1 : 0);
                     /* Load Method */
                     if ( wbStart != -1 )
                     {
                        wbStart = 35;
                     }
                     if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
                     {
                        sendrow_352( ) ;
                        GRID_nEOF = 1;
                        GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                        if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
                        {
                           GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
                        }
                     }
                     if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
                     {
                        GRID_nEOF = 0;
                        GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                     }
                     GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
                     if ( isFullAjaxMode( ) && ( nGXsfl_35_Refreshing == 0 ) )
                     {
                        context.DoAjaxLoad(35, GridRow);
                     }
                  }
                  AV37GXV1 = (int)(AV37GXV1+1);
               }
            }
            else
            {
               /* Using cursor H00JN5 */
               pr_default.execute(3, new Object[] {AV14Context.gxTpr_Userid});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A69ContratadaUsuario_UsuarioCod = H00JN5_A69ContratadaUsuario_UsuarioCod[0];
                  A66ContratadaUsuario_ContratadaCod = H00JN5_A66ContratadaUsuario_ContratadaCod[0];
                  AV27Contratada_Codigo = A66ContratadaUsuario_ContratadaCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contratada_Codigo), 6, 0)));
                  /* Execute user subroutine: 'AREADACONTRATADA' */
                  S136 ();
                  if ( returnInSub )
                  {
                     pr_default.close(3);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Execute user subroutine: 'RESUMODAAREA' */
                  S123 ();
                  if ( returnInSub )
                  {
                     pr_default.close(3);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV22Total = (short)(AV5Solicitadas+AV6Rejeitadas+AV7EmAnalise+AV8EmExecucao+AV9EmStandBy+AV10Divergencias);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Total), 4, 0)));
                  AV23TotalGeral = (short)(AV23TotalGeral+AV22Total);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TotalGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TotalGeral), 4, 0)));
                  if ( AV22Total > 0 )
                  {
                     edtavSolicitadas_Visible = ((AV5Solicitadas>0) ? 1 : 0);
                     edtavRejeitadas_Visible = ((AV6Rejeitadas>0) ? 1 : 0);
                     edtavEmanalise_Visible = ((AV7EmAnalise>0) ? 1 : 0);
                     edtavEmexecucao_Visible = ((AV8EmExecucao>0) ? 1 : 0);
                     edtavEmstandby_Visible = ((AV9EmStandBy>0) ? 1 : 0);
                     edtavDivergencias_Visible = ((AV10Divergencias>0) ? 1 : 0);
                     /* Load Method */
                     if ( wbStart != -1 )
                     {
                        wbStart = 35;
                     }
                     if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
                     {
                        sendrow_352( ) ;
                        GRID_nEOF = 1;
                        GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                        if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
                        {
                           GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
                        }
                     }
                     if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
                     {
                        GRID_nEOF = 0;
                        GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                     }
                     GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
                     if ( isFullAjaxMode( ) && ( nGXsfl_35_Refreshing == 0 ) )
                     {
                        context.DoAjaxLoad(35, GridRow);
                     }
                  }
                  pr_default.readNext(3);
               }
               pr_default.close(3);
            }
         }
         AV11AreaDeTrabalho = "� T O T A L �";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho_Internalname, AV11AreaDeTrabalho);
         edtavSolicitadas_Visible = 0;
         edtavRejeitadas_Visible = 0;
         edtavEmanalise_Visible = 0;
         edtavEmexecucao_Visible = 0;
         edtavEmstandby_Visible = 0;
         edtavDivergencias_Visible = 0;
         AV22Total = AV23TotalGeral;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Total), 4, 0)));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 35;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_352( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_35_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(35, GridRow);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29Contratadas", AV29Contratadas);
      }

      protected void E14JN2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV14Context) ;
         chkavTodos.Visible = (AV14Context.gxTpr_Userehgestor||AV14Context.gxTpr_Userehcontratante ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavTodos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavTodos.Visible), 5, 0)));
         lblTextblocktodos_Visible = (AV14Context.gxTpr_Userehgestor||AV14Context.gxTpr_Userehcontratante ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblocktodos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblocktodos_Visible), 5, 0)));
         lblTextblocktodos_Caption = (AV14Context.gxTpr_Userehcontratante ? "E todos os usu�rios do "+AV14Context.gxTpr_Areatrabalho_descricao : "E todos os usu�rios da "+AV14Context.gxTpr_Contratada_pessoanom);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblocktodos_Internalname, "Caption", lblTextblocktodos_Caption);
         AV24Todos = (bool)(((StringUtil.StrCmp(AV25WebSession.Get("Todos"), "T")==0))&&(AV14Context.gxTpr_Userehgestor||AV14Context.gxTpr_Userehcontratante));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Todos", AV24Todos);
         subGridsematriuir_Visible = (AV24Todos&&AV14Context.gxTpr_Userehcontratada ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "GridsematriuirContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subGridsematriuir_Visible), 5, 0)));
         AV23TotalGeral = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TotalGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TotalGeral), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14Context", AV14Context);
      }

      protected void E11JN2( )
      {
         /* Todos_Click Routine */
         AV25WebSession.Set("Todos", (AV24Todos ? "T" : "F"));
         context.DoAjaxRefresh();
      }

      protected void S136( )
      {
         /* 'AREADACONTRATADA' Routine */
         /* Using cursor H00JN6 */
         pr_default.execute(4, new Object[] {AV27Contratada_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A39Contratada_Codigo = H00JN6_A39Contratada_Codigo[0];
            A52Contratada_AreaTrabalhoCod = H00JN6_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00JN6_n52Contratada_AreaTrabalhoCod[0];
            A53Contratada_AreaTrabalhoDes = H00JN6_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = H00JN6_n53Contratada_AreaTrabalhoDes[0];
            A53Contratada_AreaTrabalhoDes = H00JN6_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = H00JN6_n53Contratada_AreaTrabalhoDes[0];
            AV20AreaTrabalho_Codigo = A52Contratada_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20AreaTrabalho_Codigo), 6, 0)));
            AV11AreaDeTrabalho = A53Contratada_AreaTrabalhoDes;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho_Internalname, AV11AreaDeTrabalho);
            AV30AreaDeTrabalho2 = A53Contratada_AreaTrabalhoDes;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho2_Internalname, AV30AreaDeTrabalho2);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
      }

      protected void S113( )
      {
         /* 'AREADACONTRATANTE' Routine */
         /* Using cursor H00JN7 */
         pr_default.execute(5, new Object[] {AV28Contratante_Codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A29Contratante_Codigo = H00JN7_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00JN7_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = H00JN7_A5AreaTrabalho_Codigo[0];
            A6AreaTrabalho_Descricao = H00JN7_A6AreaTrabalho_Descricao[0];
            AV20AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20AreaTrabalho_Codigo), 6, 0)));
            AV11AreaDeTrabalho = A6AreaTrabalho_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho_Internalname, AV11AreaDeTrabalho);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void S123( )
      {
         /* 'RESUMODAAREA' Routine */
         AV5Solicitadas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSolicitadas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Solicitadas), 4, 0)));
         AV6Rejeitadas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRejeitadas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Rejeitadas), 4, 0)));
         AV7EmAnalise = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmanalise_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7EmAnalise), 4, 0)));
         AV8EmExecucao = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmexecucao_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV8EmExecucao), 4, 0)));
         AV9EmStandBy = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmstandby_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9EmStandBy), 4, 0)));
         AV10Divergencias = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDivergencias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Divergencias), 4, 0)));
         AV22Total = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Total), 4, 0)));
         edtavTotal_Tooltiptext = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotal_Internalname, "Tooltiptext", edtavTotal_Tooltiptext);
         pr_default.dynParam(6, new Object[]{ new Object[]{
                                              AV24Todos ,
                                              A890ContagemResultado_Responsavel ,
                                              AV14Context.gxTpr_Userid ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV14Context.gxTpr_Userehcontratada ,
                                              A1229ContagemResultado_ContratadaDoResponsavel ,
                                              AV27Contratada_Codigo ,
                                              A1236ContagemResultado_ContratanteDoResponsavel ,
                                              AV14Context.gxTpr_Userehcontratante ,
                                              AV28Contratante_Codigo ,
                                              AV20AreaTrabalho_Codigo ,
                                              A52Contratada_AreaTrabalhoCod },
                                              new int[] {
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00JN9 */
         pr_default.execute(6, new Object[] {AV20AreaTrabalho_Codigo, AV14Context.gxTpr_Userid});
         while ( (pr_default.getStatus(6) != 101) )
         {
            BRKJN10 = false;
            A490ContagemResultado_ContratadaCod = H00JN9_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00JN9_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00JN9_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00JN9_n484ContagemResultado_StatusDmn[0];
            A531ContagemResultado_StatusUltCnt = H00JN9_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00JN9_n531ContagemResultado_StatusUltCnt[0];
            A52Contratada_AreaTrabalhoCod = H00JN9_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00JN9_n52Contratada_AreaTrabalhoCod[0];
            A890ContagemResultado_Responsavel = H00JN9_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00JN9_n890ContagemResultado_Responsavel[0];
            A456ContagemResultado_Codigo = H00JN9_A456ContagemResultado_Codigo[0];
            A52Contratada_AreaTrabalhoCod = H00JN9_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00JN9_n52Contratada_AreaTrabalhoCod[0];
            A531ContagemResultado_StatusUltCnt = H00JN9_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = H00JN9_n531ContagemResultado_StatusUltCnt[0];
            GXt_int1 = A1236ContagemResultado_ContratanteDoResponsavel;
            new prc_contratantedousuario(context ).execute( ref  A456ContagemResultado_Codigo, ref  A890ContagemResultado_Responsavel, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
            A1236ContagemResultado_ContratanteDoResponsavel = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1236ContagemResultado_ContratanteDoResponsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1236ContagemResultado_ContratanteDoResponsavel), 6, 0)));
            if ( ! ( AV14Context.gxTpr_Userehcontratada && AV24Todos ) || ( ! ( A1236ContagemResultado_ContratanteDoResponsavel > 0 ) ) )
            {
               if ( ! ( AV14Context.gxTpr_Userehcontratante && AV24Todos ) || ( ( A1236ContagemResultado_ContratanteDoResponsavel == AV28Contratante_Codigo ) ) )
               {
                  GXt_int1 = A1229ContagemResultado_ContratadaDoResponsavel;
                  new prc_contratadadousuario(context ).execute( ref  A890ContagemResultado_Responsavel, ref  A52Contratada_AreaTrabalhoCod, out  GXt_int1) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
                  A1229ContagemResultado_ContratadaDoResponsavel = GXt_int1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1229ContagemResultado_ContratadaDoResponsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1229ContagemResultado_ContratadaDoResponsavel), 6, 0)));
                  if ( ! ( AV14Context.gxTpr_Userehcontratada && AV24Todos ) || ( ( A1229ContagemResultado_ContratadaDoResponsavel == AV27Contratada_Codigo ) ) )
                  {
                     if ( ! ( AV14Context.gxTpr_Userehcontratante && AV24Todos ) || ( ! ( A1229ContagemResultado_ContratadaDoResponsavel > 0 ) ) )
                     {
                        W52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
                        n52Contratada_AreaTrabalhoCod = false;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
                        AV17StatusDmn = A484ContagemResultado_StatusDmn;
                        AV18StatusCnt = A531ContagemResultado_StatusUltCnt;
                        AV16Qtd = 0;
                        while ( (pr_default.getStatus(6) != 101) && ( H00JN9_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( StringUtil.StrCmp(H00JN9_A484ContagemResultado_StatusDmn[0], A484ContagemResultado_StatusDmn) == 0 ) && ( H00JN9_A531ContagemResultado_StatusUltCnt[0] == A531ContagemResultado_StatusUltCnt ) )
                        {
                           BRKJN10 = false;
                           A490ContagemResultado_ContratadaCod = H00JN9_A490ContagemResultado_ContratadaCod[0];
                           n490ContagemResultado_ContratadaCod = H00JN9_n490ContagemResultado_ContratadaCod[0];
                           A890ContagemResultado_Responsavel = H00JN9_A890ContagemResultado_Responsavel[0];
                           n890ContagemResultado_Responsavel = H00JN9_n890ContagemResultado_Responsavel[0];
                           A456ContagemResultado_Codigo = H00JN9_A456ContagemResultado_Codigo[0];
                           if ( ! H00JN9_n890ContagemResultado_Responsavel[0] )
                           {
                              GXt_int1 = A1229ContagemResultado_ContratadaDoResponsavel;
                              new prc_contratadadousuario(context ).execute( ref  A890ContagemResultado_Responsavel, ref  A52Contratada_AreaTrabalhoCod, out  GXt_int1) ;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
                              A1229ContagemResultado_ContratadaDoResponsavel = GXt_int1;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1229ContagemResultado_ContratadaDoResponsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1229ContagemResultado_ContratadaDoResponsavel), 6, 0)));
                              if ( ! ( AV14Context.gxTpr_Userehcontratada && AV24Todos ) || ( ( A1229ContagemResultado_ContratadaDoResponsavel == AV27Contratada_Codigo ) ) )
                              {
                                 if ( ! ( AV14Context.gxTpr_Userehcontratante && AV24Todos ) || ( ! ( A1229ContagemResultado_ContratadaDoResponsavel > 0 ) ) )
                                 {
                                    GXt_int1 = A1236ContagemResultado_ContratanteDoResponsavel;
                                    new prc_contratantedousuario(context ).execute( ref  A456ContagemResultado_Codigo, ref  A890ContagemResultado_Responsavel, out  GXt_int1) ;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
                                    A1236ContagemResultado_ContratanteDoResponsavel = GXt_int1;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1236ContagemResultado_ContratanteDoResponsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1236ContagemResultado_ContratanteDoResponsavel), 6, 0)));
                                    if ( ! ( AV14Context.gxTpr_Userehcontratada && AV24Todos ) || ( ! ( A1236ContagemResultado_ContratanteDoResponsavel > 0 ) ) )
                                    {
                                       if ( ! ( AV14Context.gxTpr_Userehcontratante && AV24Todos ) || ( ( A1236ContagemResultado_ContratanteDoResponsavel == AV28Contratante_Codigo ) ) )
                                       {
                                          AV16Qtd = (short)(AV16Qtd+1);
                                       }
                                    }
                                 }
                              }
                           }
                           BRKJN10 = true;
                           pr_default.readNext(6);
                        }
                        if ( StringUtil.StrCmp(AV17StatusDmn, "S") == 0 )
                        {
                           AV5Solicitadas = (short)(AV5Solicitadas+AV16Qtd);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSolicitadas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Solicitadas), 4, 0)));
                        }
                        else if ( StringUtil.StrCmp(AV17StatusDmn, "D") == 0 )
                        {
                           AV6Rejeitadas = (short)(AV6Rejeitadas+AV16Qtd);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRejeitadas_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Rejeitadas), 4, 0)));
                        }
                        else if ( StringUtil.StrCmp(AV17StatusDmn, "E") == 0 )
                        {
                           AV7EmAnalise = (short)(AV7EmAnalise+AV16Qtd);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmanalise_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7EmAnalise), 4, 0)));
                        }
                        else if ( StringUtil.StrCmp(AV17StatusDmn, "A") == 0 )
                        {
                           if ( AV18StatusCnt == 7 )
                           {
                              AV10Divergencias = (short)(AV10Divergencias+AV16Qtd);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDivergencias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Divergencias), 4, 0)));
                           }
                           else
                           {
                              AV8EmExecucao = (short)(AV8EmExecucao+AV16Qtd);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmexecucao_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV8EmExecucao), 4, 0)));
                           }
                        }
                        else if ( StringUtil.StrCmp(AV17StatusDmn, "B") == 0 )
                        {
                           AV9EmStandBy = (short)(AV9EmStandBy+AV16Qtd);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmstandby_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9EmStandBy), 4, 0)));
                        }
                        A52Contratada_AreaTrabalhoCod = W52Contratada_AreaTrabalhoCod;
                        n52Contratada_AreaTrabalhoCod = false;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
                     }
                  }
               }
            }
            if ( ! BRKJN10 )
            {
               BRKJN10 = true;
               pr_default.readNext(6);
            }
         }
         pr_default.close(6);
      }

      protected void S147( )
      {
         /* 'SEMATRIBUIRNAAREA' Routine */
         AV31SemAtribuir = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSematribuir_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV31SemAtribuir), 4, 0)));
         /* Optimized group. */
         /* Using cursor H00JN10 */
         pr_default.execute(7, new Object[] {AV20AreaTrabalho_Codigo, AV27Contratada_Codigo});
         cV31SemAtribuir = H00JN10_AV31SemAtribuir[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSematribuir_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(cV31SemAtribuir), 4, 0)));
         pr_default.close(7);
         AV31SemAtribuir = (short)(AV31SemAtribuir+cV31SemAtribuir*1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSematribuir_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV31SemAtribuir), 4, 0)));
         /* End optimized group. */
      }

      private void E15JN7( )
      {
         /* Gridsematriuir_Load Routine */
         if ( AV14Context.gxTpr_Userehcontratada )
         {
            AV39GXV2 = 1;
            while ( AV39GXV2 <= AV29Contratadas.Count )
            {
               AV27Contratada_Codigo = (int)(AV29Contratadas.GetNumeric(AV39GXV2));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contratada_Codigo), 6, 0)));
               /* Execute user subroutine: 'AREADACONTRATADA' */
               S136 ();
               if (returnInSub) return;
               /* Execute user subroutine: 'SEMATRIBUIRNAAREA' */
               S147 ();
               if (returnInSub) return;
               AV23TotalGeral = (short)(AV23TotalGeral+AV31SemAtribuir);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TotalGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TotalGeral), 4, 0)));
               if ( AV31SemAtribuir > 0 )
               {
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 45;
                  }
                  if ( ( subGridsematriuir_Islastpage == 1 ) || ( subGridsematriuir_Rows == 0 ) || ( ( GRIDSEMATRIUIR_nCurrentRecord >= GRIDSEMATRIUIR_nFirstRecordOnPage ) && ( GRIDSEMATRIUIR_nCurrentRecord < GRIDSEMATRIUIR_nFirstRecordOnPage + subGridsematriuir_Recordsperpage( ) ) ) )
                  {
                     sendrow_457( ) ;
                     GRIDSEMATRIUIR_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSEMATRIUIR_nEOF), 1, 0, ".", "")));
                     if ( ( subGridsematriuir_Islastpage == 1 ) && ( ((int)((GRIDSEMATRIUIR_nCurrentRecord) % (subGridsematriuir_Recordsperpage( )))) == 0 ) )
                     {
                        GRIDSEMATRIUIR_nFirstRecordOnPage = GRIDSEMATRIUIR_nCurrentRecord;
                     }
                  }
                  if ( GRIDSEMATRIUIR_nCurrentRecord >= GRIDSEMATRIUIR_nFirstRecordOnPage + subGridsematriuir_Recordsperpage( ) )
                  {
                     GRIDSEMATRIUIR_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSEMATRIUIR_nEOF), 1, 0, ".", "")));
                  }
                  GRIDSEMATRIUIR_nCurrentRecord = (long)(GRIDSEMATRIUIR_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_45_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(45, GridsematriuirRow);
                  }
               }
               AV39GXV2 = (int)(AV39GXV2+1);
            }
         }
         AV30AreaDeTrabalho2 = "� T O T A L �";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho2_Internalname, AV30AreaDeTrabalho2);
         AV31SemAtribuir = AV23TotalGeral;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSematribuir_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV31SemAtribuir), 4, 0)));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 45;
         }
         if ( ( subGridsematriuir_Islastpage == 1 ) || ( subGridsematriuir_Rows == 0 ) || ( ( GRIDSEMATRIUIR_nCurrentRecord >= GRIDSEMATRIUIR_nFirstRecordOnPage ) && ( GRIDSEMATRIUIR_nCurrentRecord < GRIDSEMATRIUIR_nFirstRecordOnPage + subGridsematriuir_Recordsperpage( ) ) ) )
         {
            sendrow_457( ) ;
            GRIDSEMATRIUIR_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSEMATRIUIR_nEOF), 1, 0, ".", "")));
            if ( ( subGridsematriuir_Islastpage == 1 ) && ( ((int)((GRIDSEMATRIUIR_nCurrentRecord) % (subGridsematriuir_Recordsperpage( )))) == 0 ) )
            {
               GRIDSEMATRIUIR_nFirstRecordOnPage = GRIDSEMATRIUIR_nCurrentRecord;
            }
         }
         if ( GRIDSEMATRIUIR_nCurrentRecord >= GRIDSEMATRIUIR_nFirstRecordOnPage + subGridsematriuir_Recordsperpage( ) )
         {
            GRIDSEMATRIUIR_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRIDSEMATRIUIR_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDSEMATRIUIR_nEOF), 1, 0, ".", "")));
         }
         GRIDSEMATRIUIR_nCurrentRecord = (long)(GRIDSEMATRIUIR_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_45_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(45, GridsematriuirRow);
         }
      }

      protected void wb_table1_2_JN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_JN2( true) ;
         }
         else
         {
            wb_table2_8_JN2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_JN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_32_JN2( true) ;
         }
         else
         {
            wb_table3_32_JN2( false) ;
         }
         return  ;
      }

      protected void wb_table3_32_JN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_50_JN2( true) ;
         }
         else
         {
            wb_table4_50_JN2( false) ;
         }
         return  ;
      }

      protected void wb_table4_50_JN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_JN2e( true) ;
         }
         else
         {
            wb_table1_2_JN2e( false) ;
         }
      }

      protected void wb_table4_50_JN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertable_Internalname, tblUsertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_50_JN2e( true) ;
         }
         else
         {
            wb_table4_50_JN2e( false) ;
         }
      }

      protected void wb_table3_32_JN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"35\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "�rea de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavSolicitadas_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Solicitadas") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavRejeitadas_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Rejeitadas") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavEmanalise_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Em An�lise") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavEmexecucao_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Em Execu��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavEmstandby_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Em Stand by") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDivergencias_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Diverg�ncias") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Total") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV11AreaDeTrabalho));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAreadetrabalho_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5Solicitadas), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSolicitadas_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSolicitadas_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6Rejeitadas), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRejeitadas_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRejeitadas_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7EmAnalise), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmanalise_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmanalise_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8EmExecucao), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmexecucao_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmexecucao_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9EmStandBy), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmstandby_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmstandby_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10Divergencias), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDivergencias_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDivergencias_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Total), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTotal_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavTotal_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 35 )
         {
            wbEnd = 0;
            nRC_GXsfl_35 = (short)(nGXsfl_35_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridsematriuirContainer.SetWrapped(nGXWrapped);
            if ( GridsematriuirContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridsematriuirContainer"+"DivS\" data-gxgridid=\"45\">") ;
               sStyleString = "";
               if ( subGridsematriuir_Visible == 0 )
               {
                  sStyleString = sStyleString + "display:none;";
               }
               GxWebStd.gx_table_start( context, subGridsematriuir_Internalname, subGridsematriuir_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridsematriuir_Backcolorstyle == 0 )
               {
                  subGridsematriuir_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridsematriuir_Class) > 0 )
                  {
                     subGridsematriuir_Linesclass = subGridsematriuir_Class+"Title";
                  }
               }
               else
               {
                  subGridsematriuir_Titlebackstyle = 1;
                  if ( subGridsematriuir_Backcolorstyle == 1 )
                  {
                     subGridsematriuir_Titlebackcolor = subGridsematriuir_Allbackcolor;
                     if ( StringUtil.Len( subGridsematriuir_Class) > 0 )
                     {
                        subGridsematriuir_Linesclass = subGridsematriuir_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridsematriuir_Class) > 0 )
                     {
                        subGridsematriuir_Linesclass = subGridsematriuir_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsematriuir_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "�rea de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridsematriuir_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Sem atribuir") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridsematriuirContainer.AddObjectProperty("GridName", "Gridsematriuir");
            }
            else
            {
               GridsematriuirContainer.AddObjectProperty("GridName", "Gridsematriuir");
               GridsematriuirContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridsematriuirContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridsematriuirContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridsematriuirContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Backcolorstyle), 1, 0, ".", "")));
               GridsematriuirContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Visible), 5, 0, ".", "")));
               GridsematriuirContainer.AddObjectProperty("CmpContext", "");
               GridsematriuirContainer.AddObjectProperty("InMasterPage", "false");
               GridsematriuirColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsematriuirColumn.AddObjectProperty("Value", StringUtil.RTrim( AV30AreaDeTrabalho2));
               GridsematriuirColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAreadetrabalho2_Enabled), 5, 0, ".", "")));
               GridsematriuirContainer.AddColumnProperties(GridsematriuirColumn);
               GridsematriuirColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridsematriuirColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31SemAtribuir), 4, 0, ".", "")));
               GridsematriuirColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSematribuir_Enabled), 5, 0, ".", "")));
               GridsematriuirContainer.AddColumnProperties(GridsematriuirColumn);
               GridsematriuirContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Allowselection), 1, 0, ".", "")));
               GridsematriuirContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Selectioncolor), 9, 0, ".", "")));
               GridsematriuirContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Allowhovering), 1, 0, ".", "")));
               GridsematriuirContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Hoveringcolor), 9, 0, ".", "")));
               GridsematriuirContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Allowcollapsing), 1, 0, ".", "")));
               GridsematriuirContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridsematriuir_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 45 )
         {
            wbEnd = 0;
            nRC_GXsfl_45 = (short)(nGXsfl_45_idx-1);
            if ( GridsematriuirContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               if ( subGridsematriuir_Visible != 0 )
               {
                  sStyleString = "";
               }
               else
               {
                  sStyleString = " style=\"display:none;\"";
               }
               context.WriteHtmlText( "<div id=\""+"GridsematriuirContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridsematriuir", GridsematriuirContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridsematriuirContainerData", GridsematriuirContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridsematriuirContainerData"+"V", GridsematriuirContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridsematriuirContainerData"+"V"+"\" value='"+GridsematriuirContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_32_JN2e( true) ;
         }
         else
         {
            wb_table3_32_JN2e( false) ;
         }
      }

      protected void wb_table2_8_JN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "OS atribuidas a voc�:", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ResumoDmn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktodos_Internalname, lblTextblocktodos_Caption, "", "", lblTextblocktodos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTextblocktodos_Visible, 1, 0, "HLP_WP_ResumoDmn.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_35_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavTodos_Internalname, StringUtil.BoolToStr( AV24Todos), "", "", chkavTodos.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(20, this, 'true', 'false');gx.ajax.executeCliEvent('e11jn2_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table5_22_JN2( true) ;
         }
         else
         {
            wb_table5_22_JN2( false) ;
         }
         return  ;
      }

      protected void wb_table5_22_JN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_26_JN2( true) ;
         }
         else
         {
            wb_table6_26_JN2( false) ;
         }
         return  ;
      }

      protected void wb_table6_26_JN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_JN2e( true) ;
         }
         else
         {
            wb_table2_8_JN2e( false) ;
         }
      }

      protected void wb_table6_26_JN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_26_JN2e( true) ;
         }
         else
         {
            wb_table6_26_JN2e( false) ;
         }
      }

      protected void wb_table5_22_JN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_22_JN2e( true) ;
         }
         else
         {
            wb_table5_22_JN2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJN2( ) ;
         WSJN2( ) ;
         WEJN2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621624458");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_resumodmn.js", "?2020621624458");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_352( )
      {
         edtavAreadetrabalho_Internalname = "vAREADETRABALHO_"+sGXsfl_35_idx;
         edtavSolicitadas_Internalname = "vSOLICITADAS_"+sGXsfl_35_idx;
         edtavRejeitadas_Internalname = "vREJEITADAS_"+sGXsfl_35_idx;
         edtavEmanalise_Internalname = "vEMANALISE_"+sGXsfl_35_idx;
         edtavEmexecucao_Internalname = "vEMEXECUCAO_"+sGXsfl_35_idx;
         edtavEmstandby_Internalname = "vEMSTANDBY_"+sGXsfl_35_idx;
         edtavDivergencias_Internalname = "vDIVERGENCIAS_"+sGXsfl_35_idx;
         edtavTotal_Internalname = "vTOTAL_"+sGXsfl_35_idx;
      }

      protected void SubsflControlProps_fel_352( )
      {
         edtavAreadetrabalho_Internalname = "vAREADETRABALHO_"+sGXsfl_35_fel_idx;
         edtavSolicitadas_Internalname = "vSOLICITADAS_"+sGXsfl_35_fel_idx;
         edtavRejeitadas_Internalname = "vREJEITADAS_"+sGXsfl_35_fel_idx;
         edtavEmanalise_Internalname = "vEMANALISE_"+sGXsfl_35_fel_idx;
         edtavEmexecucao_Internalname = "vEMEXECUCAO_"+sGXsfl_35_fel_idx;
         edtavEmstandby_Internalname = "vEMSTANDBY_"+sGXsfl_35_fel_idx;
         edtavDivergencias_Internalname = "vDIVERGENCIAS_"+sGXsfl_35_fel_idx;
         edtavTotal_Internalname = "vTOTAL_"+sGXsfl_35_fel_idx;
      }

      protected void sendrow_352( )
      {
         SubsflControlProps_352( ) ;
         WBJN0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_35_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_35_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_35_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAreadetrabalho_Internalname,StringUtil.RTrim( AV11AreaDeTrabalho),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAreadetrabalho_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavAreadetrabalho_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+((edtavSolicitadas_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSolicitadas_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5Solicitadas), 4, 0, ",", "")),((edtavSolicitadas_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV5Solicitadas), "ZZZ9")) : context.localUtil.Format( (decimal)(AV5Solicitadas), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavSolicitadas_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtavSolicitadas_Visible,(int)edtavSolicitadas_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+((edtavRejeitadas_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavRejeitadas_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6Rejeitadas), 4, 0, ",", "")),((edtavRejeitadas_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6Rejeitadas), "ZZZ9")) : context.localUtil.Format( (decimal)(AV6Rejeitadas), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavRejeitadas_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtavRejeitadas_Visible,(int)edtavRejeitadas_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+((edtavEmanalise_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEmanalise_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7EmAnalise), 4, 0, ",", "")),((edtavEmanalise_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV7EmAnalise), "ZZZ9")) : context.localUtil.Format( (decimal)(AV7EmAnalise), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEmanalise_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtavEmanalise_Visible,(int)edtavEmanalise_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+((edtavEmexecucao_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEmexecucao_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8EmExecucao), 4, 0, ",", "")),((edtavEmexecucao_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8EmExecucao), "ZZZ9")) : context.localUtil.Format( (decimal)(AV8EmExecucao), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEmexecucao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtavEmexecucao_Visible,(int)edtavEmexecucao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+((edtavEmstandby_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEmstandby_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9EmStandBy), 4, 0, ",", "")),((edtavEmstandby_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9EmStandBy), "ZZZ9")) : context.localUtil.Format( (decimal)(AV9EmStandBy), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEmstandby_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtavEmstandby_Visible,(int)edtavEmstandby_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+((edtavDivergencias_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDivergencias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10Divergencias), 4, 0, ",", "")),((edtavDivergencias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV10Divergencias), "ZZZ9")) : context.localUtil.Format( (decimal)(AV10Divergencias), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDivergencias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtavDivergencias_Visible,(int)edtavDivergencias_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BlobContentUserNickName";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTotal_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Total), 4, 0, ",", "")),((edtavTotal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV22Total), "ZZZ9")) : context.localUtil.Format( (decimal)(AV22Total), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)edtavTotal_Tooltiptext,(String)"",(String)edtavTotal_Jsonclick,(short)0,(String)"BlobContentUserNickName",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavTotal_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GridContainer.AddRow(GridRow);
            nGXsfl_35_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_35_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_35_idx+1));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
         }
         /* End function sendrow_352 */
      }

      protected void SubsflControlProps_457( )
      {
         edtavAreadetrabalho2_Internalname = "vAREADETRABALHO2_"+sGXsfl_45_idx;
         edtavSematribuir_Internalname = "vSEMATRIBUIR_"+sGXsfl_45_idx;
      }

      protected void SubsflControlProps_fel_457( )
      {
         edtavAreadetrabalho2_Internalname = "vAREADETRABALHO2_"+sGXsfl_45_fel_idx;
         edtavSematribuir_Internalname = "vSEMATRIBUIR_"+sGXsfl_45_fel_idx;
      }

      protected void sendrow_457( )
      {
         SubsflControlProps_457( ) ;
         WBJN0( ) ;
         if ( ( subGridsematriuir_Rows * 1 == 0 ) || ( nGXsfl_45_idx <= subGridsematriuir_Recordsperpage( ) * 1 ) )
         {
            GridsematriuirRow = GXWebRow.GetNew(context,GridsematriuirContainer);
            if ( subGridsematriuir_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridsematriuir_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridsematriuir_Class, "") != 0 )
               {
                  subGridsematriuir_Linesclass = subGridsematriuir_Class+"Odd";
               }
            }
            else if ( subGridsematriuir_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridsematriuir_Backstyle = 0;
               subGridsematriuir_Backcolor = subGridsematriuir_Allbackcolor;
               if ( StringUtil.StrCmp(subGridsematriuir_Class, "") != 0 )
               {
                  subGridsematriuir_Linesclass = subGridsematriuir_Class+"Uniform";
               }
            }
            else if ( subGridsematriuir_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridsematriuir_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridsematriuir_Class, "") != 0 )
               {
                  subGridsematriuir_Linesclass = subGridsematriuir_Class+"Odd";
               }
               subGridsematriuir_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridsematriuir_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridsematriuir_Backstyle = 1;
               if ( ((int)((nGXsfl_45_idx) % (2))) == 0 )
               {
                  subGridsematriuir_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridsematriuir_Class, "") != 0 )
                  {
                     subGridsematriuir_Linesclass = subGridsematriuir_Class+"Even";
                  }
               }
               else
               {
                  subGridsematriuir_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridsematriuir_Class, "") != 0 )
                  {
                     subGridsematriuir_Linesclass = subGridsematriuir_Class+"Odd";
                  }
               }
            }
            if ( GridsematriuirContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridsematriuir_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_45_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridsematriuirContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridsematriuirRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAreadetrabalho2_Internalname,StringUtil.RTrim( AV30AreaDeTrabalho2),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAreadetrabalho2_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavAreadetrabalho2_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)45,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridsematriuirContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridsematriuirRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSematribuir_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31SemAtribuir), 4, 0, ",", "")),((edtavSematribuir_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31SemAtribuir), "ZZZ9")) : context.localUtil.Format( (decimal)(AV31SemAtribuir), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavSematribuir_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavSematribuir_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)45,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
            GridsematriuirContainer.AddRow(GridsematriuirRow);
            nGXsfl_45_idx = (short)(((subGridsematriuir_Islastpage==1)&&(nGXsfl_45_idx+1>subGridsematriuir_Recordsperpage( )) ? 1 : nGXsfl_45_idx+1));
            sGXsfl_45_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_45_idx), 4, 0)), 4, "0");
            SubsflControlProps_457( ) ;
         }
         /* End function sendrow_457 */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         lblTextblocktodos_Internalname = "TEXTBLOCKTODOS";
         chkavTodos_Internalname = "vTODOS";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavAreadetrabalho_Internalname = "vAREADETRABALHO";
         edtavSolicitadas_Internalname = "vSOLICITADAS";
         edtavRejeitadas_Internalname = "vREJEITADAS";
         edtavEmanalise_Internalname = "vEMANALISE";
         edtavEmexecucao_Internalname = "vEMEXECUCAO";
         edtavEmstandby_Internalname = "vEMSTANDBY";
         edtavDivergencias_Internalname = "vDIVERGENCIAS";
         edtavTotal_Internalname = "vTOTAL";
         edtavAreadetrabalho2_Internalname = "vAREADETRABALHO2";
         edtavSematribuir_Internalname = "vSEMATRIBUIR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblUsertable_Internalname = "USERTABLE";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
         subGridsematriuir_Internalname = "GRIDSEMATRIUIR";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavSematribuir_Jsonclick = "";
         edtavAreadetrabalho2_Jsonclick = "";
         edtavTotal_Jsonclick = "";
         edtavDivergencias_Jsonclick = "";
         edtavEmstandby_Jsonclick = "";
         edtavEmexecucao_Jsonclick = "";
         edtavEmanalise_Jsonclick = "";
         edtavRejeitadas_Jsonclick = "";
         edtavSolicitadas_Jsonclick = "";
         edtavAreadetrabalho_Jsonclick = "";
         lblTextblocktodos_Visible = 1;
         subGridsematriuir_Allowcollapsing = 0;
         subGridsematriuir_Allowselection = 0;
         edtavSematribuir_Enabled = 0;
         edtavAreadetrabalho2_Enabled = 0;
         subGridsematriuir_Class = "WorkWithBorder WorkWith";
         subGridsematriuir_Visible = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowhovering = 0;
         subGrid_Selectioncolor = (int)(0xC4F0C4);
         subGrid_Allowselection = 1;
         edtavTotal_Enabled = 0;
         edtavDivergencias_Enabled = 0;
         edtavEmstandby_Enabled = 0;
         edtavEmexecucao_Enabled = 0;
         edtavEmanalise_Enabled = 0;
         edtavRejeitadas_Enabled = 0;
         edtavSolicitadas_Enabled = 0;
         edtavAreadetrabalho_Enabled = 0;
         edtavDivergencias_Visible = -1;
         edtavEmstandby_Visible = -1;
         edtavEmexecucao_Visible = -1;
         edtavEmanalise_Visible = -1;
         edtavRejeitadas_Visible = -1;
         edtavSolicitadas_Visible = -1;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavTotal_Tooltiptext = "";
         lblTextblocktodos_Caption = "Todos os usu�rios";
         chkavTodos.Visible = 1;
         subGridsematriuir_Backcolorstyle = 3;
         subGrid_Backcolorstyle = 3;
         chkavTodos.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Resumo geral";
         subGridsematriuir_Rows = 0;
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV24Todos',fld:'vTODOS',pic:'',nv:false},{av:'AV5Solicitadas',fld:'vSOLICITADAS',pic:'ZZZ9',nv:0},{av:'AV6Rejeitadas',fld:'vREJEITADAS',pic:'ZZZ9',nv:0},{av:'AV7EmAnalise',fld:'vEMANALISE',pic:'ZZZ9',nv:0},{av:'AV8EmExecucao',fld:'vEMEXECUCAO',pic:'ZZZ9',nv:0},{av:'AV9EmStandBy',fld:'vEMSTANDBY',pic:'ZZZ9',nv:0},{av:'AV10Divergencias',fld:'vDIVERGENCIAS',pic:'ZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV28Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'A1229ContagemResultado_ContratadaDoResponsavel',fld:'CONTAGEMRESULTADO_CONTRATADADORESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A1236ContagemResultado_ContratanteDoResponsavel',fld:'CONTAGEMRESULTADO_CONTRATANTEDORESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'GRIDSEMATRIUIR_nFirstRecordOnPage',nv:0},{av:'GRIDSEMATRIUIR_nEOF',nv:0},{av:'subGridsematriuir_Rows',nv:0},{av:'AV14Context',fld:'vCONTEXT',pic:'',nv:null},{av:'AV29Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV23TotalGeral',fld:'vTOTALGERAL',pic:'ZZZ9',nv:0},{av:'AV31SemAtribuir',fld:'vSEMATRIBUIR',pic:'ZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A53Contratada_AreaTrabalhoDes',fld:'CONTRATADA_AREATRABALHODES',pic:'@!',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV20AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV14Context',fld:'vCONTEXT',pic:'',nv:null},{av:'chkavTodos.Visible',ctrl:'vTODOS',prop:'Visible'},{av:'lblTextblocktodos_Visible',ctrl:'TEXTBLOCKTODOS',prop:'Visible'},{av:'lblTextblocktodos_Caption',ctrl:'TEXTBLOCKTODOS',prop:'Caption'},{av:'AV24Todos',fld:'vTODOS',pic:'',nv:false},{av:'subGridsematriuir_Visible',ctrl:'GRIDSEMATRIUIR',prop:'Visible'},{av:'AV23TotalGeral',fld:'vTOTALGERAL',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E13JN2',iparms:[{av:'AV14Context',fld:'vCONTEXT',pic:'',nv:null},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV24Todos',fld:'vTODOS',pic:'',nv:false},{av:'AV5Solicitadas',fld:'vSOLICITADAS',pic:'ZZZ9',nv:0},{av:'AV6Rejeitadas',fld:'vREJEITADAS',pic:'ZZZ9',nv:0},{av:'AV7EmAnalise',fld:'vEMANALISE',pic:'ZZZ9',nv:0},{av:'AV8EmExecucao',fld:'vEMEXECUCAO',pic:'ZZZ9',nv:0},{av:'AV9EmStandBy',fld:'vEMSTANDBY',pic:'ZZZ9',nv:0},{av:'AV10Divergencias',fld:'vDIVERGENCIAS',pic:'ZZZ9',nv:0},{av:'AV23TotalGeral',fld:'vTOTALGERAL',pic:'ZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV28Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'AV20AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A1229ContagemResultado_ContratadaDoResponsavel',fld:'CONTAGEMRESULTADO_CONTRATADADORESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV27Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1236ContagemResultado_ContratanteDoResponsavel',fld:'CONTAGEMRESULTADO_CONTRATANTEDORESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A53Contratada_AreaTrabalhoDes',fld:'CONTRATADA_AREATRABALHODES',pic:'@!',nv:''}],oparms:[{av:'AV28Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtavSolicitadas_Visible',ctrl:'vSOLICITADAS',prop:'Visible'},{av:'edtavRejeitadas_Visible',ctrl:'vREJEITADAS',prop:'Visible'},{av:'edtavEmanalise_Visible',ctrl:'vEMANALISE',prop:'Visible'},{av:'edtavEmexecucao_Visible',ctrl:'vEMEXECUCAO',prop:'Visible'},{av:'edtavEmstandby_Visible',ctrl:'vEMSTANDBY',prop:'Visible'},{av:'edtavDivergencias_Visible',ctrl:'vDIVERGENCIAS',prop:'Visible'},{av:'AV29Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV27Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22Total',fld:'vTOTAL',pic:'ZZZ9',nv:0},{av:'AV23TotalGeral',fld:'vTOTALGERAL',pic:'ZZZ9',nv:0},{av:'AV11AreaDeTrabalho',fld:'vAREADETRABALHO',pic:'',nv:''},{av:'AV20AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5Solicitadas',fld:'vSOLICITADAS',pic:'ZZZ9',nv:0},{av:'AV6Rejeitadas',fld:'vREJEITADAS',pic:'ZZZ9',nv:0},{av:'AV7EmAnalise',fld:'vEMANALISE',pic:'ZZZ9',nv:0},{av:'AV8EmExecucao',fld:'vEMEXECUCAO',pic:'ZZZ9',nv:0},{av:'AV9EmStandBy',fld:'vEMSTANDBY',pic:'ZZZ9',nv:0},{av:'AV10Divergencias',fld:'vDIVERGENCIAS',pic:'ZZZ9',nv:0},{av:'edtavTotal_Tooltiptext',ctrl:'vTOTAL',prop:'Tooltiptext'},{av:'AV30AreaDeTrabalho2',fld:'vAREADETRABALHO2',pic:'',nv:''}]}");
         setEventMetadata("GRIDSEMATRIUIR.LOAD","{handler:'E15JN7',iparms:[{av:'AV14Context',fld:'vCONTEXT',pic:'',nv:null},{av:'AV29Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV23TotalGeral',fld:'vTOTALGERAL',pic:'ZZZ9',nv:0},{av:'AV31SemAtribuir',fld:'vSEMATRIBUIR',pic:'ZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV27Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A53Contratada_AreaTrabalhoDes',fld:'CONTRATADA_AREATRABALHODES',pic:'@!',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV20AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV27Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23TotalGeral',fld:'vTOTALGERAL',pic:'ZZZ9',nv:0},{av:'AV30AreaDeTrabalho2',fld:'vAREADETRABALHO2',pic:'',nv:''},{av:'AV31SemAtribuir',fld:'vSEMATRIBUIR',pic:'ZZZ9',nv:0},{av:'AV20AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11AreaDeTrabalho',fld:'vAREADETRABALHO',pic:'',nv:''}]}");
         setEventMetadata("VTODOS.CLICK","{handler:'E11JN2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14Context',fld:'vCONTEXT',pic:'',nv:null},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV24Todos',fld:'vTODOS',pic:'',nv:false},{av:'AV5Solicitadas',fld:'vSOLICITADAS',pic:'ZZZ9',nv:0},{av:'AV6Rejeitadas',fld:'vREJEITADAS',pic:'ZZZ9',nv:0},{av:'AV7EmAnalise',fld:'vEMANALISE',pic:'ZZZ9',nv:0},{av:'AV8EmExecucao',fld:'vEMEXECUCAO',pic:'ZZZ9',nv:0},{av:'AV9EmStandBy',fld:'vEMSTANDBY',pic:'ZZZ9',nv:0},{av:'AV10Divergencias',fld:'vDIVERGENCIAS',pic:'ZZZ9',nv:0},{av:'AV23TotalGeral',fld:'vTOTALGERAL',pic:'ZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV28Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A531ContagemResultado_StatusUltCnt',fld:'CONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'AV20AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A1229ContagemResultado_ContratadaDoResponsavel',fld:'CONTAGEMRESULTADO_CONTRATADADORESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV27Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1236ContagemResultado_ContratanteDoResponsavel',fld:'CONTAGEMRESULTADO_CONTRATANTEDORESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A53Contratada_AreaTrabalhoDes',fld:'CONTRATADA_AREATRABALHODES',pic:'@!',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV14Context = new wwpbaseobjects.SdtWWPContext(context);
         A6AreaTrabalho_Descricao = "";
         A484ContagemResultado_StatusDmn = "";
         A53Contratada_AreaTrabalhoDes = "";
         GXKey = "";
         AV29Contratadas = new GxSimpleCollection();
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV11AreaDeTrabalho = "";
         AV30AreaDeTrabalho2 = "";
         GridContainer = new GXWebGrid( context);
         GridsematriuirContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00JN2_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00JN2_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         GridRow = new GXWebRow();
         H00JN3_A74Contrato_Codigo = new int[1] ;
         H00JN3_A1013Contrato_PrepostoCod = new int[1] ;
         H00JN3_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00JN3_A39Contratada_Codigo = new int[1] ;
         H00JN4_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00JN4_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00JN4_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H00JN4_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00JN5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00JN5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         AV25WebSession = context.GetSession();
         H00JN6_A39Contratada_Codigo = new int[1] ;
         H00JN6_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00JN6_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00JN6_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         H00JN6_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         H00JN7_A29Contratante_Codigo = new int[1] ;
         H00JN7_n29Contratante_Codigo = new bool[] {false} ;
         H00JN7_A5AreaTrabalho_Codigo = new int[1] ;
         H00JN7_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00JN9_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00JN9_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00JN9_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00JN9_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00JN9_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00JN9_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         H00JN9_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00JN9_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00JN9_A890ContagemResultado_Responsavel = new int[1] ;
         H00JN9_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00JN9_A456ContagemResultado_Codigo = new int[1] ;
         AV17StatusDmn = "";
         H00JN10_AV31SemAtribuir = new short[1] ;
         GridsematriuirRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         subGridsematriuir_Linesclass = "";
         GridsematriuirColumn = new GXWebColumn();
         lblTitle_Jsonclick = "";
         lblTextblocktodos_Jsonclick = "";
         TempTags = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_resumodmn__default(),
            new Object[][] {
                new Object[] {
               H00JN2_A60ContratanteUsuario_UsuarioCod, H00JN2_A63ContratanteUsuario_ContratanteCod
               }
               , new Object[] {
               H00JN3_A74Contrato_Codigo, H00JN3_A1013Contrato_PrepostoCod, H00JN3_n1013Contrato_PrepostoCod, H00JN3_A39Contratada_Codigo
               }
               , new Object[] {
               H00JN4_A1078ContratoGestor_ContratoCod, H00JN4_A1136ContratoGestor_ContratadaCod, H00JN4_n1136ContratoGestor_ContratadaCod, H00JN4_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               H00JN5_A69ContratadaUsuario_UsuarioCod, H00JN5_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00JN6_A39Contratada_Codigo, H00JN6_A52Contratada_AreaTrabalhoCod, H00JN6_A53Contratada_AreaTrabalhoDes, H00JN6_n53Contratada_AreaTrabalhoDes
               }
               , new Object[] {
               H00JN7_A29Contratante_Codigo, H00JN7_n29Contratante_Codigo, H00JN7_A5AreaTrabalho_Codigo, H00JN7_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00JN9_A490ContagemResultado_ContratadaCod, H00JN9_n490ContagemResultado_ContratadaCod, H00JN9_A484ContagemResultado_StatusDmn, H00JN9_n484ContagemResultado_StatusDmn, H00JN9_A531ContagemResultado_StatusUltCnt, H00JN9_n531ContagemResultado_StatusUltCnt, H00JN9_A52Contratada_AreaTrabalhoCod, H00JN9_n52Contratada_AreaTrabalhoCod, H00JN9_A890ContagemResultado_Responsavel, H00JN9_n890ContagemResultado_Responsavel,
               H00JN9_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00JN10_AV31SemAtribuir
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavAreadetrabalho_Enabled = 0;
         edtavSolicitadas_Enabled = 0;
         edtavRejeitadas_Enabled = 0;
         edtavEmanalise_Enabled = 0;
         edtavEmexecucao_Enabled = 0;
         edtavEmstandby_Enabled = 0;
         edtavDivergencias_Enabled = 0;
         edtavTotal_Enabled = 0;
         edtavAreadetrabalho2_Enabled = 0;
         edtavSematribuir_Enabled = 0;
      }

      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_35 ;
      private short nGXsfl_35_idx=1 ;
      private short AV5Solicitadas ;
      private short AV6Rejeitadas ;
      private short AV7EmAnalise ;
      private short AV8EmExecucao ;
      private short AV9EmStandBy ;
      private short AV10Divergencias ;
      private short AV23TotalGeral ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short nRC_GXsfl_45 ;
      private short nGXsfl_45_idx=1 ;
      private short AV31SemAtribuir ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short GRIDSEMATRIUIR_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short AV22Total ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_35_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short nGXsfl_45_Refreshing=0 ;
      private short subGridsematriuir_Backcolorstyle ;
      private short AV14Context_gxTpr_Userid ;
      private short AV18StatusCnt ;
      private short AV16Qtd ;
      private short cV31SemAtribuir ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGridsematriuir_Titlebackstyle ;
      private short subGridsematriuir_Allowselection ;
      private short subGridsematriuir_Allowhovering ;
      private short subGridsematriuir_Allowcollapsing ;
      private short subGridsematriuir_Collapsed ;
      private short subGrid_Backstyle ;
      private short subGridsematriuir_Backstyle ;
      private int subGrid_Rows ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A1013Contrato_PrepostoCod ;
      private int A39Contratada_Codigo ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A29Contratante_Codigo ;
      private int AV28Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int AV20AreaTrabalho_Codigo ;
      private int A890ContagemResultado_Responsavel ;
      private int A1229ContagemResultado_ContratadaDoResponsavel ;
      private int AV27Contratada_Codigo ;
      private int A1236ContagemResultado_ContratanteDoResponsavel ;
      private int subGridsematriuir_Rows ;
      private int A490ContagemResultado_ContratadaCod ;
      private int subGrid_Islastpage ;
      private int subGridsematriuir_Islastpage ;
      private int edtavAreadetrabalho_Enabled ;
      private int edtavSolicitadas_Enabled ;
      private int edtavRejeitadas_Enabled ;
      private int edtavEmanalise_Enabled ;
      private int edtavEmexecucao_Enabled ;
      private int edtavEmstandby_Enabled ;
      private int edtavDivergencias_Enabled ;
      private int edtavTotal_Enabled ;
      private int edtavAreadetrabalho2_Enabled ;
      private int edtavSematribuir_Enabled ;
      private int GRID_nGridOutOfScope ;
      private int subGridsematriuir_Visible ;
      private int GRIDSEMATRIUIR_nGridOutOfScope ;
      private int AV14Context_gxTpr_Contratante_codigo ;
      private int edtavSolicitadas_Visible ;
      private int edtavRejeitadas_Visible ;
      private int edtavEmanalise_Visible ;
      private int edtavEmexecucao_Visible ;
      private int edtavEmstandby_Visible ;
      private int edtavDivergencias_Visible ;
      private int A1078ContratoGestor_ContratoCod ;
      private int AV37GXV1 ;
      private int lblTextblocktodos_Visible ;
      private int A456ContagemResultado_Codigo ;
      private int W52Contratada_AreaTrabalhoCod ;
      private int GXt_int1 ;
      private int AV39GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int subGridsematriuir_Titlebackcolor ;
      private int subGridsematriuir_Allbackcolor ;
      private int subGridsematriuir_Selectioncolor ;
      private int subGridsematriuir_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int subGridsematriuir_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRIDSEMATRIUIR_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRIDSEMATRIUIR_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long GRIDSEMATRIUIR_nRecordCount ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_35_idx="0001" ;
      private String edtavSolicitadas_Internalname ;
      private String edtavRejeitadas_Internalname ;
      private String edtavEmanalise_Internalname ;
      private String edtavEmexecucao_Internalname ;
      private String edtavEmstandby_Internalname ;
      private String edtavDivergencias_Internalname ;
      private String A484ContagemResultado_StatusDmn ;
      private String GXKey ;
      private String sGXsfl_45_idx="0001" ;
      private String edtavSematribuir_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV11AreaDeTrabalho ;
      private String edtavAreadetrabalho_Internalname ;
      private String edtavTotal_Internalname ;
      private String AV30AreaDeTrabalho2 ;
      private String edtavAreadetrabalho2_Internalname ;
      private String chkavTodos_Internalname ;
      private String subGrid_Internalname ;
      private String scmdbuf ;
      private String lblTextblocktodos_Internalname ;
      private String lblTextblocktodos_Caption ;
      private String edtavTotal_Tooltiptext ;
      private String AV17StatusDmn ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblUsertable_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String subGridsematriuir_Internalname ;
      private String subGridsematriuir_Class ;
      private String subGridsematriuir_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String lblTextblocktodos_Jsonclick ;
      private String TempTags ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_35_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavAreadetrabalho_Jsonclick ;
      private String edtavSolicitadas_Jsonclick ;
      private String edtavRejeitadas_Jsonclick ;
      private String edtavEmanalise_Jsonclick ;
      private String edtavEmexecucao_Jsonclick ;
      private String edtavEmstandby_Jsonclick ;
      private String edtavDivergencias_Jsonclick ;
      private String edtavTotal_Jsonclick ;
      private String sGXsfl_45_fel_idx="0001" ;
      private String edtavAreadetrabalho2_Jsonclick ;
      private String edtavSematribuir_Jsonclick ;
      private bool entryPointCalled ;
      private bool AV24Todos ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n29Contratante_Codigo ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n531ContagemResultado_StatusUltCnt ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV14Context_gxTpr_Userehcontratada ;
      private bool AV14Context_gxTpr_Userehcontratante ;
      private bool BRKJN10 ;
      private String A6AreaTrabalho_Descricao ;
      private String A53Contratada_AreaTrabalhoDes ;
      private IGxSession AV25WebSession ;
      private GXWebGrid GridContainer ;
      private GXWebGrid GridsematriuirContainer ;
      private GXWebRow GridRow ;
      private GXWebRow GridsematriuirRow ;
      private GXWebColumn GridColumn ;
      private GXWebColumn GridsematriuirColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavTodos ;
      private IDataStoreProvider pr_default ;
      private int[] H00JN2_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00JN2_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00JN3_A74Contrato_Codigo ;
      private int[] H00JN3_A1013Contrato_PrepostoCod ;
      private bool[] H00JN3_n1013Contrato_PrepostoCod ;
      private int[] H00JN3_A39Contratada_Codigo ;
      private int[] H00JN4_A1078ContratoGestor_ContratoCod ;
      private int[] H00JN4_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00JN4_n1136ContratoGestor_ContratadaCod ;
      private int[] H00JN4_A1079ContratoGestor_UsuarioCod ;
      private int[] H00JN5_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00JN5_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00JN6_A39Contratada_Codigo ;
      private int[] H00JN6_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00JN6_n52Contratada_AreaTrabalhoCod ;
      private String[] H00JN6_A53Contratada_AreaTrabalhoDes ;
      private bool[] H00JN6_n53Contratada_AreaTrabalhoDes ;
      private int[] H00JN7_A29Contratante_Codigo ;
      private bool[] H00JN7_n29Contratante_Codigo ;
      private int[] H00JN7_A5AreaTrabalho_Codigo ;
      private String[] H00JN7_A6AreaTrabalho_Descricao ;
      private int[] H00JN9_A490ContagemResultado_ContratadaCod ;
      private bool[] H00JN9_n490ContagemResultado_ContratadaCod ;
      private String[] H00JN9_A484ContagemResultado_StatusDmn ;
      private bool[] H00JN9_n484ContagemResultado_StatusDmn ;
      private short[] H00JN9_A531ContagemResultado_StatusUltCnt ;
      private bool[] H00JN9_n531ContagemResultado_StatusUltCnt ;
      private int[] H00JN9_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00JN9_n52Contratada_AreaTrabalhoCod ;
      private int[] H00JN9_A890ContagemResultado_Responsavel ;
      private bool[] H00JN9_n890ContagemResultado_Responsavel ;
      private int[] H00JN9_A456ContagemResultado_Codigo ;
      private short[] H00JN10_AV31SemAtribuir ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV29Contratadas ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV14Context ;
   }

   public class wp_resumodmn__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00JN2( IGxContext context ,
                                             bool AV24Todos ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             int AV14Context_gxTpr_Contratante_codigo ,
                                             int A60ContratanteUsuario_UsuarioCod ,
                                             short AV14Context_gxTpr_Userid )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [2] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT [ContratanteUsuario_UsuarioCod], [ContratanteUsuario_ContratanteCod] FROM [ContratanteUsuario] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ContratanteUsuario_UsuarioCod] = @AV14Context__Userid)";
         if ( AV24Todos )
         {
            sWhereString = sWhereString + " and ([ContratanteUsuario_ContratanteCod] = @AV14Cont_1Contratante_codigo)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV24Todos )
         {
            scmdbuf = scmdbuf + " ORDER BY [ContratanteUsuario_ContratanteCod]";
         }
         else if ( ! AV24Todos )
         {
            scmdbuf = scmdbuf + " ORDER BY [ContratanteUsuario_UsuarioCod]";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + " ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod]";
         }
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00JN4( IGxContext context ,
                                             int A1136ContratoGestor_ContratadaCod ,
                                             IGxCollection AV29Contratadas ,
                                             short AV14Context_gxTpr_Userid ,
                                             int A1079ContratoGestor_UsuarioCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [1] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T1.[ContratoGestor_UsuarioCod] FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoGestor_UsuarioCod] = @AV14Context__Userid)";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV29Contratadas, "T2.[Contratada_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoGestor_UsuarioCod]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H00JN9( IGxContext context ,
                                             bool AV24Todos ,
                                             int A890ContagemResultado_Responsavel ,
                                             short AV14Context_gxTpr_Userid ,
                                             String A484ContagemResultado_StatusDmn ,
                                             bool AV14Context_gxTpr_Userehcontratada ,
                                             int A1229ContagemResultado_ContratadaDoResponsavel ,
                                             int AV27Contratada_Codigo ,
                                             int A1236ContagemResultado_ContratanteDoResponsavel ,
                                             bool AV14Context_gxTpr_Userehcontratante ,
                                             int AV28Contratante_Codigo ,
                                             int AV20AreaTrabalho_Codigo ,
                                             int A52Contratada_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [2] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T1.[ContagemResultado_StatusDmn], COALESCE( T3.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, T2.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[Contratada_AreaTrabalhoCod] = @AV20AreaTrabalho_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] = 'S' or T1.[ContagemResultado_StatusDmn] = 'D' or T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'B')";
         scmdbuf = scmdbuf + " and (Not T1.[ContagemResultado_Responsavel] IS NULL)";
         if ( ! AV24Todos )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV14Context__Userid)";
         }
         else
         {
            GXv_int6[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contratada_AreaTrabalhoCod], [ContagemResultado_StatusDmn], [ContagemResultado_StatusUltCnt]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00JN2(context, (bool)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (short)dynConstraints[4] );
               case 2 :
                     return conditional_H00JN4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (int)dynConstraints[3] );
               case 6 :
                     return conditional_H00JN9(context, (bool)dynConstraints[0] , (int)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (bool)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00JN3 ;
          prmH00JN3 = new Object[] {
          new Object[] {"@AV14Context__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00JN5 ;
          prmH00JN5 = new Object[] {
          new Object[] {"@AV14Context__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00JN6 ;
          prmH00JN6 = new Object[] {
          new Object[] {"@AV27Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JN7 ;
          prmH00JN7 = new Object[] {
          new Object[] {"@AV28Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JN10 ;
          prmH00JN10 = new Object[] {
          new Object[] {"@AV20AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JN2 ;
          prmH00JN2 = new Object[] {
          new Object[] {"@AV14Context__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV14Cont_1Contratante_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JN4 ;
          prmH00JN4 = new Object[] {
          new Object[] {"@AV14Context__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00JN9 ;
          prmH00JN9 = new Object[] {
          new Object[] {"@AV20AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14Context__Userid",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00JN2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JN2,100,0,true,false )
             ,new CursorDef("H00JN3", "SELECT [Contrato_Codigo], [Contrato_PrepostoCod], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_PrepostoCod] = @AV14Context__Userid ORDER BY [Contrato_PrepostoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JN3,100,0,false,false )
             ,new CursorDef("H00JN4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JN4,100,0,false,false )
             ,new CursorDef("H00JN5", "SELECT [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_UsuarioCod] = @AV14Context__Userid ORDER BY [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JN5,100,0,true,false )
             ,new CursorDef("H00JN6", "SELECT TOP 1 T1.[Contratada_Codigo], T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T2.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) WHERE T1.[Contratada_Codigo] = @AV27Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JN6,1,0,false,true )
             ,new CursorDef("H00JN7", "SELECT TOP 1 [Contratante_Codigo], [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @AV28Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JN7,1,0,false,true )
             ,new CursorDef("H00JN9", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JN9,100,0,true,false )
             ,new CursorDef("H00JN10", "SELECT COUNT(*) FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) WHERE (T2.[Contratada_AreaTrabalhoCod] = @AV20AreaTrabalho_Codigo) AND (T1.[ContagemResultado_StatusDmn] = 'S' or T1.[ContagemResultado_StatusDmn] = 'D' or T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'B') AND (T1.[ContagemResultado_Responsavel] IS NULL) AND (T1.[ContagemResultado_ContratadaCod] = @AV27Contratada_Codigo) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JN10,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[3]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
