/*
               File: Contrato
        Description: Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:10:39.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contrato : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action40") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n74Contrato_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            A115Contrato_UnidadeContratacao = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
            A82Contrato_DataVigenciaInicio = context.localUtil.ParseDateParm( GetNextPar( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
            A83Contrato_DataVigenciaTermino = context.localUtil.ParseDateParm( GetNextPar( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
            A516Contratada_TipoFabrica = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_40_0G17( A74Contrato_Codigo, A115Contrato_UnidadeContratacao, A82Contrato_DataVigenciaInicio, A83Contrato_DataVigenciaTermino, A516Contratada_TipoFabrica) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action41") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_41_0G17( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action42") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n74Contrato_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_42_0G17( A74Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTRATO_PREPOSTOCOD") == 0 )
         {
            AV17Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTRATO_PREPOSTOCOD0G17( AV17Contratada_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel13"+"_"+"CONTRATO_VALORUNDCNTATUAL") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n74Contrato_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            A116Contrato_ValorUnidadeContratacao = NumberUtil.Val( GetNextPar( ), ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX13ASACONTRATO_VALORUNDCNTATUAL0G17( A74Contrato_Codigo, A116Contrato_ValorUnidadeContratacao) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_49") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n74Contrato_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_49( A74Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_50") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n74Contrato_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_50( A74Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_45") == 0 )
         {
            A75Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_45( A75Contrato_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_44") == 0 )
         {
            A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_44( A39Contratada_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_47") == 0 )
         {
            A40Contratada_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_47( A40Contratada_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_46") == 0 )
         {
            A1013Contrato_PrepostoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1013Contrato_PrepostoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_46( A1013Contrato_PrepostoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_48") == 0 )
         {
            A1016Contrato_PrepostoPesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1016Contrato_PrepostoPesCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1016Contrato_PrepostoPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_48( A1016Contrato_PrepostoPesCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Contrato_Codigo), "ZZZZZ9")));
               AV17Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynContrato_PrepostoCod.Name = "CONTRATO_PREPOSTOCOD";
         dynContrato_PrepostoCod.WebTags = "";
         cmbContrato_UnidadeContratacao.Name = "CONTRATO_UNIDADECONTRATACAO";
         cmbContrato_UnidadeContratacao.WebTags = "";
         cmbContrato_UnidadeContratacao.addItem("1", "Ponto Fun��o", 0);
         cmbContrato_UnidadeContratacao.addItem("2", "Horas", 0);
         cmbContrato_UnidadeContratacao.addItem("3", "UST", 0);
         if ( cmbContrato_UnidadeContratacao.ItemCount > 0 )
         {
            A115Contrato_UnidadeContratacao = (short)(NumberUtil.Val( cmbContrato_UnidadeContratacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
         }
         cmbContrato_PrdFtrCada.Name = "CONTRATO_PRDFTRCADA";
         cmbContrato_PrdFtrCada.WebTags = "";
         cmbContrato_PrdFtrCada.addItem("", "(Nenhum)", 0);
         cmbContrato_PrdFtrCada.addItem("M", "Mensal", 0);
         cmbContrato_PrdFtrCada.addItem("S", "Semanal", 0);
         cmbContrato_PrdFtrCada.addItem("P", "Periodo", 0);
         cmbContrato_PrdFtrCada.addItem("D", "Diario", 0);
         if ( cmbContrato_PrdFtrCada.ItemCount > 0 )
         {
            A1354Contrato_PrdFtrCada = cmbContrato_PrdFtrCada.getValidValue(A1354Contrato_PrdFtrCada);
            n1354Contrato_PrdFtrCada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1354Contrato_PrdFtrCada", A1354Contrato_PrdFtrCada);
         }
         cmbavUnidadedemedicao.Name = "vUNIDADEDEMEDICAO";
         cmbavUnidadedemedicao.WebTags = "";
         cmbavUnidadedemedicao.addItem("0", "--", 0);
         cmbavUnidadedemedicao.addItem("1", "PF", 0);
         cmbavUnidadedemedicao.addItem("2", "Hs.", 0);
         cmbavUnidadedemedicao.addItem("3", "UST", 0);
         if ( cmbavUnidadedemedicao.ItemCount > 0 )
         {
            AV25UnidadeDeMedicao = (short)(NumberUtil.Val( cmbavUnidadedemedicao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25UnidadeDeMedicao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0)));
         }
         cmbContrato_CalculoDivergencia.Name = "CONTRATO_CALCULODIVERGENCIA";
         cmbContrato_CalculoDivergencia.WebTags = "";
         cmbContrato_CalculoDivergencia.addItem("B", "Sob PF Bruto", 0);
         cmbContrato_CalculoDivergencia.addItem("L", "Sob PF Liquido", 0);
         cmbContrato_CalculoDivergencia.addItem("A", "Maior diverg�ncia entre ambos", 0);
         if ( cmbContrato_CalculoDivergencia.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A452Contrato_CalculoDivergencia)) )
            {
               A452Contrato_CalculoDivergencia = "B";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
            }
            A452Contrato_CalculoDivergencia = cmbContrato_CalculoDivergencia.getValidValue(A452Contrato_CalculoDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
         }
         chkContrato_AceitaPFFS.Name = "CONTRATO_ACEITAPFFS";
         chkContrato_AceitaPFFS.WebTags = "";
         chkContrato_AceitaPFFS.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContrato_AceitaPFFS_Internalname, "TitleCaption", chkContrato_AceitaPFFS.Caption);
         chkContrato_AceitaPFFS.CheckedValue = "false";
         chkContrato_Ativo.Name = "CONTRATO_ATIVO";
         chkContrato_Ativo.WebTags = "";
         chkContrato_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContrato_Ativo_Internalname, "TitleCaption", chkContrato_Ativo.Caption);
         chkContrato_Ativo.CheckedValue = "false";
         cmbContratada_TipoFabrica.Name = "CONTRATADA_TIPOFABRICA";
         cmbContratada_TipoFabrica.WebTags = "";
         cmbContratada_TipoFabrica.addItem("", "(Nenhuma)", 0);
         cmbContratada_TipoFabrica.addItem("S", "F�brica de Software", 0);
         cmbContratada_TipoFabrica.addItem("M", "F�brica de M�trica", 0);
         cmbContratada_TipoFabrica.addItem("I", "Departamento / Setor", 0);
         cmbContratada_TipoFabrica.addItem("O", "Outras", 0);
         if ( cmbContratada_TipoFabrica.ItemCount > 0 )
         {
            A516Contratada_TipoFabrica = cmbContratada_TipoFabrica.getValidValue(A516Contratada_TipoFabrica);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contrato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynContrato_PrepostoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Contrato_Codigo ,
                           ref int aP2_Contratada_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Contrato_Codigo = aP1_Contrato_Codigo;
         this.AV17Contratada_Codigo = aP2_Contratada_Codigo;
         executePrivate();
         aP2_Contratada_Codigo=this.AV17Contratada_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynContrato_PrepostoCod = new GXCombobox();
         cmbContrato_UnidadeContratacao = new GXCombobox();
         cmbContrato_PrdFtrCada = new GXCombobox();
         cmbavUnidadedemedicao = new GXCombobox();
         cmbContrato_CalculoDivergencia = new GXCombobox();
         chkContrato_AceitaPFFS = new GXCheckbox();
         chkContrato_Ativo = new GXCheckbox();
         cmbContratada_TipoFabrica = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContrato_PrepostoCod.ItemCount > 0 )
         {
            A1013Contrato_PrepostoCod = (int)(NumberUtil.Val( dynContrato_PrepostoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0))), "."));
            n1013Contrato_PrepostoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
         }
         if ( cmbContrato_UnidadeContratacao.ItemCount > 0 )
         {
            A115Contrato_UnidadeContratacao = (short)(NumberUtil.Val( cmbContrato_UnidadeContratacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
         }
         if ( cmbContrato_PrdFtrCada.ItemCount > 0 )
         {
            A1354Contrato_PrdFtrCada = cmbContrato_PrdFtrCada.getValidValue(A1354Contrato_PrdFtrCada);
            n1354Contrato_PrdFtrCada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1354Contrato_PrdFtrCada", A1354Contrato_PrdFtrCada);
         }
         if ( cmbavUnidadedemedicao.ItemCount > 0 )
         {
            AV25UnidadeDeMedicao = (short)(NumberUtil.Val( cmbavUnidadedemedicao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25UnidadeDeMedicao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0)));
         }
         if ( cmbContrato_CalculoDivergencia.ItemCount > 0 )
         {
            A452Contrato_CalculoDivergencia = cmbContrato_CalculoDivergencia.getValidValue(A452Contrato_CalculoDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
         }
         if ( cmbContratada_TipoFabrica.ItemCount > 0 )
         {
            A516Contratada_TipoFabrica = cmbContratada_TipoFabrica.getValidValue(A516Contratada_TipoFabrica);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0G17( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0G17e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 217,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A75Contrato_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,217);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_AreaTrabalhoCod_Jsonclick, 0, "Attribute", "", "", "", edtContrato_AreaTrabalhoCod_Visible, edtContrato_AreaTrabalhoCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contrato.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")), ((edtContrato_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContrato_Codigo_Visible, edtContrato_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 219,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratada_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,219);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratada_Codigo_Visible, edtContratada_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contrato.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0G17( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0G17( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0G17e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0G17e( true) ;
         }
         else
         {
            wb_table1_2_0G17e( false) ;
         }
      }

      protected void wb_table2_5_0G17( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table3_13_0G17( true) ;
         }
         return  ;
      }

      protected void wb_table3_13_0G17e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_213_0G17( true) ;
         }
         return  ;
      }

      protected void wb_table4_213_0G17e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0G17e( true) ;
         }
         else
         {
            wb_table2_5_0G17e( false) ;
         }
      }

      protected void wb_table4_213_0G17( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsrtable_Internalname, tblUsrtable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_213_0G17e( true) ;
         }
         else
         {
            wb_table4_213_0G17e( false) ;
         }
      }

      protected void wb_table3_13_0G17( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaCNPJ_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_prepostocod_Internalname, lblTextblockcontrato_prepostocod_Caption, "", "", lblTextblockcontrato_prepostocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContrato_PrepostoCod, dynContrato_PrepostoCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)), 1, dynContrato_PrepostoCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContrato_PrepostoCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_Contrato.htm");
            dynContrato_PrepostoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContrato_PrepostoCod_Internalname, "Values", (String)(dynContrato_PrepostoCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N�mero do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Numero_Enabled, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numeroata_Internalname, "N�mero da Ata", "", "", lblTextblockcontrato_numeroata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_NumeroAta_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ano_Internalname, "Ano", "", "", lblTextblockcontrato_ano_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")), ((edtContrato_Ano_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")) : context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Ano_Enabled, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_objeto_Internalname, "Objeto do Contrato", "", "", lblTextblockcontrato_objeto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='RequiredDataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContrato_Objeto_Internalname, A80Contrato_Objeto, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", 0, 1, edtContrato_Objeto_Enabled, 0, 100, "%", 2, "row", StyleString, ClassString, "", "10000", 1, "", "", -1, true, "TextoObjeto", "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datavigenciainicio_Internalname, "D. Vig�ncia Inicio", "", "", lblTextblockcontrato_datavigenciainicio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContrato_DataVigenciaInicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataVigenciaInicio_Internalname, context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"), context.localUtil.Format( A82Contrato_DataVigenciaInicio, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataVigenciaInicio_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContrato_DataVigenciaInicio_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_Contrato.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataVigenciaInicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContrato_DataVigenciaInicio_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Contrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datavigenciatermino_Internalname, "D. Vig�ncia T�rmino", "", "", lblTextblockcontrato_datavigenciatermino_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContrato_DataVigenciaTermino_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataVigenciaTermino_Internalname, context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"), context.localUtil.Format( A83Contrato_DataVigenciaTermino, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataVigenciaTermino_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContrato_DataVigenciaTermino_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_Contrato.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataVigenciaTermino_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContrato_DataVigenciaTermino_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Contrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datapublicacaodou_Internalname, "D. Publica��o DOU", "", "", lblTextblockcontrato_datapublicacaodou_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContrato_DataPublicacaoDOU_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataPublicacaoDOU_Internalname, context.localUtil.Format(A84Contrato_DataPublicacaoDOU, "99/99/99"), context.localUtil.Format( A84Contrato_DataPublicacaoDOU, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataPublicacaoDOU_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContrato_DataPublicacaoDOU_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_Contrato.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataPublicacaoDOU_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContrato_DataPublicacaoDOU_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Contrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_dataassinatura_Internalname, "D. da Assinatura", "", "", lblTextblockcontrato_dataassinatura_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContrato_DataAssinatura_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataAssinatura_Internalname, context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"), context.localUtil.Format( A85Contrato_DataAssinatura, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataAssinatura_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContrato_DataAssinatura_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_Contrato.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataAssinatura_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContrato_DataAssinatura_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Contrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datapedidoreajuste_Internalname, "D. Pedido Reajuste", "", "", lblTextblockcontrato_datapedidoreajuste_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContrato_DataPedidoReajuste_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataPedidoReajuste_Internalname, context.localUtil.Format(A86Contrato_DataPedidoReajuste, "99/99/99"), context.localUtil.Format( A86Contrato_DataPedidoReajuste, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataPedidoReajuste_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContrato_DataPedidoReajuste_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_Contrato.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataPedidoReajuste_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContrato_DataPedidoReajuste_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Contrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_dataterminoata_Internalname, "D. T�rmino da Ata", "", "", lblTextblockcontrato_dataterminoata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContrato_DataTerminoAta_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataTerminoAta_Internalname, context.localUtil.Format(A87Contrato_DataTerminoAta, "99/99/99"), context.localUtil.Format( A87Contrato_DataTerminoAta, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataTerminoAta_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContrato_DataTerminoAta_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_Contrato.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataTerminoAta_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContrato_DataTerminoAta_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Contrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datafimadaptacao_Internalname, "Fim Adapta��o", "", "", lblTextblockcontrato_datafimadaptacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContrato_DataFimAdaptacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataFimAdaptacao_Internalname, context.localUtil.Format(A88Contrato_DataFimAdaptacao, "99/99/99"), context.localUtil.Format( A88Contrato_DataFimAdaptacao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataFimAdaptacao_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContrato_DataFimAdaptacao_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_Contrato.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataFimAdaptacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContrato_DataFimAdaptacao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Contrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_valor_Internalname, "Valor do Contrato", "", "", lblTextblockcontrato_valor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A89Contrato_Valor, 18, 5, ",", "")), ((edtContrato_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A89Contrato_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A89Contrato_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Valor_Enabled, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_unidadecontratacao_Internalname, "Unidade", "", "", lblTextblockcontrato_unidadecontratacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContrato_UnidadeContratacao, cmbContrato_UnidadeContratacao_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)), 1, cmbContrato_UnidadeContratacao_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContrato_UnidadeContratacao.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"", "", true, "HLP_Contrato.htm");
            cmbContrato_UnidadeContratacao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContrato_UnidadeContratacao_Internalname, "Values", (String)(cmbContrato_UnidadeContratacao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_quantidade_Internalname, "Quantidade", "", "", lblTextblockcontrato_quantidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_Quantidade_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A81Contrato_Quantidade), 9, 0, ",", "")), ((edtContrato_Quantidade_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A81Contrato_Quantidade), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A81Contrato_Quantidade), "ZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Quantidade_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Quantidade_Enabled, 0, "text", "", 70, "px", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "QuantidadeContrato", "right", false, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_valorunidadecontratacao_Internalname, "Valor Unidade Contrata��o", "", "", lblTextblockcontrato_valorunidadecontratacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_ValorUnidadeContratacao_Internalname, StringUtil.LTrim( StringUtil.NToC( A116Contrato_ValorUnidadeContratacao, 18, 5, ",", "")), ((edtContrato_ValorUnidadeContratacao_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A116Contrato_ValorUnidadeContratacao, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A116Contrato_ValorUnidadeContratacao, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_ValorUnidadeContratacao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_ValorUnidadeContratacao_Enabled, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_prdftrcada_Internalname, "Faturamento", "", "", lblTextblockcontrato_prdftrcada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContrato_PrdFtrCada, cmbContrato_PrdFtrCada_Internalname, StringUtil.RTrim( A1354Contrato_PrdFtrCada), 1, cmbContrato_PrdFtrCada_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContrato_PrdFtrCada.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", "", true, "HLP_Contrato.htm");
            cmbContrato_PrdFtrCada.CurrentValue = StringUtil.RTrim( A1354Contrato_PrdFtrCada);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContrato_PrdFtrCada_Internalname, "Values", (String)(cmbContrato_PrdFtrCada.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_prdftrini_Internalname, "Inicio", "", "", lblTextblockcontrato_prdftrini_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_PrdFtrIni_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1357Contrato_PrdFtrIni), 4, 0, ",", "")), ((edtContrato_PrdFtrIni_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1357Contrato_PrdFtrIni), "ZZZ9")) : context.localUtil.Format( (decimal)(A1357Contrato_PrdFtrIni), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_PrdFtrIni_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_PrdFtrIni_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_prdftrfim_Internalname, "Fim", "", "", lblTextblockcontrato_prdftrfim_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_PrdFtrFim_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1358Contrato_PrdFtrFim), 4, 0, ",", "")), ((edtContrato_PrdFtrFim_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1358Contrato_PrdFtrFim), "ZZZ9")) : context.localUtil.Format( (decimal)(A1358Contrato_PrdFtrFim), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_PrdFtrFim_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_PrdFtrFim_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_lmtftr_Internalname, "Limite de Faturamento", "", "", lblTextblockcontrato_lmtftr_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_119_0G17( true) ;
         }
         return  ;
      }

      protected void wb_table5_119_0G17e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_diaspagto_Internalname, "Dias para Pagto", "", "", lblTextblockcontrato_diaspagto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_DiasPagto_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A91Contrato_DiasPagto), 4, 0, ",", "")), ((edtContrato_DiasPagto_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A91Contrato_DiasPagto), "ZZZ9")) : context.localUtil.Format( (decimal)(A91Contrato_DiasPagto), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DiasPagto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_DiasPagto_Enabled, 0, "text", "", 45, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_regraspagto_Internalname, "Regras para pagamento", "", "", lblTextblockcontrato_regraspagto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContrato_RegrasPagto_Internalname, A90Contrato_RegrasPagto, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", 0, 1, edtContrato_RegrasPagto_Enabled, 0, 100, "%", 2, "row", StyleString, ClassString, "", "500", 1, "", "", 0, true, "RegrasPagto", "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_calculodivergencia_Internalname, "C�lculo da Diverg�ncia:", "", "", lblTextblockcontrato_calculodivergencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContrato_CalculoDivergencia, cmbContrato_CalculoDivergencia_Internalname, StringUtil.RTrim( A452Contrato_CalculoDivergencia), 1, cmbContrato_CalculoDivergencia_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContrato_CalculoDivergencia.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,140);\"", "", true, "HLP_Contrato.htm");
            cmbContrato_CalculoDivergencia.CurrentValue = StringUtil.RTrim( A452Contrato_CalculoDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContrato_CalculoDivergencia_Internalname, "Values", (String)(cmbContrato_CalculoDivergencia.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_indicedivergencia_Internalname, "Indice de Aceita��o", "", "", lblTextblockcontrato_indicedivergencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_144_0G17( true) ;
         }
         return  ;
      }

      protected void wb_table6_144_0G17e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_aceitapffs_Internalname, "Aceita menor PF da FS", "", "", lblTextblockcontrato_aceitapffs_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContrato_AceitaPFFS_Internalname, StringUtil.BoolToStr( A1150Contrato_AceitaPFFS), "", "", 1, chkContrato_AceitaPFFS.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(153, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,153);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ativo_Internalname, "Ativo", "", "", lblTextblockcontrato_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontrato_ativo_Visible, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContrato_Ativo_Internalname, StringUtil.BoolToStr( A92Contrato_Ativo), "", "", chkContrato_Ativo.Visible, chkContrato_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(158, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,158);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_sigla_Internalname, "Sigla", "", "", lblTextblockcontratada_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_Sigla_Internalname, StringUtil.RTrim( A438Contratada_Sigla), StringUtil.RTrim( context.localUtil.Format( A438Contratada_Sigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_Sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_tipofabrica_Internalname, "Tipo", "", "", lblTextblockcontratada_tipofabrica_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratada_TipoFabrica, cmbContratada_TipoFabrica_Internalname, StringUtil.RTrim( A516Contratada_TipoFabrica), 1, cmbContratada_TipoFabrica_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratada_TipoFabrica.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_Contrato.htm");
            cmbContratada_TipoFabrica.CurrentValue = StringUtil.RTrim( A516Contratada_TipoFabrica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratada_TipoFabrica_Internalname, "Values", (String)(cmbContratada_TipoFabrica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datainiciota_Internalname, "Termo Aditivo", "", "", lblTextblockcontrato_datainiciota_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataInicioTA_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataInicioTA_Internalname, context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"), context.localUtil.Format( A842Contrato_DataInicioTA, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataInicioTA_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContrato_DataInicioTA_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contrato.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataInicioTA_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContrato_DataInicioTA_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Contrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datafimta_Internalname, "Termo Aditivo", "", "", lblTextblockcontrato_datafimta_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataFimTA_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataFimTA_Internalname, context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"), context.localUtil.Format( A843Contrato_DataFimTA, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataFimTA_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContrato_DataFimTA_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contrato.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataFimTA_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContrato_DataFimTA_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Contrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datatermino_Internalname, "Vig�ncia", "", "", lblTextblockcontrato_datatermino_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataTermino_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataTermino_Internalname, context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"), context.localUtil.Format( A1869Contrato_DataTermino, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataTermino_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContrato_DataTermino_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contrato.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataTermino_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContrato_DataTermino_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Contrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_valorundcntatual_Internalname, "Unidade Contrata��o", "", "", lblTextblockcontrato_valorundcntatual_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_ValorUndCntAtual_Internalname, StringUtil.LTrim( StringUtil.NToC( A1870Contrato_ValorUndCntAtual, 18, 5, ",", "")), ((edtContrato_ValorUndCntAtual_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1870Contrato_ValorUndCntAtual, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1870Contrato_ValorUndCntAtual, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_ValorUndCntAtual_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_ValorUndCntAtual_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_prepostopescod_Internalname, "Preposto", "", "", lblTextblockcontrato_prepostopescod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_PrepostoPesCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0, ",", "")), ((edtContrato_PrepostoPesCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1016Contrato_PrepostoPesCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1016Contrato_PrepostoPesCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_PrepostoPesCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_PrepostoPesCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_prepostonom_Internalname, "Preposto", "", "", lblTextblockcontrato_prepostonom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_PrepostoNom_Internalname, StringUtil.RTrim( A1015Contrato_PrepostoNom), StringUtil.RTrim( context.localUtil.Format( A1015Contrato_PrepostoNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_PrepostoNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_PrepostoNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_identificacao_Internalname, "Contrato ", "", "", lblTextblockcontrato_identificacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Identificacao_Internalname, A2096Contrato_Identificacao, StringUtil.RTrim( context.localUtil.Format( A2096Contrato_Identificacao, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Identificacao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Identificacao_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            wb_table7_200_0G17( true) ;
         }
         return  ;
      }

      protected void wb_table7_200_0G17e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_13_0G17e( true) ;
         }
         else
         {
            wb_table3_13_0G17e( false) ;
         }
      }

      protected void wb_table7_200_0G17( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            wb_table8_203_0G17( true) ;
         }
         return  ;
      }

      protected void wb_table8_203_0G17e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_200_0G17e( true) ;
         }
         else
         {
            wb_table7_200_0G17e( false) ;
         }
      }

      protected void wb_table8_203_0G17( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedtrn_enter_Internalname, tblTablemergedtrn_enter_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 206,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 208,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 210,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_203_0G17e( true) ;
         }
         else
         {
            wb_table8_203_0G17e( false) ;
         }
      }

      protected void wb_table6_144_0G17( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_indicedivergencia_Internalname, tblTablemergedcontrato_indicedivergencia_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_IndiceDivergencia_Internalname, StringUtil.LTrim( StringUtil.NToC( A453Contrato_IndiceDivergencia, 6, 2, ",", "")), ((edtContrato_IndiceDivergencia_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A453Contrato_IndiceDivergencia, "ZZ9.99")) : context.localUtil.Format( A453Contrato_IndiceDivergencia, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,147);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_IndiceDivergencia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_IndiceDivergencia_Enabled, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContrato_indicedivergencia_righttext_Internalname, "%", "", "", lblContrato_indicedivergencia_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_144_0G17e( true) ;
         }
         else
         {
            wb_table6_144_0G17e( false) ;
         }
      }

      protected void wb_table5_119_0G17( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_lmtftr_Internalname, tblTablemergedcontrato_lmtftr_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContrato_LmtFtr_Internalname, StringUtil.LTrim( StringUtil.NToC( A2086Contrato_LmtFtr, 14, 5, ",", "")), ((edtContrato_LmtFtr_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2086Contrato_LmtFtr, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A2086Contrato_LmtFtr, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_LmtFtr_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_LmtFtr_Enabled, 0, "text", "", 70, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_Contrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUnidadedemedicao, cmbavUnidadedemedicao_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0)), 1, cmbavUnidadedemedicao_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavUnidadedemedicao.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_Contrato.htm");
            cmbavUnidadedemedicao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUnidadedemedicao_Internalname, "Values", (String)(cmbavUnidadedemedicao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_119_0G17e( true) ;
         }
         else
         {
            wb_table5_119_0G17e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110G2 */
         E110G2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
               n41Contratada_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
               n42Contratada_PessoaCNPJ = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               dynContrato_PrepostoCod.CurrentValue = cgiGet( dynContrato_PrepostoCod_Internalname);
               A1013Contrato_PrepostoCod = (int)(NumberUtil.Val( cgiGet( dynContrato_PrepostoCod_Internalname), "."));
               n1013Contrato_PrepostoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
               n1013Contrato_PrepostoCod = ((0==A1013Contrato_PrepostoCod) ? true : false);
               A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
               n78Contrato_NumeroAta = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               n78Contrato_NumeroAta = (String.IsNullOrEmpty(StringUtil.RTrim( A78Contrato_NumeroAta)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATO_ANO");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_Ano_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A79Contrato_Ano = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               }
               else
               {
                  A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               }
               A80Contrato_Objeto = cgiGet( edtContrato_Objeto_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A80Contrato_Objeto", A80Contrato_Objeto);
               if ( context.localUtil.VCDate( cgiGet( edtContrato_DataVigenciaInicio_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Vig�ncia Inicio"}), 1, "CONTRATO_DATAVIGENCIAINICIO");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_DataVigenciaInicio_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A82Contrato_DataVigenciaInicio = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
               }
               else
               {
                  A82Contrato_DataVigenciaInicio = context.localUtil.CToD( cgiGet( edtContrato_DataVigenciaInicio_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtContrato_DataVigenciaTermino_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Vig�ncia T�rmino"}), 1, "CONTRATO_DATAVIGENCIATERMINO");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_DataVigenciaTermino_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A83Contrato_DataVigenciaTermino = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
               }
               else
               {
                  A83Contrato_DataVigenciaTermino = context.localUtil.CToD( cgiGet( edtContrato_DataVigenciaTermino_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtContrato_DataPublicacaoDOU_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Publica��o DOU"}), 1, "CONTRATO_DATAPUBLICACAODOU");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_DataPublicacaoDOU_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A84Contrato_DataPublicacaoDOU = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A84Contrato_DataPublicacaoDOU", context.localUtil.Format(A84Contrato_DataPublicacaoDOU, "99/99/99"));
               }
               else
               {
                  A84Contrato_DataPublicacaoDOU = context.localUtil.CToD( cgiGet( edtContrato_DataPublicacaoDOU_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A84Contrato_DataPublicacaoDOU", context.localUtil.Format(A84Contrato_DataPublicacaoDOU, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtContrato_DataAssinatura_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Assinatura"}), 1, "CONTRATO_DATAASSINATURA");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_DataAssinatura_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A85Contrato_DataAssinatura = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A85Contrato_DataAssinatura", context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"));
               }
               else
               {
                  A85Contrato_DataAssinatura = context.localUtil.CToD( cgiGet( edtContrato_DataAssinatura_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A85Contrato_DataAssinatura", context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtContrato_DataPedidoReajuste_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pedido Reajuste"}), 1, "CONTRATO_DATAPEDIDOREAJUSTE");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_DataPedidoReajuste_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A86Contrato_DataPedidoReajuste = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A86Contrato_DataPedidoReajuste", context.localUtil.Format(A86Contrato_DataPedidoReajuste, "99/99/99"));
               }
               else
               {
                  A86Contrato_DataPedidoReajuste = context.localUtil.CToD( cgiGet( edtContrato_DataPedidoReajuste_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A86Contrato_DataPedidoReajuste", context.localUtil.Format(A86Contrato_DataPedidoReajuste, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtContrato_DataTerminoAta_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"T�rmino da Ata"}), 1, "CONTRATO_DATATERMINOATA");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_DataTerminoAta_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A87Contrato_DataTerminoAta = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A87Contrato_DataTerminoAta", context.localUtil.Format(A87Contrato_DataTerminoAta, "99/99/99"));
               }
               else
               {
                  A87Contrato_DataTerminoAta = context.localUtil.CToD( cgiGet( edtContrato_DataTerminoAta_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A87Contrato_DataTerminoAta", context.localUtil.Format(A87Contrato_DataTerminoAta, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtContrato_DataFimAdaptacao_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Fim Adapta��o"}), 1, "CONTRATO_DATAFIMADAPTACAO");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_DataFimAdaptacao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A88Contrato_DataFimAdaptacao = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A88Contrato_DataFimAdaptacao", context.localUtil.Format(A88Contrato_DataFimAdaptacao, "99/99/99"));
               }
               else
               {
                  A88Contrato_DataFimAdaptacao = context.localUtil.CToD( cgiGet( edtContrato_DataFimAdaptacao_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A88Contrato_DataFimAdaptacao", context.localUtil.Format(A88Contrato_DataFimAdaptacao, "99/99/99"));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContrato_Valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContrato_Valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATO_VALOR");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_Valor_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A89Contrato_Valor = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A89Contrato_Valor", StringUtil.LTrim( StringUtil.Str( A89Contrato_Valor, 18, 5)));
               }
               else
               {
                  A89Contrato_Valor = context.localUtil.CToN( cgiGet( edtContrato_Valor_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A89Contrato_Valor", StringUtil.LTrim( StringUtil.Str( A89Contrato_Valor, 18, 5)));
               }
               cmbContrato_UnidadeContratacao.CurrentValue = cgiGet( cmbContrato_UnidadeContratacao_Internalname);
               A115Contrato_UnidadeContratacao = (short)(NumberUtil.Val( cgiGet( cmbContrato_UnidadeContratacao_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContrato_Quantidade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContrato_Quantidade_Internalname), ",", ".") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATO_QUANTIDADE");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_Quantidade_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A81Contrato_Quantidade = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A81Contrato_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A81Contrato_Quantidade), 9, 0)));
               }
               else
               {
                  A81Contrato_Quantidade = (int)(context.localUtil.CToN( cgiGet( edtContrato_Quantidade_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A81Contrato_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A81Contrato_Quantidade), 9, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContrato_ValorUnidadeContratacao_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContrato_ValorUnidadeContratacao_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATO_VALORUNIDADECONTRATACAO");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_ValorUnidadeContratacao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A116Contrato_ValorUnidadeContratacao = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
               }
               else
               {
                  A116Contrato_ValorUnidadeContratacao = context.localUtil.CToN( cgiGet( edtContrato_ValorUnidadeContratacao_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
               }
               cmbContrato_PrdFtrCada.CurrentValue = cgiGet( cmbContrato_PrdFtrCada_Internalname);
               A1354Contrato_PrdFtrCada = cgiGet( cmbContrato_PrdFtrCada_Internalname);
               n1354Contrato_PrdFtrCada = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1354Contrato_PrdFtrCada", A1354Contrato_PrdFtrCada);
               n1354Contrato_PrdFtrCada = (String.IsNullOrEmpty(StringUtil.RTrim( A1354Contrato_PrdFtrCada)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContrato_PrdFtrIni_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContrato_PrdFtrIni_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATO_PRDFTRINI");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_PrdFtrIni_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1357Contrato_PrdFtrIni = 0;
                  n1357Contrato_PrdFtrIni = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1357Contrato_PrdFtrIni", StringUtil.LTrim( StringUtil.Str( (decimal)(A1357Contrato_PrdFtrIni), 4, 0)));
               }
               else
               {
                  A1357Contrato_PrdFtrIni = (short)(context.localUtil.CToN( cgiGet( edtContrato_PrdFtrIni_Internalname), ",", "."));
                  n1357Contrato_PrdFtrIni = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1357Contrato_PrdFtrIni", StringUtil.LTrim( StringUtil.Str( (decimal)(A1357Contrato_PrdFtrIni), 4, 0)));
               }
               n1357Contrato_PrdFtrIni = ((0==A1357Contrato_PrdFtrIni) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContrato_PrdFtrFim_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContrato_PrdFtrFim_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATO_PRDFTRFIM");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_PrdFtrFim_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1358Contrato_PrdFtrFim = 0;
                  n1358Contrato_PrdFtrFim = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1358Contrato_PrdFtrFim", StringUtil.LTrim( StringUtil.Str( (decimal)(A1358Contrato_PrdFtrFim), 4, 0)));
               }
               else
               {
                  A1358Contrato_PrdFtrFim = (short)(context.localUtil.CToN( cgiGet( edtContrato_PrdFtrFim_Internalname), ",", "."));
                  n1358Contrato_PrdFtrFim = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1358Contrato_PrdFtrFim", StringUtil.LTrim( StringUtil.Str( (decimal)(A1358Contrato_PrdFtrFim), 4, 0)));
               }
               n1358Contrato_PrdFtrFim = ((0==A1358Contrato_PrdFtrFim) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContrato_LmtFtr_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContrato_LmtFtr_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATO_LMTFTR");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_LmtFtr_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2086Contrato_LmtFtr = 0;
                  n2086Contrato_LmtFtr = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2086Contrato_LmtFtr", StringUtil.LTrim( StringUtil.Str( A2086Contrato_LmtFtr, 14, 5)));
               }
               else
               {
                  A2086Contrato_LmtFtr = context.localUtil.CToN( cgiGet( edtContrato_LmtFtr_Internalname), ",", ".");
                  n2086Contrato_LmtFtr = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2086Contrato_LmtFtr", StringUtil.LTrim( StringUtil.Str( A2086Contrato_LmtFtr, 14, 5)));
               }
               n2086Contrato_LmtFtr = ((Convert.ToDecimal(0)==A2086Contrato_LmtFtr) ? true : false);
               cmbavUnidadedemedicao.CurrentValue = cgiGet( cmbavUnidadedemedicao_Internalname);
               AV25UnidadeDeMedicao = (short)(NumberUtil.Val( cgiGet( cmbavUnidadedemedicao_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25UnidadeDeMedicao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContrato_DiasPagto_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContrato_DiasPagto_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATO_DIASPAGTO");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_DiasPagto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A91Contrato_DiasPagto = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A91Contrato_DiasPagto", StringUtil.LTrim( StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0)));
               }
               else
               {
                  A91Contrato_DiasPagto = (short)(context.localUtil.CToN( cgiGet( edtContrato_DiasPagto_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A91Contrato_DiasPagto", StringUtil.LTrim( StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0)));
               }
               A90Contrato_RegrasPagto = cgiGet( edtContrato_RegrasPagto_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A90Contrato_RegrasPagto", A90Contrato_RegrasPagto);
               cmbContrato_CalculoDivergencia.CurrentValue = cgiGet( cmbContrato_CalculoDivergencia_Internalname);
               A452Contrato_CalculoDivergencia = cgiGet( cmbContrato_CalculoDivergencia_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContrato_IndiceDivergencia_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContrato_IndiceDivergencia_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATO_INDICEDIVERGENCIA");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_IndiceDivergencia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A453Contrato_IndiceDivergencia = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
               }
               else
               {
                  A453Contrato_IndiceDivergencia = context.localUtil.CToN( cgiGet( edtContrato_IndiceDivergencia_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
               }
               A1150Contrato_AceitaPFFS = StringUtil.StrToBool( cgiGet( chkContrato_AceitaPFFS_Internalname));
               n1150Contrato_AceitaPFFS = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1150Contrato_AceitaPFFS", A1150Contrato_AceitaPFFS);
               n1150Contrato_AceitaPFFS = ((false==A1150Contrato_AceitaPFFS) ? true : false);
               A92Contrato_Ativo = StringUtil.StrToBool( cgiGet( chkContrato_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92Contrato_Ativo", A92Contrato_Ativo);
               A438Contratada_Sigla = StringUtil.Upper( cgiGet( edtContratada_Sigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
               cmbContratada_TipoFabrica.CurrentValue = cgiGet( cmbContratada_TipoFabrica_Internalname);
               A516Contratada_TipoFabrica = cgiGet( cmbContratada_TipoFabrica_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
               A842Contrato_DataInicioTA = context.localUtil.CToD( cgiGet( edtContrato_DataInicioTA_Internalname), 2);
               n842Contrato_DataInicioTA = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
               A843Contrato_DataFimTA = context.localUtil.CToD( cgiGet( edtContrato_DataFimTA_Internalname), 2);
               n843Contrato_DataFimTA = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
               A1869Contrato_DataTermino = context.localUtil.CToD( cgiGet( edtContrato_DataTermino_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
               A1870Contrato_ValorUndCntAtual = context.localUtil.CToN( cgiGet( edtContrato_ValorUndCntAtual_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1870Contrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( A1870Contrato_ValorUndCntAtual, 18, 5)));
               A1016Contrato_PrepostoPesCod = (int)(context.localUtil.CToN( cgiGet( edtContrato_PrepostoPesCod_Internalname), ",", "."));
               n1016Contrato_PrepostoPesCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1016Contrato_PrepostoPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0)));
               A1015Contrato_PrepostoNom = StringUtil.Upper( cgiGet( edtContrato_PrepostoNom_Internalname));
               n1015Contrato_PrepostoNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1015Contrato_PrepostoNom", A1015Contrato_PrepostoNom);
               A2096Contrato_Identificacao = cgiGet( edtContrato_Identificacao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2096Contrato_Identificacao", A2096Contrato_Identificacao);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContrato_AreaTrabalhoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContrato_AreaTrabalhoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATO_AREATRABALHOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A75Contrato_AreaTrabalhoCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
               }
               else
               {
                  A75Contrato_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContrato_AreaTrabalhoCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
               }
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
               n74Contrato_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATADA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratada_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A39Contratada_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               }
               else
               {
                  A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               }
               /* Read saved values. */
               Z74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z74Contrato_Codigo"), ",", "."));
               Z77Contrato_Numero = cgiGet( "Z77Contrato_Numero");
               Z78Contrato_NumeroAta = cgiGet( "Z78Contrato_NumeroAta");
               n78Contrato_NumeroAta = (String.IsNullOrEmpty(StringUtil.RTrim( A78Contrato_NumeroAta)) ? true : false);
               Z79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( "Z79Contrato_Ano"), ",", "."));
               Z115Contrato_UnidadeContratacao = (short)(context.localUtil.CToN( cgiGet( "Z115Contrato_UnidadeContratacao"), ",", "."));
               Z81Contrato_Quantidade = (int)(context.localUtil.CToN( cgiGet( "Z81Contrato_Quantidade"), ",", "."));
               Z82Contrato_DataVigenciaInicio = context.localUtil.CToD( cgiGet( "Z82Contrato_DataVigenciaInicio"), 0);
               Z83Contrato_DataVigenciaTermino = context.localUtil.CToD( cgiGet( "Z83Contrato_DataVigenciaTermino"), 0);
               Z84Contrato_DataPublicacaoDOU = context.localUtil.CToD( cgiGet( "Z84Contrato_DataPublicacaoDOU"), 0);
               Z85Contrato_DataAssinatura = context.localUtil.CToD( cgiGet( "Z85Contrato_DataAssinatura"), 0);
               Z86Contrato_DataPedidoReajuste = context.localUtil.CToD( cgiGet( "Z86Contrato_DataPedidoReajuste"), 0);
               Z87Contrato_DataTerminoAta = context.localUtil.CToD( cgiGet( "Z87Contrato_DataTerminoAta"), 0);
               Z88Contrato_DataFimAdaptacao = context.localUtil.CToD( cgiGet( "Z88Contrato_DataFimAdaptacao"), 0);
               Z89Contrato_Valor = context.localUtil.CToN( cgiGet( "Z89Contrato_Valor"), ",", ".");
               Z116Contrato_ValorUnidadeContratacao = context.localUtil.CToN( cgiGet( "Z116Contrato_ValorUnidadeContratacao"), ",", ".");
               Z91Contrato_DiasPagto = (short)(context.localUtil.CToN( cgiGet( "Z91Contrato_DiasPagto"), ",", "."));
               Z452Contrato_CalculoDivergencia = cgiGet( "Z452Contrato_CalculoDivergencia");
               Z453Contrato_IndiceDivergencia = context.localUtil.CToN( cgiGet( "Z453Contrato_IndiceDivergencia"), ",", ".");
               Z1150Contrato_AceitaPFFS = StringUtil.StrToBool( cgiGet( "Z1150Contrato_AceitaPFFS"));
               n1150Contrato_AceitaPFFS = ((false==A1150Contrato_AceitaPFFS) ? true : false);
               Z92Contrato_Ativo = StringUtil.StrToBool( cgiGet( "Z92Contrato_Ativo"));
               Z1354Contrato_PrdFtrCada = cgiGet( "Z1354Contrato_PrdFtrCada");
               n1354Contrato_PrdFtrCada = (String.IsNullOrEmpty(StringUtil.RTrim( A1354Contrato_PrdFtrCada)) ? true : false);
               Z2086Contrato_LmtFtr = context.localUtil.CToN( cgiGet( "Z2086Contrato_LmtFtr"), ",", ".");
               n2086Contrato_LmtFtr = ((Convert.ToDecimal(0)==A2086Contrato_LmtFtr) ? true : false);
               Z1357Contrato_PrdFtrIni = (short)(context.localUtil.CToN( cgiGet( "Z1357Contrato_PrdFtrIni"), ",", "."));
               n1357Contrato_PrdFtrIni = ((0==A1357Contrato_PrdFtrIni) ? true : false);
               Z1358Contrato_PrdFtrFim = (short)(context.localUtil.CToN( cgiGet( "Z1358Contrato_PrdFtrFim"), ",", "."));
               n1358Contrato_PrdFtrFim = ((0==A1358Contrato_PrdFtrFim) ? true : false);
               Z39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z39Contratada_Codigo"), ",", "."));
               Z75Contrato_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z75Contrato_AreaTrabalhoCod"), ",", "."));
               Z1013Contrato_PrepostoCod = (int)(context.localUtil.CToN( cgiGet( "Z1013Contrato_PrepostoCod"), ",", "."));
               n1013Contrato_PrepostoCod = ((0==A1013Contrato_PrepostoCod) ? true : false);
               O1013Contrato_PrepostoCod = (int)(context.localUtil.CToN( cgiGet( "O1013Contrato_PrepostoCod"), ",", "."));
               n1013Contrato_PrepostoCod = ((0==A1013Contrato_PrepostoCod) ? true : false);
               O116Contrato_ValorUnidadeContratacao = context.localUtil.CToN( cgiGet( "O116Contrato_ValorUnidadeContratacao"), ",", ".");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N75Contrato_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N75Contrato_AreaTrabalhoCod"), ",", "."));
               N39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "N39Contratada_Codigo"), ",", "."));
               N1013Contrato_PrepostoCod = (int)(context.localUtil.CToN( cgiGet( "N1013Contrato_PrepostoCod"), ",", "."));
               n1013Contrato_PrepostoCod = ((0==A1013Contrato_PrepostoCod) ? true : false);
               AV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATO_CODIGO"), ",", "."));
               AV11Insert_Contrato_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATO_AREATRABALHOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               AV12Insert_Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATADA_CODIGO"), ",", "."));
               AV16Insert_Contrato_PrepostoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATO_PREPOSTOCOD"), ",", "."));
               AV17Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATADA_CODIGO"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vAUDITINGOBJECT"), AV24AuditingObject);
               AV22SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSALDOCONTRATO_CODIGO"), ",", "."));
               A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_PESSOACOD"), ",", "."));
               A76Contrato_AreaTrabalhoDes = cgiGet( "CONTRATO_AREATRABALHODES");
               n76Contrato_AreaTrabalhoDes = false;
               AV27Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               Confirmpanel_Width = cgiGet( "CONFIRMPANEL_Width");
               Confirmpanel_Height = cgiGet( "CONFIRMPANEL_Height");
               Confirmpanel_Closeable = StringUtil.StrToBool( cgiGet( "CONFIRMPANEL_Closeable"));
               Confirmpanel_Title = cgiGet( "CONFIRMPANEL_Title");
               Confirmpanel_Icon = cgiGet( "CONFIRMPANEL_Icon");
               Confirmpanel_Modal = StringUtil.StrToBool( cgiGet( "CONFIRMPANEL_Modal"));
               Confirmpanel_Result = cgiGet( "CONFIRMPANEL_Result");
               Confirmpanel_Enabled = StringUtil.StrToBool( cgiGet( "CONFIRMPANEL_Enabled"));
               Confirmpanel_Class = cgiGet( "CONFIRMPANEL_Class");
               Confirmpanel_Confirmtext = cgiGet( "CONFIRMPANEL_Confirmtext");
               Confirmpanel_Buttonyestext = cgiGet( "CONFIRMPANEL_Buttonyestext");
               Confirmpanel_Buttonnotext = cgiGet( "CONFIRMPANEL_Buttonnotext");
               Confirmpanel_Buttoncanceltext = cgiGet( "CONFIRMPANEL_Buttoncanceltext");
               Confirmpanel_Confirmtype = cgiGet( "CONFIRMPANEL_Confirmtype");
               Confirmpanel_Draggeable = StringUtil.StrToBool( cgiGet( "CONFIRMPANEL_Draggeable"));
               Confirmpanel_Visible = StringUtil.StrToBool( cgiGet( "CONFIRMPANEL_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Contrato";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A74Contrato_Codigo != Z74Contrato_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contrato:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n74Contrato_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode17 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode17;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound17 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0G0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContrato_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110G2 */
                           E110G2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120G2 */
                           E120G2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "CONTRATO_VALORUNIDADECONTRATACAO.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E130G2 */
                           E130G2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120G2 */
            E120G2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0G17( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0G17( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUnidadedemedicao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUnidadedemedicao.Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0G0( )
      {
         BeforeValidate0G17( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0G17( ) ;
            }
            else
            {
               CheckExtendedTable0G17( ) ;
               CloseExtendedTableCursors0G17( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0G0( )
      {
      }

      protected void E110G2( )
      {
         /* Start Routine */
         if ( ! new wwpbaseobjects.isauthorized(context).executeUdp(  AV27Pgmname) )
         {
            context.wjLoc = formatLink("wwpbaseobjects.notauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV27Pgmname));
            context.wjLocDisableFrm = 1;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         AV11Insert_Contrato_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contrato_AreaTrabalhoCod), 6, 0)));
         AV12Insert_Contratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Contratada_Codigo), 6, 0)));
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV27Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV28GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GXV1), 8, 0)));
            while ( AV28GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV28GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contrato_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contrato_AreaTrabalhoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contratada_Codigo") == 0 )
               {
                  AV12Insert_Contratada_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Contratada_Codigo), 6, 0)));
               }
               AV28GXV1 = (int)(AV28GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GXV1), 8, 0)));
            }
         }
         edtContrato_AreaTrabalhoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_AreaTrabalhoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_AreaTrabalhoCod_Visible), 5, 0)));
         edtContrato_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Visible), 5, 0)));
         edtContratada_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Codigo_Visible), 5, 0)));
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            lblTextblockcontrato_ativo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontrato_ativo_Visible), 5, 0)));
            chkContrato_Ativo.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContrato_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContrato_Ativo.Visible), 5, 0)));
         }
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV27Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV29GXV2 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GXV2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GXV2), 8, 0)));
            while ( AV29GXV2 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV29GXV2));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contrato_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contrato_AreaTrabalhoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contratada_Codigo") == 0 )
               {
                  AV12Insert_Contratada_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Contratada_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contrato_PrepostoCod") == 0 )
               {
                  AV16Insert_Contrato_PrepostoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Insert_Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Insert_Contrato_PrepostoCod), 6, 0)));
               }
               AV29GXV2 = (int)(AV29GXV2+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GXV2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29GXV2), 8, 0)));
            }
         }
         edtContrato_AreaTrabalhoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_AreaTrabalhoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_AreaTrabalhoCod_Visible), 5, 0)));
         edtContrato_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Visible), 5, 0)));
         edtContratada_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Codigo_Visible), 5, 0)));
         dynContrato_PrepostoCod.Enabled = (AV8WWPContext.gxTpr_Userehcontratada||AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContrato_PrepostoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContrato_PrepostoCod.Enabled), 5, 0)));
         Form.Headerrawhtml = Form.Headerrawhtml+"<script type=\"text/javascript\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"form\").bind(\"keydown\", function(e) {if (e.keyCode === 13) return false;});";
         Form.Headerrawhtml = Form.Headerrawhtml+"</script>";
      }

      protected void E120G2( )
      {
         /* After Trn Routine */
         new wwpbaseobjects.audittransaction(context ).execute(  AV24AuditingObject,  AV27Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Pgmname", AV27Pgmname);
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            context.wjLoc = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            context.wjLocDisableFrm = 1;
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) && ( A116Contrato_ValorUnidadeContratacao != O116Contrato_ValorUnidadeContratacao ) )
            {
               new prc_atualizavalorpf(context ).execute( ref  A75Contrato_AreaTrabalhoCod,  A116Contrato_ValorUnidadeContratacao,  0) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
            }
         }
         if ( false )
         {
            new wwpbaseobjects.audittransaction(context ).execute(  AV24AuditingObject,  AV27Pgmname) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Pgmname", AV27Pgmname);
            if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
            {
               context.wjLoc = formatLink("wwcontrato.aspx") ;
               context.wjLocDisableFrm = 1;
            }
            context.setWebReturnParms(new Object[] {(int)AV17Contratada_Codigo});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E130G2( )
      {
         /* Contrato_ValorUnidadeContratacao_Isvalid Routine */
         if ( ( A116Contrato_ValorUnidadeContratacao != O116Contrato_ValorUnidadeContratacao ) && new prc_temsrvdocntcomvlr(context).executeUdp( ref  AV7Contrato_Codigo) )
         {
            this.executeUsercontrolMethod("", false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
      }

      protected void ZM0G17( short GX_JID )
      {
         if ( ( GX_JID == 43 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z77Contrato_Numero = T000G3_A77Contrato_Numero[0];
               Z78Contrato_NumeroAta = T000G3_A78Contrato_NumeroAta[0];
               Z79Contrato_Ano = T000G3_A79Contrato_Ano[0];
               Z115Contrato_UnidadeContratacao = T000G3_A115Contrato_UnidadeContratacao[0];
               Z81Contrato_Quantidade = T000G3_A81Contrato_Quantidade[0];
               Z82Contrato_DataVigenciaInicio = T000G3_A82Contrato_DataVigenciaInicio[0];
               Z83Contrato_DataVigenciaTermino = T000G3_A83Contrato_DataVigenciaTermino[0];
               Z84Contrato_DataPublicacaoDOU = T000G3_A84Contrato_DataPublicacaoDOU[0];
               Z85Contrato_DataAssinatura = T000G3_A85Contrato_DataAssinatura[0];
               Z86Contrato_DataPedidoReajuste = T000G3_A86Contrato_DataPedidoReajuste[0];
               Z87Contrato_DataTerminoAta = T000G3_A87Contrato_DataTerminoAta[0];
               Z88Contrato_DataFimAdaptacao = T000G3_A88Contrato_DataFimAdaptacao[0];
               Z89Contrato_Valor = T000G3_A89Contrato_Valor[0];
               Z116Contrato_ValorUnidadeContratacao = T000G3_A116Contrato_ValorUnidadeContratacao[0];
               Z91Contrato_DiasPagto = T000G3_A91Contrato_DiasPagto[0];
               Z452Contrato_CalculoDivergencia = T000G3_A452Contrato_CalculoDivergencia[0];
               Z453Contrato_IndiceDivergencia = T000G3_A453Contrato_IndiceDivergencia[0];
               Z1150Contrato_AceitaPFFS = T000G3_A1150Contrato_AceitaPFFS[0];
               Z92Contrato_Ativo = T000G3_A92Contrato_Ativo[0];
               Z1354Contrato_PrdFtrCada = T000G3_A1354Contrato_PrdFtrCada[0];
               Z2086Contrato_LmtFtr = T000G3_A2086Contrato_LmtFtr[0];
               Z1357Contrato_PrdFtrIni = T000G3_A1357Contrato_PrdFtrIni[0];
               Z1358Contrato_PrdFtrFim = T000G3_A1358Contrato_PrdFtrFim[0];
               Z39Contratada_Codigo = T000G3_A39Contratada_Codigo[0];
               Z75Contrato_AreaTrabalhoCod = T000G3_A75Contrato_AreaTrabalhoCod[0];
               Z1013Contrato_PrepostoCod = T000G3_A1013Contrato_PrepostoCod[0];
            }
            else
            {
               Z77Contrato_Numero = A77Contrato_Numero;
               Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
               Z79Contrato_Ano = A79Contrato_Ano;
               Z115Contrato_UnidadeContratacao = A115Contrato_UnidadeContratacao;
               Z81Contrato_Quantidade = A81Contrato_Quantidade;
               Z82Contrato_DataVigenciaInicio = A82Contrato_DataVigenciaInicio;
               Z83Contrato_DataVigenciaTermino = A83Contrato_DataVigenciaTermino;
               Z84Contrato_DataPublicacaoDOU = A84Contrato_DataPublicacaoDOU;
               Z85Contrato_DataAssinatura = A85Contrato_DataAssinatura;
               Z86Contrato_DataPedidoReajuste = A86Contrato_DataPedidoReajuste;
               Z87Contrato_DataTerminoAta = A87Contrato_DataTerminoAta;
               Z88Contrato_DataFimAdaptacao = A88Contrato_DataFimAdaptacao;
               Z89Contrato_Valor = A89Contrato_Valor;
               Z116Contrato_ValorUnidadeContratacao = A116Contrato_ValorUnidadeContratacao;
               Z91Contrato_DiasPagto = A91Contrato_DiasPagto;
               Z452Contrato_CalculoDivergencia = A452Contrato_CalculoDivergencia;
               Z453Contrato_IndiceDivergencia = A453Contrato_IndiceDivergencia;
               Z1150Contrato_AceitaPFFS = A1150Contrato_AceitaPFFS;
               Z92Contrato_Ativo = A92Contrato_Ativo;
               Z1354Contrato_PrdFtrCada = A1354Contrato_PrdFtrCada;
               Z2086Contrato_LmtFtr = A2086Contrato_LmtFtr;
               Z1357Contrato_PrdFtrIni = A1357Contrato_PrdFtrIni;
               Z1358Contrato_PrdFtrFim = A1358Contrato_PrdFtrFim;
               Z39Contratada_Codigo = A39Contratada_Codigo;
               Z75Contrato_AreaTrabalhoCod = A75Contrato_AreaTrabalhoCod;
               Z1013Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
            }
         }
         if ( GX_JID == -43 )
         {
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z80Contrato_Objeto = A80Contrato_Objeto;
            Z115Contrato_UnidadeContratacao = A115Contrato_UnidadeContratacao;
            Z81Contrato_Quantidade = A81Contrato_Quantidade;
            Z82Contrato_DataVigenciaInicio = A82Contrato_DataVigenciaInicio;
            Z83Contrato_DataVigenciaTermino = A83Contrato_DataVigenciaTermino;
            Z84Contrato_DataPublicacaoDOU = A84Contrato_DataPublicacaoDOU;
            Z85Contrato_DataAssinatura = A85Contrato_DataAssinatura;
            Z86Contrato_DataPedidoReajuste = A86Contrato_DataPedidoReajuste;
            Z87Contrato_DataTerminoAta = A87Contrato_DataTerminoAta;
            Z88Contrato_DataFimAdaptacao = A88Contrato_DataFimAdaptacao;
            Z89Contrato_Valor = A89Contrato_Valor;
            Z116Contrato_ValorUnidadeContratacao = A116Contrato_ValorUnidadeContratacao;
            Z90Contrato_RegrasPagto = A90Contrato_RegrasPagto;
            Z91Contrato_DiasPagto = A91Contrato_DiasPagto;
            Z452Contrato_CalculoDivergencia = A452Contrato_CalculoDivergencia;
            Z453Contrato_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            Z1150Contrato_AceitaPFFS = A1150Contrato_AceitaPFFS;
            Z92Contrato_Ativo = A92Contrato_Ativo;
            Z1354Contrato_PrdFtrCada = A1354Contrato_PrdFtrCada;
            Z2086Contrato_LmtFtr = A2086Contrato_LmtFtr;
            Z1357Contrato_PrdFtrIni = A1357Contrato_PrdFtrIni;
            Z1358Contrato_PrdFtrFim = A1358Contrato_PrdFtrFim;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z75Contrato_AreaTrabalhoCod = A75Contrato_AreaTrabalhoCod;
            Z1013Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
            Z842Contrato_DataInicioTA = A842Contrato_DataInicioTA;
            Z843Contrato_DataFimTA = A843Contrato_DataFimTA;
            Z76Contrato_AreaTrabalhoDes = A76Contrato_AreaTrabalhoDes;
            Z438Contratada_Sigla = A438Contratada_Sigla;
            Z516Contratada_TipoFabrica = A516Contratada_TipoFabrica;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            Z1016Contrato_PrepostoPesCod = A1016Contrato_PrepostoPesCod;
            Z1015Contrato_PrepostoNom = A1015Contrato_PrepostoNom;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContrato_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtContrato_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV7Contrato_Codigo;
            n74Contrato_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         GXACONTRATO_PREPOSTOCOD_html0G17( AV17Contratada_Codigo) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_AreaTrabalhoCod) )
         {
            edtContrato_AreaTrabalhoCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_AreaTrabalhoCod_Enabled), 5, 0)));
         }
         else
         {
            edtContrato_AreaTrabalhoCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_AreaTrabalhoCod_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Contratada_Codigo) )
         {
            edtContratada_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtContratada_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Codigo_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV16Insert_Contrato_PrepostoCod) )
         {
            dynContrato_PrepostoCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContrato_PrepostoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContrato_PrepostoCod.Enabled), 5, 0)));
         }
         else
         {
            dynContrato_PrepostoCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContrato_PrepostoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContrato_PrepostoCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV16Insert_Contrato_PrepostoCod) )
         {
            A1013Contrato_PrepostoCod = AV16Insert_Contrato_PrepostoCod;
            n1013Contrato_PrepostoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Contratada_Codigo) )
         {
            A39Contratada_Codigo = AV12Insert_Contratada_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_AreaTrabalhoCod) )
         {
            A75Contrato_AreaTrabalhoCod = AV11Insert_Contrato_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A75Contrato_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
            {
               A75Contrato_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A92Contrato_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A92Contrato_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92Contrato_Ativo", A92Contrato_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A453Contrato_IndiceDivergencia) && ( Gx_BScreen == 0 ) )
         {
            A453Contrato_IndiceDivergencia = (decimal)(10);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A452Contrato_CalculoDivergencia)) && ( Gx_BScreen == 0 ) )
         {
            A452Contrato_CalculoDivergencia = "B";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A91Contrato_DiasPagto) && ( Gx_BScreen == 0 ) )
         {
            A91Contrato_DiasPagto = 30;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A91Contrato_DiasPagto", StringUtil.LTrim( StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T000G11 */
            pr_default.execute(7, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(7) != 101) )
            {
               A842Contrato_DataInicioTA = T000G11_A842Contrato_DataInicioTA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
               n842Contrato_DataInicioTA = T000G11_n842Contrato_DataInicioTA[0];
            }
            else
            {
               A842Contrato_DataInicioTA = DateTime.MinValue;
               n842Contrato_DataInicioTA = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
            }
            pr_default.close(7);
            /* Using cursor T000G14 */
            pr_default.execute(8, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(8) != 101) )
            {
               A843Contrato_DataFimTA = T000G14_A843Contrato_DataFimTA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
               n843Contrato_DataFimTA = T000G14_n843Contrato_DataFimTA[0];
            }
            else
            {
               A843Contrato_DataFimTA = DateTime.MinValue;
               n843Contrato_DataFimTA = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
            }
            pr_default.close(8);
            /* Using cursor T000G6 */
            pr_default.execute(4, new Object[] {n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod});
            A1016Contrato_PrepostoPesCod = T000G6_A1016Contrato_PrepostoPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1016Contrato_PrepostoPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0)));
            n1016Contrato_PrepostoPesCod = T000G6_n1016Contrato_PrepostoPesCod[0];
            pr_default.close(4);
            /* Using cursor T000G8 */
            pr_default.execute(6, new Object[] {n1016Contrato_PrepostoPesCod, A1016Contrato_PrepostoPesCod});
            A1015Contrato_PrepostoNom = T000G8_A1015Contrato_PrepostoNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1015Contrato_PrepostoNom", A1015Contrato_PrepostoNom);
            n1015Contrato_PrepostoNom = T000G8_n1015Contrato_PrepostoNom[0];
            pr_default.close(6);
            AV27Pgmname = "Contrato";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Pgmname", AV27Pgmname);
            /* Using cursor T000G4 */
            pr_default.execute(2, new Object[] {A39Contratada_Codigo});
            A438Contratada_Sigla = T000G4_A438Contratada_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
            A516Contratada_TipoFabrica = T000G4_A516Contratada_TipoFabrica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
            A40Contratada_PessoaCod = T000G4_A40Contratada_PessoaCod[0];
            pr_default.close(2);
            if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
            {
               lblTextblockcontrato_prepostocod_Caption = "Preposto";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_prepostocod_Internalname, "Caption", lblTextblockcontrato_prepostocod_Caption);
            }
            else
            {
               if ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 )
               {
                  lblTextblockcontrato_prepostocod_Caption = "Respons�vel";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_prepostocod_Internalname, "Caption", lblTextblockcontrato_prepostocod_Caption);
               }
            }
            /* Using cursor T000G7 */
            pr_default.execute(5, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = T000G7_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T000G7_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T000G7_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T000G7_n42Contratada_PessoaCNPJ[0];
            pr_default.close(5);
            /* Using cursor T000G5 */
            pr_default.execute(3, new Object[] {A75Contrato_AreaTrabalhoCod});
            A76Contrato_AreaTrabalhoDes = T000G5_A76Contrato_AreaTrabalhoDes[0];
            n76Contrato_AreaTrabalhoDes = T000G5_n76Contrato_AreaTrabalhoDes[0];
            pr_default.close(3);
         }
      }

      protected void Load0G17( )
      {
         /* Using cursor T000G19 */
         pr_default.execute(9, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound17 = 1;
            A76Contrato_AreaTrabalhoDes = T000G19_A76Contrato_AreaTrabalhoDes[0];
            n76Contrato_AreaTrabalhoDes = T000G19_n76Contrato_AreaTrabalhoDes[0];
            A41Contratada_PessoaNom = T000G19_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T000G19_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T000G19_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T000G19_n42Contratada_PessoaCNPJ[0];
            A438Contratada_Sigla = T000G19_A438Contratada_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
            A516Contratada_TipoFabrica = T000G19_A516Contratada_TipoFabrica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
            A77Contrato_Numero = T000G19_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T000G19_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T000G19_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T000G19_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A80Contrato_Objeto = T000G19_A80Contrato_Objeto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A80Contrato_Objeto", A80Contrato_Objeto);
            A115Contrato_UnidadeContratacao = T000G19_A115Contrato_UnidadeContratacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
            A81Contrato_Quantidade = T000G19_A81Contrato_Quantidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A81Contrato_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A81Contrato_Quantidade), 9, 0)));
            A82Contrato_DataVigenciaInicio = T000G19_A82Contrato_DataVigenciaInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
            A83Contrato_DataVigenciaTermino = T000G19_A83Contrato_DataVigenciaTermino[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
            A84Contrato_DataPublicacaoDOU = T000G19_A84Contrato_DataPublicacaoDOU[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A84Contrato_DataPublicacaoDOU", context.localUtil.Format(A84Contrato_DataPublicacaoDOU, "99/99/99"));
            A85Contrato_DataAssinatura = T000G19_A85Contrato_DataAssinatura[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A85Contrato_DataAssinatura", context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"));
            A86Contrato_DataPedidoReajuste = T000G19_A86Contrato_DataPedidoReajuste[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A86Contrato_DataPedidoReajuste", context.localUtil.Format(A86Contrato_DataPedidoReajuste, "99/99/99"));
            A87Contrato_DataTerminoAta = T000G19_A87Contrato_DataTerminoAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A87Contrato_DataTerminoAta", context.localUtil.Format(A87Contrato_DataTerminoAta, "99/99/99"));
            A88Contrato_DataFimAdaptacao = T000G19_A88Contrato_DataFimAdaptacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A88Contrato_DataFimAdaptacao", context.localUtil.Format(A88Contrato_DataFimAdaptacao, "99/99/99"));
            A89Contrato_Valor = T000G19_A89Contrato_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A89Contrato_Valor", StringUtil.LTrim( StringUtil.Str( A89Contrato_Valor, 18, 5)));
            A116Contrato_ValorUnidadeContratacao = T000G19_A116Contrato_ValorUnidadeContratacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
            A90Contrato_RegrasPagto = T000G19_A90Contrato_RegrasPagto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A90Contrato_RegrasPagto", A90Contrato_RegrasPagto);
            A91Contrato_DiasPagto = T000G19_A91Contrato_DiasPagto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A91Contrato_DiasPagto", StringUtil.LTrim( StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0)));
            A452Contrato_CalculoDivergencia = T000G19_A452Contrato_CalculoDivergencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
            A453Contrato_IndiceDivergencia = T000G19_A453Contrato_IndiceDivergencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
            A1150Contrato_AceitaPFFS = T000G19_A1150Contrato_AceitaPFFS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1150Contrato_AceitaPFFS", A1150Contrato_AceitaPFFS);
            n1150Contrato_AceitaPFFS = T000G19_n1150Contrato_AceitaPFFS[0];
            A92Contrato_Ativo = T000G19_A92Contrato_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92Contrato_Ativo", A92Contrato_Ativo);
            A1015Contrato_PrepostoNom = T000G19_A1015Contrato_PrepostoNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1015Contrato_PrepostoNom", A1015Contrato_PrepostoNom);
            n1015Contrato_PrepostoNom = T000G19_n1015Contrato_PrepostoNom[0];
            A1354Contrato_PrdFtrCada = T000G19_A1354Contrato_PrdFtrCada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1354Contrato_PrdFtrCada", A1354Contrato_PrdFtrCada);
            n1354Contrato_PrdFtrCada = T000G19_n1354Contrato_PrdFtrCada[0];
            A2086Contrato_LmtFtr = T000G19_A2086Contrato_LmtFtr[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2086Contrato_LmtFtr", StringUtil.LTrim( StringUtil.Str( A2086Contrato_LmtFtr, 14, 5)));
            n2086Contrato_LmtFtr = T000G19_n2086Contrato_LmtFtr[0];
            A1357Contrato_PrdFtrIni = T000G19_A1357Contrato_PrdFtrIni[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1357Contrato_PrdFtrIni", StringUtil.LTrim( StringUtil.Str( (decimal)(A1357Contrato_PrdFtrIni), 4, 0)));
            n1357Contrato_PrdFtrIni = T000G19_n1357Contrato_PrdFtrIni[0];
            A1358Contrato_PrdFtrFim = T000G19_A1358Contrato_PrdFtrFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1358Contrato_PrdFtrFim", StringUtil.LTrim( StringUtil.Str( (decimal)(A1358Contrato_PrdFtrFim), 4, 0)));
            n1358Contrato_PrdFtrFim = T000G19_n1358Contrato_PrdFtrFim[0];
            A39Contratada_Codigo = T000G19_A39Contratada_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            A75Contrato_AreaTrabalhoCod = T000G19_A75Contrato_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
            A1013Contrato_PrepostoCod = T000G19_A1013Contrato_PrepostoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
            n1013Contrato_PrepostoCod = T000G19_n1013Contrato_PrepostoCod[0];
            A40Contratada_PessoaCod = T000G19_A40Contratada_PessoaCod[0];
            A1016Contrato_PrepostoPesCod = T000G19_A1016Contrato_PrepostoPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1016Contrato_PrepostoPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0)));
            n1016Contrato_PrepostoPesCod = T000G19_n1016Contrato_PrepostoPesCod[0];
            A842Contrato_DataInicioTA = T000G19_A842Contrato_DataInicioTA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
            n842Contrato_DataInicioTA = T000G19_n842Contrato_DataInicioTA[0];
            A843Contrato_DataFimTA = T000G19_A843Contrato_DataFimTA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
            n843Contrato_DataFimTA = T000G19_n843Contrato_DataFimTA[0];
            ZM0G17( -43) ;
         }
         pr_default.close(9);
         OnLoadActions0G17( ) ;
      }

      protected void OnLoadActions0G17( )
      {
         AV27Pgmname = "Contrato";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Pgmname", AV27Pgmname);
         A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
         A1870Contrato_ValorUndCntAtual = ((GetContrato_ValorUndCntAtual0( A74Contrato_Codigo)>Convert.ToDecimal(0)) ? GetContrato_ValorUndCntAtual1( A74Contrato_Codigo) : A116Contrato_ValorUnidadeContratacao);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1870Contrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( A1870Contrato_ValorUndCntAtual, 18, 5)));
         if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
         {
            lblTextblockcontrato_prepostocod_Caption = "Preposto";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_prepostocod_Internalname, "Caption", lblTextblockcontrato_prepostocod_Caption);
         }
         else
         {
            if ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 )
            {
               lblTextblockcontrato_prepostocod_Caption = "Respons�vel";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_prepostocod_Internalname, "Caption", lblTextblockcontrato_prepostocod_Caption);
            }
         }
         A2096Contrato_Identificacao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2096Contrato_Identificacao", A2096Contrato_Identificacao);
         AV25UnidadeDeMedicao = A115Contrato_UnidadeContratacao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25UnidadeDeMedicao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0)));
      }

      protected void CheckExtendedTable0G17( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         AV27Pgmname = "Contrato";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Pgmname", AV27Pgmname);
         /* Using cursor T000G11 */
         pr_default.execute(7, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            A842Contrato_DataInicioTA = T000G11_A842Contrato_DataInicioTA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
            n842Contrato_DataInicioTA = T000G11_n842Contrato_DataInicioTA[0];
         }
         else
         {
            A842Contrato_DataInicioTA = DateTime.MinValue;
            n842Contrato_DataInicioTA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
         }
         pr_default.close(7);
         /* Using cursor T000G14 */
         pr_default.execute(8, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            A843Contrato_DataFimTA = T000G14_A843Contrato_DataFimTA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
            n843Contrato_DataFimTA = T000G14_n843Contrato_DataFimTA[0];
         }
         else
         {
            A843Contrato_DataFimTA = DateTime.MinValue;
            n843Contrato_DataFimTA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
         }
         pr_default.close(8);
         A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
         A1870Contrato_ValorUndCntAtual = ((GetContrato_ValorUndCntAtual0( A74Contrato_Codigo)>Convert.ToDecimal(0)) ? GetContrato_ValorUndCntAtual1( A74Contrato_Codigo) : A116Contrato_ValorUnidadeContratacao);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1870Contrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( A1870Contrato_ValorUndCntAtual, 18, 5)));
         /* Using cursor T000G5 */
         pr_default.execute(3, new Object[] {A75Contrato_AreaTrabalhoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contrato_Area Trabalho'.", "ForeignKeyNotFound", 1, "CONTRATO_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtContrato_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A76Contrato_AreaTrabalhoDes = T000G5_A76Contrato_AreaTrabalhoDes[0];
         n76Contrato_AreaTrabalhoDes = T000G5_n76Contrato_AreaTrabalhoDes[0];
         pr_default.close(3);
         /* Using cursor T000G4 */
         pr_default.execute(2, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "CONTRATADA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratada_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A438Contratada_Sigla = T000G4_A438Contratada_Sigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
         A516Contratada_TipoFabrica = T000G4_A516Contratada_TipoFabrica[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
         A40Contratada_PessoaCod = T000G4_A40Contratada_PessoaCod[0];
         pr_default.close(2);
         if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
         {
            lblTextblockcontrato_prepostocod_Caption = "Preposto";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_prepostocod_Internalname, "Caption", lblTextblockcontrato_prepostocod_Caption);
         }
         else
         {
            if ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 )
            {
               lblTextblockcontrato_prepostocod_Caption = "Respons�vel";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_prepostocod_Internalname, "Caption", lblTextblockcontrato_prepostocod_Caption);
            }
         }
         /* Using cursor T000G7 */
         pr_default.execute(5, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T000G7_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T000G7_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T000G7_A42Contratada_PessoaCNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         n42Contratada_PessoaCNPJ = T000G7_n42Contratada_PessoaCNPJ[0];
         pr_default.close(5);
         A2096Contrato_Identificacao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2096Contrato_Identificacao", A2096Contrato_Identificacao);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
         {
            GX_msglist.addItem("N�mero do Contrato � obrigat�rio.", 1, "CONTRATO_NUMERO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Numero_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==A79Contrato_Ano) )
         {
            GX_msglist.addItem("Ano � obrigat�rio.", 1, "CONTRATO_ANO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Ano_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A80Contrato_Objeto)) )
         {
            GX_msglist.addItem("Objeto do Contrato � obrigat�rio.", 1, "CONTRATO_OBJETO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Objeto_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A115Contrato_UnidadeContratacao == 1 ) || ( A115Contrato_UnidadeContratacao == 2 ) || ( A115Contrato_UnidadeContratacao == 3 ) ) )
         {
            GX_msglist.addItem("Campo Unidade fora do intervalo", "OutOfRange", 1, "CONTRATO_UNIDADECONTRATACAO");
            AnyError = 1;
            GX_FocusControl = cmbContrato_UnidadeContratacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         AV25UnidadeDeMedicao = A115Contrato_UnidadeContratacao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25UnidadeDeMedicao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0)));
         if ( ! ( (DateTime.MinValue==A82Contrato_DataVigenciaInicio) || ( A82Contrato_DataVigenciaInicio >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Vig�ncia Inicio fora do intervalo", "OutOfRange", 1, "CONTRATO_DATAVIGENCIAINICIO");
            AnyError = 1;
            GX_FocusControl = edtContrato_DataVigenciaInicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A83Contrato_DataVigenciaTermino) || ( A83Contrato_DataVigenciaTermino >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Vig�ncia T�rmino fora do intervalo", "OutOfRange", 1, "CONTRATO_DATAVIGENCIATERMINO");
            AnyError = 1;
            GX_FocusControl = edtContrato_DataVigenciaTermino_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A84Contrato_DataPublicacaoDOU) || ( A84Contrato_DataPublicacaoDOU >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Publica��o DOU fora do intervalo", "OutOfRange", 1, "CONTRATO_DATAPUBLICACAODOU");
            AnyError = 1;
            GX_FocusControl = edtContrato_DataPublicacaoDOU_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A85Contrato_DataAssinatura) || ( A85Contrato_DataAssinatura >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Assinatura fora do intervalo", "OutOfRange", 1, "CONTRATO_DATAASSINATURA");
            AnyError = 1;
            GX_FocusControl = edtContrato_DataAssinatura_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A86Contrato_DataPedidoReajuste) || ( A86Contrato_DataPedidoReajuste >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Pedido Reajuste fora do intervalo", "OutOfRange", 1, "CONTRATO_DATAPEDIDOREAJUSTE");
            AnyError = 1;
            GX_FocusControl = edtContrato_DataPedidoReajuste_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A87Contrato_DataTerminoAta) || ( A87Contrato_DataTerminoAta >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo T�rmino da Ata fora do intervalo", "OutOfRange", 1, "CONTRATO_DATATERMINOATA");
            AnyError = 1;
            GX_FocusControl = edtContrato_DataTerminoAta_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A88Contrato_DataFimAdaptacao) || ( A88Contrato_DataFimAdaptacao >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Fim Adapta��o fora do intervalo", "OutOfRange", 1, "CONTRATO_DATAFIMADAPTACAO");
            AnyError = 1;
            GX_FocusControl = edtContrato_DataFimAdaptacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==A91Contrato_DiasPagto) )
         {
            GX_msglist.addItem("Dias para Pagamento � obrigat�rio.", 1, "CONTRATO_DIASPAGTO");
            AnyError = 1;
            GX_FocusControl = edtContrato_DiasPagto_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000G6 */
         pr_default.execute(4, new Object[] {n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A1013Contrato_PrepostoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATO_PREPOSTOCOD");
               AnyError = 1;
               GX_FocusControl = dynContrato_PrepostoCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A1016Contrato_PrepostoPesCod = T000G6_A1016Contrato_PrepostoPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1016Contrato_PrepostoPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0)));
         n1016Contrato_PrepostoPesCod = T000G6_n1016Contrato_PrepostoPesCod[0];
         pr_default.close(4);
         /* Using cursor T000G8 */
         pr_default.execute(6, new Object[] {n1016Contrato_PrepostoPesCod, A1016Contrato_PrepostoPesCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A1016Contrato_PrepostoPesCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1015Contrato_PrepostoNom = T000G8_A1015Contrato_PrepostoNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1015Contrato_PrepostoNom", A1015Contrato_PrepostoNom);
         n1015Contrato_PrepostoNom = T000G8_n1015Contrato_PrepostoNom[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors0G17( )
      {
         pr_default.close(7);
         pr_default.close(8);
         pr_default.close(3);
         pr_default.close(2);
         pr_default.close(5);
         pr_default.close(4);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_49( int A74Contrato_Codigo )
      {
         /* Using cursor T000G22 */
         pr_default.execute(10, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            A842Contrato_DataInicioTA = T000G22_A842Contrato_DataInicioTA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
            n842Contrato_DataInicioTA = T000G22_n842Contrato_DataInicioTA[0];
         }
         else
         {
            A842Contrato_DataInicioTA = DateTime.MinValue;
            n842Contrato_DataInicioTA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_50( int A74Contrato_Codigo )
      {
         /* Using cursor T000G25 */
         pr_default.execute(11, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            A843Contrato_DataFimTA = T000G25_A843Contrato_DataFimTA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
            n843Contrato_DataFimTA = T000G25_n843Contrato_DataFimTA[0];
         }
         else
         {
            A843Contrato_DataFimTA = DateTime.MinValue;
            n843Contrato_DataFimTA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_45( int A75Contrato_AreaTrabalhoCod )
      {
         /* Using cursor T000G26 */
         pr_default.execute(12, new Object[] {A75Contrato_AreaTrabalhoCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contrato_Area Trabalho'.", "ForeignKeyNotFound", 1, "CONTRATO_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtContrato_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A76Contrato_AreaTrabalhoDes = T000G26_A76Contrato_AreaTrabalhoDes[0];
         n76Contrato_AreaTrabalhoDes = T000G26_n76Contrato_AreaTrabalhoDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A76Contrato_AreaTrabalhoDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void gxLoad_44( int A39Contratada_Codigo )
      {
         /* Using cursor T000G27 */
         pr_default.execute(13, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "CONTRATADA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratada_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A438Contratada_Sigla = T000G27_A438Contratada_Sigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
         A516Contratada_TipoFabrica = T000G27_A516Contratada_TipoFabrica[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
         A40Contratada_PessoaCod = T000G27_A40Contratada_PessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A438Contratada_Sigla))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A516Contratada_TipoFabrica))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(13) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(13);
      }

      protected void gxLoad_47( int A40Contratada_PessoaCod )
      {
         /* Using cursor T000G28 */
         pr_default.execute(14, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T000G28_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T000G28_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T000G28_A42Contratada_PessoaCNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         n42Contratada_PessoaCNPJ = T000G28_n42Contratada_PessoaCNPJ[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A41Contratada_PessoaNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( A42Contratada_PessoaCNPJ)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(14);
      }

      protected void gxLoad_46( int A1013Contrato_PrepostoCod )
      {
         /* Using cursor T000G29 */
         pr_default.execute(15, new Object[] {n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (0==A1013Contrato_PrepostoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATO_PREPOSTOCOD");
               AnyError = 1;
               GX_FocusControl = dynContrato_PrepostoCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A1016Contrato_PrepostoPesCod = T000G29_A1016Contrato_PrepostoPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1016Contrato_PrepostoPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0)));
         n1016Contrato_PrepostoPesCod = T000G29_n1016Contrato_PrepostoPesCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      protected void gxLoad_48( int A1016Contrato_PrepostoPesCod )
      {
         /* Using cursor T000G30 */
         pr_default.execute(16, new Object[] {n1016Contrato_PrepostoPesCod, A1016Contrato_PrepostoPesCod});
         if ( (pr_default.getStatus(16) == 101) )
         {
            if ( ! ( (0==A1016Contrato_PrepostoPesCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1015Contrato_PrepostoNom = T000G30_A1015Contrato_PrepostoNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1015Contrato_PrepostoNom", A1015Contrato_PrepostoNom);
         n1015Contrato_PrepostoNom = T000G30_n1015Contrato_PrepostoNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1015Contrato_PrepostoNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(16) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(16);
      }

      protected void GetKey0G17( )
      {
         /* Using cursor T000G31 */
         pr_default.execute(17, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound17 = 1;
         }
         else
         {
            RcdFound17 = 0;
         }
         pr_default.close(17);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000G3 */
         pr_default.execute(1, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0G17( 43) ;
            RcdFound17 = 1;
            A74Contrato_Codigo = T000G3_A74Contrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            n74Contrato_Codigo = T000G3_n74Contrato_Codigo[0];
            A77Contrato_Numero = T000G3_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T000G3_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T000G3_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T000G3_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A80Contrato_Objeto = T000G3_A80Contrato_Objeto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A80Contrato_Objeto", A80Contrato_Objeto);
            A115Contrato_UnidadeContratacao = T000G3_A115Contrato_UnidadeContratacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
            A81Contrato_Quantidade = T000G3_A81Contrato_Quantidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A81Contrato_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A81Contrato_Quantidade), 9, 0)));
            A82Contrato_DataVigenciaInicio = T000G3_A82Contrato_DataVigenciaInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
            A83Contrato_DataVigenciaTermino = T000G3_A83Contrato_DataVigenciaTermino[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
            A84Contrato_DataPublicacaoDOU = T000G3_A84Contrato_DataPublicacaoDOU[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A84Contrato_DataPublicacaoDOU", context.localUtil.Format(A84Contrato_DataPublicacaoDOU, "99/99/99"));
            A85Contrato_DataAssinatura = T000G3_A85Contrato_DataAssinatura[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A85Contrato_DataAssinatura", context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"));
            A86Contrato_DataPedidoReajuste = T000G3_A86Contrato_DataPedidoReajuste[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A86Contrato_DataPedidoReajuste", context.localUtil.Format(A86Contrato_DataPedidoReajuste, "99/99/99"));
            A87Contrato_DataTerminoAta = T000G3_A87Contrato_DataTerminoAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A87Contrato_DataTerminoAta", context.localUtil.Format(A87Contrato_DataTerminoAta, "99/99/99"));
            A88Contrato_DataFimAdaptacao = T000G3_A88Contrato_DataFimAdaptacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A88Contrato_DataFimAdaptacao", context.localUtil.Format(A88Contrato_DataFimAdaptacao, "99/99/99"));
            A89Contrato_Valor = T000G3_A89Contrato_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A89Contrato_Valor", StringUtil.LTrim( StringUtil.Str( A89Contrato_Valor, 18, 5)));
            A116Contrato_ValorUnidadeContratacao = T000G3_A116Contrato_ValorUnidadeContratacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
            A90Contrato_RegrasPagto = T000G3_A90Contrato_RegrasPagto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A90Contrato_RegrasPagto", A90Contrato_RegrasPagto);
            A91Contrato_DiasPagto = T000G3_A91Contrato_DiasPagto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A91Contrato_DiasPagto", StringUtil.LTrim( StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0)));
            A452Contrato_CalculoDivergencia = T000G3_A452Contrato_CalculoDivergencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
            A453Contrato_IndiceDivergencia = T000G3_A453Contrato_IndiceDivergencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
            A1150Contrato_AceitaPFFS = T000G3_A1150Contrato_AceitaPFFS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1150Contrato_AceitaPFFS", A1150Contrato_AceitaPFFS);
            n1150Contrato_AceitaPFFS = T000G3_n1150Contrato_AceitaPFFS[0];
            A92Contrato_Ativo = T000G3_A92Contrato_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92Contrato_Ativo", A92Contrato_Ativo);
            A1354Contrato_PrdFtrCada = T000G3_A1354Contrato_PrdFtrCada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1354Contrato_PrdFtrCada", A1354Contrato_PrdFtrCada);
            n1354Contrato_PrdFtrCada = T000G3_n1354Contrato_PrdFtrCada[0];
            A2086Contrato_LmtFtr = T000G3_A2086Contrato_LmtFtr[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2086Contrato_LmtFtr", StringUtil.LTrim( StringUtil.Str( A2086Contrato_LmtFtr, 14, 5)));
            n2086Contrato_LmtFtr = T000G3_n2086Contrato_LmtFtr[0];
            A1357Contrato_PrdFtrIni = T000G3_A1357Contrato_PrdFtrIni[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1357Contrato_PrdFtrIni", StringUtil.LTrim( StringUtil.Str( (decimal)(A1357Contrato_PrdFtrIni), 4, 0)));
            n1357Contrato_PrdFtrIni = T000G3_n1357Contrato_PrdFtrIni[0];
            A1358Contrato_PrdFtrFim = T000G3_A1358Contrato_PrdFtrFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1358Contrato_PrdFtrFim", StringUtil.LTrim( StringUtil.Str( (decimal)(A1358Contrato_PrdFtrFim), 4, 0)));
            n1358Contrato_PrdFtrFim = T000G3_n1358Contrato_PrdFtrFim[0];
            A39Contratada_Codigo = T000G3_A39Contratada_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            A75Contrato_AreaTrabalhoCod = T000G3_A75Contrato_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
            A1013Contrato_PrepostoCod = T000G3_A1013Contrato_PrepostoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
            n1013Contrato_PrepostoCod = T000G3_n1013Contrato_PrepostoCod[0];
            O1013Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
            n1013Contrato_PrepostoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
            O116Contrato_ValorUnidadeContratacao = A116Contrato_ValorUnidadeContratacao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
            Z74Contrato_Codigo = A74Contrato_Codigo;
            sMode17 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0G17( ) ;
            if ( AnyError == 1 )
            {
               RcdFound17 = 0;
               InitializeNonKey0G17( ) ;
            }
            Gx_mode = sMode17;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound17 = 0;
            InitializeNonKey0G17( ) ;
            sMode17 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode17;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0G17( ) ;
         if ( RcdFound17 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound17 = 0;
         /* Using cursor T000G32 */
         pr_default.execute(18, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(18) != 101) )
         {
            while ( (pr_default.getStatus(18) != 101) && ( ( T000G32_A74Contrato_Codigo[0] < A74Contrato_Codigo ) ) )
            {
               pr_default.readNext(18);
            }
            if ( (pr_default.getStatus(18) != 101) && ( ( T000G32_A74Contrato_Codigo[0] > A74Contrato_Codigo ) ) )
            {
               A74Contrato_Codigo = T000G32_A74Contrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               n74Contrato_Codigo = T000G32_n74Contrato_Codigo[0];
               RcdFound17 = 1;
            }
         }
         pr_default.close(18);
      }

      protected void move_previous( )
      {
         RcdFound17 = 0;
         /* Using cursor T000G33 */
         pr_default.execute(19, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(19) != 101) )
         {
            while ( (pr_default.getStatus(19) != 101) && ( ( T000G33_A74Contrato_Codigo[0] > A74Contrato_Codigo ) ) )
            {
               pr_default.readNext(19);
            }
            if ( (pr_default.getStatus(19) != 101) && ( ( T000G33_A74Contrato_Codigo[0] < A74Contrato_Codigo ) ) )
            {
               A74Contrato_Codigo = T000G33_A74Contrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               n74Contrato_Codigo = T000G33_n74Contrato_Codigo[0];
               RcdFound17 = 1;
            }
         }
         pr_default.close(19);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0G17( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynContrato_PrepostoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0G17( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound17 == 1 )
            {
               if ( A74Contrato_Codigo != Z74Contrato_Codigo )
               {
                  A74Contrato_Codigo = Z74Contrato_Codigo;
                  n74Contrato_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContrato_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynContrato_PrepostoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0G17( ) ;
                  GX_FocusControl = dynContrato_PrepostoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A74Contrato_Codigo != Z74Contrato_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = dynContrato_PrepostoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0G17( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContrato_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynContrato_PrepostoCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0G17( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A74Contrato_Codigo != Z74Contrato_Codigo )
         {
            A74Contrato_Codigo = Z74Contrato_Codigo;
            n74Contrato_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynContrato_PrepostoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0G17( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000G2 */
            pr_default.execute(0, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contrato"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z77Contrato_Numero, T000G2_A77Contrato_Numero[0]) != 0 ) || ( StringUtil.StrCmp(Z78Contrato_NumeroAta, T000G2_A78Contrato_NumeroAta[0]) != 0 ) || ( Z79Contrato_Ano != T000G2_A79Contrato_Ano[0] ) || ( Z115Contrato_UnidadeContratacao != T000G2_A115Contrato_UnidadeContratacao[0] ) || ( Z81Contrato_Quantidade != T000G2_A81Contrato_Quantidade[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z82Contrato_DataVigenciaInicio != T000G2_A82Contrato_DataVigenciaInicio[0] ) || ( Z83Contrato_DataVigenciaTermino != T000G2_A83Contrato_DataVigenciaTermino[0] ) || ( Z84Contrato_DataPublicacaoDOU != T000G2_A84Contrato_DataPublicacaoDOU[0] ) || ( Z85Contrato_DataAssinatura != T000G2_A85Contrato_DataAssinatura[0] ) || ( Z86Contrato_DataPedidoReajuste != T000G2_A86Contrato_DataPedidoReajuste[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z87Contrato_DataTerminoAta != T000G2_A87Contrato_DataTerminoAta[0] ) || ( Z88Contrato_DataFimAdaptacao != T000G2_A88Contrato_DataFimAdaptacao[0] ) || ( Z89Contrato_Valor != T000G2_A89Contrato_Valor[0] ) || ( Z116Contrato_ValorUnidadeContratacao != T000G2_A116Contrato_ValorUnidadeContratacao[0] ) || ( Z91Contrato_DiasPagto != T000G2_A91Contrato_DiasPagto[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z452Contrato_CalculoDivergencia, T000G2_A452Contrato_CalculoDivergencia[0]) != 0 ) || ( Z453Contrato_IndiceDivergencia != T000G2_A453Contrato_IndiceDivergencia[0] ) || ( Z1150Contrato_AceitaPFFS != T000G2_A1150Contrato_AceitaPFFS[0] ) || ( Z92Contrato_Ativo != T000G2_A92Contrato_Ativo[0] ) || ( StringUtil.StrCmp(Z1354Contrato_PrdFtrCada, T000G2_A1354Contrato_PrdFtrCada[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2086Contrato_LmtFtr != T000G2_A2086Contrato_LmtFtr[0] ) || ( Z1357Contrato_PrdFtrIni != T000G2_A1357Contrato_PrdFtrIni[0] ) || ( Z1358Contrato_PrdFtrFim != T000G2_A1358Contrato_PrdFtrFim[0] ) || ( Z39Contratada_Codigo != T000G2_A39Contratada_Codigo[0] ) || ( Z75Contrato_AreaTrabalhoCod != T000G2_A75Contrato_AreaTrabalhoCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1013Contrato_PrepostoCod != T000G2_A1013Contrato_PrepostoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z77Contrato_Numero, T000G2_A77Contrato_Numero[0]) != 0 )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_Numero");
                  GXUtil.WriteLogRaw("Old: ",Z77Contrato_Numero);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A77Contrato_Numero[0]);
               }
               if ( StringUtil.StrCmp(Z78Contrato_NumeroAta, T000G2_A78Contrato_NumeroAta[0]) != 0 )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_NumeroAta");
                  GXUtil.WriteLogRaw("Old: ",Z78Contrato_NumeroAta);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A78Contrato_NumeroAta[0]);
               }
               if ( Z79Contrato_Ano != T000G2_A79Contrato_Ano[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_Ano");
                  GXUtil.WriteLogRaw("Old: ",Z79Contrato_Ano);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A79Contrato_Ano[0]);
               }
               if ( Z115Contrato_UnidadeContratacao != T000G2_A115Contrato_UnidadeContratacao[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_UnidadeContratacao");
                  GXUtil.WriteLogRaw("Old: ",Z115Contrato_UnidadeContratacao);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A115Contrato_UnidadeContratacao[0]);
               }
               if ( Z81Contrato_Quantidade != T000G2_A81Contrato_Quantidade[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_Quantidade");
                  GXUtil.WriteLogRaw("Old: ",Z81Contrato_Quantidade);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A81Contrato_Quantidade[0]);
               }
               if ( Z82Contrato_DataVigenciaInicio != T000G2_A82Contrato_DataVigenciaInicio[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_DataVigenciaInicio");
                  GXUtil.WriteLogRaw("Old: ",Z82Contrato_DataVigenciaInicio);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A82Contrato_DataVigenciaInicio[0]);
               }
               if ( Z83Contrato_DataVigenciaTermino != T000G2_A83Contrato_DataVigenciaTermino[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_DataVigenciaTermino");
                  GXUtil.WriteLogRaw("Old: ",Z83Contrato_DataVigenciaTermino);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A83Contrato_DataVigenciaTermino[0]);
               }
               if ( Z84Contrato_DataPublicacaoDOU != T000G2_A84Contrato_DataPublicacaoDOU[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_DataPublicacaoDOU");
                  GXUtil.WriteLogRaw("Old: ",Z84Contrato_DataPublicacaoDOU);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A84Contrato_DataPublicacaoDOU[0]);
               }
               if ( Z85Contrato_DataAssinatura != T000G2_A85Contrato_DataAssinatura[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_DataAssinatura");
                  GXUtil.WriteLogRaw("Old: ",Z85Contrato_DataAssinatura);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A85Contrato_DataAssinatura[0]);
               }
               if ( Z86Contrato_DataPedidoReajuste != T000G2_A86Contrato_DataPedidoReajuste[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_DataPedidoReajuste");
                  GXUtil.WriteLogRaw("Old: ",Z86Contrato_DataPedidoReajuste);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A86Contrato_DataPedidoReajuste[0]);
               }
               if ( Z87Contrato_DataTerminoAta != T000G2_A87Contrato_DataTerminoAta[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_DataTerminoAta");
                  GXUtil.WriteLogRaw("Old: ",Z87Contrato_DataTerminoAta);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A87Contrato_DataTerminoAta[0]);
               }
               if ( Z88Contrato_DataFimAdaptacao != T000G2_A88Contrato_DataFimAdaptacao[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_DataFimAdaptacao");
                  GXUtil.WriteLogRaw("Old: ",Z88Contrato_DataFimAdaptacao);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A88Contrato_DataFimAdaptacao[0]);
               }
               if ( Z89Contrato_Valor != T000G2_A89Contrato_Valor[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_Valor");
                  GXUtil.WriteLogRaw("Old: ",Z89Contrato_Valor);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A89Contrato_Valor[0]);
               }
               if ( Z116Contrato_ValorUnidadeContratacao != T000G2_A116Contrato_ValorUnidadeContratacao[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_ValorUnidadeContratacao");
                  GXUtil.WriteLogRaw("Old: ",Z116Contrato_ValorUnidadeContratacao);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A116Contrato_ValorUnidadeContratacao[0]);
               }
               if ( Z91Contrato_DiasPagto != T000G2_A91Contrato_DiasPagto[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_DiasPagto");
                  GXUtil.WriteLogRaw("Old: ",Z91Contrato_DiasPagto);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A91Contrato_DiasPagto[0]);
               }
               if ( StringUtil.StrCmp(Z452Contrato_CalculoDivergencia, T000G2_A452Contrato_CalculoDivergencia[0]) != 0 )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_CalculoDivergencia");
                  GXUtil.WriteLogRaw("Old: ",Z452Contrato_CalculoDivergencia);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A452Contrato_CalculoDivergencia[0]);
               }
               if ( Z453Contrato_IndiceDivergencia != T000G2_A453Contrato_IndiceDivergencia[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_IndiceDivergencia");
                  GXUtil.WriteLogRaw("Old: ",Z453Contrato_IndiceDivergencia);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A453Contrato_IndiceDivergencia[0]);
               }
               if ( Z1150Contrato_AceitaPFFS != T000G2_A1150Contrato_AceitaPFFS[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_AceitaPFFS");
                  GXUtil.WriteLogRaw("Old: ",Z1150Contrato_AceitaPFFS);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A1150Contrato_AceitaPFFS[0]);
               }
               if ( Z92Contrato_Ativo != T000G2_A92Contrato_Ativo[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z92Contrato_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A92Contrato_Ativo[0]);
               }
               if ( StringUtil.StrCmp(Z1354Contrato_PrdFtrCada, T000G2_A1354Contrato_PrdFtrCada[0]) != 0 )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_PrdFtrCada");
                  GXUtil.WriteLogRaw("Old: ",Z1354Contrato_PrdFtrCada);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A1354Contrato_PrdFtrCada[0]);
               }
               if ( Z2086Contrato_LmtFtr != T000G2_A2086Contrato_LmtFtr[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_LmtFtr");
                  GXUtil.WriteLogRaw("Old: ",Z2086Contrato_LmtFtr);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A2086Contrato_LmtFtr[0]);
               }
               if ( Z1357Contrato_PrdFtrIni != T000G2_A1357Contrato_PrdFtrIni[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_PrdFtrIni");
                  GXUtil.WriteLogRaw("Old: ",Z1357Contrato_PrdFtrIni);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A1357Contrato_PrdFtrIni[0]);
               }
               if ( Z1358Contrato_PrdFtrFim != T000G2_A1358Contrato_PrdFtrFim[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_PrdFtrFim");
                  GXUtil.WriteLogRaw("Old: ",Z1358Contrato_PrdFtrFim);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A1358Contrato_PrdFtrFim[0]);
               }
               if ( Z39Contratada_Codigo != T000G2_A39Contratada_Codigo[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contratada_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z39Contratada_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A39Contratada_Codigo[0]);
               }
               if ( Z75Contrato_AreaTrabalhoCod != T000G2_A75Contrato_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z75Contrato_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A75Contrato_AreaTrabalhoCod[0]);
               }
               if ( Z1013Contrato_PrepostoCod != T000G2_A1013Contrato_PrepostoCod[0] )
               {
                  GXUtil.WriteLog("contrato:[seudo value changed for attri]"+"Contrato_PrepostoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1013Contrato_PrepostoCod);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A1013Contrato_PrepostoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Contrato"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0G17( )
      {
         BeforeValidate0G17( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0G17( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0G17( 0) ;
            CheckOptimisticConcurrency0G17( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0G17( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0G17( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000G34 */
                     pr_default.execute(20, new Object[] {A77Contrato_Numero, n78Contrato_NumeroAta, A78Contrato_NumeroAta, A79Contrato_Ano, A80Contrato_Objeto, A115Contrato_UnidadeContratacao, A81Contrato_Quantidade, A82Contrato_DataVigenciaInicio, A83Contrato_DataVigenciaTermino, A84Contrato_DataPublicacaoDOU, A85Contrato_DataAssinatura, A86Contrato_DataPedidoReajuste, A87Contrato_DataTerminoAta, A88Contrato_DataFimAdaptacao, A89Contrato_Valor, A116Contrato_ValorUnidadeContratacao, A90Contrato_RegrasPagto, A91Contrato_DiasPagto, A452Contrato_CalculoDivergencia, A453Contrato_IndiceDivergencia, n1150Contrato_AceitaPFFS, A1150Contrato_AceitaPFFS, A92Contrato_Ativo, n1354Contrato_PrdFtrCada, A1354Contrato_PrdFtrCada, n2086Contrato_LmtFtr, A2086Contrato_LmtFtr, n1357Contrato_PrdFtrIni, A1357Contrato_PrdFtrIni, n1358Contrato_PrdFtrFim, A1358Contrato_PrdFtrFim, A39Contratada_Codigo, A75Contrato_AreaTrabalhoCod, n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod});
                     A74Contrato_Codigo = T000G34_A74Contrato_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
                     n74Contrato_Codigo = T000G34_n74Contrato_Codigo[0];
                     pr_default.close(20);
                     dsDefault.SmartCacheProvider.SetUpdated("Contrato") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        new prc_inscontratogestor(context ).execute( ref  A74Contrato_Codigo) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
                        if ( ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) && ! T000G3_n115Contrato_UnidadeContratacao[0] )
                        {
                           new prc_saldocontratocriar(context ).execute(  A74Contrato_Codigo,  A115Contrato_UnidadeContratacao,  A82Contrato_DataVigenciaInicio,  A83Contrato_DataVigenciaTermino, out  AV22SaldoContrato_Codigo) ;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SaldoContrato_Codigo), 6, 0)));
                        }
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0G0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0G17( ) ;
            }
            EndLevel0G17( ) ;
         }
         CloseExtendedTableCursors0G17( ) ;
      }

      protected void Update0G17( )
      {
         BeforeValidate0G17( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0G17( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0G17( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0G17( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0G17( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000G35 */
                     pr_default.execute(21, new Object[] {A77Contrato_Numero, n78Contrato_NumeroAta, A78Contrato_NumeroAta, A79Contrato_Ano, A80Contrato_Objeto, A115Contrato_UnidadeContratacao, A81Contrato_Quantidade, A82Contrato_DataVigenciaInicio, A83Contrato_DataVigenciaTermino, A84Contrato_DataPublicacaoDOU, A85Contrato_DataAssinatura, A86Contrato_DataPedidoReajuste, A87Contrato_DataTerminoAta, A88Contrato_DataFimAdaptacao, A89Contrato_Valor, A116Contrato_ValorUnidadeContratacao, A90Contrato_RegrasPagto, A91Contrato_DiasPagto, A452Contrato_CalculoDivergencia, A453Contrato_IndiceDivergencia, n1150Contrato_AceitaPFFS, A1150Contrato_AceitaPFFS, A92Contrato_Ativo, n1354Contrato_PrdFtrCada, A1354Contrato_PrdFtrCada, n2086Contrato_LmtFtr, A2086Contrato_LmtFtr, n1357Contrato_PrdFtrIni, A1357Contrato_PrdFtrIni, n1358Contrato_PrdFtrFim, A1358Contrato_PrdFtrFim, A39Contratada_Codigo, A75Contrato_AreaTrabalhoCod, n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod, n74Contrato_Codigo, A74Contrato_Codigo});
                     pr_default.close(21);
                     dsDefault.SmartCacheProvider.SetUpdated("Contrato") ;
                     if ( (pr_default.getStatus(21) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contrato"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0G17( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        new prc_inscontratogestor(context ).execute( ref  A74Contrato_Codigo) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
                        if ( A1013Contrato_PrepostoCod != O1013Contrato_PrepostoCod )
                        {
                           new prc_prepostogestor(context ).execute( ref  A74Contrato_Codigo,  A1013Contrato_PrepostoCod,  O1013Contrato_PrepostoCod) ;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
                        }
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0G17( ) ;
         }
         CloseExtendedTableCursors0G17( ) ;
      }

      protected void DeferredUpdate0G17( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0G17( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0G17( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0G17( ) ;
            AfterConfirm0G17( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0G17( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000G36 */
                  pr_default.execute(22, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
                  pr_default.close(22);
                  dsDefault.SmartCacheProvider.SetUpdated("Contrato") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode17 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0G17( ) ;
         Gx_mode = sMode17;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0G17( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            AV27Pgmname = "Contrato";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Pgmname", AV27Pgmname);
            /* Using cursor T000G39 */
            pr_default.execute(23, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               A842Contrato_DataInicioTA = T000G39_A842Contrato_DataInicioTA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
               n842Contrato_DataInicioTA = T000G39_n842Contrato_DataInicioTA[0];
            }
            else
            {
               A842Contrato_DataInicioTA = DateTime.MinValue;
               n842Contrato_DataInicioTA = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
            }
            pr_default.close(23);
            /* Using cursor T000G42 */
            pr_default.execute(24, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               A843Contrato_DataFimTA = T000G42_A843Contrato_DataFimTA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
               n843Contrato_DataFimTA = T000G42_n843Contrato_DataFimTA[0];
            }
            else
            {
               A843Contrato_DataFimTA = DateTime.MinValue;
               n843Contrato_DataFimTA = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
            }
            pr_default.close(24);
            /* Using cursor T000G43 */
            pr_default.execute(25, new Object[] {A75Contrato_AreaTrabalhoCod});
            A76Contrato_AreaTrabalhoDes = T000G43_A76Contrato_AreaTrabalhoDes[0];
            n76Contrato_AreaTrabalhoDes = T000G43_n76Contrato_AreaTrabalhoDes[0];
            pr_default.close(25);
            /* Using cursor T000G44 */
            pr_default.execute(26, new Object[] {A39Contratada_Codigo});
            A438Contratada_Sigla = T000G44_A438Contratada_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
            A516Contratada_TipoFabrica = T000G44_A516Contratada_TipoFabrica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
            A40Contratada_PessoaCod = T000G44_A40Contratada_PessoaCod[0];
            pr_default.close(26);
            if ( ! ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) )
            {
               lblTextblockcontrato_prepostocod_Caption = "Preposto";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_prepostocod_Internalname, "Caption", lblTextblockcontrato_prepostocod_Caption);
            }
            else
            {
               if ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 )
               {
                  lblTextblockcontrato_prepostocod_Caption = "Respons�vel";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_prepostocod_Internalname, "Caption", lblTextblockcontrato_prepostocod_Caption);
               }
            }
            /* Using cursor T000G45 */
            pr_default.execute(27, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = T000G45_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T000G45_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T000G45_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T000G45_n42Contratada_PessoaCNPJ[0];
            pr_default.close(27);
            AV25UnidadeDeMedicao = A115Contrato_UnidadeContratacao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25UnidadeDeMedicao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0)));
            A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
            A1870Contrato_ValorUndCntAtual = ((GetContrato_ValorUndCntAtual0( A74Contrato_Codigo)>Convert.ToDecimal(0)) ? GetContrato_ValorUndCntAtual1( A74Contrato_Codigo) : A116Contrato_ValorUnidadeContratacao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1870Contrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( A1870Contrato_ValorUndCntAtual, 18, 5)));
            A2096Contrato_Identificacao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2096Contrato_Identificacao", A2096Contrato_Identificacao);
            /* Using cursor T000G46 */
            pr_default.execute(28, new Object[] {n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod});
            A1016Contrato_PrepostoPesCod = T000G46_A1016Contrato_PrepostoPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1016Contrato_PrepostoPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0)));
            n1016Contrato_PrepostoPesCod = T000G46_n1016Contrato_PrepostoPesCod[0];
            pr_default.close(28);
            /* Using cursor T000G47 */
            pr_default.execute(29, new Object[] {n1016Contrato_PrepostoPesCod, A1016Contrato_PrepostoPesCod});
            A1015Contrato_PrepostoNom = T000G47_A1015Contrato_PrepostoNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1015Contrato_PrepostoNom", A1015Contrato_PrepostoNom);
            n1015Contrato_PrepostoNom = T000G47_n1015Contrato_PrepostoNom[0];
            pr_default.close(29);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000G48 */
            pr_default.execute(30, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(30) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Auxiliar"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(30);
            /* Using cursor T000G49 */
            pr_default.execute(31, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas atendidos pelo Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(31);
            /* Using cursor T000G50 */
            pr_default.execute(32, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Unidades Contratadas"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(32);
            /* Using cursor T000G51 */
            pr_default.execute(33, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gestor do Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
            /* Using cursor T000G52 */
            pr_default.execute(34, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T197"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
            /* Using cursor T000G53 */
            pr_default.execute(35, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T179"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
            /* Using cursor T000G54 */
            pr_default.execute(36, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(36) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Obrigacao"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(36);
            /* Using cursor T000G55 */
            pr_default.execute(37, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(37) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Termo Aditivo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(37);
            /* Using cursor T000G56 */
            pr_default.execute(38, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(38) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Dados Certame"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(38);
            /* Using cursor T000G57 */
            pr_default.execute(39, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(39) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Ocorr�ncia"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(39);
            /* Using cursor T000G58 */
            pr_default.execute(40, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(40) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Servicos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(40);
            /* Using cursor T000G59 */
            pr_default.execute(41, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(41) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Clausulas"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(41);
            /* Using cursor T000G60 */
            pr_default.execute(42, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(42) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Arquivos Anexos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(42);
            /* Using cursor T000G61 */
            pr_default.execute(43, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(43) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Garantia"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(43);
            /* Using cursor T000G62 */
            pr_default.execute(44, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            if ( (pr_default.getStatus(44) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Configura��es da Caixa de Entrada"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(44);
         }
      }

      protected void EndLevel0G17( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0G17( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(26);
            pr_default.close(25);
            pr_default.close(28);
            pr_default.close(27);
            pr_default.close(29);
            pr_default.close(23);
            pr_default.close(24);
            context.CommitDataStores( "Contrato");
            if ( AnyError == 0 )
            {
               ConfirmValues0G0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(26);
            pr_default.close(25);
            pr_default.close(28);
            pr_default.close(27);
            pr_default.close(29);
            pr_default.close(23);
            pr_default.close(24);
            context.RollbackDataStores( "Contrato");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0G17( )
      {
         /* Scan By routine */
         /* Using cursor T000G63 */
         pr_default.execute(45);
         RcdFound17 = 0;
         if ( (pr_default.getStatus(45) != 101) )
         {
            RcdFound17 = 1;
            A74Contrato_Codigo = T000G63_A74Contrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            n74Contrato_Codigo = T000G63_n74Contrato_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0G17( )
      {
         /* Scan next routine */
         pr_default.readNext(45);
         RcdFound17 = 0;
         if ( (pr_default.getStatus(45) != 101) )
         {
            RcdFound17 = 1;
            A74Contrato_Codigo = T000G63_A74Contrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            n74Contrato_Codigo = T000G63_n74Contrato_Codigo[0];
         }
      }

      protected void ScanEnd0G17( )
      {
         pr_default.close(45);
      }

      protected void AfterConfirm0G17( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0G17( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0G17( )
      {
         /* Before Update Rules */
         new loadauditcontrato(context ).execute(  "Y", ref  AV24AuditingObject,  A74Contrato_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeDelete0G17( )
      {
         /* Before Delete Rules */
         new loadauditcontrato(context ).execute(  "Y", ref  AV24AuditingObject,  A74Contrato_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeComplete0G17( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontrato(context ).execute(  "N", ref  AV24AuditingObject,  A74Contrato_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontrato(context ).execute(  "N", ref  AV24AuditingObject,  A74Contrato_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void BeforeValidate0G17( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0G17( )
      {
         edtContratada_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaNom_Enabled), 5, 0)));
         edtContratada_PessoaCNPJ_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCNPJ_Enabled), 5, 0)));
         dynContrato_PrepostoCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContrato_PrepostoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContrato_PrepostoCod.Enabled), 5, 0)));
         edtContrato_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Numero_Enabled), 5, 0)));
         edtContrato_NumeroAta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_NumeroAta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_NumeroAta_Enabled), 5, 0)));
         edtContrato_Ano_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Ano_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Ano_Enabled), 5, 0)));
         edtContrato_Objeto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Objeto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Objeto_Enabled), 5, 0)));
         edtContrato_DataVigenciaInicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DataVigenciaInicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DataVigenciaInicio_Enabled), 5, 0)));
         edtContrato_DataVigenciaTermino_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DataVigenciaTermino_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DataVigenciaTermino_Enabled), 5, 0)));
         edtContrato_DataPublicacaoDOU_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DataPublicacaoDOU_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DataPublicacaoDOU_Enabled), 5, 0)));
         edtContrato_DataAssinatura_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DataAssinatura_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DataAssinatura_Enabled), 5, 0)));
         edtContrato_DataPedidoReajuste_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DataPedidoReajuste_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DataPedidoReajuste_Enabled), 5, 0)));
         edtContrato_DataTerminoAta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DataTerminoAta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DataTerminoAta_Enabled), 5, 0)));
         edtContrato_DataFimAdaptacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DataFimAdaptacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DataFimAdaptacao_Enabled), 5, 0)));
         edtContrato_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Valor_Enabled), 5, 0)));
         cmbContrato_UnidadeContratacao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContrato_UnidadeContratacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContrato_UnidadeContratacao.Enabled), 5, 0)));
         edtContrato_Quantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Quantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Quantidade_Enabled), 5, 0)));
         edtContrato_ValorUnidadeContratacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_ValorUnidadeContratacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_ValorUnidadeContratacao_Enabled), 5, 0)));
         cmbContrato_PrdFtrCada.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContrato_PrdFtrCada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContrato_PrdFtrCada.Enabled), 5, 0)));
         edtContrato_PrdFtrIni_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_PrdFtrIni_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_PrdFtrIni_Enabled), 5, 0)));
         edtContrato_PrdFtrFim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_PrdFtrFim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_PrdFtrFim_Enabled), 5, 0)));
         edtContrato_LmtFtr_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_LmtFtr_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_LmtFtr_Enabled), 5, 0)));
         cmbavUnidadedemedicao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUnidadedemedicao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUnidadedemedicao.Enabled), 5, 0)));
         edtContrato_DiasPagto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DiasPagto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DiasPagto_Enabled), 5, 0)));
         edtContrato_RegrasPagto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_RegrasPagto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_RegrasPagto_Enabled), 5, 0)));
         cmbContrato_CalculoDivergencia.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContrato_CalculoDivergencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContrato_CalculoDivergencia.Enabled), 5, 0)));
         edtContrato_IndiceDivergencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_IndiceDivergencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_IndiceDivergencia_Enabled), 5, 0)));
         chkContrato_AceitaPFFS.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContrato_AceitaPFFS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContrato_AceitaPFFS.Enabled), 5, 0)));
         chkContrato_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContrato_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContrato_Ativo.Enabled), 5, 0)));
         edtContratada_Sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Sigla_Enabled), 5, 0)));
         cmbContratada_TipoFabrica.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratada_TipoFabrica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratada_TipoFabrica.Enabled), 5, 0)));
         edtContrato_DataInicioTA_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DataInicioTA_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DataInicioTA_Enabled), 5, 0)));
         edtContrato_DataFimTA_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DataFimTA_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DataFimTA_Enabled), 5, 0)));
         edtContrato_DataTermino_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_DataTermino_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_DataTermino_Enabled), 5, 0)));
         edtContrato_ValorUndCntAtual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_ValorUndCntAtual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_ValorUndCntAtual_Enabled), 5, 0)));
         edtContrato_PrepostoPesCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_PrepostoPesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_PrepostoPesCod_Enabled), 5, 0)));
         edtContrato_PrepostoNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_PrepostoNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_PrepostoNom_Enabled), 5, 0)));
         edtContrato_Identificacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Identificacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Identificacao_Enabled), 5, 0)));
         edtContrato_AreaTrabalhoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_AreaTrabalhoCod_Enabled), 5, 0)));
         edtContrato_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         edtContratada_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0G0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202052118104557");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contrato.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Contrato_Codigo) + "," + UrlEncode("" +AV17Contratada_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z77Contrato_Numero", StringUtil.RTrim( Z77Contrato_Numero));
         GxWebStd.gx_hidden_field( context, "Z78Contrato_NumeroAta", StringUtil.RTrim( Z78Contrato_NumeroAta));
         GxWebStd.gx_hidden_field( context, "Z79Contrato_Ano", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z79Contrato_Ano), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z115Contrato_UnidadeContratacao), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z81Contrato_Quantidade", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z81Contrato_Quantidade), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z82Contrato_DataVigenciaInicio", context.localUtil.DToC( Z82Contrato_DataVigenciaInicio, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z83Contrato_DataVigenciaTermino", context.localUtil.DToC( Z83Contrato_DataVigenciaTermino, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z84Contrato_DataPublicacaoDOU", context.localUtil.DToC( Z84Contrato_DataPublicacaoDOU, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z85Contrato_DataAssinatura", context.localUtil.DToC( Z85Contrato_DataAssinatura, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z86Contrato_DataPedidoReajuste", context.localUtil.DToC( Z86Contrato_DataPedidoReajuste, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z87Contrato_DataTerminoAta", context.localUtil.DToC( Z87Contrato_DataTerminoAta, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z88Contrato_DataFimAdaptacao", context.localUtil.DToC( Z88Contrato_DataFimAdaptacao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z89Contrato_Valor", StringUtil.LTrim( StringUtil.NToC( Z89Contrato_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.NToC( Z116Contrato_ValorUnidadeContratacao, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z91Contrato_DiasPagto", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z91Contrato_DiasPagto), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z452Contrato_CalculoDivergencia", StringUtil.RTrim( Z452Contrato_CalculoDivergencia));
         GxWebStd.gx_hidden_field( context, "Z453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.NToC( Z453Contrato_IndiceDivergencia, 6, 2, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1150Contrato_AceitaPFFS", Z1150Contrato_AceitaPFFS);
         GxWebStd.gx_boolean_hidden_field( context, "Z92Contrato_Ativo", Z92Contrato_Ativo);
         GxWebStd.gx_hidden_field( context, "Z1354Contrato_PrdFtrCada", StringUtil.RTrim( Z1354Contrato_PrdFtrCada));
         GxWebStd.gx_hidden_field( context, "Z2086Contrato_LmtFtr", StringUtil.LTrim( StringUtil.NToC( Z2086Contrato_LmtFtr, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1357Contrato_PrdFtrIni", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1357Contrato_PrdFtrIni), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1358Contrato_PrdFtrFim", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1358Contrato_PrdFtrFim), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z39Contratada_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z75Contrato_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1013Contrato_PrepostoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(O1013Contrato_PrepostoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.NToC( O116Contrato_ValorUnidadeContratacao, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N39Contratada_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1013Contrato_PrepostoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contrato_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATO_PREPOSTOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Insert_Contrato_PrepostoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Contratada_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITINGOBJECT", AV24AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITINGOBJECT", AV24AuditingObject);
         }
         GxWebStd.gx_hidden_field( context, "vSALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22SaldoContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_AREATRABALHODES", A76Contrato_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV27Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Width", StringUtil.RTrim( Confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Closeable", StringUtil.BoolToStr( Confirmpanel_Closeable));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Icon", StringUtil.RTrim( Confirmpanel_Icon));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Modal", StringUtil.BoolToStr( Confirmpanel_Modal));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Enabled", StringUtil.BoolToStr( Confirmpanel_Enabled));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtext", StringUtil.RTrim( Confirmpanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonyestext", StringUtil.RTrim( Confirmpanel_Buttonyestext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonnotext", StringUtil.RTrim( Confirmpanel_Buttonnotext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttoncanceltext", StringUtil.RTrim( Confirmpanel_Buttoncanceltext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Contrato";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contrato:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contrato.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Contrato_Codigo) + "," + UrlEncode("" +AV17Contratada_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Contrato" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato" ;
      }

      protected void InitializeNonKey0G17( )
      {
         A39Contratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         A1013Contrato_PrepostoCod = 0;
         n1013Contrato_PrepostoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
         n1013Contrato_PrepostoCod = ((0==A1013Contrato_PrepostoCod) ? true : false);
         AV24AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV22SaldoContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SaldoContrato_Codigo), 6, 0)));
         AV25UnidadeDeMedicao = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25UnidadeDeMedicao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0)));
         A1870Contrato_ValorUndCntAtual = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1870Contrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( A1870Contrato_ValorUndCntAtual, 18, 5)));
         A1869Contrato_DataTermino = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
         A2096Contrato_Identificacao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2096Contrato_Identificacao", A2096Contrato_Identificacao);
         A76Contrato_AreaTrabalhoDes = "";
         n76Contrato_AreaTrabalhoDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A76Contrato_AreaTrabalhoDes", A76Contrato_AreaTrabalhoDes);
         A40Contratada_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
         A41Contratada_PessoaNom = "";
         n41Contratada_PessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         A42Contratada_PessoaCNPJ = "";
         n42Contratada_PessoaCNPJ = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         A438Contratada_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
         A516Contratada_TipoFabrica = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A516Contratada_TipoFabrica", A516Contratada_TipoFabrica);
         A77Contrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A78Contrato_NumeroAta = "";
         n78Contrato_NumeroAta = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         n78Contrato_NumeroAta = (String.IsNullOrEmpty(StringUtil.RTrim( A78Contrato_NumeroAta)) ? true : false);
         A79Contrato_Ano = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         A80Contrato_Objeto = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A80Contrato_Objeto", A80Contrato_Objeto);
         A115Contrato_UnidadeContratacao = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
         A81Contrato_Quantidade = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A81Contrato_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A81Contrato_Quantidade), 9, 0)));
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
         A842Contrato_DataInicioTA = DateTime.MinValue;
         n842Contrato_DataInicioTA = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
         A843Contrato_DataFimTA = DateTime.MinValue;
         n843Contrato_DataFimTA = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
         A84Contrato_DataPublicacaoDOU = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A84Contrato_DataPublicacaoDOU", context.localUtil.Format(A84Contrato_DataPublicacaoDOU, "99/99/99"));
         A85Contrato_DataAssinatura = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A85Contrato_DataAssinatura", context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"));
         A86Contrato_DataPedidoReajuste = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A86Contrato_DataPedidoReajuste", context.localUtil.Format(A86Contrato_DataPedidoReajuste, "99/99/99"));
         A87Contrato_DataTerminoAta = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A87Contrato_DataTerminoAta", context.localUtil.Format(A87Contrato_DataTerminoAta, "99/99/99"));
         A88Contrato_DataFimAdaptacao = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A88Contrato_DataFimAdaptacao", context.localUtil.Format(A88Contrato_DataFimAdaptacao, "99/99/99"));
         A89Contrato_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A89Contrato_Valor", StringUtil.LTrim( StringUtil.Str( A89Contrato_Valor, 18, 5)));
         A116Contrato_ValorUnidadeContratacao = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
         A90Contrato_RegrasPagto = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A90Contrato_RegrasPagto", A90Contrato_RegrasPagto);
         A1150Contrato_AceitaPFFS = false;
         n1150Contrato_AceitaPFFS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1150Contrato_AceitaPFFS", A1150Contrato_AceitaPFFS);
         n1150Contrato_AceitaPFFS = ((false==A1150Contrato_AceitaPFFS) ? true : false);
         A1016Contrato_PrepostoPesCod = 0;
         n1016Contrato_PrepostoPesCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1016Contrato_PrepostoPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0)));
         A1015Contrato_PrepostoNom = "";
         n1015Contrato_PrepostoNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1015Contrato_PrepostoNom", A1015Contrato_PrepostoNom);
         A1354Contrato_PrdFtrCada = "";
         n1354Contrato_PrdFtrCada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1354Contrato_PrdFtrCada", A1354Contrato_PrdFtrCada);
         n1354Contrato_PrdFtrCada = (String.IsNullOrEmpty(StringUtil.RTrim( A1354Contrato_PrdFtrCada)) ? true : false);
         A2086Contrato_LmtFtr = 0;
         n2086Contrato_LmtFtr = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2086Contrato_LmtFtr", StringUtil.LTrim( StringUtil.Str( A2086Contrato_LmtFtr, 14, 5)));
         n2086Contrato_LmtFtr = ((Convert.ToDecimal(0)==A2086Contrato_LmtFtr) ? true : false);
         A1357Contrato_PrdFtrIni = 0;
         n1357Contrato_PrdFtrIni = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1357Contrato_PrdFtrIni", StringUtil.LTrim( StringUtil.Str( (decimal)(A1357Contrato_PrdFtrIni), 4, 0)));
         n1357Contrato_PrdFtrIni = ((0==A1357Contrato_PrdFtrIni) ? true : false);
         A1358Contrato_PrdFtrFim = 0;
         n1358Contrato_PrdFtrFim = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1358Contrato_PrdFtrFim", StringUtil.LTrim( StringUtil.Str( (decimal)(A1358Contrato_PrdFtrFim), 4, 0)));
         n1358Contrato_PrdFtrFim = ((0==A1358Contrato_PrdFtrFim) ? true : false);
         A75Contrato_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
         A91Contrato_DiasPagto = 30;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A91Contrato_DiasPagto", StringUtil.LTrim( StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0)));
         A452Contrato_CalculoDivergencia = "B";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
         A453Contrato_IndiceDivergencia = (decimal)(10);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
         A92Contrato_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92Contrato_Ativo", A92Contrato_Ativo);
         O1013Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
         n1013Contrato_PrepostoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
         O116Contrato_ValorUnidadeContratacao = A116Contrato_ValorUnidadeContratacao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
         Z77Contrato_Numero = "";
         Z78Contrato_NumeroAta = "";
         Z79Contrato_Ano = 0;
         Z115Contrato_UnidadeContratacao = 0;
         Z81Contrato_Quantidade = 0;
         Z82Contrato_DataVigenciaInicio = DateTime.MinValue;
         Z83Contrato_DataVigenciaTermino = DateTime.MinValue;
         Z84Contrato_DataPublicacaoDOU = DateTime.MinValue;
         Z85Contrato_DataAssinatura = DateTime.MinValue;
         Z86Contrato_DataPedidoReajuste = DateTime.MinValue;
         Z87Contrato_DataTerminoAta = DateTime.MinValue;
         Z88Contrato_DataFimAdaptacao = DateTime.MinValue;
         Z89Contrato_Valor = 0;
         Z116Contrato_ValorUnidadeContratacao = 0;
         Z91Contrato_DiasPagto = 0;
         Z452Contrato_CalculoDivergencia = "";
         Z453Contrato_IndiceDivergencia = 0;
         Z1150Contrato_AceitaPFFS = false;
         Z92Contrato_Ativo = false;
         Z1354Contrato_PrdFtrCada = "";
         Z2086Contrato_LmtFtr = 0;
         Z1357Contrato_PrdFtrIni = 0;
         Z1358Contrato_PrdFtrFim = 0;
         Z39Contratada_Codigo = 0;
         Z75Contrato_AreaTrabalhoCod = 0;
         Z1013Contrato_PrepostoCod = 0;
      }

      protected void InitAll0G17( )
      {
         A74Contrato_Codigo = 0;
         n74Contrato_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         InitializeNonKey0G17( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A75Contrato_AreaTrabalhoCod = i75Contrato_AreaTrabalhoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
         A92Contrato_Ativo = i92Contrato_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92Contrato_Ativo", A92Contrato_Ativo);
         A453Contrato_IndiceDivergencia = i453Contrato_IndiceDivergencia;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
         A452Contrato_CalculoDivergencia = i452Contrato_CalculoDivergencia;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
         A91Contrato_DiasPagto = i91Contrato_DiasPagto;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A91Contrato_DiasPagto", StringUtil.LTrim( StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202052118104616");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contrato.js", "?202052118104616");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratada_pessoanom_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         lblTextblockcontratada_pessoacnpj_Internalname = "TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         lblTextblockcontrato_prepostocod_Internalname = "TEXTBLOCKCONTRATO_PREPOSTOCOD";
         dynContrato_PrepostoCod_Internalname = "CONTRATO_PREPOSTOCOD";
         lblTextblockcontrato_numero_Internalname = "TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         lblTextblockcontrato_numeroata_Internalname = "TEXTBLOCKCONTRATO_NUMEROATA";
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA";
         lblTextblockcontrato_ano_Internalname = "TEXTBLOCKCONTRATO_ANO";
         edtContrato_Ano_Internalname = "CONTRATO_ANO";
         lblTextblockcontrato_objeto_Internalname = "TEXTBLOCKCONTRATO_OBJETO";
         edtContrato_Objeto_Internalname = "CONTRATO_OBJETO";
         lblTextblockcontrato_datavigenciainicio_Internalname = "TEXTBLOCKCONTRATO_DATAVIGENCIAINICIO";
         edtContrato_DataVigenciaInicio_Internalname = "CONTRATO_DATAVIGENCIAINICIO";
         lblTextblockcontrato_datavigenciatermino_Internalname = "TEXTBLOCKCONTRATO_DATAVIGENCIATERMINO";
         edtContrato_DataVigenciaTermino_Internalname = "CONTRATO_DATAVIGENCIATERMINO";
         lblTextblockcontrato_datapublicacaodou_Internalname = "TEXTBLOCKCONTRATO_DATAPUBLICACAODOU";
         edtContrato_DataPublicacaoDOU_Internalname = "CONTRATO_DATAPUBLICACAODOU";
         lblTextblockcontrato_dataassinatura_Internalname = "TEXTBLOCKCONTRATO_DATAASSINATURA";
         edtContrato_DataAssinatura_Internalname = "CONTRATO_DATAASSINATURA";
         lblTextblockcontrato_datapedidoreajuste_Internalname = "TEXTBLOCKCONTRATO_DATAPEDIDOREAJUSTE";
         edtContrato_DataPedidoReajuste_Internalname = "CONTRATO_DATAPEDIDOREAJUSTE";
         lblTextblockcontrato_dataterminoata_Internalname = "TEXTBLOCKCONTRATO_DATATERMINOATA";
         edtContrato_DataTerminoAta_Internalname = "CONTRATO_DATATERMINOATA";
         lblTextblockcontrato_datafimadaptacao_Internalname = "TEXTBLOCKCONTRATO_DATAFIMADAPTACAO";
         edtContrato_DataFimAdaptacao_Internalname = "CONTRATO_DATAFIMADAPTACAO";
         lblTextblockcontrato_valor_Internalname = "TEXTBLOCKCONTRATO_VALOR";
         edtContrato_Valor_Internalname = "CONTRATO_VALOR";
         lblTextblockcontrato_unidadecontratacao_Internalname = "TEXTBLOCKCONTRATO_UNIDADECONTRATACAO";
         cmbContrato_UnidadeContratacao_Internalname = "CONTRATO_UNIDADECONTRATACAO";
         lblTextblockcontrato_quantidade_Internalname = "TEXTBLOCKCONTRATO_QUANTIDADE";
         edtContrato_Quantidade_Internalname = "CONTRATO_QUANTIDADE";
         lblTextblockcontrato_valorunidadecontratacao_Internalname = "TEXTBLOCKCONTRATO_VALORUNIDADECONTRATACAO";
         edtContrato_ValorUnidadeContratacao_Internalname = "CONTRATO_VALORUNIDADECONTRATACAO";
         lblTextblockcontrato_prdftrcada_Internalname = "TEXTBLOCKCONTRATO_PRDFTRCADA";
         cmbContrato_PrdFtrCada_Internalname = "CONTRATO_PRDFTRCADA";
         lblTextblockcontrato_prdftrini_Internalname = "TEXTBLOCKCONTRATO_PRDFTRINI";
         edtContrato_PrdFtrIni_Internalname = "CONTRATO_PRDFTRINI";
         lblTextblockcontrato_prdftrfim_Internalname = "TEXTBLOCKCONTRATO_PRDFTRFIM";
         edtContrato_PrdFtrFim_Internalname = "CONTRATO_PRDFTRFIM";
         lblTextblockcontrato_lmtftr_Internalname = "TEXTBLOCKCONTRATO_LMTFTR";
         edtContrato_LmtFtr_Internalname = "CONTRATO_LMTFTR";
         cmbavUnidadedemedicao_Internalname = "vUNIDADEDEMEDICAO";
         tblTablemergedcontrato_lmtftr_Internalname = "TABLEMERGEDCONTRATO_LMTFTR";
         lblTextblockcontrato_diaspagto_Internalname = "TEXTBLOCKCONTRATO_DIASPAGTO";
         edtContrato_DiasPagto_Internalname = "CONTRATO_DIASPAGTO";
         lblTextblockcontrato_regraspagto_Internalname = "TEXTBLOCKCONTRATO_REGRASPAGTO";
         edtContrato_RegrasPagto_Internalname = "CONTRATO_REGRASPAGTO";
         lblTextblockcontrato_calculodivergencia_Internalname = "TEXTBLOCKCONTRATO_CALCULODIVERGENCIA";
         cmbContrato_CalculoDivergencia_Internalname = "CONTRATO_CALCULODIVERGENCIA";
         lblTextblockcontrato_indicedivergencia_Internalname = "TEXTBLOCKCONTRATO_INDICEDIVERGENCIA";
         edtContrato_IndiceDivergencia_Internalname = "CONTRATO_INDICEDIVERGENCIA";
         lblContrato_indicedivergencia_righttext_Internalname = "CONTRATO_INDICEDIVERGENCIA_RIGHTTEXT";
         tblTablemergedcontrato_indicedivergencia_Internalname = "TABLEMERGEDCONTRATO_INDICEDIVERGENCIA";
         lblTextblockcontrato_aceitapffs_Internalname = "TEXTBLOCKCONTRATO_ACEITAPFFS";
         chkContrato_AceitaPFFS_Internalname = "CONTRATO_ACEITAPFFS";
         lblTextblockcontrato_ativo_Internalname = "TEXTBLOCKCONTRATO_ATIVO";
         chkContrato_Ativo_Internalname = "CONTRATO_ATIVO";
         lblTextblockcontratada_sigla_Internalname = "TEXTBLOCKCONTRATADA_SIGLA";
         edtContratada_Sigla_Internalname = "CONTRATADA_SIGLA";
         lblTextblockcontratada_tipofabrica_Internalname = "TEXTBLOCKCONTRATADA_TIPOFABRICA";
         cmbContratada_TipoFabrica_Internalname = "CONTRATADA_TIPOFABRICA";
         lblTextblockcontrato_datainiciota_Internalname = "TEXTBLOCKCONTRATO_DATAINICIOTA";
         edtContrato_DataInicioTA_Internalname = "CONTRATO_DATAINICIOTA";
         lblTextblockcontrato_datafimta_Internalname = "TEXTBLOCKCONTRATO_DATAFIMTA";
         edtContrato_DataFimTA_Internalname = "CONTRATO_DATAFIMTA";
         lblTextblockcontrato_datatermino_Internalname = "TEXTBLOCKCONTRATO_DATATERMINO";
         edtContrato_DataTermino_Internalname = "CONTRATO_DATATERMINO";
         lblTextblockcontrato_valorundcntatual_Internalname = "TEXTBLOCKCONTRATO_VALORUNDCNTATUAL";
         edtContrato_ValorUndCntAtual_Internalname = "CONTRATO_VALORUNDCNTATUAL";
         lblTextblockcontrato_prepostopescod_Internalname = "TEXTBLOCKCONTRATO_PREPOSTOPESCOD";
         edtContrato_PrepostoPesCod_Internalname = "CONTRATO_PREPOSTOPESCOD";
         lblTextblockcontrato_prepostonom_Internalname = "TEXTBLOCKCONTRATO_PREPOSTONOM";
         edtContrato_PrepostoNom_Internalname = "CONTRATO_PREPOSTONOM";
         lblTextblockcontrato_identificacao_Internalname = "TEXTBLOCKCONTRATO_IDENTIFICACAO";
         edtContrato_Identificacao_Internalname = "CONTRATO_IDENTIFICACAO";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTablemergedtrn_enter_Internalname = "TABLEMERGEDTRN_ENTER";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         Confirmpanel_Internalname = "CONFIRMPANEL";
         tblUsrtable_Internalname = "USRTABLE";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContrato_AreaTrabalhoCod_Internalname = "CONTRATO_AREATRABALHOCOD";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Confirmpanel_Confirmtype = "0";
         Confirmpanel_Buttoncanceltext = "Cancelar";
         Confirmpanel_Buttonnotext = "N�o";
         Confirmpanel_Buttonyestext = "Ok";
         Confirmpanel_Confirmtext = "Os servu�os deste contrato tamb�m tem valores que podem precisar de atualiza��o!";
         Confirmpanel_Modal = Convert.ToBoolean( 0);
         Confirmpanel_Icon = "3";
         Confirmpanel_Title = "Aten��o";
         Confirmpanel_Closeable = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��es Gerais";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contrato";
         cmbavUnidadedemedicao_Jsonclick = "";
         cmbavUnidadedemedicao.Enabled = 0;
         edtContrato_LmtFtr_Jsonclick = "";
         edtContrato_LmtFtr_Enabled = 1;
         edtContrato_IndiceDivergencia_Jsonclick = "";
         edtContrato_IndiceDivergencia_Enabled = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContrato_Identificacao_Jsonclick = "";
         edtContrato_Identificacao_Enabled = 0;
         edtContrato_PrepostoNom_Jsonclick = "";
         edtContrato_PrepostoNom_Enabled = 0;
         edtContrato_PrepostoPesCod_Jsonclick = "";
         edtContrato_PrepostoPesCod_Enabled = 0;
         edtContrato_ValorUndCntAtual_Jsonclick = "";
         edtContrato_ValorUndCntAtual_Enabled = 0;
         edtContrato_DataTermino_Jsonclick = "";
         edtContrato_DataTermino_Enabled = 0;
         edtContrato_DataFimTA_Jsonclick = "";
         edtContrato_DataFimTA_Enabled = 0;
         edtContrato_DataInicioTA_Jsonclick = "";
         edtContrato_DataInicioTA_Enabled = 0;
         cmbContratada_TipoFabrica_Jsonclick = "";
         cmbContratada_TipoFabrica.Enabled = 0;
         edtContratada_Sigla_Jsonclick = "";
         edtContratada_Sigla_Enabled = 0;
         chkContrato_Ativo.Enabled = 1;
         chkContrato_Ativo.Visible = 1;
         lblTextblockcontrato_ativo_Visible = 1;
         chkContrato_AceitaPFFS.Enabled = 1;
         cmbContrato_CalculoDivergencia_Jsonclick = "";
         cmbContrato_CalculoDivergencia.Enabled = 1;
         edtContrato_RegrasPagto_Enabled = 1;
         edtContrato_DiasPagto_Jsonclick = "";
         edtContrato_DiasPagto_Enabled = 1;
         edtContrato_PrdFtrFim_Jsonclick = "";
         edtContrato_PrdFtrFim_Enabled = 1;
         edtContrato_PrdFtrIni_Jsonclick = "";
         edtContrato_PrdFtrIni_Enabled = 1;
         cmbContrato_PrdFtrCada_Jsonclick = "";
         cmbContrato_PrdFtrCada.Enabled = 1;
         edtContrato_ValorUnidadeContratacao_Jsonclick = "";
         edtContrato_ValorUnidadeContratacao_Enabled = 1;
         edtContrato_Quantidade_Jsonclick = "";
         edtContrato_Quantidade_Enabled = 1;
         cmbContrato_UnidadeContratacao_Jsonclick = "";
         cmbContrato_UnidadeContratacao.Enabled = 1;
         edtContrato_Valor_Jsonclick = "";
         edtContrato_Valor_Enabled = 1;
         edtContrato_DataFimAdaptacao_Jsonclick = "";
         edtContrato_DataFimAdaptacao_Enabled = 1;
         edtContrato_DataTerminoAta_Jsonclick = "";
         edtContrato_DataTerminoAta_Enabled = 1;
         edtContrato_DataPedidoReajuste_Jsonclick = "";
         edtContrato_DataPedidoReajuste_Enabled = 1;
         edtContrato_DataAssinatura_Jsonclick = "";
         edtContrato_DataAssinatura_Enabled = 1;
         edtContrato_DataPublicacaoDOU_Jsonclick = "";
         edtContrato_DataPublicacaoDOU_Enabled = 1;
         edtContrato_DataVigenciaTermino_Jsonclick = "";
         edtContrato_DataVigenciaTermino_Enabled = 1;
         edtContrato_DataVigenciaInicio_Jsonclick = "";
         edtContrato_DataVigenciaInicio_Enabled = 1;
         edtContrato_Objeto_Enabled = 1;
         edtContrato_Ano_Jsonclick = "";
         edtContrato_Ano_Enabled = 1;
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_NumeroAta_Enabled = 1;
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Numero_Enabled = 1;
         dynContrato_PrepostoCod_Jsonclick = "";
         dynContrato_PrepostoCod.Enabled = 1;
         lblTextblockcontrato_prepostocod_Caption = "Preposto";
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaCNPJ_Enabled = 0;
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaNom_Enabled = 0;
         edtContratada_Codigo_Jsonclick = "";
         edtContratada_Codigo_Enabled = 1;
         edtContratada_Codigo_Visible = 1;
         edtContrato_Codigo_Jsonclick = "";
         edtContrato_Codigo_Enabled = 0;
         edtContrato_Codigo_Visible = 1;
         edtContrato_AreaTrabalhoCod_Jsonclick = "";
         edtContrato_AreaTrabalhoCod_Enabled = 1;
         edtContrato_AreaTrabalhoCod_Visible = 1;
         chkContrato_Ativo.Caption = "";
         chkContrato_AceitaPFFS.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTRATO_PREPOSTOCOD0G17( int AV17Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTRATO_PREPOSTOCOD_data0G17( AV17Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTRATO_PREPOSTOCOD_html0G17( int AV17Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLACONTRATO_PREPOSTOCOD_data0G17( AV17Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynContrato_PrepostoCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContrato_PrepostoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTRATO_PREPOSTOCOD_data0G17( int AV17Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000G64 */
         pr_default.execute(46, new Object[] {AV17Contratada_Codigo});
         while ( (pr_default.getStatus(46) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000G64_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000G64_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(46);
         }
         pr_default.close(46);
      }

      protected void GX13ASACONTRATO_VALORUNDCNTATUAL0G17( int A74Contrato_Codigo ,
                                                           decimal A116Contrato_ValorUnidadeContratacao )
      {
         A1870Contrato_ValorUndCntAtual = ((GetContrato_ValorUndCntAtual0( A74Contrato_Codigo)>Convert.ToDecimal(0)) ? GetContrato_ValorUndCntAtual1( A74Contrato_Codigo) : A116Contrato_ValorUnidadeContratacao);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1870Contrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( A1870Contrato_ValorUndCntAtual, 18, 5)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A1870Contrato_ValorUndCntAtual, 18, 5, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_36_0G17( wwpbaseobjects.SdtAuditingObject AV24AuditingObject ,
                                 int A74Contrato_Codigo ,
                                 String Gx_mode )
      {
         new loadauditcontrato(context ).execute(  "Y", ref  AV24AuditingObject,  A74Contrato_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV24AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_37_0G17( wwpbaseobjects.SdtAuditingObject AV24AuditingObject ,
                                 int A74Contrato_Codigo ,
                                 String Gx_mode )
      {
         new loadauditcontrato(context ).execute(  "Y", ref  AV24AuditingObject,  A74Contrato_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV24AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_38_0G17( String Gx_mode ,
                                 wwpbaseobjects.SdtAuditingObject AV24AuditingObject ,
                                 int A74Contrato_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontrato(context ).execute(  "N", ref  AV24AuditingObject,  A74Contrato_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV24AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_39_0G17( String Gx_mode ,
                                 wwpbaseobjects.SdtAuditingObject AV24AuditingObject ,
                                 int A74Contrato_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontrato(context ).execute(  "N", ref  AV24AuditingObject,  A74Contrato_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV24AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_40_0G17( int A74Contrato_Codigo ,
                                 short A115Contrato_UnidadeContratacao ,
                                 DateTime A82Contrato_DataVigenciaInicio ,
                                 DateTime A83Contrato_DataVigenciaTermino ,
                                 String A516Contratada_TipoFabrica )
      {
         if ( ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) && ! T000G3_n115Contrato_UnidadeContratacao[0] )
         {
            new prc_saldocontratocriar(context ).execute(  A74Contrato_Codigo,  A115Contrato_UnidadeContratacao,  A82Contrato_DataVigenciaInicio,  A83Contrato_DataVigenciaTermino, out  AV22SaldoContrato_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22SaldoContrato_Codigo), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22SaldoContrato_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_41_0G17( )
      {
         if ( A1013Contrato_PrepostoCod != O1013Contrato_PrepostoCod )
         {
            new prc_prepostogestor(context ).execute( ref  A74Contrato_Codigo,  A1013Contrato_PrepostoCod,  O1013Contrato_PrepostoCod) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_42_0G17( int A74Contrato_Codigo )
      {
         new prc_inscontratogestor(context ).execute( ref  A74Contrato_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Contrato_codigo( int GX_Parm1 ,
                                         DateTime GX_Parm2 ,
                                         DateTime GX_Parm3 )
      {
         A74Contrato_Codigo = GX_Parm1;
         n74Contrato_Codigo = false;
         A842Contrato_DataInicioTA = GX_Parm2;
         n842Contrato_DataInicioTA = false;
         A843Contrato_DataFimTA = GX_Parm3;
         n843Contrato_DataFimTA = false;
         /* Using cursor T000G67 */
         pr_default.execute(47, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(47) != 101) )
         {
            A842Contrato_DataInicioTA = T000G67_A842Contrato_DataInicioTA[0];
            n842Contrato_DataInicioTA = T000G67_n842Contrato_DataInicioTA[0];
         }
         else
         {
            A842Contrato_DataInicioTA = DateTime.MinValue;
            n842Contrato_DataInicioTA = false;
         }
         pr_default.close(47);
         /* Using cursor T000G70 */
         pr_default.execute(48, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(48) != 101) )
         {
            A843Contrato_DataFimTA = T000G70_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = T000G70_n843Contrato_DataFimTA[0];
         }
         else
         {
            A843Contrato_DataFimTA = DateTime.MinValue;
            n843Contrato_DataFimTA = false;
         }
         pr_default.close(48);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A842Contrato_DataInicioTA = DateTime.MinValue;
            n842Contrato_DataInicioTA = false;
            A843Contrato_DataFimTA = DateTime.MinValue;
            n843Contrato_DataFimTA = false;
         }
         isValidOutput.Add(context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
         isValidOutput.Add(context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contrato_prepostocod( GXCombobox dynGX_Parm1 ,
                                              int GX_Parm2 ,
                                              String GX_Parm3 )
      {
         dynContrato_PrepostoCod = dynGX_Parm1;
         A1013Contrato_PrepostoCod = (int)(NumberUtil.Val( dynContrato_PrepostoCod.CurrentValue, "."));
         n1013Contrato_PrepostoCod = false;
         A1016Contrato_PrepostoPesCod = GX_Parm2;
         n1016Contrato_PrepostoPesCod = false;
         A1015Contrato_PrepostoNom = GX_Parm3;
         n1015Contrato_PrepostoNom = false;
         /* Using cursor T000G71 */
         pr_default.execute(49, new Object[] {n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod});
         if ( (pr_default.getStatus(49) == 101) )
         {
            if ( ! ( (0==A1013Contrato_PrepostoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATO_PREPOSTOCOD");
               AnyError = 1;
               GX_FocusControl = dynContrato_PrepostoCod_Internalname;
            }
         }
         A1016Contrato_PrepostoPesCod = T000G71_A1016Contrato_PrepostoPesCod[0];
         n1016Contrato_PrepostoPesCod = T000G71_n1016Contrato_PrepostoPesCod[0];
         pr_default.close(49);
         /* Using cursor T000G72 */
         pr_default.execute(50, new Object[] {n1016Contrato_PrepostoPesCod, A1016Contrato_PrepostoPesCod});
         if ( (pr_default.getStatus(50) == 101) )
         {
            if ( ! ( (0==A1016Contrato_PrepostoPesCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1015Contrato_PrepostoNom = T000G72_A1015Contrato_PrepostoNom[0];
         n1015Contrato_PrepostoNom = T000G72_n1015Contrato_PrepostoNom[0];
         pr_default.close(50);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1016Contrato_PrepostoPesCod = 0;
            n1016Contrato_PrepostoPesCod = false;
            A1015Contrato_PrepostoNom = "";
            n1015Contrato_PrepostoNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1016Contrato_PrepostoPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1015Contrato_PrepostoNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contrato_unidadecontratacao( GXCombobox cmbGX_Parm1 ,
                                                     GXCombobox cmbGX_Parm2 )
      {
         cmbContrato_UnidadeContratacao = cmbGX_Parm1;
         A115Contrato_UnidadeContratacao = (short)(NumberUtil.Val( cmbContrato_UnidadeContratacao.CurrentValue, "."));
         cmbavUnidadedemedicao = cmbGX_Parm2;
         AV25UnidadeDeMedicao = (short)(NumberUtil.Val( cmbavUnidadedemedicao.CurrentValue, "."));
         cmbavUnidadedemedicao.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0));
         if ( ! ( ( A115Contrato_UnidadeContratacao == 1 ) || ( A115Contrato_UnidadeContratacao == 2 ) || ( A115Contrato_UnidadeContratacao == 3 ) ) )
         {
            GX_msglist.addItem("Campo Unidade fora do intervalo", "OutOfRange", 1, "CONTRATO_UNIDADECONTRATACAO");
            AnyError = 1;
            GX_FocusControl = cmbContrato_UnidadeContratacao_Internalname;
         }
         AV25UnidadeDeMedicao = A115Contrato_UnidadeContratacao;
         cmbavUnidadedemedicao.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0));
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         cmbavUnidadedemedicao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeDeMedicao), 4, 0));
         isValidOutput.Add(cmbavUnidadedemedicao);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contrato_areatrabalhocod( int GX_Parm1 ,
                                                  String GX_Parm2 )
      {
         A75Contrato_AreaTrabalhoCod = GX_Parm1;
         A76Contrato_AreaTrabalhoDes = GX_Parm2;
         n76Contrato_AreaTrabalhoDes = false;
         /* Using cursor T000G73 */
         pr_default.execute(51, new Object[] {A75Contrato_AreaTrabalhoCod});
         if ( (pr_default.getStatus(51) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contrato_Area Trabalho'.", "ForeignKeyNotFound", 1, "CONTRATO_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtContrato_AreaTrabalhoCod_Internalname;
         }
         A76Contrato_AreaTrabalhoDes = T000G73_A76Contrato_AreaTrabalhoDes[0];
         n76Contrato_AreaTrabalhoDes = T000G73_n76Contrato_AreaTrabalhoDes[0];
         pr_default.close(51);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A76Contrato_AreaTrabalhoDes = "";
            n76Contrato_AreaTrabalhoDes = false;
         }
         isValidOutput.Add(A76Contrato_AreaTrabalhoDes);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratada_codigo( GXCombobox dynGX_Parm1 ,
                                           int GX_Parm2 ,
                                           int GX_Parm3 ,
                                           String GX_Parm4 ,
                                           GXCombobox cmbGX_Parm5 ,
                                           String GX_Parm6 ,
                                           String GX_Parm7 )
      {
         dynContrato_PrepostoCod = dynGX_Parm1;
         A1013Contrato_PrepostoCod = (int)(NumberUtil.Val( dynContrato_PrepostoCod.CurrentValue, "."));
         n1013Contrato_PrepostoCod = false;
         A39Contratada_Codigo = GX_Parm2;
         A40Contratada_PessoaCod = GX_Parm3;
         A438Contratada_Sigla = GX_Parm4;
         cmbContratada_TipoFabrica = cmbGX_Parm5;
         A516Contratada_TipoFabrica = cmbContratada_TipoFabrica.CurrentValue;
         cmbContratada_TipoFabrica.CurrentValue = A516Contratada_TipoFabrica;
         A41Contratada_PessoaNom = GX_Parm6;
         n41Contratada_PessoaNom = false;
         A42Contratada_PessoaCNPJ = GX_Parm7;
         n42Contratada_PessoaCNPJ = false;
         /* Using cursor T000G74 */
         pr_default.execute(52, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(52) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "CONTRATADA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratada_Codigo_Internalname;
         }
         A438Contratada_Sigla = T000G74_A438Contratada_Sigla[0];
         A516Contratada_TipoFabrica = T000G74_A516Contratada_TipoFabrica[0];
         cmbContratada_TipoFabrica.CurrentValue = A516Contratada_TipoFabrica;
         A40Contratada_PessoaCod = T000G74_A40Contratada_PessoaCod[0];
         pr_default.close(52);
         /* Using cursor T000G75 */
         pr_default.execute(53, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(53) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T000G75_A41Contratada_PessoaNom[0];
         n41Contratada_PessoaNom = T000G75_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T000G75_A42Contratada_PessoaCNPJ[0];
         n42Contratada_PessoaCNPJ = T000G75_n42Contratada_PessoaCNPJ[0];
         pr_default.close(53);
         O1013Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
         n1013Contrato_PrepostoCod = false;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A438Contratada_Sigla = "";
            A516Contratada_TipoFabrica = "";
            cmbContratada_TipoFabrica.CurrentValue = A516Contratada_TipoFabrica;
            A40Contratada_PessoaCod = 0;
            A41Contratada_PessoaNom = "";
            n41Contratada_PessoaNom = false;
            A42Contratada_PessoaCNPJ = "";
            n42Contratada_PessoaCNPJ = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A438Contratada_Sigla));
         cmbContratada_TipoFabrica.CurrentValue = A516Contratada_TipoFabrica;
         isValidOutput.Add(cmbContratada_TipoFabrica);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A41Contratada_PessoaNom));
         isValidOutput.Add(A42Contratada_PessoaCNPJ);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120G2',iparms:[{av:'AV24AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV27Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A116Contrato_ValorUnidadeContratacao',fld:'CONTRATO_VALORUNIDADECONTRATACAO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("CONTRATO_VALORUNIDADECONTRATACAO.ISVALID","{handler:'E130G2',iparms:[{av:'A116Contrato_ValorUnidadeContratacao',fld:'CONTRATO_VALORUNIDADECONTRATACAO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      protected decimal GetContrato_ValorUndCntAtual1( int E74Contrato_Codigo )
      {
         X1361ContratoTermoAditivo_VlrUntUndCnt = 0;
         X315ContratoTermoAditivo_Codigo = 0;
         /* Using cursor T000G76 */
         pr_default.execute(54, new Object[] {nA74Contrato_Codigo, E74Contrato_Codigo});
         if ( (pr_default.getStatus(54) != 101) )
         {
            X315ContratoTermoAditivo_Codigo = T000G76_A315ContratoTermoAditivo_Codigo[0];
            /* Using cursor T000G77 */
            pr_default.execute(55, new Object[] {nE74Contrato_Codigo, E74Contrato_Codigo, X315ContratoTermoAditivo_Codigo});
            if ( (pr_default.getStatus(55) != 101) )
            {
               X1361ContratoTermoAditivo_VlrUntUndCnt = T000G77_A1361ContratoTermoAditivo_VlrUntUndCnt[0];
               nX1361ContratoTermoAditivo_VlrUntUndCnt = T000G77_n1361ContratoTermoAditivo_VlrUntUndCnt[0];
            }
            pr_default.close(55);
         }
         pr_default.close(54);
         return X1361ContratoTermoAditivo_VlrUntUndCnt ;
      }

      protected decimal GetContrato_ValorUndCntAtual0( int E74Contrato_Codigo )
      {
         X1361ContratoTermoAditivo_VlrUntUndCnt = 0;
         X315ContratoTermoAditivo_Codigo = 0;
         /* Using cursor T000G78 */
         pr_default.execute(56, new Object[] {nE74Contrato_Codigo, E74Contrato_Codigo});
         if ( (pr_default.getStatus(56) != 101) )
         {
            X315ContratoTermoAditivo_Codigo = T000G78_A315ContratoTermoAditivo_Codigo[0];
            /* Using cursor T000G79 */
            pr_default.execute(57, new Object[] {nE74Contrato_Codigo, E74Contrato_Codigo, X315ContratoTermoAditivo_Codigo});
            if ( (pr_default.getStatus(57) != 101) )
            {
               X1361ContratoTermoAditivo_VlrUntUndCnt = T000G79_A1361ContratoTermoAditivo_VlrUntUndCnt[0];
               nX1361ContratoTermoAditivo_VlrUntUndCnt = T000G79_n1361ContratoTermoAditivo_VlrUntUndCnt[0];
            }
            pr_default.close(57);
         }
         pr_default.close(56);
         return X1361ContratoTermoAditivo_VlrUntUndCnt ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(52);
         pr_default.close(26);
         pr_default.close(51);
         pr_default.close(25);
         pr_default.close(49);
         pr_default.close(28);
         pr_default.close(53);
         pr_default.close(27);
         pr_default.close(50);
         pr_default.close(29);
         pr_default.close(47);
         pr_default.close(23);
         pr_default.close(48);
         pr_default.close(24);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z77Contrato_Numero = "";
         Z78Contrato_NumeroAta = "";
         Z82Contrato_DataVigenciaInicio = DateTime.MinValue;
         Z83Contrato_DataVigenciaTermino = DateTime.MinValue;
         Z84Contrato_DataPublicacaoDOU = DateTime.MinValue;
         Z85Contrato_DataAssinatura = DateTime.MinValue;
         Z86Contrato_DataPedidoReajuste = DateTime.MinValue;
         Z87Contrato_DataTerminoAta = DateTime.MinValue;
         Z88Contrato_DataFimAdaptacao = DateTime.MinValue;
         Z452Contrato_CalculoDivergencia = "";
         Z1354Contrato_PrdFtrCada = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A516Contratada_TipoFabrica = "";
         GXKey = "";
         A1354Contrato_PrdFtrCada = "";
         A452Contrato_CalculoDivergencia = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         A41Contratada_PessoaNom = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         A42Contratada_PessoaCNPJ = "";
         lblTextblockcontrato_prepostocod_Jsonclick = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         A77Contrato_Numero = "";
         lblTextblockcontrato_numeroata_Jsonclick = "";
         A78Contrato_NumeroAta = "";
         lblTextblockcontrato_ano_Jsonclick = "";
         lblTextblockcontrato_objeto_Jsonclick = "";
         A80Contrato_Objeto = "";
         lblTextblockcontrato_datavigenciainicio_Jsonclick = "";
         lblTextblockcontrato_datavigenciatermino_Jsonclick = "";
         lblTextblockcontrato_datapublicacaodou_Jsonclick = "";
         A84Contrato_DataPublicacaoDOU = DateTime.MinValue;
         lblTextblockcontrato_dataassinatura_Jsonclick = "";
         A85Contrato_DataAssinatura = DateTime.MinValue;
         lblTextblockcontrato_datapedidoreajuste_Jsonclick = "";
         A86Contrato_DataPedidoReajuste = DateTime.MinValue;
         lblTextblockcontrato_dataterminoata_Jsonclick = "";
         A87Contrato_DataTerminoAta = DateTime.MinValue;
         lblTextblockcontrato_datafimadaptacao_Jsonclick = "";
         A88Contrato_DataFimAdaptacao = DateTime.MinValue;
         lblTextblockcontrato_valor_Jsonclick = "";
         lblTextblockcontrato_unidadecontratacao_Jsonclick = "";
         lblTextblockcontrato_quantidade_Jsonclick = "";
         lblTextblockcontrato_valorunidadecontratacao_Jsonclick = "";
         lblTextblockcontrato_prdftrcada_Jsonclick = "";
         lblTextblockcontrato_prdftrini_Jsonclick = "";
         lblTextblockcontrato_prdftrfim_Jsonclick = "";
         lblTextblockcontrato_lmtftr_Jsonclick = "";
         lblTextblockcontrato_diaspagto_Jsonclick = "";
         lblTextblockcontrato_regraspagto_Jsonclick = "";
         A90Contrato_RegrasPagto = "";
         lblTextblockcontrato_calculodivergencia_Jsonclick = "";
         lblTextblockcontrato_indicedivergencia_Jsonclick = "";
         lblTextblockcontrato_aceitapffs_Jsonclick = "";
         lblTextblockcontrato_ativo_Jsonclick = "";
         lblTextblockcontratada_sigla_Jsonclick = "";
         A438Contratada_Sigla = "";
         lblTextblockcontratada_tipofabrica_Jsonclick = "";
         lblTextblockcontrato_datainiciota_Jsonclick = "";
         A842Contrato_DataInicioTA = DateTime.MinValue;
         lblTextblockcontrato_datafimta_Jsonclick = "";
         A843Contrato_DataFimTA = DateTime.MinValue;
         lblTextblockcontrato_datatermino_Jsonclick = "";
         A1869Contrato_DataTermino = DateTime.MinValue;
         lblTextblockcontrato_valorundcntatual_Jsonclick = "";
         lblTextblockcontrato_prepostopescod_Jsonclick = "";
         lblTextblockcontrato_prepostonom_Jsonclick = "";
         A1015Contrato_PrepostoNom = "";
         lblTextblockcontrato_identificacao_Jsonclick = "";
         A2096Contrato_Identificacao = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblContrato_indicedivergencia_righttext_Jsonclick = "";
         AV24AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A76Contrato_AreaTrabalhoDes = "";
         AV27Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         Confirmpanel_Width = "";
         Confirmpanel_Height = "";
         Confirmpanel_Result = "";
         Confirmpanel_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode17 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         Z80Contrato_Objeto = "";
         Z90Contrato_RegrasPagto = "";
         Z842Contrato_DataInicioTA = DateTime.MinValue;
         Z843Contrato_DataFimTA = DateTime.MinValue;
         Z76Contrato_AreaTrabalhoDes = "";
         Z438Contratada_Sigla = "";
         Z516Contratada_TipoFabrica = "";
         Z41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         Z1015Contrato_PrepostoNom = "";
         T000G11_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         T000G11_n842Contrato_DataInicioTA = new bool[] {false} ;
         T000G14_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         T000G14_n843Contrato_DataFimTA = new bool[] {false} ;
         T000G6_A1016Contrato_PrepostoPesCod = new int[1] ;
         T000G6_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         T000G8_A1015Contrato_PrepostoNom = new String[] {""} ;
         T000G8_n1015Contrato_PrepostoNom = new bool[] {false} ;
         T000G4_A438Contratada_Sigla = new String[] {""} ;
         T000G4_A516Contratada_TipoFabrica = new String[] {""} ;
         T000G4_A40Contratada_PessoaCod = new int[1] ;
         T000G7_A41Contratada_PessoaNom = new String[] {""} ;
         T000G7_n41Contratada_PessoaNom = new bool[] {false} ;
         T000G7_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000G7_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000G5_A76Contrato_AreaTrabalhoDes = new String[] {""} ;
         T000G5_n76Contrato_AreaTrabalhoDes = new bool[] {false} ;
         T000G19_A74Contrato_Codigo = new int[1] ;
         T000G19_n74Contrato_Codigo = new bool[] {false} ;
         T000G19_A76Contrato_AreaTrabalhoDes = new String[] {""} ;
         T000G19_n76Contrato_AreaTrabalhoDes = new bool[] {false} ;
         T000G19_A41Contratada_PessoaNom = new String[] {""} ;
         T000G19_n41Contratada_PessoaNom = new bool[] {false} ;
         T000G19_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000G19_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000G19_A438Contratada_Sigla = new String[] {""} ;
         T000G19_A516Contratada_TipoFabrica = new String[] {""} ;
         T000G19_A77Contrato_Numero = new String[] {""} ;
         T000G19_A78Contrato_NumeroAta = new String[] {""} ;
         T000G19_n78Contrato_NumeroAta = new bool[] {false} ;
         T000G19_A79Contrato_Ano = new short[1] ;
         T000G19_A80Contrato_Objeto = new String[] {""} ;
         T000G19_A115Contrato_UnidadeContratacao = new short[1] ;
         T000G19_A81Contrato_Quantidade = new int[1] ;
         T000G19_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T000G19_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         T000G19_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         T000G19_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         T000G19_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         T000G19_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         T000G19_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         T000G19_A89Contrato_Valor = new decimal[1] ;
         T000G19_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         T000G19_A90Contrato_RegrasPagto = new String[] {""} ;
         T000G19_A91Contrato_DiasPagto = new short[1] ;
         T000G19_A452Contrato_CalculoDivergencia = new String[] {""} ;
         T000G19_A453Contrato_IndiceDivergencia = new decimal[1] ;
         T000G19_A1150Contrato_AceitaPFFS = new bool[] {false} ;
         T000G19_n1150Contrato_AceitaPFFS = new bool[] {false} ;
         T000G19_A92Contrato_Ativo = new bool[] {false} ;
         T000G19_A1015Contrato_PrepostoNom = new String[] {""} ;
         T000G19_n1015Contrato_PrepostoNom = new bool[] {false} ;
         T000G19_A1354Contrato_PrdFtrCada = new String[] {""} ;
         T000G19_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         T000G19_A2086Contrato_LmtFtr = new decimal[1] ;
         T000G19_n2086Contrato_LmtFtr = new bool[] {false} ;
         T000G19_A1357Contrato_PrdFtrIni = new short[1] ;
         T000G19_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         T000G19_A1358Contrato_PrdFtrFim = new short[1] ;
         T000G19_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         T000G19_A39Contratada_Codigo = new int[1] ;
         T000G19_A75Contrato_AreaTrabalhoCod = new int[1] ;
         T000G19_A1013Contrato_PrepostoCod = new int[1] ;
         T000G19_n1013Contrato_PrepostoCod = new bool[] {false} ;
         T000G19_A40Contratada_PessoaCod = new int[1] ;
         T000G19_A1016Contrato_PrepostoPesCod = new int[1] ;
         T000G19_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         T000G19_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         T000G19_n842Contrato_DataInicioTA = new bool[] {false} ;
         T000G19_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         T000G19_n843Contrato_DataFimTA = new bool[] {false} ;
         T000G22_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         T000G22_n842Contrato_DataInicioTA = new bool[] {false} ;
         T000G25_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         T000G25_n843Contrato_DataFimTA = new bool[] {false} ;
         T000G26_A76Contrato_AreaTrabalhoDes = new String[] {""} ;
         T000G26_n76Contrato_AreaTrabalhoDes = new bool[] {false} ;
         T000G27_A438Contratada_Sigla = new String[] {""} ;
         T000G27_A516Contratada_TipoFabrica = new String[] {""} ;
         T000G27_A40Contratada_PessoaCod = new int[1] ;
         T000G28_A41Contratada_PessoaNom = new String[] {""} ;
         T000G28_n41Contratada_PessoaNom = new bool[] {false} ;
         T000G28_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000G28_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000G29_A1016Contrato_PrepostoPesCod = new int[1] ;
         T000G29_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         T000G30_A1015Contrato_PrepostoNom = new String[] {""} ;
         T000G30_n1015Contrato_PrepostoNom = new bool[] {false} ;
         T000G31_A74Contrato_Codigo = new int[1] ;
         T000G31_n74Contrato_Codigo = new bool[] {false} ;
         T000G3_A74Contrato_Codigo = new int[1] ;
         T000G3_n74Contrato_Codigo = new bool[] {false} ;
         T000G3_A77Contrato_Numero = new String[] {""} ;
         T000G3_A78Contrato_NumeroAta = new String[] {""} ;
         T000G3_n78Contrato_NumeroAta = new bool[] {false} ;
         T000G3_A79Contrato_Ano = new short[1] ;
         T000G3_A80Contrato_Objeto = new String[] {""} ;
         T000G3_A115Contrato_UnidadeContratacao = new short[1] ;
         T000G3_A81Contrato_Quantidade = new int[1] ;
         T000G3_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T000G3_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         T000G3_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         T000G3_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         T000G3_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         T000G3_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         T000G3_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         T000G3_A89Contrato_Valor = new decimal[1] ;
         T000G3_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         T000G3_A90Contrato_RegrasPagto = new String[] {""} ;
         T000G3_A91Contrato_DiasPagto = new short[1] ;
         T000G3_A452Contrato_CalculoDivergencia = new String[] {""} ;
         T000G3_A453Contrato_IndiceDivergencia = new decimal[1] ;
         T000G3_A1150Contrato_AceitaPFFS = new bool[] {false} ;
         T000G3_n1150Contrato_AceitaPFFS = new bool[] {false} ;
         T000G3_A92Contrato_Ativo = new bool[] {false} ;
         T000G3_A1354Contrato_PrdFtrCada = new String[] {""} ;
         T000G3_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         T000G3_A2086Contrato_LmtFtr = new decimal[1] ;
         T000G3_n2086Contrato_LmtFtr = new bool[] {false} ;
         T000G3_A1357Contrato_PrdFtrIni = new short[1] ;
         T000G3_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         T000G3_A1358Contrato_PrdFtrFim = new short[1] ;
         T000G3_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         T000G3_A39Contratada_Codigo = new int[1] ;
         T000G3_A75Contrato_AreaTrabalhoCod = new int[1] ;
         T000G3_A1013Contrato_PrepostoCod = new int[1] ;
         T000G3_n1013Contrato_PrepostoCod = new bool[] {false} ;
         T000G32_A74Contrato_Codigo = new int[1] ;
         T000G32_n74Contrato_Codigo = new bool[] {false} ;
         T000G33_A74Contrato_Codigo = new int[1] ;
         T000G33_n74Contrato_Codigo = new bool[] {false} ;
         T000G2_A74Contrato_Codigo = new int[1] ;
         T000G2_n74Contrato_Codigo = new bool[] {false} ;
         T000G2_A77Contrato_Numero = new String[] {""} ;
         T000G2_A78Contrato_NumeroAta = new String[] {""} ;
         T000G2_n78Contrato_NumeroAta = new bool[] {false} ;
         T000G2_A79Contrato_Ano = new short[1] ;
         T000G2_A80Contrato_Objeto = new String[] {""} ;
         T000G2_A115Contrato_UnidadeContratacao = new short[1] ;
         T000G2_A81Contrato_Quantidade = new int[1] ;
         T000G2_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T000G2_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         T000G2_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         T000G2_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         T000G2_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         T000G2_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         T000G2_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         T000G2_A89Contrato_Valor = new decimal[1] ;
         T000G2_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         T000G2_A90Contrato_RegrasPagto = new String[] {""} ;
         T000G2_A91Contrato_DiasPagto = new short[1] ;
         T000G2_A452Contrato_CalculoDivergencia = new String[] {""} ;
         T000G2_A453Contrato_IndiceDivergencia = new decimal[1] ;
         T000G2_A1150Contrato_AceitaPFFS = new bool[] {false} ;
         T000G2_n1150Contrato_AceitaPFFS = new bool[] {false} ;
         T000G2_A92Contrato_Ativo = new bool[] {false} ;
         T000G2_A1354Contrato_PrdFtrCada = new String[] {""} ;
         T000G2_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         T000G2_A2086Contrato_LmtFtr = new decimal[1] ;
         T000G2_n2086Contrato_LmtFtr = new bool[] {false} ;
         T000G2_A1357Contrato_PrdFtrIni = new short[1] ;
         T000G2_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         T000G2_A1358Contrato_PrdFtrFim = new short[1] ;
         T000G2_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         T000G2_A39Contratada_Codigo = new int[1] ;
         T000G2_A75Contrato_AreaTrabalhoCod = new int[1] ;
         T000G2_A1013Contrato_PrepostoCod = new int[1] ;
         T000G2_n1013Contrato_PrepostoCod = new bool[] {false} ;
         T000G34_A74Contrato_Codigo = new int[1] ;
         T000G34_n74Contrato_Codigo = new bool[] {false} ;
         T000G3_n115Contrato_UnidadeContratacao = new bool[] {false} ;
         T000G39_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         T000G39_n842Contrato_DataInicioTA = new bool[] {false} ;
         T000G42_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         T000G42_n843Contrato_DataFimTA = new bool[] {false} ;
         T000G43_A76Contrato_AreaTrabalhoDes = new String[] {""} ;
         T000G43_n76Contrato_AreaTrabalhoDes = new bool[] {false} ;
         T000G44_A438Contratada_Sigla = new String[] {""} ;
         T000G44_A516Contratada_TipoFabrica = new String[] {""} ;
         T000G44_A40Contratada_PessoaCod = new int[1] ;
         T000G45_A41Contratada_PessoaNom = new String[] {""} ;
         T000G45_n41Contratada_PessoaNom = new bool[] {false} ;
         T000G45_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000G45_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000G46_A1016Contrato_PrepostoPesCod = new int[1] ;
         T000G46_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         T000G47_A1015Contrato_PrepostoNom = new String[] {""} ;
         T000G47_n1015Contrato_PrepostoNom = new bool[] {false} ;
         T000G48_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         T000G48_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         T000G49_A1725ContratoSistemas_CntCod = new int[1] ;
         T000G49_A1726ContratoSistemas_SistemaCod = new int[1] ;
         T000G50_A1207ContratoUnidades_ContratoCod = new int[1] ;
         T000G50_A1204ContratoUnidades_UndMedCod = new int[1] ;
         T000G51_A1078ContratoGestor_ContratoCod = new int[1] ;
         T000G51_A1079ContratoGestor_UsuarioCod = new int[1] ;
         T000G52_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         T000G53_A1561SaldoContrato_Codigo = new int[1] ;
         T000G54_A321ContratoObrigacao_Codigo = new int[1] ;
         T000G55_A315ContratoTermoAditivo_Codigo = new int[1] ;
         T000G56_A314ContratoDadosCertame_Codigo = new int[1] ;
         T000G57_A294ContratoOcorrencia_Codigo = new int[1] ;
         T000G58_A160ContratoServicos_Codigo = new int[1] ;
         T000G59_A152ContratoClausulas_Codigo = new int[1] ;
         T000G60_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         T000G61_A101ContratoGarantia_Codigo = new int[1] ;
         T000G62_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         T000G62_A74Contrato_Codigo = new int[1] ;
         T000G62_n74Contrato_Codigo = new bool[] {false} ;
         T000G63_A74Contrato_Codigo = new int[1] ;
         T000G63_n74Contrato_Codigo = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i452Contrato_CalculoDivergencia = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T000G64_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         T000G64_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T000G64_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         T000G64_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         T000G64_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T000G64_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         T000G64_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T000G64_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         T000G67_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         T000G67_n842Contrato_DataInicioTA = new bool[] {false} ;
         T000G70_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         T000G70_n843Contrato_DataFimTA = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T000G71_A1016Contrato_PrepostoPesCod = new int[1] ;
         T000G71_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         T000G72_A1015Contrato_PrepostoNom = new String[] {""} ;
         T000G72_n1015Contrato_PrepostoNom = new bool[] {false} ;
         T000G73_A76Contrato_AreaTrabalhoDes = new String[] {""} ;
         T000G73_n76Contrato_AreaTrabalhoDes = new bool[] {false} ;
         T000G74_A438Contratada_Sigla = new String[] {""} ;
         T000G74_A516Contratada_TipoFabrica = new String[] {""} ;
         T000G74_A40Contratada_PessoaCod = new int[1] ;
         T000G75_A41Contratada_PessoaNom = new String[] {""} ;
         T000G75_n41Contratada_PessoaNom = new bool[] {false} ;
         T000G75_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000G75_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000G76_A315ContratoTermoAditivo_Codigo = new int[1] ;
         T000G77_A1361ContratoTermoAditivo_VlrUntUndCnt = new decimal[1] ;
         T000G77_n1361ContratoTermoAditivo_VlrUntUndCnt = new bool[] {false} ;
         T000G78_A315ContratoTermoAditivo_Codigo = new int[1] ;
         T000G79_A1361ContratoTermoAditivo_VlrUntUndCnt = new decimal[1] ;
         T000G79_n1361ContratoTermoAditivo_VlrUntUndCnt = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contrato__default(),
            new Object[][] {
                new Object[] {
               T000G2_A74Contrato_Codigo, T000G2_A77Contrato_Numero, T000G2_A78Contrato_NumeroAta, T000G2_n78Contrato_NumeroAta, T000G2_A79Contrato_Ano, T000G2_A80Contrato_Objeto, T000G2_A115Contrato_UnidadeContratacao, T000G2_A81Contrato_Quantidade, T000G2_A82Contrato_DataVigenciaInicio, T000G2_A83Contrato_DataVigenciaTermino,
               T000G2_A84Contrato_DataPublicacaoDOU, T000G2_A85Contrato_DataAssinatura, T000G2_A86Contrato_DataPedidoReajuste, T000G2_A87Contrato_DataTerminoAta, T000G2_A88Contrato_DataFimAdaptacao, T000G2_A89Contrato_Valor, T000G2_A116Contrato_ValorUnidadeContratacao, T000G2_A90Contrato_RegrasPagto, T000G2_A91Contrato_DiasPagto, T000G2_A452Contrato_CalculoDivergencia,
               T000G2_A453Contrato_IndiceDivergencia, T000G2_A1150Contrato_AceitaPFFS, T000G2_n1150Contrato_AceitaPFFS, T000G2_A92Contrato_Ativo, T000G2_A1354Contrato_PrdFtrCada, T000G2_n1354Contrato_PrdFtrCada, T000G2_A2086Contrato_LmtFtr, T000G2_n2086Contrato_LmtFtr, T000G2_A1357Contrato_PrdFtrIni, T000G2_n1357Contrato_PrdFtrIni,
               T000G2_A1358Contrato_PrdFtrFim, T000G2_n1358Contrato_PrdFtrFim, T000G2_A39Contratada_Codigo, T000G2_A75Contrato_AreaTrabalhoCod, T000G2_A1013Contrato_PrepostoCod, T000G2_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               T000G3_A74Contrato_Codigo, T000G3_A77Contrato_Numero, T000G3_A78Contrato_NumeroAta, T000G3_n78Contrato_NumeroAta, T000G3_A79Contrato_Ano, T000G3_A80Contrato_Objeto, T000G3_A115Contrato_UnidadeContratacao, T000G3_A81Contrato_Quantidade, T000G3_A82Contrato_DataVigenciaInicio, T000G3_A83Contrato_DataVigenciaTermino,
               T000G3_A84Contrato_DataPublicacaoDOU, T000G3_A85Contrato_DataAssinatura, T000G3_A86Contrato_DataPedidoReajuste, T000G3_A87Contrato_DataTerminoAta, T000G3_A88Contrato_DataFimAdaptacao, T000G3_A89Contrato_Valor, T000G3_A116Contrato_ValorUnidadeContratacao, T000G3_A90Contrato_RegrasPagto, T000G3_A91Contrato_DiasPagto, T000G3_A452Contrato_CalculoDivergencia,
               T000G3_A453Contrato_IndiceDivergencia, T000G3_A1150Contrato_AceitaPFFS, T000G3_n1150Contrato_AceitaPFFS, T000G3_A92Contrato_Ativo, T000G3_A1354Contrato_PrdFtrCada, T000G3_n1354Contrato_PrdFtrCada, T000G3_A2086Contrato_LmtFtr, T000G3_n2086Contrato_LmtFtr, T000G3_A1357Contrato_PrdFtrIni, T000G3_n1357Contrato_PrdFtrIni,
               T000G3_A1358Contrato_PrdFtrFim, T000G3_n1358Contrato_PrdFtrFim, T000G3_A39Contratada_Codigo, T000G3_A75Contrato_AreaTrabalhoCod, T000G3_A1013Contrato_PrepostoCod, T000G3_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               T000G4_A438Contratada_Sigla, T000G4_A516Contratada_TipoFabrica, T000G4_A40Contratada_PessoaCod
               }
               , new Object[] {
               T000G5_A76Contrato_AreaTrabalhoDes, T000G5_n76Contrato_AreaTrabalhoDes
               }
               , new Object[] {
               T000G6_A1016Contrato_PrepostoPesCod, T000G6_n1016Contrato_PrepostoPesCod
               }
               , new Object[] {
               T000G7_A41Contratada_PessoaNom, T000G7_n41Contratada_PessoaNom, T000G7_A42Contratada_PessoaCNPJ, T000G7_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T000G8_A1015Contrato_PrepostoNom, T000G8_n1015Contrato_PrepostoNom
               }
               , new Object[] {
               T000G11_A842Contrato_DataInicioTA, T000G11_n842Contrato_DataInicioTA
               }
               , new Object[] {
               T000G14_A843Contrato_DataFimTA, T000G14_n843Contrato_DataFimTA
               }
               , new Object[] {
               T000G19_A74Contrato_Codigo, T000G19_A76Contrato_AreaTrabalhoDes, T000G19_n76Contrato_AreaTrabalhoDes, T000G19_A41Contratada_PessoaNom, T000G19_n41Contratada_PessoaNom, T000G19_A42Contratada_PessoaCNPJ, T000G19_n42Contratada_PessoaCNPJ, T000G19_A438Contratada_Sigla, T000G19_A516Contratada_TipoFabrica, T000G19_A77Contrato_Numero,
               T000G19_A78Contrato_NumeroAta, T000G19_n78Contrato_NumeroAta, T000G19_A79Contrato_Ano, T000G19_A80Contrato_Objeto, T000G19_A115Contrato_UnidadeContratacao, T000G19_A81Contrato_Quantidade, T000G19_A82Contrato_DataVigenciaInicio, T000G19_A83Contrato_DataVigenciaTermino, T000G19_A84Contrato_DataPublicacaoDOU, T000G19_A85Contrato_DataAssinatura,
               T000G19_A86Contrato_DataPedidoReajuste, T000G19_A87Contrato_DataTerminoAta, T000G19_A88Contrato_DataFimAdaptacao, T000G19_A89Contrato_Valor, T000G19_A116Contrato_ValorUnidadeContratacao, T000G19_A90Contrato_RegrasPagto, T000G19_A91Contrato_DiasPagto, T000G19_A452Contrato_CalculoDivergencia, T000G19_A453Contrato_IndiceDivergencia, T000G19_A1150Contrato_AceitaPFFS,
               T000G19_n1150Contrato_AceitaPFFS, T000G19_A92Contrato_Ativo, T000G19_A1015Contrato_PrepostoNom, T000G19_n1015Contrato_PrepostoNom, T000G19_A1354Contrato_PrdFtrCada, T000G19_n1354Contrato_PrdFtrCada, T000G19_A2086Contrato_LmtFtr, T000G19_n2086Contrato_LmtFtr, T000G19_A1357Contrato_PrdFtrIni, T000G19_n1357Contrato_PrdFtrIni,
               T000G19_A1358Contrato_PrdFtrFim, T000G19_n1358Contrato_PrdFtrFim, T000G19_A39Contratada_Codigo, T000G19_A75Contrato_AreaTrabalhoCod, T000G19_A1013Contrato_PrepostoCod, T000G19_n1013Contrato_PrepostoCod, T000G19_A40Contratada_PessoaCod, T000G19_A1016Contrato_PrepostoPesCod, T000G19_n1016Contrato_PrepostoPesCod, T000G19_A842Contrato_DataInicioTA,
               T000G19_n842Contrato_DataInicioTA, T000G19_A843Contrato_DataFimTA, T000G19_n843Contrato_DataFimTA
               }
               , new Object[] {
               T000G22_A842Contrato_DataInicioTA, T000G22_n842Contrato_DataInicioTA
               }
               , new Object[] {
               T000G25_A843Contrato_DataFimTA, T000G25_n843Contrato_DataFimTA
               }
               , new Object[] {
               T000G26_A76Contrato_AreaTrabalhoDes, T000G26_n76Contrato_AreaTrabalhoDes
               }
               , new Object[] {
               T000G27_A438Contratada_Sigla, T000G27_A516Contratada_TipoFabrica, T000G27_A40Contratada_PessoaCod
               }
               , new Object[] {
               T000G28_A41Contratada_PessoaNom, T000G28_n41Contratada_PessoaNom, T000G28_A42Contratada_PessoaCNPJ, T000G28_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T000G29_A1016Contrato_PrepostoPesCod, T000G29_n1016Contrato_PrepostoPesCod
               }
               , new Object[] {
               T000G30_A1015Contrato_PrepostoNom, T000G30_n1015Contrato_PrepostoNom
               }
               , new Object[] {
               T000G31_A74Contrato_Codigo
               }
               , new Object[] {
               T000G32_A74Contrato_Codigo
               }
               , new Object[] {
               T000G33_A74Contrato_Codigo
               }
               , new Object[] {
               T000G34_A74Contrato_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000G39_A842Contrato_DataInicioTA, T000G39_n842Contrato_DataInicioTA
               }
               , new Object[] {
               T000G42_A843Contrato_DataFimTA, T000G42_n843Contrato_DataFimTA
               }
               , new Object[] {
               T000G43_A76Contrato_AreaTrabalhoDes, T000G43_n76Contrato_AreaTrabalhoDes
               }
               , new Object[] {
               T000G44_A438Contratada_Sigla, T000G44_A516Contratada_TipoFabrica, T000G44_A40Contratada_PessoaCod
               }
               , new Object[] {
               T000G45_A41Contratada_PessoaNom, T000G45_n41Contratada_PessoaNom, T000G45_A42Contratada_PessoaCNPJ, T000G45_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T000G46_A1016Contrato_PrepostoPesCod, T000G46_n1016Contrato_PrepostoPesCod
               }
               , new Object[] {
               T000G47_A1015Contrato_PrepostoNom, T000G47_n1015Contrato_PrepostoNom
               }
               , new Object[] {
               T000G48_A1824ContratoAuxiliar_ContratoCod, T000G48_A1825ContratoAuxiliar_UsuarioCod
               }
               , new Object[] {
               T000G49_A1725ContratoSistemas_CntCod, T000G49_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               T000G50_A1207ContratoUnidades_ContratoCod, T000G50_A1204ContratoUnidades_UndMedCod
               }
               , new Object[] {
               T000G51_A1078ContratoGestor_ContratoCod, T000G51_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               T000G52_A1774AutorizacaoConsumo_Codigo
               }
               , new Object[] {
               T000G53_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               T000G54_A321ContratoObrigacao_Codigo
               }
               , new Object[] {
               T000G55_A315ContratoTermoAditivo_Codigo
               }
               , new Object[] {
               T000G56_A314ContratoDadosCertame_Codigo
               }
               , new Object[] {
               T000G57_A294ContratoOcorrencia_Codigo
               }
               , new Object[] {
               T000G58_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T000G59_A152ContratoClausulas_Codigo
               }
               , new Object[] {
               T000G60_A108ContratoArquivosAnexos_Codigo
               }
               , new Object[] {
               T000G61_A101ContratoGarantia_Codigo
               }
               , new Object[] {
               T000G62_A2098ContratoCaixaEntrada_Codigo, T000G62_A74Contrato_Codigo
               }
               , new Object[] {
               T000G63_A74Contrato_Codigo
               }
               , new Object[] {
               T000G64_A70ContratadaUsuario_UsuarioPessoaCod, T000G64_n70ContratadaUsuario_UsuarioPessoaCod, T000G64_A69ContratadaUsuario_UsuarioCod, T000G64_A71ContratadaUsuario_UsuarioPessoaNom, T000G64_n71ContratadaUsuario_UsuarioPessoaNom, T000G64_A66ContratadaUsuario_ContratadaCod, T000G64_A1394ContratadaUsuario_UsuarioAtivo, T000G64_n1394ContratadaUsuario_UsuarioAtivo
               }
               , new Object[] {
               T000G67_A842Contrato_DataInicioTA, T000G67_n842Contrato_DataInicioTA
               }
               , new Object[] {
               T000G70_A843Contrato_DataFimTA, T000G70_n843Contrato_DataFimTA
               }
               , new Object[] {
               T000G71_A1016Contrato_PrepostoPesCod, T000G71_n1016Contrato_PrepostoPesCod
               }
               , new Object[] {
               T000G72_A1015Contrato_PrepostoNom, T000G72_n1015Contrato_PrepostoNom
               }
               , new Object[] {
               T000G73_A76Contrato_AreaTrabalhoDes, T000G73_n76Contrato_AreaTrabalhoDes
               }
               , new Object[] {
               T000G74_A438Contratada_Sigla, T000G74_A516Contratada_TipoFabrica, T000G74_A40Contratada_PessoaCod
               }
               , new Object[] {
               T000G75_A41Contratada_PessoaNom, T000G75_n41Contratada_PessoaNom, T000G75_A42Contratada_PessoaCNPJ, T000G75_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T000G76_A315ContratoTermoAditivo_Codigo
               }
               , new Object[] {
               T000G77_A1361ContratoTermoAditivo_VlrUntUndCnt, T000G77_n1361ContratoTermoAditivo_VlrUntUndCnt
               }
               , new Object[] {
               T000G78_A315ContratoTermoAditivo_Codigo
               }
               , new Object[] {
               T000G79_A1361ContratoTermoAditivo_VlrUntUndCnt, T000G79_n1361ContratoTermoAditivo_VlrUntUndCnt
               }
            }
         );
         Z92Contrato_Ativo = true;
         A92Contrato_Ativo = true;
         i92Contrato_Ativo = true;
         Z453Contrato_IndiceDivergencia = (decimal)(10);
         A453Contrato_IndiceDivergencia = (decimal)(10);
         i453Contrato_IndiceDivergencia = (decimal)(10);
         Z452Contrato_CalculoDivergencia = "B";
         A452Contrato_CalculoDivergencia = "B";
         i452Contrato_CalculoDivergencia = "B";
         Z91Contrato_DiasPagto = 30;
         A91Contrato_DiasPagto = 30;
         i91Contrato_DiasPagto = 30;
         AV27Pgmname = "Contrato";
         Z75Contrato_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         N75Contrato_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         i75Contrato_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A75Contrato_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
      }

      private short Z79Contrato_Ano ;
      private short Z115Contrato_UnidadeContratacao ;
      private short Z91Contrato_DiasPagto ;
      private short Z1357Contrato_PrdFtrIni ;
      private short Z1358Contrato_PrdFtrFim ;
      private short GxWebError ;
      private short A115Contrato_UnidadeContratacao ;
      private short gxcookieaux ;
      private short AV25UnidadeDeMedicao ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A79Contrato_Ano ;
      private short A1357Contrato_PrdFtrIni ;
      private short A1358Contrato_PrdFtrFim ;
      private short A91Contrato_DiasPagto ;
      private short Gx_BScreen ;
      private short RcdFound17 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short i91Contrato_DiasPagto ;
      private short wbTemp ;
      private int wcpOAV7Contrato_Codigo ;
      private int wcpOAV17Contratada_Codigo ;
      private int Z74Contrato_Codigo ;
      private int Z81Contrato_Quantidade ;
      private int Z39Contratada_Codigo ;
      private int Z75Contrato_AreaTrabalhoCod ;
      private int Z1013Contrato_PrepostoCod ;
      private int O1013Contrato_PrepostoCod ;
      private int N75Contrato_AreaTrabalhoCod ;
      private int N39Contratada_Codigo ;
      private int N1013Contrato_PrepostoCod ;
      private int A74Contrato_Codigo ;
      private int AV17Contratada_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A1013Contrato_PrepostoCod ;
      private int A1016Contrato_PrepostoPesCod ;
      private int AV7Contrato_Codigo ;
      private int trnEnded ;
      private int edtContrato_AreaTrabalhoCod_Visible ;
      private int edtContrato_AreaTrabalhoCod_Enabled ;
      private int edtContrato_Codigo_Enabled ;
      private int edtContrato_Codigo_Visible ;
      private int edtContratada_Codigo_Visible ;
      private int edtContratada_Codigo_Enabled ;
      private int edtContratada_PessoaNom_Enabled ;
      private int edtContratada_PessoaCNPJ_Enabled ;
      private int edtContrato_Numero_Enabled ;
      private int edtContrato_NumeroAta_Enabled ;
      private int edtContrato_Ano_Enabled ;
      private int edtContrato_Objeto_Enabled ;
      private int edtContrato_DataVigenciaInicio_Enabled ;
      private int edtContrato_DataVigenciaTermino_Enabled ;
      private int edtContrato_DataPublicacaoDOU_Enabled ;
      private int edtContrato_DataAssinatura_Enabled ;
      private int edtContrato_DataPedidoReajuste_Enabled ;
      private int edtContrato_DataTerminoAta_Enabled ;
      private int edtContrato_DataFimAdaptacao_Enabled ;
      private int edtContrato_Valor_Enabled ;
      private int A81Contrato_Quantidade ;
      private int edtContrato_Quantidade_Enabled ;
      private int edtContrato_ValorUnidadeContratacao_Enabled ;
      private int edtContrato_PrdFtrIni_Enabled ;
      private int edtContrato_PrdFtrFim_Enabled ;
      private int edtContrato_DiasPagto_Enabled ;
      private int edtContrato_RegrasPagto_Enabled ;
      private int lblTextblockcontrato_ativo_Visible ;
      private int edtContratada_Sigla_Enabled ;
      private int edtContrato_DataInicioTA_Enabled ;
      private int edtContrato_DataFimTA_Enabled ;
      private int edtContrato_DataTermino_Enabled ;
      private int edtContrato_ValorUndCntAtual_Enabled ;
      private int edtContrato_PrepostoPesCod_Enabled ;
      private int edtContrato_PrepostoNom_Enabled ;
      private int edtContrato_Identificacao_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtContrato_IndiceDivergencia_Enabled ;
      private int edtContrato_LmtFtr_Enabled ;
      private int AV11Insert_Contrato_AreaTrabalhoCod ;
      private int AV12Insert_Contratada_Codigo ;
      private int AV16Insert_Contrato_PrepostoCod ;
      private int AV22SaldoContrato_Codigo ;
      private int AV28GXV1 ;
      private int AV29GXV2 ;
      private int Z40Contratada_PessoaCod ;
      private int Z1016Contrato_PrepostoPesCod ;
      private int i75Contrato_AreaTrabalhoCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private int X315ContratoTermoAditivo_Codigo ;
      private int E74Contrato_Codigo ;
      private decimal Z89Contrato_Valor ;
      private decimal Z116Contrato_ValorUnidadeContratacao ;
      private decimal Z453Contrato_IndiceDivergencia ;
      private decimal Z2086Contrato_LmtFtr ;
      private decimal O116Contrato_ValorUnidadeContratacao ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal A89Contrato_Valor ;
      private decimal A1870Contrato_ValorUndCntAtual ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal A2086Contrato_LmtFtr ;
      private decimal i453Contrato_IndiceDivergencia ;
      private decimal X1361ContratoTermoAditivo_VlrUntUndCnt ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z77Contrato_Numero ;
      private String Z78Contrato_NumeroAta ;
      private String Z452Contrato_CalculoDivergencia ;
      private String Z1354Contrato_PrdFtrCada ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String A516Contratada_TipoFabrica ;
      private String Gx_mode ;
      private String GXKey ;
      private String A1354Contrato_PrdFtrCada ;
      private String A452Contrato_CalculoDivergencia ;
      private String chkContrato_AceitaPFFS_Internalname ;
      private String chkContrato_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynContrato_PrepostoCod_Internalname ;
      private String TempTags ;
      private String edtContrato_AreaTrabalhoCod_Internalname ;
      private String edtContrato_AreaTrabalhoCod_Jsonclick ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtContratada_Codigo_Internalname ;
      private String edtContratada_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblUsrtable_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String lblTextblockcontrato_prepostocod_Internalname ;
      private String lblTextblockcontrato_prepostocod_Caption ;
      private String lblTextblockcontrato_prepostocod_Jsonclick ;
      private String dynContrato_PrepostoCod_Jsonclick ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String edtContrato_Numero_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontrato_numeroata_Internalname ;
      private String lblTextblockcontrato_numeroata_Jsonclick ;
      private String edtContrato_NumeroAta_Internalname ;
      private String A78Contrato_NumeroAta ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTextblockcontrato_ano_Internalname ;
      private String lblTextblockcontrato_ano_Jsonclick ;
      private String edtContrato_Ano_Internalname ;
      private String edtContrato_Ano_Jsonclick ;
      private String lblTextblockcontrato_objeto_Internalname ;
      private String lblTextblockcontrato_objeto_Jsonclick ;
      private String edtContrato_Objeto_Internalname ;
      private String lblTextblockcontrato_datavigenciainicio_Internalname ;
      private String lblTextblockcontrato_datavigenciainicio_Jsonclick ;
      private String edtContrato_DataVigenciaInicio_Internalname ;
      private String edtContrato_DataVigenciaInicio_Jsonclick ;
      private String lblTextblockcontrato_datavigenciatermino_Internalname ;
      private String lblTextblockcontrato_datavigenciatermino_Jsonclick ;
      private String edtContrato_DataVigenciaTermino_Internalname ;
      private String edtContrato_DataVigenciaTermino_Jsonclick ;
      private String lblTextblockcontrato_datapublicacaodou_Internalname ;
      private String lblTextblockcontrato_datapublicacaodou_Jsonclick ;
      private String edtContrato_DataPublicacaoDOU_Internalname ;
      private String edtContrato_DataPublicacaoDOU_Jsonclick ;
      private String lblTextblockcontrato_dataassinatura_Internalname ;
      private String lblTextblockcontrato_dataassinatura_Jsonclick ;
      private String edtContrato_DataAssinatura_Internalname ;
      private String edtContrato_DataAssinatura_Jsonclick ;
      private String lblTextblockcontrato_datapedidoreajuste_Internalname ;
      private String lblTextblockcontrato_datapedidoreajuste_Jsonclick ;
      private String edtContrato_DataPedidoReajuste_Internalname ;
      private String edtContrato_DataPedidoReajuste_Jsonclick ;
      private String lblTextblockcontrato_dataterminoata_Internalname ;
      private String lblTextblockcontrato_dataterminoata_Jsonclick ;
      private String edtContrato_DataTerminoAta_Internalname ;
      private String edtContrato_DataTerminoAta_Jsonclick ;
      private String lblTextblockcontrato_datafimadaptacao_Internalname ;
      private String lblTextblockcontrato_datafimadaptacao_Jsonclick ;
      private String edtContrato_DataFimAdaptacao_Internalname ;
      private String edtContrato_DataFimAdaptacao_Jsonclick ;
      private String lblTextblockcontrato_valor_Internalname ;
      private String lblTextblockcontrato_valor_Jsonclick ;
      private String edtContrato_Valor_Internalname ;
      private String edtContrato_Valor_Jsonclick ;
      private String lblTextblockcontrato_unidadecontratacao_Internalname ;
      private String lblTextblockcontrato_unidadecontratacao_Jsonclick ;
      private String cmbContrato_UnidadeContratacao_Internalname ;
      private String cmbContrato_UnidadeContratacao_Jsonclick ;
      private String lblTextblockcontrato_quantidade_Internalname ;
      private String lblTextblockcontrato_quantidade_Jsonclick ;
      private String edtContrato_Quantidade_Internalname ;
      private String edtContrato_Quantidade_Jsonclick ;
      private String lblTextblockcontrato_valorunidadecontratacao_Internalname ;
      private String lblTextblockcontrato_valorunidadecontratacao_Jsonclick ;
      private String edtContrato_ValorUnidadeContratacao_Internalname ;
      private String edtContrato_ValorUnidadeContratacao_Jsonclick ;
      private String lblTextblockcontrato_prdftrcada_Internalname ;
      private String lblTextblockcontrato_prdftrcada_Jsonclick ;
      private String cmbContrato_PrdFtrCada_Internalname ;
      private String cmbContrato_PrdFtrCada_Jsonclick ;
      private String lblTextblockcontrato_prdftrini_Internalname ;
      private String lblTextblockcontrato_prdftrini_Jsonclick ;
      private String edtContrato_PrdFtrIni_Internalname ;
      private String edtContrato_PrdFtrIni_Jsonclick ;
      private String lblTextblockcontrato_prdftrfim_Internalname ;
      private String lblTextblockcontrato_prdftrfim_Jsonclick ;
      private String edtContrato_PrdFtrFim_Internalname ;
      private String edtContrato_PrdFtrFim_Jsonclick ;
      private String lblTextblockcontrato_lmtftr_Internalname ;
      private String lblTextblockcontrato_lmtftr_Jsonclick ;
      private String lblTextblockcontrato_diaspagto_Internalname ;
      private String lblTextblockcontrato_diaspagto_Jsonclick ;
      private String edtContrato_DiasPagto_Internalname ;
      private String edtContrato_DiasPagto_Jsonclick ;
      private String lblTextblockcontrato_regraspagto_Internalname ;
      private String lblTextblockcontrato_regraspagto_Jsonclick ;
      private String edtContrato_RegrasPagto_Internalname ;
      private String lblTextblockcontrato_calculodivergencia_Internalname ;
      private String lblTextblockcontrato_calculodivergencia_Jsonclick ;
      private String cmbContrato_CalculoDivergencia_Internalname ;
      private String cmbContrato_CalculoDivergencia_Jsonclick ;
      private String lblTextblockcontrato_indicedivergencia_Internalname ;
      private String lblTextblockcontrato_indicedivergencia_Jsonclick ;
      private String lblTextblockcontrato_aceitapffs_Internalname ;
      private String lblTextblockcontrato_aceitapffs_Jsonclick ;
      private String lblTextblockcontrato_ativo_Internalname ;
      private String lblTextblockcontrato_ativo_Jsonclick ;
      private String lblTextblockcontratada_sigla_Internalname ;
      private String lblTextblockcontratada_sigla_Jsonclick ;
      private String edtContratada_Sigla_Internalname ;
      private String A438Contratada_Sigla ;
      private String edtContratada_Sigla_Jsonclick ;
      private String lblTextblockcontratada_tipofabrica_Internalname ;
      private String lblTextblockcontratada_tipofabrica_Jsonclick ;
      private String cmbContratada_TipoFabrica_Internalname ;
      private String cmbContratada_TipoFabrica_Jsonclick ;
      private String lblTextblockcontrato_datainiciota_Internalname ;
      private String lblTextblockcontrato_datainiciota_Jsonclick ;
      private String edtContrato_DataInicioTA_Internalname ;
      private String edtContrato_DataInicioTA_Jsonclick ;
      private String lblTextblockcontrato_datafimta_Internalname ;
      private String lblTextblockcontrato_datafimta_Jsonclick ;
      private String edtContrato_DataFimTA_Internalname ;
      private String edtContrato_DataFimTA_Jsonclick ;
      private String lblTextblockcontrato_datatermino_Internalname ;
      private String lblTextblockcontrato_datatermino_Jsonclick ;
      private String edtContrato_DataTermino_Internalname ;
      private String edtContrato_DataTermino_Jsonclick ;
      private String lblTextblockcontrato_valorundcntatual_Internalname ;
      private String lblTextblockcontrato_valorundcntatual_Jsonclick ;
      private String edtContrato_ValorUndCntAtual_Internalname ;
      private String edtContrato_ValorUndCntAtual_Jsonclick ;
      private String lblTextblockcontrato_prepostopescod_Internalname ;
      private String lblTextblockcontrato_prepostopescod_Jsonclick ;
      private String edtContrato_PrepostoPesCod_Internalname ;
      private String edtContrato_PrepostoPesCod_Jsonclick ;
      private String lblTextblockcontrato_prepostonom_Internalname ;
      private String lblTextblockcontrato_prepostonom_Jsonclick ;
      private String edtContrato_PrepostoNom_Internalname ;
      private String A1015Contrato_PrepostoNom ;
      private String edtContrato_PrepostoNom_Jsonclick ;
      private String lblTextblockcontrato_identificacao_Internalname ;
      private String lblTextblockcontrato_identificacao_Jsonclick ;
      private String edtContrato_Identificacao_Internalname ;
      private String edtContrato_Identificacao_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String tblTablemergedtrn_enter_Internalname ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablemergedcontrato_indicedivergencia_Internalname ;
      private String edtContrato_IndiceDivergencia_Internalname ;
      private String edtContrato_IndiceDivergencia_Jsonclick ;
      private String lblContrato_indicedivergencia_righttext_Internalname ;
      private String lblContrato_indicedivergencia_righttext_Jsonclick ;
      private String tblTablemergedcontrato_lmtftr_Internalname ;
      private String edtContrato_LmtFtr_Internalname ;
      private String edtContrato_LmtFtr_Jsonclick ;
      private String cmbavUnidadedemedicao_Internalname ;
      private String cmbavUnidadedemedicao_Jsonclick ;
      private String AV27Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String Confirmpanel_Width ;
      private String Confirmpanel_Height ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Icon ;
      private String Confirmpanel_Result ;
      private String Confirmpanel_Class ;
      private String Confirmpanel_Confirmtext ;
      private String Confirmpanel_Buttonyestext ;
      private String Confirmpanel_Buttonnotext ;
      private String Confirmpanel_Buttoncanceltext ;
      private String Confirmpanel_Confirmtype ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode17 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z438Contratada_Sigla ;
      private String Z516Contratada_TipoFabrica ;
      private String Z41Contratada_PessoaNom ;
      private String Z1015Contrato_PrepostoNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i452Contrato_CalculoDivergencia ;
      private String Dvpanel_tableattributes_Internalname ;
      private String Confirmpanel_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z82Contrato_DataVigenciaInicio ;
      private DateTime Z83Contrato_DataVigenciaTermino ;
      private DateTime Z84Contrato_DataPublicacaoDOU ;
      private DateTime Z85Contrato_DataAssinatura ;
      private DateTime Z86Contrato_DataPedidoReajuste ;
      private DateTime Z87Contrato_DataTerminoAta ;
      private DateTime Z88Contrato_DataFimAdaptacao ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A84Contrato_DataPublicacaoDOU ;
      private DateTime A85Contrato_DataAssinatura ;
      private DateTime A86Contrato_DataPedidoReajuste ;
      private DateTime A87Contrato_DataTerminoAta ;
      private DateTime A88Contrato_DataFimAdaptacao ;
      private DateTime A842Contrato_DataInicioTA ;
      private DateTime A843Contrato_DataFimTA ;
      private DateTime A1869Contrato_DataTermino ;
      private DateTime Z842Contrato_DataInicioTA ;
      private DateTime Z843Contrato_DataFimTA ;
      private bool Z1150Contrato_AceitaPFFS ;
      private bool Z92Contrato_Ativo ;
      private bool entryPointCalled ;
      private bool n74Contrato_Codigo ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1016Contrato_PrepostoPesCod ;
      private bool toggleJsOutput ;
      private bool n1354Contrato_PrdFtrCada ;
      private bool wbErr ;
      private bool A1150Contrato_AceitaPFFS ;
      private bool A92Contrato_Ativo ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n78Contrato_NumeroAta ;
      private bool n1357Contrato_PrdFtrIni ;
      private bool n1358Contrato_PrdFtrFim ;
      private bool n2086Contrato_LmtFtr ;
      private bool n1150Contrato_AceitaPFFS ;
      private bool n842Contrato_DataInicioTA ;
      private bool n843Contrato_DataFimTA ;
      private bool n1015Contrato_PrepostoNom ;
      private bool n76Contrato_AreaTrabalhoDes ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool Confirmpanel_Closeable ;
      private bool Confirmpanel_Modal ;
      private bool Confirmpanel_Enabled ;
      private bool Confirmpanel_Draggeable ;
      private bool Confirmpanel_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i92Contrato_Ativo ;
      private bool nA74Contrato_Codigo ;
      private bool nE74Contrato_Codigo ;
      private bool nX1361ContratoTermoAditivo_VlrUntUndCnt ;
      private String A80Contrato_Objeto ;
      private String A90Contrato_RegrasPagto ;
      private String Z80Contrato_Objeto ;
      private String Z90Contrato_RegrasPagto ;
      private String A42Contratada_PessoaCNPJ ;
      private String A2096Contrato_Identificacao ;
      private String A76Contrato_AreaTrabalhoDes ;
      private String Z76Contrato_AreaTrabalhoDes ;
      private String Z42Contratada_PessoaCNPJ ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Contratada_Codigo ;
      private GXCombobox dynContrato_PrepostoCod ;
      private GXCombobox cmbContrato_UnidadeContratacao ;
      private GXCombobox cmbContrato_PrdFtrCada ;
      private GXCombobox cmbavUnidadedemedicao ;
      private GXCombobox cmbContrato_CalculoDivergencia ;
      private GXCheckbox chkContrato_AceitaPFFS ;
      private GXCheckbox chkContrato_Ativo ;
      private GXCombobox cmbContratada_TipoFabrica ;
      private IDataStoreProvider pr_default ;
      private DateTime[] T000G11_A842Contrato_DataInicioTA ;
      private bool[] T000G11_n842Contrato_DataInicioTA ;
      private DateTime[] T000G14_A843Contrato_DataFimTA ;
      private bool[] T000G14_n843Contrato_DataFimTA ;
      private int[] T000G6_A1016Contrato_PrepostoPesCod ;
      private bool[] T000G6_n1016Contrato_PrepostoPesCod ;
      private String[] T000G8_A1015Contrato_PrepostoNom ;
      private bool[] T000G8_n1015Contrato_PrepostoNom ;
      private String[] T000G4_A438Contratada_Sigla ;
      private String[] T000G4_A516Contratada_TipoFabrica ;
      private int[] T000G4_A40Contratada_PessoaCod ;
      private String[] T000G7_A41Contratada_PessoaNom ;
      private bool[] T000G7_n41Contratada_PessoaNom ;
      private String[] T000G7_A42Contratada_PessoaCNPJ ;
      private bool[] T000G7_n42Contratada_PessoaCNPJ ;
      private String[] T000G5_A76Contrato_AreaTrabalhoDes ;
      private bool[] T000G5_n76Contrato_AreaTrabalhoDes ;
      private int[] T000G19_A74Contrato_Codigo ;
      private bool[] T000G19_n74Contrato_Codigo ;
      private String[] T000G19_A76Contrato_AreaTrabalhoDes ;
      private bool[] T000G19_n76Contrato_AreaTrabalhoDes ;
      private String[] T000G19_A41Contratada_PessoaNom ;
      private bool[] T000G19_n41Contratada_PessoaNom ;
      private String[] T000G19_A42Contratada_PessoaCNPJ ;
      private bool[] T000G19_n42Contratada_PessoaCNPJ ;
      private String[] T000G19_A438Contratada_Sigla ;
      private String[] T000G19_A516Contratada_TipoFabrica ;
      private String[] T000G19_A77Contrato_Numero ;
      private String[] T000G19_A78Contrato_NumeroAta ;
      private bool[] T000G19_n78Contrato_NumeroAta ;
      private short[] T000G19_A79Contrato_Ano ;
      private String[] T000G19_A80Contrato_Objeto ;
      private short[] T000G19_A115Contrato_UnidadeContratacao ;
      private int[] T000G19_A81Contrato_Quantidade ;
      private DateTime[] T000G19_A82Contrato_DataVigenciaInicio ;
      private DateTime[] T000G19_A83Contrato_DataVigenciaTermino ;
      private DateTime[] T000G19_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] T000G19_A85Contrato_DataAssinatura ;
      private DateTime[] T000G19_A86Contrato_DataPedidoReajuste ;
      private DateTime[] T000G19_A87Contrato_DataTerminoAta ;
      private DateTime[] T000G19_A88Contrato_DataFimAdaptacao ;
      private decimal[] T000G19_A89Contrato_Valor ;
      private decimal[] T000G19_A116Contrato_ValorUnidadeContratacao ;
      private String[] T000G19_A90Contrato_RegrasPagto ;
      private short[] T000G19_A91Contrato_DiasPagto ;
      private String[] T000G19_A452Contrato_CalculoDivergencia ;
      private decimal[] T000G19_A453Contrato_IndiceDivergencia ;
      private bool[] T000G19_A1150Contrato_AceitaPFFS ;
      private bool[] T000G19_n1150Contrato_AceitaPFFS ;
      private bool[] T000G19_A92Contrato_Ativo ;
      private String[] T000G19_A1015Contrato_PrepostoNom ;
      private bool[] T000G19_n1015Contrato_PrepostoNom ;
      private String[] T000G19_A1354Contrato_PrdFtrCada ;
      private bool[] T000G19_n1354Contrato_PrdFtrCada ;
      private decimal[] T000G19_A2086Contrato_LmtFtr ;
      private bool[] T000G19_n2086Contrato_LmtFtr ;
      private short[] T000G19_A1357Contrato_PrdFtrIni ;
      private bool[] T000G19_n1357Contrato_PrdFtrIni ;
      private short[] T000G19_A1358Contrato_PrdFtrFim ;
      private bool[] T000G19_n1358Contrato_PrdFtrFim ;
      private int[] T000G19_A39Contratada_Codigo ;
      private int[] T000G19_A75Contrato_AreaTrabalhoCod ;
      private int[] T000G19_A1013Contrato_PrepostoCod ;
      private bool[] T000G19_n1013Contrato_PrepostoCod ;
      private int[] T000G19_A40Contratada_PessoaCod ;
      private int[] T000G19_A1016Contrato_PrepostoPesCod ;
      private bool[] T000G19_n1016Contrato_PrepostoPesCod ;
      private DateTime[] T000G19_A842Contrato_DataInicioTA ;
      private bool[] T000G19_n842Contrato_DataInicioTA ;
      private DateTime[] T000G19_A843Contrato_DataFimTA ;
      private bool[] T000G19_n843Contrato_DataFimTA ;
      private DateTime[] T000G22_A842Contrato_DataInicioTA ;
      private bool[] T000G22_n842Contrato_DataInicioTA ;
      private DateTime[] T000G25_A843Contrato_DataFimTA ;
      private bool[] T000G25_n843Contrato_DataFimTA ;
      private String[] T000G26_A76Contrato_AreaTrabalhoDes ;
      private bool[] T000G26_n76Contrato_AreaTrabalhoDes ;
      private String[] T000G27_A438Contratada_Sigla ;
      private String[] T000G27_A516Contratada_TipoFabrica ;
      private int[] T000G27_A40Contratada_PessoaCod ;
      private String[] T000G28_A41Contratada_PessoaNom ;
      private bool[] T000G28_n41Contratada_PessoaNom ;
      private String[] T000G28_A42Contratada_PessoaCNPJ ;
      private bool[] T000G28_n42Contratada_PessoaCNPJ ;
      private int[] T000G29_A1016Contrato_PrepostoPesCod ;
      private bool[] T000G29_n1016Contrato_PrepostoPesCod ;
      private String[] T000G30_A1015Contrato_PrepostoNom ;
      private bool[] T000G30_n1015Contrato_PrepostoNom ;
      private int[] T000G31_A74Contrato_Codigo ;
      private bool[] T000G31_n74Contrato_Codigo ;
      private int[] T000G3_A74Contrato_Codigo ;
      private bool[] T000G3_n74Contrato_Codigo ;
      private String[] T000G3_A77Contrato_Numero ;
      private String[] T000G3_A78Contrato_NumeroAta ;
      private bool[] T000G3_n78Contrato_NumeroAta ;
      private short[] T000G3_A79Contrato_Ano ;
      private String[] T000G3_A80Contrato_Objeto ;
      private short[] T000G3_A115Contrato_UnidadeContratacao ;
      private int[] T000G3_A81Contrato_Quantidade ;
      private DateTime[] T000G3_A82Contrato_DataVigenciaInicio ;
      private DateTime[] T000G3_A83Contrato_DataVigenciaTermino ;
      private DateTime[] T000G3_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] T000G3_A85Contrato_DataAssinatura ;
      private DateTime[] T000G3_A86Contrato_DataPedidoReajuste ;
      private DateTime[] T000G3_A87Contrato_DataTerminoAta ;
      private DateTime[] T000G3_A88Contrato_DataFimAdaptacao ;
      private decimal[] T000G3_A89Contrato_Valor ;
      private decimal[] T000G3_A116Contrato_ValorUnidadeContratacao ;
      private String[] T000G3_A90Contrato_RegrasPagto ;
      private short[] T000G3_A91Contrato_DiasPagto ;
      private String[] T000G3_A452Contrato_CalculoDivergencia ;
      private decimal[] T000G3_A453Contrato_IndiceDivergencia ;
      private bool[] T000G3_A1150Contrato_AceitaPFFS ;
      private bool[] T000G3_n1150Contrato_AceitaPFFS ;
      private bool[] T000G3_A92Contrato_Ativo ;
      private String[] T000G3_A1354Contrato_PrdFtrCada ;
      private bool[] T000G3_n1354Contrato_PrdFtrCada ;
      private decimal[] T000G3_A2086Contrato_LmtFtr ;
      private bool[] T000G3_n2086Contrato_LmtFtr ;
      private short[] T000G3_A1357Contrato_PrdFtrIni ;
      private bool[] T000G3_n1357Contrato_PrdFtrIni ;
      private short[] T000G3_A1358Contrato_PrdFtrFim ;
      private bool[] T000G3_n1358Contrato_PrdFtrFim ;
      private int[] T000G3_A39Contratada_Codigo ;
      private int[] T000G3_A75Contrato_AreaTrabalhoCod ;
      private int[] T000G3_A1013Contrato_PrepostoCod ;
      private bool[] T000G3_n1013Contrato_PrepostoCod ;
      private int[] T000G32_A74Contrato_Codigo ;
      private bool[] T000G32_n74Contrato_Codigo ;
      private int[] T000G33_A74Contrato_Codigo ;
      private bool[] T000G33_n74Contrato_Codigo ;
      private int[] T000G2_A74Contrato_Codigo ;
      private bool[] T000G2_n74Contrato_Codigo ;
      private String[] T000G2_A77Contrato_Numero ;
      private String[] T000G2_A78Contrato_NumeroAta ;
      private bool[] T000G2_n78Contrato_NumeroAta ;
      private short[] T000G2_A79Contrato_Ano ;
      private String[] T000G2_A80Contrato_Objeto ;
      private short[] T000G2_A115Contrato_UnidadeContratacao ;
      private int[] T000G2_A81Contrato_Quantidade ;
      private DateTime[] T000G2_A82Contrato_DataVigenciaInicio ;
      private DateTime[] T000G2_A83Contrato_DataVigenciaTermino ;
      private DateTime[] T000G2_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] T000G2_A85Contrato_DataAssinatura ;
      private DateTime[] T000G2_A86Contrato_DataPedidoReajuste ;
      private DateTime[] T000G2_A87Contrato_DataTerminoAta ;
      private DateTime[] T000G2_A88Contrato_DataFimAdaptacao ;
      private decimal[] T000G2_A89Contrato_Valor ;
      private decimal[] T000G2_A116Contrato_ValorUnidadeContratacao ;
      private String[] T000G2_A90Contrato_RegrasPagto ;
      private short[] T000G2_A91Contrato_DiasPagto ;
      private String[] T000G2_A452Contrato_CalculoDivergencia ;
      private decimal[] T000G2_A453Contrato_IndiceDivergencia ;
      private bool[] T000G2_A1150Contrato_AceitaPFFS ;
      private bool[] T000G2_n1150Contrato_AceitaPFFS ;
      private bool[] T000G2_A92Contrato_Ativo ;
      private String[] T000G2_A1354Contrato_PrdFtrCada ;
      private bool[] T000G2_n1354Contrato_PrdFtrCada ;
      private decimal[] T000G2_A2086Contrato_LmtFtr ;
      private bool[] T000G2_n2086Contrato_LmtFtr ;
      private short[] T000G2_A1357Contrato_PrdFtrIni ;
      private bool[] T000G2_n1357Contrato_PrdFtrIni ;
      private short[] T000G2_A1358Contrato_PrdFtrFim ;
      private bool[] T000G2_n1358Contrato_PrdFtrFim ;
      private int[] T000G2_A39Contratada_Codigo ;
      private int[] T000G2_A75Contrato_AreaTrabalhoCod ;
      private int[] T000G2_A1013Contrato_PrepostoCod ;
      private bool[] T000G2_n1013Contrato_PrepostoCod ;
      private int[] T000G34_A74Contrato_Codigo ;
      private bool[] T000G34_n74Contrato_Codigo ;
      private bool[] T000G3_n115Contrato_UnidadeContratacao ;
      private DateTime[] T000G39_A842Contrato_DataInicioTA ;
      private bool[] T000G39_n842Contrato_DataInicioTA ;
      private DateTime[] T000G42_A843Contrato_DataFimTA ;
      private bool[] T000G42_n843Contrato_DataFimTA ;
      private String[] T000G43_A76Contrato_AreaTrabalhoDes ;
      private bool[] T000G43_n76Contrato_AreaTrabalhoDes ;
      private String[] T000G44_A438Contratada_Sigla ;
      private String[] T000G44_A516Contratada_TipoFabrica ;
      private int[] T000G44_A40Contratada_PessoaCod ;
      private String[] T000G45_A41Contratada_PessoaNom ;
      private bool[] T000G45_n41Contratada_PessoaNom ;
      private String[] T000G45_A42Contratada_PessoaCNPJ ;
      private bool[] T000G45_n42Contratada_PessoaCNPJ ;
      private int[] T000G46_A1016Contrato_PrepostoPesCod ;
      private bool[] T000G46_n1016Contrato_PrepostoPesCod ;
      private String[] T000G47_A1015Contrato_PrepostoNom ;
      private bool[] T000G47_n1015Contrato_PrepostoNom ;
      private int[] T000G48_A1824ContratoAuxiliar_ContratoCod ;
      private int[] T000G48_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] T000G49_A1725ContratoSistemas_CntCod ;
      private int[] T000G49_A1726ContratoSistemas_SistemaCod ;
      private int[] T000G50_A1207ContratoUnidades_ContratoCod ;
      private int[] T000G50_A1204ContratoUnidades_UndMedCod ;
      private int[] T000G51_A1078ContratoGestor_ContratoCod ;
      private int[] T000G51_A1079ContratoGestor_UsuarioCod ;
      private int[] T000G52_A1774AutorizacaoConsumo_Codigo ;
      private int[] T000G53_A1561SaldoContrato_Codigo ;
      private int[] T000G54_A321ContratoObrigacao_Codigo ;
      private int[] T000G55_A315ContratoTermoAditivo_Codigo ;
      private int[] T000G56_A314ContratoDadosCertame_Codigo ;
      private int[] T000G57_A294ContratoOcorrencia_Codigo ;
      private int[] T000G58_A160ContratoServicos_Codigo ;
      private int[] T000G59_A152ContratoClausulas_Codigo ;
      private int[] T000G60_A108ContratoArquivosAnexos_Codigo ;
      private int[] T000G61_A101ContratoGarantia_Codigo ;
      private int[] T000G62_A2098ContratoCaixaEntrada_Codigo ;
      private int[] T000G62_A74Contrato_Codigo ;
      private bool[] T000G62_n74Contrato_Codigo ;
      private int[] T000G63_A74Contrato_Codigo ;
      private bool[] T000G63_n74Contrato_Codigo ;
      private int[] T000G64_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] T000G64_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] T000G64_A69ContratadaUsuario_UsuarioCod ;
      private String[] T000G64_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] T000G64_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] T000G64_A66ContratadaUsuario_ContratadaCod ;
      private bool[] T000G64_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] T000G64_n1394ContratadaUsuario_UsuarioAtivo ;
      private DateTime[] T000G67_A842Contrato_DataInicioTA ;
      private bool[] T000G67_n842Contrato_DataInicioTA ;
      private DateTime[] T000G70_A843Contrato_DataFimTA ;
      private bool[] T000G70_n843Contrato_DataFimTA ;
      private int[] T000G71_A1016Contrato_PrepostoPesCod ;
      private bool[] T000G71_n1016Contrato_PrepostoPesCod ;
      private String[] T000G72_A1015Contrato_PrepostoNom ;
      private bool[] T000G72_n1015Contrato_PrepostoNom ;
      private String[] T000G73_A76Contrato_AreaTrabalhoDes ;
      private bool[] T000G73_n76Contrato_AreaTrabalhoDes ;
      private String[] T000G74_A438Contratada_Sigla ;
      private String[] T000G74_A516Contratada_TipoFabrica ;
      private int[] T000G74_A40Contratada_PessoaCod ;
      private String[] T000G75_A41Contratada_PessoaNom ;
      private bool[] T000G75_n41Contratada_PessoaNom ;
      private String[] T000G75_A42Contratada_PessoaCNPJ ;
      private bool[] T000G75_n42Contratada_PessoaCNPJ ;
      private int[] T000G76_A315ContratoTermoAditivo_Codigo ;
      private decimal[] T000G77_A1361ContratoTermoAditivo_VlrUntUndCnt ;
      private bool[] T000G77_n1361ContratoTermoAditivo_VlrUntUndCnt ;
      private int[] T000G78_A315ContratoTermoAditivo_Codigo ;
      private decimal[] T000G79_A1361ContratoTermoAditivo_VlrUntUndCnt ;
      private bool[] T000G79_n1361ContratoTermoAditivo_VlrUntUndCnt ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtAuditingObject AV24AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class contrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new UpdateCursor(def[21])
         ,new UpdateCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new ForEachCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
         ,new ForEachCursor(def[48])
         ,new ForEachCursor(def[49])
         ,new ForEachCursor(def[50])
         ,new ForEachCursor(def[51])
         ,new ForEachCursor(def[52])
         ,new ForEachCursor(def[53])
         ,new ForEachCursor(def[54])
         ,new ForEachCursor(def[55])
         ,new ForEachCursor(def[56])
         ,new ForEachCursor(def[57])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000G19 ;
          prmT000G19 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT000G19 ;
          cmdBufferT000G19=" SELECT TM1.[Contrato_Codigo], T4.[AreaTrabalho_Descricao] AS Contrato_AreaTrabalhoDes, T6.[Pessoa_Nome] AS Contratada_PessoaNom, T6.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T5.[Contratada_Sigla], T5.[Contratada_TipoFabrica], TM1.[Contrato_Numero], TM1.[Contrato_NumeroAta], TM1.[Contrato_Ano], TM1.[Contrato_Objeto], TM1.[Contrato_UnidadeContratacao], TM1.[Contrato_Quantidade], TM1.[Contrato_DataVigenciaInicio], TM1.[Contrato_DataVigenciaTermino], TM1.[Contrato_DataPublicacaoDOU], TM1.[Contrato_DataAssinatura], TM1.[Contrato_DataPedidoReajuste], TM1.[Contrato_DataTerminoAta], TM1.[Contrato_DataFimAdaptacao], TM1.[Contrato_Valor], TM1.[Contrato_ValorUnidadeContratacao], TM1.[Contrato_RegrasPagto], TM1.[Contrato_DiasPagto], TM1.[Contrato_CalculoDivergencia], TM1.[Contrato_IndiceDivergencia], TM1.[Contrato_AceitaPFFS], TM1.[Contrato_Ativo], T8.[Pessoa_Nome] AS Contrato_PrepostoNom, TM1.[Contrato_PrdFtrCada], TM1.[Contrato_LmtFtr], TM1.[Contrato_PrdFtrIni], TM1.[Contrato_PrdFtrFim], TM1.[Contratada_Codigo], TM1.[Contrato_AreaTrabalhoCod] AS Contrato_AreaTrabalhoCod, TM1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T5.[Contratada_PessoaCod] AS Contratada_PessoaCod, T7.[Usuario_PessoaCod] AS Contrato_PrepostoPesCod, COALESCE( T2.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA, COALESCE( T3.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM ((((((([Contrato] TM1 WITH (NOLOCK) LEFT JOIN (SELECT T9.[ContratoTermoAditivo_DataInicio], T9.[Contrato_Codigo], T9.[ContratoTermoAditivo_Codigo], T10.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T9 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] "
          + " ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) WHERE T9.[ContratoTermoAditivo_Codigo] = T10.[GXC1] ) T2 ON T2.[Contrato_Codigo] = TM1.[Contrato_Codigo]) LEFT JOIN (SELECT T9.[ContratoTermoAditivo_DataFim], T9.[Contrato_Codigo], T9.[ContratoTermoAditivo_Codigo], T10.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T9 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) WHERE T9.[ContratoTermoAditivo_Codigo] = T10.[GXC1] ) T3 ON T3.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = TM1.[Contrato_AreaTrabalhoCod]) INNER JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = TM1.[Contratada_Codigo]) INNER JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Contratada_PessoaCod]) LEFT JOIN [Usuario] T7 WITH (NOLOCK) ON T7.[Usuario_Codigo] = TM1.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T8 WITH (NOLOCK) ON T8.[Pessoa_Codigo] = T7.[Usuario_PessoaCod]) WHERE TM1.[Contrato_Codigo] = @Contrato_Codigo ORDER BY TM1.[Contrato_Codigo]  OPTION (FAST 100)" ;
          Object[] prmT000G11 ;
          prmT000G11 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G14 ;
          prmT000G14 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G5 ;
          prmT000G5 = new Object[] {
          new Object[] {"@Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G4 ;
          prmT000G4 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G7 ;
          prmT000G7 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G6 ;
          prmT000G6 = new Object[] {
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G8 ;
          prmT000G8 = new Object[] {
          new Object[] {"@Contrato_PrepostoPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G22 ;
          prmT000G22 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G25 ;
          prmT000G25 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G26 ;
          prmT000G26 = new Object[] {
          new Object[] {"@Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G27 ;
          prmT000G27 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G28 ;
          prmT000G28 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G29 ;
          prmT000G29 = new Object[] {
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G30 ;
          prmT000G30 = new Object[] {
          new Object[] {"@Contrato_PrepostoPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G31 ;
          prmT000G31 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G3 ;
          prmT000G3 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G32 ;
          prmT000G32 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G33 ;
          prmT000G33 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G2 ;
          prmT000G2 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G34 ;
          prmT000G34 = new Object[] {
          new Object[] {"@Contrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@Contrato_NumeroAta",SqlDbType.Char,10,0} ,
          new Object[] {"@Contrato_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_Objeto",SqlDbType.VarChar,10000,0} ,
          new Object[] {"@Contrato_UnidadeContratacao",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Contrato_Quantidade",SqlDbType.Int,9,0} ,
          new Object[] {"@Contrato_DataVigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataVigenciaTermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataPublicacaoDOU",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataAssinatura",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataPedidoReajuste",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataTerminoAta",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataFimAdaptacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contrato_ValorUnidadeContratacao",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contrato_RegrasPagto",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Contrato_DiasPagto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_CalculoDivergencia",SqlDbType.Char,1,0} ,
          new Object[] {"@Contrato_IndiceDivergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contrato_AceitaPFFS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_PrdFtrCada",SqlDbType.Char,1,0} ,
          new Object[] {"@Contrato_LmtFtr",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contrato_PrdFtrIni",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_PrdFtrFim",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G35 ;
          prmT000G35 = new Object[] {
          new Object[] {"@Contrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@Contrato_NumeroAta",SqlDbType.Char,10,0} ,
          new Object[] {"@Contrato_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_Objeto",SqlDbType.VarChar,10000,0} ,
          new Object[] {"@Contrato_UnidadeContratacao",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Contrato_Quantidade",SqlDbType.Int,9,0} ,
          new Object[] {"@Contrato_DataVigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataVigenciaTermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataPublicacaoDOU",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataAssinatura",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataPedidoReajuste",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataTerminoAta",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataFimAdaptacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contrato_ValorUnidadeContratacao",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contrato_RegrasPagto",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Contrato_DiasPagto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_CalculoDivergencia",SqlDbType.Char,1,0} ,
          new Object[] {"@Contrato_IndiceDivergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contrato_AceitaPFFS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_PrdFtrCada",SqlDbType.Char,1,0} ,
          new Object[] {"@Contrato_LmtFtr",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contrato_PrdFtrIni",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_PrdFtrFim",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G36 ;
          prmT000G36 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G39 ;
          prmT000G39 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G42 ;
          prmT000G42 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G43 ;
          prmT000G43 = new Object[] {
          new Object[] {"@Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G44 ;
          prmT000G44 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G45 ;
          prmT000G45 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G46 ;
          prmT000G46 = new Object[] {
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G47 ;
          prmT000G47 = new Object[] {
          new Object[] {"@Contrato_PrepostoPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G48 ;
          prmT000G48 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G49 ;
          prmT000G49 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G50 ;
          prmT000G50 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G51 ;
          prmT000G51 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G52 ;
          prmT000G52 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G53 ;
          prmT000G53 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G54 ;
          prmT000G54 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G55 ;
          prmT000G55 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G56 ;
          prmT000G56 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G57 ;
          prmT000G57 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G58 ;
          prmT000G58 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G59 ;
          prmT000G59 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G60 ;
          prmT000G60 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G61 ;
          prmT000G61 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G62 ;
          prmT000G62 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G63 ;
          prmT000G63 = new Object[] {
          } ;
          Object[] prmT000G64 ;
          prmT000G64 = new Object[] {
          new Object[] {"@AV17Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G67 ;
          prmT000G67 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G70 ;
          prmT000G70 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G71 ;
          prmT000G71 = new Object[] {
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G72 ;
          prmT000G72 = new Object[] {
          new Object[] {"@Contrato_PrepostoPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G73 ;
          prmT000G73 = new Object[] {
          new Object[] {"@Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G74 ;
          prmT000G74 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G75 ;
          prmT000G75 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G76 ;
          prmT000G76 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G77 ;
          prmT000G77 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoTermoAditivo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G78 ;
          prmT000G78 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000G79 ;
          prmT000G79 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoTermoAditivo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000G2", "SELECT [Contrato_Codigo], [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contrato_Objeto], [Contrato_UnidadeContratacao], [Contrato_Quantidade], [Contrato_DataVigenciaInicio], [Contrato_DataVigenciaTermino], [Contrato_DataPublicacaoDOU], [Contrato_DataAssinatura], [Contrato_DataPedidoReajuste], [Contrato_DataTerminoAta], [Contrato_DataFimAdaptacao], [Contrato_Valor], [Contrato_ValorUnidadeContratacao], [Contrato_RegrasPagto], [Contrato_DiasPagto], [Contrato_CalculoDivergencia], [Contrato_IndiceDivergencia], [Contrato_AceitaPFFS], [Contrato_Ativo], [Contrato_PrdFtrCada], [Contrato_LmtFtr], [Contrato_PrdFtrIni], [Contrato_PrdFtrFim], [Contratada_Codigo], [Contrato_AreaTrabalhoCod] AS Contrato_AreaTrabalhoCod, [Contrato_PrepostoCod] AS Contrato_PrepostoCod FROM [Contrato] WITH (UPDLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G2,1,0,true,false )
             ,new CursorDef("T000G3", "SELECT [Contrato_Codigo], [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contrato_Objeto], [Contrato_UnidadeContratacao], [Contrato_Quantidade], [Contrato_DataVigenciaInicio], [Contrato_DataVigenciaTermino], [Contrato_DataPublicacaoDOU], [Contrato_DataAssinatura], [Contrato_DataPedidoReajuste], [Contrato_DataTerminoAta], [Contrato_DataFimAdaptacao], [Contrato_Valor], [Contrato_ValorUnidadeContratacao], [Contrato_RegrasPagto], [Contrato_DiasPagto], [Contrato_CalculoDivergencia], [Contrato_IndiceDivergencia], [Contrato_AceitaPFFS], [Contrato_Ativo], [Contrato_PrdFtrCada], [Contrato_LmtFtr], [Contrato_PrdFtrIni], [Contrato_PrdFtrFim], [Contratada_Codigo], [Contrato_AreaTrabalhoCod] AS Contrato_AreaTrabalhoCod, [Contrato_PrepostoCod] AS Contrato_PrepostoCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G3,1,0,true,false )
             ,new CursorDef("T000G4", "SELECT [Contratada_Sigla], [Contratada_TipoFabrica], [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G4,1,0,true,false )
             ,new CursorDef("T000G5", "SELECT [AreaTrabalho_Descricao] AS Contrato_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contrato_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G5,1,0,true,false )
             ,new CursorDef("T000G6", "SELECT [Usuario_PessoaCod] AS Contrato_PrepostoPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Contrato_PrepostoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G6,1,0,true,false )
             ,new CursorDef("T000G7", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G7,1,0,true,false )
             ,new CursorDef("T000G8", "SELECT [Pessoa_Nome] AS Contrato_PrepostoNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contrato_PrepostoPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G8,1,0,true,false )
             ,new CursorDef("T000G11", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA FROM (SELECT T2.[ContratoTermoAditivo_DataInicio], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC1] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G11,1,0,true,false )
             ,new CursorDef("T000G14", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (SELECT T2.[ContratoTermoAditivo_DataFim], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC1] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G14,1,0,true,false )
             ,new CursorDef("T000G19", cmdBufferT000G19,true, GxErrorMask.GX_NOMASK, false, this,prmT000G19,100,0,true,false )
             ,new CursorDef("T000G22", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA FROM (SELECT T2.[ContratoTermoAditivo_DataInicio], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC1] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G22,1,0,true,false )
             ,new CursorDef("T000G25", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (SELECT T2.[ContratoTermoAditivo_DataFim], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC1] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G25,1,0,true,false )
             ,new CursorDef("T000G26", "SELECT [AreaTrabalho_Descricao] AS Contrato_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contrato_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G26,1,0,true,false )
             ,new CursorDef("T000G27", "SELECT [Contratada_Sigla], [Contratada_TipoFabrica], [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G27,1,0,true,false )
             ,new CursorDef("T000G28", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G28,1,0,true,false )
             ,new CursorDef("T000G29", "SELECT [Usuario_PessoaCod] AS Contrato_PrepostoPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Contrato_PrepostoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G29,1,0,true,false )
             ,new CursorDef("T000G30", "SELECT [Pessoa_Nome] AS Contrato_PrepostoNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contrato_PrepostoPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G30,1,0,true,false )
             ,new CursorDef("T000G31", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000G31,1,0,true,false )
             ,new CursorDef("T000G32", "SELECT TOP 1 [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE ( [Contrato_Codigo] > @Contrato_Codigo) ORDER BY [Contrato_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000G32,1,0,true,true )
             ,new CursorDef("T000G33", "SELECT TOP 1 [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE ( [Contrato_Codigo] < @Contrato_Codigo) ORDER BY [Contrato_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000G33,1,0,true,true )
             ,new CursorDef("T000G34", "INSERT INTO [Contrato]([Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contrato_Objeto], [Contrato_UnidadeContratacao], [Contrato_Quantidade], [Contrato_DataVigenciaInicio], [Contrato_DataVigenciaTermino], [Contrato_DataPublicacaoDOU], [Contrato_DataAssinatura], [Contrato_DataPedidoReajuste], [Contrato_DataTerminoAta], [Contrato_DataFimAdaptacao], [Contrato_Valor], [Contrato_ValorUnidadeContratacao], [Contrato_RegrasPagto], [Contrato_DiasPagto], [Contrato_CalculoDivergencia], [Contrato_IndiceDivergencia], [Contrato_AceitaPFFS], [Contrato_Ativo], [Contrato_PrdFtrCada], [Contrato_LmtFtr], [Contrato_PrdFtrIni], [Contrato_PrdFtrFim], [Contratada_Codigo], [Contrato_AreaTrabalhoCod], [Contrato_PrepostoCod]) VALUES(@Contrato_Numero, @Contrato_NumeroAta, @Contrato_Ano, @Contrato_Objeto, @Contrato_UnidadeContratacao, @Contrato_Quantidade, @Contrato_DataVigenciaInicio, @Contrato_DataVigenciaTermino, @Contrato_DataPublicacaoDOU, @Contrato_DataAssinatura, @Contrato_DataPedidoReajuste, @Contrato_DataTerminoAta, @Contrato_DataFimAdaptacao, @Contrato_Valor, @Contrato_ValorUnidadeContratacao, @Contrato_RegrasPagto, @Contrato_DiasPagto, @Contrato_CalculoDivergencia, @Contrato_IndiceDivergencia, @Contrato_AceitaPFFS, @Contrato_Ativo, @Contrato_PrdFtrCada, @Contrato_LmtFtr, @Contrato_PrdFtrIni, @Contrato_PrdFtrFim, @Contratada_Codigo, @Contrato_AreaTrabalhoCod, @Contrato_PrepostoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000G34)
             ,new CursorDef("T000G35", "UPDATE [Contrato] SET [Contrato_Numero]=@Contrato_Numero, [Contrato_NumeroAta]=@Contrato_NumeroAta, [Contrato_Ano]=@Contrato_Ano, [Contrato_Objeto]=@Contrato_Objeto, [Contrato_UnidadeContratacao]=@Contrato_UnidadeContratacao, [Contrato_Quantidade]=@Contrato_Quantidade, [Contrato_DataVigenciaInicio]=@Contrato_DataVigenciaInicio, [Contrato_DataVigenciaTermino]=@Contrato_DataVigenciaTermino, [Contrato_DataPublicacaoDOU]=@Contrato_DataPublicacaoDOU, [Contrato_DataAssinatura]=@Contrato_DataAssinatura, [Contrato_DataPedidoReajuste]=@Contrato_DataPedidoReajuste, [Contrato_DataTerminoAta]=@Contrato_DataTerminoAta, [Contrato_DataFimAdaptacao]=@Contrato_DataFimAdaptacao, [Contrato_Valor]=@Contrato_Valor, [Contrato_ValorUnidadeContratacao]=@Contrato_ValorUnidadeContratacao, [Contrato_RegrasPagto]=@Contrato_RegrasPagto, [Contrato_DiasPagto]=@Contrato_DiasPagto, [Contrato_CalculoDivergencia]=@Contrato_CalculoDivergencia, [Contrato_IndiceDivergencia]=@Contrato_IndiceDivergencia, [Contrato_AceitaPFFS]=@Contrato_AceitaPFFS, [Contrato_Ativo]=@Contrato_Ativo, [Contrato_PrdFtrCada]=@Contrato_PrdFtrCada, [Contrato_LmtFtr]=@Contrato_LmtFtr, [Contrato_PrdFtrIni]=@Contrato_PrdFtrIni, [Contrato_PrdFtrFim]=@Contrato_PrdFtrFim, [Contratada_Codigo]=@Contratada_Codigo, [Contrato_AreaTrabalhoCod]=@Contrato_AreaTrabalhoCod, [Contrato_PrepostoCod]=@Contrato_PrepostoCod  WHERE [Contrato_Codigo] = @Contrato_Codigo", GxErrorMask.GX_NOMASK,prmT000G35)
             ,new CursorDef("T000G36", "DELETE FROM [Contrato]  WHERE [Contrato_Codigo] = @Contrato_Codigo", GxErrorMask.GX_NOMASK,prmT000G36)
             ,new CursorDef("T000G39", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA FROM (SELECT T2.[ContratoTermoAditivo_DataInicio], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC1] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G39,1,0,true,false )
             ,new CursorDef("T000G42", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (SELECT T2.[ContratoTermoAditivo_DataFim], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC1] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G42,1,0,true,false )
             ,new CursorDef("T000G43", "SELECT [AreaTrabalho_Descricao] AS Contrato_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contrato_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G43,1,0,true,false )
             ,new CursorDef("T000G44", "SELECT [Contratada_Sigla], [Contratada_TipoFabrica], [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G44,1,0,true,false )
             ,new CursorDef("T000G45", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G45,1,0,true,false )
             ,new CursorDef("T000G46", "SELECT [Usuario_PessoaCod] AS Contrato_PrepostoPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Contrato_PrepostoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G46,1,0,true,false )
             ,new CursorDef("T000G47", "SELECT [Pessoa_Nome] AS Contrato_PrepostoNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contrato_PrepostoPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G47,1,0,true,false )
             ,new CursorDef("T000G48", "SELECT TOP 1 [ContratoAuxiliar_ContratoCod], [ContratoAuxiliar_UsuarioCod] FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_ContratoCod] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G48,1,0,true,true )
             ,new CursorDef("T000G49", "SELECT TOP 1 [ContratoSistemas_CntCod], [ContratoSistemas_SistemaCod] FROM [ContratoSistemas] WITH (NOLOCK) WHERE [ContratoSistemas_CntCod] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G49,1,0,true,true )
             ,new CursorDef("T000G50", "SELECT TOP 1 [ContratoUnidades_ContratoCod], [ContratoUnidades_UndMedCod] FROM [ContratoUnidades] WITH (NOLOCK) WHERE [ContratoUnidades_ContratoCod] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G50,1,0,true,true )
             ,new CursorDef("T000G51", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G51,1,0,true,true )
             ,new CursorDef("T000G52", "SELECT TOP 1 [AutorizacaoConsumo_Codigo] FROM [AutorizacaoConsumo] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G52,1,0,true,true )
             ,new CursorDef("T000G53", "SELECT TOP 1 [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G53,1,0,true,true )
             ,new CursorDef("T000G54", "SELECT TOP 1 [ContratoObrigacao_Codigo] FROM [ContratoObrigacao] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G54,1,0,true,true )
             ,new CursorDef("T000G55", "SELECT TOP 1 [ContratoTermoAditivo_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G55,1,0,true,true )
             ,new CursorDef("T000G56", "SELECT TOP 1 [ContratoDadosCertame_Codigo] FROM [ContratoDadosCertame] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G56,1,0,true,true )
             ,new CursorDef("T000G57", "SELECT TOP 1 [ContratoOcorrencia_Codigo] FROM [ContratoOcorrencia] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G57,1,0,true,true )
             ,new CursorDef("T000G58", "SELECT TOP 1 [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G58,1,0,true,true )
             ,new CursorDef("T000G59", "SELECT TOP 1 [ContratoClausulas_Codigo] FROM [ContratoClausulas] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G59,1,0,true,true )
             ,new CursorDef("T000G60", "SELECT TOP 1 [ContratoArquivosAnexos_Codigo] FROM [ContratoArquivosAnexos] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G60,1,0,true,true )
             ,new CursorDef("T000G61", "SELECT TOP 1 [ContratoGarantia_Codigo] FROM [ContratoGarantia] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G61,1,0,true,true )
             ,new CursorDef("T000G62", "SELECT TOP 1 [ContratoCaixaEntrada_Codigo], [Contrato_Codigo] FROM [ContratoCaixaEntrada] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G62,1,0,true,true )
             ,new CursorDef("T000G63", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) ORDER BY [Contrato_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000G63,100,0,true,false )
             ,new CursorDef("T000G64", "SELECT T3.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod], T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE (T2.[Usuario_Ativo] = 1) AND (T1.[ContratadaUsuario_ContratadaCod] = @AV17Contratada_Codigo) ORDER BY T3.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G64,0,0,true,false )
             ,new CursorDef("T000G67", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA FROM (SELECT T2.[ContratoTermoAditivo_DataInicio], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC1] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G67,1,0,true,false )
             ,new CursorDef("T000G70", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (SELECT T2.[ContratoTermoAditivo_DataFim], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC1] AS GXC1 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC1, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC1] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G70,1,0,true,false )
             ,new CursorDef("T000G71", "SELECT [Usuario_PessoaCod] AS Contrato_PrepostoPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Contrato_PrepostoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G71,1,0,true,false )
             ,new CursorDef("T000G72", "SELECT [Pessoa_Nome] AS Contrato_PrepostoNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contrato_PrepostoPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G72,1,0,true,false )
             ,new CursorDef("T000G73", "SELECT [AreaTrabalho_Descricao] AS Contrato_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contrato_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G73,1,0,true,false )
             ,new CursorDef("T000G74", "SELECT [Contratada_Sigla], [Contratada_TipoFabrica], [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G74,1,0,true,false )
             ,new CursorDef("T000G75", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G75,1,0,true,false )
             ,new CursorDef("T000G76", "SELECT MAX([ContratoTermoAditivo_Codigo]) FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G76,1,0,true,true )
             ,new CursorDef("T000G77", "SELECT [ContratoTermoAditivo_VlrUntUndCnt] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE ( [Contrato_Codigo] = @Contrato_Codigo) and ( [ContratoTermoAditivo_Codigo] = @ContratoTermoAditivo_Codigo) ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G77,1,0,true,true )
             ,new CursorDef("T000G78", "SELECT MAX([ContratoTermoAditivo_Codigo]) FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G78,1,0,true,true )
             ,new CursorDef("T000G79", "SELECT [ContratoTermoAditivo_VlrUntUndCnt] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE ( [Contrato_Codigo] = @Contrato_Codigo) and ( [ContratoTermoAditivo_Codigo] = @ContratoTermoAditivo_Codigo) ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G79,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(9) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(10) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(12) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(13) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(14) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(15) ;
                ((decimal[]) buf[16])[0] = rslt.getDecimal(16) ;
                ((String[]) buf[17])[0] = rslt.getLongVarchar(17) ;
                ((short[]) buf[18])[0] = rslt.getShort(18) ;
                ((String[]) buf[19])[0] = rslt.getString(19, 1) ;
                ((decimal[]) buf[20])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[21])[0] = rslt.getBool(21) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(21);
                ((bool[]) buf[23])[0] = rslt.getBool(22) ;
                ((String[]) buf[24])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(23);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(24);
                ((short[]) buf[28])[0] = rslt.getShort(25) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(25);
                ((short[]) buf[30])[0] = rslt.getShort(26) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(26);
                ((int[]) buf[32])[0] = rslt.getInt(27) ;
                ((int[]) buf[33])[0] = rslt.getInt(28) ;
                ((int[]) buf[34])[0] = rslt.getInt(29) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(29);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(9) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(10) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(12) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(13) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(14) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(15) ;
                ((decimal[]) buf[16])[0] = rslt.getDecimal(16) ;
                ((String[]) buf[17])[0] = rslt.getLongVarchar(17) ;
                ((short[]) buf[18])[0] = rslt.getShort(18) ;
                ((String[]) buf[19])[0] = rslt.getString(19, 1) ;
                ((decimal[]) buf[20])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[21])[0] = rslt.getBool(21) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(21);
                ((bool[]) buf[23])[0] = rslt.getBool(22) ;
                ((String[]) buf[24])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(23);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(24);
                ((short[]) buf[28])[0] = rslt.getShort(25) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(25);
                ((short[]) buf[30])[0] = rslt.getShort(26) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(26);
                ((int[]) buf[32])[0] = rslt.getInt(27) ;
                ((int[]) buf[33])[0] = rslt.getInt(28) ;
                ((int[]) buf[34])[0] = rslt.getInt(29) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(29);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((short[]) buf[12])[0] = rslt.getShort(9) ;
                ((String[]) buf[13])[0] = rslt.getLongVarchar(10) ;
                ((short[]) buf[14])[0] = rslt.getShort(11) ;
                ((int[]) buf[15])[0] = rslt.getInt(12) ;
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(13) ;
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(14) ;
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(15) ;
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(16) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(17) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(18) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(19) ;
                ((decimal[]) buf[23])[0] = rslt.getDecimal(20) ;
                ((decimal[]) buf[24])[0] = rslt.getDecimal(21) ;
                ((String[]) buf[25])[0] = rslt.getLongVarchar(22) ;
                ((short[]) buf[26])[0] = rslt.getShort(23) ;
                ((String[]) buf[27])[0] = rslt.getString(24, 1) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[29])[0] = rslt.getBool(26) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(26);
                ((bool[]) buf[31])[0] = rslt.getBool(27) ;
                ((String[]) buf[32])[0] = rslt.getString(28, 100) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(28);
                ((String[]) buf[34])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(29);
                ((decimal[]) buf[36])[0] = rslt.getDecimal(30) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(30);
                ((short[]) buf[38])[0] = rslt.getShort(31) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(31);
                ((short[]) buf[40])[0] = rslt.getShort(32) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(32);
                ((int[]) buf[42])[0] = rslt.getInt(33) ;
                ((int[]) buf[43])[0] = rslt.getInt(34) ;
                ((int[]) buf[44])[0] = rslt.getInt(35) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(35);
                ((int[]) buf[46])[0] = rslt.getInt(36) ;
                ((int[]) buf[47])[0] = rslt.getInt(37) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(37);
                ((DateTime[]) buf[49])[0] = rslt.getGXDate(38) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(38);
                ((DateTime[]) buf[51])[0] = rslt.getGXDate(39) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(39);
                return;
             case 10 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 24 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 26 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 27 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 29 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 45 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 46 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 47 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 48 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 49 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 50 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 51 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 52 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 53 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 54 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 55 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 56 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 57 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (short)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                stmt.SetParameter(5, (short)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (DateTime)parms[7]);
                stmt.SetParameter(8, (DateTime)parms[8]);
                stmt.SetParameter(9, (DateTime)parms[9]);
                stmt.SetParameter(10, (DateTime)parms[10]);
                stmt.SetParameter(11, (DateTime)parms[11]);
                stmt.SetParameter(12, (DateTime)parms[12]);
                stmt.SetParameter(13, (DateTime)parms[13]);
                stmt.SetParameter(14, (decimal)parms[14]);
                stmt.SetParameter(15, (decimal)parms[15]);
                stmt.SetParameter(16, (String)parms[16]);
                stmt.SetParameter(17, (short)parms[17]);
                stmt.SetParameter(18, (String)parms[18]);
                stmt.SetParameter(19, (decimal)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 20 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(20, (bool)parms[21]);
                }
                stmt.SetParameter(21, (bool)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 22 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 24 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(24, (short)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 25 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(25, (short)parms[30]);
                }
                stmt.SetParameter(26, (int)parms[31]);
                stmt.SetParameter(27, (int)parms[32]);
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[34]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (short)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                stmt.SetParameter(5, (short)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (DateTime)parms[7]);
                stmt.SetParameter(8, (DateTime)parms[8]);
                stmt.SetParameter(9, (DateTime)parms[9]);
                stmt.SetParameter(10, (DateTime)parms[10]);
                stmt.SetParameter(11, (DateTime)parms[11]);
                stmt.SetParameter(12, (DateTime)parms[12]);
                stmt.SetParameter(13, (DateTime)parms[13]);
                stmt.SetParameter(14, (decimal)parms[14]);
                stmt.SetParameter(15, (decimal)parms[15]);
                stmt.SetParameter(16, (String)parms[16]);
                stmt.SetParameter(17, (short)parms[17]);
                stmt.SetParameter(18, (String)parms[18]);
                stmt.SetParameter(19, (decimal)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 20 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(20, (bool)parms[21]);
                }
                stmt.SetParameter(21, (bool)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 22 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 24 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(24, (short)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 25 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(25, (short)parms[30]);
                }
                stmt.SetParameter(26, (int)parms[31]);
                stmt.SetParameter(27, (int)parms[32]);
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 29 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(29, (int)parms[36]);
                }
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 31 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 32 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 33 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 34 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 35 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 36 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 37 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 38 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 39 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 40 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 41 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 42 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 43 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 44 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 46 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 47 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 48 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 49 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 50 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 51 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 52 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 53 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 54 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 55 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 56 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 57 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
