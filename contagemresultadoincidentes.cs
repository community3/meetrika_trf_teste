/*
               File: ContagemResultadoIncidentes
        Description: Contagem Resultado Incidentes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:26.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoincidentes : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A1239Incidentes_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1239Incidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1239Incidentes_DemandaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A1239Incidentes_DemandaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Incidentes_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Incidentes_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vINCIDENTES_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Incidentes_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Resultado Incidentes", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtIncidentes_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadoincidentes( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadoincidentes( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Incidentes_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Incidentes_Codigo = aP1_Incidentes_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3E145( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3E145e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtIncidentes_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1241Incidentes_Codigo), 6, 0, ",", "")), ((edtIncidentes_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1241Incidentes_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1241Incidentes_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtIncidentes_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtIncidentes_Codigo_Visible, edtIncidentes_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoIncidentes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtIncidentes_DemandaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1239Incidentes_DemandaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A1239Incidentes_DemandaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtIncidentes_DemandaCod_Jsonclick, 0, "Attribute", "", "", "", edtIncidentes_DemandaCod_Visible, edtIncidentes_DemandaCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoIncidentes.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3E145( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3E145( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3E145e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_3E145( true) ;
         }
         return  ;
      }

      protected void wb_table3_31_3E145e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3E145e( true) ;
         }
         else
         {
            wb_table1_2_3E145e( false) ;
         }
      }

      protected void wb_table3_31_3E145( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_3E145e( true) ;
         }
         else
         {
            wb_table3_31_3E145e( false) ;
         }
      }

      protected void wb_table2_5_3E145( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_3E145( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_3E145e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3E145e( true) ;
         }
         else
         {
            wb_table2_5_3E145e( false) ;
         }
      }

      protected void wb_table4_13_3E145( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockincidentes_data_Internalname, "Data", "", "", lblTextblockincidentes_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtIncidentes_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtIncidentes_Data_Internalname, context.localUtil.TToC( A1242Incidentes_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1242Incidentes_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtIncidentes_Data_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtIncidentes_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_ContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtIncidentes_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtIncidentes_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockincidentes_descricao_Internalname, "Descricao", "", "", lblTextblockincidentes_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtIncidentes_Descricao_Internalname, A1243Incidentes_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", 0, 1, edtIncidentes_Descricao_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_ContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockincidentes_demandanum_Internalname, "Demanda", "", "", lblTextblockincidentes_demandanum_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtIncidentes_DemandaNum_Internalname, A1240Incidentes_DemandaNum, StringUtil.RTrim( context.localUtil.Format( A1240Incidentes_DemandaNum, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtIncidentes_DemandaNum_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtIncidentes_DemandaNum_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_3E145e( true) ;
         }
         else
         {
            wb_table4_13_3E145e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113E2 */
         E113E2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( context.localUtil.VCDateTime( cgiGet( edtIncidentes_Data_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data"}), 1, "INCIDENTES_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtIncidentes_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1242Incidentes_Data = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1242Incidentes_Data", context.localUtil.TToC( A1242Incidentes_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1242Incidentes_Data = context.localUtil.CToT( cgiGet( edtIncidentes_Data_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1242Incidentes_Data", context.localUtil.TToC( A1242Incidentes_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               A1243Incidentes_Descricao = cgiGet( edtIncidentes_Descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1243Incidentes_Descricao", A1243Incidentes_Descricao);
               A1240Incidentes_DemandaNum = StringUtil.Upper( cgiGet( edtIncidentes_DemandaNum_Internalname));
               n1240Incidentes_DemandaNum = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1240Incidentes_DemandaNum", A1240Incidentes_DemandaNum);
               A1241Incidentes_Codigo = (int)(context.localUtil.CToN( cgiGet( edtIncidentes_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtIncidentes_DemandaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtIncidentes_DemandaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "INCIDENTES_DEMANDACOD");
                  AnyError = 1;
                  GX_FocusControl = edtIncidentes_DemandaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1239Incidentes_DemandaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1239Incidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1239Incidentes_DemandaCod), 6, 0)));
               }
               else
               {
                  A1239Incidentes_DemandaCod = (int)(context.localUtil.CToN( cgiGet( edtIncidentes_DemandaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1239Incidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1239Incidentes_DemandaCod), 6, 0)));
               }
               /* Read saved values. */
               Z1241Incidentes_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1241Incidentes_Codigo"), ",", "."));
               Z1242Incidentes_Data = context.localUtil.CToT( cgiGet( "Z1242Incidentes_Data"), 0);
               Z1239Incidentes_DemandaCod = (int)(context.localUtil.CToN( cgiGet( "Z1239Incidentes_DemandaCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1239Incidentes_DemandaCod = (int)(context.localUtil.CToN( cgiGet( "N1239Incidentes_DemandaCod"), ",", "."));
               AV7Incidentes_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINCIDENTES_CODIGO"), ",", "."));
               AV11Insert_Incidentes_DemandaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_INCIDENTES_DEMANDACOD"), ",", "."));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContagemResultadoIncidentes";
               A1241Incidentes_Codigo = (int)(context.localUtil.CToN( cgiGet( edtIncidentes_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1241Incidentes_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1241Incidentes_Codigo != Z1241Incidentes_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contagemresultadoincidentes:[SecurityCheckFailed value for]"+"Incidentes_Codigo:"+context.localUtil.Format( (decimal)(A1241Incidentes_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contagemresultadoincidentes:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1241Incidentes_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode145 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode145;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound145 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3E0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "INCIDENTES_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtIncidentes_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113E2 */
                           E113E2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123E2 */
                           E123E2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123E2 */
            E123E2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3E145( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3E145( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3E0( )
      {
         BeforeValidate3E145( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3E145( ) ;
            }
            else
            {
               CheckExtendedTable3E145( ) ;
               CloseExtendedTableCursors3E145( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3E0( )
      {
      }

      protected void E113E2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Incidentes_DemandaCod") == 0 )
               {
                  AV11Insert_Incidentes_DemandaCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Incidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Incidentes_DemandaCod), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
         edtIncidentes_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIncidentes_Codigo_Visible), 5, 0)));
         edtIncidentes_DemandaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_DemandaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIncidentes_DemandaCod_Visible), 5, 0)));
      }

      protected void E123E2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontagemresultadoincidentes.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM3E145( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1242Incidentes_Data = T003E3_A1242Incidentes_Data[0];
               Z1239Incidentes_DemandaCod = T003E3_A1239Incidentes_DemandaCod[0];
            }
            else
            {
               Z1242Incidentes_Data = A1242Incidentes_Data;
               Z1239Incidentes_DemandaCod = A1239Incidentes_DemandaCod;
            }
         }
         if ( GX_JID == -9 )
         {
            Z1241Incidentes_Codigo = A1241Incidentes_Codigo;
            Z1242Incidentes_Data = A1242Incidentes_Data;
            Z1243Incidentes_Descricao = A1243Incidentes_Descricao;
            Z1239Incidentes_DemandaCod = A1239Incidentes_DemandaCod;
            Z1240Incidentes_DemandaNum = A1240Incidentes_DemandaNum;
         }
      }

      protected void standaloneNotModal( )
      {
         edtIncidentes_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIncidentes_Codigo_Enabled), 5, 0)));
         AV13Pgmname = "ContagemResultadoIncidentes";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         edtIncidentes_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIncidentes_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Incidentes_Codigo) )
         {
            A1241Incidentes_Codigo = AV7Incidentes_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Incidentes_DemandaCod) )
         {
            edtIncidentes_DemandaCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_DemandaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIncidentes_DemandaCod_Enabled), 5, 0)));
         }
         else
         {
            edtIncidentes_DemandaCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_DemandaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIncidentes_DemandaCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Incidentes_DemandaCod) )
         {
            A1239Incidentes_DemandaCod = AV11Insert_Incidentes_DemandaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1239Incidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1239Incidentes_DemandaCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T003E4 */
            pr_default.execute(2, new Object[] {A1239Incidentes_DemandaCod});
            A1240Incidentes_DemandaNum = T003E4_A1240Incidentes_DemandaNum[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1240Incidentes_DemandaNum", A1240Incidentes_DemandaNum);
            n1240Incidentes_DemandaNum = T003E4_n1240Incidentes_DemandaNum[0];
            pr_default.close(2);
         }
      }

      protected void Load3E145( )
      {
         /* Using cursor T003E5 */
         pr_default.execute(3, new Object[] {A1241Incidentes_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound145 = 1;
            A1242Incidentes_Data = T003E5_A1242Incidentes_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1242Incidentes_Data", context.localUtil.TToC( A1242Incidentes_Data, 8, 5, 0, 3, "/", ":", " "));
            A1243Incidentes_Descricao = T003E5_A1243Incidentes_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1243Incidentes_Descricao", A1243Incidentes_Descricao);
            A1240Incidentes_DemandaNum = T003E5_A1240Incidentes_DemandaNum[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1240Incidentes_DemandaNum", A1240Incidentes_DemandaNum);
            n1240Incidentes_DemandaNum = T003E5_n1240Incidentes_DemandaNum[0];
            A1239Incidentes_DemandaCod = T003E5_A1239Incidentes_DemandaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1239Incidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1239Incidentes_DemandaCod), 6, 0)));
            ZM3E145( -9) ;
         }
         pr_default.close(3);
         OnLoadActions3E145( ) ;
      }

      protected void OnLoadActions3E145( )
      {
      }

      protected void CheckExtendedTable3E145( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1242Incidentes_Data) || ( A1242Incidentes_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "INCIDENTES_DATA");
            AnyError = 1;
            GX_FocusControl = edtIncidentes_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T003E4 */
         pr_default.execute(2, new Object[] {A1239Incidentes_DemandaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Incidentes_Contagem Resultado'.", "ForeignKeyNotFound", 1, "INCIDENTES_DEMANDACOD");
            AnyError = 1;
            GX_FocusControl = edtIncidentes_DemandaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1240Incidentes_DemandaNum = T003E4_A1240Incidentes_DemandaNum[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1240Incidentes_DemandaNum", A1240Incidentes_DemandaNum);
         n1240Incidentes_DemandaNum = T003E4_n1240Incidentes_DemandaNum[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors3E145( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A1239Incidentes_DemandaCod )
      {
         /* Using cursor T003E6 */
         pr_default.execute(4, new Object[] {A1239Incidentes_DemandaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Incidentes_Contagem Resultado'.", "ForeignKeyNotFound", 1, "INCIDENTES_DEMANDACOD");
            AnyError = 1;
            GX_FocusControl = edtIncidentes_DemandaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1240Incidentes_DemandaNum = T003E6_A1240Incidentes_DemandaNum[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1240Incidentes_DemandaNum", A1240Incidentes_DemandaNum);
         n1240Incidentes_DemandaNum = T003E6_n1240Incidentes_DemandaNum[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A1240Incidentes_DemandaNum)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey3E145( )
      {
         /* Using cursor T003E7 */
         pr_default.execute(5, new Object[] {A1241Incidentes_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound145 = 1;
         }
         else
         {
            RcdFound145 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003E3 */
         pr_default.execute(1, new Object[] {A1241Incidentes_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3E145( 9) ;
            RcdFound145 = 1;
            A1241Incidentes_Codigo = T003E3_A1241Incidentes_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
            A1242Incidentes_Data = T003E3_A1242Incidentes_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1242Incidentes_Data", context.localUtil.TToC( A1242Incidentes_Data, 8, 5, 0, 3, "/", ":", " "));
            A1243Incidentes_Descricao = T003E3_A1243Incidentes_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1243Incidentes_Descricao", A1243Incidentes_Descricao);
            A1239Incidentes_DemandaCod = T003E3_A1239Incidentes_DemandaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1239Incidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1239Incidentes_DemandaCod), 6, 0)));
            Z1241Incidentes_Codigo = A1241Incidentes_Codigo;
            sMode145 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3E145( ) ;
            if ( AnyError == 1 )
            {
               RcdFound145 = 0;
               InitializeNonKey3E145( ) ;
            }
            Gx_mode = sMode145;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound145 = 0;
            InitializeNonKey3E145( ) ;
            sMode145 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode145;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3E145( ) ;
         if ( RcdFound145 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound145 = 0;
         /* Using cursor T003E8 */
         pr_default.execute(6, new Object[] {A1241Incidentes_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T003E8_A1241Incidentes_Codigo[0] < A1241Incidentes_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T003E8_A1241Incidentes_Codigo[0] > A1241Incidentes_Codigo ) ) )
            {
               A1241Incidentes_Codigo = T003E8_A1241Incidentes_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
               RcdFound145 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound145 = 0;
         /* Using cursor T003E9 */
         pr_default.execute(7, new Object[] {A1241Incidentes_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T003E9_A1241Incidentes_Codigo[0] > A1241Incidentes_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T003E9_A1241Incidentes_Codigo[0] < A1241Incidentes_Codigo ) ) )
            {
               A1241Incidentes_Codigo = T003E9_A1241Incidentes_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
               RcdFound145 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3E145( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtIncidentes_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3E145( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound145 == 1 )
            {
               if ( A1241Incidentes_Codigo != Z1241Incidentes_Codigo )
               {
                  A1241Incidentes_Codigo = Z1241Incidentes_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "INCIDENTES_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtIncidentes_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtIncidentes_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3E145( ) ;
                  GX_FocusControl = edtIncidentes_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1241Incidentes_Codigo != Z1241Incidentes_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtIncidentes_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3E145( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "INCIDENTES_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtIncidentes_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtIncidentes_Data_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3E145( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1241Incidentes_Codigo != Z1241Incidentes_Codigo )
         {
            A1241Incidentes_Codigo = Z1241Incidentes_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "INCIDENTES_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtIncidentes_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtIncidentes_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3E145( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003E2 */
            pr_default.execute(0, new Object[] {A1241Incidentes_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoIncidentes"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1242Incidentes_Data != T003E2_A1242Incidentes_Data[0] ) || ( Z1239Incidentes_DemandaCod != T003E2_A1239Incidentes_DemandaCod[0] ) )
            {
               if ( Z1242Incidentes_Data != T003E2_A1242Incidentes_Data[0] )
               {
                  GXUtil.WriteLog("contagemresultadoincidentes:[seudo value changed for attri]"+"Incidentes_Data");
                  GXUtil.WriteLogRaw("Old: ",Z1242Incidentes_Data);
                  GXUtil.WriteLogRaw("Current: ",T003E2_A1242Incidentes_Data[0]);
               }
               if ( Z1239Incidentes_DemandaCod != T003E2_A1239Incidentes_DemandaCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadoincidentes:[seudo value changed for attri]"+"Incidentes_DemandaCod");
                  GXUtil.WriteLogRaw("Old: ",Z1239Incidentes_DemandaCod);
                  GXUtil.WriteLogRaw("Current: ",T003E2_A1239Incidentes_DemandaCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoIncidentes"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3E145( )
      {
         BeforeValidate3E145( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3E145( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3E145( 0) ;
            CheckOptimisticConcurrency3E145( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3E145( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3E145( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003E10 */
                     pr_default.execute(8, new Object[] {A1242Incidentes_Data, A1243Incidentes_Descricao, A1239Incidentes_DemandaCod});
                     A1241Incidentes_Codigo = T003E10_A1241Incidentes_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIncidentes") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3E0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3E145( ) ;
            }
            EndLevel3E145( ) ;
         }
         CloseExtendedTableCursors3E145( ) ;
      }

      protected void Update3E145( )
      {
         BeforeValidate3E145( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3E145( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3E145( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3E145( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3E145( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003E11 */
                     pr_default.execute(9, new Object[] {A1242Incidentes_Data, A1243Incidentes_Descricao, A1239Incidentes_DemandaCod, A1241Incidentes_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIncidentes") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoIncidentes"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3E145( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3E145( ) ;
         }
         CloseExtendedTableCursors3E145( ) ;
      }

      protected void DeferredUpdate3E145( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3E145( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3E145( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3E145( ) ;
            AfterConfirm3E145( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3E145( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003E12 */
                  pr_default.execute(10, new Object[] {A1241Incidentes_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIncidentes") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode145 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3E145( ) ;
         Gx_mode = sMode145;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3E145( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003E13 */
            pr_default.execute(11, new Object[] {A1239Incidentes_DemandaCod});
            A1240Incidentes_DemandaNum = T003E13_A1240Incidentes_DemandaNum[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1240Incidentes_DemandaNum", A1240Incidentes_DemandaNum);
            n1240Incidentes_DemandaNum = T003E13_n1240Incidentes_DemandaNum[0];
            pr_default.close(11);
         }
      }

      protected void EndLevel3E145( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3E145( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "ContagemResultadoIncidentes");
            if ( AnyError == 0 )
            {
               ConfirmValues3E0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "ContagemResultadoIncidentes");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3E145( )
      {
         /* Scan By routine */
         /* Using cursor T003E14 */
         pr_default.execute(12);
         RcdFound145 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound145 = 1;
            A1241Incidentes_Codigo = T003E14_A1241Incidentes_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3E145( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound145 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound145 = 1;
            A1241Incidentes_Codigo = T003E14_A1241Incidentes_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3E145( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm3E145( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3E145( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3E145( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3E145( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3E145( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3E145( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3E145( )
      {
         edtIncidentes_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIncidentes_Data_Enabled), 5, 0)));
         edtIncidentes_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIncidentes_Descricao_Enabled), 5, 0)));
         edtIncidentes_DemandaNum_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_DemandaNum_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIncidentes_DemandaNum_Enabled), 5, 0)));
         edtIncidentes_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIncidentes_Codigo_Enabled), 5, 0)));
         edtIncidentes_DemandaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_DemandaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIncidentes_DemandaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3E0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216172788");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadoincidentes.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Incidentes_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1241Incidentes_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1242Incidentes_Data", context.localUtil.TToC( Z1242Incidentes_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1239Incidentes_DemandaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1239Incidentes_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1239Incidentes_DemandaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1239Incidentes_DemandaCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vINCIDENTES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Incidentes_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_INCIDENTES_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Incidentes_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vINCIDENTES_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Incidentes_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContagemResultadoIncidentes";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1241Incidentes_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemresultadoincidentes:[SendSecurityCheck value for]"+"Incidentes_Codigo:"+context.localUtil.Format( (decimal)(A1241Incidentes_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contagemresultadoincidentes:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadoincidentes.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Incidentes_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoIncidentes" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Incidentes" ;
      }

      protected void InitializeNonKey3E145( )
      {
         A1239Incidentes_DemandaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1239Incidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1239Incidentes_DemandaCod), 6, 0)));
         A1242Incidentes_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1242Incidentes_Data", context.localUtil.TToC( A1242Incidentes_Data, 8, 5, 0, 3, "/", ":", " "));
         A1243Incidentes_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1243Incidentes_Descricao", A1243Incidentes_Descricao);
         A1240Incidentes_DemandaNum = "";
         n1240Incidentes_DemandaNum = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1240Incidentes_DemandaNum", A1240Incidentes_DemandaNum);
         Z1242Incidentes_Data = (DateTime)(DateTime.MinValue);
         Z1239Incidentes_DemandaCod = 0;
      }

      protected void InitAll3E145( )
      {
         A1241Incidentes_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1241Incidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1241Incidentes_Codigo), 6, 0)));
         InitializeNonKey3E145( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621617285");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadoincidentes.js", "?2020621617285");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockincidentes_data_Internalname = "TEXTBLOCKINCIDENTES_DATA";
         edtIncidentes_Data_Internalname = "INCIDENTES_DATA";
         lblTextblockincidentes_descricao_Internalname = "TEXTBLOCKINCIDENTES_DESCRICAO";
         edtIncidentes_Descricao_Internalname = "INCIDENTES_DESCRICAO";
         lblTextblockincidentes_demandanum_Internalname = "TEXTBLOCKINCIDENTES_DEMANDANUM";
         edtIncidentes_DemandaNum_Internalname = "INCIDENTES_DEMANDANUM";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtIncidentes_Codigo_Internalname = "INCIDENTES_CODIGO";
         edtIncidentes_DemandaCod_Internalname = "INCIDENTES_DEMANDACOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Contagem Resultado Incidentes";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Resultado Incidentes";
         edtIncidentes_DemandaNum_Jsonclick = "";
         edtIncidentes_DemandaNum_Enabled = 0;
         edtIncidentes_Descricao_Enabled = 1;
         edtIncidentes_Data_Jsonclick = "";
         edtIncidentes_Data_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtIncidentes_DemandaCod_Jsonclick = "";
         edtIncidentes_DemandaCod_Enabled = 1;
         edtIncidentes_DemandaCod_Visible = 1;
         edtIncidentes_Codigo_Jsonclick = "";
         edtIncidentes_Codigo_Enabled = 0;
         edtIncidentes_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Incidentes_demandacod( int GX_Parm1 ,
                                               String GX_Parm2 )
      {
         A1239Incidentes_DemandaCod = GX_Parm1;
         A1240Incidentes_DemandaNum = GX_Parm2;
         n1240Incidentes_DemandaNum = false;
         /* Using cursor T003E13 */
         pr_default.execute(11, new Object[] {A1239Incidentes_DemandaCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Incidentes_Contagem Resultado'.", "ForeignKeyNotFound", 1, "INCIDENTES_DEMANDACOD");
            AnyError = 1;
            GX_FocusControl = edtIncidentes_DemandaCod_Internalname;
         }
         A1240Incidentes_DemandaNum = T003E13_A1240Incidentes_DemandaNum[0];
         n1240Incidentes_DemandaNum = T003E13_n1240Incidentes_DemandaNum[0];
         pr_default.close(11);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1240Incidentes_DemandaNum = "";
            n1240Incidentes_DemandaNum = false;
         }
         isValidOutput.Add(A1240Incidentes_DemandaNum);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Incidentes_Codigo',fld:'vINCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123E2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1242Incidentes_Data = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockincidentes_data_Jsonclick = "";
         A1242Incidentes_Data = (DateTime)(DateTime.MinValue);
         lblTextblockincidentes_descricao_Jsonclick = "";
         A1243Incidentes_Descricao = "";
         lblTextblockincidentes_demandanum_Jsonclick = "";
         A1240Incidentes_DemandaNum = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode145 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1243Incidentes_Descricao = "";
         Z1240Incidentes_DemandaNum = "";
         T003E4_A1240Incidentes_DemandaNum = new String[] {""} ;
         T003E4_n1240Incidentes_DemandaNum = new bool[] {false} ;
         T003E5_A1241Incidentes_Codigo = new int[1] ;
         T003E5_A1242Incidentes_Data = new DateTime[] {DateTime.MinValue} ;
         T003E5_A1243Incidentes_Descricao = new String[] {""} ;
         T003E5_A1240Incidentes_DemandaNum = new String[] {""} ;
         T003E5_n1240Incidentes_DemandaNum = new bool[] {false} ;
         T003E5_A1239Incidentes_DemandaCod = new int[1] ;
         T003E6_A1240Incidentes_DemandaNum = new String[] {""} ;
         T003E6_n1240Incidentes_DemandaNum = new bool[] {false} ;
         T003E7_A1241Incidentes_Codigo = new int[1] ;
         T003E3_A1241Incidentes_Codigo = new int[1] ;
         T003E3_A1242Incidentes_Data = new DateTime[] {DateTime.MinValue} ;
         T003E3_A1243Incidentes_Descricao = new String[] {""} ;
         T003E3_A1239Incidentes_DemandaCod = new int[1] ;
         T003E8_A1241Incidentes_Codigo = new int[1] ;
         T003E9_A1241Incidentes_Codigo = new int[1] ;
         T003E2_A1241Incidentes_Codigo = new int[1] ;
         T003E2_A1242Incidentes_Data = new DateTime[] {DateTime.MinValue} ;
         T003E2_A1243Incidentes_Descricao = new String[] {""} ;
         T003E2_A1239Incidentes_DemandaCod = new int[1] ;
         T003E10_A1241Incidentes_Codigo = new int[1] ;
         T003E13_A1240Incidentes_DemandaNum = new String[] {""} ;
         T003E13_n1240Incidentes_DemandaNum = new bool[] {false} ;
         T003E14_A1241Incidentes_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoincidentes__default(),
            new Object[][] {
                new Object[] {
               T003E2_A1241Incidentes_Codigo, T003E2_A1242Incidentes_Data, T003E2_A1243Incidentes_Descricao, T003E2_A1239Incidentes_DemandaCod
               }
               , new Object[] {
               T003E3_A1241Incidentes_Codigo, T003E3_A1242Incidentes_Data, T003E3_A1243Incidentes_Descricao, T003E3_A1239Incidentes_DemandaCod
               }
               , new Object[] {
               T003E4_A1240Incidentes_DemandaNum, T003E4_n1240Incidentes_DemandaNum
               }
               , new Object[] {
               T003E5_A1241Incidentes_Codigo, T003E5_A1242Incidentes_Data, T003E5_A1243Incidentes_Descricao, T003E5_A1240Incidentes_DemandaNum, T003E5_n1240Incidentes_DemandaNum, T003E5_A1239Incidentes_DemandaCod
               }
               , new Object[] {
               T003E6_A1240Incidentes_DemandaNum, T003E6_n1240Incidentes_DemandaNum
               }
               , new Object[] {
               T003E7_A1241Incidentes_Codigo
               }
               , new Object[] {
               T003E8_A1241Incidentes_Codigo
               }
               , new Object[] {
               T003E9_A1241Incidentes_Codigo
               }
               , new Object[] {
               T003E10_A1241Incidentes_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003E13_A1240Incidentes_DemandaNum, T003E13_n1240Incidentes_DemandaNum
               }
               , new Object[] {
               T003E14_A1241Incidentes_Codigo
               }
            }
         );
         AV13Pgmname = "ContagemResultadoIncidentes";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound145 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Incidentes_Codigo ;
      private int Z1241Incidentes_Codigo ;
      private int Z1239Incidentes_DemandaCod ;
      private int N1239Incidentes_DemandaCod ;
      private int A1239Incidentes_DemandaCod ;
      private int AV7Incidentes_Codigo ;
      private int trnEnded ;
      private int A1241Incidentes_Codigo ;
      private int edtIncidentes_Codigo_Enabled ;
      private int edtIncidentes_Codigo_Visible ;
      private int edtIncidentes_DemandaCod_Visible ;
      private int edtIncidentes_DemandaCod_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtIncidentes_Data_Enabled ;
      private int edtIncidentes_Descricao_Enabled ;
      private int edtIncidentes_DemandaNum_Enabled ;
      private int AV11Insert_Incidentes_DemandaCod ;
      private int AV14GXV1 ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtIncidentes_Data_Internalname ;
      private String edtIncidentes_Codigo_Internalname ;
      private String edtIncidentes_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtIncidentes_DemandaCod_Internalname ;
      private String edtIncidentes_DemandaCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockincidentes_data_Internalname ;
      private String lblTextblockincidentes_data_Jsonclick ;
      private String edtIncidentes_Data_Jsonclick ;
      private String lblTextblockincidentes_descricao_Internalname ;
      private String lblTextblockincidentes_descricao_Jsonclick ;
      private String edtIncidentes_Descricao_Internalname ;
      private String lblTextblockincidentes_demandanum_Internalname ;
      private String lblTextblockincidentes_demandanum_Jsonclick ;
      private String edtIncidentes_DemandaNum_Internalname ;
      private String edtIncidentes_DemandaNum_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode145 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z1242Incidentes_Data ;
      private DateTime A1242Incidentes_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1240Incidentes_DemandaNum ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A1243Incidentes_Descricao ;
      private String Z1243Incidentes_Descricao ;
      private String A1240Incidentes_DemandaNum ;
      private String Z1240Incidentes_DemandaNum ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T003E4_A1240Incidentes_DemandaNum ;
      private bool[] T003E4_n1240Incidentes_DemandaNum ;
      private int[] T003E5_A1241Incidentes_Codigo ;
      private DateTime[] T003E5_A1242Incidentes_Data ;
      private String[] T003E5_A1243Incidentes_Descricao ;
      private String[] T003E5_A1240Incidentes_DemandaNum ;
      private bool[] T003E5_n1240Incidentes_DemandaNum ;
      private int[] T003E5_A1239Incidentes_DemandaCod ;
      private String[] T003E6_A1240Incidentes_DemandaNum ;
      private bool[] T003E6_n1240Incidentes_DemandaNum ;
      private int[] T003E7_A1241Incidentes_Codigo ;
      private int[] T003E3_A1241Incidentes_Codigo ;
      private DateTime[] T003E3_A1242Incidentes_Data ;
      private String[] T003E3_A1243Incidentes_Descricao ;
      private int[] T003E3_A1239Incidentes_DemandaCod ;
      private int[] T003E8_A1241Incidentes_Codigo ;
      private int[] T003E9_A1241Incidentes_Codigo ;
      private int[] T003E2_A1241Incidentes_Codigo ;
      private DateTime[] T003E2_A1242Incidentes_Data ;
      private String[] T003E2_A1243Incidentes_Descricao ;
      private int[] T003E2_A1239Incidentes_DemandaCod ;
      private int[] T003E10_A1241Incidentes_Codigo ;
      private String[] T003E13_A1240Incidentes_DemandaNum ;
      private bool[] T003E13_n1240Incidentes_DemandaNum ;
      private int[] T003E14_A1241Incidentes_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class contagemresultadoincidentes__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003E5 ;
          prmT003E5 = new Object[] {
          new Object[] {"@Incidentes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003E4 ;
          prmT003E4 = new Object[] {
          new Object[] {"@Incidentes_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003E6 ;
          prmT003E6 = new Object[] {
          new Object[] {"@Incidentes_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003E7 ;
          prmT003E7 = new Object[] {
          new Object[] {"@Incidentes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003E3 ;
          prmT003E3 = new Object[] {
          new Object[] {"@Incidentes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003E8 ;
          prmT003E8 = new Object[] {
          new Object[] {"@Incidentes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003E9 ;
          prmT003E9 = new Object[] {
          new Object[] {"@Incidentes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003E2 ;
          prmT003E2 = new Object[] {
          new Object[] {"@Incidentes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003E10 ;
          prmT003E10 = new Object[] {
          new Object[] {"@Incidentes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Incidentes_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Incidentes_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003E11 ;
          prmT003E11 = new Object[] {
          new Object[] {"@Incidentes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Incidentes_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Incidentes_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Incidentes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003E12 ;
          prmT003E12 = new Object[] {
          new Object[] {"@Incidentes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003E14 ;
          prmT003E14 = new Object[] {
          } ;
          Object[] prmT003E13 ;
          prmT003E13 = new Object[] {
          new Object[] {"@Incidentes_DemandaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003E2", "SELECT [Incidentes_Codigo], [Incidentes_Data], [Incidentes_Descricao], [Incidentes_DemandaCod] AS Incidentes_DemandaCod FROM [ContagemResultadoIncidentes] WITH (UPDLOCK) WHERE [Incidentes_Codigo] = @Incidentes_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003E2,1,0,true,false )
             ,new CursorDef("T003E3", "SELECT [Incidentes_Codigo], [Incidentes_Data], [Incidentes_Descricao], [Incidentes_DemandaCod] AS Incidentes_DemandaCod FROM [ContagemResultadoIncidentes] WITH (NOLOCK) WHERE [Incidentes_Codigo] = @Incidentes_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003E3,1,0,true,false )
             ,new CursorDef("T003E4", "SELECT [ContagemResultado_Demanda] AS Incidentes_DemandaNum FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @Incidentes_DemandaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003E4,1,0,true,false )
             ,new CursorDef("T003E5", "SELECT TM1.[Incidentes_Codigo], TM1.[Incidentes_Data], TM1.[Incidentes_Descricao], T2.[ContagemResultado_Demanda] AS Incidentes_DemandaNum, TM1.[Incidentes_DemandaCod] AS Incidentes_DemandaCod FROM ([ContagemResultadoIncidentes] TM1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[Incidentes_DemandaCod]) WHERE TM1.[Incidentes_Codigo] = @Incidentes_Codigo ORDER BY TM1.[Incidentes_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003E5,100,0,true,false )
             ,new CursorDef("T003E6", "SELECT [ContagemResultado_Demanda] AS Incidentes_DemandaNum FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @Incidentes_DemandaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003E6,1,0,true,false )
             ,new CursorDef("T003E7", "SELECT [Incidentes_Codigo] FROM [ContagemResultadoIncidentes] WITH (NOLOCK) WHERE [Incidentes_Codigo] = @Incidentes_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003E7,1,0,true,false )
             ,new CursorDef("T003E8", "SELECT TOP 1 [Incidentes_Codigo] FROM [ContagemResultadoIncidentes] WITH (NOLOCK) WHERE ( [Incidentes_Codigo] > @Incidentes_Codigo) ORDER BY [Incidentes_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003E8,1,0,true,true )
             ,new CursorDef("T003E9", "SELECT TOP 1 [Incidentes_Codigo] FROM [ContagemResultadoIncidentes] WITH (NOLOCK) WHERE ( [Incidentes_Codigo] < @Incidentes_Codigo) ORDER BY [Incidentes_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003E9,1,0,true,true )
             ,new CursorDef("T003E10", "INSERT INTO [ContagemResultadoIncidentes]([Incidentes_Data], [Incidentes_Descricao], [Incidentes_DemandaCod]) VALUES(@Incidentes_Data, @Incidentes_Descricao, @Incidentes_DemandaCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003E10)
             ,new CursorDef("T003E11", "UPDATE [ContagemResultadoIncidentes] SET [Incidentes_Data]=@Incidentes_Data, [Incidentes_Descricao]=@Incidentes_Descricao, [Incidentes_DemandaCod]=@Incidentes_DemandaCod  WHERE [Incidentes_Codigo] = @Incidentes_Codigo", GxErrorMask.GX_NOMASK,prmT003E11)
             ,new CursorDef("T003E12", "DELETE FROM [ContagemResultadoIncidentes]  WHERE [Incidentes_Codigo] = @Incidentes_Codigo", GxErrorMask.GX_NOMASK,prmT003E12)
             ,new CursorDef("T003E13", "SELECT [ContagemResultado_Demanda] AS Incidentes_DemandaNum FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @Incidentes_DemandaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003E13,1,0,true,false )
             ,new CursorDef("T003E14", "SELECT [Incidentes_Codigo] FROM [ContagemResultadoIncidentes] WITH (NOLOCK) ORDER BY [Incidentes_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003E14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
