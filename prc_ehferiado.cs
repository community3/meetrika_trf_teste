/*
               File: PRC_EhFeriado
        Description: Eh Feriado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:42.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_ehferiado : GXProcedure
   {
      public prc_ehferiado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_ehferiado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref DateTime aP0_DataHora ,
                           out bool aP1_EhFeriado )
      {
         this.AV12DataHora = aP0_DataHora;
         this.AV8EhFeriado = false ;
         initialize();
         executePrivate();
         aP0_DataHora=this.AV12DataHora;
         aP1_EhFeriado=this.AV8EhFeriado;
      }

      public bool executeUdp( ref DateTime aP0_DataHora )
      {
         this.AV12DataHora = aP0_DataHora;
         this.AV8EhFeriado = false ;
         initialize();
         executePrivate();
         aP0_DataHora=this.AV12DataHora;
         aP1_EhFeriado=this.AV8EhFeriado;
         return AV8EhFeriado ;
      }

      public void executeSubmit( ref DateTime aP0_DataHora ,
                                 out bool aP1_EhFeriado )
      {
         prc_ehferiado objprc_ehferiado;
         objprc_ehferiado = new prc_ehferiado();
         objprc_ehferiado.AV12DataHora = aP0_DataHora;
         objprc_ehferiado.AV8EhFeriado = false ;
         objprc_ehferiado.context.SetSubmitInitialConfig(context);
         objprc_ehferiado.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_ehferiado);
         aP0_DataHora=this.AV12DataHora;
         aP1_EhFeriado=this.AV8EhFeriado;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_ehferiado)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV13Feriado_Data = DateTimeUtil.ResetTime(AV12DataHora);
         /* Using cursor P007K2 */
         pr_default.execute(0, new Object[] {AV13Feriado_Data});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1175Feriado_Data = P007K2_A1175Feriado_Data[0];
            AV8EhFeriado = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13Feriado_Data = DateTime.MinValue;
         scmdbuf = "";
         P007K2_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         A1175Feriado_Data = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_ehferiado__default(),
            new Object[][] {
                new Object[] {
               P007K2_A1175Feriado_Data
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String scmdbuf ;
      private DateTime AV12DataHora ;
      private DateTime AV13Feriado_Data ;
      private DateTime A1175Feriado_Data ;
      private bool AV8EhFeriado ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private DateTime aP0_DataHora ;
      private IDataStoreProvider pr_default ;
      private DateTime[] P007K2_A1175Feriado_Data ;
      private bool aP1_EhFeriado ;
   }

   public class prc_ehferiado__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP007K2 ;
          prmP007K2 = new Object[] {
          new Object[] {"@AV13Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P007K2", "SELECT TOP 1 [Feriado_Data] FROM [Feriados] WITH (NOLOCK) WHERE [Feriado_Data] = @AV13Feriado_Data ORDER BY [Feriado_Data] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007K2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
       }
    }

 }

}
