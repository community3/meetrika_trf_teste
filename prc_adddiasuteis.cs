/*
               File: PRC_AddDiasUteis
        Description: Add Dias Uteis
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:43.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_adddiasuteis : GXProcedure
   {
      public prc_adddiasuteis( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_adddiasuteis( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( DateTime aP0_DataHora ,
                           short aP1_Dias ,
                           String aP2_TipoDias ,
                           out DateTime aP3_DataRetorno )
      {
         this.AV10DataHora = aP0_DataHora;
         this.AV8Dias = aP1_Dias;
         this.AV16TipoDias = aP2_TipoDias;
         this.AV13DataRetorno = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP3_DataRetorno=this.AV13DataRetorno;
      }

      public DateTime executeUdp( DateTime aP0_DataHora ,
                                  short aP1_Dias ,
                                  String aP2_TipoDias )
      {
         this.AV10DataHora = aP0_DataHora;
         this.AV8Dias = aP1_Dias;
         this.AV16TipoDias = aP2_TipoDias;
         this.AV13DataRetorno = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP3_DataRetorno=this.AV13DataRetorno;
         return AV13DataRetorno ;
      }

      public void executeSubmit( DateTime aP0_DataHora ,
                                 short aP1_Dias ,
                                 String aP2_TipoDias ,
                                 out DateTime aP3_DataRetorno )
      {
         prc_adddiasuteis objprc_adddiasuteis;
         objprc_adddiasuteis = new prc_adddiasuteis();
         objprc_adddiasuteis.AV10DataHora = aP0_DataHora;
         objprc_adddiasuteis.AV8Dias = aP1_Dias;
         objprc_adddiasuteis.AV16TipoDias = aP2_TipoDias;
         objprc_adddiasuteis.AV13DataRetorno = DateTime.MinValue ;
         objprc_adddiasuteis.context.SetSubmitInitialConfig(context);
         objprc_adddiasuteis.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_adddiasuteis);
         aP3_DataRetorno=this.AV13DataRetorno;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_adddiasuteis)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV13DataRetorno = AV10DataHora;
         if ( AV8Dias > 0 )
         {
            if ( StringUtil.StrCmp(AV16TipoDias, "C") == 0 )
            {
               AV13DataRetorno = DateTimeUtil.TAdd( AV13DataRetorno, 86400*(AV8Dias));
            }
            else
            {
               while ( new prc_ehferiado(context).executeUdp( ref  AV13DataRetorno) || ( DateTimeUtil.Dow( AV13DataRetorno) == 1 ) || ( DateTimeUtil.Dow( AV13DataRetorno) == 7 ) )
               {
                  AV13DataRetorno = DateTimeUtil.TAdd( AV13DataRetorno, 86400*(1));
               }
               AV14d = 1;
               while ( AV14d <= AV8Dias )
               {
                  AV13DataRetorno = DateTimeUtil.TAdd( AV13DataRetorno, 86400*(1));
                  while ( new prc_ehferiado(context).executeUdp( ref  AV13DataRetorno) || ( DateTimeUtil.Dow( AV13DataRetorno) == 1 ) || ( DateTimeUtil.Dow( AV13DataRetorno) == 7 ) )
                  {
                     AV13DataRetorno = DateTimeUtil.TAdd( AV13DataRetorno, 86400*(1));
                  }
                  AV14d = (short)(AV14d+1);
               }
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8Dias ;
      private short AV14d ;
      private String AV16TipoDias ;
      private DateTime AV10DataHora ;
      private DateTime AV13DataRetorno ;
      private DateTime aP3_DataRetorno ;
   }

}
