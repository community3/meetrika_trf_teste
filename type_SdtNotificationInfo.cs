/*
               File: type_SdtNotificationInfo
        Description: NotificationInfo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/4/2020 0:22:42.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "NotificationInfo" )]
   [XmlType(TypeName =  "NotificationInfo" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtNotificationInfo : GxUserType
   {
      public SdtNotificationInfo( )
      {
         /* Constructor for serialization */
         gxTv_SdtNotificationInfo_Id = "";
         gxTv_SdtNotificationInfo_Object = "";
         gxTv_SdtNotificationInfo_Message = "";
      }

      public SdtNotificationInfo( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtNotificationInfo deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtNotificationInfo)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtNotificationInfo obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Object = deserialized.gxTpr_Object;
         obj.gxTpr_Message = deserialized.gxTpr_Message;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Id") )
               {
                  gxTv_SdtNotificationInfo_Id = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Object") )
               {
                  gxTv_SdtNotificationInfo_Object = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Message") )
               {
                  gxTv_SdtNotificationInfo_Message = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "NotificationInfo";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Id", StringUtil.RTrim( gxTv_SdtNotificationInfo_Id));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Object", StringUtil.RTrim( gxTv_SdtNotificationInfo_Object));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Message", StringUtil.RTrim( gxTv_SdtNotificationInfo_Message));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Id", gxTv_SdtNotificationInfo_Id, false);
         AddObjectProperty("Object", gxTv_SdtNotificationInfo_Object, false);
         AddObjectProperty("Message", gxTv_SdtNotificationInfo_Message, false);
         return  ;
      }

      [  SoapElement( ElementName = "Id" )]
      [  XmlElement( ElementName = "Id"   )]
      public String gxTpr_Id
      {
         get {
            return gxTv_SdtNotificationInfo_Id ;
         }

         set {
            gxTv_SdtNotificationInfo_Id = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Object" )]
      [  XmlElement( ElementName = "Object"   )]
      public String gxTpr_Object
      {
         get {
            return gxTv_SdtNotificationInfo_Object ;
         }

         set {
            gxTv_SdtNotificationInfo_Object = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Message" )]
      [  XmlElement( ElementName = "Message"   )]
      public String gxTpr_Message
      {
         get {
            return gxTv_SdtNotificationInfo_Message ;
         }

         set {
            gxTv_SdtNotificationInfo_Message = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtNotificationInfo_Id = "";
         gxTv_SdtNotificationInfo_Object = "";
         gxTv_SdtNotificationInfo_Message = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtNotificationInfo_Id ;
      protected String gxTv_SdtNotificationInfo_Object ;
      protected String sTagName ;
      protected String gxTv_SdtNotificationInfo_Message ;
   }

   [DataContract(Name = @"NotificationInfo", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtNotificationInfo_RESTInterface : GxGenericCollectionItem<SdtNotificationInfo>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtNotificationInfo_RESTInterface( ) : base()
      {
      }

      public SdtNotificationInfo_RESTInterface( SdtNotificationInfo psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Id" , Order = 0 )]
      public String gxTpr_Id
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Id) ;
         }

         set {
            sdt.gxTpr_Id = (String)(value);
         }

      }

      [DataMember( Name = "Object" , Order = 1 )]
      public String gxTpr_Object
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Object) ;
         }

         set {
            sdt.gxTpr_Object = (String)(value);
         }

      }

      [DataMember( Name = "Message" , Order = 2 )]
      public String gxTpr_Message
      {
         get {
            return sdt.gxTpr_Message ;
         }

         set {
            sdt.gxTpr_Message = (String)(value);
         }

      }

      public SdtNotificationInfo sdt
      {
         get {
            return (SdtNotificationInfo)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtNotificationInfo() ;
         }
      }

   }

}
