/*
               File: ServicoProcessoServico1WC
        Description: Servico Processo Servico1 WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:15:46.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicoprocessoservico1wc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public servicoprocessoservico1wc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public servicoprocessoservico1wc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Servico_Vinculado )
      {
         this.AV7Servico_Vinculado = aP0_Servico_Vinculado;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbServico_UORespExclusiva = new GXCombobox();
         cmbServico_Terceriza = new GXCombobox();
         cmbServico_Tela = new GXCombobox();
         cmbServico_Atende = new GXCombobox();
         cmbServico_ObrigaValores = new GXCombobox();
         cmbServico_ObjetoControle = new GXCombobox();
         cmbServico_TipoHierarquia = new GXCombobox();
         cmbServico_Ativo = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Servico_Vinculado = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Vinculado), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Servico_Vinculado});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_80 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_80_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_80_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV18Servico_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Servico_Nome1", AV18Servico_Nome1);
                  AV20DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
                  AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22Servico_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Servico_Nome2", AV22Servico_Nome2);
                  AV24DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                  AV26Servico_Nome3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Servico_Nome3", AV26Servico_Nome3);
                  AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
                  AV23DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV7Servico_Vinculado = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Vinculado), 6, 0)));
                  AV37Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV28DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
                  AV27DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
                  A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV30ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30ServicoGrupo_Codigo), 6, 0)));
                  A631Servico_Vinculado = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n631Servico_Vinculado = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Servico_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Servico_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7Servico_Vinculado, AV37Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A155Servico_Codigo, AV30ServicoGrupo_Codigo, A631Servico_Vinculado, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  forbiddenHiddens = sPrefix + "hsh" + "ServicoProcessoServico1WC";
                  forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9");
                  GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
                  GXUtil.WriteLog("servicoprocessoservico1wc:[SendSecurityCheck value for]"+"Servico_Vinculado:"+context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9"));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PALP2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV37Pgmname = "ServicoProcessoServico1WC";
               context.Gx_err = 0;
               WSLP2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Servico Processo Servico1 WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202052118154690");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicoprocessoservico1wc.aspx") + "?" + UrlEncode("" +AV7Servico_Vinculado)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSERVICO_NOME1", StringUtil.RTrim( AV18Servico_Nome1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSERVICO_NOME2", StringUtil.RTrim( AV22Servico_Nome2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV24DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vSERVICO_NOME3", StringUtil.RTrim( AV26Servico_Nome3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV23DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_80", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_80), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Servico_Vinculado", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Servico_Vinculado), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSERVICO_VINCULADO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Servico_Vinculado), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV37Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV28DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV27DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"vSERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ServicoProcessoServico1WC";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("servicoprocessoservico1wc:[SendSecurityCheck value for]"+"Servico_Vinculado:"+context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormLP2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("servicoprocessoservico1wc.js", "?202052118154699");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ServicoProcessoServico1WC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servico Processo Servico1 WC" ;
      }

      protected void WBLP0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "servicoprocessoservico1wc.aspx");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_LP2( true) ;
         }
         else
         {
            wb_table1_2_LP2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_LP2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Vinculado_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A631Servico_Vinculado), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Vinculado_Jsonclick, 0, "Attribute", "", "", "", edtServico_Vinculado_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoProcessoServico1WC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(107, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV23DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(108, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"");
         }
         wbLoad = true;
      }

      protected void STARTLP2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Servico Processo Servico1 WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPLP0( ) ;
            }
         }
      }

      protected void WSLP2( )
      {
         STARTLP2( ) ;
         EVTLP2( ) ;
      }

      protected void EVTLP2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11LP2 */
                                    E11LP2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12LP2 */
                                    E12LP2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13LP2 */
                                    E13LP2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14LP2 */
                                    E14LP2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15LP2 */
                                    E15LP2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16LP2 */
                                    E16LP2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17LP2 */
                                    E17LP2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18LP2 */
                                    E18LP2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19LP2 */
                                    E19LP2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20LP2 */
                                    E20LP2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21LP2 */
                                    E21LP2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLP0( ) ;
                              }
                              nGXsfl_80_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
                              SubsflControlProps_802( ) ;
                              AV29Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV35Update_GXI : context.convertURL( context.PathToRelativeUrl( AV29Update))));
                              AV31Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Delete)) ? AV36Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV31Delete))));
                              A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
                              A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
                              A156Servico_Descricao = cgiGet( edtServico_Descricao_Internalname);
                              n156Servico_Descricao = false;
                              A605Servico_Sigla = StringUtil.Upper( cgiGet( edtServico_Sigla_Internalname));
                              A157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoGrupo_Codigo_Internalname), ",", "."));
                              A633Servico_UO = (int)(context.localUtil.CToN( cgiGet( edtServico_UO_Internalname), ",", "."));
                              n633Servico_UO = false;
                              cmbServico_UORespExclusiva.Name = cmbServico_UORespExclusiva_Internalname;
                              cmbServico_UORespExclusiva.CurrentValue = cgiGet( cmbServico_UORespExclusiva_Internalname);
                              A1077Servico_UORespExclusiva = StringUtil.StrToBool( cgiGet( cmbServico_UORespExclusiva_Internalname));
                              n1077Servico_UORespExclusiva = false;
                              A640Servico_Vinculados = (short)(context.localUtil.CToN( cgiGet( edtServico_Vinculados_Internalname), ",", "."));
                              A641Servico_VincDesc = cgiGet( edtServico_VincDesc_Internalname);
                              n641Servico_VincDesc = false;
                              cmbServico_Terceriza.Name = cmbServico_Terceriza_Internalname;
                              cmbServico_Terceriza.CurrentValue = cgiGet( cmbServico_Terceriza_Internalname);
                              A889Servico_Terceriza = StringUtil.StrToBool( cgiGet( cmbServico_Terceriza_Internalname));
                              n889Servico_Terceriza = false;
                              cmbServico_Tela.Name = cmbServico_Tela_Internalname;
                              cmbServico_Tela.CurrentValue = cgiGet( cmbServico_Tela_Internalname);
                              A1061Servico_Tela = cgiGet( cmbServico_Tela_Internalname);
                              n1061Servico_Tela = false;
                              cmbServico_Atende.Name = cmbServico_Atende_Internalname;
                              cmbServico_Atende.CurrentValue = cgiGet( cmbServico_Atende_Internalname);
                              A1072Servico_Atende = cgiGet( cmbServico_Atende_Internalname);
                              n1072Servico_Atende = false;
                              cmbServico_ObrigaValores.Name = cmbServico_ObrigaValores_Internalname;
                              cmbServico_ObrigaValores.CurrentValue = cgiGet( cmbServico_ObrigaValores_Internalname);
                              A1429Servico_ObrigaValores = cgiGet( cmbServico_ObrigaValores_Internalname);
                              n1429Servico_ObrigaValores = false;
                              cmbServico_ObjetoControle.Name = cmbServico_ObjetoControle_Internalname;
                              cmbServico_ObjetoControle.CurrentValue = cgiGet( cmbServico_ObjetoControle_Internalname);
                              A1436Servico_ObjetoControle = cgiGet( cmbServico_ObjetoControle_Internalname);
                              cmbServico_TipoHierarquia.Name = cmbServico_TipoHierarquia_Internalname;
                              cmbServico_TipoHierarquia.CurrentValue = cgiGet( cmbServico_TipoHierarquia_Internalname);
                              A1530Servico_TipoHierarquia = (short)(NumberUtil.Val( cgiGet( cmbServico_TipoHierarquia_Internalname), "."));
                              n1530Servico_TipoHierarquia = false;
                              A1534Servico_PercTmp = (short)(context.localUtil.CToN( cgiGet( edtServico_PercTmp_Internalname), ",", "."));
                              n1534Servico_PercTmp = false;
                              A1535Servico_PercPgm = (short)(context.localUtil.CToN( cgiGet( edtServico_PercPgm_Internalname), ",", "."));
                              n1535Servico_PercPgm = false;
                              A1536Servico_PercCnc = (short)(context.localUtil.CToN( cgiGet( edtServico_PercCnc_Internalname), ",", "."));
                              n1536Servico_PercCnc = false;
                              A1545Servico_Anterior = (int)(context.localUtil.CToN( cgiGet( edtServico_Anterior_Internalname), ",", "."));
                              n1545Servico_Anterior = false;
                              A1546Servico_Posterior = (int)(context.localUtil.CToN( cgiGet( edtServico_Posterior_Internalname), ",", "."));
                              n1546Servico_Posterior = false;
                              A1551Servico_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtServico_Responsavel_Internalname), ",", "."));
                              cmbServico_Ativo.Name = cmbServico_Ativo_Internalname;
                              cmbServico_Ativo.CurrentValue = cgiGet( cmbServico_Ativo_Internalname);
                              A632Servico_Ativo = StringUtil.StrToBool( cgiGet( cmbServico_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E22LP2 */
                                          E22LP2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23LP2 */
                                          E23LP2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24LP2 */
                                          E24LP2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Servico_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSERVICO_NOME1"), AV18Servico_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Servico_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSERVICO_NOME2"), AV22Servico_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Servico_nome3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSERVICO_NOME3"), AV26Servico_Nome3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPLP0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WELP2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormLP2( ) ;
            }
         }
      }

      protected void PALP2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SERVICO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SERVICO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SERVICO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "SERVICO_UORESPEXCLUSIVA_" + sGXsfl_80_idx;
            cmbServico_UORespExclusiva.Name = GXCCtl;
            cmbServico_UORespExclusiva.WebTags = "";
            cmbServico_UORespExclusiva.addItem(StringUtil.BoolToStr( false), "Herdada pelas sub-unidades", 0);
            cmbServico_UORespExclusiva.addItem(StringUtil.BoolToStr( true), "Exclusiva", 0);
            if ( cmbServico_UORespExclusiva.ItemCount > 0 )
            {
               A1077Servico_UORespExclusiva = StringUtil.StrToBool( cmbServico_UORespExclusiva.getValidValue(StringUtil.BoolToStr( A1077Servico_UORespExclusiva)));
               n1077Servico_UORespExclusiva = false;
            }
            GXCCtl = "SERVICO_TERCERIZA_" + sGXsfl_80_idx;
            cmbServico_Terceriza.Name = GXCCtl;
            cmbServico_Terceriza.WebTags = "";
            cmbServico_Terceriza.addItem(StringUtil.BoolToStr( false), "Desconhecido", 0);
            cmbServico_Terceriza.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbServico_Terceriza.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbServico_Terceriza.ItemCount > 0 )
            {
               A889Servico_Terceriza = StringUtil.StrToBool( cmbServico_Terceriza.getValidValue(StringUtil.BoolToStr( A889Servico_Terceriza)));
               n889Servico_Terceriza = false;
            }
            GXCCtl = "SERVICO_TELA_" + sGXsfl_80_idx;
            cmbServico_Tela.Name = GXCCtl;
            cmbServico_Tela.WebTags = "";
            cmbServico_Tela.addItem("", "(Nenhuma)", 0);
            cmbServico_Tela.addItem("CNT", "Contagem", 0);
            cmbServico_Tela.addItem("CHK", "Check List", 0);
            if ( cmbServico_Tela.ItemCount > 0 )
            {
               A1061Servico_Tela = cmbServico_Tela.getValidValue(A1061Servico_Tela);
               n1061Servico_Tela = false;
            }
            GXCCtl = "SERVICO_ATENDE_" + sGXsfl_80_idx;
            cmbServico_Atende.Name = GXCCtl;
            cmbServico_Atende.WebTags = "";
            cmbServico_Atende.addItem("S", "Software", 0);
            cmbServico_Atende.addItem("H", "Hardware", 0);
            if ( cmbServico_Atende.ItemCount > 0 )
            {
               A1072Servico_Atende = cmbServico_Atende.getValidValue(A1072Servico_Atende);
               n1072Servico_Atende = false;
            }
            GXCCtl = "SERVICO_OBRIGAVALORES_" + sGXsfl_80_idx;
            cmbServico_ObrigaValores.Name = GXCCtl;
            cmbServico_ObrigaValores.WebTags = "";
            cmbServico_ObrigaValores.addItem("", "Nenhum", 0);
            cmbServico_ObrigaValores.addItem("L", "Valor L�quido", 0);
            cmbServico_ObrigaValores.addItem("B", "Valor Bruto", 0);
            cmbServico_ObrigaValores.addItem("A", "Ambos", 0);
            if ( cmbServico_ObrigaValores.ItemCount > 0 )
            {
               A1429Servico_ObrigaValores = cmbServico_ObrigaValores.getValidValue(A1429Servico_ObrigaValores);
               n1429Servico_ObrigaValores = false;
            }
            GXCCtl = "SERVICO_OBJETOCONTROLE_" + sGXsfl_80_idx;
            cmbServico_ObjetoControle.Name = GXCCtl;
            cmbServico_ObjetoControle.WebTags = "";
            cmbServico_ObjetoControle.addItem("SIS", "Sistema", 0);
            cmbServico_ObjetoControle.addItem("PRC", "Processo", 0);
            cmbServico_ObjetoControle.addItem("PRD", "Produto", 0);
            if ( cmbServico_ObjetoControle.ItemCount > 0 )
            {
               A1436Servico_ObjetoControle = cmbServico_ObjetoControle.getValidValue(A1436Servico_ObjetoControle);
            }
            GXCCtl = "SERVICO_TIPOHIERARQUIA_" + sGXsfl_80_idx;
            cmbServico_TipoHierarquia.Name = GXCCtl;
            cmbServico_TipoHierarquia.WebTags = "";
            cmbServico_TipoHierarquia.addItem("1", "Prim�rio", 0);
            cmbServico_TipoHierarquia.addItem("2", "Secund�rio", 0);
            if ( cmbServico_TipoHierarquia.ItemCount > 0 )
            {
               A1530Servico_TipoHierarquia = (short)(NumberUtil.Val( cmbServico_TipoHierarquia.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0))), "."));
               n1530Servico_TipoHierarquia = false;
            }
            GXCCtl = "SERVICO_ATIVO_" + sGXsfl_80_idx;
            cmbServico_Ativo.Name = GXCCtl;
            cmbServico_Ativo.WebTags = "";
            cmbServico_Ativo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbServico_Ativo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbServico_Ativo.ItemCount > 0 )
            {
               A632Servico_Ativo = StringUtil.StrToBool( cmbServico_Ativo.getValidValue(StringUtil.BoolToStr( A632Servico_Ativo)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_802( ) ;
         while ( nGXsfl_80_idx <= nRC_GXsfl_80 )
         {
            sendrow_802( ) ;
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18Servico_Nome1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22Servico_Nome2 ,
                                       String AV24DynamicFiltersSelector3 ,
                                       short AV25DynamicFiltersOperator3 ,
                                       String AV26Servico_Nome3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV23DynamicFiltersEnabled3 ,
                                       int AV7Servico_Vinculado ,
                                       String AV37Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV28DynamicFiltersIgnoreFirst ,
                                       bool AV27DynamicFiltersRemoving ,
                                       int A155Servico_Codigo ,
                                       int AV30ServicoGrupo_Codigo ,
                                       int A631Servico_Vinculado ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFLP2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_NOME", StringUtil.RTrim( A608Servico_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_DESCRICAO", GetSecureSignedToken( sPrefix, A156Servico_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_DESCRICAO", A156Servico_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOGRUPO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_UO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A633Servico_UO), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_UO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A633Servico_UO), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_UORESPEXCLUSIVA", GetSecureSignedToken( sPrefix, A1077Servico_UORespExclusiva));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_UORESPEXCLUSIVA", StringUtil.BoolToStr( A1077Servico_UORespExclusiva));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_VINCULADOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A640Servico_Vinculados), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_VINCULADOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A640Servico_Vinculados), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_TERCERIZA", GetSecureSignedToken( sPrefix, A889Servico_Terceriza));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_TERCERIZA", StringUtil.BoolToStr( A889Servico_Terceriza));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1061Servico_Tela, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_TELA", StringUtil.RTrim( A1061Servico_Tela));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ATENDE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1072Servico_Atende, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_ATENDE", StringUtil.RTrim( A1072Servico_Atende));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_OBRIGAVALORES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1429Servico_ObrigaValores, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_OBRIGAVALORES", StringUtil.RTrim( A1429Servico_ObrigaValores));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_OBJETOCONTROLE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_OBJETOCONTROLE", StringUtil.RTrim( A1436Servico_ObjetoControle));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_TIPOHIERARQUIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_TIPOHIERARQUIA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1530Servico_TipoHierarquia), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PERCTMP", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_PERCTMP", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1534Servico_PercTmp), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PERCPGM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_PERCPGM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1535Servico_PercPgm), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PERCCNC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_PERCCNC", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1536Servico_PercCnc), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ANTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1545Servico_Anterior), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_ANTERIOR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1545Servico_Anterior), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_POSTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1546Servico_Posterior), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_POSTERIOR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1546Servico_Posterior), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_RESPONSAVEL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1551Servico_Responsavel), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1551Servico_Responsavel), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ATIVO", GetSecureSignedToken( sPrefix, A632Servico_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICO_ATIVO", StringUtil.BoolToStr( A632Servico_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFLP2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV37Pgmname = "ServicoProcessoServico1WC";
         context.Gx_err = 0;
      }

      protected void RFLP2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 80;
         /* Execute user event: E23LP2 */
         E23LP2 ();
         nGXsfl_80_idx = 1;
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_802( ) ;
         nGXsfl_80_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_802( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18Servico_Nome1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV22Servico_Nome2 ,
                                                 AV23DynamicFiltersEnabled3 ,
                                                 AV24DynamicFiltersSelector3 ,
                                                 AV25DynamicFiltersOperator3 ,
                                                 AV26Servico_Nome3 ,
                                                 A608Servico_Nome ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A631Servico_Vinculado ,
                                                 AV7Servico_Vinculado },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV18Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Servico_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Servico_Nome1", AV18Servico_Nome1);
            lV18Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Servico_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Servico_Nome1", AV18Servico_Nome1);
            lV22Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Servico_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Servico_Nome2", AV22Servico_Nome2);
            lV22Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Servico_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Servico_Nome2", AV22Servico_Nome2);
            lV26Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV26Servico_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Servico_Nome3", AV26Servico_Nome3);
            lV26Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV26Servico_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Servico_Nome3", AV26Servico_Nome3);
            /* Using cursor H00LP2 */
            pr_default.execute(0, new Object[] {AV7Servico_Vinculado, lV18Servico_Nome1, lV18Servico_Nome1, lV22Servico_Nome2, lV22Servico_Nome2, lV26Servico_Nome3, lV26Servico_Nome3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_80_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A631Servico_Vinculado = H00LP2_A631Servico_Vinculado[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
               n631Servico_Vinculado = H00LP2_n631Servico_Vinculado[0];
               A632Servico_Ativo = H00LP2_A632Servico_Ativo[0];
               A1546Servico_Posterior = H00LP2_A1546Servico_Posterior[0];
               n1546Servico_Posterior = H00LP2_n1546Servico_Posterior[0];
               A1545Servico_Anterior = H00LP2_A1545Servico_Anterior[0];
               n1545Servico_Anterior = H00LP2_n1545Servico_Anterior[0];
               A1536Servico_PercCnc = H00LP2_A1536Servico_PercCnc[0];
               n1536Servico_PercCnc = H00LP2_n1536Servico_PercCnc[0];
               A1535Servico_PercPgm = H00LP2_A1535Servico_PercPgm[0];
               n1535Servico_PercPgm = H00LP2_n1535Servico_PercPgm[0];
               A1534Servico_PercTmp = H00LP2_A1534Servico_PercTmp[0];
               n1534Servico_PercTmp = H00LP2_n1534Servico_PercTmp[0];
               A1530Servico_TipoHierarquia = H00LP2_A1530Servico_TipoHierarquia[0];
               n1530Servico_TipoHierarquia = H00LP2_n1530Servico_TipoHierarquia[0];
               A1436Servico_ObjetoControle = H00LP2_A1436Servico_ObjetoControle[0];
               A1429Servico_ObrigaValores = H00LP2_A1429Servico_ObrigaValores[0];
               n1429Servico_ObrigaValores = H00LP2_n1429Servico_ObrigaValores[0];
               A1072Servico_Atende = H00LP2_A1072Servico_Atende[0];
               n1072Servico_Atende = H00LP2_n1072Servico_Atende[0];
               A1061Servico_Tela = H00LP2_A1061Servico_Tela[0];
               n1061Servico_Tela = H00LP2_n1061Servico_Tela[0];
               A889Servico_Terceriza = H00LP2_A889Servico_Terceriza[0];
               n889Servico_Terceriza = H00LP2_n889Servico_Terceriza[0];
               A641Servico_VincDesc = H00LP2_A641Servico_VincDesc[0];
               n641Servico_VincDesc = H00LP2_n641Servico_VincDesc[0];
               A1077Servico_UORespExclusiva = H00LP2_A1077Servico_UORespExclusiva[0];
               n1077Servico_UORespExclusiva = H00LP2_n1077Servico_UORespExclusiva[0];
               A633Servico_UO = H00LP2_A633Servico_UO[0];
               n633Servico_UO = H00LP2_n633Servico_UO[0];
               A157ServicoGrupo_Codigo = H00LP2_A157ServicoGrupo_Codigo[0];
               A605Servico_Sigla = H00LP2_A605Servico_Sigla[0];
               A156Servico_Descricao = H00LP2_A156Servico_Descricao[0];
               n156Servico_Descricao = H00LP2_n156Servico_Descricao[0];
               A608Servico_Nome = H00LP2_A608Servico_Nome[0];
               A155Servico_Codigo = H00LP2_A155Servico_Codigo[0];
               A641Servico_VincDesc = H00LP2_A641Servico_VincDesc[0];
               n641Servico_VincDesc = H00LP2_n641Servico_VincDesc[0];
               GXt_int1 = A640Servico_Vinculados;
               new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int1) ;
               A640Servico_Vinculados = GXt_int1;
               GXt_int2 = A1551Servico_Responsavel;
               new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
               A1551Servico_Responsavel = GXt_int2;
               /* Execute user event: E24LP2 */
               E24LP2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 80;
            WBLP0( ) ;
         }
         nGXsfl_80_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18Servico_Nome1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21DynamicFiltersOperator2 ,
                                              AV22Servico_Nome2 ,
                                              AV23DynamicFiltersEnabled3 ,
                                              AV24DynamicFiltersSelector3 ,
                                              AV25DynamicFiltersOperator3 ,
                                              AV26Servico_Nome3 ,
                                              A608Servico_Nome ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A631Servico_Vinculado ,
                                              AV7Servico_Vinculado },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV18Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Servico_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Servico_Nome1", AV18Servico_Nome1);
         lV18Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Servico_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Servico_Nome1", AV18Servico_Nome1);
         lV22Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Servico_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Servico_Nome2", AV22Servico_Nome2);
         lV22Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Servico_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Servico_Nome2", AV22Servico_Nome2);
         lV26Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV26Servico_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Servico_Nome3", AV26Servico_Nome3);
         lV26Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV26Servico_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Servico_Nome3", AV26Servico_Nome3);
         /* Using cursor H00LP3 */
         pr_default.execute(1, new Object[] {AV7Servico_Vinculado, lV18Servico_Nome1, lV18Servico_Nome1, lV22Servico_Nome2, lV22Servico_Nome2, lV26Servico_Nome3, lV26Servico_Nome3});
         GRID_nRecordCount = H00LP3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Servico_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Servico_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7Servico_Vinculado, AV37Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A155Servico_Codigo, AV30ServicoGrupo_Codigo, A631Servico_Vinculado, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Servico_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Servico_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7Servico_Vinculado, AV37Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A155Servico_Codigo, AV30ServicoGrupo_Codigo, A631Servico_Vinculado, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Servico_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Servico_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7Servico_Vinculado, AV37Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A155Servico_Codigo, AV30ServicoGrupo_Codigo, A631Servico_Vinculado, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Servico_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Servico_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7Servico_Vinculado, AV37Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A155Servico_Codigo, AV30ServicoGrupo_Codigo, A631Servico_Vinculado, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Servico_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Servico_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7Servico_Vinculado, AV37Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A155Servico_Codigo, AV30ServicoGrupo_Codigo, A631Servico_Vinculado, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPLP0( )
      {
         /* Before Start, stand alone formulas. */
         AV37Pgmname = "ServicoProcessoServico1WC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22LP2 */
         E22LP2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18Servico_Nome1 = StringUtil.Upper( cgiGet( edtavServico_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Servico_Nome1", AV18Servico_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22Servico_Nome2 = StringUtil.Upper( cgiGet( edtavServico_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Servico_Nome2", AV22Servico_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV24DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            AV26Servico_Nome3 = StringUtil.Upper( cgiGet( edtavServico_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Servico_Nome3", AV26Servico_Nome3);
            A631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( edtServico_Vinculado_Internalname), ",", "."));
            n631Servico_Vinculado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV23DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_80 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_80"), ",", "."));
            wcpOAV7Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Servico_Vinculado"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ServicoProcessoServico1WC";
            A631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( edtServico_Vinculado_Internalname), ",", "."));
            n631Servico_Vinculado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("servicoprocessoservico1wc:[SecurityCheckFailed value for]"+"Servico_Vinculado:"+context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSERVICO_NOME1"), AV18Servico_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSERVICO_NOME2"), AV22Servico_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vSERVICO_NOME3"), AV26Servico_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22LP2 */
         E22LP2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22LP2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtServico_Vinculado_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Vinculado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Vinculado_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Descri��o de Servi�o", 0);
         cmbavOrderedby.addItem("3", "Sigla", 0);
         cmbavOrderedby.addItem("4", "Unidade Organizacional", 0);
         cmbavOrderedby.addItem("5", "Responsabilidade", 0);
         cmbavOrderedby.addItem("6", "Descri��o", 0);
         cmbavOrderedby.addItem("7", "Pode ser tercerizado?", 0);
         cmbavOrderedby.addItem("8", "Tela", 0);
         cmbavOrderedby.addItem("9", "Atende", 0);
         cmbavOrderedby.addItem("10", "Valores", 0);
         cmbavOrderedby.addItem("11", "Controle", 0);
         cmbavOrderedby.addItem("12", "de Hierarquia", 0);
         cmbavOrderedby.addItem("13", "de Tempo", 0);
         cmbavOrderedby.addItem("14", "de Pagamento", 0);
         cmbavOrderedby.addItem("15", "de Cancelamento", 0);
         cmbavOrderedby.addItem("16", "Anterior", 0);
         cmbavOrderedby.addItem("17", "Posterior", 0);
         cmbavOrderedby.addItem("18", "Ativo?", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E23LP2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtServico_Nome_Titleformat = 2;
         edtServico_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV14OrderedBy==1) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Nome_Internalname, "Title", edtServico_Nome_Title);
         edtServico_Descricao_Titleformat = 2;
         edtServico_Descricao_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV14OrderedBy==2) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Descri��o de Servi�o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Descricao_Internalname, "Title", edtServico_Descricao_Title);
         edtServico_Sigla_Titleformat = 2;
         edtServico_Sigla_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV14OrderedBy==3) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Sigla", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Sigla_Internalname, "Title", edtServico_Sigla_Title);
         edtServico_UO_Titleformat = 2;
         edtServico_UO_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV14OrderedBy==4) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Unidade Organizacional", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_UO_Internalname, "Title", edtServico_UO_Title);
         cmbServico_UORespExclusiva_Titleformat = 2;
         cmbServico_UORespExclusiva.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV14OrderedBy==5) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Responsabilidade", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_UORespExclusiva_Internalname, "Title", cmbServico_UORespExclusiva.Title.Text);
         edtServico_VincDesc_Titleformat = 2;
         edtServico_VincDesc_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV14OrderedBy==6) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Descri��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_VincDesc_Internalname, "Title", edtServico_VincDesc_Title);
         cmbServico_Terceriza_Titleformat = 2;
         cmbServico_Terceriza.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV14OrderedBy==7) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Pode ser tercerizado?", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Terceriza_Internalname, "Title", cmbServico_Terceriza.Title.Text);
         cmbServico_Tela_Titleformat = 2;
         cmbServico_Tela.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV14OrderedBy==8) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tela", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Tela_Internalname, "Title", cmbServico_Tela.Title.Text);
         cmbServico_Atende_Titleformat = 2;
         cmbServico_Atende.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV14OrderedBy==9) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Atende", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Atende_Internalname, "Title", cmbServico_Atende.Title.Text);
         cmbServico_ObrigaValores_Titleformat = 2;
         cmbServico_ObrigaValores.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 10);' >%5</span>", ((AV14OrderedBy==10) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Valores", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_ObrigaValores_Internalname, "Title", cmbServico_ObrigaValores.Title.Text);
         cmbServico_ObjetoControle_Titleformat = 2;
         cmbServico_ObjetoControle.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 11);' >%5</span>", ((AV14OrderedBy==11) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Controle", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_ObjetoControle_Internalname, "Title", cmbServico_ObjetoControle.Title.Text);
         cmbServico_TipoHierarquia_Titleformat = 2;
         cmbServico_TipoHierarquia.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 12);' >%5</span>", ((AV14OrderedBy==12) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "de Hierarquia", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_TipoHierarquia_Internalname, "Title", cmbServico_TipoHierarquia.Title.Text);
         edtServico_PercTmp_Titleformat = 2;
         edtServico_PercTmp_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 13);' >%5</span>", ((AV14OrderedBy==13) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "de Tempo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_PercTmp_Internalname, "Title", edtServico_PercTmp_Title);
         edtServico_PercPgm_Titleformat = 2;
         edtServico_PercPgm_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 14);' >%5</span>", ((AV14OrderedBy==14) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "de Pagamento", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_PercPgm_Internalname, "Title", edtServico_PercPgm_Title);
         edtServico_PercCnc_Titleformat = 2;
         edtServico_PercCnc_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 15);' >%5</span>", ((AV14OrderedBy==15) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "de Cancelamento", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_PercCnc_Internalname, "Title", edtServico_PercCnc_Title);
         edtServico_Anterior_Titleformat = 2;
         edtServico_Anterior_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 16);' >%5</span>", ((AV14OrderedBy==16) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Anterior", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Anterior_Internalname, "Title", edtServico_Anterior_Title);
         edtServico_Posterior_Titleformat = 2;
         edtServico_Posterior_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 17);' >%5</span>", ((AV14OrderedBy==17) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Posterior", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Posterior_Internalname, "Title", edtServico_Posterior_Title);
         cmbServico_Ativo_Titleformat = 2;
         cmbServico_Ativo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 18);' >%5</span>", ((AV14OrderedBy==18) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ativo?", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Ativo_Internalname, "Title", cmbServico_Ativo.Title.Text);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      private void E24LP2( )
      {
         /* Grid_Load Routine */
         AV29Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV29Update);
         AV35Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode("" +AV30ServicoGrupo_Codigo);
         AV31Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV31Delete);
         AV36Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode("" +AV30ServicoGrupo_Codigo);
         edtServico_Descricao_Link = formatLink("viewservico.aspx") + "?" + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtServico_VincDesc_Link = formatLink("viewservico.aspx") + "?" + UrlEncode("" +A631Servico_Vinculado) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 80;
         }
         sendrow_802( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_80_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(80, GridRow);
         }
      }

      protected void E11LP2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E17LP2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E12LP2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Servico_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Servico_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7Servico_Vinculado, AV37Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A155Servico_Codigo, AV30ServicoGrupo_Codigo, A631Servico_Vinculado, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E18LP2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19LP2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV23DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E13LP2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Servico_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Servico_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7Servico_Vinculado, AV37Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A155Servico_Codigo, AV30ServicoGrupo_Codigo, A631Servico_Vinculado, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20LP2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14LP2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Servico_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Servico_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV7Servico_Vinculado, AV37Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A155Servico_Codigo, AV30ServicoGrupo_Codigo, A631Servico_Vinculado, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21LP2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15LP2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E16LP2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV30ServicoGrupo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavServico_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavServico_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavServico_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavServico_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S152( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "SERVICO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22Servico_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Servico_Nome2", AV22Servico_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         AV24DynamicFiltersSelector3 = "SERVICO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         AV25DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         AV26Servico_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Servico_Nome3", AV26Servico_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV16DynamicFiltersSelector1 = "SERVICO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV18Servico_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Servico_Nome1", AV18Servico_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV32Session.Get(AV37Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV37Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV32Session.Get(AV37Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18Servico_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Servico_Nome1", AV18Servico_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22Servico_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Servico_Nome2", AV22Servico_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S182 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV23DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV24DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SERVICO_NOME") == 0 )
                  {
                     AV25DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                     AV26Servico_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Servico_Nome3", AV26Servico_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S192 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV27DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV32Session.Get(AV37Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV37Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S142( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV28DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Servico_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18Servico_Nome1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Servico_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV22Servico_Nome2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV23DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV24DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Servico_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV26Servico_Nome3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV25DynamicFiltersOperator3;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV37Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Servico";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Servico_Vinculado";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Servico_Vinculado), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV32Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_LP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_LP2( true) ;
         }
         else
         {
            wb_table2_8_LP2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_LP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"80\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo de Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(380), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(570), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Grupo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_UO_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_UO_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_UO_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServico_UORespExclusiva_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServico_UORespExclusiva.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServico_UORespExclusiva.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Servico_Vinculados") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(570), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_VincDesc_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_VincDesc_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_VincDesc_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServico_Terceriza_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServico_Terceriza.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServico_Terceriza.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServico_Tela_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServico_Tela.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServico_Tela.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServico_Atende_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServico_Atende.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServico_Atende.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServico_ObrigaValores_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServico_ObrigaValores.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServico_ObrigaValores.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServico_ObjetoControle_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServico_ObjetoControle.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServico_ObjetoControle.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServico_TipoHierarquia_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServico_TipoHierarquia.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServico_TipoHierarquia.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(61), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_PercTmp_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_PercTmp_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_PercTmp_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(89), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_PercPgm_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_PercPgm_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_PercPgm_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"center"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(108), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_PercCnc_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_PercCnc_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_PercCnc_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Anterior_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Anterior_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Anterior_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Posterior_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Posterior_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Posterior_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "da Contratante") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServico_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServico_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServico_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A608Servico_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A156Servico_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtServico_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A605Servico_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A633Servico_UO), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_UO_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_UO_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1077Servico_UORespExclusiva));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServico_UORespExclusiva.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServico_UORespExclusiva_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A640Servico_Vinculados), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A641Servico_VincDesc);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_VincDesc_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_VincDesc_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtServico_VincDesc_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A889Servico_Terceriza));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServico_Terceriza.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServico_Terceriza_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1061Servico_Tela));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServico_Tela.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServico_Tela_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1072Servico_Atende));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServico_Atende.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServico_Atende_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1429Servico_ObrigaValores));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServico_ObrigaValores.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServico_ObrigaValores_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1436Servico_ObjetoControle));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServico_ObjetoControle.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServico_ObjetoControle_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1530Servico_TipoHierarquia), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServico_TipoHierarquia.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServico_TipoHierarquia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1534Servico_PercTmp), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_PercTmp_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_PercTmp_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1535Servico_PercPgm), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_PercPgm_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_PercPgm_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1536Servico_PercCnc), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_PercCnc_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_PercCnc_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1545Servico_Anterior), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Anterior_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Anterior_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1546Servico_Posterior), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Posterior_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Posterior_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1551Servico_Responsavel), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A632Servico_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServico_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServico_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 80 )
         {
            wbEnd = 0;
            nRC_GXsfl_80 = (short)(nGXsfl_80_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_LP2e( true) ;
         }
         else
         {
            wb_table1_2_LP2e( false) ;
         }
      }

      protected void wb_table2_8_LP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table3_11_LP2( true) ;
         }
         else
         {
            wb_table3_11_LP2( false) ;
         }
         return  ;
      }

      protected void wb_table3_11_LP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenar por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_ServicoProcessoServico1WC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table4_21_LP2( true) ;
         }
         else
         {
            wb_table4_21_LP2( false) ;
         }
         return  ;
      }

      protected void wb_table4_21_LP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_LP2e( true) ;
         }
         else
         {
            wb_table2_8_LP2e( false) ;
         }
      }

      protected void wb_table4_21_LP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table5_26_LP2( true) ;
         }
         else
         {
            wb_table5_26_LP2( false) ;
         }
         return  ;
      }

      protected void wb_table5_26_LP2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_21_LP2e( true) ;
         }
         else
         {
            wb_table4_21_LP2e( false) ;
         }
      }

      protected void wb_table5_26_LP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Procurar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_ServicoProcessoServico1WC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_35_LP2( true) ;
         }
         else
         {
            wb_table6_35_LP2( false) ;
         }
         return  ;
      }

      protected void wb_table6_35_LP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Add filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoProcessoServico1WC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remove filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Procurar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_ServicoProcessoServico1WC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_52_LP2( true) ;
         }
         else
         {
            wb_table7_52_LP2( false) ;
         }
         return  ;
      }

      protected void wb_table7_52_LP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Add filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoProcessoServico1WC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remove filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Procurar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV24DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_ServicoProcessoServico1WC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_69_LP2( true) ;
         }
         else
         {
            wb_table8_69_LP2( false) ;
         }
         return  ;
      }

      protected void wb_table8_69_LP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remove filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_26_LP2e( true) ;
         }
         else
         {
            wb_table5_26_LP2e( false) ;
         }
      }

      protected void wb_table8_69_LP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "", true, "HLP_ServicoProcessoServico1WC.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome3_Internalname, StringUtil.RTrim( AV26Servico_Nome3), StringUtil.RTrim( context.localUtil.Format( AV26Servico_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,74);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome3_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_69_LP2e( true) ;
         }
         else
         {
            wb_table8_69_LP2e( false) ;
         }
      }

      protected void wb_table7_52_LP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_ServicoProcessoServico1WC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome2_Internalname, StringUtil.RTrim( AV22Servico_Nome2), StringUtil.RTrim( context.localUtil.Format( AV22Servico_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome2_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_52_LP2e( true) ;
         }
         else
         {
            wb_table7_52_LP2e( false) ;
         }
      }

      protected void wb_table6_35_LP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_ServicoProcessoServico1WC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome1_Internalname, StringUtil.RTrim( AV18Servico_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Servico_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome1_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_35_LP2e( true) ;
         }
         else
         {
            wb_table6_35_LP2e( false) ;
         }
      }

      protected void wb_table3_11_LP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoProcessoServico1WC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_11_LP2e( true) ;
         }
         else
         {
            wb_table3_11_LP2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Servico_Vinculado = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Vinculado), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PALP2( ) ;
         WSLP2( ) ;
         WELP2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Servico_Vinculado = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PALP2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "servicoprocessoservico1wc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PALP2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Servico_Vinculado = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Vinculado), 6, 0)));
         }
         wcpOAV7Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Servico_Vinculado"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Servico_Vinculado != wcpOAV7Servico_Vinculado ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Servico_Vinculado = AV7Servico_Vinculado;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Servico_Vinculado = cgiGet( sPrefix+"AV7Servico_Vinculado_CTRL");
         if ( StringUtil.Len( sCtrlAV7Servico_Vinculado) > 0 )
         {
            AV7Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Servico_Vinculado), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Vinculado), 6, 0)));
         }
         else
         {
            AV7Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Servico_Vinculado_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PALP2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSLP2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSLP2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Servico_Vinculado_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Servico_Vinculado), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Servico_Vinculado)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Servico_Vinculado_CTRL", StringUtil.RTrim( sCtrlAV7Servico_Vinculado));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WELP2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202052118155052");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("servicoprocessoservico1wc.js", "?202052118155052");
            context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_802( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_80_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_80_idx;
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO_"+sGXsfl_80_idx;
         edtServico_Nome_Internalname = sPrefix+"SERVICO_NOME_"+sGXsfl_80_idx;
         edtServico_Descricao_Internalname = sPrefix+"SERVICO_DESCRICAO_"+sGXsfl_80_idx;
         edtServico_Sigla_Internalname = sPrefix+"SERVICO_SIGLA_"+sGXsfl_80_idx;
         edtServicoGrupo_Codigo_Internalname = sPrefix+"SERVICOGRUPO_CODIGO_"+sGXsfl_80_idx;
         edtServico_UO_Internalname = sPrefix+"SERVICO_UO_"+sGXsfl_80_idx;
         cmbServico_UORespExclusiva_Internalname = sPrefix+"SERVICO_UORESPEXCLUSIVA_"+sGXsfl_80_idx;
         edtServico_Vinculados_Internalname = sPrefix+"SERVICO_VINCULADOS_"+sGXsfl_80_idx;
         edtServico_VincDesc_Internalname = sPrefix+"SERVICO_VINCDESC_"+sGXsfl_80_idx;
         cmbServico_Terceriza_Internalname = sPrefix+"SERVICO_TERCERIZA_"+sGXsfl_80_idx;
         cmbServico_Tela_Internalname = sPrefix+"SERVICO_TELA_"+sGXsfl_80_idx;
         cmbServico_Atende_Internalname = sPrefix+"SERVICO_ATENDE_"+sGXsfl_80_idx;
         cmbServico_ObrigaValores_Internalname = sPrefix+"SERVICO_OBRIGAVALORES_"+sGXsfl_80_idx;
         cmbServico_ObjetoControle_Internalname = sPrefix+"SERVICO_OBJETOCONTROLE_"+sGXsfl_80_idx;
         cmbServico_TipoHierarquia_Internalname = sPrefix+"SERVICO_TIPOHIERARQUIA_"+sGXsfl_80_idx;
         edtServico_PercTmp_Internalname = sPrefix+"SERVICO_PERCTMP_"+sGXsfl_80_idx;
         edtServico_PercPgm_Internalname = sPrefix+"SERVICO_PERCPGM_"+sGXsfl_80_idx;
         edtServico_PercCnc_Internalname = sPrefix+"SERVICO_PERCCNC_"+sGXsfl_80_idx;
         edtServico_Anterior_Internalname = sPrefix+"SERVICO_ANTERIOR_"+sGXsfl_80_idx;
         edtServico_Posterior_Internalname = sPrefix+"SERVICO_POSTERIOR_"+sGXsfl_80_idx;
         edtServico_Responsavel_Internalname = sPrefix+"SERVICO_RESPONSAVEL_"+sGXsfl_80_idx;
         cmbServico_Ativo_Internalname = sPrefix+"SERVICO_ATIVO_"+sGXsfl_80_idx;
      }

      protected void SubsflControlProps_fel_802( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_80_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_80_fel_idx;
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO_"+sGXsfl_80_fel_idx;
         edtServico_Nome_Internalname = sPrefix+"SERVICO_NOME_"+sGXsfl_80_fel_idx;
         edtServico_Descricao_Internalname = sPrefix+"SERVICO_DESCRICAO_"+sGXsfl_80_fel_idx;
         edtServico_Sigla_Internalname = sPrefix+"SERVICO_SIGLA_"+sGXsfl_80_fel_idx;
         edtServicoGrupo_Codigo_Internalname = sPrefix+"SERVICOGRUPO_CODIGO_"+sGXsfl_80_fel_idx;
         edtServico_UO_Internalname = sPrefix+"SERVICO_UO_"+sGXsfl_80_fel_idx;
         cmbServico_UORespExclusiva_Internalname = sPrefix+"SERVICO_UORESPEXCLUSIVA_"+sGXsfl_80_fel_idx;
         edtServico_Vinculados_Internalname = sPrefix+"SERVICO_VINCULADOS_"+sGXsfl_80_fel_idx;
         edtServico_VincDesc_Internalname = sPrefix+"SERVICO_VINCDESC_"+sGXsfl_80_fel_idx;
         cmbServico_Terceriza_Internalname = sPrefix+"SERVICO_TERCERIZA_"+sGXsfl_80_fel_idx;
         cmbServico_Tela_Internalname = sPrefix+"SERVICO_TELA_"+sGXsfl_80_fel_idx;
         cmbServico_Atende_Internalname = sPrefix+"SERVICO_ATENDE_"+sGXsfl_80_fel_idx;
         cmbServico_ObrigaValores_Internalname = sPrefix+"SERVICO_OBRIGAVALORES_"+sGXsfl_80_fel_idx;
         cmbServico_ObjetoControle_Internalname = sPrefix+"SERVICO_OBJETOCONTROLE_"+sGXsfl_80_fel_idx;
         cmbServico_TipoHierarquia_Internalname = sPrefix+"SERVICO_TIPOHIERARQUIA_"+sGXsfl_80_fel_idx;
         edtServico_PercTmp_Internalname = sPrefix+"SERVICO_PERCTMP_"+sGXsfl_80_fel_idx;
         edtServico_PercPgm_Internalname = sPrefix+"SERVICO_PERCPGM_"+sGXsfl_80_fel_idx;
         edtServico_PercCnc_Internalname = sPrefix+"SERVICO_PERCCNC_"+sGXsfl_80_fel_idx;
         edtServico_Anterior_Internalname = sPrefix+"SERVICO_ANTERIOR_"+sGXsfl_80_fel_idx;
         edtServico_Posterior_Internalname = sPrefix+"SERVICO_POSTERIOR_"+sGXsfl_80_fel_idx;
         edtServico_Responsavel_Internalname = sPrefix+"SERVICO_RESPONSAVEL_"+sGXsfl_80_fel_idx;
         cmbServico_Ativo_Internalname = sPrefix+"SERVICO_ATIVO_"+sGXsfl_80_fel_idx;
      }

      protected void sendrow_802( )
      {
         SubsflControlProps_802( ) ;
         WBLP0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_80_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_80_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_80_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV35Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV35Update_GXI : context.PathToRelativeUrl( AV29Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Delete)) ? AV36Delete_GXI : context.PathToRelativeUrl( AV31Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Nome_Internalname,StringUtil.RTrim( A608Servico_Nome),StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)380,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Descricao_Internalname,(String)A156Servico_Descricao,(String)A156Servico_Descricao,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtServico_Descricao_Link,(String)"",(String)"",(String)"",(String)edtServico_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)570,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)80,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Sigla_Internalname,StringUtil.RTrim( A605Servico_Sigla),StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoGrupo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoGrupo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_UO_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A633Servico_UO), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A633Servico_UO), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_UO_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SERVICO_UORESPEXCLUSIVA_" + sGXsfl_80_idx;
               cmbServico_UORespExclusiva.Name = GXCCtl;
               cmbServico_UORespExclusiva.WebTags = "";
               cmbServico_UORespExclusiva.addItem(StringUtil.BoolToStr( false), "Herdada pelas sub-unidades", 0);
               cmbServico_UORespExclusiva.addItem(StringUtil.BoolToStr( true), "Exclusiva", 0);
               if ( cmbServico_UORespExclusiva.ItemCount > 0 )
               {
                  A1077Servico_UORespExclusiva = StringUtil.StrToBool( cmbServico_UORespExclusiva.getValidValue(StringUtil.BoolToStr( A1077Servico_UORespExclusiva)));
                  n1077Servico_UORespExclusiva = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServico_UORespExclusiva,(String)cmbServico_UORespExclusiva_Internalname,StringUtil.BoolToStr( A1077Servico_UORespExclusiva),(short)1,(String)cmbServico_UORespExclusiva_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServico_UORespExclusiva.CurrentValue = StringUtil.BoolToStr( A1077Servico_UORespExclusiva);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_UORespExclusiva_Internalname, "Values", (String)(cmbServico_UORespExclusiva.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Vinculados_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A640Servico_Vinculados), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A640Servico_Vinculados), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Vinculados_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_VincDesc_Internalname,(String)A641Servico_VincDesc,(String)A641Servico_VincDesc,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtServico_VincDesc_Link,(String)"",(String)"",(String)"",(String)edtServico_VincDesc_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)570,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)80,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SERVICO_TERCERIZA_" + sGXsfl_80_idx;
               cmbServico_Terceriza.Name = GXCCtl;
               cmbServico_Terceriza.WebTags = "";
               cmbServico_Terceriza.addItem(StringUtil.BoolToStr( false), "Desconhecido", 0);
               cmbServico_Terceriza.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               cmbServico_Terceriza.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               if ( cmbServico_Terceriza.ItemCount > 0 )
               {
                  A889Servico_Terceriza = StringUtil.StrToBool( cmbServico_Terceriza.getValidValue(StringUtil.BoolToStr( A889Servico_Terceriza)));
                  n889Servico_Terceriza = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServico_Terceriza,(String)cmbServico_Terceriza_Internalname,StringUtil.BoolToStr( A889Servico_Terceriza),(short)1,(String)cmbServico_Terceriza_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServico_Terceriza.CurrentValue = StringUtil.BoolToStr( A889Servico_Terceriza);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Terceriza_Internalname, "Values", (String)(cmbServico_Terceriza.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SERVICO_TELA_" + sGXsfl_80_idx;
               cmbServico_Tela.Name = GXCCtl;
               cmbServico_Tela.WebTags = "";
               cmbServico_Tela.addItem("", "(Nenhuma)", 0);
               cmbServico_Tela.addItem("CNT", "Contagem", 0);
               cmbServico_Tela.addItem("CHK", "Check List", 0);
               if ( cmbServico_Tela.ItemCount > 0 )
               {
                  A1061Servico_Tela = cmbServico_Tela.getValidValue(A1061Servico_Tela);
                  n1061Servico_Tela = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServico_Tela,(String)cmbServico_Tela_Internalname,StringUtil.RTrim( A1061Servico_Tela),(short)1,(String)cmbServico_Tela_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServico_Tela.CurrentValue = StringUtil.RTrim( A1061Servico_Tela);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Tela_Internalname, "Values", (String)(cmbServico_Tela.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SERVICO_ATENDE_" + sGXsfl_80_idx;
               cmbServico_Atende.Name = GXCCtl;
               cmbServico_Atende.WebTags = "";
               cmbServico_Atende.addItem("S", "Software", 0);
               cmbServico_Atende.addItem("H", "Hardware", 0);
               if ( cmbServico_Atende.ItemCount > 0 )
               {
                  A1072Servico_Atende = cmbServico_Atende.getValidValue(A1072Servico_Atende);
                  n1072Servico_Atende = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServico_Atende,(String)cmbServico_Atende_Internalname,StringUtil.RTrim( A1072Servico_Atende),(short)1,(String)cmbServico_Atende_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServico_Atende.CurrentValue = StringUtil.RTrim( A1072Servico_Atende);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Atende_Internalname, "Values", (String)(cmbServico_Atende.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SERVICO_OBRIGAVALORES_" + sGXsfl_80_idx;
               cmbServico_ObrigaValores.Name = GXCCtl;
               cmbServico_ObrigaValores.WebTags = "";
               cmbServico_ObrigaValores.addItem("", "Nenhum", 0);
               cmbServico_ObrigaValores.addItem("L", "Valor L�quido", 0);
               cmbServico_ObrigaValores.addItem("B", "Valor Bruto", 0);
               cmbServico_ObrigaValores.addItem("A", "Ambos", 0);
               if ( cmbServico_ObrigaValores.ItemCount > 0 )
               {
                  A1429Servico_ObrigaValores = cmbServico_ObrigaValores.getValidValue(A1429Servico_ObrigaValores);
                  n1429Servico_ObrigaValores = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServico_ObrigaValores,(String)cmbServico_ObrigaValores_Internalname,StringUtil.RTrim( A1429Servico_ObrigaValores),(short)1,(String)cmbServico_ObrigaValores_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServico_ObrigaValores.CurrentValue = StringUtil.RTrim( A1429Servico_ObrigaValores);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_ObrigaValores_Internalname, "Values", (String)(cmbServico_ObrigaValores.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SERVICO_OBJETOCONTROLE_" + sGXsfl_80_idx;
               cmbServico_ObjetoControle.Name = GXCCtl;
               cmbServico_ObjetoControle.WebTags = "";
               cmbServico_ObjetoControle.addItem("SIS", "Sistema", 0);
               cmbServico_ObjetoControle.addItem("PRC", "Processo", 0);
               cmbServico_ObjetoControle.addItem("PRD", "Produto", 0);
               if ( cmbServico_ObjetoControle.ItemCount > 0 )
               {
                  A1436Servico_ObjetoControle = cmbServico_ObjetoControle.getValidValue(A1436Servico_ObjetoControle);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServico_ObjetoControle,(String)cmbServico_ObjetoControle_Internalname,StringUtil.RTrim( A1436Servico_ObjetoControle),(short)1,(String)cmbServico_ObjetoControle_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServico_ObjetoControle.CurrentValue = StringUtil.RTrim( A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_ObjetoControle_Internalname, "Values", (String)(cmbServico_ObjetoControle.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SERVICO_TIPOHIERARQUIA_" + sGXsfl_80_idx;
               cmbServico_TipoHierarquia.Name = GXCCtl;
               cmbServico_TipoHierarquia.WebTags = "";
               cmbServico_TipoHierarquia.addItem("1", "Prim�rio", 0);
               cmbServico_TipoHierarquia.addItem("2", "Secund�rio", 0);
               if ( cmbServico_TipoHierarquia.ItemCount > 0 )
               {
                  A1530Servico_TipoHierarquia = (short)(NumberUtil.Val( cmbServico_TipoHierarquia.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0))), "."));
                  n1530Servico_TipoHierarquia = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServico_TipoHierarquia,(String)cmbServico_TipoHierarquia_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)),(short)1,(String)cmbServico_TipoHierarquia_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServico_TipoHierarquia.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_TipoHierarquia_Internalname, "Values", (String)(cmbServico_TipoHierarquia.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_PercTmp_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1534Servico_PercTmp), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_PercTmp_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)61,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_PercPgm_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1535Servico_PercPgm), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_PercPgm_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)89,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"center"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_PercCnc_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1536Servico_PercCnc), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_PercCnc_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)108,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"center",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Anterior_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1545Servico_Anterior), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1545Servico_Anterior), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Anterior_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Posterior_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1546Servico_Posterior), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1546Servico_Posterior), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Posterior_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Responsavel_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1551Servico_Responsavel), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1551Servico_Responsavel), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Responsavel_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SERVICO_ATIVO_" + sGXsfl_80_idx;
               cmbServico_Ativo.Name = GXCCtl;
               cmbServico_Ativo.WebTags = "";
               cmbServico_Ativo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbServico_Ativo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbServico_Ativo.ItemCount > 0 )
               {
                  A632Servico_Ativo = StringUtil.StrToBool( cmbServico_Ativo.getValidValue(StringUtil.BoolToStr( A632Servico_Ativo)));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServico_Ativo,(String)cmbServico_Ativo_Internalname,StringUtil.BoolToStr( A632Servico_Ativo),(short)1,(String)cmbServico_Ativo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttributeCheckBox",(String)"",(String)"",(String)"",(bool)true});
            cmbServico_Ativo.CurrentValue = StringUtil.BoolToStr( A632Servico_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Ativo_Internalname, "Values", (String)(cmbServico_Ativo.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_NOME"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_DESCRICAO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, A156Servico_Descricao));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_SIGLA"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOGRUPO_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_UO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, context.localUtil.Format( (decimal)(A633Servico_UO), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_UORESPEXCLUSIVA"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, A1077Servico_UORespExclusiva));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_VINCULADOS"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, context.localUtil.Format( (decimal)(A640Servico_Vinculados), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_TERCERIZA"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, A889Servico_Terceriza));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_TELA"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A1061Servico_Tela, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ATENDE"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A1072Servico_Atende, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_OBRIGAVALORES"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A1429Servico_ObrigaValores, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_OBJETOCONTROLE"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_TIPOHIERARQUIA"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PERCTMP"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PERCPGM"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PERCCNC"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ANTERIOR"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1545Servico_Anterior), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_POSTERIOR"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1546Servico_Posterior), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_RESPONSAVEL"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1551Servico_Responsavel), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ATIVO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sPrefix+sGXsfl_80_idx, A632Servico_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         /* End function sendrow_802 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavServico_nome1_Internalname = sPrefix+"vSERVICO_NOME1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavServico_nome2_Internalname = sPrefix+"vSERVICO_NOME2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR3";
         edtavServico_nome3_Internalname = sPrefix+"vSERVICO_NOME3";
         tblTablemergeddynamicfilters3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO";
         edtServico_Nome_Internalname = sPrefix+"SERVICO_NOME";
         edtServico_Descricao_Internalname = sPrefix+"SERVICO_DESCRICAO";
         edtServico_Sigla_Internalname = sPrefix+"SERVICO_SIGLA";
         edtServicoGrupo_Codigo_Internalname = sPrefix+"SERVICOGRUPO_CODIGO";
         edtServico_UO_Internalname = sPrefix+"SERVICO_UO";
         cmbServico_UORespExclusiva_Internalname = sPrefix+"SERVICO_UORESPEXCLUSIVA";
         edtServico_Vinculados_Internalname = sPrefix+"SERVICO_VINCULADOS";
         edtServico_VincDesc_Internalname = sPrefix+"SERVICO_VINCDESC";
         cmbServico_Terceriza_Internalname = sPrefix+"SERVICO_TERCERIZA";
         cmbServico_Tela_Internalname = sPrefix+"SERVICO_TELA";
         cmbServico_Atende_Internalname = sPrefix+"SERVICO_ATENDE";
         cmbServico_ObrigaValores_Internalname = sPrefix+"SERVICO_OBRIGAVALORES";
         cmbServico_ObjetoControle_Internalname = sPrefix+"SERVICO_OBJETOCONTROLE";
         cmbServico_TipoHierarquia_Internalname = sPrefix+"SERVICO_TIPOHIERARQUIA";
         edtServico_PercTmp_Internalname = sPrefix+"SERVICO_PERCTMP";
         edtServico_PercPgm_Internalname = sPrefix+"SERVICO_PERCPGM";
         edtServico_PercCnc_Internalname = sPrefix+"SERVICO_PERCCNC";
         edtServico_Anterior_Internalname = sPrefix+"SERVICO_ANTERIOR";
         edtServico_Posterior_Internalname = sPrefix+"SERVICO_POSTERIOR";
         edtServico_Responsavel_Internalname = sPrefix+"SERVICO_RESPONSAVEL";
         cmbServico_Ativo_Internalname = sPrefix+"SERVICO_ATIVO";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtServico_Vinculado_Internalname = sPrefix+"SERVICO_VINCULADO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbServico_Ativo_Jsonclick = "";
         edtServico_Responsavel_Jsonclick = "";
         edtServico_Posterior_Jsonclick = "";
         edtServico_Anterior_Jsonclick = "";
         edtServico_PercCnc_Jsonclick = "";
         edtServico_PercPgm_Jsonclick = "";
         edtServico_PercTmp_Jsonclick = "";
         cmbServico_TipoHierarquia_Jsonclick = "";
         cmbServico_ObjetoControle_Jsonclick = "";
         cmbServico_ObrigaValores_Jsonclick = "";
         cmbServico_Atende_Jsonclick = "";
         cmbServico_Tela_Jsonclick = "";
         cmbServico_Terceriza_Jsonclick = "";
         edtServico_VincDesc_Jsonclick = "";
         edtServico_Vinculados_Jsonclick = "";
         cmbServico_UORespExclusiva_Jsonclick = "";
         edtServico_UO_Jsonclick = "";
         edtServicoGrupo_Codigo_Jsonclick = "";
         edtServico_Sigla_Jsonclick = "";
         edtServico_Descricao_Jsonclick = "";
         edtServico_Nome_Jsonclick = "";
         edtServico_Codigo_Jsonclick = "";
         edtavServico_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavServico_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavServico_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtServico_VincDesc_Link = "";
         edtServico_Descricao_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         cmbServico_Ativo_Titleformat = 0;
         edtServico_Posterior_Titleformat = 0;
         edtServico_Anterior_Titleformat = 0;
         edtServico_PercCnc_Titleformat = 0;
         edtServico_PercPgm_Titleformat = 0;
         edtServico_PercTmp_Titleformat = 0;
         cmbServico_TipoHierarquia_Titleformat = 0;
         cmbServico_ObjetoControle_Titleformat = 0;
         cmbServico_ObrigaValores_Titleformat = 0;
         cmbServico_Atende_Titleformat = 0;
         cmbServico_Tela_Titleformat = 0;
         cmbServico_Terceriza_Titleformat = 0;
         edtServico_VincDesc_Titleformat = 0;
         cmbServico_UORespExclusiva_Titleformat = 0;
         edtServico_UO_Titleformat = 0;
         edtServico_Sigla_Titleformat = 0;
         edtServico_Descricao_Titleformat = 0;
         edtServico_Nome_Titleformat = 0;
         subGrid_Class = "WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavServico_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavServico_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavServico_nome1_Visible = 1;
         cmbServico_Ativo.Title.Text = "Ativo?";
         edtServico_Posterior_Title = "Posterior";
         edtServico_Anterior_Title = "Anterior";
         edtServico_PercCnc_Title = "de Cancelamento";
         edtServico_PercPgm_Title = "de Pagamento";
         edtServico_PercTmp_Title = "de Tempo";
         cmbServico_TipoHierarquia.Title.Text = "de Hierarquia";
         cmbServico_ObjetoControle.Title.Text = "Controle";
         cmbServico_ObrigaValores.Title.Text = "Valores";
         cmbServico_Atende.Title.Text = "Atende";
         cmbServico_Tela.Title.Text = "Tela";
         cmbServico_Terceriza.Title.Text = "Pode ser tercerizado?";
         edtServico_VincDesc_Title = "Descri��o";
         cmbServico_UORespExclusiva.Title.Text = "Responsabilidade";
         edtServico_UO_Title = "Unidade Organizacional";
         edtServico_Sigla_Title = "Sigla";
         edtServico_Descricao_Title = "Descri��o de Servi�o";
         edtServico_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtServico_Vinculado_Jsonclick = "";
         edtServico_Vinculado_Visible = 1;
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Servico_Vinculado',fld:'vSERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A631Servico_Vinculado',fld:'SERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'edtServico_Nome_Titleformat',ctrl:'SERVICO_NOME',prop:'Titleformat'},{av:'edtServico_Nome_Title',ctrl:'SERVICO_NOME',prop:'Title'},{av:'edtServico_Descricao_Titleformat',ctrl:'SERVICO_DESCRICAO',prop:'Titleformat'},{av:'edtServico_Descricao_Title',ctrl:'SERVICO_DESCRICAO',prop:'Title'},{av:'edtServico_Sigla_Titleformat',ctrl:'SERVICO_SIGLA',prop:'Titleformat'},{av:'edtServico_Sigla_Title',ctrl:'SERVICO_SIGLA',prop:'Title'},{av:'edtServico_UO_Titleformat',ctrl:'SERVICO_UO',prop:'Titleformat'},{av:'edtServico_UO_Title',ctrl:'SERVICO_UO',prop:'Title'},{av:'cmbServico_UORespExclusiva'},{av:'edtServico_VincDesc_Titleformat',ctrl:'SERVICO_VINCDESC',prop:'Titleformat'},{av:'edtServico_VincDesc_Title',ctrl:'SERVICO_VINCDESC',prop:'Title'},{av:'cmbServico_Terceriza'},{av:'cmbServico_Tela'},{av:'cmbServico_Atende'},{av:'cmbServico_ObrigaValores'},{av:'cmbServico_ObjetoControle'},{av:'cmbServico_TipoHierarquia'},{av:'edtServico_PercTmp_Titleformat',ctrl:'SERVICO_PERCTMP',prop:'Titleformat'},{av:'edtServico_PercTmp_Title',ctrl:'SERVICO_PERCTMP',prop:'Title'},{av:'edtServico_PercPgm_Titleformat',ctrl:'SERVICO_PERCPGM',prop:'Titleformat'},{av:'edtServico_PercPgm_Title',ctrl:'SERVICO_PERCPGM',prop:'Title'},{av:'edtServico_PercCnc_Titleformat',ctrl:'SERVICO_PERCCNC',prop:'Titleformat'},{av:'edtServico_PercCnc_Title',ctrl:'SERVICO_PERCCNC',prop:'Title'},{av:'edtServico_Anterior_Titleformat',ctrl:'SERVICO_ANTERIOR',prop:'Titleformat'},{av:'edtServico_Anterior_Title',ctrl:'SERVICO_ANTERIOR',prop:'Title'},{av:'edtServico_Posterior_Titleformat',ctrl:'SERVICO_POSTERIOR',prop:'Titleformat'},{av:'edtServico_Posterior_Title',ctrl:'SERVICO_POSTERIOR',prop:'Title'},{av:'cmbServico_Ativo'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID.LOAD","{handler:'E24LP2',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A631Servico_Vinculado',fld:'SERVICO_VINCULADO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV29Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV31Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtServico_Descricao_Link',ctrl:'SERVICO_DESCRICAO',prop:'Link'},{av:'edtServico_VincDesc_Link',ctrl:'SERVICO_VINCDESC',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E11LP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Servico_Vinculado',fld:'vSERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A631Servico_Vinculado',fld:'SERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17LP2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E12LP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Servico_Vinculado',fld:'vSERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A631Servico_Vinculado',fld:'SERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18LP2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19LP2',iparms:[],oparms:[{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E13LP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Servico_Vinculado',fld:'vSERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A631Servico_Vinculado',fld:'SERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20LP2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E14LP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Servico_Vinculado',fld:'vSERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A631Servico_Vinculado',fld:'SERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21LP2',iparms:[{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E15LP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Servico_Vinculado',fld:'vSERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A631Servico_Vinculado',fld:'SERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E16LP2',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Servico_Vinculado',fld:'vSERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A631Servico_Vinculado',fld:'SERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'edtServico_Nome_Titleformat',ctrl:'SERVICO_NOME',prop:'Titleformat'},{av:'edtServico_Nome_Title',ctrl:'SERVICO_NOME',prop:'Title'},{av:'edtServico_Descricao_Titleformat',ctrl:'SERVICO_DESCRICAO',prop:'Titleformat'},{av:'edtServico_Descricao_Title',ctrl:'SERVICO_DESCRICAO',prop:'Title'},{av:'edtServico_Sigla_Titleformat',ctrl:'SERVICO_SIGLA',prop:'Titleformat'},{av:'edtServico_Sigla_Title',ctrl:'SERVICO_SIGLA',prop:'Title'},{av:'edtServico_UO_Titleformat',ctrl:'SERVICO_UO',prop:'Titleformat'},{av:'edtServico_UO_Title',ctrl:'SERVICO_UO',prop:'Title'},{av:'cmbServico_UORespExclusiva'},{av:'edtServico_VincDesc_Titleformat',ctrl:'SERVICO_VINCDESC',prop:'Titleformat'},{av:'edtServico_VincDesc_Title',ctrl:'SERVICO_VINCDESC',prop:'Title'},{av:'cmbServico_Terceriza'},{av:'cmbServico_Tela'},{av:'cmbServico_Atende'},{av:'cmbServico_ObrigaValores'},{av:'cmbServico_ObjetoControle'},{av:'cmbServico_TipoHierarquia'},{av:'edtServico_PercTmp_Titleformat',ctrl:'SERVICO_PERCTMP',prop:'Titleformat'},{av:'edtServico_PercTmp_Title',ctrl:'SERVICO_PERCTMP',prop:'Title'},{av:'edtServico_PercPgm_Titleformat',ctrl:'SERVICO_PERCPGM',prop:'Titleformat'},{av:'edtServico_PercPgm_Title',ctrl:'SERVICO_PERCPGM',prop:'Title'},{av:'edtServico_PercCnc_Titleformat',ctrl:'SERVICO_PERCCNC',prop:'Titleformat'},{av:'edtServico_PercCnc_Title',ctrl:'SERVICO_PERCCNC',prop:'Title'},{av:'edtServico_Anterior_Titleformat',ctrl:'SERVICO_ANTERIOR',prop:'Titleformat'},{av:'edtServico_Anterior_Title',ctrl:'SERVICO_ANTERIOR',prop:'Title'},{av:'edtServico_Posterior_Titleformat',ctrl:'SERVICO_POSTERIOR',prop:'Titleformat'},{av:'edtServico_Posterior_Title',ctrl:'SERVICO_POSTERIOR',prop:'Title'},{av:'cmbServico_Ativo'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Servico_Vinculado',fld:'vSERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A631Servico_Vinculado',fld:'SERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'edtServico_Nome_Titleformat',ctrl:'SERVICO_NOME',prop:'Titleformat'},{av:'edtServico_Nome_Title',ctrl:'SERVICO_NOME',prop:'Title'},{av:'edtServico_Descricao_Titleformat',ctrl:'SERVICO_DESCRICAO',prop:'Titleformat'},{av:'edtServico_Descricao_Title',ctrl:'SERVICO_DESCRICAO',prop:'Title'},{av:'edtServico_Sigla_Titleformat',ctrl:'SERVICO_SIGLA',prop:'Titleformat'},{av:'edtServico_Sigla_Title',ctrl:'SERVICO_SIGLA',prop:'Title'},{av:'edtServico_UO_Titleformat',ctrl:'SERVICO_UO',prop:'Titleformat'},{av:'edtServico_UO_Title',ctrl:'SERVICO_UO',prop:'Title'},{av:'cmbServico_UORespExclusiva'},{av:'edtServico_VincDesc_Titleformat',ctrl:'SERVICO_VINCDESC',prop:'Titleformat'},{av:'edtServico_VincDesc_Title',ctrl:'SERVICO_VINCDESC',prop:'Title'},{av:'cmbServico_Terceriza'},{av:'cmbServico_Tela'},{av:'cmbServico_Atende'},{av:'cmbServico_ObrigaValores'},{av:'cmbServico_ObjetoControle'},{av:'cmbServico_TipoHierarquia'},{av:'edtServico_PercTmp_Titleformat',ctrl:'SERVICO_PERCTMP',prop:'Titleformat'},{av:'edtServico_PercTmp_Title',ctrl:'SERVICO_PERCTMP',prop:'Title'},{av:'edtServico_PercPgm_Titleformat',ctrl:'SERVICO_PERCPGM',prop:'Titleformat'},{av:'edtServico_PercPgm_Title',ctrl:'SERVICO_PERCPGM',prop:'Title'},{av:'edtServico_PercCnc_Titleformat',ctrl:'SERVICO_PERCCNC',prop:'Titleformat'},{av:'edtServico_PercCnc_Title',ctrl:'SERVICO_PERCCNC',prop:'Title'},{av:'edtServico_Anterior_Titleformat',ctrl:'SERVICO_ANTERIOR',prop:'Titleformat'},{av:'edtServico_Anterior_Title',ctrl:'SERVICO_ANTERIOR',prop:'Title'},{av:'edtServico_Posterior_Titleformat',ctrl:'SERVICO_POSTERIOR',prop:'Titleformat'},{av:'edtServico_Posterior_Title',ctrl:'SERVICO_POSTERIOR',prop:'Title'},{av:'cmbServico_Ativo'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Servico_Vinculado',fld:'vSERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A631Servico_Vinculado',fld:'SERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'edtServico_Nome_Titleformat',ctrl:'SERVICO_NOME',prop:'Titleformat'},{av:'edtServico_Nome_Title',ctrl:'SERVICO_NOME',prop:'Title'},{av:'edtServico_Descricao_Titleformat',ctrl:'SERVICO_DESCRICAO',prop:'Titleformat'},{av:'edtServico_Descricao_Title',ctrl:'SERVICO_DESCRICAO',prop:'Title'},{av:'edtServico_Sigla_Titleformat',ctrl:'SERVICO_SIGLA',prop:'Titleformat'},{av:'edtServico_Sigla_Title',ctrl:'SERVICO_SIGLA',prop:'Title'},{av:'edtServico_UO_Titleformat',ctrl:'SERVICO_UO',prop:'Titleformat'},{av:'edtServico_UO_Title',ctrl:'SERVICO_UO',prop:'Title'},{av:'cmbServico_UORespExclusiva'},{av:'edtServico_VincDesc_Titleformat',ctrl:'SERVICO_VINCDESC',prop:'Titleformat'},{av:'edtServico_VincDesc_Title',ctrl:'SERVICO_VINCDESC',prop:'Title'},{av:'cmbServico_Terceriza'},{av:'cmbServico_Tela'},{av:'cmbServico_Atende'},{av:'cmbServico_ObrigaValores'},{av:'cmbServico_ObjetoControle'},{av:'cmbServico_TipoHierarquia'},{av:'edtServico_PercTmp_Titleformat',ctrl:'SERVICO_PERCTMP',prop:'Titleformat'},{av:'edtServico_PercTmp_Title',ctrl:'SERVICO_PERCTMP',prop:'Title'},{av:'edtServico_PercPgm_Titleformat',ctrl:'SERVICO_PERCPGM',prop:'Titleformat'},{av:'edtServico_PercPgm_Title',ctrl:'SERVICO_PERCPGM',prop:'Title'},{av:'edtServico_PercCnc_Titleformat',ctrl:'SERVICO_PERCCNC',prop:'Titleformat'},{av:'edtServico_PercCnc_Title',ctrl:'SERVICO_PERCCNC',prop:'Title'},{av:'edtServico_Anterior_Titleformat',ctrl:'SERVICO_ANTERIOR',prop:'Titleformat'},{av:'edtServico_Anterior_Title',ctrl:'SERVICO_ANTERIOR',prop:'Title'},{av:'edtServico_Posterior_Titleformat',ctrl:'SERVICO_POSTERIOR',prop:'Titleformat'},{av:'edtServico_Posterior_Title',ctrl:'SERVICO_POSTERIOR',prop:'Title'},{av:'cmbServico_Ativo'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Servico_Vinculado',fld:'vSERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV30ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A631Servico_Vinculado',fld:'SERVICO_VINCULADO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'edtServico_Nome_Titleformat',ctrl:'SERVICO_NOME',prop:'Titleformat'},{av:'edtServico_Nome_Title',ctrl:'SERVICO_NOME',prop:'Title'},{av:'edtServico_Descricao_Titleformat',ctrl:'SERVICO_DESCRICAO',prop:'Titleformat'},{av:'edtServico_Descricao_Title',ctrl:'SERVICO_DESCRICAO',prop:'Title'},{av:'edtServico_Sigla_Titleformat',ctrl:'SERVICO_SIGLA',prop:'Titleformat'},{av:'edtServico_Sigla_Title',ctrl:'SERVICO_SIGLA',prop:'Title'},{av:'edtServico_UO_Titleformat',ctrl:'SERVICO_UO',prop:'Titleformat'},{av:'edtServico_UO_Title',ctrl:'SERVICO_UO',prop:'Title'},{av:'cmbServico_UORespExclusiva'},{av:'edtServico_VincDesc_Titleformat',ctrl:'SERVICO_VINCDESC',prop:'Titleformat'},{av:'edtServico_VincDesc_Title',ctrl:'SERVICO_VINCDESC',prop:'Title'},{av:'cmbServico_Terceriza'},{av:'cmbServico_Tela'},{av:'cmbServico_Atende'},{av:'cmbServico_ObrigaValores'},{av:'cmbServico_ObjetoControle'},{av:'cmbServico_TipoHierarquia'},{av:'edtServico_PercTmp_Titleformat',ctrl:'SERVICO_PERCTMP',prop:'Titleformat'},{av:'edtServico_PercTmp_Title',ctrl:'SERVICO_PERCTMP',prop:'Title'},{av:'edtServico_PercPgm_Titleformat',ctrl:'SERVICO_PERCPGM',prop:'Titleformat'},{av:'edtServico_PercPgm_Title',ctrl:'SERVICO_PERCPGM',prop:'Title'},{av:'edtServico_PercCnc_Titleformat',ctrl:'SERVICO_PERCCNC',prop:'Titleformat'},{av:'edtServico_PercCnc_Title',ctrl:'SERVICO_PERCCNC',prop:'Title'},{av:'edtServico_Anterior_Titleformat',ctrl:'SERVICO_ANTERIOR',prop:'Titleformat'},{av:'edtServico_Anterior_Title',ctrl:'SERVICO_ANTERIOR',prop:'Title'},{av:'edtServico_Posterior_Titleformat',ctrl:'SERVICO_POSTERIOR',prop:'Titleformat'},{av:'edtServico_Posterior_Title',ctrl:'SERVICO_POSTERIOR',prop:'Title'},{av:'cmbServico_Ativo'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV18Servico_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22Servico_Nome2 = "";
         AV24DynamicFiltersSelector3 = "";
         AV26Servico_Nome3 = "";
         AV37Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV29Update = "";
         AV35Update_GXI = "";
         AV31Delete = "";
         AV36Delete_GXI = "";
         A608Servico_Nome = "";
         A156Servico_Descricao = "";
         A605Servico_Sigla = "";
         A641Servico_VincDesc = "";
         A1061Servico_Tela = "";
         A1072Servico_Atende = "";
         A1429Servico_ObrigaValores = "";
         A1436Servico_ObjetoControle = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18Servico_Nome1 = "";
         lV22Servico_Nome2 = "";
         lV26Servico_Nome3 = "";
         H00LP2_A631Servico_Vinculado = new int[1] ;
         H00LP2_n631Servico_Vinculado = new bool[] {false} ;
         H00LP2_A632Servico_Ativo = new bool[] {false} ;
         H00LP2_A1546Servico_Posterior = new int[1] ;
         H00LP2_n1546Servico_Posterior = new bool[] {false} ;
         H00LP2_A1545Servico_Anterior = new int[1] ;
         H00LP2_n1545Servico_Anterior = new bool[] {false} ;
         H00LP2_A1536Servico_PercCnc = new short[1] ;
         H00LP2_n1536Servico_PercCnc = new bool[] {false} ;
         H00LP2_A1535Servico_PercPgm = new short[1] ;
         H00LP2_n1535Servico_PercPgm = new bool[] {false} ;
         H00LP2_A1534Servico_PercTmp = new short[1] ;
         H00LP2_n1534Servico_PercTmp = new bool[] {false} ;
         H00LP2_A1530Servico_TipoHierarquia = new short[1] ;
         H00LP2_n1530Servico_TipoHierarquia = new bool[] {false} ;
         H00LP2_A1436Servico_ObjetoControle = new String[] {""} ;
         H00LP2_A1429Servico_ObrigaValores = new String[] {""} ;
         H00LP2_n1429Servico_ObrigaValores = new bool[] {false} ;
         H00LP2_A1072Servico_Atende = new String[] {""} ;
         H00LP2_n1072Servico_Atende = new bool[] {false} ;
         H00LP2_A1061Servico_Tela = new String[] {""} ;
         H00LP2_n1061Servico_Tela = new bool[] {false} ;
         H00LP2_A889Servico_Terceriza = new bool[] {false} ;
         H00LP2_n889Servico_Terceriza = new bool[] {false} ;
         H00LP2_A641Servico_VincDesc = new String[] {""} ;
         H00LP2_n641Servico_VincDesc = new bool[] {false} ;
         H00LP2_A1077Servico_UORespExclusiva = new bool[] {false} ;
         H00LP2_n1077Servico_UORespExclusiva = new bool[] {false} ;
         H00LP2_A633Servico_UO = new int[1] ;
         H00LP2_n633Servico_UO = new bool[] {false} ;
         H00LP2_A157ServicoGrupo_Codigo = new int[1] ;
         H00LP2_A605Servico_Sigla = new String[] {""} ;
         H00LP2_A156Servico_Descricao = new String[] {""} ;
         H00LP2_n156Servico_Descricao = new bool[] {false} ;
         H00LP2_A608Servico_Nome = new String[] {""} ;
         H00LP2_A155Servico_Codigo = new int[1] ;
         H00LP3_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV32Session = context.GetSession();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Servico_Vinculado = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicoprocessoservico1wc__default(),
            new Object[][] {
                new Object[] {
               H00LP2_A631Servico_Vinculado, H00LP2_n631Servico_Vinculado, H00LP2_A632Servico_Ativo, H00LP2_A1546Servico_Posterior, H00LP2_n1546Servico_Posterior, H00LP2_A1545Servico_Anterior, H00LP2_n1545Servico_Anterior, H00LP2_A1536Servico_PercCnc, H00LP2_n1536Servico_PercCnc, H00LP2_A1535Servico_PercPgm,
               H00LP2_n1535Servico_PercPgm, H00LP2_A1534Servico_PercTmp, H00LP2_n1534Servico_PercTmp, H00LP2_A1530Servico_TipoHierarquia, H00LP2_n1530Servico_TipoHierarquia, H00LP2_A1436Servico_ObjetoControle, H00LP2_A1429Servico_ObrigaValores, H00LP2_n1429Servico_ObrigaValores, H00LP2_A1072Servico_Atende, H00LP2_n1072Servico_Atende,
               H00LP2_A1061Servico_Tela, H00LP2_n1061Servico_Tela, H00LP2_A889Servico_Terceriza, H00LP2_n889Servico_Terceriza, H00LP2_A641Servico_VincDesc, H00LP2_n641Servico_VincDesc, H00LP2_A1077Servico_UORespExclusiva, H00LP2_n1077Servico_UORespExclusiva, H00LP2_A633Servico_UO, H00LP2_n633Servico_UO,
               H00LP2_A157ServicoGrupo_Codigo, H00LP2_A605Servico_Sigla, H00LP2_A156Servico_Descricao, H00LP2_n156Servico_Descricao, H00LP2_A608Servico_Nome, H00LP2_A155Servico_Codigo
               }
               , new Object[] {
               H00LP3_AGRID_nRecordCount
               }
            }
         );
         AV37Pgmname = "ServicoProcessoServico1WC";
         /* GeneXus formulas. */
         AV37Pgmname = "ServicoProcessoServico1WC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_80 ;
      private short nGXsfl_80_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV25DynamicFiltersOperator3 ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A640Servico_Vinculados ;
      private short A1530Servico_TipoHierarquia ;
      private short A1534Servico_PercTmp ;
      private short A1535Servico_PercPgm ;
      private short A1536Servico_PercCnc ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_80_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short GXt_int1 ;
      private short edtServico_Nome_Titleformat ;
      private short edtServico_Descricao_Titleformat ;
      private short edtServico_Sigla_Titleformat ;
      private short edtServico_UO_Titleformat ;
      private short cmbServico_UORespExclusiva_Titleformat ;
      private short edtServico_VincDesc_Titleformat ;
      private short cmbServico_Terceriza_Titleformat ;
      private short cmbServico_Tela_Titleformat ;
      private short cmbServico_Atende_Titleformat ;
      private short cmbServico_ObrigaValores_Titleformat ;
      private short cmbServico_ObjetoControle_Titleformat ;
      private short cmbServico_TipoHierarquia_Titleformat ;
      private short edtServico_PercTmp_Titleformat ;
      private short edtServico_PercPgm_Titleformat ;
      private short edtServico_PercCnc_Titleformat ;
      private short edtServico_Anterior_Titleformat ;
      private short edtServico_Posterior_Titleformat ;
      private short cmbServico_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int AV7Servico_Vinculado ;
      private int wcpOAV7Servico_Vinculado ;
      private int subGrid_Rows ;
      private int A155Servico_Codigo ;
      private int AV30ServicoGrupo_Codigo ;
      private int A631Servico_Vinculado ;
      private int edtServico_Vinculado_Visible ;
      private int A157ServicoGrupo_Codigo ;
      private int A633Servico_UO ;
      private int A1545Servico_Anterior ;
      private int A1546Servico_Posterior ;
      private int A1551Servico_Responsavel ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int GXt_int2 ;
      private int edtavOrdereddsc_Visible ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavServico_nome1_Visible ;
      private int edtavServico_nome2_Visible ;
      private int edtavServico_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_80_idx="0001" ;
      private String AV18Servico_Nome1 ;
      private String AV22Servico_Nome2 ;
      private String AV26Servico_Nome3 ;
      private String AV37Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String edtServico_Vinculado_Internalname ;
      private String edtServico_Vinculado_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtServico_Codigo_Internalname ;
      private String A608Servico_Nome ;
      private String edtServico_Nome_Internalname ;
      private String edtServico_Descricao_Internalname ;
      private String A605Servico_Sigla ;
      private String edtServico_Sigla_Internalname ;
      private String edtServicoGrupo_Codigo_Internalname ;
      private String edtServico_UO_Internalname ;
      private String cmbServico_UORespExclusiva_Internalname ;
      private String edtServico_Vinculados_Internalname ;
      private String edtServico_VincDesc_Internalname ;
      private String cmbServico_Terceriza_Internalname ;
      private String cmbServico_Tela_Internalname ;
      private String A1061Servico_Tela ;
      private String cmbServico_Atende_Internalname ;
      private String A1072Servico_Atende ;
      private String cmbServico_ObrigaValores_Internalname ;
      private String A1429Servico_ObrigaValores ;
      private String cmbServico_ObjetoControle_Internalname ;
      private String A1436Servico_ObjetoControle ;
      private String cmbServico_TipoHierarquia_Internalname ;
      private String edtServico_PercTmp_Internalname ;
      private String edtServico_PercPgm_Internalname ;
      private String edtServico_PercCnc_Internalname ;
      private String edtServico_Anterior_Internalname ;
      private String edtServico_Posterior_Internalname ;
      private String edtServico_Responsavel_Internalname ;
      private String cmbServico_Ativo_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV18Servico_Nome1 ;
      private String lV22Servico_Nome2 ;
      private String lV26Servico_Nome3 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavServico_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavServico_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavServico_nome3_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtServico_Nome_Title ;
      private String edtServico_Descricao_Title ;
      private String edtServico_Sigla_Title ;
      private String edtServico_UO_Title ;
      private String edtServico_VincDesc_Title ;
      private String edtServico_PercTmp_Title ;
      private String edtServico_PercPgm_Title ;
      private String edtServico_PercCnc_Title ;
      private String edtServico_Anterior_Title ;
      private String edtServico_Posterior_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtServico_Descricao_Link ;
      private String edtServico_VincDesc_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavServico_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavServico_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavServico_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7Servico_Vinculado ;
      private String sGXsfl_80_fel_idx="0001" ;
      private String ROClassString ;
      private String edtServico_Codigo_Jsonclick ;
      private String edtServico_Nome_Jsonclick ;
      private String edtServico_Descricao_Jsonclick ;
      private String edtServico_Sigla_Jsonclick ;
      private String edtServicoGrupo_Codigo_Jsonclick ;
      private String edtServico_UO_Jsonclick ;
      private String cmbServico_UORespExclusiva_Jsonclick ;
      private String edtServico_Vinculados_Jsonclick ;
      private String edtServico_VincDesc_Jsonclick ;
      private String cmbServico_Terceriza_Jsonclick ;
      private String cmbServico_Tela_Jsonclick ;
      private String cmbServico_Atende_Jsonclick ;
      private String cmbServico_ObrigaValores_Jsonclick ;
      private String cmbServico_ObjetoControle_Jsonclick ;
      private String cmbServico_TipoHierarquia_Jsonclick ;
      private String edtServico_PercTmp_Jsonclick ;
      private String edtServico_PercPgm_Jsonclick ;
      private String edtServico_PercCnc_Jsonclick ;
      private String edtServico_Anterior_Jsonclick ;
      private String edtServico_Posterior_Jsonclick ;
      private String edtServico_Responsavel_Jsonclick ;
      private String cmbServico_Ativo_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV23DynamicFiltersEnabled3 ;
      private bool AV28DynamicFiltersIgnoreFirst ;
      private bool AV27DynamicFiltersRemoving ;
      private bool n631Servico_Vinculado ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n156Servico_Descricao ;
      private bool n633Servico_UO ;
      private bool A1077Servico_UORespExclusiva ;
      private bool n1077Servico_UORespExclusiva ;
      private bool n641Servico_VincDesc ;
      private bool A889Servico_Terceriza ;
      private bool n889Servico_Terceriza ;
      private bool n1061Servico_Tela ;
      private bool n1072Servico_Atende ;
      private bool n1429Servico_ObrigaValores ;
      private bool n1530Servico_TipoHierarquia ;
      private bool n1534Servico_PercTmp ;
      private bool n1535Servico_PercPgm ;
      private bool n1536Servico_PercCnc ;
      private bool n1545Servico_Anterior ;
      private bool n1546Servico_Posterior ;
      private bool A632Servico_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV29Update_IsBlob ;
      private bool AV31Delete_IsBlob ;
      private String A156Servico_Descricao ;
      private String A641Servico_VincDesc ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV24DynamicFiltersSelector3 ;
      private String AV35Update_GXI ;
      private String AV36Delete_GXI ;
      private String AV29Update ;
      private String AV31Delete ;
      private IGxSession AV32Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbServico_UORespExclusiva ;
      private GXCombobox cmbServico_Terceriza ;
      private GXCombobox cmbServico_Tela ;
      private GXCombobox cmbServico_Atende ;
      private GXCombobox cmbServico_ObrigaValores ;
      private GXCombobox cmbServico_ObjetoControle ;
      private GXCombobox cmbServico_TipoHierarquia ;
      private GXCombobox cmbServico_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00LP2_A631Servico_Vinculado ;
      private bool[] H00LP2_n631Servico_Vinculado ;
      private bool[] H00LP2_A632Servico_Ativo ;
      private int[] H00LP2_A1546Servico_Posterior ;
      private bool[] H00LP2_n1546Servico_Posterior ;
      private int[] H00LP2_A1545Servico_Anterior ;
      private bool[] H00LP2_n1545Servico_Anterior ;
      private short[] H00LP2_A1536Servico_PercCnc ;
      private bool[] H00LP2_n1536Servico_PercCnc ;
      private short[] H00LP2_A1535Servico_PercPgm ;
      private bool[] H00LP2_n1535Servico_PercPgm ;
      private short[] H00LP2_A1534Servico_PercTmp ;
      private bool[] H00LP2_n1534Servico_PercTmp ;
      private short[] H00LP2_A1530Servico_TipoHierarquia ;
      private bool[] H00LP2_n1530Servico_TipoHierarquia ;
      private String[] H00LP2_A1436Servico_ObjetoControle ;
      private String[] H00LP2_A1429Servico_ObrigaValores ;
      private bool[] H00LP2_n1429Servico_ObrigaValores ;
      private String[] H00LP2_A1072Servico_Atende ;
      private bool[] H00LP2_n1072Servico_Atende ;
      private String[] H00LP2_A1061Servico_Tela ;
      private bool[] H00LP2_n1061Servico_Tela ;
      private bool[] H00LP2_A889Servico_Terceriza ;
      private bool[] H00LP2_n889Servico_Terceriza ;
      private String[] H00LP2_A641Servico_VincDesc ;
      private bool[] H00LP2_n641Servico_VincDesc ;
      private bool[] H00LP2_A1077Servico_UORespExclusiva ;
      private bool[] H00LP2_n1077Servico_UORespExclusiva ;
      private int[] H00LP2_A633Servico_UO ;
      private bool[] H00LP2_n633Servico_UO ;
      private int[] H00LP2_A157ServicoGrupo_Codigo ;
      private String[] H00LP2_A605Servico_Sigla ;
      private String[] H00LP2_A156Servico_Descricao ;
      private bool[] H00LP2_n156Servico_Descricao ;
      private String[] H00LP2_A608Servico_Nome ;
      private int[] H00LP2_A155Servico_Codigo ;
      private long[] H00LP3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
   }

   public class servicoprocessoservico1wc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00LP2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18Servico_Nome1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22Servico_Nome2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             short AV25DynamicFiltersOperator3 ,
                                             String AV26Servico_Nome3 ,
                                             String A608Servico_Nome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A631Servico_Vinculado ,
                                             int AV7Servico_Vinculado )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Servico_Vinculado] AS Servico_Vinculado, T1.[Servico_Ativo], T1.[Servico_Posterior], T1.[Servico_Anterior], T1.[Servico_PercCnc], T1.[Servico_PercPgm], T1.[Servico_PercTmp], T1.[Servico_TipoHierarquia], T1.[Servico_ObjetoControle], T1.[Servico_ObrigaValores], T1.[Servico_Atende], T1.[Servico_Tela], T1.[Servico_Terceriza], T2.[Servico_Descricao] AS Servico_VincDesc, T1.[Servico_UORespExclusiva], T1.[Servico_UO], T1.[ServicoGrupo_Codigo], T1.[Servico_Sigla], T1.[Servico_Descricao], T1.[Servico_Nome], T1.[Servico_Codigo]";
         sFromString = " FROM ([Servico] T1 WITH (NOLOCK) LEFT JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Vinculado])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Servico_Vinculado] = @AV7Servico_Vinculado)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV18Servico_Nome1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV18Servico_Nome1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Servico_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV22Servico_Nome2)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Servico_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV22Servico_Nome2)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Servico_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV26Servico_Nome3)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Servico_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV26Servico_Nome3)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_Descricao]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_Descricao] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_Sigla]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_Sigla] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_UO]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_UO] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_UORespExclusiva]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_UORespExclusiva] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T2.[Servico_Descricao]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T2.[Servico_Descricao] DESC";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_Terceriza]";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_Terceriza] DESC";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_Tela]";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_Tela] DESC";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_Atende]";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_Atende] DESC";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_ObrigaValores]";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_ObrigaValores] DESC";
         }
         else if ( ( AV14OrderedBy == 11 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_ObjetoControle]";
         }
         else if ( ( AV14OrderedBy == 11 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_ObjetoControle] DESC";
         }
         else if ( ( AV14OrderedBy == 12 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_TipoHierarquia]";
         }
         else if ( ( AV14OrderedBy == 12 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_TipoHierarquia] DESC";
         }
         else if ( ( AV14OrderedBy == 13 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_PercTmp]";
         }
         else if ( ( AV14OrderedBy == 13 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_PercTmp] DESC";
         }
         else if ( ( AV14OrderedBy == 14 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_PercPgm]";
         }
         else if ( ( AV14OrderedBy == 14 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_PercPgm] DESC";
         }
         else if ( ( AV14OrderedBy == 15 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_PercCnc]";
         }
         else if ( ( AV14OrderedBy == 15 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_PercCnc] DESC";
         }
         else if ( ( AV14OrderedBy == 16 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_Anterior]";
         }
         else if ( ( AV14OrderedBy == 16 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_Anterior] DESC";
         }
         else if ( ( AV14OrderedBy == 17 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_Posterior]";
         }
         else if ( ( AV14OrderedBy == 17 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_Posterior] DESC";
         }
         else if ( ( AV14OrderedBy == 18 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado], T1.[Servico_Ativo]";
         }
         else if ( ( AV14OrderedBy == 18 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Vinculado] DESC, T1.[Servico_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00LP3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18Servico_Nome1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22Servico_Nome2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             short AV25DynamicFiltersOperator3 ,
                                             String AV26Servico_Nome3 ,
                                             String A608Servico_Nome ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A631Servico_Vinculado ,
                                             int AV7Servico_Vinculado )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [7] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Servico] T1 WITH (NOLOCK) LEFT JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Vinculado])";
         scmdbuf = scmdbuf + " WHERE (T1.[Servico_Vinculado] = @AV7Servico_Vinculado)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV18Servico_Nome1)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV18Servico_Nome1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Servico_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV22Servico_Nome2)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Servico_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV22Servico_Nome2)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Servico_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV26Servico_Nome3)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Servico_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV26Servico_Nome3)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 11 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 11 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 12 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 12 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 13 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 13 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 14 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 14 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 15 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 15 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 16 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 16 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 17 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 17 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 18 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 18 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00LP2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
               case 1 :
                     return conditional_H00LP3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00LP2 ;
          prmH00LP2 = new Object[] {
          new Object[] {"@AV7Servico_Vinculado",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00LP3 ;
          prmH00LP3 = new Object[] {
          new Object[] {"@AV7Servico_Vinculado",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26Servico_Nome3",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00LP2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LP2,11,0,true,false )
             ,new CursorDef("H00LP3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LP3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 3) ;
                ((String[]) buf[16])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 3) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((bool[]) buf[22])[0] = rslt.getBool(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((bool[]) buf[26])[0] = rslt.getBool(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((int[]) buf[28])[0] = rslt.getInt(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((String[]) buf[31])[0] = rslt.getString(18, 15) ;
                ((String[]) buf[32])[0] = rslt.getLongVarchar(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((String[]) buf[34])[0] = rslt.getString(20, 50) ;
                ((int[]) buf[35])[0] = rslt.getInt(21) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

}
