/*
               File: FatoresImpactoGeneral
        Description: Fatores Impacto General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:10:2.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class fatoresimpactogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public fatoresimpactogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public fatoresimpactogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           int aP1_FatoresImpacto_Codigo )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.A2127FatoresImpacto_Codigo = aP1_FatoresImpacto_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  A2127FatoresImpacto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A5AreaTrabalho_Codigo,(int)A2127FatoresImpacto_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PASY2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "FatoresImpactoGeneral";
               context.Gx_err = 0;
               WSSY2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Fatores Impacto General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282310254");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("fatoresimpactogeneral.aspx") + "?" + UrlEncode("" +A5AreaTrabalho_Codigo) + "," + UrlEncode("" +A2127FatoresImpacto_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA2127FatoresImpacto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FATORESIMPACTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2127FatoresImpacto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FATORESIMPACTO_VALOREMCSD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2119FatoresImpacto_ValorEMCSD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FATORESIMPACTO_VALOREMCCD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2120FatoresImpacto_ValorEMCCD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FATORESIMPACTO_VALORENCSD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2121FatoresImpacto_ValorENCSD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FATORESIMPACTO_VALORENCCD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2122FatoresImpacto_ValorENCCD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FATORESIMPACTO_VALORCMCSD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2123FatoresImpacto_ValorCMCSD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FATORESIMPACTO_VALORCMCCD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2124FatoresImpacto_ValorCMCCD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FATORESIMPACTO_VALORCNCSD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2125FatoresImpacto_ValorCNCSD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FATORESIMPACTO_VALORCNCCD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2126FatoresImpacto_ValorCNCCD, "Z9.99")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormSY2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("fatoresimpactogeneral.js", "?20204282310256");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "FatoresImpactoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Fatores Impacto General" ;
      }

      protected void WBSY0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "fatoresimpactogeneral.aspx");
            }
            wb_table1_2_SY2( true) ;
         }
         else
         {
            wb_table1_2_SY2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_SY2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTSY2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Fatores Impacto General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPSY0( ) ;
            }
         }
      }

      protected void WSSY2( )
      {
         STARTSY2( ) ;
         EVTSY2( ) ;
      }

      protected void EVTSY2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11SY2 */
                                    E11SY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12SY2 */
                                    E12SY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13SY2 */
                                    E13SY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14SY2 */
                                    E14SY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WESY2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormSY2( ) ;
            }
         }
      }

      protected void PASY2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFSY2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "FatoresImpactoGeneral";
         context.Gx_err = 0;
      }

      protected void RFSY2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00SY2 */
            pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo, A2127FatoresImpacto_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A2126FatoresImpacto_ValorCNCCD = H00SY2_A2126FatoresImpacto_ValorCNCCD[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2126FatoresImpacto_ValorCNCCD", StringUtil.LTrim( StringUtil.Str( A2126FatoresImpacto_ValorCNCCD, 5, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALORCNCCD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2126FatoresImpacto_ValorCNCCD, "Z9.99")));
               A2125FatoresImpacto_ValorCNCSD = H00SY2_A2125FatoresImpacto_ValorCNCSD[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2125FatoresImpacto_ValorCNCSD", StringUtil.LTrim( StringUtil.Str( A2125FatoresImpacto_ValorCNCSD, 5, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALORCNCSD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2125FatoresImpacto_ValorCNCSD, "Z9.99")));
               A2124FatoresImpacto_ValorCMCCD = H00SY2_A2124FatoresImpacto_ValorCMCCD[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2124FatoresImpacto_ValorCMCCD", StringUtil.LTrim( StringUtil.Str( A2124FatoresImpacto_ValorCMCCD, 5, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALORCMCCD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2124FatoresImpacto_ValorCMCCD, "Z9.99")));
               A2123FatoresImpacto_ValorCMCSD = H00SY2_A2123FatoresImpacto_ValorCMCSD[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2123FatoresImpacto_ValorCMCSD", StringUtil.LTrim( StringUtil.Str( A2123FatoresImpacto_ValorCMCSD, 5, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALORCMCSD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2123FatoresImpacto_ValorCMCSD, "Z9.99")));
               A2122FatoresImpacto_ValorENCCD = H00SY2_A2122FatoresImpacto_ValorENCCD[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2122FatoresImpacto_ValorENCCD", StringUtil.LTrim( StringUtil.Str( A2122FatoresImpacto_ValorENCCD, 5, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALORENCCD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2122FatoresImpacto_ValorENCCD, "Z9.99")));
               A2121FatoresImpacto_ValorENCSD = H00SY2_A2121FatoresImpacto_ValorENCSD[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2121FatoresImpacto_ValorENCSD", StringUtil.LTrim( StringUtil.Str( A2121FatoresImpacto_ValorENCSD, 5, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALORENCSD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2121FatoresImpacto_ValorENCSD, "Z9.99")));
               A2120FatoresImpacto_ValorEMCCD = H00SY2_A2120FatoresImpacto_ValorEMCCD[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2120FatoresImpacto_ValorEMCCD", StringUtil.LTrim( StringUtil.Str( A2120FatoresImpacto_ValorEMCCD, 5, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALOREMCCD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2120FatoresImpacto_ValorEMCCD, "Z9.99")));
               A2119FatoresImpacto_ValorEMCSD = H00SY2_A2119FatoresImpacto_ValorEMCSD[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2119FatoresImpacto_ValorEMCSD", StringUtil.LTrim( StringUtil.Str( A2119FatoresImpacto_ValorEMCSD, 5, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALOREMCSD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2119FatoresImpacto_ValorEMCSD, "Z9.99")));
               /* Execute user event: E12SY2 */
               E12SY2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBSY0( ) ;
         }
      }

      protected void STRUPSY0( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "FatoresImpactoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11SY2 */
         E11SY2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A2119FatoresImpacto_ValorEMCSD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorEMCSD_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2119FatoresImpacto_ValorEMCSD", StringUtil.LTrim( StringUtil.Str( A2119FatoresImpacto_ValorEMCSD, 5, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALOREMCSD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2119FatoresImpacto_ValorEMCSD, "Z9.99")));
            A2120FatoresImpacto_ValorEMCCD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorEMCCD_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2120FatoresImpacto_ValorEMCCD", StringUtil.LTrim( StringUtil.Str( A2120FatoresImpacto_ValorEMCCD, 5, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALOREMCCD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2120FatoresImpacto_ValorEMCCD, "Z9.99")));
            A2121FatoresImpacto_ValorENCSD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorENCSD_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2121FatoresImpacto_ValorENCSD", StringUtil.LTrim( StringUtil.Str( A2121FatoresImpacto_ValorENCSD, 5, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALORENCSD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2121FatoresImpacto_ValorENCSD, "Z9.99")));
            A2122FatoresImpacto_ValorENCCD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorENCCD_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2122FatoresImpacto_ValorENCCD", StringUtil.LTrim( StringUtil.Str( A2122FatoresImpacto_ValorENCCD, 5, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALORENCCD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2122FatoresImpacto_ValorENCCD, "Z9.99")));
            A2123FatoresImpacto_ValorCMCSD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCMCSD_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2123FatoresImpacto_ValorCMCSD", StringUtil.LTrim( StringUtil.Str( A2123FatoresImpacto_ValorCMCSD, 5, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALORCMCSD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2123FatoresImpacto_ValorCMCSD, "Z9.99")));
            A2124FatoresImpacto_ValorCMCCD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCMCCD_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2124FatoresImpacto_ValorCMCCD", StringUtil.LTrim( StringUtil.Str( A2124FatoresImpacto_ValorCMCCD, 5, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALORCMCCD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2124FatoresImpacto_ValorCMCCD, "Z9.99")));
            A2125FatoresImpacto_ValorCNCSD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCNCSD_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2125FatoresImpacto_ValorCNCSD", StringUtil.LTrim( StringUtil.Str( A2125FatoresImpacto_ValorCNCSD, 5, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALORCNCSD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2125FatoresImpacto_ValorCNCSD, "Z9.99")));
            A2126FatoresImpacto_ValorCNCCD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCNCCD_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2126FatoresImpacto_ValorCNCCD", StringUtil.LTrim( StringUtil.Str( A2126FatoresImpacto_ValorCNCCD, 5, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FATORESIMPACTO_VALORCNCCD", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2126FatoresImpacto_ValorCNCCD, "Z9.99")));
            /* Read saved values. */
            wcpOA5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA5AreaTrabalho_Codigo"), ",", "."));
            wcpOA2127FatoresImpacto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA2127FatoresImpacto_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11SY2 */
         E11SY2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11SY2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12SY2( )
      {
         /* Load Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E13SY2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("fatoresimpacto.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A5AreaTrabalho_Codigo) + "," + UrlEncode("" +A2127FatoresImpacto_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14SY2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("fatoresimpacto.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A5AreaTrabalho_Codigo) + "," + UrlEncode("" +A2127FatoresImpacto_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "FatoresImpacto";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "AreaTrabalho_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "FatoresImpacto_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV12FatoresImpacto_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_SY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_SY2( true) ;
         }
         else
         {
            wb_table2_8_SY2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_SY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_59_SY2( true) ;
         }
         else
         {
            wb_table3_59_SY2( false) ;
         }
         return  ;
      }

      protected void wb_table3_59_SY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_SY2e( true) ;
         }
         else
         {
            wb_table1_2_SY2e( false) ;
         }
      }

      protected void wb_table3_59_SY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_59_SY2e( true) ;
         }
         else
         {
            wb_table3_59_SY2e( false) ;
         }
      }

      protected void wb_table2_8_SY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup2_Internalname, "Projetos de Melhoria sem Garantia (Evolutiva)", 1, 0, "px", 0, "px", "Group", "", "HLP_FatoresImpactoGeneral.htm");
            wb_table4_12_SY2( true) ;
         }
         else
         {
            wb_table4_12_SY2( false) ;
         }
         return  ;
      }

      protected void wb_table4_12_SY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup4_Internalname, "Projetos de Melhoria com Garantia (Corretiva)", 1, 0, "px", 0, "px", "Group", "", "HLP_FatoresImpactoGeneral.htm");
            wb_table5_36_SY2( true) ;
         }
         else
         {
            wb_table5_36_SY2( false) ;
         }
         return  ;
      }

      protected void wb_table5_36_SY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_SY2e( true) ;
         }
         else
         {
            wb_table2_8_SY2e( false) ;
         }
      }

      protected void wb_table5_36_SY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valorcmcsd_Internalname, "Funcionalidade Mantida pela Contratada atual S/Redocumentação - CMCSD", "", "", lblTextblockfatoresimpacto_valorcmcsd_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorCMCSD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2123FatoresImpacto_ValorCMCSD, 5, 2, ",", "")), context.localUtil.Format( A2123FatoresImpacto_ValorCMCSD, "Z9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorCMCSD_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valorcmccd_Internalname, "Funcionalidade Mantida pela Contratada atual C/Redocumentação - CMCCD", "", "", lblTextblockfatoresimpacto_valorcmccd_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorCMCCD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2124FatoresImpacto_ValorCMCCD, 5, 2, ",", "")), context.localUtil.Format( A2124FatoresImpacto_ValorCMCCD, "Z9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorCMCCD_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valorcncsd_Internalname, "Funcionalidade Não Mantida pela Contratada atual S/Redocumentação - CNCSD", "", "", lblTextblockfatoresimpacto_valorcncsd_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorCNCSD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2125FatoresImpacto_ValorCNCSD, 5, 2, ",", "")), context.localUtil.Format( A2125FatoresImpacto_ValorCNCSD, "Z9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorCNCSD_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valorcnccd_Internalname, "Funcionalidade Não Mantida pela Contratada atual C/Redocumentação - CNCCD", "", "", lblTextblockfatoresimpacto_valorcnccd_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorCNCCD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2126FatoresImpacto_ValorCNCCD, 5, 2, ",", "")), context.localUtil.Format( A2126FatoresImpacto_ValorCNCCD, "Z9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorCNCCD_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_36_SY2e( true) ;
         }
         else
         {
            wb_table5_36_SY2e( false) ;
         }
      }

      protected void wb_table4_12_SY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valoremcsd_Internalname, "Funcionalidade Mantida pela Contratada atual S/Redocumentação - EMCSD", "", "", lblTextblockfatoresimpacto_valoremcsd_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorEMCSD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2119FatoresImpacto_ValorEMCSD, 5, 2, ",", "")), context.localUtil.Format( A2119FatoresImpacto_ValorEMCSD, "Z9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorEMCSD_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valoremccd_Internalname, "Funcionalidade Mantida pela Contratada atual C/Redocumentação - EMCCD", "", "", lblTextblockfatoresimpacto_valoremccd_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorEMCCD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2120FatoresImpacto_ValorEMCCD, 5, 2, ",", "")), context.localUtil.Format( A2120FatoresImpacto_ValorEMCCD, "Z9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorEMCCD_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valorencsd_Internalname, "Funcionalidade Não Mantida pela Contratada atual S/Redocumentação - ENCSD", "", "", lblTextblockfatoresimpacto_valorencsd_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorENCSD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2121FatoresImpacto_ValorENCSD, 5, 2, ",", "")), context.localUtil.Format( A2121FatoresImpacto_ValorENCSD, "Z9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorENCSD_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfatoresimpacto_valorenccd_Internalname, "Funcionalidade Não Mantida pela Contratada atual C/Redocumentação - ENCCD", "", "", lblTextblockfatoresimpacto_valorenccd_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFatoresImpacto_ValorENCCD_Internalname, StringUtil.LTrim( StringUtil.NToC( A2122FatoresImpacto_ValorENCCD, 5, 2, ",", "")), context.localUtil.Format( A2122FatoresImpacto_ValorENCCD, "Z9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFatoresImpacto_ValorENCCD_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FatoresImpactoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_12_SY2e( true) ;
         }
         else
         {
            wb_table4_12_SY2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A5AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         A2127FatoresImpacto_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PASY2( ) ;
         WSSY2( ) ;
         WESY2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA5AreaTrabalho_Codigo = (String)((String)getParm(obj,0));
         sCtrlA2127FatoresImpacto_Codigo = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PASY2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "fatoresimpactogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PASY2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A5AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            A2127FatoresImpacto_Codigo = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
         }
         wcpOA5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA5AreaTrabalho_Codigo"), ",", "."));
         wcpOA2127FatoresImpacto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA2127FatoresImpacto_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A5AreaTrabalho_Codigo != wcpOA5AreaTrabalho_Codigo ) || ( A2127FatoresImpacto_Codigo != wcpOA2127FatoresImpacto_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
         wcpOA2127FatoresImpacto_Codigo = A2127FatoresImpacto_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA5AreaTrabalho_Codigo = cgiGet( sPrefix+"A5AreaTrabalho_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA5AreaTrabalho_Codigo) > 0 )
         {
            A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA5AreaTrabalho_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         }
         else
         {
            A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A5AreaTrabalho_Codigo_PARM"), ",", "."));
         }
         sCtrlA2127FatoresImpacto_Codigo = cgiGet( sPrefix+"A2127FatoresImpacto_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA2127FatoresImpacto_Codigo) > 0 )
         {
            A2127FatoresImpacto_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA2127FatoresImpacto_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2127FatoresImpacto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2127FatoresImpacto_Codigo), 6, 0)));
         }
         else
         {
            A2127FatoresImpacto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A2127FatoresImpacto_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PASY2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSSY2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSSY2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A5AreaTrabalho_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA5AreaTrabalho_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A5AreaTrabalho_Codigo_CTRL", StringUtil.RTrim( sCtrlA5AreaTrabalho_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A2127FatoresImpacto_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2127FatoresImpacto_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA2127FatoresImpacto_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A2127FatoresImpacto_Codigo_CTRL", StringUtil.RTrim( sCtrlA2127FatoresImpacto_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WESY2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282310292");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("fatoresimpactogeneral.js", "?20204282310292");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockfatoresimpacto_valoremcsd_Internalname = sPrefix+"TEXTBLOCKFATORESIMPACTO_VALOREMCSD";
         edtFatoresImpacto_ValorEMCSD_Internalname = sPrefix+"FATORESIMPACTO_VALOREMCSD";
         lblTextblockfatoresimpacto_valoremccd_Internalname = sPrefix+"TEXTBLOCKFATORESIMPACTO_VALOREMCCD";
         edtFatoresImpacto_ValorEMCCD_Internalname = sPrefix+"FATORESIMPACTO_VALOREMCCD";
         lblTextblockfatoresimpacto_valorencsd_Internalname = sPrefix+"TEXTBLOCKFATORESIMPACTO_VALORENCSD";
         edtFatoresImpacto_ValorENCSD_Internalname = sPrefix+"FATORESIMPACTO_VALORENCSD";
         lblTextblockfatoresimpacto_valorenccd_Internalname = sPrefix+"TEXTBLOCKFATORESIMPACTO_VALORENCCD";
         edtFatoresImpacto_ValorENCCD_Internalname = sPrefix+"FATORESIMPACTO_VALORENCCD";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         grpUnnamedgroup2_Internalname = sPrefix+"UNNAMEDGROUP2";
         lblTextblockfatoresimpacto_valorcmcsd_Internalname = sPrefix+"TEXTBLOCKFATORESIMPACTO_VALORCMCSD";
         edtFatoresImpacto_ValorCMCSD_Internalname = sPrefix+"FATORESIMPACTO_VALORCMCSD";
         lblTextblockfatoresimpacto_valorcmccd_Internalname = sPrefix+"TEXTBLOCKFATORESIMPACTO_VALORCMCCD";
         edtFatoresImpacto_ValorCMCCD_Internalname = sPrefix+"FATORESIMPACTO_VALORCMCCD";
         lblTextblockfatoresimpacto_valorcncsd_Internalname = sPrefix+"TEXTBLOCKFATORESIMPACTO_VALORCNCSD";
         edtFatoresImpacto_ValorCNCSD_Internalname = sPrefix+"FATORESIMPACTO_VALORCNCSD";
         lblTextblockfatoresimpacto_valorcnccd_Internalname = sPrefix+"TEXTBLOCKFATORESIMPACTO_VALORCNCCD";
         edtFatoresImpacto_ValorCNCCD_Internalname = sPrefix+"FATORESIMPACTO_VALORCNCCD";
         tblUnnamedtable3_Internalname = sPrefix+"UNNAMEDTABLE3";
         grpUnnamedgroup4_Internalname = sPrefix+"UNNAMEDGROUP4";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtFatoresImpacto_ValorENCCD_Jsonclick = "";
         edtFatoresImpacto_ValorENCSD_Jsonclick = "";
         edtFatoresImpacto_ValorEMCCD_Jsonclick = "";
         edtFatoresImpacto_ValorEMCSD_Jsonclick = "";
         edtFatoresImpacto_ValorCNCCD_Jsonclick = "";
         edtFatoresImpacto_ValorCNCSD_Jsonclick = "";
         edtFatoresImpacto_ValorCMCCD_Jsonclick = "";
         edtFatoresImpacto_ValorCMCSD_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13SY2',iparms:[{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2127FatoresImpacto_Codigo',fld:'FATORESIMPACTO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14SY2',iparms:[{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2127FatoresImpacto_Codigo',fld:'FATORESIMPACTO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00SY2_A5AreaTrabalho_Codigo = new int[1] ;
         H00SY2_A2127FatoresImpacto_Codigo = new int[1] ;
         H00SY2_A2126FatoresImpacto_ValorCNCCD = new decimal[1] ;
         H00SY2_A2125FatoresImpacto_ValorCNCSD = new decimal[1] ;
         H00SY2_A2124FatoresImpacto_ValorCMCCD = new decimal[1] ;
         H00SY2_A2123FatoresImpacto_ValorCMCSD = new decimal[1] ;
         H00SY2_A2122FatoresImpacto_ValorENCCD = new decimal[1] ;
         H00SY2_A2121FatoresImpacto_ValorENCSD = new decimal[1] ;
         H00SY2_A2120FatoresImpacto_ValorEMCCD = new decimal[1] ;
         H00SY2_A2119FatoresImpacto_ValorEMCSD = new decimal[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockfatoresimpacto_valorcmcsd_Jsonclick = "";
         lblTextblockfatoresimpacto_valorcmccd_Jsonclick = "";
         lblTextblockfatoresimpacto_valorcncsd_Jsonclick = "";
         lblTextblockfatoresimpacto_valorcnccd_Jsonclick = "";
         lblTextblockfatoresimpacto_valoremcsd_Jsonclick = "";
         lblTextblockfatoresimpacto_valoremccd_Jsonclick = "";
         lblTextblockfatoresimpacto_valorencsd_Jsonclick = "";
         lblTextblockfatoresimpacto_valorenccd_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA5AreaTrabalho_Codigo = "";
         sCtrlA2127FatoresImpacto_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.fatoresimpactogeneral__default(),
            new Object[][] {
                new Object[] {
               H00SY2_A5AreaTrabalho_Codigo, H00SY2_A2127FatoresImpacto_Codigo, H00SY2_A2126FatoresImpacto_ValorCNCCD, H00SY2_A2125FatoresImpacto_ValorCNCSD, H00SY2_A2124FatoresImpacto_ValorCMCCD, H00SY2_A2123FatoresImpacto_ValorCMCSD, H00SY2_A2122FatoresImpacto_ValorENCCD, H00SY2_A2121FatoresImpacto_ValorENCSD, H00SY2_A2120FatoresImpacto_ValorEMCCD, H00SY2_A2119FatoresImpacto_ValorEMCSD
               }
            }
         );
         AV15Pgmname = "FatoresImpactoGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "FatoresImpactoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A5AreaTrabalho_Codigo ;
      private int A2127FatoresImpacto_Codigo ;
      private int wcpOA5AreaTrabalho_Codigo ;
      private int wcpOA2127FatoresImpacto_Codigo ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7AreaTrabalho_Codigo ;
      private int AV12FatoresImpacto_Codigo ;
      private int idxLst ;
      private decimal A2119FatoresImpacto_ValorEMCSD ;
      private decimal A2120FatoresImpacto_ValorEMCCD ;
      private decimal A2121FatoresImpacto_ValorENCSD ;
      private decimal A2122FatoresImpacto_ValorENCCD ;
      private decimal A2123FatoresImpacto_ValorCMCSD ;
      private decimal A2124FatoresImpacto_ValorCMCCD ;
      private decimal A2125FatoresImpacto_ValorCNCSD ;
      private decimal A2126FatoresImpacto_ValorCNCCD ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtFatoresImpacto_ValorEMCSD_Internalname ;
      private String edtFatoresImpacto_ValorEMCCD_Internalname ;
      private String edtFatoresImpacto_ValorENCSD_Internalname ;
      private String edtFatoresImpacto_ValorENCCD_Internalname ;
      private String edtFatoresImpacto_ValorCMCSD_Internalname ;
      private String edtFatoresImpacto_ValorCMCCD_Internalname ;
      private String edtFatoresImpacto_ValorCNCSD_Internalname ;
      private String edtFatoresImpacto_ValorCNCCD_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String grpUnnamedgroup2_Internalname ;
      private String grpUnnamedgroup4_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTextblockfatoresimpacto_valorcmcsd_Internalname ;
      private String lblTextblockfatoresimpacto_valorcmcsd_Jsonclick ;
      private String edtFatoresImpacto_ValorCMCSD_Jsonclick ;
      private String lblTextblockfatoresimpacto_valorcmccd_Internalname ;
      private String lblTextblockfatoresimpacto_valorcmccd_Jsonclick ;
      private String edtFatoresImpacto_ValorCMCCD_Jsonclick ;
      private String lblTextblockfatoresimpacto_valorcncsd_Internalname ;
      private String lblTextblockfatoresimpacto_valorcncsd_Jsonclick ;
      private String edtFatoresImpacto_ValorCNCSD_Jsonclick ;
      private String lblTextblockfatoresimpacto_valorcnccd_Internalname ;
      private String lblTextblockfatoresimpacto_valorcnccd_Jsonclick ;
      private String edtFatoresImpacto_ValorCNCCD_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockfatoresimpacto_valoremcsd_Internalname ;
      private String lblTextblockfatoresimpacto_valoremcsd_Jsonclick ;
      private String edtFatoresImpacto_ValorEMCSD_Jsonclick ;
      private String lblTextblockfatoresimpacto_valoremccd_Internalname ;
      private String lblTextblockfatoresimpacto_valoremccd_Jsonclick ;
      private String edtFatoresImpacto_ValorEMCCD_Jsonclick ;
      private String lblTextblockfatoresimpacto_valorencsd_Internalname ;
      private String lblTextblockfatoresimpacto_valorencsd_Jsonclick ;
      private String edtFatoresImpacto_ValorENCSD_Jsonclick ;
      private String lblTextblockfatoresimpacto_valorenccd_Internalname ;
      private String lblTextblockfatoresimpacto_valorenccd_Jsonclick ;
      private String edtFatoresImpacto_ValorENCCD_Jsonclick ;
      private String sCtrlA5AreaTrabalho_Codigo ;
      private String sCtrlA2127FatoresImpacto_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00SY2_A5AreaTrabalho_Codigo ;
      private int[] H00SY2_A2127FatoresImpacto_Codigo ;
      private decimal[] H00SY2_A2126FatoresImpacto_ValorCNCCD ;
      private decimal[] H00SY2_A2125FatoresImpacto_ValorCNCSD ;
      private decimal[] H00SY2_A2124FatoresImpacto_ValorCMCCD ;
      private decimal[] H00SY2_A2123FatoresImpacto_ValorCMCSD ;
      private decimal[] H00SY2_A2122FatoresImpacto_ValorENCCD ;
      private decimal[] H00SY2_A2121FatoresImpacto_ValorENCSD ;
      private decimal[] H00SY2_A2120FatoresImpacto_ValorEMCCD ;
      private decimal[] H00SY2_A2119FatoresImpacto_ValorEMCSD ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class fatoresimpactogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00SY2 ;
          prmH00SY2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FatoresImpacto_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00SY2", "SELECT [AreaTrabalho_Codigo], [FatoresImpacto_Codigo], [FatoresImpacto_ValorCNCCD], [FatoresImpacto_ValorCNCSD], [FatoresImpacto_ValorCMCCD], [FatoresImpacto_ValorCMCSD], [FatoresImpacto_ValorENCCD], [FatoresImpacto_ValorENCSD], [FatoresImpacto_ValorEMCCD], [FatoresImpacto_ValorEMCSD] FROM [FatoresImpacto] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo and [FatoresImpacto_Codigo] = @FatoresImpacto_Codigo ORDER BY [AreaTrabalho_Codigo], [FatoresImpacto_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SY2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(8) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(9) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
