/*
               File: PRC_UpdAnexo
        Description: Update Anexo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:4.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updanexo : GXProcedure
   {
      public prc_updanexo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updanexo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Local_Codigo ,
                           int aP1_Tabela ,
                           String aP2_Arquivo )
      {
         this.AV8Local_Codigo = aP0_Local_Codigo;
         this.AV9Tabela = aP1_Tabela;
         this.AV10Arquivo = aP2_Arquivo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Local_Codigo ,
                                 int aP1_Tabela ,
                                 String aP2_Arquivo )
      {
         prc_updanexo objprc_updanexo;
         objprc_updanexo = new prc_updanexo();
         objprc_updanexo.AV8Local_Codigo = aP0_Local_Codigo;
         objprc_updanexo.AV9Tabela = aP1_Tabela;
         objprc_updanexo.AV10Arquivo = aP2_Arquivo;
         objprc_updanexo.context.SetSubmitInitialConfig(context);
         objprc_updanexo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updanexo);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updanexo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV9Tabela == 2 )
         {
            /* Using cursor P00W32 */
            pr_default.execute(0, new Object[] {AV8Local_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A586ContagemResultadoEvidencia_Codigo = P00W32_A586ContagemResultadoEvidencia_Codigo[0];
               A589ContagemResultadoEvidencia_NomeArq = P00W32_A589ContagemResultadoEvidencia_NomeArq[0];
               n589ContagemResultadoEvidencia_NomeArq = P00W32_n589ContagemResultadoEvidencia_NomeArq[0];
               A590ContagemResultadoEvidencia_TipoArq = P00W32_A590ContagemResultadoEvidencia_TipoArq[0];
               n590ContagemResultadoEvidencia_TipoArq = P00W32_n590ContagemResultadoEvidencia_TipoArq[0];
               A588ContagemResultadoEvidencia_Arquivo = P00W32_A588ContagemResultadoEvidencia_Arquivo[0];
               n588ContagemResultadoEvidencia_Arquivo = P00W32_n588ContagemResultadoEvidencia_Arquivo[0];
               AV12NomeArq = A589ContagemResultadoEvidencia_NomeArq;
               A588ContagemResultadoEvidencia_Arquivo = AV10Arquivo;
               n588ContagemResultadoEvidencia_Arquivo = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00W33 */
               A590ContagemResultadoEvidencia_TipoArq = FileUtil.GetFileType( A588ContagemResultadoEvidencia_Arquivo);
               n590ContagemResultadoEvidencia_TipoArq = false;
               A589ContagemResultadoEvidencia_NomeArq = FileUtil.GetFileName( A588ContagemResultadoEvidencia_Arquivo);
               n589ContagemResultadoEvidencia_NomeArq = false;
               pr_default.execute(1, new Object[] {n588ContagemResultadoEvidencia_Arquivo, A588ContagemResultadoEvidencia_Arquivo, n590ContagemResultadoEvidencia_TipoArq, A590ContagemResultadoEvidencia_TipoArq, n589ContagemResultadoEvidencia_NomeArq, A589ContagemResultadoEvidencia_NomeArq, A586ContagemResultadoEvidencia_Codigo});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
               if (true) break;
               /* Using cursor P00W34 */
               A590ContagemResultadoEvidencia_TipoArq = FileUtil.GetFileType( A588ContagemResultadoEvidencia_Arquivo);
               n590ContagemResultadoEvidencia_TipoArq = false;
               A589ContagemResultadoEvidencia_NomeArq = FileUtil.GetFileName( A588ContagemResultadoEvidencia_Arquivo);
               n589ContagemResultadoEvidencia_NomeArq = false;
               pr_default.execute(2, new Object[] {n588ContagemResultadoEvidencia_Arquivo, A588ContagemResultadoEvidencia_Arquivo, n590ContagemResultadoEvidencia_TipoArq, A590ContagemResultadoEvidencia_TipoArq, n589ContagemResultadoEvidencia_NomeArq, A589ContagemResultadoEvidencia_NomeArq, A586ContagemResultadoEvidencia_Codigo});
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            /* Using cursor P00W35 */
            pr_default.execute(3, new Object[] {AV8Local_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A586ContagemResultadoEvidencia_Codigo = P00W35_A586ContagemResultadoEvidencia_Codigo[0];
               A589ContagemResultadoEvidencia_NomeArq = P00W35_A589ContagemResultadoEvidencia_NomeArq[0];
               n589ContagemResultadoEvidencia_NomeArq = P00W35_n589ContagemResultadoEvidencia_NomeArq[0];
               A589ContagemResultadoEvidencia_NomeArq = AV12NomeArq;
               n589ContagemResultadoEvidencia_NomeArq = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00W36 */
               A589ContagemResultadoEvidencia_NomeArq = FileUtil.GetFileName( A588ContagemResultadoEvidencia_Arquivo);
               n589ContagemResultadoEvidencia_NomeArq = false;
               pr_default.execute(4, new Object[] {n589ContagemResultadoEvidencia_NomeArq, A589ContagemResultadoEvidencia_NomeArq, A586ContagemResultadoEvidencia_Codigo});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
               if (true) break;
               /* Using cursor P00W37 */
               A589ContagemResultadoEvidencia_NomeArq = FileUtil.GetFileName( A588ContagemResultadoEvidencia_Arquivo);
               n589ContagemResultadoEvidencia_NomeArq = false;
               pr_default.execute(5, new Object[] {n589ContagemResultadoEvidencia_NomeArq, A589ContagemResultadoEvidencia_NomeArq, A586ContagemResultadoEvidencia_Codigo});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
         }
         else if ( AV9Tabela == 3 )
         {
            /* Using cursor P00W38 */
            pr_default.execute(6, new Object[] {AV8Local_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A1106Anexo_Codigo = P00W38_A1106Anexo_Codigo[0];
               A1107Anexo_NomeArq = P00W38_A1107Anexo_NomeArq[0];
               n1107Anexo_NomeArq = P00W38_n1107Anexo_NomeArq[0];
               A1108Anexo_TipoArq = P00W38_A1108Anexo_TipoArq[0];
               n1108Anexo_TipoArq = P00W38_n1108Anexo_TipoArq[0];
               A1101Anexo_Arquivo = P00W38_A1101Anexo_Arquivo[0];
               n1101Anexo_Arquivo = P00W38_n1101Anexo_Arquivo[0];
               AV12NomeArq = A1107Anexo_NomeArq;
               A1101Anexo_Arquivo = AV10Arquivo;
               n1101Anexo_Arquivo = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00W39 */
               A1108Anexo_TipoArq = FileUtil.GetFileType( A1101Anexo_Arquivo);
               n1108Anexo_TipoArq = false;
               A1107Anexo_NomeArq = FileUtil.GetFileName( A1101Anexo_Arquivo);
               n1107Anexo_NomeArq = false;
               pr_default.execute(7, new Object[] {n1101Anexo_Arquivo, A1101Anexo_Arquivo, n1108Anexo_TipoArq, A1108Anexo_TipoArq, n1107Anexo_NomeArq, A1107Anexo_NomeArq, A1106Anexo_Codigo});
               pr_default.close(7);
               dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
               if (true) break;
               /* Using cursor P00W310 */
               A1108Anexo_TipoArq = FileUtil.GetFileType( A1101Anexo_Arquivo);
               n1108Anexo_TipoArq = false;
               A1107Anexo_NomeArq = FileUtil.GetFileName( A1101Anexo_Arquivo);
               n1107Anexo_NomeArq = false;
               pr_default.execute(8, new Object[] {n1101Anexo_Arquivo, A1101Anexo_Arquivo, n1108Anexo_TipoArq, A1108Anexo_TipoArq, n1107Anexo_NomeArq, A1107Anexo_NomeArq, A1106Anexo_Codigo});
               pr_default.close(8);
               dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(6);
            /* Using cursor P00W311 */
            pr_default.execute(9, new Object[] {AV8Local_Codigo});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A1106Anexo_Codigo = P00W311_A1106Anexo_Codigo[0];
               A1107Anexo_NomeArq = P00W311_A1107Anexo_NomeArq[0];
               n1107Anexo_NomeArq = P00W311_n1107Anexo_NomeArq[0];
               A1107Anexo_NomeArq = AV12NomeArq;
               n1107Anexo_NomeArq = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00W312 */
               A1107Anexo_NomeArq = FileUtil.GetFileName( A1101Anexo_Arquivo);
               n1107Anexo_NomeArq = false;
               pr_default.execute(10, new Object[] {n1107Anexo_NomeArq, A1107Anexo_NomeArq, A1106Anexo_Codigo});
               pr_default.close(10);
               dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
               if (true) break;
               /* Using cursor P00W313 */
               A1107Anexo_NomeArq = FileUtil.GetFileName( A1101Anexo_Arquivo);
               n1107Anexo_NomeArq = false;
               pr_default.execute(11, new Object[] {n1107Anexo_NomeArq, A1107Anexo_NomeArq, A1106Anexo_Codigo});
               pr_default.close(11);
               dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(9);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UpdAnexo");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00W32_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         P00W32_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         P00W32_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         P00W32_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         P00W32_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         P00W32_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         P00W32_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         A589ContagemResultadoEvidencia_NomeArq = "";
         A590ContagemResultadoEvidencia_TipoArq = "";
         A588ContagemResultadoEvidencia_Arquivo = "";
         AV12NomeArq = "";
         P00W35_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         P00W35_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         P00W35_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         P00W38_A1106Anexo_Codigo = new int[1] ;
         P00W38_A1107Anexo_NomeArq = new String[] {""} ;
         P00W38_n1107Anexo_NomeArq = new bool[] {false} ;
         P00W38_A1108Anexo_TipoArq = new String[] {""} ;
         P00W38_n1108Anexo_TipoArq = new bool[] {false} ;
         P00W38_A1101Anexo_Arquivo = new String[] {""} ;
         P00W38_n1101Anexo_Arquivo = new bool[] {false} ;
         A1107Anexo_NomeArq = "";
         A1108Anexo_TipoArq = "";
         A1101Anexo_Arquivo = "";
         P00W311_A1106Anexo_Codigo = new int[1] ;
         P00W311_A1107Anexo_NomeArq = new String[] {""} ;
         P00W311_n1107Anexo_NomeArq = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updanexo__default(),
            new Object[][] {
                new Object[] {
               P00W32_A586ContagemResultadoEvidencia_Codigo, P00W32_A589ContagemResultadoEvidencia_NomeArq, P00W32_n589ContagemResultadoEvidencia_NomeArq, P00W32_A590ContagemResultadoEvidencia_TipoArq, P00W32_n590ContagemResultadoEvidencia_TipoArq, P00W32_A588ContagemResultadoEvidencia_Arquivo, P00W32_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00W35_A586ContagemResultadoEvidencia_Codigo, P00W35_A589ContagemResultadoEvidencia_NomeArq, P00W35_n589ContagemResultadoEvidencia_NomeArq
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00W38_A1106Anexo_Codigo, P00W38_A1107Anexo_NomeArq, P00W38_n1107Anexo_NomeArq, P00W38_A1108Anexo_TipoArq, P00W38_n1108Anexo_TipoArq, P00W38_A1101Anexo_Arquivo, P00W38_n1101Anexo_Arquivo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00W311_A1106Anexo_Codigo, P00W311_A1107Anexo_NomeArq, P00W311_n1107Anexo_NomeArq
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Local_Codigo ;
      private int AV9Tabela ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int A1106Anexo_Codigo ;
      private String scmdbuf ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private String AV12NomeArq ;
      private String A1107Anexo_NomeArq ;
      private String A1108Anexo_TipoArq ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool n588ContagemResultadoEvidencia_Arquivo ;
      private bool n1107Anexo_NomeArq ;
      private bool n1108Anexo_TipoArq ;
      private bool n1101Anexo_Arquivo ;
      private String AV10Arquivo ;
      private String A588ContagemResultadoEvidencia_Arquivo ;
      private String A1101Anexo_Arquivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00W32_A586ContagemResultadoEvidencia_Codigo ;
      private String[] P00W32_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] P00W32_n589ContagemResultadoEvidencia_NomeArq ;
      private String[] P00W32_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] P00W32_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] P00W32_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] P00W32_n588ContagemResultadoEvidencia_Arquivo ;
      private int[] P00W35_A586ContagemResultadoEvidencia_Codigo ;
      private String[] P00W35_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] P00W35_n589ContagemResultadoEvidencia_NomeArq ;
      private int[] P00W38_A1106Anexo_Codigo ;
      private String[] P00W38_A1107Anexo_NomeArq ;
      private bool[] P00W38_n1107Anexo_NomeArq ;
      private String[] P00W38_A1108Anexo_TipoArq ;
      private bool[] P00W38_n1108Anexo_TipoArq ;
      private String[] P00W38_A1101Anexo_Arquivo ;
      private bool[] P00W38_n1101Anexo_Arquivo ;
      private int[] P00W311_A1106Anexo_Codigo ;
      private String[] P00W311_A1107Anexo_NomeArq ;
      private bool[] P00W311_n1107Anexo_NomeArq ;
   }

   public class prc_updanexo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00W32 ;
          prmP00W32 = new Object[] {
          new Object[] {"@AV8Local_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W33 ;
          prmP00W33 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W34 ;
          prmP00W34 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W35 ;
          prmP00W35 = new Object[] {
          new Object[] {"@AV8Local_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W36 ;
          prmP00W36 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W37 ;
          prmP00W37 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W38 ;
          prmP00W38 = new Object[] {
          new Object[] {"@AV8Local_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W39 ;
          prmP00W39 = new Object[] {
          new Object[] {"@Anexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Anexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Anexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W310 ;
          prmP00W310 = new Object[] {
          new Object[] {"@Anexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Anexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Anexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W311 ;
          prmP00W311 = new Object[] {
          new Object[] {"@AV8Local_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W312 ;
          prmP00W312 = new Object[] {
          new Object[] {"@Anexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W313 ;
          prmP00W313 = new Object[] {
          new Object[] {"@Anexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00W32", "SELECT TOP 1 [ContagemResultadoEvidencia_Codigo], [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Arquivo] FROM [ContagemResultadoEvidencia] WITH (UPDLOCK) WHERE [ContagemResultadoEvidencia_Codigo] = @AV8Local_Codigo ORDER BY [ContagemResultadoEvidencia_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W32,1,0,true,true )
             ,new CursorDef("P00W33", "UPDATE [ContagemResultadoEvidencia] SET [ContagemResultadoEvidencia_Arquivo]=@ContagemResultadoEvidencia_Arquivo, [ContagemResultadoEvidencia_TipoArq]=@ContagemResultadoEvidencia_TipoArq, [ContagemResultadoEvidencia_NomeArq]=@ContagemResultadoEvidencia_NomeArq  WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W33)
             ,new CursorDef("P00W34", "UPDATE [ContagemResultadoEvidencia] SET [ContagemResultadoEvidencia_Arquivo]=@ContagemResultadoEvidencia_Arquivo, [ContagemResultadoEvidencia_TipoArq]=@ContagemResultadoEvidencia_TipoArq, [ContagemResultadoEvidencia_NomeArq]=@ContagemResultadoEvidencia_NomeArq  WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W34)
             ,new CursorDef("P00W35", "SELECT TOP 1 [ContagemResultadoEvidencia_Codigo], [ContagemResultadoEvidencia_NomeArq] FROM [ContagemResultadoEvidencia] WITH (UPDLOCK) WHERE [ContagemResultadoEvidencia_Codigo] = @AV8Local_Codigo ORDER BY [ContagemResultadoEvidencia_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W35,1,0,true,true )
             ,new CursorDef("P00W36", "UPDATE [ContagemResultadoEvidencia] SET [ContagemResultadoEvidencia_NomeArq]=@ContagemResultadoEvidencia_NomeArq  WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W36)
             ,new CursorDef("P00W37", "UPDATE [ContagemResultadoEvidencia] SET [ContagemResultadoEvidencia_NomeArq]=@ContagemResultadoEvidencia_NomeArq  WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W37)
             ,new CursorDef("P00W38", "SELECT TOP 1 [Anexo_Codigo], [Anexo_NomeArq], [Anexo_TipoArq], [Anexo_Arquivo] FROM [Anexos] WITH (UPDLOCK) WHERE [Anexo_Codigo] = @AV8Local_Codigo ORDER BY [Anexo_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W38,1,0,true,true )
             ,new CursorDef("P00W39", "UPDATE [Anexos] SET [Anexo_Arquivo]=@Anexo_Arquivo, [Anexo_TipoArq]=@Anexo_TipoArq, [Anexo_NomeArq]=@Anexo_NomeArq  WHERE [Anexo_Codigo] = @Anexo_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W39)
             ,new CursorDef("P00W310", "UPDATE [Anexos] SET [Anexo_Arquivo]=@Anexo_Arquivo, [Anexo_TipoArq]=@Anexo_TipoArq, [Anexo_NomeArq]=@Anexo_NomeArq  WHERE [Anexo_Codigo] = @Anexo_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W310)
             ,new CursorDef("P00W311", "SELECT TOP 1 [Anexo_Codigo], [Anexo_NomeArq] FROM [Anexos] WITH (UPDLOCK) WHERE [Anexo_Codigo] = @AV8Local_Codigo ORDER BY [Anexo_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W311,1,0,true,true )
             ,new CursorDef("P00W312", "UPDATE [Anexos] SET [Anexo_NomeArq]=@Anexo_NomeArq  WHERE [Anexo_Codigo] = @Anexo_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W312)
             ,new CursorDef("P00W313", "UPDATE [Anexos] SET [Anexo_NomeArq]=@Anexo_NomeArq  WHERE [Anexo_Codigo] = @Anexo_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W313)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getBLOBFile(4, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getBLOBFile(4, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
