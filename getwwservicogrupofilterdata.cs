/*
               File: GetWWServicoGrupoFilterData
        Description: Get WWServico Grupo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:24.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwservicogrupofilterdata : GXProcedure
   {
      public getwwservicogrupofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwservicogrupofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
         return AV24OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwservicogrupofilterdata objgetwwservicogrupofilterdata;
         objgetwwservicogrupofilterdata = new getwwservicogrupofilterdata();
         objgetwwservicogrupofilterdata.AV15DDOName = aP0_DDOName;
         objgetwwservicogrupofilterdata.AV13SearchTxt = aP1_SearchTxt;
         objgetwwservicogrupofilterdata.AV14SearchTxtTo = aP2_SearchTxtTo;
         objgetwwservicogrupofilterdata.AV19OptionsJson = "" ;
         objgetwwservicogrupofilterdata.AV22OptionsDescJson = "" ;
         objgetwwservicogrupofilterdata.AV24OptionIndexesJson = "" ;
         objgetwwservicogrupofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwservicogrupofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwservicogrupofilterdata);
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwservicogrupofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18Options = (IGxCollection)(new GxSimpleCollection());
         AV21OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV23OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV15DDOName), "DDO_SERVICOGRUPO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICOGRUPO_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV19OptionsJson = AV18Options.ToJSonString(false);
         AV22OptionsDescJson = AV21OptionsDesc.ToJSonString(false);
         AV24OptionIndexesJson = AV23OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV26Session.Get("WWServicoGrupoGridState"), "") == 0 )
         {
            AV28GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWServicoGrupoGridState"), "");
         }
         else
         {
            AV28GridState.FromXml(AV26Session.Get("WWServicoGrupoGridState"), "");
         }
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV28GridState.gxTpr_Filtervalues.Count )
         {
            AV29GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV28GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFSERVICOGRUPO_DESCRICAO") == 0 )
            {
               AV10TFServicoGrupo_Descricao = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFSERVICOGRUPO_DESCRICAO_SEL") == 0 )
            {
               AV11TFServicoGrupo_Descricao_Sel = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFSERVICOGRUPO_ATIVO_SEL") == 0 )
            {
               AV12TFServicoGrupo_Ativo_Sel = (short)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
         if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(1));
            AV31DynamicFiltersSelector1 = AV30GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV31DynamicFiltersSelector1, "SERVICOGRUPO_DESCRICAO") == 0 )
            {
               AV32ServicoGrupo_Descricao1 = AV30GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31DynamicFiltersSelector1, "SERVICOGRUPO_ATIVO") == 0 )
            {
               AV33ServicoGrupo_Ativo1 = BooleanUtil.Val( AV30GridStateDynamicFilter.gxTpr_Value);
            }
            if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV34DynamicFiltersEnabled2 = true;
               AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(2));
               AV35DynamicFiltersSelector2 = AV30GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "SERVICOGRUPO_DESCRICAO") == 0 )
               {
                  AV36ServicoGrupo_Descricao2 = AV30GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "SERVICOGRUPO_ATIVO") == 0 )
               {
                  AV37ServicoGrupo_Ativo2 = BooleanUtil.Val( AV30GridStateDynamicFilter.gxTpr_Value);
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSERVICOGRUPO_DESCRICAOOPTIONS' Routine */
         AV10TFServicoGrupo_Descricao = AV13SearchTxt;
         AV11TFServicoGrupo_Descricao_Sel = "";
         AV43WWServicoGrupoDS_1_Dynamicfiltersselector1 = AV31DynamicFiltersSelector1;
         AV44WWServicoGrupoDS_2_Servicogrupo_descricao1 = AV32ServicoGrupo_Descricao1;
         AV45WWServicoGrupoDS_3_Servicogrupo_ativo1 = AV33ServicoGrupo_Ativo1;
         AV46WWServicoGrupoDS_4_Dynamicfiltersenabled2 = AV34DynamicFiltersEnabled2;
         AV47WWServicoGrupoDS_5_Dynamicfiltersselector2 = AV35DynamicFiltersSelector2;
         AV48WWServicoGrupoDS_6_Servicogrupo_descricao2 = AV36ServicoGrupo_Descricao2;
         AV49WWServicoGrupoDS_7_Servicogrupo_ativo2 = AV37ServicoGrupo_Ativo2;
         AV50WWServicoGrupoDS_8_Tfservicogrupo_descricao = AV10TFServicoGrupo_Descricao;
         AV51WWServicoGrupoDS_9_Tfservicogrupo_descricao_sel = AV11TFServicoGrupo_Descricao_Sel;
         AV52WWServicoGrupoDS_10_Tfservicogrupo_ativo_sel = AV12TFServicoGrupo_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV43WWServicoGrupoDS_1_Dynamicfiltersselector1 ,
                                              AV44WWServicoGrupoDS_2_Servicogrupo_descricao1 ,
                                              AV46WWServicoGrupoDS_4_Dynamicfiltersenabled2 ,
                                              AV47WWServicoGrupoDS_5_Dynamicfiltersselector2 ,
                                              AV48WWServicoGrupoDS_6_Servicogrupo_descricao2 ,
                                              AV51WWServicoGrupoDS_9_Tfservicogrupo_descricao_sel ,
                                              AV50WWServicoGrupoDS_8_Tfservicogrupo_descricao ,
                                              AV52WWServicoGrupoDS_10_Tfservicogrupo_ativo_sel ,
                                              A158ServicoGrupo_Descricao ,
                                              A159ServicoGrupo_Ativo ,
                                              AV45WWServicoGrupoDS_3_Servicogrupo_ativo1 ,
                                              AV49WWServicoGrupoDS_7_Servicogrupo_ativo2 },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV44WWServicoGrupoDS_2_Servicogrupo_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV44WWServicoGrupoDS_2_Servicogrupo_descricao1), "%", "");
         lV48WWServicoGrupoDS_6_Servicogrupo_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV48WWServicoGrupoDS_6_Servicogrupo_descricao2), "%", "");
         lV50WWServicoGrupoDS_8_Tfservicogrupo_descricao = StringUtil.Concat( StringUtil.RTrim( AV50WWServicoGrupoDS_8_Tfservicogrupo_descricao), "%", "");
         /* Using cursor P00JH2 */
         pr_default.execute(0, new Object[] {lV44WWServicoGrupoDS_2_Servicogrupo_descricao1, AV45WWServicoGrupoDS_3_Servicogrupo_ativo1, lV48WWServicoGrupoDS_6_Servicogrupo_descricao2, AV49WWServicoGrupoDS_7_Servicogrupo_ativo2, lV50WWServicoGrupoDS_8_Tfservicogrupo_descricao, AV51WWServicoGrupoDS_9_Tfservicogrupo_descricao_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJH2 = false;
            A158ServicoGrupo_Descricao = P00JH2_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JH2_A159ServicoGrupo_Ativo[0];
            A157ServicoGrupo_Codigo = P00JH2_A157ServicoGrupo_Codigo[0];
            AV25count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00JH2_A158ServicoGrupo_Descricao[0], A158ServicoGrupo_Descricao) == 0 ) )
            {
               BRKJH2 = false;
               A157ServicoGrupo_Codigo = P00JH2_A157ServicoGrupo_Codigo[0];
               AV25count = (long)(AV25count+1);
               BRKJH2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A158ServicoGrupo_Descricao)) )
            {
               AV17Option = A158ServicoGrupo_Descricao;
               AV18Options.Add(AV17Option, 0);
               AV23OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV25count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV18Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJH2 )
            {
               BRKJH2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Options = new GxSimpleCollection();
         AV21OptionsDesc = new GxSimpleCollection();
         AV23OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV26Session = context.GetSession();
         AV28GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV29GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFServicoGrupo_Descricao = "";
         AV11TFServicoGrupo_Descricao_Sel = "";
         AV30GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV31DynamicFiltersSelector1 = "";
         AV32ServicoGrupo_Descricao1 = "";
         AV33ServicoGrupo_Ativo1 = true;
         AV35DynamicFiltersSelector2 = "";
         AV36ServicoGrupo_Descricao2 = "";
         AV37ServicoGrupo_Ativo2 = true;
         AV43WWServicoGrupoDS_1_Dynamicfiltersselector1 = "";
         AV44WWServicoGrupoDS_2_Servicogrupo_descricao1 = "";
         AV47WWServicoGrupoDS_5_Dynamicfiltersselector2 = "";
         AV48WWServicoGrupoDS_6_Servicogrupo_descricao2 = "";
         AV50WWServicoGrupoDS_8_Tfservicogrupo_descricao = "";
         AV51WWServicoGrupoDS_9_Tfservicogrupo_descricao_sel = "";
         scmdbuf = "";
         lV44WWServicoGrupoDS_2_Servicogrupo_descricao1 = "";
         lV48WWServicoGrupoDS_6_Servicogrupo_descricao2 = "";
         lV50WWServicoGrupoDS_8_Tfservicogrupo_descricao = "";
         A158ServicoGrupo_Descricao = "";
         P00JH2_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00JH2_A159ServicoGrupo_Ativo = new bool[] {false} ;
         P00JH2_A157ServicoGrupo_Codigo = new int[1] ;
         AV17Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwservicogrupofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JH2_A158ServicoGrupo_Descricao, P00JH2_A159ServicoGrupo_Ativo, P00JH2_A157ServicoGrupo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFServicoGrupo_Ativo_Sel ;
      private short AV52WWServicoGrupoDS_10_Tfservicogrupo_ativo_sel ;
      private int AV41GXV1 ;
      private int A157ServicoGrupo_Codigo ;
      private long AV25count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV33ServicoGrupo_Ativo1 ;
      private bool AV34DynamicFiltersEnabled2 ;
      private bool AV37ServicoGrupo_Ativo2 ;
      private bool AV45WWServicoGrupoDS_3_Servicogrupo_ativo1 ;
      private bool AV46WWServicoGrupoDS_4_Dynamicfiltersenabled2 ;
      private bool AV49WWServicoGrupoDS_7_Servicogrupo_ativo2 ;
      private bool A159ServicoGrupo_Ativo ;
      private bool BRKJH2 ;
      private String AV24OptionIndexesJson ;
      private String AV19OptionsJson ;
      private String AV22OptionsDescJson ;
      private String AV15DDOName ;
      private String AV13SearchTxt ;
      private String AV14SearchTxtTo ;
      private String AV10TFServicoGrupo_Descricao ;
      private String AV11TFServicoGrupo_Descricao_Sel ;
      private String AV31DynamicFiltersSelector1 ;
      private String AV32ServicoGrupo_Descricao1 ;
      private String AV35DynamicFiltersSelector2 ;
      private String AV36ServicoGrupo_Descricao2 ;
      private String AV43WWServicoGrupoDS_1_Dynamicfiltersselector1 ;
      private String AV44WWServicoGrupoDS_2_Servicogrupo_descricao1 ;
      private String AV47WWServicoGrupoDS_5_Dynamicfiltersselector2 ;
      private String AV48WWServicoGrupoDS_6_Servicogrupo_descricao2 ;
      private String AV50WWServicoGrupoDS_8_Tfservicogrupo_descricao ;
      private String AV51WWServicoGrupoDS_9_Tfservicogrupo_descricao_sel ;
      private String lV44WWServicoGrupoDS_2_Servicogrupo_descricao1 ;
      private String lV48WWServicoGrupoDS_6_Servicogrupo_descricao2 ;
      private String lV50WWServicoGrupoDS_8_Tfservicogrupo_descricao ;
      private String A158ServicoGrupo_Descricao ;
      private String AV17Option ;
      private IGxSession AV26Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00JH2_A158ServicoGrupo_Descricao ;
      private bool[] P00JH2_A159ServicoGrupo_Ativo ;
      private int[] P00JH2_A157ServicoGrupo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV18Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV28GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV29GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV30GridStateDynamicFilter ;
   }

   public class getwwservicogrupofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JH2( IGxContext context ,
                                             String AV43WWServicoGrupoDS_1_Dynamicfiltersselector1 ,
                                             String AV44WWServicoGrupoDS_2_Servicogrupo_descricao1 ,
                                             bool AV46WWServicoGrupoDS_4_Dynamicfiltersenabled2 ,
                                             String AV47WWServicoGrupoDS_5_Dynamicfiltersselector2 ,
                                             String AV48WWServicoGrupoDS_6_Servicogrupo_descricao2 ,
                                             String AV51WWServicoGrupoDS_9_Tfservicogrupo_descricao_sel ,
                                             String AV50WWServicoGrupoDS_8_Tfservicogrupo_descricao ,
                                             short AV52WWServicoGrupoDS_10_Tfservicogrupo_ativo_sel ,
                                             String A158ServicoGrupo_Descricao ,
                                             bool A159ServicoGrupo_Ativo ,
                                             bool AV45WWServicoGrupoDS_3_Servicogrupo_ativo1 ,
                                             bool AV49WWServicoGrupoDS_7_Servicogrupo_ativo2 )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ServicoGrupo_Descricao], [ServicoGrupo_Ativo], [ServicoGrupo_Codigo] FROM [ServicoGrupo] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV43WWServicoGrupoDS_1_Dynamicfiltersselector1, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44WWServicoGrupoDS_2_Servicogrupo_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV44WWServicoGrupoDS_2_Servicogrupo_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoGrupo_Descricao] like '%' + @lV44WWServicoGrupoDS_2_Servicogrupo_descricao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( StringUtil.StrCmp(AV43WWServicoGrupoDS_1_Dynamicfiltersselector1, "SERVICOGRUPO_ATIVO") == 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoGrupo_Ativo] = @AV45WWServicoGrupoDS_3_Servicogrupo_ativo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoGrupo_Ativo] = @AV45WWServicoGrupoDS_3_Servicogrupo_ativo1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV46WWServicoGrupoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV47WWServicoGrupoDS_5_Dynamicfiltersselector2, "SERVICOGRUPO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWServicoGrupoDS_6_Servicogrupo_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like '%' + @lV48WWServicoGrupoDS_6_Servicogrupo_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoGrupo_Descricao] like '%' + @lV48WWServicoGrupoDS_6_Servicogrupo_descricao2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV46WWServicoGrupoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV47WWServicoGrupoDS_5_Dynamicfiltersselector2, "SERVICOGRUPO_ATIVO") == 0 ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoGrupo_Ativo] = @AV49WWServicoGrupoDS_7_Servicogrupo_ativo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoGrupo_Ativo] = @AV49WWServicoGrupoDS_7_Servicogrupo_ativo2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WWServicoGrupoDS_9_Tfservicogrupo_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWServicoGrupoDS_8_Tfservicogrupo_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] like @lV50WWServicoGrupoDS_8_Tfservicogrupo_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoGrupo_Descricao] like @lV50WWServicoGrupoDS_8_Tfservicogrupo_descricao)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWServicoGrupoDS_9_Tfservicogrupo_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoGrupo_Descricao] = @AV51WWServicoGrupoDS_9_Tfservicogrupo_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoGrupo_Descricao] = @AV51WWServicoGrupoDS_9_Tfservicogrupo_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV52WWServicoGrupoDS_10_Tfservicogrupo_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoGrupo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoGrupo_Ativo] = 1)";
            }
         }
         if ( AV52WWServicoGrupoDS_10_Tfservicogrupo_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ServicoGrupo_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([ServicoGrupo_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [ServicoGrupo_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JH2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (bool)dynConstraints[10] , (bool)dynConstraints[11] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JH2 ;
          prmP00JH2 = new Object[] {
          new Object[] {"@lV44WWServicoGrupoDS_2_Servicogrupo_descricao1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV45WWServicoGrupoDS_3_Servicogrupo_ativo1",SqlDbType.Bit,4,0} ,
          new Object[] {"@lV48WWServicoGrupoDS_6_Servicogrupo_descricao2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV49WWServicoGrupoDS_7_Servicogrupo_ativo2",SqlDbType.Bit,4,0} ,
          new Object[] {"@lV50WWServicoGrupoDS_8_Tfservicogrupo_descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV51WWServicoGrupoDS_9_Tfservicogrupo_descricao_sel",SqlDbType.VarChar,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JH2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JH2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwservicogrupofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwservicogrupofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwservicogrupofilterdata") )
          {
             return  ;
          }
          getwwservicogrupofilterdata worker = new getwwservicogrupofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
