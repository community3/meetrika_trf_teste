/*
               File: WP_AssociationContratadaUsuario
        Description: Associar Usu�rios da Contratada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:32:58.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associationcontratadausuario : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associationcontratadausuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associationcontratadausuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratadaUsuario_ContratadaCod )
      {
         this.AV7ContratadaUsuario_ContratadaCod = aP0_ContratadaUsuario_ContratadaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         lstavNotassociatedrecords = new GXListbox();
         lstavAssociatedrecords = new GXListbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratadaUsuario_ContratadaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADAUSUARIO_CONTRATADACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratadaUsuario_ContratadaCod), "ZZZZZ9")));
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA3D2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavContratada_pessoanom_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom_Enabled), 5, 0)));
               WS3D2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE3D2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Associar Usu�rios da Contratada") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299325847");
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associationcontratadausuario.aspx") + "?" + UrlEncode("" +AV7ContratadaUsuario_ContratadaCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm3D2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
      }

      public override String GetPgmname( )
      {
         return "WP_AssociationContratadaUsuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Associar Usu�rios da Contratada" ;
      }

      protected void WB3D0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_3D2( true) ;
         }
         else
         {
            wb_table1_2_3D2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_3D2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddedkeylistxml_Internalname, AV16AddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, edtavAddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociationContratadaUsuario.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddedkeylistxml_Internalname, AV17NotAddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", 0, edtavNotaddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociationContratadaUsuario.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddeddsclistxml_Internalname, AV18AddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavAddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociationContratadaUsuario.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddeddsclistxml_Internalname, AV19NotAddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", 0, edtavNotaddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociationContratadaUsuario.htm");
         }
         wbLoad = true;
      }

      protected void START3D2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Associar Usu�rios da Contratada", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP3D0( ) ;
      }

      protected void WS3D2( )
      {
         START3D2( ) ;
         EVT3D2( ) ;
      }

      protected void EVT3D2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113D2 */
                           E113D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123D2 */
                           E123D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E133D2 */
                                 E133D2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E143D2 */
                           E143D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E153D2 */
                           E153D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E163D2 */
                           E163D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E173D2 */
                           E173D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E183D2 */
                           E183D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E193D2 */
                           E193D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E203D2 */
                           E203D2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E183D2 */
                           E183D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E193D2 */
                           E193D2 ();
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE3D2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm3D2( ) ;
            }
         }
      }

      protected void PA3D2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            lstavNotassociatedrecords.Name = "vNOTASSOCIATEDRECORDS";
            lstavNotassociatedrecords.WebTags = "";
            if ( lstavNotassociatedrecords.ItemCount > 0 )
            {
               AV24NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
            }
            lstavAssociatedrecords.Name = "vASSOCIATEDRECORDS";
            lstavAssociatedrecords.WebTags = "";
            if ( lstavAssociatedrecords.ItemCount > 0 )
            {
               AV25AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)));
            }
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = lstavNotassociatedrecords_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( lstavNotassociatedrecords.ItemCount > 0 )
         {
            AV24NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
         }
         if ( lstavAssociatedrecords.ItemCount > 0 )
         {
            AV25AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF3D2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF3D2( )
      {
         /* Execute user event: E123D2 */
         E123D2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E203D2 */
            E203D2 ();
            WB3D0( ) ;
         }
      }

      protected void STRUP3D0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavContratada_pessoanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113D2 */
         E113D2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV27Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtavContratada_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contratada_PessoaNom", AV27Contratada_PessoaNom);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Contratada_PessoaNom, "@!"))));
            lstavNotassociatedrecords.CurrentValue = cgiGet( lstavNotassociatedrecords_Internalname);
            AV24NotAssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavNotassociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
            lstavAssociatedrecords.CurrentValue = cgiGet( lstavAssociatedrecords_Internalname);
            AV25AssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavAssociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)));
            AV16AddedKeyListXml = cgiGet( edtavAddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AddedKeyListXml", AV16AddedKeyListXml);
            AV17NotAddedKeyListXml = cgiGet( edtavNotaddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotAddedKeyListXml", AV17NotAddedKeyListXml);
            AV18AddedDscListXml = cgiGet( edtavAddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AddedDscListXml", AV18AddedDscListXml);
            AV19NotAddedDscListXml = cgiGet( edtavNotaddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NotAddedDscListXml", AV19NotAddedDscListXml);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E113D2 */
         E113D2 ();
         if (returnInSub) return;
      }

      protected void E113D2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( StringUtil.StrCmp(AV9HTTPRequest.Method, "GET") == 0 )
         {
            AV30GXLvl8 = 0;
            /* Using cursor H003D2 */
            pr_default.execute(0, new Object[] {AV7ContratadaUsuario_ContratadaCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A40Contratada_PessoaCod = H003D2_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = H003D2_A39Contratada_Codigo[0];
               A41Contratada_PessoaNom = H003D2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H003D2_n41Contratada_PessoaNom[0];
               A41Contratada_PessoaNom = H003D2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H003D2_n41Contratada_PessoaNom[0];
               AV30GXLvl8 = 1;
               AV27Contratada_PessoaNom = A41Contratada_PessoaNom;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contratada_PessoaNom", AV27Contratada_PessoaNom);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Contratada_PessoaNom, "@!"))));
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV30GXLvl8 == 0 )
            {
               AV27Contratada_PessoaNom = StringUtil.Str( (decimal)(AV7ContratadaUsuario_ContratadaCod), 6, 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Contratada_PessoaNom", AV27Contratada_PessoaNom);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Contratada_PessoaNom, "@!"))));
               GX_msglist.addItem("Registro n�o encontrado.");
            }
            /* Using cursor H003D3 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               A57Usuario_PessoaCod = H003D3_A57Usuario_PessoaCod[0];
               A54Usuario_Ativo = H003D3_A54Usuario_Ativo[0];
               A58Usuario_PessoaNom = H003D3_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H003D3_n58Usuario_PessoaNom[0];
               A1Usuario_Codigo = H003D3_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = H003D3_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H003D3_n58Usuario_PessoaNom[0];
               AV8ContratadaUsuario_UsuarioCod = A1Usuario_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0)));
               AV10Exist = false;
               /* Using cursor H003D4 */
               pr_default.execute(2, new Object[] {AV7ContratadaUsuario_ContratadaCod, AV8ContratadaUsuario_UsuarioCod});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A69ContratadaUsuario_UsuarioCod = H003D4_A69ContratadaUsuario_UsuarioCod[0];
                  A66ContratadaUsuario_ContratadaCod = H003D4_A66ContratadaUsuario_ContratadaCod[0];
                  AV10Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               AV13Description = A58Usuario_PessoaNom;
               if ( AV10Exist )
               {
                  AV20AddedKeyList.Add(A1Usuario_Codigo, 0);
                  AV22AddedDscList.Add(AV13Description, 0);
               }
               else
               {
                  AV21NotAddedKeyList.Add(A1Usuario_Codigo, 0);
                  AV23NotAddedDscList.Add(AV13Description, 0);
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if (returnInSub) return;
         }
         edtavAddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddedkeylistxml_Visible), 5, 0)));
         edtavNotaddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddedkeylistxml_Visible), 5, 0)));
         edtavAddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddeddsclistxml_Visible), 5, 0)));
         edtavNotaddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddeddsclistxml_Visible), 5, 0)));
      }

      protected void E123D2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         imgImageassociateselected_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         bttBtn_confirm_Visible = (AV6WWPContext.gxTpr_Insert||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if (returnInSub) return;
         lstavAssociatedrecords.removeAllItems();
         lstavNotassociatedrecords.removeAllItems();
         AV14i = 1;
         AV33GXV1 = 1;
         while ( AV33GXV1 <= AV20AddedKeyList.Count )
         {
            AV8ContratadaUsuario_UsuarioCod = (int)(AV20AddedKeyList.GetNumeric(AV33GXV1));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0)));
            AV13Description = ((String)AV22AddedDscList.Item(AV14i));
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0)), StringUtil.Trim( AV13Description), 0);
            AV14i = (int)(AV14i+1);
            AV33GXV1 = (int)(AV33GXV1+1);
         }
         AV14i = 1;
         AV34GXV2 = 1;
         while ( AV34GXV2 <= AV21NotAddedKeyList.Count )
         {
            AV8ContratadaUsuario_UsuarioCod = (int)(AV21NotAddedKeyList.GetNumeric(AV34GXV2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0)));
            AV13Description = ((String)AV23NotAddedDscList.Item(AV14i));
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0)), StringUtil.Trim( AV13Description), 0);
            AV14i = (int)(AV14i+1);
            AV34GXV2 = (int)(AV34GXV2+1);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E133D2 */
         E133D2 ();
         if (returnInSub) return;
      }

      protected void E133D2( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if (returnInSub) return;
         AV14i = 1;
         AV12Success = true;
         AV35GXV3 = 1;
         while ( AV35GXV3 <= AV20AddedKeyList.Count )
         {
            AV8ContratadaUsuario_UsuarioCod = (int)(AV20AddedKeyList.GetNumeric(AV35GXV3));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0)));
            if ( AV12Success )
            {
               AV10Exist = false;
               /* Using cursor H003D5 */
               pr_default.execute(3, new Object[] {AV7ContratadaUsuario_ContratadaCod, AV8ContratadaUsuario_UsuarioCod});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A69ContratadaUsuario_UsuarioCod = H003D5_A69ContratadaUsuario_UsuarioCod[0];
                  A66ContratadaUsuario_ContratadaCod = H003D5_A66ContratadaUsuario_ContratadaCod[0];
                  AV10Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
               if ( ! AV10Exist )
               {
                  AV11ContratadaUsuario = new SdtContratadaUsuario(context);
                  AV11ContratadaUsuario.gxTpr_Contratadausuario_contratadacod = AV7ContratadaUsuario_ContratadaCod;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11ContratadaUsuario", AV11ContratadaUsuario);
                  AV11ContratadaUsuario.gxTpr_Contratadausuario_usuariocod = AV8ContratadaUsuario_UsuarioCod;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11ContratadaUsuario", AV11ContratadaUsuario);
                  AV11ContratadaUsuario.Save();
                  if ( ! AV11ContratadaUsuario.Success() )
                  {
                     AV12Success = false;
                  }
               }
            }
            AV14i = (int)(AV14i+1);
            AV35GXV3 = (int)(AV35GXV3+1);
         }
         AV14i = 1;
         AV37GXV4 = 1;
         while ( AV37GXV4 <= AV21NotAddedKeyList.Count )
         {
            AV8ContratadaUsuario_UsuarioCod = (int)(AV21NotAddedKeyList.GetNumeric(AV37GXV4));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0)));
            if ( AV12Success )
            {
               AV10Exist = false;
               /* Using cursor H003D6 */
               pr_default.execute(4, new Object[] {AV7ContratadaUsuario_ContratadaCod, AV8ContratadaUsuario_UsuarioCod});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A69ContratadaUsuario_UsuarioCod = H003D6_A69ContratadaUsuario_UsuarioCod[0];
                  A66ContratadaUsuario_ContratadaCod = H003D6_A66ContratadaUsuario_ContratadaCod[0];
                  AV10Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
               if ( AV10Exist )
               {
                  AV11ContratadaUsuario = new SdtContratadaUsuario(context);
                  AV11ContratadaUsuario.Load(AV7ContratadaUsuario_ContratadaCod, AV8ContratadaUsuario_UsuarioCod);
                  if ( AV11ContratadaUsuario.Success() )
                  {
                     AV11ContratadaUsuario.Delete();
                  }
                  if ( ! AV11ContratadaUsuario.Success() )
                  {
                     AV12Success = false;
                  }
               }
            }
            AV14i = (int)(AV14i+1);
            AV37GXV4 = (int)(AV37GXV4+1);
         }
         if ( AV12Success )
         {
            context.CommitDataStores( "WP_AssociationContratadaUsuario");
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S132 ();
            if (returnInSub) return;
         }
      }

      protected void E143D2( )
      {
         /* 'Disassociate Selected' Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S142 ();
         if (returnInSub) return;
      }

      protected void E153D2( )
      {
         /* 'Associate selected' Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S152 ();
         if (returnInSub) return;
      }

      protected void E163D2( )
      {
         /* 'Associate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S162 ();
         if (returnInSub) return;
      }

      protected void E173D2( )
      {
         /* 'Disassociate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S162 ();
         if (returnInSub) return;
         AV21NotAddedKeyList = (IGxCollection)(AV20AddedKeyList.Clone());
         AV23NotAddedDscList = (IGxCollection)(AV22AddedDscList.Clone());
         AV22AddedDscList.Clear();
         AV20AddedKeyList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if (returnInSub) return;
      }

      protected void E183D2( )
      {
         /* Associatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S142 ();
         if (returnInSub) return;
      }

      protected void E193D2( )
      {
         /* Notassociatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S152 ();
         if (returnInSub) return;
      }

      protected void S132( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV40GXV6 = 1;
         AV39GXV5 = AV11ContratadaUsuario.GetMessages();
         while ( AV40GXV6 <= AV39GXV5.Count )
         {
            AV15Message = ((SdtMessages_Message)AV39GXV5.Item(AV40GXV6));
            if ( AV15Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV15Message.gxTpr_Description);
            }
            AV40GXV6 = (int)(AV40GXV6+1);
         }
      }

      protected void S122( )
      {
         /* 'LOADLISTS' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16AddedKeyListXml)) )
         {
            AV22AddedDscList.FromXml(AV18AddedDscListXml, "Collection");
            AV20AddedKeyList.FromXml(AV16AddedKeyListXml, "Collection");
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17NotAddedKeyListXml)) )
         {
            AV21NotAddedKeyList.FromXml(AV17NotAddedKeyListXml, "Collection");
            AV23NotAddedDscList.FromXml(AV19NotAddedDscListXml, "Collection");
         }
      }

      protected void S112( )
      {
         /* 'SAVELISTS' Routine */
         if ( AV20AddedKeyList.Count > 0 )
         {
            AV16AddedKeyListXml = AV20AddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AddedKeyListXml", AV16AddedKeyListXml);
            AV18AddedDscListXml = AV22AddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AddedDscListXml", AV18AddedDscListXml);
         }
         else
         {
            AV16AddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AddedKeyListXml", AV16AddedKeyListXml);
            AV18AddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AddedDscListXml", AV18AddedDscListXml);
         }
         if ( AV21NotAddedKeyList.Count > 0 )
         {
            AV17NotAddedKeyListXml = AV21NotAddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotAddedKeyListXml", AV17NotAddedKeyListXml);
            AV19NotAddedDscListXml = AV23NotAddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NotAddedDscListXml", AV19NotAddedDscListXml);
         }
         else
         {
            AV17NotAddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotAddedKeyListXml", AV17NotAddedKeyListXml);
            AV19NotAddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NotAddedDscListXml", AV19NotAddedDscListXml);
         }
      }

      protected void S162( )
      {
         /* 'ASSOCIATEALL' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if (returnInSub) return;
         AV14i = 1;
         AV26InsertIndex = 1;
         AV41GXV7 = 1;
         while ( AV41GXV7 <= AV21NotAddedKeyList.Count )
         {
            AV8ContratadaUsuario_UsuarioCod = (int)(AV21NotAddedKeyList.GetNumeric(AV41GXV7));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0)));
            AV13Description = ((String)AV23NotAddedDscList.Item(AV14i));
            while ( ( AV26InsertIndex <= AV22AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV22AddedDscList.Item(AV26InsertIndex)), AV13Description) < 0 ) )
            {
               AV26InsertIndex = (int)(AV26InsertIndex+1);
            }
            AV20AddedKeyList.Add(AV8ContratadaUsuario_UsuarioCod, AV26InsertIndex);
            AV22AddedDscList.Add(AV13Description, AV26InsertIndex);
            AV14i = (int)(AV14i+1);
            AV41GXV7 = (int)(AV41GXV7+1);
         }
         AV21NotAddedKeyList.Clear();
         AV23NotAddedDscList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if (returnInSub) return;
      }

      protected void S152( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if (returnInSub) return;
         AV14i = 1;
         AV42GXV8 = 1;
         while ( AV42GXV8 <= AV21NotAddedKeyList.Count )
         {
            AV8ContratadaUsuario_UsuarioCod = (int)(AV21NotAddedKeyList.GetNumeric(AV42GXV8));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0)));
            if ( AV8ContratadaUsuario_UsuarioCod == AV24NotAssociatedRecords )
            {
               if (true) break;
            }
            AV14i = (int)(AV14i+1);
            AV42GXV8 = (int)(AV42GXV8+1);
         }
         if ( AV14i <= AV21NotAddedKeyList.Count )
         {
            AV13Description = ((String)AV23NotAddedDscList.Item(AV14i));
            AV26InsertIndex = 1;
            while ( ( AV26InsertIndex <= AV22AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV22AddedDscList.Item(AV26InsertIndex)), AV13Description) < 0 ) )
            {
               AV26InsertIndex = (int)(AV26InsertIndex+1);
            }
            AV20AddedKeyList.Add(AV24NotAssociatedRecords, AV26InsertIndex);
            AV22AddedDscList.Add(AV13Description, AV26InsertIndex);
            AV21NotAddedKeyList.RemoveItem(AV14i);
            AV23NotAddedDscList.RemoveItem(AV14i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void S142( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if (returnInSub) return;
         AV14i = 1;
         AV43GXV9 = 1;
         while ( AV43GXV9 <= AV20AddedKeyList.Count )
         {
            AV8ContratadaUsuario_UsuarioCod = (int)(AV20AddedKeyList.GetNumeric(AV43GXV9));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratadaUsuario_UsuarioCod), 6, 0)));
            if ( AV8ContratadaUsuario_UsuarioCod == AV25AssociatedRecords )
            {
               if (true) break;
            }
            AV14i = (int)(AV14i+1);
            AV43GXV9 = (int)(AV43GXV9+1);
         }
         if ( AV14i <= AV20AddedKeyList.Count )
         {
            AV13Description = ((String)AV22AddedDscList.Item(AV14i));
            AV26InsertIndex = 1;
            while ( ( AV26InsertIndex <= AV23NotAddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV23NotAddedDscList.Item(AV26InsertIndex)), AV13Description) < 0 ) )
            {
               AV26InsertIndex = (int)(AV26InsertIndex+1);
            }
            AV21NotAddedKeyList.Add(AV25AssociatedRecords, AV26InsertIndex);
            AV23NotAddedDscList.Add(AV13Description, AV26InsertIndex);
            AV20AddedKeyList.RemoveItem(AV14i);
            AV22AddedDscList.RemoveItem(AV14i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E203D2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_3D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_3D2( true) ;
         }
         else
         {
            wb_table2_8_3D2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_3D2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table3_16_3D2( true) ;
         }
         else
         {
            wb_table3_16_3D2( false) ;
         }
         return  ;
      }

      protected void wb_table3_16_3D2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table4_48_3D2( true) ;
         }
         else
         {
            wb_table4_48_3D2( false) ;
         }
         return  ;
      }

      protected void wb_table4_48_3D2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3D2e( true) ;
         }
         else
         {
            wb_table1_2_3D2e( false) ;
         }
      }

      protected void wb_table4_48_3D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociationContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociationContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_48_3D2e( true) ;
         }
         else
         {
            wb_table4_48_3D2e( false) ;
         }
      }

      protected void wb_table3_16_3D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_19_3D2( true) ;
         }
         else
         {
            wb_table5_19_3D2( false) ;
         }
         return  ;
      }

      protected void wb_table5_19_3D2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_16_3D2e( true) ;
         }
         else
         {
            wb_table3_16_3D2e( false) ;
         }
      }

      protected void wb_table5_19_3D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, "Usu�rios N�o Associados", "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociationContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociationContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, "Usu�rios Associados", "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociationContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavNotassociatedrecords, lstavNotassociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)), 2, lstavNotassociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_WP_AssociationContratadaUsuario.htm");
            lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", (String)(lstavNotassociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='Table'>") ;
            wb_table6_31_3D2( true) ;
         }
         else
         {
            wb_table6_31_3D2( false) ;
         }
         return  ;
      }

      protected void wb_table6_31_3D2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavAssociatedrecords, lstavAssociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)), 2, lstavAssociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WP_AssociationContratadaUsuario.htm");
            lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", (String)(lstavAssociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_3D2e( true) ;
         }
         else
         {
            wb_table5_19_3D2e( false) ;
         }
      }

      protected void wb_table6_31_3D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_31_3D2e( true) ;
         }
         else
         {
            wb_table6_31_3D2e( false) ;
         }
      }

      protected void wb_table2_8_3D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Contratada :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociationContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom_Internalname, StringUtil.RTrim( AV27Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV27Contratada_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, edtavContratada_pessoanom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_AssociationContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_3D2e( true) ;
         }
         else
         {
            wb_table2_8_3D2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContratadaUsuario_ContratadaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratadaUsuario_ContratadaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADAUSUARIO_CONTRATADACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratadaUsuario_ContratadaCod), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA3D2( ) ;
         WS3D2( ) ;
         WE3D2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299325936");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associationcontratadausuario.js", "?20205299325936");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtavContratada_pessoanom_Internalname = "vCONTRATADA_PESSOANOM";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         lstavNotassociatedrecords_Internalname = "vNOTASSOCIATEDRECORDS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblTable1_Internalname = "TABLE1";
         lstavAssociatedrecords_Internalname = "vASSOCIATEDRECORDS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavAddedkeylistxml_Internalname = "vADDEDKEYLISTXML";
         edtavNotaddedkeylistxml_Internalname = "vNOTADDEDKEYLISTXML";
         edtavAddeddsclistxml_Internalname = "vADDEDDSCLISTXML";
         edtavNotaddeddsclistxml_Internalname = "vNOTADDEDDSCLISTXML";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         edtavContratada_pessoanom_Jsonclick = "";
         edtavContratada_pessoanom_Enabled = 0;
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         lstavAssociatedrecords_Jsonclick = "";
         lstavNotassociatedrecords_Jsonclick = "";
         bttBtn_confirm_Visible = 1;
         edtavNotaddeddsclistxml_Visible = 1;
         edtavAddeddsclistxml_Visible = 1;
         edtavNotaddedkeylistxml_Visible = 1;
         edtavAddedkeylistxml_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV16AddedKeyListXml = "";
         AV17NotAddedKeyListXml = "";
         AV18AddedDscListXml = "";
         AV19NotAddedDscListXml = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV27Contratada_PessoaNom = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9HTTPRequest = new GxHttpRequest( context);
         scmdbuf = "";
         H003D2_A40Contratada_PessoaCod = new int[1] ;
         H003D2_A39Contratada_Codigo = new int[1] ;
         H003D2_A41Contratada_PessoaNom = new String[] {""} ;
         H003D2_n41Contratada_PessoaNom = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         H003D3_A57Usuario_PessoaCod = new int[1] ;
         H003D3_A54Usuario_Ativo = new bool[] {false} ;
         H003D3_A58Usuario_PessoaNom = new String[] {""} ;
         H003D3_n58Usuario_PessoaNom = new bool[] {false} ;
         H003D3_A1Usuario_Codigo = new int[1] ;
         A58Usuario_PessoaNom = "";
         H003D4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H003D4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         AV13Description = "";
         AV20AddedKeyList = new GxSimpleCollection();
         AV22AddedDscList = new GxSimpleCollection();
         AV21NotAddedKeyList = new GxSimpleCollection();
         AV23NotAddedDscList = new GxSimpleCollection();
         H003D5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H003D5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         AV11ContratadaUsuario = new SdtContratadaUsuario(context);
         H003D6_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H003D6_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         AV39GXV5 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV15Message = new SdtMessages_Message(context);
         sStyleString = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associationcontratadausuario__default(),
            new Object[][] {
                new Object[] {
               H003D2_A40Contratada_PessoaCod, H003D2_A39Contratada_Codigo, H003D2_A41Contratada_PessoaNom, H003D2_n41Contratada_PessoaNom
               }
               , new Object[] {
               H003D3_A57Usuario_PessoaCod, H003D3_A54Usuario_Ativo, H003D3_A58Usuario_PessoaNom, H003D3_n58Usuario_PessoaNom, H003D3_A1Usuario_Codigo
               }
               , new Object[] {
               H003D4_A69ContratadaUsuario_UsuarioCod, H003D4_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H003D5_A69ContratadaUsuario_UsuarioCod, H003D5_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H003D6_A69ContratadaUsuario_UsuarioCod, H003D6_A66ContratadaUsuario_ContratadaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV30GXLvl8 ;
      private short nGXWrapped ;
      private int AV7ContratadaUsuario_ContratadaCod ;
      private int wcpOAV7ContratadaUsuario_ContratadaCod ;
      private int edtavContratada_pessoanom_Enabled ;
      private int edtavAddedkeylistxml_Visible ;
      private int edtavNotaddedkeylistxml_Visible ;
      private int edtavAddeddsclistxml_Visible ;
      private int edtavNotaddeddsclistxml_Visible ;
      private int AV24NotAssociatedRecords ;
      private int AV25AssociatedRecords ;
      private int A40Contratada_PessoaCod ;
      private int A39Contratada_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int AV8ContratadaUsuario_UsuarioCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int bttBtn_confirm_Visible ;
      private int AV14i ;
      private int AV33GXV1 ;
      private int AV34GXV2 ;
      private int AV35GXV3 ;
      private int AV37GXV4 ;
      private int AV40GXV6 ;
      private int AV26InsertIndex ;
      private int AV41GXV7 ;
      private int AV42GXV8 ;
      private int AV43GXV9 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String edtavContratada_pessoanom_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String edtavAddedkeylistxml_Internalname ;
      private String edtavNotaddedkeylistxml_Internalname ;
      private String edtavAddeddsclistxml_Internalname ;
      private String edtavNotaddeddsclistxml_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String lstavNotassociatedrecords_Internalname ;
      private String AV27Contratada_PessoaNom ;
      private String lstavAssociatedrecords_Internalname ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private String A58Usuario_PessoaNom ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String lstavNotassociatedrecords_Jsonclick ;
      private String lstavAssociatedrecords_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private String edtavContratada_pessoanom_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n41Contratada_PessoaNom ;
      private bool A54Usuario_Ativo ;
      private bool n58Usuario_PessoaNom ;
      private bool AV10Exist ;
      private bool AV12Success ;
      private String AV16AddedKeyListXml ;
      private String AV17NotAddedKeyListXml ;
      private String AV18AddedDscListXml ;
      private String AV19NotAddedDscListXml ;
      private String AV13Description ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXListbox lstavNotassociatedrecords ;
      private GXListbox lstavAssociatedrecords ;
      private IDataStoreProvider pr_default ;
      private int[] H003D2_A40Contratada_PessoaCod ;
      private int[] H003D2_A39Contratada_Codigo ;
      private String[] H003D2_A41Contratada_PessoaNom ;
      private bool[] H003D2_n41Contratada_PessoaNom ;
      private int[] H003D3_A57Usuario_PessoaCod ;
      private bool[] H003D3_A54Usuario_Ativo ;
      private String[] H003D3_A58Usuario_PessoaNom ;
      private bool[] H003D3_n58Usuario_PessoaNom ;
      private int[] H003D3_A1Usuario_Codigo ;
      private int[] H003D4_A69ContratadaUsuario_UsuarioCod ;
      private int[] H003D4_A66ContratadaUsuario_ContratadaCod ;
      private int[] H003D5_A69ContratadaUsuario_UsuarioCod ;
      private int[] H003D5_A66ContratadaUsuario_ContratadaCod ;
      private int[] H003D6_A69ContratadaUsuario_UsuarioCod ;
      private int[] H003D6_A66ContratadaUsuario_ContratadaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV9HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV20AddedKeyList ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV21NotAddedKeyList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22AddedDscList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23NotAddedDscList ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV39GXV5 ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private SdtContratadaUsuario AV11ContratadaUsuario ;
      private SdtMessages_Message AV15Message ;
   }

   public class wp_associationcontratadausuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH003D2 ;
          prmH003D2 = new Object[] {
          new Object[] {"@AV7ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003D3 ;
          prmH003D3 = new Object[] {
          } ;
          Object[] prmH003D4 ;
          prmH003D4 = new Object[] {
          new Object[] {"@AV7ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003D5 ;
          prmH003D5 = new Object[] {
          new Object[] {"@AV7ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003D6 ;
          prmH003D6 = new Object[] {
          new Object[] {"@AV7ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H003D2", "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_Codigo] = @AV7ContratadaUsuario_ContratadaCod ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003D2,1,0,false,true )
             ,new CursorDef("H003D3", "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Ativo], T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Codigo] FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Ativo] = 1 ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003D3,100,0,true,false )
             ,new CursorDef("H003D4", "SELECT [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV7ContratadaUsuario_ContratadaCod and [ContratadaUsuario_UsuarioCod] = @AV8ContratadaUsuario_UsuarioCod ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003D4,1,0,false,true )
             ,new CursorDef("H003D5", "SELECT [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV7ContratadaUsuario_ContratadaCod and [ContratadaUsuario_UsuarioCod] = @AV8ContratadaUsuario_UsuarioCod ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003D5,1,0,false,true )
             ,new CursorDef("H003D6", "SELECT [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV7ContratadaUsuario_ContratadaCod and [ContratadaUsuario_UsuarioCod] = @AV8ContratadaUsuario_UsuarioCod ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003D6,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
