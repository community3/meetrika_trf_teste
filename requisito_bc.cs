/*
               File: Requisito_BC
        Description: Requisito
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:32:17.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class requisito_bc : GXHttpHandler, IGxSilentTrn
   {
      public requisito_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public requisito_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4U215( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4U215( ) ;
         standaloneModal( ) ;
         AddRow4U215( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E114U2 */
            E114U2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1919Requisito_Codigo = A1919Requisito_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4U0( )
      {
         BeforeValidate4U215( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4U215( ) ;
            }
            else
            {
               CheckExtendedTable4U215( ) ;
               if ( AnyError == 0 )
               {
                  ZM4U215( 10) ;
                  ZM4U215( 11) ;
                  ZM4U215( 12) ;
               }
               CloseExtendedTableCursors4U215( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E124U2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV25Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV26GXV1 = 1;
            while ( AV26GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV26GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Proposta_Codigo") == 0 )
               {
                  AV15Insert_Proposta_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Requisito_TipoReqCod") == 0 )
               {
                  AV24Insert_Requisito_TipoReqCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Requisito_ReqCod") == 0 )
               {
                  AV22Insert_Requisito_ReqCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV26GXV1 = (int)(AV26GXV1+1);
            }
         }
      }

      protected void E114U2( )
      {
         /* After Trn Routine */
      }

      protected void E134U2( )
      {
         /* 'CalculaPontucao' Routine */
         AV20Window.Url = formatLink("RequisitoPontuacaoWP") + "?" + UrlEncode("" +A1919Requisito_Codigo) + "," + UrlEncode("" +0) + "," + UrlEncode(StringUtil.BoolToStr(true)) + "," + UrlEncode("" +A1685Proposta_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(true)) + "," + UrlEncode(StringUtil.Str(A1932Requisito_Pontuacao,14,5));
         AV20Window.SetReturnParms(new Object[] {});
         AV20Window.Autoresize = 0;
         AV20Window.Height = 220;
         AV20Window.Width = 620;
         context.NewWindow(AV20Window);
      }

      protected void ZM4U215( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            Z1935Requisito_Ativo = A1935Requisito_Ativo;
            Z2001Requisito_Identificador = A2001Requisito_Identificador;
            Z1927Requisito_Titulo = A1927Requisito_Titulo;
            Z1926Requisito_Agrupador = A1926Requisito_Agrupador;
            Z1931Requisito_Ordem = A1931Requisito_Ordem;
            Z1932Requisito_Pontuacao = A1932Requisito_Pontuacao;
            Z1933Requisito_DataHomologacao = A1933Requisito_DataHomologacao;
            Z1934Requisito_Status = A1934Requisito_Status;
            Z1943Requisito_Cadastro = A1943Requisito_Cadastro;
            Z2002Requisito_Prioridade = A2002Requisito_Prioridade;
            Z1685Proposta_Codigo = A1685Proposta_Codigo;
            Z2049Requisito_TipoReqCod = A2049Requisito_TipoReqCod;
            Z1999Requisito_ReqCod = A1999Requisito_ReqCod;
         }
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -9 )
         {
            Z1919Requisito_Codigo = A1919Requisito_Codigo;
            Z1935Requisito_Ativo = A1935Requisito_Ativo;
            Z2001Requisito_Identificador = A2001Requisito_Identificador;
            Z1927Requisito_Titulo = A1927Requisito_Titulo;
            Z1923Requisito_Descricao = A1923Requisito_Descricao;
            Z1925Requisito_ReferenciaTecnica = A1925Requisito_ReferenciaTecnica;
            Z1926Requisito_Agrupador = A1926Requisito_Agrupador;
            Z1929Requisito_Restricao = A1929Requisito_Restricao;
            Z1931Requisito_Ordem = A1931Requisito_Ordem;
            Z1932Requisito_Pontuacao = A1932Requisito_Pontuacao;
            Z1933Requisito_DataHomologacao = A1933Requisito_DataHomologacao;
            Z1934Requisito_Status = A1934Requisito_Status;
            Z1943Requisito_Cadastro = A1943Requisito_Cadastro;
            Z2002Requisito_Prioridade = A2002Requisito_Prioridade;
            Z1685Proposta_Codigo = A1685Proposta_Codigo;
            Z2049Requisito_TipoReqCod = A2049Requisito_TipoReqCod;
            Z1999Requisito_ReqCod = A1999Requisito_ReqCod;
            Z1690Proposta_Objetivo = A1690Proposta_Objetivo;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV25Pgmname = "Requisito_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1935Requisito_Ativo = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A1943Requisito_Cadastro) && ( Gx_BScreen == 0 ) )
         {
            A1943Requisito_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
            n1943Requisito_Cadastro = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load4U215( )
      {
         /* Using cursor BC004U7 */
         pr_default.execute(5, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound215 = 1;
            A1935Requisito_Ativo = BC004U7_A1935Requisito_Ativo[0];
            A2001Requisito_Identificador = BC004U7_A2001Requisito_Identificador[0];
            n2001Requisito_Identificador = BC004U7_n2001Requisito_Identificador[0];
            A1927Requisito_Titulo = BC004U7_A1927Requisito_Titulo[0];
            n1927Requisito_Titulo = BC004U7_n1927Requisito_Titulo[0];
            A1923Requisito_Descricao = BC004U7_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = BC004U7_n1923Requisito_Descricao[0];
            A1690Proposta_Objetivo = BC004U7_A1690Proposta_Objetivo[0];
            A1925Requisito_ReferenciaTecnica = BC004U7_A1925Requisito_ReferenciaTecnica[0];
            n1925Requisito_ReferenciaTecnica = BC004U7_n1925Requisito_ReferenciaTecnica[0];
            A1926Requisito_Agrupador = BC004U7_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = BC004U7_n1926Requisito_Agrupador[0];
            A1929Requisito_Restricao = BC004U7_A1929Requisito_Restricao[0];
            n1929Requisito_Restricao = BC004U7_n1929Requisito_Restricao[0];
            A1931Requisito_Ordem = BC004U7_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = BC004U7_n1931Requisito_Ordem[0];
            A1932Requisito_Pontuacao = BC004U7_A1932Requisito_Pontuacao[0];
            n1932Requisito_Pontuacao = BC004U7_n1932Requisito_Pontuacao[0];
            A1933Requisito_DataHomologacao = BC004U7_A1933Requisito_DataHomologacao[0];
            n1933Requisito_DataHomologacao = BC004U7_n1933Requisito_DataHomologacao[0];
            A1934Requisito_Status = BC004U7_A1934Requisito_Status[0];
            A1943Requisito_Cadastro = BC004U7_A1943Requisito_Cadastro[0];
            n1943Requisito_Cadastro = BC004U7_n1943Requisito_Cadastro[0];
            A2002Requisito_Prioridade = BC004U7_A2002Requisito_Prioridade[0];
            n2002Requisito_Prioridade = BC004U7_n2002Requisito_Prioridade[0];
            A1685Proposta_Codigo = BC004U7_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = BC004U7_n1685Proposta_Codigo[0];
            A2049Requisito_TipoReqCod = BC004U7_A2049Requisito_TipoReqCod[0];
            n2049Requisito_TipoReqCod = BC004U7_n2049Requisito_TipoReqCod[0];
            A1999Requisito_ReqCod = BC004U7_A1999Requisito_ReqCod[0];
            n1999Requisito_ReqCod = BC004U7_n1999Requisito_ReqCod[0];
            ZM4U215( -9) ;
         }
         pr_default.close(5);
         OnLoadActions4U215( ) ;
      }

      protected void OnLoadActions4U215( )
      {
      }

      protected void CheckExtendedTable4U215( )
      {
         standaloneModal( ) ;
         /* Using cursor BC004U4 */
         pr_default.execute(2, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A1685Proposta_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Proposta'.", "ForeignKeyNotFound", 1, "PROPOSTA_CODIGO");
               AnyError = 1;
            }
         }
         A1690Proposta_Objetivo = BC004U4_A1690Proposta_Objetivo[0];
         pr_default.close(2);
         /* Using cursor BC004U5 */
         pr_default.execute(3, new Object[] {n2049Requisito_TipoReqCod, A2049Requisito_TipoReqCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A2049Requisito_TipoReqCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Requisito_Tipo Requisito'.", "ForeignKeyNotFound", 1, "REQUISITO_TIPOREQCOD");
               AnyError = 1;
            }
         }
         pr_default.close(3);
         if ( ! ( (DateTime.MinValue==A1933Requisito_DataHomologacao) || ( A1933Requisito_DataHomologacao >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data Homologa��o fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A1934Requisito_Status == 0 ) || ( A1934Requisito_Status == 1 ) || ( A1934Requisito_Status == 2 ) || ( A1934Requisito_Status == 3 ) || ( A1934Requisito_Status == 4 ) || ( A1934Requisito_Status == 5 ) ) )
         {
            GX_msglist.addItem("Campo Status fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A1943Requisito_Cadastro) || ( A1943Requisito_Cadastro >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Cadastrado fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC004U6 */
         pr_default.execute(4, new Object[] {n1999Requisito_ReqCod, A1999Requisito_ReqCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A1999Requisito_ReqCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Requisito_Requisito De'.", "ForeignKeyNotFound", 1, "REQUISITO_REQCOD");
               AnyError = 1;
            }
         }
         pr_default.close(4);
         if ( ! ( ( A2002Requisito_Prioridade == 1 ) || ( A2002Requisito_Prioridade == 2 ) || ( A2002Requisito_Prioridade == 3 ) || ( A2002Requisito_Prioridade == 4 ) || ( A2002Requisito_Prioridade == 5 ) || ( A2002Requisito_Prioridade == 6 ) || ( A2002Requisito_Prioridade == 7 ) || ( A2002Requisito_Prioridade == 8 ) || ( A2002Requisito_Prioridade == 9 ) || (0==A2002Requisito_Prioridade) ) )
         {
            GX_msglist.addItem("Campo Prioridade fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors4U215( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4U215( )
      {
         /* Using cursor BC004U8 */
         pr_default.execute(6, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound215 = 1;
         }
         else
         {
            RcdFound215 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004U3 */
         pr_default.execute(1, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4U215( 9) ;
            RcdFound215 = 1;
            A1919Requisito_Codigo = BC004U3_A1919Requisito_Codigo[0];
            A1935Requisito_Ativo = BC004U3_A1935Requisito_Ativo[0];
            A2001Requisito_Identificador = BC004U3_A2001Requisito_Identificador[0];
            n2001Requisito_Identificador = BC004U3_n2001Requisito_Identificador[0];
            A1927Requisito_Titulo = BC004U3_A1927Requisito_Titulo[0];
            n1927Requisito_Titulo = BC004U3_n1927Requisito_Titulo[0];
            A1923Requisito_Descricao = BC004U3_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = BC004U3_n1923Requisito_Descricao[0];
            A1925Requisito_ReferenciaTecnica = BC004U3_A1925Requisito_ReferenciaTecnica[0];
            n1925Requisito_ReferenciaTecnica = BC004U3_n1925Requisito_ReferenciaTecnica[0];
            A1926Requisito_Agrupador = BC004U3_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = BC004U3_n1926Requisito_Agrupador[0];
            A1929Requisito_Restricao = BC004U3_A1929Requisito_Restricao[0];
            n1929Requisito_Restricao = BC004U3_n1929Requisito_Restricao[0];
            A1931Requisito_Ordem = BC004U3_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = BC004U3_n1931Requisito_Ordem[0];
            A1932Requisito_Pontuacao = BC004U3_A1932Requisito_Pontuacao[0];
            n1932Requisito_Pontuacao = BC004U3_n1932Requisito_Pontuacao[0];
            A1933Requisito_DataHomologacao = BC004U3_A1933Requisito_DataHomologacao[0];
            n1933Requisito_DataHomologacao = BC004U3_n1933Requisito_DataHomologacao[0];
            A1934Requisito_Status = BC004U3_A1934Requisito_Status[0];
            A1943Requisito_Cadastro = BC004U3_A1943Requisito_Cadastro[0];
            n1943Requisito_Cadastro = BC004U3_n1943Requisito_Cadastro[0];
            A2002Requisito_Prioridade = BC004U3_A2002Requisito_Prioridade[0];
            n2002Requisito_Prioridade = BC004U3_n2002Requisito_Prioridade[0];
            A1685Proposta_Codigo = BC004U3_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = BC004U3_n1685Proposta_Codigo[0];
            A2049Requisito_TipoReqCod = BC004U3_A2049Requisito_TipoReqCod[0];
            n2049Requisito_TipoReqCod = BC004U3_n2049Requisito_TipoReqCod[0];
            A1999Requisito_ReqCod = BC004U3_A1999Requisito_ReqCod[0];
            n1999Requisito_ReqCod = BC004U3_n1999Requisito_ReqCod[0];
            Z1919Requisito_Codigo = A1919Requisito_Codigo;
            sMode215 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4U215( ) ;
            if ( AnyError == 1 )
            {
               RcdFound215 = 0;
               InitializeNonKey4U215( ) ;
            }
            Gx_mode = sMode215;
         }
         else
         {
            RcdFound215 = 0;
            InitializeNonKey4U215( ) ;
            sMode215 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode215;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4U215( ) ;
         if ( RcdFound215 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4U0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4U215( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004U2 */
            pr_default.execute(0, new Object[] {A1919Requisito_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Requisito"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1935Requisito_Ativo != BC004U2_A1935Requisito_Ativo[0] ) || ( StringUtil.StrCmp(Z2001Requisito_Identificador, BC004U2_A2001Requisito_Identificador[0]) != 0 ) || ( StringUtil.StrCmp(Z1927Requisito_Titulo, BC004U2_A1927Requisito_Titulo[0]) != 0 ) || ( StringUtil.StrCmp(Z1926Requisito_Agrupador, BC004U2_A1926Requisito_Agrupador[0]) != 0 ) || ( Z1931Requisito_Ordem != BC004U2_A1931Requisito_Ordem[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1932Requisito_Pontuacao != BC004U2_A1932Requisito_Pontuacao[0] ) || ( Z1933Requisito_DataHomologacao != BC004U2_A1933Requisito_DataHomologacao[0] ) || ( Z1934Requisito_Status != BC004U2_A1934Requisito_Status[0] ) || ( Z1943Requisito_Cadastro != BC004U2_A1943Requisito_Cadastro[0] ) || ( Z2002Requisito_Prioridade != BC004U2_A2002Requisito_Prioridade[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1685Proposta_Codigo != BC004U2_A1685Proposta_Codigo[0] ) || ( Z2049Requisito_TipoReqCod != BC004U2_A2049Requisito_TipoReqCod[0] ) || ( Z1999Requisito_ReqCod != BC004U2_A1999Requisito_ReqCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Requisito"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4U215( )
      {
         BeforeValidate4U215( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4U215( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4U215( 0) ;
            CheckOptimisticConcurrency4U215( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4U215( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4U215( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004U9 */
                     pr_default.execute(7, new Object[] {A1935Requisito_Ativo, n2001Requisito_Identificador, A2001Requisito_Identificador, n1927Requisito_Titulo, A1927Requisito_Titulo, n1923Requisito_Descricao, A1923Requisito_Descricao, n1925Requisito_ReferenciaTecnica, A1925Requisito_ReferenciaTecnica, n1926Requisito_Agrupador, A1926Requisito_Agrupador, n1929Requisito_Restricao, A1929Requisito_Restricao, n1931Requisito_Ordem, A1931Requisito_Ordem, n1932Requisito_Pontuacao, A1932Requisito_Pontuacao, n1933Requisito_DataHomologacao, A1933Requisito_DataHomologacao, A1934Requisito_Status, n1943Requisito_Cadastro, A1943Requisito_Cadastro, n2002Requisito_Prioridade, A2002Requisito_Prioridade, n1685Proposta_Codigo, A1685Proposta_Codigo, n2049Requisito_TipoReqCod, A2049Requisito_TipoReqCod, n1999Requisito_ReqCod, A1999Requisito_ReqCod});
                     A1919Requisito_Codigo = BC004U9_A1919Requisito_Codigo[0];
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Requisito") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4U215( ) ;
            }
            EndLevel4U215( ) ;
         }
         CloseExtendedTableCursors4U215( ) ;
      }

      protected void Update4U215( )
      {
         BeforeValidate4U215( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4U215( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4U215( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4U215( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4U215( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004U10 */
                     pr_default.execute(8, new Object[] {A1935Requisito_Ativo, n2001Requisito_Identificador, A2001Requisito_Identificador, n1927Requisito_Titulo, A1927Requisito_Titulo, n1923Requisito_Descricao, A1923Requisito_Descricao, n1925Requisito_ReferenciaTecnica, A1925Requisito_ReferenciaTecnica, n1926Requisito_Agrupador, A1926Requisito_Agrupador, n1929Requisito_Restricao, A1929Requisito_Restricao, n1931Requisito_Ordem, A1931Requisito_Ordem, n1932Requisito_Pontuacao, A1932Requisito_Pontuacao, n1933Requisito_DataHomologacao, A1933Requisito_DataHomologacao, A1934Requisito_Status, n1943Requisito_Cadastro, A1943Requisito_Cadastro, n2002Requisito_Prioridade, A2002Requisito_Prioridade, n1685Proposta_Codigo, A1685Proposta_Codigo, n2049Requisito_TipoReqCod, A2049Requisito_TipoReqCod, n1999Requisito_ReqCod, A1999Requisito_ReqCod, A1919Requisito_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Requisito") ;
                     if ( (pr_default.getStatus(8) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Requisito"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4U215( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4U215( ) ;
         }
         CloseExtendedTableCursors4U215( ) ;
      }

      protected void DeferredUpdate4U215( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4U215( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4U215( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4U215( ) ;
            AfterConfirm4U215( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4U215( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004U11 */
                  pr_default.execute(9, new Object[] {A1919Requisito_Codigo});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("Requisito") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode215 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4U215( ) ;
         Gx_mode = sMode215;
      }

      protected void OnDeleteControls4U215( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC004U12 */
            pr_default.execute(10, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
            A1690Proposta_Objetivo = BC004U12_A1690Proposta_Objetivo[0];
            pr_default.close(10);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC004U13 */
            pr_default.execute(11, new Object[] {A1919Requisito_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T213"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC004U14 */
            pr_default.execute(12, new Object[] {A1919Requisito_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Requisito"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor BC004U15 */
            pr_default.execute(13, new Object[] {A1919Requisito_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Req Neg Req Tec"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel4U215( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4U215( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4U215( )
      {
         /* Scan By routine */
         /* Using cursor BC004U16 */
         pr_default.execute(14, new Object[] {A1919Requisito_Codigo});
         RcdFound215 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound215 = 1;
            A1919Requisito_Codigo = BC004U16_A1919Requisito_Codigo[0];
            A1935Requisito_Ativo = BC004U16_A1935Requisito_Ativo[0];
            A2001Requisito_Identificador = BC004U16_A2001Requisito_Identificador[0];
            n2001Requisito_Identificador = BC004U16_n2001Requisito_Identificador[0];
            A1927Requisito_Titulo = BC004U16_A1927Requisito_Titulo[0];
            n1927Requisito_Titulo = BC004U16_n1927Requisito_Titulo[0];
            A1923Requisito_Descricao = BC004U16_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = BC004U16_n1923Requisito_Descricao[0];
            A1690Proposta_Objetivo = BC004U16_A1690Proposta_Objetivo[0];
            A1925Requisito_ReferenciaTecnica = BC004U16_A1925Requisito_ReferenciaTecnica[0];
            n1925Requisito_ReferenciaTecnica = BC004U16_n1925Requisito_ReferenciaTecnica[0];
            A1926Requisito_Agrupador = BC004U16_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = BC004U16_n1926Requisito_Agrupador[0];
            A1929Requisito_Restricao = BC004U16_A1929Requisito_Restricao[0];
            n1929Requisito_Restricao = BC004U16_n1929Requisito_Restricao[0];
            A1931Requisito_Ordem = BC004U16_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = BC004U16_n1931Requisito_Ordem[0];
            A1932Requisito_Pontuacao = BC004U16_A1932Requisito_Pontuacao[0];
            n1932Requisito_Pontuacao = BC004U16_n1932Requisito_Pontuacao[0];
            A1933Requisito_DataHomologacao = BC004U16_A1933Requisito_DataHomologacao[0];
            n1933Requisito_DataHomologacao = BC004U16_n1933Requisito_DataHomologacao[0];
            A1934Requisito_Status = BC004U16_A1934Requisito_Status[0];
            A1943Requisito_Cadastro = BC004U16_A1943Requisito_Cadastro[0];
            n1943Requisito_Cadastro = BC004U16_n1943Requisito_Cadastro[0];
            A2002Requisito_Prioridade = BC004U16_A2002Requisito_Prioridade[0];
            n2002Requisito_Prioridade = BC004U16_n2002Requisito_Prioridade[0];
            A1685Proposta_Codigo = BC004U16_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = BC004U16_n1685Proposta_Codigo[0];
            A2049Requisito_TipoReqCod = BC004U16_A2049Requisito_TipoReqCod[0];
            n2049Requisito_TipoReqCod = BC004U16_n2049Requisito_TipoReqCod[0];
            A1999Requisito_ReqCod = BC004U16_A1999Requisito_ReqCod[0];
            n1999Requisito_ReqCod = BC004U16_n1999Requisito_ReqCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4U215( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound215 = 0;
         ScanKeyLoad4U215( ) ;
      }

      protected void ScanKeyLoad4U215( )
      {
         sMode215 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound215 = 1;
            A1919Requisito_Codigo = BC004U16_A1919Requisito_Codigo[0];
            A1935Requisito_Ativo = BC004U16_A1935Requisito_Ativo[0];
            A2001Requisito_Identificador = BC004U16_A2001Requisito_Identificador[0];
            n2001Requisito_Identificador = BC004U16_n2001Requisito_Identificador[0];
            A1927Requisito_Titulo = BC004U16_A1927Requisito_Titulo[0];
            n1927Requisito_Titulo = BC004U16_n1927Requisito_Titulo[0];
            A1923Requisito_Descricao = BC004U16_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = BC004U16_n1923Requisito_Descricao[0];
            A1690Proposta_Objetivo = BC004U16_A1690Proposta_Objetivo[0];
            A1925Requisito_ReferenciaTecnica = BC004U16_A1925Requisito_ReferenciaTecnica[0];
            n1925Requisito_ReferenciaTecnica = BC004U16_n1925Requisito_ReferenciaTecnica[0];
            A1926Requisito_Agrupador = BC004U16_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = BC004U16_n1926Requisito_Agrupador[0];
            A1929Requisito_Restricao = BC004U16_A1929Requisito_Restricao[0];
            n1929Requisito_Restricao = BC004U16_n1929Requisito_Restricao[0];
            A1931Requisito_Ordem = BC004U16_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = BC004U16_n1931Requisito_Ordem[0];
            A1932Requisito_Pontuacao = BC004U16_A1932Requisito_Pontuacao[0];
            n1932Requisito_Pontuacao = BC004U16_n1932Requisito_Pontuacao[0];
            A1933Requisito_DataHomologacao = BC004U16_A1933Requisito_DataHomologacao[0];
            n1933Requisito_DataHomologacao = BC004U16_n1933Requisito_DataHomologacao[0];
            A1934Requisito_Status = BC004U16_A1934Requisito_Status[0];
            A1943Requisito_Cadastro = BC004U16_A1943Requisito_Cadastro[0];
            n1943Requisito_Cadastro = BC004U16_n1943Requisito_Cadastro[0];
            A2002Requisito_Prioridade = BC004U16_A2002Requisito_Prioridade[0];
            n2002Requisito_Prioridade = BC004U16_n2002Requisito_Prioridade[0];
            A1685Proposta_Codigo = BC004U16_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = BC004U16_n1685Proposta_Codigo[0];
            A2049Requisito_TipoReqCod = BC004U16_A2049Requisito_TipoReqCod[0];
            n2049Requisito_TipoReqCod = BC004U16_n2049Requisito_TipoReqCod[0];
            A1999Requisito_ReqCod = BC004U16_A1999Requisito_ReqCod[0];
            n1999Requisito_ReqCod = BC004U16_n1999Requisito_ReqCod[0];
         }
         Gx_mode = sMode215;
      }

      protected void ScanKeyEnd4U215( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm4U215( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4U215( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4U215( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4U215( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4U215( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4U215( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4U215( )
      {
      }

      protected void AddRow4U215( )
      {
         VarsToRow215( bcRequisito) ;
      }

      protected void ReadRow4U215( )
      {
         RowToVars215( bcRequisito, 1) ;
      }

      protected void InitializeNonKey4U215( )
      {
         A1935Requisito_Ativo = false;
         A2001Requisito_Identificador = "";
         n2001Requisito_Identificador = false;
         A1927Requisito_Titulo = "";
         n1927Requisito_Titulo = false;
         A1923Requisito_Descricao = "";
         n1923Requisito_Descricao = false;
         A1685Proposta_Codigo = 0;
         n1685Proposta_Codigo = false;
         A1690Proposta_Objetivo = "";
         A2049Requisito_TipoReqCod = 0;
         n2049Requisito_TipoReqCod = false;
         A1925Requisito_ReferenciaTecnica = "";
         n1925Requisito_ReferenciaTecnica = false;
         A1926Requisito_Agrupador = "";
         n1926Requisito_Agrupador = false;
         A1929Requisito_Restricao = "";
         n1929Requisito_Restricao = false;
         A1931Requisito_Ordem = 0;
         n1931Requisito_Ordem = false;
         A1932Requisito_Pontuacao = 0;
         n1932Requisito_Pontuacao = false;
         A1933Requisito_DataHomologacao = DateTime.MinValue;
         n1933Requisito_DataHomologacao = false;
         A1934Requisito_Status = 0;
         A1999Requisito_ReqCod = 0;
         n1999Requisito_ReqCod = false;
         A2002Requisito_Prioridade = 0;
         n2002Requisito_Prioridade = false;
         A1943Requisito_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1943Requisito_Cadastro = false;
         Z1935Requisito_Ativo = false;
         Z2001Requisito_Identificador = "";
         Z1927Requisito_Titulo = "";
         Z1926Requisito_Agrupador = "";
         Z1931Requisito_Ordem = 0;
         Z1932Requisito_Pontuacao = 0;
         Z1933Requisito_DataHomologacao = DateTime.MinValue;
         Z1934Requisito_Status = 0;
         Z1943Requisito_Cadastro = (DateTime)(DateTime.MinValue);
         Z2002Requisito_Prioridade = 0;
         Z1685Proposta_Codigo = 0;
         Z2049Requisito_TipoReqCod = 0;
         Z1999Requisito_ReqCod = 0;
      }

      protected void InitAll4U215( )
      {
         A1919Requisito_Codigo = 0;
         InitializeNonKey4U215( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1935Requisito_Ativo = i1935Requisito_Ativo;
         A1943Requisito_Cadastro = i1943Requisito_Cadastro;
         n1943Requisito_Cadastro = false;
      }

      public void VarsToRow215( SdtRequisito obj215 )
      {
         obj215.gxTpr_Mode = Gx_mode;
         obj215.gxTpr_Requisito_ativo = A1935Requisito_Ativo;
         obj215.gxTpr_Requisito_identificador = A2001Requisito_Identificador;
         obj215.gxTpr_Requisito_titulo = A1927Requisito_Titulo;
         obj215.gxTpr_Requisito_descricao = A1923Requisito_Descricao;
         obj215.gxTpr_Proposta_codigo = A1685Proposta_Codigo;
         obj215.gxTpr_Proposta_objetivo = A1690Proposta_Objetivo;
         obj215.gxTpr_Requisito_tiporeqcod = A2049Requisito_TipoReqCod;
         obj215.gxTpr_Requisito_referenciatecnica = A1925Requisito_ReferenciaTecnica;
         obj215.gxTpr_Requisito_agrupador = A1926Requisito_Agrupador;
         obj215.gxTpr_Requisito_restricao = A1929Requisito_Restricao;
         obj215.gxTpr_Requisito_ordem = A1931Requisito_Ordem;
         obj215.gxTpr_Requisito_pontuacao = A1932Requisito_Pontuacao;
         obj215.gxTpr_Requisito_datahomologacao = A1933Requisito_DataHomologacao;
         obj215.gxTpr_Requisito_status = A1934Requisito_Status;
         obj215.gxTpr_Requisito_reqcod = A1999Requisito_ReqCod;
         obj215.gxTpr_Requisito_prioridade = A2002Requisito_Prioridade;
         obj215.gxTpr_Requisito_cadastro = A1943Requisito_Cadastro;
         obj215.gxTpr_Requisito_codigo = A1919Requisito_Codigo;
         obj215.gxTpr_Requisito_codigo_Z = Z1919Requisito_Codigo;
         obj215.gxTpr_Requisito_identificador_Z = Z2001Requisito_Identificador;
         obj215.gxTpr_Requisito_titulo_Z = Z1927Requisito_Titulo;
         obj215.gxTpr_Proposta_codigo_Z = Z1685Proposta_Codigo;
         obj215.gxTpr_Requisito_tiporeqcod_Z = Z2049Requisito_TipoReqCod;
         obj215.gxTpr_Requisito_agrupador_Z = Z1926Requisito_Agrupador;
         obj215.gxTpr_Requisito_ordem_Z = Z1931Requisito_Ordem;
         obj215.gxTpr_Requisito_pontuacao_Z = Z1932Requisito_Pontuacao;
         obj215.gxTpr_Requisito_datahomologacao_Z = Z1933Requisito_DataHomologacao;
         obj215.gxTpr_Requisito_status_Z = Z1934Requisito_Status;
         obj215.gxTpr_Requisito_cadastro_Z = Z1943Requisito_Cadastro;
         obj215.gxTpr_Requisito_reqcod_Z = Z1999Requisito_ReqCod;
         obj215.gxTpr_Requisito_prioridade_Z = Z2002Requisito_Prioridade;
         obj215.gxTpr_Requisito_ativo_Z = Z1935Requisito_Ativo;
         obj215.gxTpr_Requisito_identificador_N = (short)(Convert.ToInt16(n2001Requisito_Identificador));
         obj215.gxTpr_Requisito_titulo_N = (short)(Convert.ToInt16(n1927Requisito_Titulo));
         obj215.gxTpr_Requisito_descricao_N = (short)(Convert.ToInt16(n1923Requisito_Descricao));
         obj215.gxTpr_Proposta_codigo_N = (short)(Convert.ToInt16(n1685Proposta_Codigo));
         obj215.gxTpr_Requisito_tiporeqcod_N = (short)(Convert.ToInt16(n2049Requisito_TipoReqCod));
         obj215.gxTpr_Requisito_referenciatecnica_N = (short)(Convert.ToInt16(n1925Requisito_ReferenciaTecnica));
         obj215.gxTpr_Requisito_agrupador_N = (short)(Convert.ToInt16(n1926Requisito_Agrupador));
         obj215.gxTpr_Requisito_restricao_N = (short)(Convert.ToInt16(n1929Requisito_Restricao));
         obj215.gxTpr_Requisito_ordem_N = (short)(Convert.ToInt16(n1931Requisito_Ordem));
         obj215.gxTpr_Requisito_pontuacao_N = (short)(Convert.ToInt16(n1932Requisito_Pontuacao));
         obj215.gxTpr_Requisito_datahomologacao_N = (short)(Convert.ToInt16(n1933Requisito_DataHomologacao));
         obj215.gxTpr_Requisito_cadastro_N = (short)(Convert.ToInt16(n1943Requisito_Cadastro));
         obj215.gxTpr_Requisito_reqcod_N = (short)(Convert.ToInt16(n1999Requisito_ReqCod));
         obj215.gxTpr_Requisito_prioridade_N = (short)(Convert.ToInt16(n2002Requisito_Prioridade));
         obj215.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow215( SdtRequisito obj215 )
      {
         obj215.gxTpr_Requisito_codigo = A1919Requisito_Codigo;
         return  ;
      }

      public void RowToVars215( SdtRequisito obj215 ,
                                int forceLoad )
      {
         Gx_mode = obj215.gxTpr_Mode;
         A1935Requisito_Ativo = obj215.gxTpr_Requisito_ativo;
         A2001Requisito_Identificador = obj215.gxTpr_Requisito_identificador;
         n2001Requisito_Identificador = false;
         A1927Requisito_Titulo = obj215.gxTpr_Requisito_titulo;
         n1927Requisito_Titulo = false;
         A1923Requisito_Descricao = obj215.gxTpr_Requisito_descricao;
         n1923Requisito_Descricao = false;
         A1685Proposta_Codigo = obj215.gxTpr_Proposta_codigo;
         n1685Proposta_Codigo = false;
         A1690Proposta_Objetivo = obj215.gxTpr_Proposta_objetivo;
         A2049Requisito_TipoReqCod = obj215.gxTpr_Requisito_tiporeqcod;
         n2049Requisito_TipoReqCod = false;
         A1925Requisito_ReferenciaTecnica = obj215.gxTpr_Requisito_referenciatecnica;
         n1925Requisito_ReferenciaTecnica = false;
         A1926Requisito_Agrupador = obj215.gxTpr_Requisito_agrupador;
         n1926Requisito_Agrupador = false;
         A1929Requisito_Restricao = obj215.gxTpr_Requisito_restricao;
         n1929Requisito_Restricao = false;
         A1931Requisito_Ordem = obj215.gxTpr_Requisito_ordem;
         n1931Requisito_Ordem = false;
         A1932Requisito_Pontuacao = obj215.gxTpr_Requisito_pontuacao;
         n1932Requisito_Pontuacao = false;
         A1933Requisito_DataHomologacao = obj215.gxTpr_Requisito_datahomologacao;
         n1933Requisito_DataHomologacao = false;
         A1934Requisito_Status = obj215.gxTpr_Requisito_status;
         A1999Requisito_ReqCod = obj215.gxTpr_Requisito_reqcod;
         n1999Requisito_ReqCod = false;
         A2002Requisito_Prioridade = obj215.gxTpr_Requisito_prioridade;
         n2002Requisito_Prioridade = false;
         A1943Requisito_Cadastro = obj215.gxTpr_Requisito_cadastro;
         n1943Requisito_Cadastro = false;
         A1919Requisito_Codigo = obj215.gxTpr_Requisito_codigo;
         Z1919Requisito_Codigo = obj215.gxTpr_Requisito_codigo_Z;
         Z2001Requisito_Identificador = obj215.gxTpr_Requisito_identificador_Z;
         Z1927Requisito_Titulo = obj215.gxTpr_Requisito_titulo_Z;
         Z1685Proposta_Codigo = obj215.gxTpr_Proposta_codigo_Z;
         Z2049Requisito_TipoReqCod = obj215.gxTpr_Requisito_tiporeqcod_Z;
         Z1926Requisito_Agrupador = obj215.gxTpr_Requisito_agrupador_Z;
         Z1931Requisito_Ordem = obj215.gxTpr_Requisito_ordem_Z;
         Z1932Requisito_Pontuacao = obj215.gxTpr_Requisito_pontuacao_Z;
         Z1933Requisito_DataHomologacao = obj215.gxTpr_Requisito_datahomologacao_Z;
         Z1934Requisito_Status = obj215.gxTpr_Requisito_status_Z;
         Z1943Requisito_Cadastro = obj215.gxTpr_Requisito_cadastro_Z;
         Z1999Requisito_ReqCod = obj215.gxTpr_Requisito_reqcod_Z;
         Z2002Requisito_Prioridade = obj215.gxTpr_Requisito_prioridade_Z;
         Z1935Requisito_Ativo = obj215.gxTpr_Requisito_ativo_Z;
         n2001Requisito_Identificador = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_identificador_N));
         n1927Requisito_Titulo = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_titulo_N));
         n1923Requisito_Descricao = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_descricao_N));
         n1685Proposta_Codigo = (bool)(Convert.ToBoolean(obj215.gxTpr_Proposta_codigo_N));
         n2049Requisito_TipoReqCod = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_tiporeqcod_N));
         n1925Requisito_ReferenciaTecnica = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_referenciatecnica_N));
         n1926Requisito_Agrupador = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_agrupador_N));
         n1929Requisito_Restricao = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_restricao_N));
         n1931Requisito_Ordem = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_ordem_N));
         n1932Requisito_Pontuacao = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_pontuacao_N));
         n1933Requisito_DataHomologacao = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_datahomologacao_N));
         n1943Requisito_Cadastro = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_cadastro_N));
         n1999Requisito_ReqCod = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_reqcod_N));
         n2002Requisito_Prioridade = (bool)(Convert.ToBoolean(obj215.gxTpr_Requisito_prioridade_N));
         Gx_mode = obj215.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1919Requisito_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4U215( ) ;
         ScanKeyStart4U215( ) ;
         if ( RcdFound215 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1919Requisito_Codigo = A1919Requisito_Codigo;
         }
         ZM4U215( -9) ;
         OnLoadActions4U215( ) ;
         AddRow4U215( ) ;
         ScanKeyEnd4U215( ) ;
         if ( RcdFound215 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars215( bcRequisito, 0) ;
         ScanKeyStart4U215( ) ;
         if ( RcdFound215 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1919Requisito_Codigo = A1919Requisito_Codigo;
         }
         ZM4U215( -9) ;
         OnLoadActions4U215( ) ;
         AddRow4U215( ) ;
         ScanKeyEnd4U215( ) ;
         if ( RcdFound215 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars215( bcRequisito, 0) ;
         nKeyPressed = 1;
         GetKey4U215( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4U215( ) ;
         }
         else
         {
            if ( RcdFound215 == 1 )
            {
               if ( A1919Requisito_Codigo != Z1919Requisito_Codigo )
               {
                  A1919Requisito_Codigo = Z1919Requisito_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4U215( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1919Requisito_Codigo != Z1919Requisito_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4U215( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4U215( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow215( bcRequisito) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars215( bcRequisito, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4U215( ) ;
         if ( RcdFound215 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1919Requisito_Codigo != Z1919Requisito_Codigo )
            {
               A1919Requisito_Codigo = Z1919Requisito_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1919Requisito_Codigo != Z1919Requisito_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(10);
         context.RollbackDataStores( "Requisito_BC");
         VarsToRow215( bcRequisito) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcRequisito.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcRequisito.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcRequisito )
         {
            bcRequisito = (SdtRequisito)(sdt);
            if ( StringUtil.StrCmp(bcRequisito.gxTpr_Mode, "") == 0 )
            {
               bcRequisito.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow215( bcRequisito) ;
            }
            else
            {
               RowToVars215( bcRequisito, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcRequisito.gxTpr_Mode, "") == 0 )
            {
               bcRequisito.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars215( bcRequisito, 1) ;
         return  ;
      }

      public SdtRequisito Requisito_BC
      {
         get {
            return bcRequisito ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV25Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV20Window = new GXWindow();
         Z2001Requisito_Identificador = "";
         A2001Requisito_Identificador = "";
         Z1927Requisito_Titulo = "";
         A1927Requisito_Titulo = "";
         Z1926Requisito_Agrupador = "";
         A1926Requisito_Agrupador = "";
         Z1933Requisito_DataHomologacao = DateTime.MinValue;
         A1933Requisito_DataHomologacao = DateTime.MinValue;
         Z1943Requisito_Cadastro = (DateTime)(DateTime.MinValue);
         A1943Requisito_Cadastro = (DateTime)(DateTime.MinValue);
         Z1923Requisito_Descricao = "";
         A1923Requisito_Descricao = "";
         Z1925Requisito_ReferenciaTecnica = "";
         A1925Requisito_ReferenciaTecnica = "";
         Z1929Requisito_Restricao = "";
         A1929Requisito_Restricao = "";
         Z1690Proposta_Objetivo = "";
         A1690Proposta_Objetivo = "";
         BC004U7_A1919Requisito_Codigo = new int[1] ;
         BC004U7_A1935Requisito_Ativo = new bool[] {false} ;
         BC004U7_A2001Requisito_Identificador = new String[] {""} ;
         BC004U7_n2001Requisito_Identificador = new bool[] {false} ;
         BC004U7_A1927Requisito_Titulo = new String[] {""} ;
         BC004U7_n1927Requisito_Titulo = new bool[] {false} ;
         BC004U7_A1923Requisito_Descricao = new String[] {""} ;
         BC004U7_n1923Requisito_Descricao = new bool[] {false} ;
         BC004U7_A1690Proposta_Objetivo = new String[] {""} ;
         BC004U7_A1925Requisito_ReferenciaTecnica = new String[] {""} ;
         BC004U7_n1925Requisito_ReferenciaTecnica = new bool[] {false} ;
         BC004U7_A1926Requisito_Agrupador = new String[] {""} ;
         BC004U7_n1926Requisito_Agrupador = new bool[] {false} ;
         BC004U7_A1929Requisito_Restricao = new String[] {""} ;
         BC004U7_n1929Requisito_Restricao = new bool[] {false} ;
         BC004U7_A1931Requisito_Ordem = new short[1] ;
         BC004U7_n1931Requisito_Ordem = new bool[] {false} ;
         BC004U7_A1932Requisito_Pontuacao = new decimal[1] ;
         BC004U7_n1932Requisito_Pontuacao = new bool[] {false} ;
         BC004U7_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         BC004U7_n1933Requisito_DataHomologacao = new bool[] {false} ;
         BC004U7_A1934Requisito_Status = new short[1] ;
         BC004U7_A1943Requisito_Cadastro = new DateTime[] {DateTime.MinValue} ;
         BC004U7_n1943Requisito_Cadastro = new bool[] {false} ;
         BC004U7_A2002Requisito_Prioridade = new short[1] ;
         BC004U7_n2002Requisito_Prioridade = new bool[] {false} ;
         BC004U7_A1685Proposta_Codigo = new int[1] ;
         BC004U7_n1685Proposta_Codigo = new bool[] {false} ;
         BC004U7_A2049Requisito_TipoReqCod = new int[1] ;
         BC004U7_n2049Requisito_TipoReqCod = new bool[] {false} ;
         BC004U7_A1999Requisito_ReqCod = new int[1] ;
         BC004U7_n1999Requisito_ReqCod = new bool[] {false} ;
         BC004U4_A1690Proposta_Objetivo = new String[] {""} ;
         BC004U5_A2049Requisito_TipoReqCod = new int[1] ;
         BC004U5_n2049Requisito_TipoReqCod = new bool[] {false} ;
         BC004U6_A1999Requisito_ReqCod = new int[1] ;
         BC004U6_n1999Requisito_ReqCod = new bool[] {false} ;
         BC004U8_A1919Requisito_Codigo = new int[1] ;
         BC004U3_A1919Requisito_Codigo = new int[1] ;
         BC004U3_A1935Requisito_Ativo = new bool[] {false} ;
         BC004U3_A2001Requisito_Identificador = new String[] {""} ;
         BC004U3_n2001Requisito_Identificador = new bool[] {false} ;
         BC004U3_A1927Requisito_Titulo = new String[] {""} ;
         BC004U3_n1927Requisito_Titulo = new bool[] {false} ;
         BC004U3_A1923Requisito_Descricao = new String[] {""} ;
         BC004U3_n1923Requisito_Descricao = new bool[] {false} ;
         BC004U3_A1925Requisito_ReferenciaTecnica = new String[] {""} ;
         BC004U3_n1925Requisito_ReferenciaTecnica = new bool[] {false} ;
         BC004U3_A1926Requisito_Agrupador = new String[] {""} ;
         BC004U3_n1926Requisito_Agrupador = new bool[] {false} ;
         BC004U3_A1929Requisito_Restricao = new String[] {""} ;
         BC004U3_n1929Requisito_Restricao = new bool[] {false} ;
         BC004U3_A1931Requisito_Ordem = new short[1] ;
         BC004U3_n1931Requisito_Ordem = new bool[] {false} ;
         BC004U3_A1932Requisito_Pontuacao = new decimal[1] ;
         BC004U3_n1932Requisito_Pontuacao = new bool[] {false} ;
         BC004U3_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         BC004U3_n1933Requisito_DataHomologacao = new bool[] {false} ;
         BC004U3_A1934Requisito_Status = new short[1] ;
         BC004U3_A1943Requisito_Cadastro = new DateTime[] {DateTime.MinValue} ;
         BC004U3_n1943Requisito_Cadastro = new bool[] {false} ;
         BC004U3_A2002Requisito_Prioridade = new short[1] ;
         BC004U3_n2002Requisito_Prioridade = new bool[] {false} ;
         BC004U3_A1685Proposta_Codigo = new int[1] ;
         BC004U3_n1685Proposta_Codigo = new bool[] {false} ;
         BC004U3_A2049Requisito_TipoReqCod = new int[1] ;
         BC004U3_n2049Requisito_TipoReqCod = new bool[] {false} ;
         BC004U3_A1999Requisito_ReqCod = new int[1] ;
         BC004U3_n1999Requisito_ReqCod = new bool[] {false} ;
         sMode215 = "";
         BC004U2_A1919Requisito_Codigo = new int[1] ;
         BC004U2_A1935Requisito_Ativo = new bool[] {false} ;
         BC004U2_A2001Requisito_Identificador = new String[] {""} ;
         BC004U2_n2001Requisito_Identificador = new bool[] {false} ;
         BC004U2_A1927Requisito_Titulo = new String[] {""} ;
         BC004U2_n1927Requisito_Titulo = new bool[] {false} ;
         BC004U2_A1923Requisito_Descricao = new String[] {""} ;
         BC004U2_n1923Requisito_Descricao = new bool[] {false} ;
         BC004U2_A1925Requisito_ReferenciaTecnica = new String[] {""} ;
         BC004U2_n1925Requisito_ReferenciaTecnica = new bool[] {false} ;
         BC004U2_A1926Requisito_Agrupador = new String[] {""} ;
         BC004U2_n1926Requisito_Agrupador = new bool[] {false} ;
         BC004U2_A1929Requisito_Restricao = new String[] {""} ;
         BC004U2_n1929Requisito_Restricao = new bool[] {false} ;
         BC004U2_A1931Requisito_Ordem = new short[1] ;
         BC004U2_n1931Requisito_Ordem = new bool[] {false} ;
         BC004U2_A1932Requisito_Pontuacao = new decimal[1] ;
         BC004U2_n1932Requisito_Pontuacao = new bool[] {false} ;
         BC004U2_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         BC004U2_n1933Requisito_DataHomologacao = new bool[] {false} ;
         BC004U2_A1934Requisito_Status = new short[1] ;
         BC004U2_A1943Requisito_Cadastro = new DateTime[] {DateTime.MinValue} ;
         BC004U2_n1943Requisito_Cadastro = new bool[] {false} ;
         BC004U2_A2002Requisito_Prioridade = new short[1] ;
         BC004U2_n2002Requisito_Prioridade = new bool[] {false} ;
         BC004U2_A1685Proposta_Codigo = new int[1] ;
         BC004U2_n1685Proposta_Codigo = new bool[] {false} ;
         BC004U2_A2049Requisito_TipoReqCod = new int[1] ;
         BC004U2_n2049Requisito_TipoReqCod = new bool[] {false} ;
         BC004U2_A1999Requisito_ReqCod = new int[1] ;
         BC004U2_n1999Requisito_ReqCod = new bool[] {false} ;
         BC004U9_A1919Requisito_Codigo = new int[1] ;
         BC004U12_A1690Proposta_Objetivo = new String[] {""} ;
         BC004U13_A1999Requisito_ReqCod = new int[1] ;
         BC004U13_n1999Requisito_ReqCod = new bool[] {false} ;
         BC004U14_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         BC004U15_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004U15_A1919Requisito_Codigo = new int[1] ;
         BC004U16_A1919Requisito_Codigo = new int[1] ;
         BC004U16_A1935Requisito_Ativo = new bool[] {false} ;
         BC004U16_A2001Requisito_Identificador = new String[] {""} ;
         BC004U16_n2001Requisito_Identificador = new bool[] {false} ;
         BC004U16_A1927Requisito_Titulo = new String[] {""} ;
         BC004U16_n1927Requisito_Titulo = new bool[] {false} ;
         BC004U16_A1923Requisito_Descricao = new String[] {""} ;
         BC004U16_n1923Requisito_Descricao = new bool[] {false} ;
         BC004U16_A1690Proposta_Objetivo = new String[] {""} ;
         BC004U16_A1925Requisito_ReferenciaTecnica = new String[] {""} ;
         BC004U16_n1925Requisito_ReferenciaTecnica = new bool[] {false} ;
         BC004U16_A1926Requisito_Agrupador = new String[] {""} ;
         BC004U16_n1926Requisito_Agrupador = new bool[] {false} ;
         BC004U16_A1929Requisito_Restricao = new String[] {""} ;
         BC004U16_n1929Requisito_Restricao = new bool[] {false} ;
         BC004U16_A1931Requisito_Ordem = new short[1] ;
         BC004U16_n1931Requisito_Ordem = new bool[] {false} ;
         BC004U16_A1932Requisito_Pontuacao = new decimal[1] ;
         BC004U16_n1932Requisito_Pontuacao = new bool[] {false} ;
         BC004U16_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         BC004U16_n1933Requisito_DataHomologacao = new bool[] {false} ;
         BC004U16_A1934Requisito_Status = new short[1] ;
         BC004U16_A1943Requisito_Cadastro = new DateTime[] {DateTime.MinValue} ;
         BC004U16_n1943Requisito_Cadastro = new bool[] {false} ;
         BC004U16_A2002Requisito_Prioridade = new short[1] ;
         BC004U16_n2002Requisito_Prioridade = new bool[] {false} ;
         BC004U16_A1685Proposta_Codigo = new int[1] ;
         BC004U16_n1685Proposta_Codigo = new bool[] {false} ;
         BC004U16_A2049Requisito_TipoReqCod = new int[1] ;
         BC004U16_n2049Requisito_TipoReqCod = new bool[] {false} ;
         BC004U16_A1999Requisito_ReqCod = new int[1] ;
         BC004U16_n1999Requisito_ReqCod = new bool[] {false} ;
         i1943Requisito_Cadastro = (DateTime)(DateTime.MinValue);
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.requisito_bc__default(),
            new Object[][] {
                new Object[] {
               BC004U2_A1919Requisito_Codigo, BC004U2_A1935Requisito_Ativo, BC004U2_A2001Requisito_Identificador, BC004U2_n2001Requisito_Identificador, BC004U2_A1927Requisito_Titulo, BC004U2_n1927Requisito_Titulo, BC004U2_A1923Requisito_Descricao, BC004U2_n1923Requisito_Descricao, BC004U2_A1925Requisito_ReferenciaTecnica, BC004U2_n1925Requisito_ReferenciaTecnica,
               BC004U2_A1926Requisito_Agrupador, BC004U2_n1926Requisito_Agrupador, BC004U2_A1929Requisito_Restricao, BC004U2_n1929Requisito_Restricao, BC004U2_A1931Requisito_Ordem, BC004U2_n1931Requisito_Ordem, BC004U2_A1932Requisito_Pontuacao, BC004U2_n1932Requisito_Pontuacao, BC004U2_A1933Requisito_DataHomologacao, BC004U2_n1933Requisito_DataHomologacao,
               BC004U2_A1934Requisito_Status, BC004U2_A1943Requisito_Cadastro, BC004U2_n1943Requisito_Cadastro, BC004U2_A2002Requisito_Prioridade, BC004U2_n2002Requisito_Prioridade, BC004U2_A1685Proposta_Codigo, BC004U2_n1685Proposta_Codigo, BC004U2_A2049Requisito_TipoReqCod, BC004U2_n2049Requisito_TipoReqCod, BC004U2_A1999Requisito_ReqCod,
               BC004U2_n1999Requisito_ReqCod
               }
               , new Object[] {
               BC004U3_A1919Requisito_Codigo, BC004U3_A1935Requisito_Ativo, BC004U3_A2001Requisito_Identificador, BC004U3_n2001Requisito_Identificador, BC004U3_A1927Requisito_Titulo, BC004U3_n1927Requisito_Titulo, BC004U3_A1923Requisito_Descricao, BC004U3_n1923Requisito_Descricao, BC004U3_A1925Requisito_ReferenciaTecnica, BC004U3_n1925Requisito_ReferenciaTecnica,
               BC004U3_A1926Requisito_Agrupador, BC004U3_n1926Requisito_Agrupador, BC004U3_A1929Requisito_Restricao, BC004U3_n1929Requisito_Restricao, BC004U3_A1931Requisito_Ordem, BC004U3_n1931Requisito_Ordem, BC004U3_A1932Requisito_Pontuacao, BC004U3_n1932Requisito_Pontuacao, BC004U3_A1933Requisito_DataHomologacao, BC004U3_n1933Requisito_DataHomologacao,
               BC004U3_A1934Requisito_Status, BC004U3_A1943Requisito_Cadastro, BC004U3_n1943Requisito_Cadastro, BC004U3_A2002Requisito_Prioridade, BC004U3_n2002Requisito_Prioridade, BC004U3_A1685Proposta_Codigo, BC004U3_n1685Proposta_Codigo, BC004U3_A2049Requisito_TipoReqCod, BC004U3_n2049Requisito_TipoReqCod, BC004U3_A1999Requisito_ReqCod,
               BC004U3_n1999Requisito_ReqCod
               }
               , new Object[] {
               BC004U4_A1690Proposta_Objetivo
               }
               , new Object[] {
               BC004U5_A2049Requisito_TipoReqCod
               }
               , new Object[] {
               BC004U6_A1999Requisito_ReqCod
               }
               , new Object[] {
               BC004U7_A1919Requisito_Codigo, BC004U7_A1935Requisito_Ativo, BC004U7_A2001Requisito_Identificador, BC004U7_n2001Requisito_Identificador, BC004U7_A1927Requisito_Titulo, BC004U7_n1927Requisito_Titulo, BC004U7_A1923Requisito_Descricao, BC004U7_n1923Requisito_Descricao, BC004U7_A1690Proposta_Objetivo, BC004U7_A1925Requisito_ReferenciaTecnica,
               BC004U7_n1925Requisito_ReferenciaTecnica, BC004U7_A1926Requisito_Agrupador, BC004U7_n1926Requisito_Agrupador, BC004U7_A1929Requisito_Restricao, BC004U7_n1929Requisito_Restricao, BC004U7_A1931Requisito_Ordem, BC004U7_n1931Requisito_Ordem, BC004U7_A1932Requisito_Pontuacao, BC004U7_n1932Requisito_Pontuacao, BC004U7_A1933Requisito_DataHomologacao,
               BC004U7_n1933Requisito_DataHomologacao, BC004U7_A1934Requisito_Status, BC004U7_A1943Requisito_Cadastro, BC004U7_n1943Requisito_Cadastro, BC004U7_A2002Requisito_Prioridade, BC004U7_n2002Requisito_Prioridade, BC004U7_A1685Proposta_Codigo, BC004U7_n1685Proposta_Codigo, BC004U7_A2049Requisito_TipoReqCod, BC004U7_n2049Requisito_TipoReqCod,
               BC004U7_A1999Requisito_ReqCod, BC004U7_n1999Requisito_ReqCod
               }
               , new Object[] {
               BC004U8_A1919Requisito_Codigo
               }
               , new Object[] {
               BC004U9_A1919Requisito_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004U12_A1690Proposta_Objetivo
               }
               , new Object[] {
               BC004U13_A1999Requisito_ReqCod
               }
               , new Object[] {
               BC004U14_A2005ContagemResultadoRequisito_Codigo
               }
               , new Object[] {
               BC004U15_A1895SolicServicoReqNeg_Codigo, BC004U15_A1919Requisito_Codigo
               }
               , new Object[] {
               BC004U16_A1919Requisito_Codigo, BC004U16_A1935Requisito_Ativo, BC004U16_A2001Requisito_Identificador, BC004U16_n2001Requisito_Identificador, BC004U16_A1927Requisito_Titulo, BC004U16_n1927Requisito_Titulo, BC004U16_A1923Requisito_Descricao, BC004U16_n1923Requisito_Descricao, BC004U16_A1690Proposta_Objetivo, BC004U16_A1925Requisito_ReferenciaTecnica,
               BC004U16_n1925Requisito_ReferenciaTecnica, BC004U16_A1926Requisito_Agrupador, BC004U16_n1926Requisito_Agrupador, BC004U16_A1929Requisito_Restricao, BC004U16_n1929Requisito_Restricao, BC004U16_A1931Requisito_Ordem, BC004U16_n1931Requisito_Ordem, BC004U16_A1932Requisito_Pontuacao, BC004U16_n1932Requisito_Pontuacao, BC004U16_A1933Requisito_DataHomologacao,
               BC004U16_n1933Requisito_DataHomologacao, BC004U16_A1934Requisito_Status, BC004U16_A1943Requisito_Cadastro, BC004U16_n1943Requisito_Cadastro, BC004U16_A2002Requisito_Prioridade, BC004U16_n2002Requisito_Prioridade, BC004U16_A1685Proposta_Codigo, BC004U16_n1685Proposta_Codigo, BC004U16_A2049Requisito_TipoReqCod, BC004U16_n2049Requisito_TipoReqCod,
               BC004U16_A1999Requisito_ReqCod, BC004U16_n1999Requisito_ReqCod
               }
            }
         );
         Z1943Requisito_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1943Requisito_Cadastro = false;
         A1943Requisito_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1943Requisito_Cadastro = false;
         i1943Requisito_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1943Requisito_Cadastro = false;
         AV25Pgmname = "Requisito_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E124U2 */
         E124U2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short AV18Pergunta_Codigo ;
      private short GX_JID ;
      private short Z1931Requisito_Ordem ;
      private short A1931Requisito_Ordem ;
      private short Z1934Requisito_Status ;
      private short A1934Requisito_Status ;
      private short Z2002Requisito_Prioridade ;
      private short A2002Requisito_Prioridade ;
      private short Gx_BScreen ;
      private short RcdFound215 ;
      private int trnEnded ;
      private int Z1919Requisito_Codigo ;
      private int A1919Requisito_Codigo ;
      private int AV26GXV1 ;
      private int AV15Insert_Proposta_Codigo ;
      private int AV24Insert_Requisito_TipoReqCod ;
      private int AV22Insert_Requisito_ReqCod ;
      private int A1685Proposta_Codigo ;
      private int Z1685Proposta_Codigo ;
      private int Z2049Requisito_TipoReqCod ;
      private int A2049Requisito_TipoReqCod ;
      private int Z1999Requisito_ReqCod ;
      private int A1999Requisito_ReqCod ;
      private decimal A1932Requisito_Pontuacao ;
      private decimal Z1932Requisito_Pontuacao ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV25Pgmname ;
      private String sMode215 ;
      private DateTime Z1943Requisito_Cadastro ;
      private DateTime A1943Requisito_Cadastro ;
      private DateTime i1943Requisito_Cadastro ;
      private DateTime Z1933Requisito_DataHomologacao ;
      private DateTime A1933Requisito_DataHomologacao ;
      private bool Z1935Requisito_Ativo ;
      private bool A1935Requisito_Ativo ;
      private bool n1943Requisito_Cadastro ;
      private bool n2001Requisito_Identificador ;
      private bool n1927Requisito_Titulo ;
      private bool n1923Requisito_Descricao ;
      private bool n1925Requisito_ReferenciaTecnica ;
      private bool n1926Requisito_Agrupador ;
      private bool n1929Requisito_Restricao ;
      private bool n1931Requisito_Ordem ;
      private bool n1932Requisito_Pontuacao ;
      private bool n1933Requisito_DataHomologacao ;
      private bool n2002Requisito_Prioridade ;
      private bool n1685Proposta_Codigo ;
      private bool n2049Requisito_TipoReqCod ;
      private bool n1999Requisito_ReqCod ;
      private bool Gx_longc ;
      private bool i1935Requisito_Ativo ;
      private String Z1923Requisito_Descricao ;
      private String A1923Requisito_Descricao ;
      private String Z1925Requisito_ReferenciaTecnica ;
      private String A1925Requisito_ReferenciaTecnica ;
      private String Z1929Requisito_Restricao ;
      private String A1929Requisito_Restricao ;
      private String Z1690Proposta_Objetivo ;
      private String A1690Proposta_Objetivo ;
      private String Z2001Requisito_Identificador ;
      private String A2001Requisito_Identificador ;
      private String Z1927Requisito_Titulo ;
      private String A1927Requisito_Titulo ;
      private String Z1926Requisito_Agrupador ;
      private String A1926Requisito_Agrupador ;
      private IGxSession AV10WebSession ;
      private SdtRequisito bcRequisito ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC004U7_A1919Requisito_Codigo ;
      private bool[] BC004U7_A1935Requisito_Ativo ;
      private String[] BC004U7_A2001Requisito_Identificador ;
      private bool[] BC004U7_n2001Requisito_Identificador ;
      private String[] BC004U7_A1927Requisito_Titulo ;
      private bool[] BC004U7_n1927Requisito_Titulo ;
      private String[] BC004U7_A1923Requisito_Descricao ;
      private bool[] BC004U7_n1923Requisito_Descricao ;
      private String[] BC004U7_A1690Proposta_Objetivo ;
      private String[] BC004U7_A1925Requisito_ReferenciaTecnica ;
      private bool[] BC004U7_n1925Requisito_ReferenciaTecnica ;
      private String[] BC004U7_A1926Requisito_Agrupador ;
      private bool[] BC004U7_n1926Requisito_Agrupador ;
      private String[] BC004U7_A1929Requisito_Restricao ;
      private bool[] BC004U7_n1929Requisito_Restricao ;
      private short[] BC004U7_A1931Requisito_Ordem ;
      private bool[] BC004U7_n1931Requisito_Ordem ;
      private decimal[] BC004U7_A1932Requisito_Pontuacao ;
      private bool[] BC004U7_n1932Requisito_Pontuacao ;
      private DateTime[] BC004U7_A1933Requisito_DataHomologacao ;
      private bool[] BC004U7_n1933Requisito_DataHomologacao ;
      private short[] BC004U7_A1934Requisito_Status ;
      private DateTime[] BC004U7_A1943Requisito_Cadastro ;
      private bool[] BC004U7_n1943Requisito_Cadastro ;
      private short[] BC004U7_A2002Requisito_Prioridade ;
      private bool[] BC004U7_n2002Requisito_Prioridade ;
      private int[] BC004U7_A1685Proposta_Codigo ;
      private bool[] BC004U7_n1685Proposta_Codigo ;
      private int[] BC004U7_A2049Requisito_TipoReqCod ;
      private bool[] BC004U7_n2049Requisito_TipoReqCod ;
      private int[] BC004U7_A1999Requisito_ReqCod ;
      private bool[] BC004U7_n1999Requisito_ReqCod ;
      private String[] BC004U4_A1690Proposta_Objetivo ;
      private int[] BC004U5_A2049Requisito_TipoReqCod ;
      private bool[] BC004U5_n2049Requisito_TipoReqCod ;
      private int[] BC004U6_A1999Requisito_ReqCod ;
      private bool[] BC004U6_n1999Requisito_ReqCod ;
      private int[] BC004U8_A1919Requisito_Codigo ;
      private int[] BC004U3_A1919Requisito_Codigo ;
      private bool[] BC004U3_A1935Requisito_Ativo ;
      private String[] BC004U3_A2001Requisito_Identificador ;
      private bool[] BC004U3_n2001Requisito_Identificador ;
      private String[] BC004U3_A1927Requisito_Titulo ;
      private bool[] BC004U3_n1927Requisito_Titulo ;
      private String[] BC004U3_A1923Requisito_Descricao ;
      private bool[] BC004U3_n1923Requisito_Descricao ;
      private String[] BC004U3_A1925Requisito_ReferenciaTecnica ;
      private bool[] BC004U3_n1925Requisito_ReferenciaTecnica ;
      private String[] BC004U3_A1926Requisito_Agrupador ;
      private bool[] BC004U3_n1926Requisito_Agrupador ;
      private String[] BC004U3_A1929Requisito_Restricao ;
      private bool[] BC004U3_n1929Requisito_Restricao ;
      private short[] BC004U3_A1931Requisito_Ordem ;
      private bool[] BC004U3_n1931Requisito_Ordem ;
      private decimal[] BC004U3_A1932Requisito_Pontuacao ;
      private bool[] BC004U3_n1932Requisito_Pontuacao ;
      private DateTime[] BC004U3_A1933Requisito_DataHomologacao ;
      private bool[] BC004U3_n1933Requisito_DataHomologacao ;
      private short[] BC004U3_A1934Requisito_Status ;
      private DateTime[] BC004U3_A1943Requisito_Cadastro ;
      private bool[] BC004U3_n1943Requisito_Cadastro ;
      private short[] BC004U3_A2002Requisito_Prioridade ;
      private bool[] BC004U3_n2002Requisito_Prioridade ;
      private int[] BC004U3_A1685Proposta_Codigo ;
      private bool[] BC004U3_n1685Proposta_Codigo ;
      private int[] BC004U3_A2049Requisito_TipoReqCod ;
      private bool[] BC004U3_n2049Requisito_TipoReqCod ;
      private int[] BC004U3_A1999Requisito_ReqCod ;
      private bool[] BC004U3_n1999Requisito_ReqCod ;
      private int[] BC004U2_A1919Requisito_Codigo ;
      private bool[] BC004U2_A1935Requisito_Ativo ;
      private String[] BC004U2_A2001Requisito_Identificador ;
      private bool[] BC004U2_n2001Requisito_Identificador ;
      private String[] BC004U2_A1927Requisito_Titulo ;
      private bool[] BC004U2_n1927Requisito_Titulo ;
      private String[] BC004U2_A1923Requisito_Descricao ;
      private bool[] BC004U2_n1923Requisito_Descricao ;
      private String[] BC004U2_A1925Requisito_ReferenciaTecnica ;
      private bool[] BC004U2_n1925Requisito_ReferenciaTecnica ;
      private String[] BC004U2_A1926Requisito_Agrupador ;
      private bool[] BC004U2_n1926Requisito_Agrupador ;
      private String[] BC004U2_A1929Requisito_Restricao ;
      private bool[] BC004U2_n1929Requisito_Restricao ;
      private short[] BC004U2_A1931Requisito_Ordem ;
      private bool[] BC004U2_n1931Requisito_Ordem ;
      private decimal[] BC004U2_A1932Requisito_Pontuacao ;
      private bool[] BC004U2_n1932Requisito_Pontuacao ;
      private DateTime[] BC004U2_A1933Requisito_DataHomologacao ;
      private bool[] BC004U2_n1933Requisito_DataHomologacao ;
      private short[] BC004U2_A1934Requisito_Status ;
      private DateTime[] BC004U2_A1943Requisito_Cadastro ;
      private bool[] BC004U2_n1943Requisito_Cadastro ;
      private short[] BC004U2_A2002Requisito_Prioridade ;
      private bool[] BC004U2_n2002Requisito_Prioridade ;
      private int[] BC004U2_A1685Proposta_Codigo ;
      private bool[] BC004U2_n1685Proposta_Codigo ;
      private int[] BC004U2_A2049Requisito_TipoReqCod ;
      private bool[] BC004U2_n2049Requisito_TipoReqCod ;
      private int[] BC004U2_A1999Requisito_ReqCod ;
      private bool[] BC004U2_n1999Requisito_ReqCod ;
      private int[] BC004U9_A1919Requisito_Codigo ;
      private String[] BC004U12_A1690Proposta_Objetivo ;
      private int[] BC004U13_A1999Requisito_ReqCod ;
      private bool[] BC004U13_n1999Requisito_ReqCod ;
      private int[] BC004U14_A2005ContagemResultadoRequisito_Codigo ;
      private long[] BC004U15_A1895SolicServicoReqNeg_Codigo ;
      private int[] BC004U15_A1919Requisito_Codigo ;
      private int[] BC004U16_A1919Requisito_Codigo ;
      private bool[] BC004U16_A1935Requisito_Ativo ;
      private String[] BC004U16_A2001Requisito_Identificador ;
      private bool[] BC004U16_n2001Requisito_Identificador ;
      private String[] BC004U16_A1927Requisito_Titulo ;
      private bool[] BC004U16_n1927Requisito_Titulo ;
      private String[] BC004U16_A1923Requisito_Descricao ;
      private bool[] BC004U16_n1923Requisito_Descricao ;
      private String[] BC004U16_A1690Proposta_Objetivo ;
      private String[] BC004U16_A1925Requisito_ReferenciaTecnica ;
      private bool[] BC004U16_n1925Requisito_ReferenciaTecnica ;
      private String[] BC004U16_A1926Requisito_Agrupador ;
      private bool[] BC004U16_n1926Requisito_Agrupador ;
      private String[] BC004U16_A1929Requisito_Restricao ;
      private bool[] BC004U16_n1929Requisito_Restricao ;
      private short[] BC004U16_A1931Requisito_Ordem ;
      private bool[] BC004U16_n1931Requisito_Ordem ;
      private decimal[] BC004U16_A1932Requisito_Pontuacao ;
      private bool[] BC004U16_n1932Requisito_Pontuacao ;
      private DateTime[] BC004U16_A1933Requisito_DataHomologacao ;
      private bool[] BC004U16_n1933Requisito_DataHomologacao ;
      private short[] BC004U16_A1934Requisito_Status ;
      private DateTime[] BC004U16_A1943Requisito_Cadastro ;
      private bool[] BC004U16_n1943Requisito_Cadastro ;
      private short[] BC004U16_A2002Requisito_Prioridade ;
      private bool[] BC004U16_n2002Requisito_Prioridade ;
      private int[] BC004U16_A1685Proposta_Codigo ;
      private bool[] BC004U16_n1685Proposta_Codigo ;
      private int[] BC004U16_A2049Requisito_TipoReqCod ;
      private bool[] BC004U16_n2049Requisito_TipoReqCod ;
      private int[] BC004U16_A1999Requisito_ReqCod ;
      private bool[] BC004U16_n1999Requisito_ReqCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWindow AV20Window ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class requisito_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004U7 ;
          prmBC004U7 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004U4 ;
          prmBC004U4 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmBC004U5 ;
          prmBC004U5 = new Object[] {
          new Object[] {"@Requisito_TipoReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004U6 ;
          prmBC004U6 = new Object[] {
          new Object[] {"@Requisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004U8 ;
          prmBC004U8 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004U3 ;
          prmBC004U3 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004U2 ;
          prmBC004U2 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004U9 ;
          prmBC004U9 = new Object[] {
          new Object[] {"@Requisito_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Requisito_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Requisito_Titulo",SqlDbType.VarChar,250,0} ,
          new Object[] {"@Requisito_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Requisito_ReferenciaTecnica",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Requisito_Agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@Requisito_Restricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Requisito_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@Requisito_Pontuacao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Requisito_DataHomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Requisito_Status",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Requisito_Cadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Requisito_Prioridade",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0} ,
          new Object[] {"@Requisito_TipoReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Requisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004U10 ;
          prmBC004U10 = new Object[] {
          new Object[] {"@Requisito_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Requisito_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Requisito_Titulo",SqlDbType.VarChar,250,0} ,
          new Object[] {"@Requisito_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Requisito_ReferenciaTecnica",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Requisito_Agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@Requisito_Restricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Requisito_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@Requisito_Pontuacao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Requisito_DataHomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Requisito_Status",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Requisito_Cadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Requisito_Prioridade",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0} ,
          new Object[] {"@Requisito_TipoReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Requisito_ReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004U11 ;
          prmBC004U11 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004U12 ;
          prmBC004U12 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmBC004U13 ;
          prmBC004U13 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004U14 ;
          prmBC004U14 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004U15 ;
          prmBC004U15 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004U16 ;
          prmBC004U16 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004U2", "SELECT [Requisito_Codigo], [Requisito_Ativo], [Requisito_Identificador], [Requisito_Titulo], [Requisito_Descricao], [Requisito_ReferenciaTecnica], [Requisito_Agrupador], [Requisito_Restricao], [Requisito_Ordem], [Requisito_Pontuacao], [Requisito_DataHomologacao], [Requisito_Status], [Requisito_Cadastro], [Requisito_Prioridade], [Proposta_Codigo], [Requisito_TipoReqCod] AS Requisito_TipoReqCod, [Requisito_ReqCod] AS Requisito_ReqCod FROM [Requisito] WITH (UPDLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004U2,1,0,true,false )
             ,new CursorDef("BC004U3", "SELECT [Requisito_Codigo], [Requisito_Ativo], [Requisito_Identificador], [Requisito_Titulo], [Requisito_Descricao], [Requisito_ReferenciaTecnica], [Requisito_Agrupador], [Requisito_Restricao], [Requisito_Ordem], [Requisito_Pontuacao], [Requisito_DataHomologacao], [Requisito_Status], [Requisito_Cadastro], [Requisito_Prioridade], [Proposta_Codigo], [Requisito_TipoReqCod] AS Requisito_TipoReqCod, [Requisito_ReqCod] AS Requisito_ReqCod FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004U3,1,0,true,false )
             ,new CursorDef("BC004U4", "SELECT [Proposta_Objetivo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004U4,1,0,true,false )
             ,new CursorDef("BC004U5", "SELECT [TipoRequisito_Codigo] AS Requisito_TipoReqCod FROM [TipoRequisito] WITH (NOLOCK) WHERE [TipoRequisito_Codigo] = @Requisito_TipoReqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004U5,1,0,true,false )
             ,new CursorDef("BC004U6", "SELECT [Requisito_Codigo] AS Requisito_ReqCod FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_ReqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004U6,1,0,true,false )
             ,new CursorDef("BC004U7", "SELECT TM1.[Requisito_Codigo], TM1.[Requisito_Ativo], TM1.[Requisito_Identificador], TM1.[Requisito_Titulo], TM1.[Requisito_Descricao], T2.[Proposta_Objetivo], TM1.[Requisito_ReferenciaTecnica], TM1.[Requisito_Agrupador], TM1.[Requisito_Restricao], TM1.[Requisito_Ordem], TM1.[Requisito_Pontuacao], TM1.[Requisito_DataHomologacao], TM1.[Requisito_Status], TM1.[Requisito_Cadastro], TM1.[Requisito_Prioridade], TM1.[Proposta_Codigo], TM1.[Requisito_TipoReqCod] AS Requisito_TipoReqCod, TM1.[Requisito_ReqCod] AS Requisito_ReqCod FROM ([Requisito] TM1 WITH (NOLOCK) LEFT JOIN dbo.[Proposta] T2 WITH (NOLOCK) ON T2.[Proposta_Codigo] = TM1.[Proposta_Codigo]) WHERE TM1.[Requisito_Codigo] = @Requisito_Codigo ORDER BY TM1.[Requisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004U7,100,0,true,false )
             ,new CursorDef("BC004U8", "SELECT [Requisito_Codigo] FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004U8,1,0,true,false )
             ,new CursorDef("BC004U9", "INSERT INTO [Requisito]([Requisito_Ativo], [Requisito_Identificador], [Requisito_Titulo], [Requisito_Descricao], [Requisito_ReferenciaTecnica], [Requisito_Agrupador], [Requisito_Restricao], [Requisito_Ordem], [Requisito_Pontuacao], [Requisito_DataHomologacao], [Requisito_Status], [Requisito_Cadastro], [Requisito_Prioridade], [Proposta_Codigo], [Requisito_TipoReqCod], [Requisito_ReqCod]) VALUES(@Requisito_Ativo, @Requisito_Identificador, @Requisito_Titulo, @Requisito_Descricao, @Requisito_ReferenciaTecnica, @Requisito_Agrupador, @Requisito_Restricao, @Requisito_Ordem, @Requisito_Pontuacao, @Requisito_DataHomologacao, @Requisito_Status, @Requisito_Cadastro, @Requisito_Prioridade, @Proposta_Codigo, @Requisito_TipoReqCod, @Requisito_ReqCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC004U9)
             ,new CursorDef("BC004U10", "UPDATE [Requisito] SET [Requisito_Ativo]=@Requisito_Ativo, [Requisito_Identificador]=@Requisito_Identificador, [Requisito_Titulo]=@Requisito_Titulo, [Requisito_Descricao]=@Requisito_Descricao, [Requisito_ReferenciaTecnica]=@Requisito_ReferenciaTecnica, [Requisito_Agrupador]=@Requisito_Agrupador, [Requisito_Restricao]=@Requisito_Restricao, [Requisito_Ordem]=@Requisito_Ordem, [Requisito_Pontuacao]=@Requisito_Pontuacao, [Requisito_DataHomologacao]=@Requisito_DataHomologacao, [Requisito_Status]=@Requisito_Status, [Requisito_Cadastro]=@Requisito_Cadastro, [Requisito_Prioridade]=@Requisito_Prioridade, [Proposta_Codigo]=@Proposta_Codigo, [Requisito_TipoReqCod]=@Requisito_TipoReqCod, [Requisito_ReqCod]=@Requisito_ReqCod  WHERE [Requisito_Codigo] = @Requisito_Codigo", GxErrorMask.GX_NOMASK,prmBC004U10)
             ,new CursorDef("BC004U11", "DELETE FROM [Requisito]  WHERE [Requisito_Codigo] = @Requisito_Codigo", GxErrorMask.GX_NOMASK,prmBC004U11)
             ,new CursorDef("BC004U12", "SELECT [Proposta_Objetivo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004U12,1,0,true,false )
             ,new CursorDef("BC004U13", "SELECT TOP 1 [Requisito_Codigo] AS Requisito_ReqCod FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_ReqCod] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004U13,1,0,true,true )
             ,new CursorDef("BC004U14", "SELECT TOP 1 [ContagemResultadoRequisito_Codigo] FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE [ContagemResultadoRequisito_ReqCod] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004U14,1,0,true,true )
             ,new CursorDef("BC004U15", "SELECT TOP 1 [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004U15,1,0,true,true )
             ,new CursorDef("BC004U16", "SELECT TM1.[Requisito_Codigo], TM1.[Requisito_Ativo], TM1.[Requisito_Identificador], TM1.[Requisito_Titulo], TM1.[Requisito_Descricao], T2.[Proposta_Objetivo], TM1.[Requisito_ReferenciaTecnica], TM1.[Requisito_Agrupador], TM1.[Requisito_Restricao], TM1.[Requisito_Ordem], TM1.[Requisito_Pontuacao], TM1.[Requisito_DataHomologacao], TM1.[Requisito_Status], TM1.[Requisito_Cadastro], TM1.[Requisito_Prioridade], TM1.[Proposta_Codigo], TM1.[Requisito_TipoReqCod] AS Requisito_TipoReqCod, TM1.[Requisito_ReqCod] AS Requisito_ReqCod FROM ([Requisito] TM1 WITH (NOLOCK) LEFT JOIN dbo.[Proposta] T2 WITH (NOLOCK) ON T2.[Proposta_Codigo] = TM1.[Proposta_Codigo]) WHERE TM1.[Requisito_Codigo] = @Requisito_Codigo ORDER BY TM1.[Requisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004U16,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((int[]) buf[29])[0] = rslt.getInt(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((int[]) buf[29])[0] = rslt.getInt(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((short[]) buf[15])[0] = rslt.getShort(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((short[]) buf[24])[0] = rslt.getShort(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((int[]) buf[26])[0] = rslt.getInt(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((int[]) buf[28])[0] = rslt.getInt(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((short[]) buf[15])[0] = rslt.getShort(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((short[]) buf[24])[0] = rslt.getShort(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((int[]) buf[26])[0] = rslt.getInt(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((int[]) buf[28])[0] = rslt.getInt(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (bool)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(10, (DateTime)parms[18]);
                }
                stmt.SetParameter(11, (short)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(12, (DateTime)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[29]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (bool)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(8, (short)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(10, (DateTime)parms[18]);
                }
                stmt.SetParameter(11, (short)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(12, (DateTime)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[29]);
                }
                stmt.SetParameter(17, (int)parms[30]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
