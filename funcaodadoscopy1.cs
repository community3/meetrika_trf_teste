/*
               File: FuncaoDadosCopy1
        Description: Funcao Dados Copy1
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/26/2020 9:3:9.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaodadoscopy1 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"FUNCAODADOS_FUNCAODADOSCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAFUNCAODADOS_FUNCAODADOSCOD5E59( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"FUNCAODADOS_MELHORACOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAFUNCAODADOS_MELHORACOD5E59( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel5"+"_"+"FUNCAODADOS_PF") == 0 )
         {
            A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX5ASAFUNCAODADOS_PF5E59( A368FuncaoDados_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel6"+"_"+"FUNCAODADOS_COMPLEXIDADE") == 0 )
         {
            A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX6ASAFUNCAODADOS_COMPLEXIDADE5E59( A368FuncaoDados_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel7"+"_"+"FUNCAODADOS_RLR") == 0 )
         {
            A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX7ASAFUNCAODADOS_RLR5E59( A368FuncaoDados_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel8"+"_"+"FUNCAODADOS_DER") == 0 )
         {
            A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX8ASAFUNCAODADOS_DER5E59( A368FuncaoDados_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A370FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A370FuncaoDados_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_17") == 0 )
         {
            A391FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n391FuncaoDados_FuncaoDadosCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_17( A391FuncaoDados_FuncaoDadosCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_19") == 0 )
         {
            A404FuncaoDados_FuncaoDadosSisCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n404FuncaoDados_FuncaoDadosSisCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A404FuncaoDados_FuncaoDadosSisCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_19( A404FuncaoDados_FuncaoDadosSisCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_18") == 0 )
         {
            A745FuncaoDados_MelhoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n745FuncaoDados_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_18( A745FuncaoDados_MelhoraCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbFuncaoDados_SistemaTecnica.Name = "FUNCAODADOS_SISTEMATECNICA";
         cmbFuncaoDados_SistemaTecnica.WebTags = "";
         cmbFuncaoDados_SistemaTecnica.addItem("", "(Nenhum)", 0);
         cmbFuncaoDados_SistemaTecnica.addItem("I", "Indicativa", 0);
         cmbFuncaoDados_SistemaTecnica.addItem("E", "Estimada", 0);
         cmbFuncaoDados_SistemaTecnica.addItem("D", "Detalhada", 0);
         if ( cmbFuncaoDados_SistemaTecnica.ItemCount > 0 )
         {
            A705FuncaoDados_SistemaTecnica = cmbFuncaoDados_SistemaTecnica.getValidValue(A705FuncaoDados_SistemaTecnica);
            n705FuncaoDados_SistemaTecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A705FuncaoDados_SistemaTecnica", A705FuncaoDados_SistemaTecnica);
         }
         cmbFuncaoDados_Tipo.Name = "FUNCAODADOS_TIPO";
         cmbFuncaoDados_Tipo.WebTags = "";
         cmbFuncaoDados_Tipo.addItem("", "(Nenhum)", 0);
         cmbFuncaoDados_Tipo.addItem("ALI", "ALI", 0);
         cmbFuncaoDados_Tipo.addItem("AIE", "AIE", 0);
         cmbFuncaoDados_Tipo.addItem("DC", "DC", 0);
         if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
         {
            A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
         }
         cmbFuncaoDados_Complexidade.Name = "FUNCAODADOS_COMPLEXIDADE";
         cmbFuncaoDados_Complexidade.WebTags = "";
         cmbFuncaoDados_Complexidade.addItem("E", "", 0);
         cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
         cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
         cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
         if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
         {
            A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         }
         dynFuncaoDados_FuncaoDadosCod.Name = "FUNCAODADOS_FUNCAODADOSCOD";
         dynFuncaoDados_FuncaoDadosCod.WebTags = "";
         dynFuncaoDados_MelhoraCod.Name = "FUNCAODADOS_MELHORACOD";
         dynFuncaoDados_MelhoraCod.WebTags = "";
         chkFuncaoDados_Contar.Name = "FUNCAODADOS_CONTAR";
         chkFuncaoDados_Contar.WebTags = "";
         chkFuncaoDados_Contar.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncaoDados_Contar_Internalname, "TitleCaption", chkFuncaoDados_Contar.Caption);
         chkFuncaoDados_Contar.CheckedValue = "false";
         cmbFuncaoDados_UpdAoImpBsln.Name = "FUNCAODADOS_UPDAOIMPBSLN";
         cmbFuncaoDados_UpdAoImpBsln.WebTags = "";
         cmbFuncaoDados_UpdAoImpBsln.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         cmbFuncaoDados_UpdAoImpBsln.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         if ( cmbFuncaoDados_UpdAoImpBsln.ItemCount > 0 )
         {
            if ( (false==A1267FuncaoDados_UpdAoImpBsln) )
            {
               A1267FuncaoDados_UpdAoImpBsln = true;
               n1267FuncaoDados_UpdAoImpBsln = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
            }
            A1267FuncaoDados_UpdAoImpBsln = StringUtil.StrToBool( cmbFuncaoDados_UpdAoImpBsln.getValidValue(StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln)));
            n1267FuncaoDados_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
         }
         cmbFuncaoDados_Ativo.Name = "FUNCAODADOS_ATIVO";
         cmbFuncaoDados_Ativo.WebTags = "";
         cmbFuncaoDados_Ativo.addItem("A", "Ativa", 0);
         cmbFuncaoDados_Ativo.addItem("E", "Exclu�da", 0);
         cmbFuncaoDados_Ativo.addItem("R", "Rejeitada", 0);
         if ( cmbFuncaoDados_Ativo.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A394FuncaoDados_Ativo)) )
            {
               A394FuncaoDados_Ativo = "A";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
            }
            A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.getValidValue(A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Funcao Dados Copy1", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public funcaodadoscopy1( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public funcaodadoscopy1( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbFuncaoDados_SistemaTecnica = new GXCombobox();
         cmbFuncaoDados_Tipo = new GXCombobox();
         cmbFuncaoDados_Complexidade = new GXCombobox();
         dynFuncaoDados_FuncaoDadosCod = new GXCombobox();
         dynFuncaoDados_MelhoraCod = new GXCombobox();
         chkFuncaoDados_Contar = new GXCheckbox();
         cmbFuncaoDados_UpdAoImpBsln = new GXCombobox();
         cmbFuncaoDados_Ativo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbFuncaoDados_SistemaTecnica.ItemCount > 0 )
         {
            A705FuncaoDados_SistemaTecnica = cmbFuncaoDados_SistemaTecnica.getValidValue(A705FuncaoDados_SistemaTecnica);
            n705FuncaoDados_SistemaTecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A705FuncaoDados_SistemaTecnica", A705FuncaoDados_SistemaTecnica);
         }
         if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
         {
            A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
         }
         if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
         {
            A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         }
         if ( dynFuncaoDados_FuncaoDadosCod.ItemCount > 0 )
         {
            A391FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( dynFuncaoDados_FuncaoDadosCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0))), "."));
            n391FuncaoDados_FuncaoDadosCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
         }
         if ( dynFuncaoDados_MelhoraCod.ItemCount > 0 )
         {
            A745FuncaoDados_MelhoraCod = (int)(NumberUtil.Val( dynFuncaoDados_MelhoraCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0))), "."));
            n745FuncaoDados_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
         }
         if ( cmbFuncaoDados_UpdAoImpBsln.ItemCount > 0 )
         {
            A1267FuncaoDados_UpdAoImpBsln = StringUtil.StrToBool( cmbFuncaoDados_UpdAoImpBsln.getValidValue(StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln)));
            n1267FuncaoDados_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
         }
         if ( cmbFuncaoDados_Ativo.ItemCount > 0 )
         {
            A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.getValidValue(A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_5E59( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_5E59e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_5E59( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_5E59( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_5E59e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Funcao Dados Copy1", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_FuncaoDadosCopy1.htm");
            wb_table3_28_5E59( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_5E59e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_5E59e( true) ;
         }
         else
         {
            wb_table1_2_5E59e( false) ;
         }
      }

      protected void wb_table3_28_5E59( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_5E59( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_5E59e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 172,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoDadosCopy1.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 173,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoDadosCopy1.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 174,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_5E59e( true) ;
         }
         else
         {
            wb_table3_28_5E59e( false) ;
         }
      }

      protected void wb_table4_34_5E59( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_codigo_Internalname, "C�digo", "", "", lblTextblockfuncaodados_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")), ((edtFuncaoDados_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_codigoaliaie_Internalname, "Ali Aie", "", "", lblTextblockfuncaodados_codigoaliaie_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_CodigoAliAie_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0, ",", "")), ((edtFuncaoDados_CodigoAliAie_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A420FuncaoDados_CodigoAliAie), "ZZZ9")) : context.localUtil.Format( (decimal)(A420FuncaoDados_CodigoAliAie), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_CodigoAliAie_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_CodigoAliAie_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_nome_Internalname, "Dados", "", "", lblTextblockfuncaodados_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoDados_Nome_Internalname, A369FuncaoDados_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", 0, 1, edtFuncaoDados_Nome_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_nome50_Internalname, "Dados_Nome50", "", "", lblTextblockfuncaodados_nome50_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_Nome50_Internalname, A419FuncaoDados_Nome50, StringUtil.RTrim( context.localUtil.Format( A419FuncaoDados_Nome50, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_Nome50_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_Nome50_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_descricao_Internalname, "Descri��o", "", "", lblTextblockfuncaodados_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoDados_Descricao_Internalname, A397FuncaoDados_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, 1, edtFuncaoDados_Descricao_Enabled, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_sistemacod_Internalname, "Sistema", "", "", lblTextblockfuncaodados_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")), ((edtFuncaoDados_SistemaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_SistemaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_SistemaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_sistemades_Internalname, "Sistema", "", "", lblTextblockfuncaodados_sistemades_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_SistemaDes_Internalname, A371FuncaoDados_SistemaDes, StringUtil.RTrim( context.localUtil.Format( A371FuncaoDados_SistemaDes, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_SistemaDes_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_SistemaDes_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_sistemaareacod_Internalname, "Area", "", "", lblTextblockfuncaodados_sistemaareacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_SistemaAreaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0, ",", "")), ((edtFuncaoDados_SistemaAreaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A372FuncaoDados_SistemaAreaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A372FuncaoDados_SistemaAreaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_SistemaAreaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_SistemaAreaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_sistematecnica_Internalname, "T�cnica", "", "", lblTextblockfuncaodados_sistematecnica_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_SistemaTecnica, cmbFuncaoDados_SistemaTecnica_Internalname, StringUtil.RTrim( A705FuncaoDados_SistemaTecnica), 1, cmbFuncaoDados_SistemaTecnica_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbFuncaoDados_SistemaTecnica.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", "", true, "HLP_FuncaoDadosCopy1.htm");
            cmbFuncaoDados_SistemaTecnica.CurrentValue = StringUtil.RTrim( A705FuncaoDados_SistemaTecnica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_SistemaTecnica_Internalname, "Values", (String)(cmbFuncaoDados_SistemaTecnica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tipo_Internalname, "Dados_Tipo", "", "", lblTextblockfuncaodados_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_Tipo, cmbFuncaoDados_Tipo_Internalname, StringUtil.RTrim( A373FuncaoDados_Tipo), 1, cmbFuncaoDados_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbFuncaoDados_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", "", true, "HLP_FuncaoDadosCopy1.htm");
            cmbFuncaoDados_Tipo.CurrentValue = StringUtil.RTrim( A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Tipo_Internalname, "Values", (String)(cmbFuncaoDados_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_der_Internalname, "DER", "", "", lblTextblockfuncaodados_der_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_DER_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ",", "")), ((edtFuncaoDados_DER_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")) : context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_DER_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_DER_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_derimp_Internalname, "Dados_DERImp", "", "", lblTextblockfuncaodados_derimp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_DERImp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1024FuncaoDados_DERImp), 4, 0, ",", "")), ((edtFuncaoDados_DERImp_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1024FuncaoDados_DERImp), "ZZZ9")) : context.localUtil.Format( (decimal)(A1024FuncaoDados_DERImp), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_DERImp_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_DERImp_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_rlr_Internalname, "RLR", "", "", lblTextblockfuncaodados_rlr_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_RLR_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ",", "")), ((edtFuncaoDados_RLR_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")) : context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_RLR_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_RLR_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_raimp_Internalname, "Dados_RAImp", "", "", lblTextblockfuncaodados_raimp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_RAImp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1025FuncaoDados_RAImp), 4, 0, ",", "")), ((edtFuncaoDados_RAImp_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1025FuncaoDados_RAImp), "ZZZ9")) : context.localUtil.Format( (decimal)(A1025FuncaoDados_RAImp), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_RAImp_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_RAImp_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_complexidade_Internalname, "Complexidade", "", "", lblTextblockfuncaodados_complexidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_Complexidade, cmbFuncaoDados_Complexidade_Internalname, StringUtil.RTrim( A376FuncaoDados_Complexidade), 1, cmbFuncaoDados_Complexidade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbFuncaoDados_Complexidade.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", "", true, "HLP_FuncaoDadosCopy1.htm");
            cmbFuncaoDados_Complexidade.CurrentValue = StringUtil.RTrim( A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Complexidade_Internalname, "Values", (String)(cmbFuncaoDados_Complexidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_pf_Internalname, "de Fun��o", "", "", lblTextblockfuncaodados_pf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_PF_Internalname, StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ",", "")), ((edtFuncaoDados_PF_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_PF_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_PF_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_funcaodadoscod_Internalname, "Dados Externa", "", "", lblTextblockfuncaodados_funcaodadoscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynFuncaoDados_FuncaoDadosCod, dynFuncaoDados_FuncaoDadosCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)), 1, dynFuncaoDados_FuncaoDadosCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynFuncaoDados_FuncaoDadosCod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", "", true, "HLP_FuncaoDadosCopy1.htm");
            dynFuncaoDados_FuncaoDadosCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoDados_FuncaoDadosCod_Internalname, "Values", (String)(dynFuncaoDados_FuncaoDadosCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_funcaodadosdes_Internalname, "Dados Des", "", "", lblTextblockfuncaodados_funcaodadosdes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoDados_FuncaoDadosDes_Internalname, A403FuncaoDados_FuncaoDadosDes, "", "", 0, 1, edtFuncaoDados_FuncaoDadosDes_Enabled, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_funcaodadossiscod_Internalname, "Sis Cod", "", "", lblTextblockfuncaodados_funcaodadossiscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_FuncaoDadosSisCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0, ",", "")), ((edtFuncaoDados_FuncaoDadosSisCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_FuncaoDadosSisCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_FuncaoDadosSisCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_funcaodadossisdes_Internalname, "Sis Des", "", "", lblTextblockfuncaodados_funcaodadossisdes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_FuncaoDadosSisDes_Internalname, A405FuncaoDados_FuncaoDadosSisDes, StringUtil.RTrim( context.localUtil.Format( A405FuncaoDados_FuncaoDadosSisDes, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_FuncaoDadosSisDes_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_FuncaoDadosSisDes_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_melhoracod_Internalname, "Dados", "", "", lblTextblockfuncaodados_melhoracod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynFuncaoDados_MelhoraCod, dynFuncaoDados_MelhoraCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)), 1, dynFuncaoDados_MelhoraCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynFuncaoDados_MelhoraCod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", "", true, "HLP_FuncaoDadosCopy1.htm");
            dynFuncaoDados_MelhoraCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoDados_MelhoraCod_Internalname, "Values", (String)(dynFuncaoDados_MelhoraCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_contar_Internalname, "Incluir", "", "", lblTextblockfuncaodados_contar_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkFuncaoDados_Contar_Internalname, StringUtil.BoolToStr( A755FuncaoDados_Contar), "", "", 1, chkFuncaoDados_Contar.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(144, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,144);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_tecnica_Internalname, "T�cnica", "", "", lblTextblockfuncaodados_tecnica_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_Tecnica_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1147FuncaoDados_Tecnica), 1, 0, ",", "")), ((edtFuncaoDados_Tecnica_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1147FuncaoDados_Tecnica), "9")) : context.localUtil.Format( (decimal)(A1147FuncaoDados_Tecnica), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,149);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_Tecnica_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_Tecnica_Enabled, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_observacao_Internalname, "Observa��o", "", "", lblTextblockfuncaodados_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"FUNCAODADOS_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_importada_Internalname, "Dados_Importada", "", "", lblTextblockfuncaodados_importada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtFuncaoDados_Importada_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtFuncaoDados_Importada_Internalname, context.localUtil.Format(A1258FuncaoDados_Importada, "99/99/99"), context.localUtil.Format( A1258FuncaoDados_Importada, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,159);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDados_Importada_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDados_Importada_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoDadosCopy1.htm");
            GxWebStd.gx_bitmap( context, edtFuncaoDados_Importada_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtFuncaoDados_Importada_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_updaoimpbsln_Internalname, "importar Baseline", "", "", lblTextblockfuncaodados_updaoimpbsln_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_UpdAoImpBsln, cmbFuncaoDados_UpdAoImpBsln_Internalname, StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln), 1, cmbFuncaoDados_UpdAoImpBsln_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbFuncaoDados_UpdAoImpBsln.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,164);\"", "", true, "HLP_FuncaoDadosCopy1.htm");
            cmbFuncaoDados_UpdAoImpBsln.CurrentValue = StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_UpdAoImpBsln_Internalname, "Values", (String)(cmbFuncaoDados_UpdAoImpBsln.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_ativo_Internalname, "Status", "", "", lblTextblockfuncaodados_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosCopy1.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDados_Ativo, cmbFuncaoDados_Ativo_Internalname, StringUtil.RTrim( A394FuncaoDados_Ativo), 1, cmbFuncaoDados_Ativo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbFuncaoDados_Ativo.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,169);\"", "", true, "HLP_FuncaoDadosCopy1.htm");
            cmbFuncaoDados_Ativo.CurrentValue = StringUtil.RTrim( A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Ativo_Internalname, "Values", (String)(cmbFuncaoDados_Ativo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_5E59e( true) ;
         }
         else
         {
            wb_table4_34_5E59e( false) ;
         }
      }

      protected void wb_table2_5_5E59( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosCopy1.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_5E59e( true) ;
         }
         else
         {
            wb_table2_5_5E59e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoDados_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoDados_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAODADOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A368FuncaoDados_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               }
               else
               {
                  A368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               }
               A420FuncaoDados_CodigoAliAie = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_CodigoAliAie_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A420FuncaoDados_CodigoAliAie", StringUtil.LTrim( StringUtil.Str( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0)));
               A369FuncaoDados_Nome = cgiGet( edtFuncaoDados_Nome_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
               A419FuncaoDados_Nome50 = cgiGet( edtFuncaoDados_Nome50_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A419FuncaoDados_Nome50", A419FuncaoDados_Nome50);
               A397FuncaoDados_Descricao = cgiGet( edtFuncaoDados_Descricao_Internalname);
               n397FuncaoDados_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
               n397FuncaoDados_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A397FuncaoDados_Descricao)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoDados_SistemaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoDados_SistemaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAODADOS_SISTEMACOD");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoDados_SistemaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A370FuncaoDados_SistemaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
               }
               else
               {
                  A370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_SistemaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
               }
               A371FuncaoDados_SistemaDes = StringUtil.Upper( cgiGet( edtFuncaoDados_SistemaDes_Internalname));
               n371FuncaoDados_SistemaDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A371FuncaoDados_SistemaDes", A371FuncaoDados_SistemaDes);
               A372FuncaoDados_SistemaAreaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_SistemaAreaCod_Internalname), ",", "."));
               n372FuncaoDados_SistemaAreaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A372FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0)));
               cmbFuncaoDados_SistemaTecnica.CurrentValue = cgiGet( cmbFuncaoDados_SistemaTecnica_Internalname);
               A705FuncaoDados_SistemaTecnica = cgiGet( cmbFuncaoDados_SistemaTecnica_Internalname);
               n705FuncaoDados_SistemaTecnica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A705FuncaoDados_SistemaTecnica", A705FuncaoDados_SistemaTecnica);
               cmbFuncaoDados_Tipo.CurrentValue = cgiGet( cmbFuncaoDados_Tipo_Internalname);
               A373FuncaoDados_Tipo = cgiGet( cmbFuncaoDados_Tipo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
               A374FuncaoDados_DER = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_DER_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoDados_DERImp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoDados_DERImp_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAODADOS_DERIMP");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoDados_DERImp_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1024FuncaoDados_DERImp = 0;
                  n1024FuncaoDados_DERImp = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1024FuncaoDados_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1024FuncaoDados_DERImp), 4, 0)));
               }
               else
               {
                  A1024FuncaoDados_DERImp = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_DERImp_Internalname), ",", "."));
                  n1024FuncaoDados_DERImp = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1024FuncaoDados_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1024FuncaoDados_DERImp), 4, 0)));
               }
               n1024FuncaoDados_DERImp = ((0==A1024FuncaoDados_DERImp) ? true : false);
               A375FuncaoDados_RLR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_RLR_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoDados_RAImp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoDados_RAImp_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAODADOS_RAIMP");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoDados_RAImp_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1025FuncaoDados_RAImp = 0;
                  n1025FuncaoDados_RAImp = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1025FuncaoDados_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1025FuncaoDados_RAImp), 4, 0)));
               }
               else
               {
                  A1025FuncaoDados_RAImp = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_RAImp_Internalname), ",", "."));
                  n1025FuncaoDados_RAImp = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1025FuncaoDados_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1025FuncaoDados_RAImp), 4, 0)));
               }
               n1025FuncaoDados_RAImp = ((0==A1025FuncaoDados_RAImp) ? true : false);
               cmbFuncaoDados_Complexidade.CurrentValue = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
               A376FuncaoDados_Complexidade = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
               A377FuncaoDados_PF = context.localUtil.CToN( cgiGet( edtFuncaoDados_PF_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
               dynFuncaoDados_FuncaoDadosCod.CurrentValue = cgiGet( dynFuncaoDados_FuncaoDadosCod_Internalname);
               A391FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( cgiGet( dynFuncaoDados_FuncaoDadosCod_Internalname), "."));
               n391FuncaoDados_FuncaoDadosCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
               n391FuncaoDados_FuncaoDadosCod = ((0==A391FuncaoDados_FuncaoDadosCod) ? true : false);
               A403FuncaoDados_FuncaoDadosDes = cgiGet( edtFuncaoDados_FuncaoDadosDes_Internalname);
               n403FuncaoDados_FuncaoDadosDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A403FuncaoDados_FuncaoDadosDes", A403FuncaoDados_FuncaoDadosDes);
               A404FuncaoDados_FuncaoDadosSisCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_FuncaoDadosSisCod_Internalname), ",", "."));
               n404FuncaoDados_FuncaoDadosSisCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A404FuncaoDados_FuncaoDadosSisCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0)));
               A405FuncaoDados_FuncaoDadosSisDes = StringUtil.Upper( cgiGet( edtFuncaoDados_FuncaoDadosSisDes_Internalname));
               n405FuncaoDados_FuncaoDadosSisDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A405FuncaoDados_FuncaoDadosSisDes", A405FuncaoDados_FuncaoDadosSisDes);
               dynFuncaoDados_MelhoraCod.CurrentValue = cgiGet( dynFuncaoDados_MelhoraCod_Internalname);
               A745FuncaoDados_MelhoraCod = (int)(NumberUtil.Val( cgiGet( dynFuncaoDados_MelhoraCod_Internalname), "."));
               n745FuncaoDados_MelhoraCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
               n745FuncaoDados_MelhoraCod = ((0==A745FuncaoDados_MelhoraCod) ? true : false);
               A755FuncaoDados_Contar = StringUtil.StrToBool( cgiGet( chkFuncaoDados_Contar_Internalname));
               n755FuncaoDados_Contar = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A755FuncaoDados_Contar", A755FuncaoDados_Contar);
               n755FuncaoDados_Contar = ((false==A755FuncaoDados_Contar) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoDados_Tecnica_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoDados_Tecnica_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAODADOS_TECNICA");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoDados_Tecnica_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1147FuncaoDados_Tecnica = 0;
                  n1147FuncaoDados_Tecnica = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1147FuncaoDados_Tecnica", StringUtil.Str( (decimal)(A1147FuncaoDados_Tecnica), 1, 0));
               }
               else
               {
                  A1147FuncaoDados_Tecnica = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_Tecnica_Internalname), ",", "."));
                  n1147FuncaoDados_Tecnica = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1147FuncaoDados_Tecnica", StringUtil.Str( (decimal)(A1147FuncaoDados_Tecnica), 1, 0));
               }
               n1147FuncaoDados_Tecnica = ((0==A1147FuncaoDados_Tecnica) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtFuncaoDados_Importada_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Funcao Dados_Importada"}), 1, "FUNCAODADOS_IMPORTADA");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoDados_Importada_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1258FuncaoDados_Importada = DateTime.MinValue;
                  n1258FuncaoDados_Importada = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1258FuncaoDados_Importada", context.localUtil.Format(A1258FuncaoDados_Importada, "99/99/99"));
               }
               else
               {
                  A1258FuncaoDados_Importada = context.localUtil.CToD( cgiGet( edtFuncaoDados_Importada_Internalname), 2);
                  n1258FuncaoDados_Importada = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1258FuncaoDados_Importada", context.localUtil.Format(A1258FuncaoDados_Importada, "99/99/99"));
               }
               n1258FuncaoDados_Importada = ((DateTime.MinValue==A1258FuncaoDados_Importada) ? true : false);
               cmbFuncaoDados_UpdAoImpBsln.CurrentValue = cgiGet( cmbFuncaoDados_UpdAoImpBsln_Internalname);
               A1267FuncaoDados_UpdAoImpBsln = StringUtil.StrToBool( cgiGet( cmbFuncaoDados_UpdAoImpBsln_Internalname));
               n1267FuncaoDados_UpdAoImpBsln = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
               n1267FuncaoDados_UpdAoImpBsln = ((false==A1267FuncaoDados_UpdAoImpBsln) ? true : false);
               cmbFuncaoDados_Ativo.CurrentValue = cgiGet( cmbFuncaoDados_Ativo_Internalname);
               A394FuncaoDados_Ativo = cgiGet( cmbFuncaoDados_Ativo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
               /* Read saved values. */
               Z368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z368FuncaoDados_Codigo"), ",", "."));
               Z369FuncaoDados_Nome = cgiGet( "Z369FuncaoDados_Nome");
               Z373FuncaoDados_Tipo = cgiGet( "Z373FuncaoDados_Tipo");
               Z1024FuncaoDados_DERImp = (short)(context.localUtil.CToN( cgiGet( "Z1024FuncaoDados_DERImp"), ",", "."));
               n1024FuncaoDados_DERImp = ((0==A1024FuncaoDados_DERImp) ? true : false);
               Z1025FuncaoDados_RAImp = (short)(context.localUtil.CToN( cgiGet( "Z1025FuncaoDados_RAImp"), ",", "."));
               n1025FuncaoDados_RAImp = ((0==A1025FuncaoDados_RAImp) ? true : false);
               Z755FuncaoDados_Contar = StringUtil.StrToBool( cgiGet( "Z755FuncaoDados_Contar"));
               n755FuncaoDados_Contar = ((false==A755FuncaoDados_Contar) ? true : false);
               Z1147FuncaoDados_Tecnica = (short)(context.localUtil.CToN( cgiGet( "Z1147FuncaoDados_Tecnica"), ",", "."));
               n1147FuncaoDados_Tecnica = ((0==A1147FuncaoDados_Tecnica) ? true : false);
               Z1258FuncaoDados_Importada = context.localUtil.CToD( cgiGet( "Z1258FuncaoDados_Importada"), 0);
               n1258FuncaoDados_Importada = ((DateTime.MinValue==A1258FuncaoDados_Importada) ? true : false);
               Z1267FuncaoDados_UpdAoImpBsln = StringUtil.StrToBool( cgiGet( "Z1267FuncaoDados_UpdAoImpBsln"));
               n1267FuncaoDados_UpdAoImpBsln = ((false==A1267FuncaoDados_UpdAoImpBsln) ? true : false);
               Z394FuncaoDados_Ativo = cgiGet( "Z394FuncaoDados_Ativo");
               Z370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z370FuncaoDados_SistemaCod"), ",", "."));
               Z391FuncaoDados_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( "Z391FuncaoDados_FuncaoDadosCod"), ",", "."));
               n391FuncaoDados_FuncaoDadosCod = ((0==A391FuncaoDados_FuncaoDadosCod) ? true : false);
               Z745FuncaoDados_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "Z745FuncaoDados_MelhoraCod"), ",", "."));
               n745FuncaoDados_MelhoraCod = ((0==A745FuncaoDados_MelhoraCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A1245FuncaoDados_Observacao = cgiGet( "FUNCAODADOS_OBSERVACAO");
               n1245FuncaoDados_Observacao = false;
               n1245FuncaoDados_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1245FuncaoDados_Observacao)) ? true : false);
               A402FuncaoDados_FuncaoDadosNom = cgiGet( "FUNCAODADOS_FUNCAODADOSNOM");
               n402FuncaoDados_FuncaoDadosNom = false;
               Gx_mode = cgiGet( "vMODE");
               Funcaodados_observacao_Width = cgiGet( "FUNCAODADOS_OBSERVACAO_Width");
               Funcaodados_observacao_Height = cgiGet( "FUNCAODADOS_OBSERVACAO_Height");
               Funcaodados_observacao_Skin = cgiGet( "FUNCAODADOS_OBSERVACAO_Skin");
               Funcaodados_observacao_Toolbar = cgiGet( "FUNCAODADOS_OBSERVACAO_Toolbar");
               Funcaodados_observacao_Color = (int)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_OBSERVACAO_Color"), ",", "."));
               Funcaodados_observacao_Enabled = StringUtil.StrToBool( cgiGet( "FUNCAODADOS_OBSERVACAO_Enabled"));
               Funcaodados_observacao_Class = cgiGet( "FUNCAODADOS_OBSERVACAO_Class");
               Funcaodados_observacao_Customtoolbar = cgiGet( "FUNCAODADOS_OBSERVACAO_Customtoolbar");
               Funcaodados_observacao_Customconfiguration = cgiGet( "FUNCAODADOS_OBSERVACAO_Customconfiguration");
               Funcaodados_observacao_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "FUNCAODADOS_OBSERVACAO_Toolbarcancollapse"));
               Funcaodados_observacao_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "FUNCAODADOS_OBSERVACAO_Toolbarexpanded"));
               Funcaodados_observacao_Buttonpressedid = cgiGet( "FUNCAODADOS_OBSERVACAO_Buttonpressedid");
               Funcaodados_observacao_Captionvalue = cgiGet( "FUNCAODADOS_OBSERVACAO_Captionvalue");
               Funcaodados_observacao_Captionclass = cgiGet( "FUNCAODADOS_OBSERVACAO_Captionclass");
               Funcaodados_observacao_Captionposition = cgiGet( "FUNCAODADOS_OBSERVACAO_Captionposition");
               Funcaodados_observacao_Coltitle = cgiGet( "FUNCAODADOS_OBSERVACAO_Coltitle");
               Funcaodados_observacao_Coltitlefont = cgiGet( "FUNCAODADOS_OBSERVACAO_Coltitlefont");
               Funcaodados_observacao_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "FUNCAODADOS_OBSERVACAO_Coltitlecolor"), ",", "."));
               Funcaodados_observacao_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "FUNCAODADOS_OBSERVACAO_Usercontroliscolumn"));
               Funcaodados_observacao_Visible = StringUtil.StrToBool( cgiGet( "FUNCAODADOS_OBSERVACAO_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll5E59( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes5E59( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption5E0( )
      {
      }

      protected void ZM5E59( short GX_JID )
      {
         if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z369FuncaoDados_Nome = T005E3_A369FuncaoDados_Nome[0];
               Z373FuncaoDados_Tipo = T005E3_A373FuncaoDados_Tipo[0];
               Z1024FuncaoDados_DERImp = T005E3_A1024FuncaoDados_DERImp[0];
               Z1025FuncaoDados_RAImp = T005E3_A1025FuncaoDados_RAImp[0];
               Z755FuncaoDados_Contar = T005E3_A755FuncaoDados_Contar[0];
               Z1147FuncaoDados_Tecnica = T005E3_A1147FuncaoDados_Tecnica[0];
               Z1258FuncaoDados_Importada = T005E3_A1258FuncaoDados_Importada[0];
               Z1267FuncaoDados_UpdAoImpBsln = T005E3_A1267FuncaoDados_UpdAoImpBsln[0];
               Z394FuncaoDados_Ativo = T005E3_A394FuncaoDados_Ativo[0];
               Z370FuncaoDados_SistemaCod = T005E3_A370FuncaoDados_SistemaCod[0];
               Z391FuncaoDados_FuncaoDadosCod = T005E3_A391FuncaoDados_FuncaoDadosCod[0];
               Z745FuncaoDados_MelhoraCod = T005E3_A745FuncaoDados_MelhoraCod[0];
            }
            else
            {
               Z369FuncaoDados_Nome = A369FuncaoDados_Nome;
               Z373FuncaoDados_Tipo = A373FuncaoDados_Tipo;
               Z1024FuncaoDados_DERImp = A1024FuncaoDados_DERImp;
               Z1025FuncaoDados_RAImp = A1025FuncaoDados_RAImp;
               Z755FuncaoDados_Contar = A755FuncaoDados_Contar;
               Z1147FuncaoDados_Tecnica = A1147FuncaoDados_Tecnica;
               Z1258FuncaoDados_Importada = A1258FuncaoDados_Importada;
               Z1267FuncaoDados_UpdAoImpBsln = A1267FuncaoDados_UpdAoImpBsln;
               Z394FuncaoDados_Ativo = A394FuncaoDados_Ativo;
               Z370FuncaoDados_SistemaCod = A370FuncaoDados_SistemaCod;
               Z391FuncaoDados_FuncaoDadosCod = A391FuncaoDados_FuncaoDadosCod;
               Z745FuncaoDados_MelhoraCod = A745FuncaoDados_MelhoraCod;
            }
         }
         if ( GX_JID == -15 )
         {
            Z368FuncaoDados_Codigo = A368FuncaoDados_Codigo;
            Z369FuncaoDados_Nome = A369FuncaoDados_Nome;
            Z397FuncaoDados_Descricao = A397FuncaoDados_Descricao;
            Z373FuncaoDados_Tipo = A373FuncaoDados_Tipo;
            Z1024FuncaoDados_DERImp = A1024FuncaoDados_DERImp;
            Z1025FuncaoDados_RAImp = A1025FuncaoDados_RAImp;
            Z755FuncaoDados_Contar = A755FuncaoDados_Contar;
            Z1147FuncaoDados_Tecnica = A1147FuncaoDados_Tecnica;
            Z1245FuncaoDados_Observacao = A1245FuncaoDados_Observacao;
            Z1258FuncaoDados_Importada = A1258FuncaoDados_Importada;
            Z1267FuncaoDados_UpdAoImpBsln = A1267FuncaoDados_UpdAoImpBsln;
            Z394FuncaoDados_Ativo = A394FuncaoDados_Ativo;
            Z370FuncaoDados_SistemaCod = A370FuncaoDados_SistemaCod;
            Z391FuncaoDados_FuncaoDadosCod = A391FuncaoDados_FuncaoDadosCod;
            Z745FuncaoDados_MelhoraCod = A745FuncaoDados_MelhoraCod;
            Z371FuncaoDados_SistemaDes = A371FuncaoDados_SistemaDes;
            Z705FuncaoDados_SistemaTecnica = A705FuncaoDados_SistemaTecnica;
            Z372FuncaoDados_SistemaAreaCod = A372FuncaoDados_SistemaAreaCod;
            Z402FuncaoDados_FuncaoDadosNom = A402FuncaoDados_FuncaoDadosNom;
            Z403FuncaoDados_FuncaoDadosDes = A403FuncaoDados_FuncaoDadosDes;
            Z404FuncaoDados_FuncaoDadosSisCod = A404FuncaoDados_FuncaoDadosSisCod;
            Z405FuncaoDados_FuncaoDadosSisDes = A405FuncaoDados_FuncaoDadosSisDes;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAFUNCAODADOS_FUNCAODADOSCOD_html5E59( ) ;
         GXAFUNCAODADOS_MELHORACOD_html5E59( ) ;
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A394FuncaoDados_Ativo)) && ( Gx_BScreen == 0 ) )
         {
            A394FuncaoDados_Ativo = "A";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1267FuncaoDados_UpdAoImpBsln) && ( Gx_BScreen == 0 ) )
         {
            A1267FuncaoDados_UpdAoImpBsln = true;
            n1267FuncaoDados_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A755FuncaoDados_Contar) && ( Gx_BScreen == 0 ) )
         {
            A755FuncaoDados_Contar = true;
            n755FuncaoDados_Contar = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A755FuncaoDados_Contar", A755FuncaoDados_Contar);
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T005E5 */
            pr_default.execute(3, new Object[] {n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod});
            A402FuncaoDados_FuncaoDadosNom = T005E5_A402FuncaoDados_FuncaoDadosNom[0];
            n402FuncaoDados_FuncaoDadosNom = T005E5_n402FuncaoDados_FuncaoDadosNom[0];
            A403FuncaoDados_FuncaoDadosDes = T005E5_A403FuncaoDados_FuncaoDadosDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A403FuncaoDados_FuncaoDadosDes", A403FuncaoDados_FuncaoDadosDes);
            n403FuncaoDados_FuncaoDadosDes = T005E5_n403FuncaoDados_FuncaoDadosDes[0];
            A404FuncaoDados_FuncaoDadosSisCod = T005E5_A404FuncaoDados_FuncaoDadosSisCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A404FuncaoDados_FuncaoDadosSisCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0)));
            n404FuncaoDados_FuncaoDadosSisCod = T005E5_n404FuncaoDados_FuncaoDadosSisCod[0];
            pr_default.close(3);
            /* Using cursor T005E7 */
            pr_default.execute(5, new Object[] {n404FuncaoDados_FuncaoDadosSisCod, A404FuncaoDados_FuncaoDadosSisCod});
            A405FuncaoDados_FuncaoDadosSisDes = T005E7_A405FuncaoDados_FuncaoDadosSisDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A405FuncaoDados_FuncaoDadosSisDes", A405FuncaoDados_FuncaoDadosSisDes);
            n405FuncaoDados_FuncaoDadosSisDes = T005E7_n405FuncaoDados_FuncaoDadosSisDes[0];
            pr_default.close(5);
         }
      }

      protected void Load5E59( )
      {
         /* Using cursor T005E8 */
         pr_default.execute(6, new Object[] {A368FuncaoDados_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound59 = 1;
            A369FuncaoDados_Nome = T005E8_A369FuncaoDados_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
            A397FuncaoDados_Descricao = T005E8_A397FuncaoDados_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
            n397FuncaoDados_Descricao = T005E8_n397FuncaoDados_Descricao[0];
            A371FuncaoDados_SistemaDes = T005E8_A371FuncaoDados_SistemaDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A371FuncaoDados_SistemaDes", A371FuncaoDados_SistemaDes);
            n371FuncaoDados_SistemaDes = T005E8_n371FuncaoDados_SistemaDes[0];
            A705FuncaoDados_SistemaTecnica = T005E8_A705FuncaoDados_SistemaTecnica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A705FuncaoDados_SistemaTecnica", A705FuncaoDados_SistemaTecnica);
            n705FuncaoDados_SistemaTecnica = T005E8_n705FuncaoDados_SistemaTecnica[0];
            A373FuncaoDados_Tipo = T005E8_A373FuncaoDados_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
            A1024FuncaoDados_DERImp = T005E8_A1024FuncaoDados_DERImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1024FuncaoDados_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1024FuncaoDados_DERImp), 4, 0)));
            n1024FuncaoDados_DERImp = T005E8_n1024FuncaoDados_DERImp[0];
            A1025FuncaoDados_RAImp = T005E8_A1025FuncaoDados_RAImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1025FuncaoDados_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1025FuncaoDados_RAImp), 4, 0)));
            n1025FuncaoDados_RAImp = T005E8_n1025FuncaoDados_RAImp[0];
            A402FuncaoDados_FuncaoDadosNom = T005E8_A402FuncaoDados_FuncaoDadosNom[0];
            n402FuncaoDados_FuncaoDadosNom = T005E8_n402FuncaoDados_FuncaoDadosNom[0];
            A403FuncaoDados_FuncaoDadosDes = T005E8_A403FuncaoDados_FuncaoDadosDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A403FuncaoDados_FuncaoDadosDes", A403FuncaoDados_FuncaoDadosDes);
            n403FuncaoDados_FuncaoDadosDes = T005E8_n403FuncaoDados_FuncaoDadosDes[0];
            A405FuncaoDados_FuncaoDadosSisDes = T005E8_A405FuncaoDados_FuncaoDadosSisDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A405FuncaoDados_FuncaoDadosSisDes", A405FuncaoDados_FuncaoDadosSisDes);
            n405FuncaoDados_FuncaoDadosSisDes = T005E8_n405FuncaoDados_FuncaoDadosSisDes[0];
            A755FuncaoDados_Contar = T005E8_A755FuncaoDados_Contar[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A755FuncaoDados_Contar", A755FuncaoDados_Contar);
            n755FuncaoDados_Contar = T005E8_n755FuncaoDados_Contar[0];
            A1147FuncaoDados_Tecnica = T005E8_A1147FuncaoDados_Tecnica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1147FuncaoDados_Tecnica", StringUtil.Str( (decimal)(A1147FuncaoDados_Tecnica), 1, 0));
            n1147FuncaoDados_Tecnica = T005E8_n1147FuncaoDados_Tecnica[0];
            A1245FuncaoDados_Observacao = T005E8_A1245FuncaoDados_Observacao[0];
            n1245FuncaoDados_Observacao = T005E8_n1245FuncaoDados_Observacao[0];
            A1258FuncaoDados_Importada = T005E8_A1258FuncaoDados_Importada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1258FuncaoDados_Importada", context.localUtil.Format(A1258FuncaoDados_Importada, "99/99/99"));
            n1258FuncaoDados_Importada = T005E8_n1258FuncaoDados_Importada[0];
            A1267FuncaoDados_UpdAoImpBsln = T005E8_A1267FuncaoDados_UpdAoImpBsln[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
            n1267FuncaoDados_UpdAoImpBsln = T005E8_n1267FuncaoDados_UpdAoImpBsln[0];
            A394FuncaoDados_Ativo = T005E8_A394FuncaoDados_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
            A370FuncaoDados_SistemaCod = T005E8_A370FuncaoDados_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
            A391FuncaoDados_FuncaoDadosCod = T005E8_A391FuncaoDados_FuncaoDadosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
            n391FuncaoDados_FuncaoDadosCod = T005E8_n391FuncaoDados_FuncaoDadosCod[0];
            A745FuncaoDados_MelhoraCod = T005E8_A745FuncaoDados_MelhoraCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
            n745FuncaoDados_MelhoraCod = T005E8_n745FuncaoDados_MelhoraCod[0];
            A372FuncaoDados_SistemaAreaCod = T005E8_A372FuncaoDados_SistemaAreaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A372FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0)));
            n372FuncaoDados_SistemaAreaCod = T005E8_n372FuncaoDados_SistemaAreaCod[0];
            A404FuncaoDados_FuncaoDadosSisCod = T005E8_A404FuncaoDados_FuncaoDadosSisCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A404FuncaoDados_FuncaoDadosSisCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0)));
            n404FuncaoDados_FuncaoDadosSisCod = T005E8_n404FuncaoDados_FuncaoDadosSisCod[0];
            ZM5E59( -15) ;
         }
         pr_default.close(6);
         OnLoadActions5E59( ) ;
      }

      protected void OnLoadActions5E59( )
      {
         A420FuncaoDados_CodigoAliAie = (short)((T005E3_n391FuncaoDados_FuncaoDadosCod[0] ? A368FuncaoDados_Codigo : A391FuncaoDados_FuncaoDadosCod));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A420FuncaoDados_CodigoAliAie", StringUtil.LTrim( StringUtil.Str( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0)));
         GXt_int1 = (short)(A377FuncaoDados_PF);
         new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A377FuncaoDados_PF = (decimal)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
         GXt_char2 = A376FuncaoDados_Complexidade;
         new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A376FuncaoDados_Complexidade = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         GXt_int1 = A375FuncaoDados_RLR;
         new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A375FuncaoDados_RLR = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
         GXt_int1 = A374FuncaoDados_DER;
         new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A374FuncaoDados_DER = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
         A419FuncaoDados_Nome50 = StringUtil.Substring( A369FuncaoDados_Nome, 1, 50);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A419FuncaoDados_Nome50", A419FuncaoDados_Nome50);
      }

      protected void CheckExtendedTable5E59( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         A420FuncaoDados_CodigoAliAie = (short)((T005E3_n391FuncaoDados_FuncaoDadosCod[0] ? A368FuncaoDados_Codigo : A391FuncaoDados_FuncaoDadosCod));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A420FuncaoDados_CodigoAliAie", StringUtil.LTrim( StringUtil.Str( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0)));
         GXt_int1 = (short)(A377FuncaoDados_PF);
         new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A377FuncaoDados_PF = (decimal)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
         GXt_char2 = A376FuncaoDados_Complexidade;
         new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A376FuncaoDados_Complexidade = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         GXt_int1 = A375FuncaoDados_RLR;
         new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A375FuncaoDados_RLR = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
         GXt_int1 = A374FuncaoDados_DER;
         new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A374FuncaoDados_DER = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
         A419FuncaoDados_Nome50 = StringUtil.Substring( A369FuncaoDados_Nome, 1, 50);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A419FuncaoDados_Nome50", A419FuncaoDados_Nome50);
         /* Using cursor T005E4 */
         pr_default.execute(2, new Object[] {A370FuncaoDados_SistemaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao Dados_Sistema'.", "ForeignKeyNotFound", 1, "FUNCAODADOS_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtFuncaoDados_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A371FuncaoDados_SistemaDes = T005E4_A371FuncaoDados_SistemaDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A371FuncaoDados_SistemaDes", A371FuncaoDados_SistemaDes);
         n371FuncaoDados_SistemaDes = T005E4_n371FuncaoDados_SistemaDes[0];
         A705FuncaoDados_SistemaTecnica = T005E4_A705FuncaoDados_SistemaTecnica[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A705FuncaoDados_SistemaTecnica", A705FuncaoDados_SistemaTecnica);
         n705FuncaoDados_SistemaTecnica = T005E4_n705FuncaoDados_SistemaTecnica[0];
         A372FuncaoDados_SistemaAreaCod = T005E4_A372FuncaoDados_SistemaAreaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A372FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0)));
         n372FuncaoDados_SistemaAreaCod = T005E4_n372FuncaoDados_SistemaAreaCod[0];
         pr_default.close(2);
         if ( ! ( ( StringUtil.StrCmp(A373FuncaoDados_Tipo, "ALI") == 0 ) || ( StringUtil.StrCmp(A373FuncaoDados_Tipo, "AIE") == 0 ) || ( StringUtil.StrCmp(A373FuncaoDados_Tipo, "DC") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Funcao Dados_Tipo fora do intervalo", "OutOfRange", 1, "FUNCAODADOS_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbFuncaoDados_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T005E5 */
         pr_default.execute(3, new Object[] {n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A391FuncaoDados_FuncaoDadosCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Autorelacionamento fun��es de dados externas'.", "ForeignKeyNotFound", 1, "FUNCAODADOS_FUNCAODADOSCOD");
               AnyError = 1;
               GX_FocusControl = dynFuncaoDados_FuncaoDadosCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A402FuncaoDados_FuncaoDadosNom = T005E5_A402FuncaoDados_FuncaoDadosNom[0];
         n402FuncaoDados_FuncaoDadosNom = T005E5_n402FuncaoDados_FuncaoDadosNom[0];
         A403FuncaoDados_FuncaoDadosDes = T005E5_A403FuncaoDados_FuncaoDadosDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A403FuncaoDados_FuncaoDadosDes", A403FuncaoDados_FuncaoDadosDes);
         n403FuncaoDados_FuncaoDadosDes = T005E5_n403FuncaoDados_FuncaoDadosDes[0];
         A404FuncaoDados_FuncaoDadosSisCod = T005E5_A404FuncaoDados_FuncaoDadosSisCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A404FuncaoDados_FuncaoDadosSisCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0)));
         n404FuncaoDados_FuncaoDadosSisCod = T005E5_n404FuncaoDados_FuncaoDadosSisCod[0];
         pr_default.close(3);
         /* Using cursor T005E7 */
         pr_default.execute(5, new Object[] {n404FuncaoDados_FuncaoDadosSisCod, A404FuncaoDados_FuncaoDadosSisCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A404FuncaoDados_FuncaoDadosSisCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A405FuncaoDados_FuncaoDadosSisDes = T005E7_A405FuncaoDados_FuncaoDadosSisDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A405FuncaoDados_FuncaoDadosSisDes", A405FuncaoDados_FuncaoDadosSisDes);
         n405FuncaoDados_FuncaoDadosSisDes = T005E7_n405FuncaoDados_FuncaoDadosSisDes[0];
         pr_default.close(5);
         /* Using cursor T005E6 */
         pr_default.execute(4, new Object[] {n745FuncaoDados_MelhoraCod, A745FuncaoDados_MelhoraCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A745FuncaoDados_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao DAdos_Funcao Dados Melhora'.", "ForeignKeyNotFound", 1, "FUNCAODADOS_MELHORACOD");
               AnyError = 1;
               GX_FocusControl = dynFuncaoDados_MelhoraCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(4);
         if ( ! ( (DateTime.MinValue==A1258FuncaoDados_Importada) || ( A1258FuncaoDados_Importada >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Funcao Dados_Importada fora do intervalo", "OutOfRange", 1, "FUNCAODADOS_IMPORTADA");
            AnyError = 1;
            GX_FocusControl = edtFuncaoDados_Importada_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A394FuncaoDados_Ativo, "A") == 0 ) || ( StringUtil.StrCmp(A394FuncaoDados_Ativo, "E") == 0 ) || ( StringUtil.StrCmp(A394FuncaoDados_Ativo, "R") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Status fora do intervalo", "OutOfRange", 1, "FUNCAODADOS_ATIVO");
            AnyError = 1;
            GX_FocusControl = cmbFuncaoDados_Ativo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors5E59( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(5);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_16( int A370FuncaoDados_SistemaCod )
      {
         /* Using cursor T005E9 */
         pr_default.execute(7, new Object[] {A370FuncaoDados_SistemaCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao Dados_Sistema'.", "ForeignKeyNotFound", 1, "FUNCAODADOS_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtFuncaoDados_SistemaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A371FuncaoDados_SistemaDes = T005E9_A371FuncaoDados_SistemaDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A371FuncaoDados_SistemaDes", A371FuncaoDados_SistemaDes);
         n371FuncaoDados_SistemaDes = T005E9_n371FuncaoDados_SistemaDes[0];
         A705FuncaoDados_SistemaTecnica = T005E9_A705FuncaoDados_SistemaTecnica[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A705FuncaoDados_SistemaTecnica", A705FuncaoDados_SistemaTecnica);
         n705FuncaoDados_SistemaTecnica = T005E9_n705FuncaoDados_SistemaTecnica[0];
         A372FuncaoDados_SistemaAreaCod = T005E9_A372FuncaoDados_SistemaAreaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A372FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0)));
         n372FuncaoDados_SistemaAreaCod = T005E9_n372FuncaoDados_SistemaAreaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A371FuncaoDados_SistemaDes)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A705FuncaoDados_SistemaTecnica))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_17( int A391FuncaoDados_FuncaoDadosCod )
      {
         /* Using cursor T005E10 */
         pr_default.execute(8, new Object[] {n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A391FuncaoDados_FuncaoDadosCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Autorelacionamento fun��es de dados externas'.", "ForeignKeyNotFound", 1, "FUNCAODADOS_FUNCAODADOSCOD");
               AnyError = 1;
               GX_FocusControl = dynFuncaoDados_FuncaoDadosCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A402FuncaoDados_FuncaoDadosNom = T005E10_A402FuncaoDados_FuncaoDadosNom[0];
         n402FuncaoDados_FuncaoDadosNom = T005E10_n402FuncaoDados_FuncaoDadosNom[0];
         A403FuncaoDados_FuncaoDadosDes = T005E10_A403FuncaoDados_FuncaoDadosDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A403FuncaoDados_FuncaoDadosDes", A403FuncaoDados_FuncaoDadosDes);
         n403FuncaoDados_FuncaoDadosDes = T005E10_n403FuncaoDados_FuncaoDadosDes[0];
         A404FuncaoDados_FuncaoDadosSisCod = T005E10_A404FuncaoDados_FuncaoDadosSisCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A404FuncaoDados_FuncaoDadosSisCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0)));
         n404FuncaoDados_FuncaoDadosSisCod = T005E10_n404FuncaoDados_FuncaoDadosSisCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A402FuncaoDados_FuncaoDadosNom)+"\""+","+"\""+GXUtil.EncodeJSConstant( A403FuncaoDados_FuncaoDadosDes)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_19( int A404FuncaoDados_FuncaoDadosSisCod )
      {
         /* Using cursor T005E11 */
         pr_default.execute(9, new Object[] {n404FuncaoDados_FuncaoDadosSisCod, A404FuncaoDados_FuncaoDadosSisCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A404FuncaoDados_FuncaoDadosSisCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A405FuncaoDados_FuncaoDadosSisDes = T005E11_A405FuncaoDados_FuncaoDadosSisDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A405FuncaoDados_FuncaoDadosSisDes", A405FuncaoDados_FuncaoDadosSisDes);
         n405FuncaoDados_FuncaoDadosSisDes = T005E11_n405FuncaoDados_FuncaoDadosSisDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A405FuncaoDados_FuncaoDadosSisDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_18( int A745FuncaoDados_MelhoraCod )
      {
         /* Using cursor T005E12 */
         pr_default.execute(10, new Object[] {n745FuncaoDados_MelhoraCod, A745FuncaoDados_MelhoraCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            if ( ! ( (0==A745FuncaoDados_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao DAdos_Funcao Dados Melhora'.", "ForeignKeyNotFound", 1, "FUNCAODADOS_MELHORACOD");
               AnyError = 1;
               GX_FocusControl = dynFuncaoDados_MelhoraCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void GetKey5E59( )
      {
         /* Using cursor T005E13 */
         pr_default.execute(11, new Object[] {A368FuncaoDados_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound59 = 1;
         }
         else
         {
            RcdFound59 = 0;
         }
         pr_default.close(11);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T005E3 */
         pr_default.execute(1, new Object[] {A368FuncaoDados_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM5E59( 15) ;
            RcdFound59 = 1;
            A368FuncaoDados_Codigo = T005E3_A368FuncaoDados_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A369FuncaoDados_Nome = T005E3_A369FuncaoDados_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
            A397FuncaoDados_Descricao = T005E3_A397FuncaoDados_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
            n397FuncaoDados_Descricao = T005E3_n397FuncaoDados_Descricao[0];
            A373FuncaoDados_Tipo = T005E3_A373FuncaoDados_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
            A1024FuncaoDados_DERImp = T005E3_A1024FuncaoDados_DERImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1024FuncaoDados_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1024FuncaoDados_DERImp), 4, 0)));
            n1024FuncaoDados_DERImp = T005E3_n1024FuncaoDados_DERImp[0];
            A1025FuncaoDados_RAImp = T005E3_A1025FuncaoDados_RAImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1025FuncaoDados_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1025FuncaoDados_RAImp), 4, 0)));
            n1025FuncaoDados_RAImp = T005E3_n1025FuncaoDados_RAImp[0];
            A755FuncaoDados_Contar = T005E3_A755FuncaoDados_Contar[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A755FuncaoDados_Contar", A755FuncaoDados_Contar);
            n755FuncaoDados_Contar = T005E3_n755FuncaoDados_Contar[0];
            A1147FuncaoDados_Tecnica = T005E3_A1147FuncaoDados_Tecnica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1147FuncaoDados_Tecnica", StringUtil.Str( (decimal)(A1147FuncaoDados_Tecnica), 1, 0));
            n1147FuncaoDados_Tecnica = T005E3_n1147FuncaoDados_Tecnica[0];
            A1245FuncaoDados_Observacao = T005E3_A1245FuncaoDados_Observacao[0];
            n1245FuncaoDados_Observacao = T005E3_n1245FuncaoDados_Observacao[0];
            A1258FuncaoDados_Importada = T005E3_A1258FuncaoDados_Importada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1258FuncaoDados_Importada", context.localUtil.Format(A1258FuncaoDados_Importada, "99/99/99"));
            n1258FuncaoDados_Importada = T005E3_n1258FuncaoDados_Importada[0];
            A1267FuncaoDados_UpdAoImpBsln = T005E3_A1267FuncaoDados_UpdAoImpBsln[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
            n1267FuncaoDados_UpdAoImpBsln = T005E3_n1267FuncaoDados_UpdAoImpBsln[0];
            A394FuncaoDados_Ativo = T005E3_A394FuncaoDados_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
            A370FuncaoDados_SistemaCod = T005E3_A370FuncaoDados_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
            A391FuncaoDados_FuncaoDadosCod = T005E3_A391FuncaoDados_FuncaoDadosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
            n391FuncaoDados_FuncaoDadosCod = T005E3_n391FuncaoDados_FuncaoDadosCod[0];
            A745FuncaoDados_MelhoraCod = T005E3_A745FuncaoDados_MelhoraCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
            n745FuncaoDados_MelhoraCod = T005E3_n745FuncaoDados_MelhoraCod[0];
            Z368FuncaoDados_Codigo = A368FuncaoDados_Codigo;
            sMode59 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load5E59( ) ;
            if ( AnyError == 1 )
            {
               RcdFound59 = 0;
               InitializeNonKey5E59( ) ;
            }
            Gx_mode = sMode59;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound59 = 0;
            InitializeNonKey5E59( ) ;
            sMode59 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode59;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey5E59( ) ;
         if ( RcdFound59 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound59 = 0;
         /* Using cursor T005E14 */
         pr_default.execute(12, new Object[] {A368FuncaoDados_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T005E14_A368FuncaoDados_Codigo[0] < A368FuncaoDados_Codigo ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T005E14_A368FuncaoDados_Codigo[0] > A368FuncaoDados_Codigo ) ) )
            {
               A368FuncaoDados_Codigo = T005E14_A368FuncaoDados_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               RcdFound59 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void move_previous( )
      {
         RcdFound59 = 0;
         /* Using cursor T005E15 */
         pr_default.execute(13, new Object[] {A368FuncaoDados_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T005E15_A368FuncaoDados_Codigo[0] > A368FuncaoDados_Codigo ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T005E15_A368FuncaoDados_Codigo[0] < A368FuncaoDados_Codigo ) ) )
            {
               A368FuncaoDados_Codigo = T005E15_A368FuncaoDados_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               RcdFound59 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey5E59( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert5E59( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound59 == 1 )
            {
               if ( A368FuncaoDados_Codigo != Z368FuncaoDados_Codigo )
               {
                  A368FuncaoDados_Codigo = Z368FuncaoDados_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FUNCAODADOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update5E59( ) ;
                  GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A368FuncaoDados_Codigo != Z368FuncaoDados_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert5E59( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FUNCAODADOS_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert5E59( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A368FuncaoDados_Codigo != Z368FuncaoDados_Codigo )
         {
            A368FuncaoDados_Codigo = Z368FuncaoDados_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FUNCAODADOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound59 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "FUNCAODADOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoDados_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtFuncaoDados_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart5E59( ) ;
         if ( RcdFound59 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtFuncaoDados_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd5E59( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound59 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtFuncaoDados_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound59 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtFuncaoDados_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart5E59( ) ;
         if ( RcdFound59 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound59 != 0 )
            {
               ScanNext5E59( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtFuncaoDados_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd5E59( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency5E59( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T005E2 */
            pr_default.execute(0, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncaoDados"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z369FuncaoDados_Nome, T005E2_A369FuncaoDados_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z373FuncaoDados_Tipo, T005E2_A373FuncaoDados_Tipo[0]) != 0 ) || ( Z1024FuncaoDados_DERImp != T005E2_A1024FuncaoDados_DERImp[0] ) || ( Z1025FuncaoDados_RAImp != T005E2_A1025FuncaoDados_RAImp[0] ) || ( Z755FuncaoDados_Contar != T005E2_A755FuncaoDados_Contar[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1147FuncaoDados_Tecnica != T005E2_A1147FuncaoDados_Tecnica[0] ) || ( Z1258FuncaoDados_Importada != T005E2_A1258FuncaoDados_Importada[0] ) || ( Z1267FuncaoDados_UpdAoImpBsln != T005E2_A1267FuncaoDados_UpdAoImpBsln[0] ) || ( StringUtil.StrCmp(Z394FuncaoDados_Ativo, T005E2_A394FuncaoDados_Ativo[0]) != 0 ) || ( Z370FuncaoDados_SistemaCod != T005E2_A370FuncaoDados_SistemaCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z391FuncaoDados_FuncaoDadosCod != T005E2_A391FuncaoDados_FuncaoDadosCod[0] ) || ( Z745FuncaoDados_MelhoraCod != T005E2_A745FuncaoDados_MelhoraCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z369FuncaoDados_Nome, T005E2_A369FuncaoDados_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaodadoscopy1:[seudo value changed for attri]"+"FuncaoDados_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z369FuncaoDados_Nome);
                  GXUtil.WriteLogRaw("Current: ",T005E2_A369FuncaoDados_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z373FuncaoDados_Tipo, T005E2_A373FuncaoDados_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaodadoscopy1:[seudo value changed for attri]"+"FuncaoDados_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z373FuncaoDados_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T005E2_A373FuncaoDados_Tipo[0]);
               }
               if ( Z1024FuncaoDados_DERImp != T005E2_A1024FuncaoDados_DERImp[0] )
               {
                  GXUtil.WriteLog("funcaodadoscopy1:[seudo value changed for attri]"+"FuncaoDados_DERImp");
                  GXUtil.WriteLogRaw("Old: ",Z1024FuncaoDados_DERImp);
                  GXUtil.WriteLogRaw("Current: ",T005E2_A1024FuncaoDados_DERImp[0]);
               }
               if ( Z1025FuncaoDados_RAImp != T005E2_A1025FuncaoDados_RAImp[0] )
               {
                  GXUtil.WriteLog("funcaodadoscopy1:[seudo value changed for attri]"+"FuncaoDados_RAImp");
                  GXUtil.WriteLogRaw("Old: ",Z1025FuncaoDados_RAImp);
                  GXUtil.WriteLogRaw("Current: ",T005E2_A1025FuncaoDados_RAImp[0]);
               }
               if ( Z755FuncaoDados_Contar != T005E2_A755FuncaoDados_Contar[0] )
               {
                  GXUtil.WriteLog("funcaodadoscopy1:[seudo value changed for attri]"+"FuncaoDados_Contar");
                  GXUtil.WriteLogRaw("Old: ",Z755FuncaoDados_Contar);
                  GXUtil.WriteLogRaw("Current: ",T005E2_A755FuncaoDados_Contar[0]);
               }
               if ( Z1147FuncaoDados_Tecnica != T005E2_A1147FuncaoDados_Tecnica[0] )
               {
                  GXUtil.WriteLog("funcaodadoscopy1:[seudo value changed for attri]"+"FuncaoDados_Tecnica");
                  GXUtil.WriteLogRaw("Old: ",Z1147FuncaoDados_Tecnica);
                  GXUtil.WriteLogRaw("Current: ",T005E2_A1147FuncaoDados_Tecnica[0]);
               }
               if ( Z1258FuncaoDados_Importada != T005E2_A1258FuncaoDados_Importada[0] )
               {
                  GXUtil.WriteLog("funcaodadoscopy1:[seudo value changed for attri]"+"FuncaoDados_Importada");
                  GXUtil.WriteLogRaw("Old: ",Z1258FuncaoDados_Importada);
                  GXUtil.WriteLogRaw("Current: ",T005E2_A1258FuncaoDados_Importada[0]);
               }
               if ( Z1267FuncaoDados_UpdAoImpBsln != T005E2_A1267FuncaoDados_UpdAoImpBsln[0] )
               {
                  GXUtil.WriteLog("funcaodadoscopy1:[seudo value changed for attri]"+"FuncaoDados_UpdAoImpBsln");
                  GXUtil.WriteLogRaw("Old: ",Z1267FuncaoDados_UpdAoImpBsln);
                  GXUtil.WriteLogRaw("Current: ",T005E2_A1267FuncaoDados_UpdAoImpBsln[0]);
               }
               if ( StringUtil.StrCmp(Z394FuncaoDados_Ativo, T005E2_A394FuncaoDados_Ativo[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaodadoscopy1:[seudo value changed for attri]"+"FuncaoDados_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z394FuncaoDados_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T005E2_A394FuncaoDados_Ativo[0]);
               }
               if ( Z370FuncaoDados_SistemaCod != T005E2_A370FuncaoDados_SistemaCod[0] )
               {
                  GXUtil.WriteLog("funcaodadoscopy1:[seudo value changed for attri]"+"FuncaoDados_SistemaCod");
                  GXUtil.WriteLogRaw("Old: ",Z370FuncaoDados_SistemaCod);
                  GXUtil.WriteLogRaw("Current: ",T005E2_A370FuncaoDados_SistemaCod[0]);
               }
               if ( Z391FuncaoDados_FuncaoDadosCod != T005E2_A391FuncaoDados_FuncaoDadosCod[0] )
               {
                  GXUtil.WriteLog("funcaodadoscopy1:[seudo value changed for attri]"+"FuncaoDados_FuncaoDadosCod");
                  GXUtil.WriteLogRaw("Old: ",Z391FuncaoDados_FuncaoDadosCod);
                  GXUtil.WriteLogRaw("Current: ",T005E2_A391FuncaoDados_FuncaoDadosCod[0]);
               }
               if ( Z745FuncaoDados_MelhoraCod != T005E2_A745FuncaoDados_MelhoraCod[0] )
               {
                  GXUtil.WriteLog("funcaodadoscopy1:[seudo value changed for attri]"+"FuncaoDados_MelhoraCod");
                  GXUtil.WriteLogRaw("Old: ",Z745FuncaoDados_MelhoraCod);
                  GXUtil.WriteLogRaw("Current: ",T005E2_A745FuncaoDados_MelhoraCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FuncaoDados"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert5E59( )
      {
         BeforeValidate5E59( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable5E59( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM5E59( 0) ;
            CheckOptimisticConcurrency5E59( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm5E59( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert5E59( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005E16 */
                     pr_default.execute(14, new Object[] {A369FuncaoDados_Nome, n397FuncaoDados_Descricao, A397FuncaoDados_Descricao, A373FuncaoDados_Tipo, n1024FuncaoDados_DERImp, A1024FuncaoDados_DERImp, n1025FuncaoDados_RAImp, A1025FuncaoDados_RAImp, n755FuncaoDados_Contar, A755FuncaoDados_Contar, n1147FuncaoDados_Tecnica, A1147FuncaoDados_Tecnica, n1245FuncaoDados_Observacao, A1245FuncaoDados_Observacao, n1258FuncaoDados_Importada, A1258FuncaoDados_Importada, n1267FuncaoDados_UpdAoImpBsln, A1267FuncaoDados_UpdAoImpBsln, A394FuncaoDados_Ativo, A370FuncaoDados_SistemaCod, n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, n745FuncaoDados_MelhoraCod, A745FuncaoDados_MelhoraCod});
                     A368FuncaoDados_Codigo = T005E16_A368FuncaoDados_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption5E0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load5E59( ) ;
            }
            EndLevel5E59( ) ;
         }
         CloseExtendedTableCursors5E59( ) ;
      }

      protected void Update5E59( )
      {
         BeforeValidate5E59( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable5E59( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency5E59( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm5E59( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate5E59( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005E17 */
                     pr_default.execute(15, new Object[] {A369FuncaoDados_Nome, n397FuncaoDados_Descricao, A397FuncaoDados_Descricao, A373FuncaoDados_Tipo, n1024FuncaoDados_DERImp, A1024FuncaoDados_DERImp, n1025FuncaoDados_RAImp, A1025FuncaoDados_RAImp, n755FuncaoDados_Contar, A755FuncaoDados_Contar, n1147FuncaoDados_Tecnica, A1147FuncaoDados_Tecnica, n1245FuncaoDados_Observacao, A1245FuncaoDados_Observacao, n1258FuncaoDados_Importada, A1258FuncaoDados_Importada, n1267FuncaoDados_UpdAoImpBsln, A1267FuncaoDados_UpdAoImpBsln, A394FuncaoDados_Ativo, A370FuncaoDados_SistemaCod, n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, n745FuncaoDados_MelhoraCod, A745FuncaoDados_MelhoraCod, A368FuncaoDados_Codigo});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
                     if ( (pr_default.getStatus(15) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncaoDados"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate5E59( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption5E0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel5E59( ) ;
         }
         CloseExtendedTableCursors5E59( ) ;
      }

      protected void DeferredUpdate5E59( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate5E59( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency5E59( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls5E59( ) ;
            AfterConfirm5E59( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete5E59( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T005E18 */
                  pr_default.execute(16, new Object[] {A368FuncaoDados_Codigo});
                  pr_default.close(16);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound59 == 0 )
                        {
                           InitAll5E59( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption5E0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode59 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel5E59( ) ;
         Gx_mode = sMode59;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls5E59( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_int1 = (short)(A377FuncaoDados_PF);
            new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A377FuncaoDados_PF = (decimal)(GXt_int1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
            GXt_char2 = A376FuncaoDados_Complexidade;
            new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A376FuncaoDados_Complexidade = GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
            GXt_int1 = A375FuncaoDados_RLR;
            new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A375FuncaoDados_RLR = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
            GXt_int1 = A374FuncaoDados_DER;
            new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
            A374FuncaoDados_DER = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
            A419FuncaoDados_Nome50 = StringUtil.Substring( A369FuncaoDados_Nome, 1, 50);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A419FuncaoDados_Nome50", A419FuncaoDados_Nome50);
            /* Using cursor T005E19 */
            pr_default.execute(17, new Object[] {A370FuncaoDados_SistemaCod});
            A371FuncaoDados_SistemaDes = T005E19_A371FuncaoDados_SistemaDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A371FuncaoDados_SistemaDes", A371FuncaoDados_SistemaDes);
            n371FuncaoDados_SistemaDes = T005E19_n371FuncaoDados_SistemaDes[0];
            A705FuncaoDados_SistemaTecnica = T005E19_A705FuncaoDados_SistemaTecnica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A705FuncaoDados_SistemaTecnica", A705FuncaoDados_SistemaTecnica);
            n705FuncaoDados_SistemaTecnica = T005E19_n705FuncaoDados_SistemaTecnica[0];
            A372FuncaoDados_SistemaAreaCod = T005E19_A372FuncaoDados_SistemaAreaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A372FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0)));
            n372FuncaoDados_SistemaAreaCod = T005E19_n372FuncaoDados_SistemaAreaCod[0];
            pr_default.close(17);
            /* Using cursor T005E20 */
            pr_default.execute(18, new Object[] {n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod});
            A402FuncaoDados_FuncaoDadosNom = T005E20_A402FuncaoDados_FuncaoDadosNom[0];
            n402FuncaoDados_FuncaoDadosNom = T005E20_n402FuncaoDados_FuncaoDadosNom[0];
            A403FuncaoDados_FuncaoDadosDes = T005E20_A403FuncaoDados_FuncaoDadosDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A403FuncaoDados_FuncaoDadosDes", A403FuncaoDados_FuncaoDadosDes);
            n403FuncaoDados_FuncaoDadosDes = T005E20_n403FuncaoDados_FuncaoDadosDes[0];
            A404FuncaoDados_FuncaoDadosSisCod = T005E20_A404FuncaoDados_FuncaoDadosSisCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A404FuncaoDados_FuncaoDadosSisCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0)));
            n404FuncaoDados_FuncaoDadosSisCod = T005E20_n404FuncaoDados_FuncaoDadosSisCod[0];
            pr_default.close(18);
            /* Using cursor T005E21 */
            pr_default.execute(19, new Object[] {n404FuncaoDados_FuncaoDadosSisCod, A404FuncaoDados_FuncaoDadosSisCod});
            A405FuncaoDados_FuncaoDadosSisDes = T005E21_A405FuncaoDados_FuncaoDadosSisDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A405FuncaoDados_FuncaoDadosSisDes", A405FuncaoDados_FuncaoDadosSisDes);
            n405FuncaoDados_FuncaoDadosSisDes = T005E21_n405FuncaoDados_FuncaoDadosSisDes[0];
            pr_default.close(19);
            A420FuncaoDados_CodigoAliAie = (short)((T005E3_n391FuncaoDados_FuncaoDadosCod[0] ? A368FuncaoDados_Codigo : A391FuncaoDados_FuncaoDadosCod));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A420FuncaoDados_CodigoAliAie", StringUtil.LTrim( StringUtil.Str( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0)));
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T005E22 */
            pr_default.execute(20, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Grupo L�gico de Dados"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor T005E23 */
            pr_default.execute(21, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Grupo L�gico de Dados"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor T005E24 */
            pr_default.execute(22, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Funcao Dados Pre Importa��o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor T005E25 */
            pr_default.execute(23, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto Melhoria"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor T005E26 */
            pr_default.execute(24, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Atributos das Fun��es de APF"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor T005E27 */
            pr_default.execute(25, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Funcao Dados Tabela"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
         }
      }

      protected void EndLevel5E59( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete5E59( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(17);
            pr_default.close(18);
            pr_default.close(19);
            context.CommitDataStores( "FuncaoDadosCopy1");
            if ( AnyError == 0 )
            {
               ConfirmValues5E0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(17);
            pr_default.close(18);
            pr_default.close(19);
            context.RollbackDataStores( "FuncaoDadosCopy1");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart5E59( )
      {
         /* Scan By routine */
         /* Using cursor T005E28 */
         pr_default.execute(26);
         RcdFound59 = 0;
         if ( (pr_default.getStatus(26) != 101) )
         {
            RcdFound59 = 1;
            A368FuncaoDados_Codigo = T005E28_A368FuncaoDados_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext5E59( )
      {
         /* Scan next routine */
         pr_default.readNext(26);
         RcdFound59 = 0;
         if ( (pr_default.getStatus(26) != 101) )
         {
            RcdFound59 = 1;
            A368FuncaoDados_Codigo = T005E28_A368FuncaoDados_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd5E59( )
      {
         pr_default.close(26);
      }

      protected void AfterConfirm5E59( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert5E59( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate5E59( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete5E59( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete5E59( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate5E59( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes5E59( )
      {
         edtFuncaoDados_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Codigo_Enabled), 5, 0)));
         edtFuncaoDados_CodigoAliAie_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_CodigoAliAie_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_CodigoAliAie_Enabled), 5, 0)));
         edtFuncaoDados_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Nome_Enabled), 5, 0)));
         edtFuncaoDados_Nome50_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Nome50_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Nome50_Enabled), 5, 0)));
         edtFuncaoDados_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Descricao_Enabled), 5, 0)));
         edtFuncaoDados_SistemaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_SistemaCod_Enabled), 5, 0)));
         edtFuncaoDados_SistemaDes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_SistemaDes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_SistemaDes_Enabled), 5, 0)));
         edtFuncaoDados_SistemaAreaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_SistemaAreaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_SistemaAreaCod_Enabled), 5, 0)));
         cmbFuncaoDados_SistemaTecnica.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_SistemaTecnica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoDados_SistemaTecnica.Enabled), 5, 0)));
         cmbFuncaoDados_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoDados_Tipo.Enabled), 5, 0)));
         edtFuncaoDados_DER_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_DER_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_DER_Enabled), 5, 0)));
         edtFuncaoDados_DERImp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_DERImp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_DERImp_Enabled), 5, 0)));
         edtFuncaoDados_RLR_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_RLR_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_RLR_Enabled), 5, 0)));
         edtFuncaoDados_RAImp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_RAImp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_RAImp_Enabled), 5, 0)));
         cmbFuncaoDados_Complexidade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Complexidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoDados_Complexidade.Enabled), 5, 0)));
         edtFuncaoDados_PF_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_PF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_PF_Enabled), 5, 0)));
         dynFuncaoDados_FuncaoDadosCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoDados_FuncaoDadosCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynFuncaoDados_FuncaoDadosCod.Enabled), 5, 0)));
         edtFuncaoDados_FuncaoDadosDes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_FuncaoDadosDes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_FuncaoDadosDes_Enabled), 5, 0)));
         edtFuncaoDados_FuncaoDadosSisCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_FuncaoDadosSisCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_FuncaoDadosSisCod_Enabled), 5, 0)));
         edtFuncaoDados_FuncaoDadosSisDes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_FuncaoDadosSisDes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_FuncaoDadosSisDes_Enabled), 5, 0)));
         dynFuncaoDados_MelhoraCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoDados_MelhoraCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynFuncaoDados_MelhoraCod.Enabled), 5, 0)));
         chkFuncaoDados_Contar.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncaoDados_Contar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkFuncaoDados_Contar.Enabled), 5, 0)));
         edtFuncaoDados_Tecnica_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Tecnica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Tecnica_Enabled), 5, 0)));
         edtFuncaoDados_Importada_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Importada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Importada_Enabled), 5, 0)));
         cmbFuncaoDados_UpdAoImpBsln.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_UpdAoImpBsln_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoDados_UpdAoImpBsln.Enabled), 5, 0)));
         cmbFuncaoDados_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoDados_Ativo.Enabled), 5, 0)));
         Funcaodados_observacao_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Funcaodados_observacao_Internalname, "Enabled", StringUtil.BoolToStr( Funcaodados_observacao_Enabled));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues5E0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020426931195");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaodadoscopy1.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z368FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z369FuncaoDados_Nome", Z369FuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, "Z373FuncaoDados_Tipo", StringUtil.RTrim( Z373FuncaoDados_Tipo));
         GxWebStd.gx_hidden_field( context, "Z1024FuncaoDados_DERImp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1024FuncaoDados_DERImp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1025FuncaoDados_RAImp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1025FuncaoDados_RAImp), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z755FuncaoDados_Contar", Z755FuncaoDados_Contar);
         GxWebStd.gx_hidden_field( context, "Z1147FuncaoDados_Tecnica", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1147FuncaoDados_Tecnica), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1258FuncaoDados_Importada", context.localUtil.DToC( Z1258FuncaoDados_Importada, 0, "/"));
         GxWebStd.gx_boolean_hidden_field( context, "Z1267FuncaoDados_UpdAoImpBsln", Z1267FuncaoDados_UpdAoImpBsln);
         GxWebStd.gx_hidden_field( context, "Z394FuncaoDados_Ativo", StringUtil.RTrim( Z394FuncaoDados_Ativo));
         GxWebStd.gx_hidden_field( context, "Z370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z391FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z745FuncaoDados_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_OBSERVACAO", A1245FuncaoDados_Observacao);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_FUNCAODADOSNOM", A402FuncaoDados_FuncaoDadosNom);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_OBSERVACAO_Enabled", StringUtil.BoolToStr( Funcaodados_observacao_Enabled));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("funcaodadoscopy1.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "FuncaoDadosCopy1" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcao Dados Copy1" ;
      }

      protected void InitializeNonKey5E59( )
      {
         A419FuncaoDados_Nome50 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A419FuncaoDados_Nome50", A419FuncaoDados_Nome50);
         A374FuncaoDados_DER = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
         A375FuncaoDados_RLR = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
         A376FuncaoDados_Complexidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         A377FuncaoDados_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
         A420FuncaoDados_CodigoAliAie = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A420FuncaoDados_CodigoAliAie", StringUtil.LTrim( StringUtil.Str( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0)));
         A369FuncaoDados_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
         A397FuncaoDados_Descricao = "";
         n397FuncaoDados_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
         n397FuncaoDados_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A397FuncaoDados_Descricao)) ? true : false);
         A370FuncaoDados_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
         A371FuncaoDados_SistemaDes = "";
         n371FuncaoDados_SistemaDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A371FuncaoDados_SistemaDes", A371FuncaoDados_SistemaDes);
         A372FuncaoDados_SistemaAreaCod = 0;
         n372FuncaoDados_SistemaAreaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A372FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0)));
         A705FuncaoDados_SistemaTecnica = "";
         n705FuncaoDados_SistemaTecnica = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A705FuncaoDados_SistemaTecnica", A705FuncaoDados_SistemaTecnica);
         A373FuncaoDados_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
         A1024FuncaoDados_DERImp = 0;
         n1024FuncaoDados_DERImp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1024FuncaoDados_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1024FuncaoDados_DERImp), 4, 0)));
         n1024FuncaoDados_DERImp = ((0==A1024FuncaoDados_DERImp) ? true : false);
         A1025FuncaoDados_RAImp = 0;
         n1025FuncaoDados_RAImp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1025FuncaoDados_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1025FuncaoDados_RAImp), 4, 0)));
         n1025FuncaoDados_RAImp = ((0==A1025FuncaoDados_RAImp) ? true : false);
         A391FuncaoDados_FuncaoDadosCod = 0;
         n391FuncaoDados_FuncaoDadosCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
         n391FuncaoDados_FuncaoDadosCod = ((0==A391FuncaoDados_FuncaoDadosCod) ? true : false);
         A402FuncaoDados_FuncaoDadosNom = "";
         n402FuncaoDados_FuncaoDadosNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A402FuncaoDados_FuncaoDadosNom", A402FuncaoDados_FuncaoDadosNom);
         A403FuncaoDados_FuncaoDadosDes = "";
         n403FuncaoDados_FuncaoDadosDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A403FuncaoDados_FuncaoDadosDes", A403FuncaoDados_FuncaoDadosDes);
         A404FuncaoDados_FuncaoDadosSisCod = 0;
         n404FuncaoDados_FuncaoDadosSisCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A404FuncaoDados_FuncaoDadosSisCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0)));
         A405FuncaoDados_FuncaoDadosSisDes = "";
         n405FuncaoDados_FuncaoDadosSisDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A405FuncaoDados_FuncaoDadosSisDes", A405FuncaoDados_FuncaoDadosSisDes);
         A745FuncaoDados_MelhoraCod = 0;
         n745FuncaoDados_MelhoraCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A745FuncaoDados_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0)));
         n745FuncaoDados_MelhoraCod = ((0==A745FuncaoDados_MelhoraCod) ? true : false);
         A1147FuncaoDados_Tecnica = 0;
         n1147FuncaoDados_Tecnica = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1147FuncaoDados_Tecnica", StringUtil.Str( (decimal)(A1147FuncaoDados_Tecnica), 1, 0));
         n1147FuncaoDados_Tecnica = ((0==A1147FuncaoDados_Tecnica) ? true : false);
         A1245FuncaoDados_Observacao = "";
         n1245FuncaoDados_Observacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1245FuncaoDados_Observacao", A1245FuncaoDados_Observacao);
         A1258FuncaoDados_Importada = DateTime.MinValue;
         n1258FuncaoDados_Importada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1258FuncaoDados_Importada", context.localUtil.Format(A1258FuncaoDados_Importada, "99/99/99"));
         n1258FuncaoDados_Importada = ((DateTime.MinValue==A1258FuncaoDados_Importada) ? true : false);
         A755FuncaoDados_Contar = true;
         n755FuncaoDados_Contar = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A755FuncaoDados_Contar", A755FuncaoDados_Contar);
         A1267FuncaoDados_UpdAoImpBsln = true;
         n1267FuncaoDados_UpdAoImpBsln = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
         A394FuncaoDados_Ativo = "A";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
         Z369FuncaoDados_Nome = "";
         Z373FuncaoDados_Tipo = "";
         Z1024FuncaoDados_DERImp = 0;
         Z1025FuncaoDados_RAImp = 0;
         Z755FuncaoDados_Contar = false;
         Z1147FuncaoDados_Tecnica = 0;
         Z1258FuncaoDados_Importada = DateTime.MinValue;
         Z1267FuncaoDados_UpdAoImpBsln = false;
         Z394FuncaoDados_Ativo = "";
         Z370FuncaoDados_SistemaCod = 0;
         Z391FuncaoDados_FuncaoDadosCod = 0;
         Z745FuncaoDados_MelhoraCod = 0;
      }

      protected void InitAll5E59( )
      {
         A368FuncaoDados_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         InitializeNonKey5E59( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A394FuncaoDados_Ativo = i394FuncaoDados_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
         A1267FuncaoDados_UpdAoImpBsln = i1267FuncaoDados_UpdAoImpBsln;
         n1267FuncaoDados_UpdAoImpBsln = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1267FuncaoDados_UpdAoImpBsln", A1267FuncaoDados_UpdAoImpBsln);
         A755FuncaoDados_Contar = i755FuncaoDados_Contar;
         n755FuncaoDados_Contar = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A755FuncaoDados_Contar", A755FuncaoDados_Contar);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?5113033");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042693125");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("funcaodadoscopy1.js", "?202042693125");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockfuncaodados_codigo_Internalname = "TEXTBLOCKFUNCAODADOS_CODIGO";
         edtFuncaoDados_Codigo_Internalname = "FUNCAODADOS_CODIGO";
         lblTextblockfuncaodados_codigoaliaie_Internalname = "TEXTBLOCKFUNCAODADOS_CODIGOALIAIE";
         edtFuncaoDados_CodigoAliAie_Internalname = "FUNCAODADOS_CODIGOALIAIE";
         lblTextblockfuncaodados_nome_Internalname = "TEXTBLOCKFUNCAODADOS_NOME";
         edtFuncaoDados_Nome_Internalname = "FUNCAODADOS_NOME";
         lblTextblockfuncaodados_nome50_Internalname = "TEXTBLOCKFUNCAODADOS_NOME50";
         edtFuncaoDados_Nome50_Internalname = "FUNCAODADOS_NOME50";
         lblTextblockfuncaodados_descricao_Internalname = "TEXTBLOCKFUNCAODADOS_DESCRICAO";
         edtFuncaoDados_Descricao_Internalname = "FUNCAODADOS_DESCRICAO";
         lblTextblockfuncaodados_sistemacod_Internalname = "TEXTBLOCKFUNCAODADOS_SISTEMACOD";
         edtFuncaoDados_SistemaCod_Internalname = "FUNCAODADOS_SISTEMACOD";
         lblTextblockfuncaodados_sistemades_Internalname = "TEXTBLOCKFUNCAODADOS_SISTEMADES";
         edtFuncaoDados_SistemaDes_Internalname = "FUNCAODADOS_SISTEMADES";
         lblTextblockfuncaodados_sistemaareacod_Internalname = "TEXTBLOCKFUNCAODADOS_SISTEMAAREACOD";
         edtFuncaoDados_SistemaAreaCod_Internalname = "FUNCAODADOS_SISTEMAAREACOD";
         lblTextblockfuncaodados_sistematecnica_Internalname = "TEXTBLOCKFUNCAODADOS_SISTEMATECNICA";
         cmbFuncaoDados_SistemaTecnica_Internalname = "FUNCAODADOS_SISTEMATECNICA";
         lblTextblockfuncaodados_tipo_Internalname = "TEXTBLOCKFUNCAODADOS_TIPO";
         cmbFuncaoDados_Tipo_Internalname = "FUNCAODADOS_TIPO";
         lblTextblockfuncaodados_der_Internalname = "TEXTBLOCKFUNCAODADOS_DER";
         edtFuncaoDados_DER_Internalname = "FUNCAODADOS_DER";
         lblTextblockfuncaodados_derimp_Internalname = "TEXTBLOCKFUNCAODADOS_DERIMP";
         edtFuncaoDados_DERImp_Internalname = "FUNCAODADOS_DERIMP";
         lblTextblockfuncaodados_rlr_Internalname = "TEXTBLOCKFUNCAODADOS_RLR";
         edtFuncaoDados_RLR_Internalname = "FUNCAODADOS_RLR";
         lblTextblockfuncaodados_raimp_Internalname = "TEXTBLOCKFUNCAODADOS_RAIMP";
         edtFuncaoDados_RAImp_Internalname = "FUNCAODADOS_RAIMP";
         lblTextblockfuncaodados_complexidade_Internalname = "TEXTBLOCKFUNCAODADOS_COMPLEXIDADE";
         cmbFuncaoDados_Complexidade_Internalname = "FUNCAODADOS_COMPLEXIDADE";
         lblTextblockfuncaodados_pf_Internalname = "TEXTBLOCKFUNCAODADOS_PF";
         edtFuncaoDados_PF_Internalname = "FUNCAODADOS_PF";
         lblTextblockfuncaodados_funcaodadoscod_Internalname = "TEXTBLOCKFUNCAODADOS_FUNCAODADOSCOD";
         dynFuncaoDados_FuncaoDadosCod_Internalname = "FUNCAODADOS_FUNCAODADOSCOD";
         lblTextblockfuncaodados_funcaodadosdes_Internalname = "TEXTBLOCKFUNCAODADOS_FUNCAODADOSDES";
         edtFuncaoDados_FuncaoDadosDes_Internalname = "FUNCAODADOS_FUNCAODADOSDES";
         lblTextblockfuncaodados_funcaodadossiscod_Internalname = "TEXTBLOCKFUNCAODADOS_FUNCAODADOSSISCOD";
         edtFuncaoDados_FuncaoDadosSisCod_Internalname = "FUNCAODADOS_FUNCAODADOSSISCOD";
         lblTextblockfuncaodados_funcaodadossisdes_Internalname = "TEXTBLOCKFUNCAODADOS_FUNCAODADOSSISDES";
         edtFuncaoDados_FuncaoDadosSisDes_Internalname = "FUNCAODADOS_FUNCAODADOSSISDES";
         lblTextblockfuncaodados_melhoracod_Internalname = "TEXTBLOCKFUNCAODADOS_MELHORACOD";
         dynFuncaoDados_MelhoraCod_Internalname = "FUNCAODADOS_MELHORACOD";
         lblTextblockfuncaodados_contar_Internalname = "TEXTBLOCKFUNCAODADOS_CONTAR";
         chkFuncaoDados_Contar_Internalname = "FUNCAODADOS_CONTAR";
         lblTextblockfuncaodados_tecnica_Internalname = "TEXTBLOCKFUNCAODADOS_TECNICA";
         edtFuncaoDados_Tecnica_Internalname = "FUNCAODADOS_TECNICA";
         lblTextblockfuncaodados_observacao_Internalname = "TEXTBLOCKFUNCAODADOS_OBSERVACAO";
         Funcaodados_observacao_Internalname = "FUNCAODADOS_OBSERVACAO";
         lblTextblockfuncaodados_importada_Internalname = "TEXTBLOCKFUNCAODADOS_IMPORTADA";
         edtFuncaoDados_Importada_Internalname = "FUNCAODADOS_IMPORTADA";
         lblTextblockfuncaodados_updaoimpbsln_Internalname = "TEXTBLOCKFUNCAODADOS_UPDAOIMPBSLN";
         cmbFuncaoDados_UpdAoImpBsln_Internalname = "FUNCAODADOS_UPDAOIMPBSLN";
         lblTextblockfuncaodados_ativo_Internalname = "TEXTBLOCKFUNCAODADOS_ATIVO";
         cmbFuncaoDados_Ativo_Internalname = "FUNCAODADOS_ATIVO";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Funcao Dados Copy1";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         cmbFuncaoDados_Ativo_Jsonclick = "";
         cmbFuncaoDados_Ativo.Enabled = 1;
         cmbFuncaoDados_UpdAoImpBsln_Jsonclick = "";
         cmbFuncaoDados_UpdAoImpBsln.Enabled = 1;
         edtFuncaoDados_Importada_Jsonclick = "";
         edtFuncaoDados_Importada_Enabled = 1;
         Funcaodados_observacao_Enabled = Convert.ToBoolean( 1);
         edtFuncaoDados_Tecnica_Jsonclick = "";
         edtFuncaoDados_Tecnica_Enabled = 1;
         chkFuncaoDados_Contar.Enabled = 1;
         dynFuncaoDados_MelhoraCod_Jsonclick = "";
         dynFuncaoDados_MelhoraCod.Enabled = 1;
         edtFuncaoDados_FuncaoDadosSisDes_Jsonclick = "";
         edtFuncaoDados_FuncaoDadosSisDes_Enabled = 0;
         edtFuncaoDados_FuncaoDadosSisCod_Jsonclick = "";
         edtFuncaoDados_FuncaoDadosSisCod_Enabled = 0;
         edtFuncaoDados_FuncaoDadosDes_Enabled = 0;
         dynFuncaoDados_FuncaoDadosCod_Jsonclick = "";
         dynFuncaoDados_FuncaoDadosCod.Enabled = 1;
         edtFuncaoDados_PF_Jsonclick = "";
         edtFuncaoDados_PF_Enabled = 0;
         cmbFuncaoDados_Complexidade_Jsonclick = "";
         cmbFuncaoDados_Complexidade.Enabled = 0;
         edtFuncaoDados_RAImp_Jsonclick = "";
         edtFuncaoDados_RAImp_Enabled = 1;
         edtFuncaoDados_RLR_Jsonclick = "";
         edtFuncaoDados_RLR_Enabled = 0;
         edtFuncaoDados_DERImp_Jsonclick = "";
         edtFuncaoDados_DERImp_Enabled = 1;
         edtFuncaoDados_DER_Jsonclick = "";
         edtFuncaoDados_DER_Enabled = 0;
         cmbFuncaoDados_Tipo_Jsonclick = "";
         cmbFuncaoDados_Tipo.Enabled = 1;
         cmbFuncaoDados_SistemaTecnica_Jsonclick = "";
         cmbFuncaoDados_SistemaTecnica.Enabled = 0;
         edtFuncaoDados_SistemaAreaCod_Jsonclick = "";
         edtFuncaoDados_SistemaAreaCod_Enabled = 0;
         edtFuncaoDados_SistemaDes_Jsonclick = "";
         edtFuncaoDados_SistemaDes_Enabled = 0;
         edtFuncaoDados_SistemaCod_Jsonclick = "";
         edtFuncaoDados_SistemaCod_Enabled = 1;
         edtFuncaoDados_Descricao_Enabled = 1;
         edtFuncaoDados_Nome50_Jsonclick = "";
         edtFuncaoDados_Nome50_Enabled = 0;
         edtFuncaoDados_Nome_Enabled = 1;
         edtFuncaoDados_CodigoAliAie_Jsonclick = "";
         edtFuncaoDados_CodigoAliAie_Enabled = 0;
         edtFuncaoDados_Codigo_Jsonclick = "";
         edtFuncaoDados_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         chkFuncaoDados_Contar.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAFUNCAODADOS_FUNCAODADOSCOD5E59( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAFUNCAODADOS_FUNCAODADOSCOD_data5E59( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAFUNCAODADOS_FUNCAODADOSCOD_html5E59( )
      {
         int gxdynajaxvalue ;
         GXDLAFUNCAODADOS_FUNCAODADOSCOD_data5E59( ) ;
         gxdynajaxindex = 1;
         dynFuncaoDados_FuncaoDadosCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynFuncaoDados_FuncaoDadosCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAFUNCAODADOS_FUNCAODADOSCOD_data5E59( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor T005E29 */
         pr_default.execute(27);
         while ( (pr_default.getStatus(27) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T005E29_A391FuncaoDados_FuncaoDadosCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T005E29_A402FuncaoDados_FuncaoDadosNom[0]);
            pr_default.readNext(27);
         }
         pr_default.close(27);
      }

      protected void GXDLAFUNCAODADOS_MELHORACOD5E59( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAFUNCAODADOS_MELHORACOD_data5E59( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAFUNCAODADOS_MELHORACOD_html5E59( )
      {
         int gxdynajaxvalue ;
         GXDLAFUNCAODADOS_MELHORACOD_data5E59( ) ;
         gxdynajaxindex = 1;
         dynFuncaoDados_MelhoraCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynFuncaoDados_MelhoraCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAFUNCAODADOS_MELHORACOD_data5E59( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor T005E30 */
         pr_default.execute(28);
         while ( (pr_default.getStatus(28) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T005E30_A745FuncaoDados_MelhoraCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T005E30_A369FuncaoDados_Nome[0]);
            pr_default.readNext(28);
         }
         pr_default.close(28);
      }

      protected void GX5ASAFUNCAODADOS_PF5E59( int A368FuncaoDados_Codigo )
      {
         GXt_int1 = (short)(A377FuncaoDados_PF);
         new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A377FuncaoDados_PF = (decimal)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX6ASAFUNCAODADOS_COMPLEXIDADE5E59( int A368FuncaoDados_Codigo )
      {
         GXt_char2 = A376FuncaoDados_Complexidade;
         new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A376FuncaoDados_Complexidade = GXt_char2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A376FuncaoDados_Complexidade))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX7ASAFUNCAODADOS_RLR5E59( int A368FuncaoDados_Codigo )
      {
         GXt_int1 = A375FuncaoDados_RLR;
         new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A375FuncaoDados_RLR = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX8ASAFUNCAODADOS_DER5E59( int A368FuncaoDados_Codigo )
      {
         GXt_int1 = A374FuncaoDados_DER;
         new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
         A374FuncaoDados_DER = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtFuncaoDados_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Funcaodados_codigo( GXCombobox cmbGX_Parm1 ,
                                            int GX_Parm2 ,
                                            String GX_Parm3 ,
                                            String GX_Parm4 ,
                                            GXCombobox cmbGX_Parm5 ,
                                            short GX_Parm6 ,
                                            short GX_Parm7 ,
                                            bool GX_Parm8 ,
                                            short GX_Parm9 ,
                                            String GX_Parm10 ,
                                            DateTime GX_Parm11 ,
                                            GXCombobox cmbGX_Parm12 ,
                                            GXCombobox cmbGX_Parm13 ,
                                            int GX_Parm14 ,
                                            GXCombobox dynGX_Parm15 ,
                                            GXCombobox dynGX_Parm16 ,
                                            decimal GX_Parm17 ,
                                            GXCombobox cmbGX_Parm18 ,
                                            short GX_Parm19 ,
                                            short GX_Parm20 )
      {
         cmbFuncaoDados_SistemaTecnica = cmbGX_Parm1;
         A705FuncaoDados_SistemaTecnica = cmbFuncaoDados_SistemaTecnica.CurrentValue;
         n705FuncaoDados_SistemaTecnica = false;
         cmbFuncaoDados_SistemaTecnica.CurrentValue = A705FuncaoDados_SistemaTecnica;
         A368FuncaoDados_Codigo = GX_Parm2;
         A369FuncaoDados_Nome = GX_Parm3;
         A397FuncaoDados_Descricao = GX_Parm4;
         n397FuncaoDados_Descricao = false;
         cmbFuncaoDados_Tipo = cmbGX_Parm5;
         A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.CurrentValue;
         cmbFuncaoDados_Tipo.CurrentValue = A373FuncaoDados_Tipo;
         A1024FuncaoDados_DERImp = GX_Parm6;
         n1024FuncaoDados_DERImp = false;
         A1025FuncaoDados_RAImp = GX_Parm7;
         n1025FuncaoDados_RAImp = false;
         A755FuncaoDados_Contar = GX_Parm8;
         n755FuncaoDados_Contar = false;
         A1147FuncaoDados_Tecnica = GX_Parm9;
         n1147FuncaoDados_Tecnica = false;
         A1245FuncaoDados_Observacao = GX_Parm10;
         n1245FuncaoDados_Observacao = false;
         A1258FuncaoDados_Importada = GX_Parm11;
         n1258FuncaoDados_Importada = false;
         cmbFuncaoDados_UpdAoImpBsln = cmbGX_Parm12;
         A1267FuncaoDados_UpdAoImpBsln = StringUtil.StrToBool( cmbFuncaoDados_UpdAoImpBsln.CurrentValue);
         n1267FuncaoDados_UpdAoImpBsln = false;
         cmbFuncaoDados_UpdAoImpBsln.CurrentValue = StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln);
         cmbFuncaoDados_Ativo = cmbGX_Parm13;
         A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.CurrentValue;
         cmbFuncaoDados_Ativo.CurrentValue = A394FuncaoDados_Ativo;
         A370FuncaoDados_SistemaCod = GX_Parm14;
         dynFuncaoDados_FuncaoDadosCod = dynGX_Parm15;
         A391FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( dynFuncaoDados_FuncaoDadosCod.CurrentValue, "."));
         n391FuncaoDados_FuncaoDadosCod = false;
         dynFuncaoDados_MelhoraCod = dynGX_Parm16;
         A745FuncaoDados_MelhoraCod = (int)(NumberUtil.Val( dynFuncaoDados_MelhoraCod.CurrentValue, "."));
         n745FuncaoDados_MelhoraCod = false;
         A377FuncaoDados_PF = GX_Parm17;
         cmbFuncaoDados_Complexidade = cmbGX_Parm18;
         A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.CurrentValue;
         cmbFuncaoDados_Complexidade.CurrentValue = A376FuncaoDados_Complexidade;
         A375FuncaoDados_RLR = GX_Parm19;
         A374FuncaoDados_DER = GX_Parm20;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         GXt_int1 = (short)(A377FuncaoDados_PF);
         new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int1) ;
         A377FuncaoDados_PF = (decimal)(GXt_int1);
         GXt_char2 = A376FuncaoDados_Complexidade;
         new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
         A376FuncaoDados_Complexidade = GXt_char2;
         cmbFuncaoDados_Complexidade.CurrentValue = A376FuncaoDados_Complexidade;
         GXt_int1 = A375FuncaoDados_RLR;
         new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
         A375FuncaoDados_RLR = GXt_int1;
         GXt_int1 = A374FuncaoDados_DER;
         new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
         A374FuncaoDados_DER = GXt_int1;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A371FuncaoDados_SistemaDes = "";
            n371FuncaoDados_SistemaDes = false;
            A705FuncaoDados_SistemaTecnica = "";
            n705FuncaoDados_SistemaTecnica = false;
            cmbFuncaoDados_SistemaTecnica.CurrentValue = A705FuncaoDados_SistemaTecnica;
            A372FuncaoDados_SistemaAreaCod = 0;
            n372FuncaoDados_SistemaAreaCod = false;
            A402FuncaoDados_FuncaoDadosNom = "";
            n402FuncaoDados_FuncaoDadosNom = false;
            A403FuncaoDados_FuncaoDadosDes = "";
            n403FuncaoDados_FuncaoDadosDes = false;
            A404FuncaoDados_FuncaoDadosSisCod = 0;
            n404FuncaoDados_FuncaoDadosSisCod = false;
            A405FuncaoDados_FuncaoDadosSisDes = "";
            n405FuncaoDados_FuncaoDadosSisDes = false;
         }
         GXAFUNCAODADOS_FUNCAODADOSCOD_html5E59( ) ;
         dynFuncaoDados_FuncaoDadosCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0));
         GXAFUNCAODADOS_MELHORACOD_html5E59( ) ;
         dynFuncaoDados_MelhoraCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0));
         isValidOutput.Add(A369FuncaoDados_Nome);
         isValidOutput.Add(A397FuncaoDados_Descricao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ".", "")));
         cmbFuncaoDados_Tipo.CurrentValue = A373FuncaoDados_Tipo;
         isValidOutput.Add(cmbFuncaoDados_Tipo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1024FuncaoDados_DERImp), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1025FuncaoDados_RAImp), 4, 0, ".", "")));
         if ( dynFuncaoDados_FuncaoDadosCod.ItemCount > 0 )
         {
            A391FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( dynFuncaoDados_FuncaoDadosCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0))), "."));
            n391FuncaoDados_FuncaoDadosCod = false;
         }
         dynFuncaoDados_FuncaoDadosCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0));
         isValidOutput.Add(dynFuncaoDados_FuncaoDadosCod);
         if ( dynFuncaoDados_MelhoraCod.ItemCount > 0 )
         {
            A745FuncaoDados_MelhoraCod = (int)(NumberUtil.Val( dynFuncaoDados_MelhoraCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0))), "."));
            n745FuncaoDados_MelhoraCod = false;
         }
         dynFuncaoDados_MelhoraCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A745FuncaoDados_MelhoraCod), 6, 0));
         isValidOutput.Add(dynFuncaoDados_MelhoraCod);
         isValidOutput.Add(A755FuncaoDados_Contar);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1147FuncaoDados_Tecnica), 1, 0, ".", "")));
         isValidOutput.Add(A1245FuncaoDados_Observacao);
         isValidOutput.Add(context.localUtil.Format(A1258FuncaoDados_Importada, "99/99/99"));
         cmbFuncaoDados_UpdAoImpBsln.CurrentValue = StringUtil.BoolToStr( A1267FuncaoDados_UpdAoImpBsln);
         isValidOutput.Add(cmbFuncaoDados_UpdAoImpBsln);
         cmbFuncaoDados_Ativo.CurrentValue = A394FuncaoDados_Ativo;
         isValidOutput.Add(cmbFuncaoDados_Ativo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")));
         cmbFuncaoDados_Complexidade.CurrentValue = A376FuncaoDados_Complexidade;
         isValidOutput.Add(cmbFuncaoDados_Complexidade);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")));
         isValidOutput.Add(A419FuncaoDados_Nome50);
         isValidOutput.Add(A371FuncaoDados_SistemaDes);
         cmbFuncaoDados_SistemaTecnica.CurrentValue = A705FuncaoDados_SistemaTecnica;
         isValidOutput.Add(cmbFuncaoDados_SistemaTecnica);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0, ".", "")));
         isValidOutput.Add(A402FuncaoDados_FuncaoDadosNom);
         isValidOutput.Add(A403FuncaoDados_FuncaoDadosDes);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0, ".", "")));
         isValidOutput.Add(A405FuncaoDados_FuncaoDadosSisDes);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z368FuncaoDados_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(Z369FuncaoDados_Nome);
         isValidOutput.Add(Z397FuncaoDados_Descricao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z373FuncaoDados_Tipo));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1024FuncaoDados_DERImp), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1025FuncaoDados_RAImp), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z391FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z745FuncaoDados_MelhoraCod), 6, 0, ",", "")));
         isValidOutput.Add(Z755FuncaoDados_Contar);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1147FuncaoDados_Tecnica), 1, 0, ",", "")));
         isValidOutput.Add(Z1245FuncaoDados_Observacao);
         isValidOutput.Add(context.localUtil.DToC( Z1258FuncaoDados_Importada, 0, "/"));
         isValidOutput.Add(Z1267FuncaoDados_UpdAoImpBsln);
         isValidOutput.Add(StringUtil.RTrim( Z394FuncaoDados_Ativo));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z420FuncaoDados_CodigoAliAie), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z377FuncaoDados_PF, 14, 5, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Z376FuncaoDados_Complexidade));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z375FuncaoDados_RLR), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z374FuncaoDados_DER), 4, 0, ".", "")));
         isValidOutput.Add(Z419FuncaoDados_Nome50);
         isValidOutput.Add(Z371FuncaoDados_SistemaDes);
         isValidOutput.Add(StringUtil.RTrim( Z705FuncaoDados_SistemaTecnica));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z372FuncaoDados_SistemaAreaCod), 6, 0, ".", "")));
         isValidOutput.Add(Z402FuncaoDados_FuncaoDadosNom);
         isValidOutput.Add(Z403FuncaoDados_FuncaoDadosDes);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z404FuncaoDados_FuncaoDadosSisCod), 6, 0, ".", "")));
         isValidOutput.Add(Z405FuncaoDados_FuncaoDadosSisDes);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcaodados_sistemacod( int GX_Parm1 ,
                                                String GX_Parm2 ,
                                                GXCombobox cmbGX_Parm3 ,
                                                int GX_Parm4 )
      {
         A370FuncaoDados_SistemaCod = GX_Parm1;
         A371FuncaoDados_SistemaDes = GX_Parm2;
         n371FuncaoDados_SistemaDes = false;
         cmbFuncaoDados_SistemaTecnica = cmbGX_Parm3;
         A705FuncaoDados_SistemaTecnica = cmbFuncaoDados_SistemaTecnica.CurrentValue;
         n705FuncaoDados_SistemaTecnica = false;
         cmbFuncaoDados_SistemaTecnica.CurrentValue = A705FuncaoDados_SistemaTecnica;
         A372FuncaoDados_SistemaAreaCod = GX_Parm4;
         n372FuncaoDados_SistemaAreaCod = false;
         /* Using cursor T005E31 */
         pr_default.execute(29, new Object[] {A370FuncaoDados_SistemaCod});
         if ( (pr_default.getStatus(29) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao Dados_Sistema'.", "ForeignKeyNotFound", 1, "FUNCAODADOS_SISTEMACOD");
            AnyError = 1;
            GX_FocusControl = edtFuncaoDados_SistemaCod_Internalname;
         }
         A371FuncaoDados_SistemaDes = T005E31_A371FuncaoDados_SistemaDes[0];
         n371FuncaoDados_SistemaDes = T005E31_n371FuncaoDados_SistemaDes[0];
         A705FuncaoDados_SistemaTecnica = T005E31_A705FuncaoDados_SistemaTecnica[0];
         cmbFuncaoDados_SistemaTecnica.CurrentValue = A705FuncaoDados_SistemaTecnica;
         n705FuncaoDados_SistemaTecnica = T005E31_n705FuncaoDados_SistemaTecnica[0];
         A372FuncaoDados_SistemaAreaCod = T005E31_A372FuncaoDados_SistemaAreaCod[0];
         n372FuncaoDados_SistemaAreaCod = T005E31_n372FuncaoDados_SistemaAreaCod[0];
         pr_default.close(29);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A371FuncaoDados_SistemaDes = "";
            n371FuncaoDados_SistemaDes = false;
            A705FuncaoDados_SistemaTecnica = "";
            n705FuncaoDados_SistemaTecnica = false;
            cmbFuncaoDados_SistemaTecnica.CurrentValue = A705FuncaoDados_SistemaTecnica;
            A372FuncaoDados_SistemaAreaCod = 0;
            n372FuncaoDados_SistemaAreaCod = false;
         }
         isValidOutput.Add(A371FuncaoDados_SistemaDes);
         cmbFuncaoDados_SistemaTecnica.CurrentValue = A705FuncaoDados_SistemaTecnica;
         isValidOutput.Add(cmbFuncaoDados_SistemaTecnica);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A372FuncaoDados_SistemaAreaCod), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcaodados_funcaodadoscod( GXCombobox dynGX_Parm1 ,
                                                    int GX_Parm2 ,
                                                    int GX_Parm3 ,
                                                    String GX_Parm4 ,
                                                    String GX_Parm5 ,
                                                    String GX_Parm6 ,
                                                    short GX_Parm7 )
      {
         dynFuncaoDados_FuncaoDadosCod = dynGX_Parm1;
         A391FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( dynFuncaoDados_FuncaoDadosCod.CurrentValue, "."));
         n391FuncaoDados_FuncaoDadosCod = false;
         A404FuncaoDados_FuncaoDadosSisCod = GX_Parm2;
         n404FuncaoDados_FuncaoDadosSisCod = false;
         A368FuncaoDados_Codigo = GX_Parm3;
         A402FuncaoDados_FuncaoDadosNom = GX_Parm4;
         n402FuncaoDados_FuncaoDadosNom = false;
         A403FuncaoDados_FuncaoDadosDes = GX_Parm5;
         n403FuncaoDados_FuncaoDadosDes = false;
         A405FuncaoDados_FuncaoDadosSisDes = GX_Parm6;
         n405FuncaoDados_FuncaoDadosSisDes = false;
         A420FuncaoDados_CodigoAliAie = GX_Parm7;
         /* Using cursor T005E32 */
         pr_default.execute(30, new Object[] {n391FuncaoDados_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod});
         if ( (pr_default.getStatus(30) == 101) )
         {
            if ( ! ( (0==A391FuncaoDados_FuncaoDadosCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Autorelacionamento fun��es de dados externas'.", "ForeignKeyNotFound", 1, "FUNCAODADOS_FUNCAODADOSCOD");
               AnyError = 1;
               GX_FocusControl = dynFuncaoDados_FuncaoDadosCod_Internalname;
            }
         }
         A402FuncaoDados_FuncaoDadosNom = T005E32_A402FuncaoDados_FuncaoDadosNom[0];
         n402FuncaoDados_FuncaoDadosNom = T005E32_n402FuncaoDados_FuncaoDadosNom[0];
         A403FuncaoDados_FuncaoDadosDes = T005E32_A403FuncaoDados_FuncaoDadosDes[0];
         n403FuncaoDados_FuncaoDadosDes = T005E32_n403FuncaoDados_FuncaoDadosDes[0];
         A404FuncaoDados_FuncaoDadosSisCod = T005E32_A404FuncaoDados_FuncaoDadosSisCod[0];
         n404FuncaoDados_FuncaoDadosSisCod = T005E32_n404FuncaoDados_FuncaoDadosSisCod[0];
         pr_default.close(30);
         /* Using cursor T005E33 */
         pr_default.execute(31, new Object[] {n404FuncaoDados_FuncaoDadosSisCod, A404FuncaoDados_FuncaoDadosSisCod});
         if ( (pr_default.getStatus(31) == 101) )
         {
            if ( ! ( (0==A404FuncaoDados_FuncaoDadosSisCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A405FuncaoDados_FuncaoDadosSisDes = T005E33_A405FuncaoDados_FuncaoDadosSisDes[0];
         n405FuncaoDados_FuncaoDadosSisDes = T005E33_n405FuncaoDados_FuncaoDadosSisDes[0];
         pr_default.close(31);
         A420FuncaoDados_CodigoAliAie = (short)((T005E3_n391FuncaoDados_FuncaoDadosCod[0] ? A368FuncaoDados_Codigo : A391FuncaoDados_FuncaoDadosCod));
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A402FuncaoDados_FuncaoDadosNom = "";
            n402FuncaoDados_FuncaoDadosNom = false;
            A403FuncaoDados_FuncaoDadosDes = "";
            n403FuncaoDados_FuncaoDadosDes = false;
            A404FuncaoDados_FuncaoDadosSisCod = 0;
            n404FuncaoDados_FuncaoDadosSisCod = false;
            A405FuncaoDados_FuncaoDadosSisDes = "";
            n405FuncaoDados_FuncaoDadosSisDes = false;
         }
         isValidOutput.Add(A402FuncaoDados_FuncaoDadosNom);
         isValidOutput.Add(A403FuncaoDados_FuncaoDadosDes);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A404FuncaoDados_FuncaoDadosSisCod), 6, 0, ".", "")));
         isValidOutput.Add(A405FuncaoDados_FuncaoDadosSisDes);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A420FuncaoDados_CodigoAliAie), 4, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcaodados_melhoracod( GXCombobox dynGX_Parm1 )
      {
         dynFuncaoDados_MelhoraCod = dynGX_Parm1;
         A745FuncaoDados_MelhoraCod = (int)(NumberUtil.Val( dynFuncaoDados_MelhoraCod.CurrentValue, "."));
         n745FuncaoDados_MelhoraCod = false;
         /* Using cursor T005E34 */
         pr_default.execute(32, new Object[] {n745FuncaoDados_MelhoraCod, A745FuncaoDados_MelhoraCod});
         if ( (pr_default.getStatus(32) == 101) )
         {
            if ( ! ( (0==A745FuncaoDados_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao DAdos_Funcao Dados Melhora'.", "ForeignKeyNotFound", 1, "FUNCAODADOS_MELHORACOD");
               AnyError = 1;
               GX_FocusControl = dynFuncaoDados_MelhoraCod_Internalname;
            }
         }
         pr_default.close(32);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(29);
         pr_default.close(17);
         pr_default.close(30);
         pr_default.close(18);
         pr_default.close(32);
         pr_default.close(31);
         pr_default.close(19);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z369FuncaoDados_Nome = "";
         Z373FuncaoDados_Tipo = "";
         Z1258FuncaoDados_Importada = DateTime.MinValue;
         Z394FuncaoDados_Ativo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A705FuncaoDados_SistemaTecnica = "";
         A373FuncaoDados_Tipo = "";
         A376FuncaoDados_Complexidade = "";
         A394FuncaoDados_Ativo = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockfuncaodados_codigo_Jsonclick = "";
         lblTextblockfuncaodados_codigoaliaie_Jsonclick = "";
         lblTextblockfuncaodados_nome_Jsonclick = "";
         A369FuncaoDados_Nome = "";
         lblTextblockfuncaodados_nome50_Jsonclick = "";
         A419FuncaoDados_Nome50 = "";
         lblTextblockfuncaodados_descricao_Jsonclick = "";
         A397FuncaoDados_Descricao = "";
         lblTextblockfuncaodados_sistemacod_Jsonclick = "";
         lblTextblockfuncaodados_sistemades_Jsonclick = "";
         A371FuncaoDados_SistemaDes = "";
         lblTextblockfuncaodados_sistemaareacod_Jsonclick = "";
         lblTextblockfuncaodados_sistematecnica_Jsonclick = "";
         lblTextblockfuncaodados_tipo_Jsonclick = "";
         lblTextblockfuncaodados_der_Jsonclick = "";
         lblTextblockfuncaodados_derimp_Jsonclick = "";
         lblTextblockfuncaodados_rlr_Jsonclick = "";
         lblTextblockfuncaodados_raimp_Jsonclick = "";
         lblTextblockfuncaodados_complexidade_Jsonclick = "";
         lblTextblockfuncaodados_pf_Jsonclick = "";
         lblTextblockfuncaodados_funcaodadoscod_Jsonclick = "";
         lblTextblockfuncaodados_funcaodadosdes_Jsonclick = "";
         A403FuncaoDados_FuncaoDadosDes = "";
         lblTextblockfuncaodados_funcaodadossiscod_Jsonclick = "";
         lblTextblockfuncaodados_funcaodadossisdes_Jsonclick = "";
         A405FuncaoDados_FuncaoDadosSisDes = "";
         lblTextblockfuncaodados_melhoracod_Jsonclick = "";
         lblTextblockfuncaodados_contar_Jsonclick = "";
         lblTextblockfuncaodados_tecnica_Jsonclick = "";
         lblTextblockfuncaodados_observacao_Jsonclick = "";
         lblTextblockfuncaodados_importada_Jsonclick = "";
         A1258FuncaoDados_Importada = DateTime.MinValue;
         lblTextblockfuncaodados_updaoimpbsln_Jsonclick = "";
         lblTextblockfuncaodados_ativo_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         A1245FuncaoDados_Observacao = "";
         A402FuncaoDados_FuncaoDadosNom = "";
         Funcaodados_observacao_Width = "";
         Funcaodados_observacao_Height = "";
         Funcaodados_observacao_Skin = "";
         Funcaodados_observacao_Toolbar = "";
         Funcaodados_observacao_Class = "";
         Funcaodados_observacao_Customtoolbar = "";
         Funcaodados_observacao_Customconfiguration = "";
         Funcaodados_observacao_Buttonpressedid = "";
         Funcaodados_observacao_Captionvalue = "";
         Funcaodados_observacao_Captionclass = "";
         Funcaodados_observacao_Captionposition = "";
         Funcaodados_observacao_Coltitle = "";
         Funcaodados_observacao_Coltitlefont = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z397FuncaoDados_Descricao = "";
         Z1245FuncaoDados_Observacao = "";
         Z371FuncaoDados_SistemaDes = "";
         Z705FuncaoDados_SistemaTecnica = "";
         Z402FuncaoDados_FuncaoDadosNom = "";
         Z403FuncaoDados_FuncaoDadosDes = "";
         Z405FuncaoDados_FuncaoDadosSisDes = "";
         T005E5_A402FuncaoDados_FuncaoDadosNom = new String[] {""} ;
         T005E5_n402FuncaoDados_FuncaoDadosNom = new bool[] {false} ;
         T005E5_A403FuncaoDados_FuncaoDadosDes = new String[] {""} ;
         T005E5_n403FuncaoDados_FuncaoDadosDes = new bool[] {false} ;
         T005E5_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         T005E5_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         T005E7_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         T005E7_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         T005E8_A368FuncaoDados_Codigo = new int[1] ;
         T005E8_A369FuncaoDados_Nome = new String[] {""} ;
         T005E8_A397FuncaoDados_Descricao = new String[] {""} ;
         T005E8_n397FuncaoDados_Descricao = new bool[] {false} ;
         T005E8_A371FuncaoDados_SistemaDes = new String[] {""} ;
         T005E8_n371FuncaoDados_SistemaDes = new bool[] {false} ;
         T005E8_A705FuncaoDados_SistemaTecnica = new String[] {""} ;
         T005E8_n705FuncaoDados_SistemaTecnica = new bool[] {false} ;
         T005E8_A373FuncaoDados_Tipo = new String[] {""} ;
         T005E8_A1024FuncaoDados_DERImp = new short[1] ;
         T005E8_n1024FuncaoDados_DERImp = new bool[] {false} ;
         T005E8_A1025FuncaoDados_RAImp = new short[1] ;
         T005E8_n1025FuncaoDados_RAImp = new bool[] {false} ;
         T005E8_A402FuncaoDados_FuncaoDadosNom = new String[] {""} ;
         T005E8_n402FuncaoDados_FuncaoDadosNom = new bool[] {false} ;
         T005E8_A403FuncaoDados_FuncaoDadosDes = new String[] {""} ;
         T005E8_n403FuncaoDados_FuncaoDadosDes = new bool[] {false} ;
         T005E8_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         T005E8_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         T005E8_A755FuncaoDados_Contar = new bool[] {false} ;
         T005E8_n755FuncaoDados_Contar = new bool[] {false} ;
         T005E8_A1147FuncaoDados_Tecnica = new short[1] ;
         T005E8_n1147FuncaoDados_Tecnica = new bool[] {false} ;
         T005E8_A1245FuncaoDados_Observacao = new String[] {""} ;
         T005E8_n1245FuncaoDados_Observacao = new bool[] {false} ;
         T005E8_A1258FuncaoDados_Importada = new DateTime[] {DateTime.MinValue} ;
         T005E8_n1258FuncaoDados_Importada = new bool[] {false} ;
         T005E8_A1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         T005E8_n1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         T005E8_A394FuncaoDados_Ativo = new String[] {""} ;
         T005E8_A370FuncaoDados_SistemaCod = new int[1] ;
         T005E8_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         T005E8_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         T005E8_A745FuncaoDados_MelhoraCod = new int[1] ;
         T005E8_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         T005E8_A372FuncaoDados_SistemaAreaCod = new int[1] ;
         T005E8_n372FuncaoDados_SistemaAreaCod = new bool[] {false} ;
         T005E8_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         T005E8_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         T005E3_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         T005E4_A371FuncaoDados_SistemaDes = new String[] {""} ;
         T005E4_n371FuncaoDados_SistemaDes = new bool[] {false} ;
         T005E4_A705FuncaoDados_SistemaTecnica = new String[] {""} ;
         T005E4_n705FuncaoDados_SistemaTecnica = new bool[] {false} ;
         T005E4_A372FuncaoDados_SistemaAreaCod = new int[1] ;
         T005E4_n372FuncaoDados_SistemaAreaCod = new bool[] {false} ;
         T005E6_A745FuncaoDados_MelhoraCod = new int[1] ;
         T005E6_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         T005E9_A371FuncaoDados_SistemaDes = new String[] {""} ;
         T005E9_n371FuncaoDados_SistemaDes = new bool[] {false} ;
         T005E9_A705FuncaoDados_SistemaTecnica = new String[] {""} ;
         T005E9_n705FuncaoDados_SistemaTecnica = new bool[] {false} ;
         T005E9_A372FuncaoDados_SistemaAreaCod = new int[1] ;
         T005E9_n372FuncaoDados_SistemaAreaCod = new bool[] {false} ;
         T005E10_A402FuncaoDados_FuncaoDadosNom = new String[] {""} ;
         T005E10_n402FuncaoDados_FuncaoDadosNom = new bool[] {false} ;
         T005E10_A403FuncaoDados_FuncaoDadosDes = new String[] {""} ;
         T005E10_n403FuncaoDados_FuncaoDadosDes = new bool[] {false} ;
         T005E10_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         T005E10_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         T005E11_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         T005E11_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         T005E12_A745FuncaoDados_MelhoraCod = new int[1] ;
         T005E12_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         T005E13_A368FuncaoDados_Codigo = new int[1] ;
         T005E3_A368FuncaoDados_Codigo = new int[1] ;
         T005E3_A369FuncaoDados_Nome = new String[] {""} ;
         T005E3_A397FuncaoDados_Descricao = new String[] {""} ;
         T005E3_n397FuncaoDados_Descricao = new bool[] {false} ;
         T005E3_A373FuncaoDados_Tipo = new String[] {""} ;
         T005E3_A1024FuncaoDados_DERImp = new short[1] ;
         T005E3_n1024FuncaoDados_DERImp = new bool[] {false} ;
         T005E3_A1025FuncaoDados_RAImp = new short[1] ;
         T005E3_n1025FuncaoDados_RAImp = new bool[] {false} ;
         T005E3_A755FuncaoDados_Contar = new bool[] {false} ;
         T005E3_n755FuncaoDados_Contar = new bool[] {false} ;
         T005E3_A1147FuncaoDados_Tecnica = new short[1] ;
         T005E3_n1147FuncaoDados_Tecnica = new bool[] {false} ;
         T005E3_A1245FuncaoDados_Observacao = new String[] {""} ;
         T005E3_n1245FuncaoDados_Observacao = new bool[] {false} ;
         T005E3_A1258FuncaoDados_Importada = new DateTime[] {DateTime.MinValue} ;
         T005E3_n1258FuncaoDados_Importada = new bool[] {false} ;
         T005E3_A1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         T005E3_n1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         T005E3_A394FuncaoDados_Ativo = new String[] {""} ;
         T005E3_A370FuncaoDados_SistemaCod = new int[1] ;
         T005E3_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         T005E3_A745FuncaoDados_MelhoraCod = new int[1] ;
         T005E3_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         sMode59 = "";
         T005E14_A368FuncaoDados_Codigo = new int[1] ;
         T005E15_A368FuncaoDados_Codigo = new int[1] ;
         T005E2_A368FuncaoDados_Codigo = new int[1] ;
         T005E2_A369FuncaoDados_Nome = new String[] {""} ;
         T005E2_A397FuncaoDados_Descricao = new String[] {""} ;
         T005E2_n397FuncaoDados_Descricao = new bool[] {false} ;
         T005E2_A373FuncaoDados_Tipo = new String[] {""} ;
         T005E2_A1024FuncaoDados_DERImp = new short[1] ;
         T005E2_n1024FuncaoDados_DERImp = new bool[] {false} ;
         T005E2_A1025FuncaoDados_RAImp = new short[1] ;
         T005E2_n1025FuncaoDados_RAImp = new bool[] {false} ;
         T005E2_A755FuncaoDados_Contar = new bool[] {false} ;
         T005E2_n755FuncaoDados_Contar = new bool[] {false} ;
         T005E2_A1147FuncaoDados_Tecnica = new short[1] ;
         T005E2_n1147FuncaoDados_Tecnica = new bool[] {false} ;
         T005E2_A1245FuncaoDados_Observacao = new String[] {""} ;
         T005E2_n1245FuncaoDados_Observacao = new bool[] {false} ;
         T005E2_A1258FuncaoDados_Importada = new DateTime[] {DateTime.MinValue} ;
         T005E2_n1258FuncaoDados_Importada = new bool[] {false} ;
         T005E2_A1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         T005E2_n1267FuncaoDados_UpdAoImpBsln = new bool[] {false} ;
         T005E2_A394FuncaoDados_Ativo = new String[] {""} ;
         T005E2_A370FuncaoDados_SistemaCod = new int[1] ;
         T005E2_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         T005E2_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         T005E2_A745FuncaoDados_MelhoraCod = new int[1] ;
         T005E2_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         T005E16_A368FuncaoDados_Codigo = new int[1] ;
         T005E19_A371FuncaoDados_SistemaDes = new String[] {""} ;
         T005E19_n371FuncaoDados_SistemaDes = new bool[] {false} ;
         T005E19_A705FuncaoDados_SistemaTecnica = new String[] {""} ;
         T005E19_n705FuncaoDados_SistemaTecnica = new bool[] {false} ;
         T005E19_A372FuncaoDados_SistemaAreaCod = new int[1] ;
         T005E19_n372FuncaoDados_SistemaAreaCod = new bool[] {false} ;
         T005E20_A402FuncaoDados_FuncaoDadosNom = new String[] {""} ;
         T005E20_n402FuncaoDados_FuncaoDadosNom = new bool[] {false} ;
         T005E20_A403FuncaoDados_FuncaoDadosDes = new String[] {""} ;
         T005E20_n403FuncaoDados_FuncaoDadosDes = new bool[] {false} ;
         T005E20_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         T005E20_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         T005E21_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         T005E21_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         T005E22_A745FuncaoDados_MelhoraCod = new int[1] ;
         T005E22_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         T005E23_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         T005E23_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         T005E24_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         T005E24_A1259FuncaoDadosPreImp_Sequencial = new short[1] ;
         T005E25_A736ProjetoMelhoria_Codigo = new int[1] ;
         T005E26_A165FuncaoAPF_Codigo = new int[1] ;
         T005E26_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         T005E27_A368FuncaoDados_Codigo = new int[1] ;
         T005E27_A172Tabela_Codigo = new int[1] ;
         T005E28_A368FuncaoDados_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i394FuncaoDados_Ativo = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T005E29_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         T005E29_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         T005E29_A402FuncaoDados_FuncaoDadosNom = new String[] {""} ;
         T005E29_n402FuncaoDados_FuncaoDadosNom = new bool[] {false} ;
         T005E30_A745FuncaoDados_MelhoraCod = new int[1] ;
         T005E30_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         T005E30_A369FuncaoDados_Nome = new String[] {""} ;
         Z376FuncaoDados_Complexidade = "";
         Z419FuncaoDados_Nome50 = "";
         GXt_char2 = "";
         isValidOutput = new GxUnknownObjectCollection();
         T005E31_A371FuncaoDados_SistemaDes = new String[] {""} ;
         T005E31_n371FuncaoDados_SistemaDes = new bool[] {false} ;
         T005E31_A705FuncaoDados_SistemaTecnica = new String[] {""} ;
         T005E31_n705FuncaoDados_SistemaTecnica = new bool[] {false} ;
         T005E31_A372FuncaoDados_SistemaAreaCod = new int[1] ;
         T005E31_n372FuncaoDados_SistemaAreaCod = new bool[] {false} ;
         T005E32_A402FuncaoDados_FuncaoDadosNom = new String[] {""} ;
         T005E32_n402FuncaoDados_FuncaoDadosNom = new bool[] {false} ;
         T005E32_A403FuncaoDados_FuncaoDadosDes = new String[] {""} ;
         T005E32_n403FuncaoDados_FuncaoDadosDes = new bool[] {false} ;
         T005E32_A404FuncaoDados_FuncaoDadosSisCod = new int[1] ;
         T005E32_n404FuncaoDados_FuncaoDadosSisCod = new bool[] {false} ;
         T005E33_A405FuncaoDados_FuncaoDadosSisDes = new String[] {""} ;
         T005E33_n405FuncaoDados_FuncaoDadosSisDes = new bool[] {false} ;
         T005E34_A745FuncaoDados_MelhoraCod = new int[1] ;
         T005E34_n745FuncaoDados_MelhoraCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaodadoscopy1__default(),
            new Object[][] {
                new Object[] {
               T005E2_A368FuncaoDados_Codigo, T005E2_A369FuncaoDados_Nome, T005E2_A397FuncaoDados_Descricao, T005E2_n397FuncaoDados_Descricao, T005E2_A373FuncaoDados_Tipo, T005E2_A1024FuncaoDados_DERImp, T005E2_n1024FuncaoDados_DERImp, T005E2_A1025FuncaoDados_RAImp, T005E2_n1025FuncaoDados_RAImp, T005E2_A755FuncaoDados_Contar,
               T005E2_n755FuncaoDados_Contar, T005E2_A1147FuncaoDados_Tecnica, T005E2_n1147FuncaoDados_Tecnica, T005E2_A1245FuncaoDados_Observacao, T005E2_n1245FuncaoDados_Observacao, T005E2_A1258FuncaoDados_Importada, T005E2_n1258FuncaoDados_Importada, T005E2_A1267FuncaoDados_UpdAoImpBsln, T005E2_n1267FuncaoDados_UpdAoImpBsln, T005E2_A394FuncaoDados_Ativo,
               T005E2_A370FuncaoDados_SistemaCod, T005E2_A391FuncaoDados_FuncaoDadosCod, T005E2_n391FuncaoDados_FuncaoDadosCod, T005E2_A745FuncaoDados_MelhoraCod, T005E2_n745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               T005E3_A368FuncaoDados_Codigo, T005E3_A369FuncaoDados_Nome, T005E3_A397FuncaoDados_Descricao, T005E3_n397FuncaoDados_Descricao, T005E3_A373FuncaoDados_Tipo, T005E3_A1024FuncaoDados_DERImp, T005E3_n1024FuncaoDados_DERImp, T005E3_A1025FuncaoDados_RAImp, T005E3_n1025FuncaoDados_RAImp, T005E3_A755FuncaoDados_Contar,
               T005E3_n755FuncaoDados_Contar, T005E3_A1147FuncaoDados_Tecnica, T005E3_n1147FuncaoDados_Tecnica, T005E3_A1245FuncaoDados_Observacao, T005E3_n1245FuncaoDados_Observacao, T005E3_A1258FuncaoDados_Importada, T005E3_n1258FuncaoDados_Importada, T005E3_A1267FuncaoDados_UpdAoImpBsln, T005E3_n1267FuncaoDados_UpdAoImpBsln, T005E3_A394FuncaoDados_Ativo,
               T005E3_A370FuncaoDados_SistemaCod, T005E3_A391FuncaoDados_FuncaoDadosCod, T005E3_n391FuncaoDados_FuncaoDadosCod, T005E3_A745FuncaoDados_MelhoraCod, T005E3_n745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               T005E4_A371FuncaoDados_SistemaDes, T005E4_n371FuncaoDados_SistemaDes, T005E4_A705FuncaoDados_SistemaTecnica, T005E4_n705FuncaoDados_SistemaTecnica, T005E4_A372FuncaoDados_SistemaAreaCod, T005E4_n372FuncaoDados_SistemaAreaCod
               }
               , new Object[] {
               T005E5_A402FuncaoDados_FuncaoDadosNom, T005E5_n402FuncaoDados_FuncaoDadosNom, T005E5_A403FuncaoDados_FuncaoDadosDes, T005E5_n403FuncaoDados_FuncaoDadosDes, T005E5_A404FuncaoDados_FuncaoDadosSisCod, T005E5_n404FuncaoDados_FuncaoDadosSisCod
               }
               , new Object[] {
               T005E6_A745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               T005E7_A405FuncaoDados_FuncaoDadosSisDes, T005E7_n405FuncaoDados_FuncaoDadosSisDes
               }
               , new Object[] {
               T005E8_A368FuncaoDados_Codigo, T005E8_A369FuncaoDados_Nome, T005E8_A397FuncaoDados_Descricao, T005E8_n397FuncaoDados_Descricao, T005E8_A371FuncaoDados_SistemaDes, T005E8_n371FuncaoDados_SistemaDes, T005E8_A705FuncaoDados_SistemaTecnica, T005E8_n705FuncaoDados_SistemaTecnica, T005E8_A373FuncaoDados_Tipo, T005E8_A1024FuncaoDados_DERImp,
               T005E8_n1024FuncaoDados_DERImp, T005E8_A1025FuncaoDados_RAImp, T005E8_n1025FuncaoDados_RAImp, T005E8_A402FuncaoDados_FuncaoDadosNom, T005E8_n402FuncaoDados_FuncaoDadosNom, T005E8_A403FuncaoDados_FuncaoDadosDes, T005E8_n403FuncaoDados_FuncaoDadosDes, T005E8_A405FuncaoDados_FuncaoDadosSisDes, T005E8_n405FuncaoDados_FuncaoDadosSisDes, T005E8_A755FuncaoDados_Contar,
               T005E8_n755FuncaoDados_Contar, T005E8_A1147FuncaoDados_Tecnica, T005E8_n1147FuncaoDados_Tecnica, T005E8_A1245FuncaoDados_Observacao, T005E8_n1245FuncaoDados_Observacao, T005E8_A1258FuncaoDados_Importada, T005E8_n1258FuncaoDados_Importada, T005E8_A1267FuncaoDados_UpdAoImpBsln, T005E8_n1267FuncaoDados_UpdAoImpBsln, T005E8_A394FuncaoDados_Ativo,
               T005E8_A370FuncaoDados_SistemaCod, T005E8_A391FuncaoDados_FuncaoDadosCod, T005E8_n391FuncaoDados_FuncaoDadosCod, T005E8_A745FuncaoDados_MelhoraCod, T005E8_n745FuncaoDados_MelhoraCod, T005E8_A372FuncaoDados_SistemaAreaCod, T005E8_n372FuncaoDados_SistemaAreaCod, T005E8_A404FuncaoDados_FuncaoDadosSisCod, T005E8_n404FuncaoDados_FuncaoDadosSisCod
               }
               , new Object[] {
               T005E9_A371FuncaoDados_SistemaDes, T005E9_n371FuncaoDados_SistemaDes, T005E9_A705FuncaoDados_SistemaTecnica, T005E9_n705FuncaoDados_SistemaTecnica, T005E9_A372FuncaoDados_SistemaAreaCod, T005E9_n372FuncaoDados_SistemaAreaCod
               }
               , new Object[] {
               T005E10_A402FuncaoDados_FuncaoDadosNom, T005E10_n402FuncaoDados_FuncaoDadosNom, T005E10_A403FuncaoDados_FuncaoDadosDes, T005E10_n403FuncaoDados_FuncaoDadosDes, T005E10_A404FuncaoDados_FuncaoDadosSisCod, T005E10_n404FuncaoDados_FuncaoDadosSisCod
               }
               , new Object[] {
               T005E11_A405FuncaoDados_FuncaoDadosSisDes, T005E11_n405FuncaoDados_FuncaoDadosSisDes
               }
               , new Object[] {
               T005E12_A745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               T005E13_A368FuncaoDados_Codigo
               }
               , new Object[] {
               T005E14_A368FuncaoDados_Codigo
               }
               , new Object[] {
               T005E15_A368FuncaoDados_Codigo
               }
               , new Object[] {
               T005E16_A368FuncaoDados_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005E19_A371FuncaoDados_SistemaDes, T005E19_n371FuncaoDados_SistemaDes, T005E19_A705FuncaoDados_SistemaTecnica, T005E19_n705FuncaoDados_SistemaTecnica, T005E19_A372FuncaoDados_SistemaAreaCod, T005E19_n372FuncaoDados_SistemaAreaCod
               }
               , new Object[] {
               T005E20_A402FuncaoDados_FuncaoDadosNom, T005E20_n402FuncaoDados_FuncaoDadosNom, T005E20_A403FuncaoDados_FuncaoDadosDes, T005E20_n403FuncaoDados_FuncaoDadosDes, T005E20_A404FuncaoDados_FuncaoDadosSisCod, T005E20_n404FuncaoDados_FuncaoDadosSisCod
               }
               , new Object[] {
               T005E21_A405FuncaoDados_FuncaoDadosSisDes, T005E21_n405FuncaoDados_FuncaoDadosSisDes
               }
               , new Object[] {
               T005E22_A745FuncaoDados_MelhoraCod
               }
               , new Object[] {
               T005E23_A391FuncaoDados_FuncaoDadosCod
               }
               , new Object[] {
               T005E24_A1261FuncaoDadosPreImp_FnDadosCodigo, T005E24_A1259FuncaoDadosPreImp_Sequencial
               }
               , new Object[] {
               T005E25_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               T005E26_A165FuncaoAPF_Codigo, T005E26_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               T005E27_A368FuncaoDados_Codigo, T005E27_A172Tabela_Codigo
               }
               , new Object[] {
               T005E28_A368FuncaoDados_Codigo
               }
               , new Object[] {
               T005E29_A391FuncaoDados_FuncaoDadosCod, T005E29_A402FuncaoDados_FuncaoDadosNom, T005E29_n402FuncaoDados_FuncaoDadosNom
               }
               , new Object[] {
               T005E30_A745FuncaoDados_MelhoraCod, T005E30_A369FuncaoDados_Nome
               }
               , new Object[] {
               T005E31_A371FuncaoDados_SistemaDes, T005E31_n371FuncaoDados_SistemaDes, T005E31_A705FuncaoDados_SistemaTecnica, T005E31_n705FuncaoDados_SistemaTecnica, T005E31_A372FuncaoDados_SistemaAreaCod, T005E31_n372FuncaoDados_SistemaAreaCod
               }
               , new Object[] {
               T005E32_A402FuncaoDados_FuncaoDadosNom, T005E32_n402FuncaoDados_FuncaoDadosNom, T005E32_A403FuncaoDados_FuncaoDadosDes, T005E32_n403FuncaoDados_FuncaoDadosDes, T005E32_A404FuncaoDados_FuncaoDadosSisCod, T005E32_n404FuncaoDados_FuncaoDadosSisCod
               }
               , new Object[] {
               T005E33_A405FuncaoDados_FuncaoDadosSisDes, T005E33_n405FuncaoDados_FuncaoDadosSisDes
               }
               , new Object[] {
               T005E34_A745FuncaoDados_MelhoraCod
               }
            }
         );
         Z394FuncaoDados_Ativo = "A";
         A394FuncaoDados_Ativo = "A";
         i394FuncaoDados_Ativo = "A";
         Z1267FuncaoDados_UpdAoImpBsln = true;
         n1267FuncaoDados_UpdAoImpBsln = false;
         A1267FuncaoDados_UpdAoImpBsln = true;
         n1267FuncaoDados_UpdAoImpBsln = false;
         i1267FuncaoDados_UpdAoImpBsln = true;
         n1267FuncaoDados_UpdAoImpBsln = false;
         Z755FuncaoDados_Contar = true;
         n755FuncaoDados_Contar = false;
         A755FuncaoDados_Contar = true;
         n755FuncaoDados_Contar = false;
         i755FuncaoDados_Contar = true;
         n755FuncaoDados_Contar = false;
      }

      private short Z1024FuncaoDados_DERImp ;
      private short Z1025FuncaoDados_RAImp ;
      private short Z1147FuncaoDados_Tecnica ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A420FuncaoDados_CodigoAliAie ;
      private short A374FuncaoDados_DER ;
      private short A1024FuncaoDados_DERImp ;
      private short A375FuncaoDados_RLR ;
      private short A1025FuncaoDados_RAImp ;
      private short A1147FuncaoDados_Tecnica ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound59 ;
      private short gxajaxcallmode ;
      private short Z420FuncaoDados_CodigoAliAie ;
      private short Z375FuncaoDados_RLR ;
      private short Z374FuncaoDados_DER ;
      private short GXt_int1 ;
      private short wbTemp ;
      private int Z368FuncaoDados_Codigo ;
      private int Z370FuncaoDados_SistemaCod ;
      private int Z391FuncaoDados_FuncaoDadosCod ;
      private int Z745FuncaoDados_MelhoraCod ;
      private int A368FuncaoDados_Codigo ;
      private int A370FuncaoDados_SistemaCod ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int A404FuncaoDados_FuncaoDadosSisCod ;
      private int A745FuncaoDados_MelhoraCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtFuncaoDados_Codigo_Enabled ;
      private int edtFuncaoDados_CodigoAliAie_Enabled ;
      private int edtFuncaoDados_Nome_Enabled ;
      private int edtFuncaoDados_Nome50_Enabled ;
      private int edtFuncaoDados_Descricao_Enabled ;
      private int edtFuncaoDados_SistemaCod_Enabled ;
      private int edtFuncaoDados_SistemaDes_Enabled ;
      private int A372FuncaoDados_SistemaAreaCod ;
      private int edtFuncaoDados_SistemaAreaCod_Enabled ;
      private int edtFuncaoDados_DER_Enabled ;
      private int edtFuncaoDados_DERImp_Enabled ;
      private int edtFuncaoDados_RLR_Enabled ;
      private int edtFuncaoDados_RAImp_Enabled ;
      private int edtFuncaoDados_PF_Enabled ;
      private int edtFuncaoDados_FuncaoDadosDes_Enabled ;
      private int edtFuncaoDados_FuncaoDadosSisCod_Enabled ;
      private int edtFuncaoDados_FuncaoDadosSisDes_Enabled ;
      private int edtFuncaoDados_Tecnica_Enabled ;
      private int edtFuncaoDados_Importada_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Funcaodados_observacao_Color ;
      private int Funcaodados_observacao_Coltitlecolor ;
      private int Z372FuncaoDados_SistemaAreaCod ;
      private int Z404FuncaoDados_FuncaoDadosSisCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal A377FuncaoDados_PF ;
      private decimal Z377FuncaoDados_PF ;
      private String sPrefix ;
      private String Z373FuncaoDados_Tipo ;
      private String Z394FuncaoDados_Ativo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String A705FuncaoDados_SistemaTecnica ;
      private String A373FuncaoDados_Tipo ;
      private String A376FuncaoDados_Complexidade ;
      private String chkFuncaoDados_Contar_Internalname ;
      private String A394FuncaoDados_Ativo ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtFuncaoDados_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockfuncaodados_codigo_Internalname ;
      private String lblTextblockfuncaodados_codigo_Jsonclick ;
      private String edtFuncaoDados_Codigo_Jsonclick ;
      private String lblTextblockfuncaodados_codigoaliaie_Internalname ;
      private String lblTextblockfuncaodados_codigoaliaie_Jsonclick ;
      private String edtFuncaoDados_CodigoAliAie_Internalname ;
      private String edtFuncaoDados_CodigoAliAie_Jsonclick ;
      private String lblTextblockfuncaodados_nome_Internalname ;
      private String lblTextblockfuncaodados_nome_Jsonclick ;
      private String edtFuncaoDados_Nome_Internalname ;
      private String lblTextblockfuncaodados_nome50_Internalname ;
      private String lblTextblockfuncaodados_nome50_Jsonclick ;
      private String edtFuncaoDados_Nome50_Internalname ;
      private String edtFuncaoDados_Nome50_Jsonclick ;
      private String lblTextblockfuncaodados_descricao_Internalname ;
      private String lblTextblockfuncaodados_descricao_Jsonclick ;
      private String edtFuncaoDados_Descricao_Internalname ;
      private String lblTextblockfuncaodados_sistemacod_Internalname ;
      private String lblTextblockfuncaodados_sistemacod_Jsonclick ;
      private String edtFuncaoDados_SistemaCod_Internalname ;
      private String edtFuncaoDados_SistemaCod_Jsonclick ;
      private String lblTextblockfuncaodados_sistemades_Internalname ;
      private String lblTextblockfuncaodados_sistemades_Jsonclick ;
      private String edtFuncaoDados_SistemaDes_Internalname ;
      private String edtFuncaoDados_SistemaDes_Jsonclick ;
      private String lblTextblockfuncaodados_sistemaareacod_Internalname ;
      private String lblTextblockfuncaodados_sistemaareacod_Jsonclick ;
      private String edtFuncaoDados_SistemaAreaCod_Internalname ;
      private String edtFuncaoDados_SistemaAreaCod_Jsonclick ;
      private String lblTextblockfuncaodados_sistematecnica_Internalname ;
      private String lblTextblockfuncaodados_sistematecnica_Jsonclick ;
      private String cmbFuncaoDados_SistemaTecnica_Internalname ;
      private String cmbFuncaoDados_SistemaTecnica_Jsonclick ;
      private String lblTextblockfuncaodados_tipo_Internalname ;
      private String lblTextblockfuncaodados_tipo_Jsonclick ;
      private String cmbFuncaoDados_Tipo_Internalname ;
      private String cmbFuncaoDados_Tipo_Jsonclick ;
      private String lblTextblockfuncaodados_der_Internalname ;
      private String lblTextblockfuncaodados_der_Jsonclick ;
      private String edtFuncaoDados_DER_Internalname ;
      private String edtFuncaoDados_DER_Jsonclick ;
      private String lblTextblockfuncaodados_derimp_Internalname ;
      private String lblTextblockfuncaodados_derimp_Jsonclick ;
      private String edtFuncaoDados_DERImp_Internalname ;
      private String edtFuncaoDados_DERImp_Jsonclick ;
      private String lblTextblockfuncaodados_rlr_Internalname ;
      private String lblTextblockfuncaodados_rlr_Jsonclick ;
      private String edtFuncaoDados_RLR_Internalname ;
      private String edtFuncaoDados_RLR_Jsonclick ;
      private String lblTextblockfuncaodados_raimp_Internalname ;
      private String lblTextblockfuncaodados_raimp_Jsonclick ;
      private String edtFuncaoDados_RAImp_Internalname ;
      private String edtFuncaoDados_RAImp_Jsonclick ;
      private String lblTextblockfuncaodados_complexidade_Internalname ;
      private String lblTextblockfuncaodados_complexidade_Jsonclick ;
      private String cmbFuncaoDados_Complexidade_Internalname ;
      private String cmbFuncaoDados_Complexidade_Jsonclick ;
      private String lblTextblockfuncaodados_pf_Internalname ;
      private String lblTextblockfuncaodados_pf_Jsonclick ;
      private String edtFuncaoDados_PF_Internalname ;
      private String edtFuncaoDados_PF_Jsonclick ;
      private String lblTextblockfuncaodados_funcaodadoscod_Internalname ;
      private String lblTextblockfuncaodados_funcaodadoscod_Jsonclick ;
      private String dynFuncaoDados_FuncaoDadosCod_Internalname ;
      private String dynFuncaoDados_FuncaoDadosCod_Jsonclick ;
      private String lblTextblockfuncaodados_funcaodadosdes_Internalname ;
      private String lblTextblockfuncaodados_funcaodadosdes_Jsonclick ;
      private String edtFuncaoDados_FuncaoDadosDes_Internalname ;
      private String lblTextblockfuncaodados_funcaodadossiscod_Internalname ;
      private String lblTextblockfuncaodados_funcaodadossiscod_Jsonclick ;
      private String edtFuncaoDados_FuncaoDadosSisCod_Internalname ;
      private String edtFuncaoDados_FuncaoDadosSisCod_Jsonclick ;
      private String lblTextblockfuncaodados_funcaodadossisdes_Internalname ;
      private String lblTextblockfuncaodados_funcaodadossisdes_Jsonclick ;
      private String edtFuncaoDados_FuncaoDadosSisDes_Internalname ;
      private String edtFuncaoDados_FuncaoDadosSisDes_Jsonclick ;
      private String lblTextblockfuncaodados_melhoracod_Internalname ;
      private String lblTextblockfuncaodados_melhoracod_Jsonclick ;
      private String dynFuncaoDados_MelhoraCod_Internalname ;
      private String dynFuncaoDados_MelhoraCod_Jsonclick ;
      private String lblTextblockfuncaodados_contar_Internalname ;
      private String lblTextblockfuncaodados_contar_Jsonclick ;
      private String lblTextblockfuncaodados_tecnica_Internalname ;
      private String lblTextblockfuncaodados_tecnica_Jsonclick ;
      private String edtFuncaoDados_Tecnica_Internalname ;
      private String edtFuncaoDados_Tecnica_Jsonclick ;
      private String lblTextblockfuncaodados_observacao_Internalname ;
      private String lblTextblockfuncaodados_observacao_Jsonclick ;
      private String lblTextblockfuncaodados_importada_Internalname ;
      private String lblTextblockfuncaodados_importada_Jsonclick ;
      private String edtFuncaoDados_Importada_Internalname ;
      private String edtFuncaoDados_Importada_Jsonclick ;
      private String lblTextblockfuncaodados_updaoimpbsln_Internalname ;
      private String lblTextblockfuncaodados_updaoimpbsln_Jsonclick ;
      private String cmbFuncaoDados_UpdAoImpBsln_Internalname ;
      private String cmbFuncaoDados_UpdAoImpBsln_Jsonclick ;
      private String lblTextblockfuncaodados_ativo_Internalname ;
      private String lblTextblockfuncaodados_ativo_Jsonclick ;
      private String cmbFuncaoDados_Ativo_Internalname ;
      private String cmbFuncaoDados_Ativo_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String Funcaodados_observacao_Width ;
      private String Funcaodados_observacao_Height ;
      private String Funcaodados_observacao_Skin ;
      private String Funcaodados_observacao_Toolbar ;
      private String Funcaodados_observacao_Class ;
      private String Funcaodados_observacao_Customtoolbar ;
      private String Funcaodados_observacao_Customconfiguration ;
      private String Funcaodados_observacao_Buttonpressedid ;
      private String Funcaodados_observacao_Captionvalue ;
      private String Funcaodados_observacao_Captionclass ;
      private String Funcaodados_observacao_Captionposition ;
      private String Funcaodados_observacao_Coltitle ;
      private String Funcaodados_observacao_Coltitlefont ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z705FuncaoDados_SistemaTecnica ;
      private String sMode59 ;
      private String Funcaodados_observacao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i394FuncaoDados_Ativo ;
      private String gxwrpcisep ;
      private String Z376FuncaoDados_Complexidade ;
      private String GXt_char2 ;
      private DateTime Z1258FuncaoDados_Importada ;
      private DateTime A1258FuncaoDados_Importada ;
      private bool Z755FuncaoDados_Contar ;
      private bool Z1267FuncaoDados_UpdAoImpBsln ;
      private bool entryPointCalled ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool n404FuncaoDados_FuncaoDadosSisCod ;
      private bool n745FuncaoDados_MelhoraCod ;
      private bool toggleJsOutput ;
      private bool n705FuncaoDados_SistemaTecnica ;
      private bool A1267FuncaoDados_UpdAoImpBsln ;
      private bool n1267FuncaoDados_UpdAoImpBsln ;
      private bool wbErr ;
      private bool A755FuncaoDados_Contar ;
      private bool n397FuncaoDados_Descricao ;
      private bool n371FuncaoDados_SistemaDes ;
      private bool n372FuncaoDados_SistemaAreaCod ;
      private bool n1024FuncaoDados_DERImp ;
      private bool n1025FuncaoDados_RAImp ;
      private bool n403FuncaoDados_FuncaoDadosDes ;
      private bool n405FuncaoDados_FuncaoDadosSisDes ;
      private bool n755FuncaoDados_Contar ;
      private bool n1147FuncaoDados_Tecnica ;
      private bool n1258FuncaoDados_Importada ;
      private bool n1245FuncaoDados_Observacao ;
      private bool n402FuncaoDados_FuncaoDadosNom ;
      private bool Funcaodados_observacao_Enabled ;
      private bool Funcaodados_observacao_Toolbarcancollapse ;
      private bool Funcaodados_observacao_Toolbarexpanded ;
      private bool Funcaodados_observacao_Usercontroliscolumn ;
      private bool Funcaodados_observacao_Visible ;
      private bool Gx_longc ;
      private bool i1267FuncaoDados_UpdAoImpBsln ;
      private bool i755FuncaoDados_Contar ;
      private String A397FuncaoDados_Descricao ;
      private String A403FuncaoDados_FuncaoDadosDes ;
      private String A1245FuncaoDados_Observacao ;
      private String Z397FuncaoDados_Descricao ;
      private String Z1245FuncaoDados_Observacao ;
      private String Z403FuncaoDados_FuncaoDadosDes ;
      private String Z369FuncaoDados_Nome ;
      private String A369FuncaoDados_Nome ;
      private String A419FuncaoDados_Nome50 ;
      private String A371FuncaoDados_SistemaDes ;
      private String A405FuncaoDados_FuncaoDadosSisDes ;
      private String A402FuncaoDados_FuncaoDadosNom ;
      private String Z371FuncaoDados_SistemaDes ;
      private String Z402FuncaoDados_FuncaoDadosNom ;
      private String Z405FuncaoDados_FuncaoDadosSisDes ;
      private String Z419FuncaoDados_Nome50 ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbFuncaoDados_SistemaTecnica ;
      private GXCombobox cmbFuncaoDados_Tipo ;
      private GXCombobox cmbFuncaoDados_Complexidade ;
      private GXCombobox dynFuncaoDados_FuncaoDadosCod ;
      private GXCombobox dynFuncaoDados_MelhoraCod ;
      private GXCheckbox chkFuncaoDados_Contar ;
      private GXCombobox cmbFuncaoDados_UpdAoImpBsln ;
      private GXCombobox cmbFuncaoDados_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T005E5_A402FuncaoDados_FuncaoDadosNom ;
      private bool[] T005E5_n402FuncaoDados_FuncaoDadosNom ;
      private String[] T005E5_A403FuncaoDados_FuncaoDadosDes ;
      private bool[] T005E5_n403FuncaoDados_FuncaoDadosDes ;
      private int[] T005E5_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] T005E5_n404FuncaoDados_FuncaoDadosSisCod ;
      private String[] T005E7_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] T005E7_n405FuncaoDados_FuncaoDadosSisDes ;
      private int[] T005E8_A368FuncaoDados_Codigo ;
      private String[] T005E8_A369FuncaoDados_Nome ;
      private String[] T005E8_A397FuncaoDados_Descricao ;
      private bool[] T005E8_n397FuncaoDados_Descricao ;
      private String[] T005E8_A371FuncaoDados_SistemaDes ;
      private bool[] T005E8_n371FuncaoDados_SistemaDes ;
      private String[] T005E8_A705FuncaoDados_SistemaTecnica ;
      private bool[] T005E8_n705FuncaoDados_SistemaTecnica ;
      private String[] T005E8_A373FuncaoDados_Tipo ;
      private short[] T005E8_A1024FuncaoDados_DERImp ;
      private bool[] T005E8_n1024FuncaoDados_DERImp ;
      private short[] T005E8_A1025FuncaoDados_RAImp ;
      private bool[] T005E8_n1025FuncaoDados_RAImp ;
      private String[] T005E8_A402FuncaoDados_FuncaoDadosNom ;
      private bool[] T005E8_n402FuncaoDados_FuncaoDadosNom ;
      private String[] T005E8_A403FuncaoDados_FuncaoDadosDes ;
      private bool[] T005E8_n403FuncaoDados_FuncaoDadosDes ;
      private String[] T005E8_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] T005E8_n405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] T005E8_A755FuncaoDados_Contar ;
      private bool[] T005E8_n755FuncaoDados_Contar ;
      private short[] T005E8_A1147FuncaoDados_Tecnica ;
      private bool[] T005E8_n1147FuncaoDados_Tecnica ;
      private String[] T005E8_A1245FuncaoDados_Observacao ;
      private bool[] T005E8_n1245FuncaoDados_Observacao ;
      private DateTime[] T005E8_A1258FuncaoDados_Importada ;
      private bool[] T005E8_n1258FuncaoDados_Importada ;
      private bool[] T005E8_A1267FuncaoDados_UpdAoImpBsln ;
      private bool[] T005E8_n1267FuncaoDados_UpdAoImpBsln ;
      private String[] T005E8_A394FuncaoDados_Ativo ;
      private int[] T005E8_A370FuncaoDados_SistemaCod ;
      private int[] T005E8_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] T005E8_n391FuncaoDados_FuncaoDadosCod ;
      private int[] T005E8_A745FuncaoDados_MelhoraCod ;
      private bool[] T005E8_n745FuncaoDados_MelhoraCod ;
      private int[] T005E8_A372FuncaoDados_SistemaAreaCod ;
      private bool[] T005E8_n372FuncaoDados_SistemaAreaCod ;
      private int[] T005E8_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] T005E8_n404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] T005E3_n391FuncaoDados_FuncaoDadosCod ;
      private String[] T005E4_A371FuncaoDados_SistemaDes ;
      private bool[] T005E4_n371FuncaoDados_SistemaDes ;
      private String[] T005E4_A705FuncaoDados_SistemaTecnica ;
      private bool[] T005E4_n705FuncaoDados_SistemaTecnica ;
      private int[] T005E4_A372FuncaoDados_SistemaAreaCod ;
      private bool[] T005E4_n372FuncaoDados_SistemaAreaCod ;
      private int[] T005E6_A745FuncaoDados_MelhoraCod ;
      private bool[] T005E6_n745FuncaoDados_MelhoraCod ;
      private String[] T005E9_A371FuncaoDados_SistemaDes ;
      private bool[] T005E9_n371FuncaoDados_SistemaDes ;
      private String[] T005E9_A705FuncaoDados_SistemaTecnica ;
      private bool[] T005E9_n705FuncaoDados_SistemaTecnica ;
      private int[] T005E9_A372FuncaoDados_SistemaAreaCod ;
      private bool[] T005E9_n372FuncaoDados_SistemaAreaCod ;
      private String[] T005E10_A402FuncaoDados_FuncaoDadosNom ;
      private bool[] T005E10_n402FuncaoDados_FuncaoDadosNom ;
      private String[] T005E10_A403FuncaoDados_FuncaoDadosDes ;
      private bool[] T005E10_n403FuncaoDados_FuncaoDadosDes ;
      private int[] T005E10_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] T005E10_n404FuncaoDados_FuncaoDadosSisCod ;
      private String[] T005E11_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] T005E11_n405FuncaoDados_FuncaoDadosSisDes ;
      private int[] T005E12_A745FuncaoDados_MelhoraCod ;
      private bool[] T005E12_n745FuncaoDados_MelhoraCod ;
      private int[] T005E13_A368FuncaoDados_Codigo ;
      private int[] T005E3_A368FuncaoDados_Codigo ;
      private String[] T005E3_A369FuncaoDados_Nome ;
      private String[] T005E3_A397FuncaoDados_Descricao ;
      private bool[] T005E3_n397FuncaoDados_Descricao ;
      private String[] T005E3_A373FuncaoDados_Tipo ;
      private short[] T005E3_A1024FuncaoDados_DERImp ;
      private bool[] T005E3_n1024FuncaoDados_DERImp ;
      private short[] T005E3_A1025FuncaoDados_RAImp ;
      private bool[] T005E3_n1025FuncaoDados_RAImp ;
      private bool[] T005E3_A755FuncaoDados_Contar ;
      private bool[] T005E3_n755FuncaoDados_Contar ;
      private short[] T005E3_A1147FuncaoDados_Tecnica ;
      private bool[] T005E3_n1147FuncaoDados_Tecnica ;
      private String[] T005E3_A1245FuncaoDados_Observacao ;
      private bool[] T005E3_n1245FuncaoDados_Observacao ;
      private DateTime[] T005E3_A1258FuncaoDados_Importada ;
      private bool[] T005E3_n1258FuncaoDados_Importada ;
      private bool[] T005E3_A1267FuncaoDados_UpdAoImpBsln ;
      private bool[] T005E3_n1267FuncaoDados_UpdAoImpBsln ;
      private String[] T005E3_A394FuncaoDados_Ativo ;
      private int[] T005E3_A370FuncaoDados_SistemaCod ;
      private int[] T005E3_A391FuncaoDados_FuncaoDadosCod ;
      private int[] T005E3_A745FuncaoDados_MelhoraCod ;
      private bool[] T005E3_n745FuncaoDados_MelhoraCod ;
      private int[] T005E14_A368FuncaoDados_Codigo ;
      private int[] T005E15_A368FuncaoDados_Codigo ;
      private int[] T005E2_A368FuncaoDados_Codigo ;
      private String[] T005E2_A369FuncaoDados_Nome ;
      private String[] T005E2_A397FuncaoDados_Descricao ;
      private bool[] T005E2_n397FuncaoDados_Descricao ;
      private String[] T005E2_A373FuncaoDados_Tipo ;
      private short[] T005E2_A1024FuncaoDados_DERImp ;
      private bool[] T005E2_n1024FuncaoDados_DERImp ;
      private short[] T005E2_A1025FuncaoDados_RAImp ;
      private bool[] T005E2_n1025FuncaoDados_RAImp ;
      private bool[] T005E2_A755FuncaoDados_Contar ;
      private bool[] T005E2_n755FuncaoDados_Contar ;
      private short[] T005E2_A1147FuncaoDados_Tecnica ;
      private bool[] T005E2_n1147FuncaoDados_Tecnica ;
      private String[] T005E2_A1245FuncaoDados_Observacao ;
      private bool[] T005E2_n1245FuncaoDados_Observacao ;
      private DateTime[] T005E2_A1258FuncaoDados_Importada ;
      private bool[] T005E2_n1258FuncaoDados_Importada ;
      private bool[] T005E2_A1267FuncaoDados_UpdAoImpBsln ;
      private bool[] T005E2_n1267FuncaoDados_UpdAoImpBsln ;
      private String[] T005E2_A394FuncaoDados_Ativo ;
      private int[] T005E2_A370FuncaoDados_SistemaCod ;
      private int[] T005E2_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] T005E2_n391FuncaoDados_FuncaoDadosCod ;
      private int[] T005E2_A745FuncaoDados_MelhoraCod ;
      private bool[] T005E2_n745FuncaoDados_MelhoraCod ;
      private int[] T005E16_A368FuncaoDados_Codigo ;
      private String[] T005E19_A371FuncaoDados_SistemaDes ;
      private bool[] T005E19_n371FuncaoDados_SistemaDes ;
      private String[] T005E19_A705FuncaoDados_SistemaTecnica ;
      private bool[] T005E19_n705FuncaoDados_SistemaTecnica ;
      private int[] T005E19_A372FuncaoDados_SistemaAreaCod ;
      private bool[] T005E19_n372FuncaoDados_SistemaAreaCod ;
      private String[] T005E20_A402FuncaoDados_FuncaoDadosNom ;
      private bool[] T005E20_n402FuncaoDados_FuncaoDadosNom ;
      private String[] T005E20_A403FuncaoDados_FuncaoDadosDes ;
      private bool[] T005E20_n403FuncaoDados_FuncaoDadosDes ;
      private int[] T005E20_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] T005E20_n404FuncaoDados_FuncaoDadosSisCod ;
      private String[] T005E21_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] T005E21_n405FuncaoDados_FuncaoDadosSisDes ;
      private int[] T005E22_A745FuncaoDados_MelhoraCod ;
      private bool[] T005E22_n745FuncaoDados_MelhoraCod ;
      private int[] T005E23_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] T005E23_n391FuncaoDados_FuncaoDadosCod ;
      private int[] T005E24_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private short[] T005E24_A1259FuncaoDadosPreImp_Sequencial ;
      private int[] T005E25_A736ProjetoMelhoria_Codigo ;
      private int[] T005E26_A165FuncaoAPF_Codigo ;
      private int[] T005E26_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] T005E27_A368FuncaoDados_Codigo ;
      private int[] T005E27_A172Tabela_Codigo ;
      private int[] T005E28_A368FuncaoDados_Codigo ;
      private int[] T005E29_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] T005E29_n391FuncaoDados_FuncaoDadosCod ;
      private String[] T005E29_A402FuncaoDados_FuncaoDadosNom ;
      private bool[] T005E29_n402FuncaoDados_FuncaoDadosNom ;
      private int[] T005E30_A745FuncaoDados_MelhoraCod ;
      private bool[] T005E30_n745FuncaoDados_MelhoraCod ;
      private String[] T005E30_A369FuncaoDados_Nome ;
      private String[] T005E31_A371FuncaoDados_SistemaDes ;
      private bool[] T005E31_n371FuncaoDados_SistemaDes ;
      private String[] T005E31_A705FuncaoDados_SistemaTecnica ;
      private bool[] T005E31_n705FuncaoDados_SistemaTecnica ;
      private int[] T005E31_A372FuncaoDados_SistemaAreaCod ;
      private bool[] T005E31_n372FuncaoDados_SistemaAreaCod ;
      private String[] T005E32_A402FuncaoDados_FuncaoDadosNom ;
      private bool[] T005E32_n402FuncaoDados_FuncaoDadosNom ;
      private String[] T005E32_A403FuncaoDados_FuncaoDadosDes ;
      private bool[] T005E32_n403FuncaoDados_FuncaoDadosDes ;
      private int[] T005E32_A404FuncaoDados_FuncaoDadosSisCod ;
      private bool[] T005E32_n404FuncaoDados_FuncaoDadosSisCod ;
      private String[] T005E33_A405FuncaoDados_FuncaoDadosSisDes ;
      private bool[] T005E33_n405FuncaoDados_FuncaoDadosSisDes ;
      private int[] T005E34_A745FuncaoDados_MelhoraCod ;
      private bool[] T005E34_n745FuncaoDados_MelhoraCod ;
      private GXWebForm Form ;
   }

   public class funcaodadoscopy1__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT005E8 ;
          prmT005E8 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E4 ;
          prmT005E4 = new Object[] {
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E5 ;
          prmT005E5 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E7 ;
          prmT005E7 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosSisCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E6 ;
          prmT005E6 = new Object[] {
          new Object[] {"@FuncaoDados_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E9 ;
          prmT005E9 = new Object[] {
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E10 ;
          prmT005E10 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E11 ;
          prmT005E11 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosSisCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E12 ;
          prmT005E12 = new Object[] {
          new Object[] {"@FuncaoDados_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E13 ;
          prmT005E13 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E3 ;
          prmT005E3 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E14 ;
          prmT005E14 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E15 ;
          prmT005E15 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E2 ;
          prmT005E2 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E16 ;
          prmT005E16 = new Object[] {
          new Object[] {"@FuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@FuncaoDados_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoDados_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDados_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_Contar",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoDados_Tecnica",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@FuncaoDados_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDados_Importada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@FuncaoDados_UpdAoImpBsln",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoDados_Ativo",SqlDbType.Char,1,0} ,
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E17 ;
          prmT005E17 = new Object[] {
          new Object[] {"@FuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@FuncaoDados_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoDados_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDados_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDados_Contar",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoDados_Tecnica",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@FuncaoDados_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDados_Importada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@FuncaoDados_UpdAoImpBsln",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoDados_Ativo",SqlDbType.Char,1,0} ,
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E18 ;
          prmT005E18 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E19 ;
          prmT005E19 = new Object[] {
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E20 ;
          prmT005E20 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E21 ;
          prmT005E21 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosSisCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E22 ;
          prmT005E22 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E23 ;
          prmT005E23 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E24 ;
          prmT005E24 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E25 ;
          prmT005E25 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E26 ;
          prmT005E26 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E27 ;
          prmT005E27 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E28 ;
          prmT005E28 = new Object[] {
          } ;
          Object[] prmT005E29 ;
          prmT005E29 = new Object[] {
          } ;
          Object[] prmT005E30 ;
          prmT005E30 = new Object[] {
          } ;
          Object[] prmT005E31 ;
          prmT005E31 = new Object[] {
          new Object[] {"@FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E32 ;
          prmT005E32 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E33 ;
          prmT005E33 = new Object[] {
          new Object[] {"@FuncaoDados_FuncaoDadosSisCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005E34 ;
          prmT005E34 = new Object[] {
          new Object[] {"@FuncaoDados_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T005E2", "SELECT [FuncaoDados_Codigo], [FuncaoDados_Nome], [FuncaoDados_Descricao], [FuncaoDados_Tipo], [FuncaoDados_DERImp], [FuncaoDados_RAImp], [FuncaoDados_Contar], [FuncaoDados_Tecnica], [FuncaoDados_Observacao], [FuncaoDados_Importada], [FuncaoDados_UpdAoImpBsln], [FuncaoDados_Ativo], [FuncaoDados_SistemaCod] AS FuncaoDados_SistemaCod, [FuncaoDados_FuncaoDadosCod] AS FuncaoDados_FuncaoDadosCod, [FuncaoDados_MelhoraCod] AS FuncaoDados_MelhoraCod FROM [FuncaoDados] WITH (UPDLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E2,1,0,true,false )
             ,new CursorDef("T005E3", "SELECT [FuncaoDados_Codigo], [FuncaoDados_Nome], [FuncaoDados_Descricao], [FuncaoDados_Tipo], [FuncaoDados_DERImp], [FuncaoDados_RAImp], [FuncaoDados_Contar], [FuncaoDados_Tecnica], [FuncaoDados_Observacao], [FuncaoDados_Importada], [FuncaoDados_UpdAoImpBsln], [FuncaoDados_Ativo], [FuncaoDados_SistemaCod] AS FuncaoDados_SistemaCod, [FuncaoDados_FuncaoDadosCod] AS FuncaoDados_FuncaoDadosCod, [FuncaoDados_MelhoraCod] AS FuncaoDados_MelhoraCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E3,1,0,true,false )
             ,new CursorDef("T005E4", "SELECT [Sistema_Nome] AS FuncaoDados_SistemaDes, [Sistema_Tecnica] AS FuncaoDados_SistemaTecnica, [Sistema_AreaTrabalhoCod] AS FuncaoDados_SistemaAreaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E4,1,0,true,false )
             ,new CursorDef("T005E5", "SELECT [FuncaoDados_Nome] AS FuncaoDados_FuncaoDadosNom, [FuncaoDados_Descricao] AS FuncaoDados_FuncaoDadosDes, [FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_FuncaoDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E5,1,0,true,false )
             ,new CursorDef("T005E6", "SELECT [FuncaoDados_Codigo] AS FuncaoDados_MelhoraCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E6,1,0,true,false )
             ,new CursorDef("T005E7", "SELECT [Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_FuncaoDadosSisCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E7,1,0,true,false )
             ,new CursorDef("T005E8", "SELECT TM1.[FuncaoDados_Codigo], TM1.[FuncaoDados_Nome], TM1.[FuncaoDados_Descricao], T2.[Sistema_Nome] AS FuncaoDados_SistemaDes, T2.[Sistema_Tecnica] AS FuncaoDados_SistemaTecnica, TM1.[FuncaoDados_Tipo], TM1.[FuncaoDados_DERImp], TM1.[FuncaoDados_RAImp], T3.[FuncaoDados_Nome] AS FuncaoDados_FuncaoDadosNom, T3.[FuncaoDados_Descricao] AS FuncaoDados_FuncaoDadosDes, T4.[Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes, TM1.[FuncaoDados_Contar], TM1.[FuncaoDados_Tecnica], TM1.[FuncaoDados_Observacao], TM1.[FuncaoDados_Importada], TM1.[FuncaoDados_UpdAoImpBsln], TM1.[FuncaoDados_Ativo], TM1.[FuncaoDados_SistemaCod] AS FuncaoDados_SistemaCod, TM1.[FuncaoDados_FuncaoDadosCod] AS FuncaoDados_FuncaoDadosCod, TM1.[FuncaoDados_MelhoraCod] AS FuncaoDados_MelhoraCod, T2.[Sistema_AreaTrabalhoCod] AS FuncaoDados_SistemaAreaCod, T3.[FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod FROM ((([FuncaoDados] TM1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = TM1.[FuncaoDados_SistemaCod]) LEFT JOIN [FuncaoDados] T3 WITH (NOLOCK) ON T3.[FuncaoDados_Codigo] = TM1.[FuncaoDados_FuncaoDadosCod]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T3.[FuncaoDados_SistemaCod]) WHERE TM1.[FuncaoDados_Codigo] = @FuncaoDados_Codigo ORDER BY TM1.[FuncaoDados_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005E8,100,0,true,false )
             ,new CursorDef("T005E9", "SELECT [Sistema_Nome] AS FuncaoDados_SistemaDes, [Sistema_Tecnica] AS FuncaoDados_SistemaTecnica, [Sistema_AreaTrabalhoCod] AS FuncaoDados_SistemaAreaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E9,1,0,true,false )
             ,new CursorDef("T005E10", "SELECT [FuncaoDados_Nome] AS FuncaoDados_FuncaoDadosNom, [FuncaoDados_Descricao] AS FuncaoDados_FuncaoDadosDes, [FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_FuncaoDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E10,1,0,true,false )
             ,new CursorDef("T005E11", "SELECT [Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_FuncaoDadosSisCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E11,1,0,true,false )
             ,new CursorDef("T005E12", "SELECT [FuncaoDados_Codigo] AS FuncaoDados_MelhoraCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E12,1,0,true,false )
             ,new CursorDef("T005E13", "SELECT [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005E13,1,0,true,false )
             ,new CursorDef("T005E14", "SELECT TOP 1 [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE ( [FuncaoDados_Codigo] > @FuncaoDados_Codigo) ORDER BY [FuncaoDados_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005E14,1,0,true,true )
             ,new CursorDef("T005E15", "SELECT TOP 1 [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE ( [FuncaoDados_Codigo] < @FuncaoDados_Codigo) ORDER BY [FuncaoDados_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005E15,1,0,true,true )
             ,new CursorDef("T005E16", "INSERT INTO [FuncaoDados]([FuncaoDados_Nome], [FuncaoDados_Descricao], [FuncaoDados_Tipo], [FuncaoDados_DERImp], [FuncaoDados_RAImp], [FuncaoDados_Contar], [FuncaoDados_Tecnica], [FuncaoDados_Observacao], [FuncaoDados_Importada], [FuncaoDados_UpdAoImpBsln], [FuncaoDados_Ativo], [FuncaoDados_SistemaCod], [FuncaoDados_FuncaoDadosCod], [FuncaoDados_MelhoraCod]) VALUES(@FuncaoDados_Nome, @FuncaoDados_Descricao, @FuncaoDados_Tipo, @FuncaoDados_DERImp, @FuncaoDados_RAImp, @FuncaoDados_Contar, @FuncaoDados_Tecnica, @FuncaoDados_Observacao, @FuncaoDados_Importada, @FuncaoDados_UpdAoImpBsln, @FuncaoDados_Ativo, @FuncaoDados_SistemaCod, @FuncaoDados_FuncaoDadosCod, @FuncaoDados_MelhoraCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT005E16)
             ,new CursorDef("T005E17", "UPDATE [FuncaoDados] SET [FuncaoDados_Nome]=@FuncaoDados_Nome, [FuncaoDados_Descricao]=@FuncaoDados_Descricao, [FuncaoDados_Tipo]=@FuncaoDados_Tipo, [FuncaoDados_DERImp]=@FuncaoDados_DERImp, [FuncaoDados_RAImp]=@FuncaoDados_RAImp, [FuncaoDados_Contar]=@FuncaoDados_Contar, [FuncaoDados_Tecnica]=@FuncaoDados_Tecnica, [FuncaoDados_Observacao]=@FuncaoDados_Observacao, [FuncaoDados_Importada]=@FuncaoDados_Importada, [FuncaoDados_UpdAoImpBsln]=@FuncaoDados_UpdAoImpBsln, [FuncaoDados_Ativo]=@FuncaoDados_Ativo, [FuncaoDados_SistemaCod]=@FuncaoDados_SistemaCod, [FuncaoDados_FuncaoDadosCod]=@FuncaoDados_FuncaoDadosCod, [FuncaoDados_MelhoraCod]=@FuncaoDados_MelhoraCod  WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo", GxErrorMask.GX_NOMASK,prmT005E17)
             ,new CursorDef("T005E18", "DELETE FROM [FuncaoDados]  WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo", GxErrorMask.GX_NOMASK,prmT005E18)
             ,new CursorDef("T005E19", "SELECT [Sistema_Nome] AS FuncaoDados_SistemaDes, [Sistema_Tecnica] AS FuncaoDados_SistemaTecnica, [Sistema_AreaTrabalhoCod] AS FuncaoDados_SistemaAreaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E19,1,0,true,false )
             ,new CursorDef("T005E20", "SELECT [FuncaoDados_Nome] AS FuncaoDados_FuncaoDadosNom, [FuncaoDados_Descricao] AS FuncaoDados_FuncaoDadosDes, [FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_FuncaoDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E20,1,0,true,false )
             ,new CursorDef("T005E21", "SELECT [Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_FuncaoDadosSisCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E21,1,0,true,false )
             ,new CursorDef("T005E22", "SELECT TOP 1 [FuncaoDados_Codigo] AS FuncaoDados_MelhoraCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_MelhoraCod] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E22,1,0,true,true )
             ,new CursorDef("T005E23", "SELECT TOP 1 [FuncaoDados_Codigo] AS FuncaoDados_FuncaoDadosCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_FuncaoDadosCod] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E23,1,0,true,true )
             ,new CursorDef("T005E24", "SELECT TOP 1 [FuncaoDadosPreImp_FnDadosCodigo], [FuncaoDadosPreImp_Sequencial] FROM [FuncaoDadosPreImp] WITH (NOLOCK) WHERE [FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E24,1,0,true,true )
             ,new CursorDef("T005E25", "SELECT TOP 1 [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_FnDadosCod] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E25,1,0,true,true )
             ,new CursorDef("T005E26", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPFAtributos_FuncaoDadosCod] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E26,1,0,true,true )
             ,new CursorDef("T005E27", "SELECT TOP 1 [FuncaoDados_Codigo], [Tabela_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E27,1,0,true,true )
             ,new CursorDef("T005E28", "SELECT [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) ORDER BY [FuncaoDados_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005E28,100,0,true,false )
             ,new CursorDef("T005E29", "SELECT [FuncaoDados_Codigo] AS FuncaoDados_FuncaoDadosCod, [FuncaoDados_Nome] AS FuncaoDados_FuncaoDadosNom FROM [FuncaoDados] WITH (NOLOCK) ORDER BY [FuncaoDados_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E29,0,0,true,false )
             ,new CursorDef("T005E30", "SELECT [FuncaoDados_Codigo] AS FuncaoDados_MelhoraCod, [FuncaoDados_Nome] FROM [FuncaoDados] WITH (NOLOCK) ORDER BY [FuncaoDados_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E30,0,0,true,false )
             ,new CursorDef("T005E31", "SELECT [Sistema_Nome] AS FuncaoDados_SistemaDes, [Sistema_Tecnica] AS FuncaoDados_SistemaTecnica, [Sistema_AreaTrabalhoCod] AS FuncaoDados_SistemaAreaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E31,1,0,true,false )
             ,new CursorDef("T005E32", "SELECT [FuncaoDados_Nome] AS FuncaoDados_FuncaoDadosNom, [FuncaoDados_Descricao] AS FuncaoDados_FuncaoDadosDes, [FuncaoDados_SistemaCod] AS FuncaoDados_FuncaoDadosSisCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_FuncaoDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E32,1,0,true,false )
             ,new CursorDef("T005E33", "SELECT [Sistema_Nome] AS FuncaoDados_FuncaoDadosSisDes FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoDados_FuncaoDadosSisCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E33,1,0,true,false )
             ,new CursorDef("T005E34", "SELECT [FuncaoDados_Codigo] AS FuncaoDados_MelhoraCod FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005E34,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 1) ;
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                ((int[]) buf[21])[0] = rslt.getInt(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((int[]) buf[23])[0] = rslt.getInt(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 1) ;
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                ((int[]) buf[21])[0] = rslt.getInt(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((int[]) buf[23])[0] = rslt.getInt(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 3) ;
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getLongVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[25])[0] = rslt.getGXDate(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((bool[]) buf[27])[0] = rslt.getBool(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((String[]) buf[29])[0] = rslt.getString(17, 1) ;
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((int[]) buf[35])[0] = rslt.getInt(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((int[]) buf[37])[0] = rslt.getInt(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 29 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 31 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(7, (short)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(9, (DateTime)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[17]);
                }
                stmt.SetParameter(11, (String)parms[18]);
                stmt.SetParameter(12, (int)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[23]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(7, (short)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(9, (DateTime)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[17]);
                }
                stmt.SetParameter(11, (String)parms[18]);
                stmt.SetParameter(12, (int)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[23]);
                }
                stmt.SetParameter(15, (int)parms[24]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 31 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 32 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
