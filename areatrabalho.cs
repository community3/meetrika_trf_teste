/*
               File: AreaTrabalho
        Description: �rea de Trabalho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:26:51.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class areatrabalho : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action70") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV8WWPContext);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_70_04106( AV8WWPContext) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"AREATRABALHO_SERVICOPADRAO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAAREATRABALHO_SERVICOPADRAO04106( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"AREATRABALHO_CONTRATADAUPDBSLCOD") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV8WWPContext);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAAREATRABALHO_CONTRATADAUPDBSLCOD04106( AV8WWPContext) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vESTADO_UF") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvESTADO_UF04106( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vMUNICIPIO_CODIGO") == 0 )
         {
            AV24Estado_UF = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Estado_UF", AV24Estado_UF);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvMUNICIPIO_CODIGO04106( AV24Estado_UF) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel8"+"_"+"CONTRATANTE_CODIGO") == 0 )
         {
            AV11Insert_Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contratante_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX8ASACONTRATANTE_CODIGO04106( AV11Insert_Contratante_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel9"+"_"+"CONTRATANTE_CODIGO") == 0 )
         {
            AV26Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Contratante_Codigo), 6, 0)));
            Gx_mode = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            AV15Contratante_CNPJ = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratante_CNPJ", AV15Contratante_CNPJ);
            AV16Contratante_RazaoSocial = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contratante_RazaoSocial", AV16Contratante_RazaoSocial);
            AV17Contratante_NomeFantasia = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratante_NomeFantasia", AV17Contratante_NomeFantasia);
            AV18Contratante_IE = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratante_IE", AV18Contratante_IE);
            AV21Contratante_Telefone = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratante_Telefone", AV21Contratante_Telefone);
            AV22Contratante_Ramal = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratante_Ramal", AV22Contratante_Ramal);
            AV23Contratante_Fax = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratante_Fax", AV23Contratante_Fax);
            AV20Contratante_Email = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratante_Email", AV20Contratante_Email);
            AV19Contratante_WebSite = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratante_WebSite", AV19Contratante_WebSite);
            AV25Municipio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0)));
            AV32Contratante_EmailSdaHost = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contratante_EmailSdaHost", AV32Contratante_EmailSdaHost);
            AV33Contratante_EmailSdaUser = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratante_EmailSdaUser", AV33Contratante_EmailSdaUser);
            AV35Contratante_EmailSdaPass = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratante_EmailSdaPass", AV35Contratante_EmailSdaPass);
            AV36Contratante_EmailSdaPort = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contratante_EmailSdaPort), 4, 0)));
            AV37Contratante_EmailSdaAut = (bool)(BooleanUtil.Val(GetNextPar( )));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contratante_EmailSdaAut", AV37Contratante_EmailSdaAut);
            AV34Contratante_EmailSdaSec = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX9ASACONTRATANTE_CODIGO04106( AV26Contratante_Codigo, Gx_mode, AV15Contratante_CNPJ, AV16Contratante_RazaoSocial, AV17Contratante_NomeFantasia, AV18Contratante_IE, AV21Contratante_Telefone, AV22Contratante_Ramal, AV23Contratante_Fax, AV20Contratante_Email, AV19Contratante_WebSite, AV25Municipio_Codigo, AV32Contratante_EmailSdaHost, AV33Contratante_EmailSdaUser, AV35Contratante_EmailSdaPass, AV36Contratante_EmailSdaPort, AV37Contratante_EmailSdaAut, AV34Contratante_EmailSdaSec) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_79") == 0 )
         {
            A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_79( A5AreaTrabalho_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_73") == 0 )
         {
            A29Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n29Contratante_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_73( A29Contratante_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_76") == 0 )
         {
            A25Municipio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n25Municipio_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_76( A25Municipio_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_77") == 0 )
         {
            A23Estado_UF = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_77( A23Estado_UF) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_78") == 0 )
         {
            A335Contratante_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A335Contratante_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_78( A335Contratante_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_74") == 0 )
         {
            A830AreaTrabalho_ServicoPadrao = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n830AreaTrabalho_ServicoPadrao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A830AreaTrabalho_ServicoPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_74( A830AreaTrabalho_ServicoPadrao) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_75") == 0 )
         {
            A1216AreaTrabalho_OrganizacaoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1216AreaTrabalho_OrganizacaoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1216AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_75( A1216AreaTrabalho_OrganizacaoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7AreaTrabalho_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbAreaTrabalho_CalculoPFinal.Name = "AREATRABALHO_CALCULOPFINAL";
         cmbAreaTrabalho_CalculoPFinal.WebTags = "";
         cmbAreaTrabalho_CalculoPFinal.addItem("MB", "Menor PFB", 0);
         cmbAreaTrabalho_CalculoPFinal.addItem("BM", "PFB FM", 0);
         if ( cmbAreaTrabalho_CalculoPFinal.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal)) )
            {
               A642AreaTrabalho_CalculoPFinal = "MB";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
            }
            A642AreaTrabalho_CalculoPFinal = cmbAreaTrabalho_CalculoPFinal.getValidValue(A642AreaTrabalho_CalculoPFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
         }
         dynAreaTrabalho_ServicoPadrao.Name = "AREATRABALHO_SERVICOPADRAO";
         dynAreaTrabalho_ServicoPadrao.WebTags = "";
         cmbAreaTrabalho_ValidaOSFM.Name = "AREATRABALHO_VALIDAOSFM";
         cmbAreaTrabalho_ValidaOSFM.WebTags = "";
         cmbAreaTrabalho_ValidaOSFM.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         cmbAreaTrabalho_ValidaOSFM.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         if ( cmbAreaTrabalho_ValidaOSFM.ItemCount > 0 )
         {
            A834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cmbAreaTrabalho_ValidaOSFM.getValidValue(StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM)));
            n834AreaTrabalho_ValidaOSFM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A834AreaTrabalho_ValidaOSFM", A834AreaTrabalho_ValidaOSFM);
         }
         dynAreaTrabalho_ContratadaUpdBslCod.Name = "AREATRABALHO_CONTRATADAUPDBSLCOD";
         dynAreaTrabalho_ContratadaUpdBslCod.WebTags = "";
         cmbAreaTrabalho_VerTA.Name = "AREATRABALHO_VERTA";
         cmbAreaTrabalho_VerTA.WebTags = "";
         cmbAreaTrabalho_VerTA.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Padr�o", 0);
         cmbAreaTrabalho_VerTA.addItem("1", "Vers�o 1", 0);
         if ( cmbAreaTrabalho_VerTA.ItemCount > 0 )
         {
            A2081AreaTrabalho_VerTA = (short)(NumberUtil.Val( cmbAreaTrabalho_VerTA.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0))), "."));
            n2081AreaTrabalho_VerTA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2081AreaTrabalho_VerTA", StringUtil.LTrim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)));
         }
         chkAreaTrabalho_Ativo.Name = "AREATRABALHO_ATIVO";
         chkAreaTrabalho_Ativo.WebTags = "";
         chkAreaTrabalho_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAreaTrabalho_Ativo_Internalname, "TitleCaption", chkAreaTrabalho_Ativo.Caption);
         chkAreaTrabalho_Ativo.CheckedValue = "false";
         dynavEstado_uf.Name = "vESTADO_UF";
         dynavEstado_uf.WebTags = "";
         dynavMunicipio_codigo.Name = "vMUNICIPIO_CODIGO";
         dynavMunicipio_codigo.WebTags = "";
         cmbavContratante_emailsdaaut.Name = "vCONTRATANTE_EMAILSDAAUT";
         cmbavContratante_emailsdaaut.WebTags = "";
         cmbavContratante_emailsdaaut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbavContratante_emailsdaaut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbavContratante_emailsdaaut.ItemCount > 0 )
         {
            AV37Contratante_EmailSdaAut = StringUtil.StrToBool( cmbavContratante_emailsdaaut.getValidValue(StringUtil.BoolToStr( AV37Contratante_EmailSdaAut)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contratante_EmailSdaAut", AV37Contratante_EmailSdaAut);
         }
         cmbavContratante_emailsdasec.Name = "vCONTRATANTE_EMAILSDASEC";
         cmbavContratante_emailsdasec.WebTags = "";
         cmbavContratante_emailsdasec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
         cmbavContratante_emailsdasec.addItem("1", "SSL e TLS", 0);
         if ( cmbavContratante_emailsdasec.ItemCount > 0 )
         {
            AV34Contratante_EmailSdaSec = (short)(NumberUtil.Val( cmbavContratante_emailsdasec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0)));
         }
         chkavPodedesativar.Name = "vPODEDESATIVAR";
         chkavPodedesativar.WebTags = "";
         chkavPodedesativar.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavPodedesativar_Internalname, "TitleCaption", chkavPodedesativar.Caption);
         chkavPodedesativar.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "�rea de Trabalho", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtAreaTrabalho_Descricao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public areatrabalho( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public areatrabalho( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_AreaTrabalho_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7AreaTrabalho_Codigo = aP1_AreaTrabalho_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbAreaTrabalho_CalculoPFinal = new GXCombobox();
         dynAreaTrabalho_ServicoPadrao = new GXCombobox();
         cmbAreaTrabalho_ValidaOSFM = new GXCombobox();
         dynAreaTrabalho_ContratadaUpdBslCod = new GXCombobox();
         cmbAreaTrabalho_VerTA = new GXCombobox();
         chkAreaTrabalho_Ativo = new GXCheckbox();
         dynavEstado_uf = new GXCombobox();
         dynavMunicipio_codigo = new GXCombobox();
         cmbavContratante_emailsdaaut = new GXCombobox();
         cmbavContratante_emailsdasec = new GXCombobox();
         chkavPodedesativar = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbAreaTrabalho_CalculoPFinal.ItemCount > 0 )
         {
            A642AreaTrabalho_CalculoPFinal = cmbAreaTrabalho_CalculoPFinal.getValidValue(A642AreaTrabalho_CalculoPFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
         }
         if ( dynAreaTrabalho_ServicoPadrao.ItemCount > 0 )
         {
            A830AreaTrabalho_ServicoPadrao = (int)(NumberUtil.Val( dynAreaTrabalho_ServicoPadrao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0))), "."));
            n830AreaTrabalho_ServicoPadrao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A830AreaTrabalho_ServicoPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0)));
         }
         if ( cmbAreaTrabalho_ValidaOSFM.ItemCount > 0 )
         {
            A834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cmbAreaTrabalho_ValidaOSFM.getValidValue(StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM)));
            n834AreaTrabalho_ValidaOSFM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A834AreaTrabalho_ValidaOSFM", A834AreaTrabalho_ValidaOSFM);
         }
         if ( dynAreaTrabalho_ContratadaUpdBslCod.ItemCount > 0 )
         {
            A987AreaTrabalho_ContratadaUpdBslCod = (int)(NumberUtil.Val( dynAreaTrabalho_ContratadaUpdBslCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0))), "."));
            n987AreaTrabalho_ContratadaUpdBslCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A987AreaTrabalho_ContratadaUpdBslCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0)));
         }
         if ( cmbAreaTrabalho_VerTA.ItemCount > 0 )
         {
            A2081AreaTrabalho_VerTA = (short)(NumberUtil.Val( cmbAreaTrabalho_VerTA.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0))), "."));
            n2081AreaTrabalho_VerTA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2081AreaTrabalho_VerTA", StringUtil.LTrim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)));
         }
         if ( dynavEstado_uf.ItemCount > 0 )
         {
            AV24Estado_UF = dynavEstado_uf.getValidValue(AV24Estado_UF);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Estado_UF", AV24Estado_UF);
         }
         if ( dynavMunicipio_codigo.ItemCount > 0 )
         {
            AV25Municipio_Codigo = (int)(NumberUtil.Val( dynavMunicipio_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0)));
         }
         if ( cmbavContratante_emailsdaaut.ItemCount > 0 )
         {
            AV37Contratante_EmailSdaAut = StringUtil.StrToBool( cmbavContratante_emailsdaaut.getValidValue(StringUtil.BoolToStr( AV37Contratante_EmailSdaAut)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contratante_EmailSdaAut", AV37Contratante_EmailSdaAut);
         }
         if ( cmbavContratante_emailsdasec.ItemCount > 0 )
         {
            AV34Contratante_EmailSdaSec = (short)(NumberUtil.Val( cmbavContratante_emailsdasec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_04106( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_04106e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Check box */
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavPodedesativar_Internalname, StringUtil.BoolToStr( AV38PodeDesativar), "", "", chkavPodedesativar.Visible, chkavPodedesativar.Enabled, "true", "", StyleString, ClassString, "", "");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 198,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,198);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratante_Codigo_Visible, edtContratante_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AreaTrabalho.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")), ((edtAreaTrabalho_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtAreaTrabalho_Codigo_Visible, edtAreaTrabalho_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AreaTrabalho.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_04106( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAreatrabalhotitle_Internalname, "�rea de Trabalho", "", "", lblAreatrabalhotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_11_04106( true) ;
         }
         return  ;
      }

      protected void wb_table2_11_04106e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_189_04106( true) ;
         }
         return  ;
      }

      protected void wb_table3_189_04106e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_04106e( true) ;
         }
         else
         {
            wb_table1_2_04106e( false) ;
         }
      }

      protected void wb_table3_189_04106( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 192,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 194,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 196,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_189_04106e( true) ;
         }
         else
         {
            wb_table3_189_04106e( false) ;
         }
      }

      protected void wb_table2_11_04106( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_AREADETRABALHOContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_AREADETRABALHOContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_04106( true) ;
         }
         return  ;
      }

      protected void wb_table4_16_04106e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_11_04106e( true) ;
         }
         else
         {
            wb_table2_11_04106e( false) ;
         }
      }

      protected void wb_table4_16_04106( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblAreadetrabalho_Internalname, tblAreadetrabalho_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_19_04106( true) ;
         }
         return  ;
      }

      protected void wb_table5_19_04106e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_CONTRATANTEContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_CONTRATANTEContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table6_97_04106( true) ;
         }
         return  ;
      }

      protected void wb_table6_97_04106e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_04106e( true) ;
         }
         else
         {
            wb_table4_16_04106e( false) ;
         }
      }

      protected void wb_table6_97_04106( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblContratante_Internalname, tblContratante_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_cnpj_Internalname, "CNPJ", "", "", lblTextblockcontratante_cnpj_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_cnpj_Internalname, AV15Contratante_CNPJ, StringUtil.RTrim( context.localUtil.Format( AV15Contratante_CNPJ, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_cnpj_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_cnpj_Enabled, 1, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ie_Internalname, "Insc. Estadual", "", "", lblTextblockcontratante_ie_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_ie_Internalname, StringUtil.RTrim( AV18Contratante_IE), StringUtil.RTrim( context.localUtil.Format( AV18Contratante_IE, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_ie_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_ie_Enabled, 1, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_razaosocial_Internalname, "Raz�o Social", "", "", lblTextblockcontratante_razaosocial_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_razaosocial_Internalname, StringUtil.RTrim( AV16Contratante_RazaoSocial), StringUtil.RTrim( context.localUtil.Format( AV16Contratante_RazaoSocial, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_razaosocial_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_razaosocial_Enabled, 1, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_nomefantasia_Internalname, "Nome Fantasia", "", "", lblTextblockcontratante_nomefantasia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_nomefantasia_Internalname, StringUtil.RTrim( AV17Contratante_NomeFantasia), StringUtil.RTrim( context.localUtil.Format( AV17Contratante_NomeFantasia, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_nomefantasia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_nomefantasia_Enabled, 1, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_website_Internalname, "Site", "", "", lblTextblockcontratante_website_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_website_Internalname, AV19Contratante_WebSite, StringUtil.RTrim( context.localUtil.Format( AV19Contratante_WebSite, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_website_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_website_Enabled, 1, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_email_Internalname, "Email", "", "", lblTextblockcontratante_email_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_email_Internalname, AV20Contratante_Email, StringUtil.RTrim( context.localUtil.Format( AV20Contratante_Email, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_email_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_email_Enabled, 1, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_telefone_Internalname, "Telefone", "", "", lblTextblockcontratante_telefone_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_telefone_Internalname, StringUtil.RTrim( AV21Contratante_Telefone), StringUtil.RTrim( context.localUtil.Format( AV21Contratante_Telefone, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_telefone_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_telefone_Enabled, 1, "text", "", 100, "px", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ramal_Internalname, "Ramal", "", "", lblTextblockcontratante_ramal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_ramal_Internalname, StringUtil.RTrim( AV22Contratante_Ramal), StringUtil.RTrim( context.localUtil.Format( AV22Contratante_Ramal, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_ramal_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_ramal_Enabled, 1, "text", "", 100, "px", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_fax_Internalname, "Fax", "", "", lblTextblockcontratante_fax_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_fax_Internalname, StringUtil.RTrim( AV23Contratante_Fax), StringUtil.RTrim( context.localUtil.Format( AV23Contratante_Fax, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_fax_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_fax_Enabled, 1, "text", "", 100, "px", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockestado_uf_Internalname, "UF", "", "", lblTextblockestado_uf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavEstado_uf, dynavEstado_uf_Internalname, StringUtil.RTrim( AV24Estado_UF), 1, dynavEstado_uf_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, dynavEstado_uf.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"", "", true, "HLP_AreaTrabalho.htm");
            dynavEstado_uf.CurrentValue = StringUtil.RTrim( AV24Estado_UF);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf_Internalname, "Values", (String)(dynavEstado_uf.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmunicipio_codigo_Internalname, "Municipio", "", "", lblTextblockmunicipio_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 150,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavMunicipio_codigo, dynavMunicipio_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0)), 1, dynavMunicipio_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavMunicipio_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,150);\"", "", true, "HLP_AreaTrabalho.htm");
            dynavMunicipio_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavMunicipio_codigo_Internalname, "Values", (String)(dynavMunicipio_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblemail_Internalname, "Servidor de E-mail:", "", "", lblLblemail_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "SelectedTab", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_emailsdahost_Internalname, "Host SMTP", "", "", lblTextblockcontratante_emailsdahost_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 165,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_emailsdahost_Internalname, AV32Contratante_EmailSdaHost, StringUtil.RTrim( context.localUtil.Format( AV32Contratante_EmailSdaHost, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,165);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_emailsdahost_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_emailsdahost_Enabled, 1, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_emailsdauser_Internalname, "Usu�rio", "", "", lblTextblockcontratante_emailsdauser_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_emailsdauser_Internalname, AV33Contratante_EmailSdaUser, StringUtil.RTrim( context.localUtil.Format( AV33Contratante_EmailSdaUser, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,169);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_emailsdauser_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_emailsdauser_Enabled, 1, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_emailsdapass_Internalname, "Senha", "", "", lblTextblockcontratante_emailsdapass_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 173,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_emailsdapass_Internalname, AV35Contratante_EmailSdaPass, StringUtil.RTrim( context.localUtil.Format( AV35Contratante_EmailSdaPass, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,173);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_emailsdapass_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_emailsdapass_Enabled, 1, "text", "", 80, "chr", 1, "row", 100, -1, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_emailsdaport_Internalname, "Porta", "", "", lblTextblockcontratante_emailsdaport_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 178,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_emailsdaport_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Contratante_EmailSdaPort), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36Contratante_EmailSdaPort), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,178);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_emailsdaport_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratante_emailsdaport_Enabled, 1, "text", "", 45, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_emailsdaaut_Internalname, "Autentica��o", "", "", lblTextblockcontratante_emailsdaaut_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 182,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratante_emailsdaaut, cmbavContratante_emailsdaaut_Internalname, StringUtil.BoolToStr( AV37Contratante_EmailSdaAut), 1, cmbavContratante_emailsdaaut_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbavContratante_emailsdaaut.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,182);\"", "", true, "HLP_AreaTrabalho.htm");
            cmbavContratante_emailsdaaut.CurrentValue = StringUtil.BoolToStr( AV37Contratante_EmailSdaAut);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratante_emailsdaaut_Internalname, "Values", (String)(cmbavContratante_emailsdaaut.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_emailsdasec_Internalname, "Seguran�a", "", "", lblTextblockcontratante_emailsdasec_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 186,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratante_emailsdasec, cmbavContratante_emailsdasec_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0)), 1, cmbavContratante_emailsdasec_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavContratante_emailsdasec.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,186);\"", "", true, "HLP_AreaTrabalho.htm");
            cmbavContratante_emailsdasec.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratante_emailsdasec_Internalname, "Values", (String)(cmbavContratante_emailsdasec.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_97_04106e( true) ;
         }
         else
         {
            wb_table6_97_04106e( false) ;
         }
      }

      protected void wb_table5_19_04106( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockorganizacao_nome_Internalname, "Organiza��o", "", "", lblTextblockorganizacao_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtOrganizacao_Nome_Internalname, StringUtil.RTrim( A1214Organizacao_Nome), StringUtil.RTrim( context.localUtil.Format( A1214Organizacao_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtOrganizacao_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtOrganizacao_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_descricao_Internalname, "Nome", "", "", lblTextblockareatrabalho_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_Descricao_Internalname, A6AreaTrabalho_Descricao, StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAreaTrabalho_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_calculopfinal_Internalname, "Calculo P. Final", "", "", lblTextblockareatrabalho_calculopfinal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbAreaTrabalho_CalculoPFinal, cmbAreaTrabalho_CalculoPFinal_Internalname, StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal), 1, cmbAreaTrabalho_CalculoPFinal_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbAreaTrabalho_CalculoPFinal.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_AreaTrabalho.htm");
            cmbAreaTrabalho_CalculoPFinal.CurrentValue = StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_CalculoPFinal_Internalname, "Values", (String)(cmbAreaTrabalho_CalculoPFinal.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_servicopadrao_Internalname, "Servi�o padr�o", "", "", lblTextblockareatrabalho_servicopadrao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynAreaTrabalho_ServicoPadrao, dynAreaTrabalho_ServicoPadrao_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0)), 1, dynAreaTrabalho_ServicoPadrao_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynAreaTrabalho_ServicoPadrao.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_AreaTrabalho.htm");
            dynAreaTrabalho_ServicoPadrao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAreaTrabalho_ServicoPadrao_Internalname, "Values", (String)(dynAreaTrabalho_ServicoPadrao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_validaosfm_Internalname, "Valida OS FM", "", "", lblTextblockareatrabalho_validaosfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbAreaTrabalho_ValidaOSFM, cmbAreaTrabalho_ValidaOSFM_Internalname, StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM), 1, cmbAreaTrabalho_ValidaOSFM_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbAreaTrabalho_ValidaOSFM.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_AreaTrabalho.htm");
            cmbAreaTrabalho_ValidaOSFM.CurrentValue = StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_ValidaOSFM_Internalname, "Values", (String)(cmbAreaTrabalho_ValidaOSFM.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_diasparapagar_Internalname, "Dias para pagar", "", "", lblTextblockareatrabalho_diasparapagar_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAreaTrabalho_DiasParaPagar_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0, ",", "")), ((edtAreaTrabalho_DiasParaPagar_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A855AreaTrabalho_DiasParaPagar), "ZZZ9")) : context.localUtil.Format( (decimal)(A855AreaTrabalho_DiasParaPagar), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAreaTrabalho_DiasParaPagar_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAreaTrabalho_DiasParaPagar_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_contratadaupdbslcod_Internalname, "Quem atualiza o Baseline", "", "", lblTextblockareatrabalho_contratadaupdbslcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynAreaTrabalho_ContratadaUpdBslCod, dynAreaTrabalho_ContratadaUpdBslCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0)), 1, dynAreaTrabalho_ContratadaUpdBslCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynAreaTrabalho_ContratadaUpdBslCod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_AreaTrabalho.htm");
            dynAreaTrabalho_ContratadaUpdBslCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAreaTrabalho_ContratadaUpdBslCod_Internalname, "Values", (String)(dynAreaTrabalho_ContratadaUpdBslCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_verta_Internalname, "Vers�o do TA", "", "", lblTextblockareatrabalho_verta_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbAreaTrabalho_VerTA, cmbAreaTrabalho_VerTA_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)), 1, cmbAreaTrabalho_VerTA_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbAreaTrabalho_VerTA.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "", true, "HLP_AreaTrabalho.htm");
            cmbAreaTrabalho_VerTA.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_VerTA_Internalname, "Values", (String)(cmbAreaTrabalho_VerTA.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_ativo_Internalname, "Ativa", "", "", lblTextblockareatrabalho_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockareatrabalho_ativo_Visible, 1, 0, "HLP_AreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkAreaTrabalho_Ativo_Internalname, StringUtil.BoolToStr( A72AreaTrabalho_Ativo), "", "", chkAreaTrabalho_Ativo.Visible, chkAreaTrabalho_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(84, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_04106e( true) ;
         }
         else
         {
            wb_table5_19_04106e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11042 */
         E11042 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1214Organizacao_Nome = StringUtil.Upper( cgiGet( edtOrganizacao_Nome_Internalname));
               n1214Organizacao_Nome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
               A6AreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtAreaTrabalho_Descricao_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
               cmbAreaTrabalho_CalculoPFinal.CurrentValue = cgiGet( cmbAreaTrabalho_CalculoPFinal_Internalname);
               A642AreaTrabalho_CalculoPFinal = cgiGet( cmbAreaTrabalho_CalculoPFinal_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
               dynAreaTrabalho_ServicoPadrao.CurrentValue = cgiGet( dynAreaTrabalho_ServicoPadrao_Internalname);
               A830AreaTrabalho_ServicoPadrao = (int)(NumberUtil.Val( cgiGet( dynAreaTrabalho_ServicoPadrao_Internalname), "."));
               n830AreaTrabalho_ServicoPadrao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A830AreaTrabalho_ServicoPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0)));
               n830AreaTrabalho_ServicoPadrao = ((0==A830AreaTrabalho_ServicoPadrao) ? true : false);
               cmbAreaTrabalho_ValidaOSFM.CurrentValue = cgiGet( cmbAreaTrabalho_ValidaOSFM_Internalname);
               A834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cgiGet( cmbAreaTrabalho_ValidaOSFM_Internalname));
               n834AreaTrabalho_ValidaOSFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A834AreaTrabalho_ValidaOSFM", A834AreaTrabalho_ValidaOSFM);
               n834AreaTrabalho_ValidaOSFM = ((false==A834AreaTrabalho_ValidaOSFM) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtAreaTrabalho_DiasParaPagar_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAreaTrabalho_DiasParaPagar_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AREATRABALHO_DIASPARAPAGAR");
                  AnyError = 1;
                  GX_FocusControl = edtAreaTrabalho_DiasParaPagar_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A855AreaTrabalho_DiasParaPagar = 0;
                  n855AreaTrabalho_DiasParaPagar = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A855AreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0)));
               }
               else
               {
                  A855AreaTrabalho_DiasParaPagar = (short)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_DiasParaPagar_Internalname), ",", "."));
                  n855AreaTrabalho_DiasParaPagar = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A855AreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0)));
               }
               n855AreaTrabalho_DiasParaPagar = ((0==A855AreaTrabalho_DiasParaPagar) ? true : false);
               dynAreaTrabalho_ContratadaUpdBslCod.CurrentValue = cgiGet( dynAreaTrabalho_ContratadaUpdBslCod_Internalname);
               A987AreaTrabalho_ContratadaUpdBslCod = (int)(NumberUtil.Val( cgiGet( dynAreaTrabalho_ContratadaUpdBslCod_Internalname), "."));
               n987AreaTrabalho_ContratadaUpdBslCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A987AreaTrabalho_ContratadaUpdBslCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0)));
               n987AreaTrabalho_ContratadaUpdBslCod = ((0==A987AreaTrabalho_ContratadaUpdBslCod) ? true : false);
               cmbAreaTrabalho_VerTA.CurrentValue = cgiGet( cmbAreaTrabalho_VerTA_Internalname);
               A2081AreaTrabalho_VerTA = (short)(NumberUtil.Val( cgiGet( cmbAreaTrabalho_VerTA_Internalname), "."));
               n2081AreaTrabalho_VerTA = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2081AreaTrabalho_VerTA", StringUtil.LTrim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)));
               n2081AreaTrabalho_VerTA = ((0==A2081AreaTrabalho_VerTA) ? true : false);
               A72AreaTrabalho_Ativo = StringUtil.StrToBool( cgiGet( chkAreaTrabalho_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72AreaTrabalho_Ativo", A72AreaTrabalho_Ativo);
               AV15Contratante_CNPJ = cgiGet( edtavContratante_cnpj_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratante_CNPJ", AV15Contratante_CNPJ);
               AV18Contratante_IE = cgiGet( edtavContratante_ie_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratante_IE", AV18Contratante_IE);
               AV16Contratante_RazaoSocial = StringUtil.Upper( cgiGet( edtavContratante_razaosocial_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contratante_RazaoSocial", AV16Contratante_RazaoSocial);
               AV17Contratante_NomeFantasia = StringUtil.Upper( cgiGet( edtavContratante_nomefantasia_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratante_NomeFantasia", AV17Contratante_NomeFantasia);
               AV19Contratante_WebSite = cgiGet( edtavContratante_website_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratante_WebSite", AV19Contratante_WebSite);
               AV20Contratante_Email = cgiGet( edtavContratante_email_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratante_Email", AV20Contratante_Email);
               AV21Contratante_Telefone = cgiGet( edtavContratante_telefone_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratante_Telefone", AV21Contratante_Telefone);
               AV22Contratante_Ramal = cgiGet( edtavContratante_ramal_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratante_Ramal", AV22Contratante_Ramal);
               AV23Contratante_Fax = cgiGet( edtavContratante_fax_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratante_Fax", AV23Contratante_Fax);
               dynavEstado_uf.CurrentValue = cgiGet( dynavEstado_uf_Internalname);
               AV24Estado_UF = cgiGet( dynavEstado_uf_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Estado_UF", AV24Estado_UF);
               dynavMunicipio_codigo.CurrentValue = cgiGet( dynavMunicipio_codigo_Internalname);
               AV25Municipio_Codigo = (int)(NumberUtil.Val( cgiGet( dynavMunicipio_codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0)));
               AV32Contratante_EmailSdaHost = cgiGet( edtavContratante_emailsdahost_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contratante_EmailSdaHost", AV32Contratante_EmailSdaHost);
               AV33Contratante_EmailSdaUser = cgiGet( edtavContratante_emailsdauser_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratante_EmailSdaUser", AV33Contratante_EmailSdaUser);
               AV35Contratante_EmailSdaPass = cgiGet( edtavContratante_emailsdapass_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratante_EmailSdaPass", AV35Contratante_EmailSdaPass);
               if ( ( ( context.localUtil.CToN( cgiGet( edtavContratante_emailsdaport_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratante_emailsdaport_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATANTE_EMAILSDAPORT");
                  AnyError = 1;
                  GX_FocusControl = edtavContratante_emailsdaport_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV36Contratante_EmailSdaPort = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contratante_EmailSdaPort), 4, 0)));
               }
               else
               {
                  AV36Contratante_EmailSdaPort = (short)(context.localUtil.CToN( cgiGet( edtavContratante_emailsdaport_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contratante_EmailSdaPort), 4, 0)));
               }
               cmbavContratante_emailsdaaut.CurrentValue = cgiGet( cmbavContratante_emailsdaaut_Internalname);
               AV37Contratante_EmailSdaAut = StringUtil.StrToBool( cgiGet( cmbavContratante_emailsdaaut_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contratante_EmailSdaAut", AV37Contratante_EmailSdaAut);
               cmbavContratante_emailsdasec.CurrentValue = cgiGet( cmbavContratante_emailsdasec_Internalname);
               AV34Contratante_EmailSdaSec = (short)(NumberUtil.Val( cgiGet( cmbavContratante_emailsdasec_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0)));
               AV38PodeDesativar = StringUtil.StrToBool( cgiGet( chkavPodedesativar_Internalname));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratante_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratante_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATANTE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratante_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A29Contratante_Codigo = 0;
                  n29Contratante_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
               }
               else
               {
                  A29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratante_Codigo_Internalname), ",", "."));
                  n29Contratante_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
               }
               n29Contratante_Codigo = ((0==A29Contratante_Codigo) ? true : false);
               A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               /* Read saved values. */
               Z5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z5AreaTrabalho_Codigo"), ",", "."));
               Z6AreaTrabalho_Descricao = cgiGet( "Z6AreaTrabalho_Descricao");
               Z642AreaTrabalho_CalculoPFinal = cgiGet( "Z642AreaTrabalho_CalculoPFinal");
               Z834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cgiGet( "Z834AreaTrabalho_ValidaOSFM"));
               n834AreaTrabalho_ValidaOSFM = ((false==A834AreaTrabalho_ValidaOSFM) ? true : false);
               Z855AreaTrabalho_DiasParaPagar = (short)(context.localUtil.CToN( cgiGet( "Z855AreaTrabalho_DiasParaPagar"), ",", "."));
               n855AreaTrabalho_DiasParaPagar = ((0==A855AreaTrabalho_DiasParaPagar) ? true : false);
               Z987AreaTrabalho_ContratadaUpdBslCod = (int)(context.localUtil.CToN( cgiGet( "Z987AreaTrabalho_ContratadaUpdBslCod"), ",", "."));
               n987AreaTrabalho_ContratadaUpdBslCod = ((0==A987AreaTrabalho_ContratadaUpdBslCod) ? true : false);
               Z1154AreaTrabalho_TipoPlanilha = (short)(context.localUtil.CToN( cgiGet( "Z1154AreaTrabalho_TipoPlanilha"), ",", "."));
               n1154AreaTrabalho_TipoPlanilha = ((0==A1154AreaTrabalho_TipoPlanilha) ? true : false);
               Z72AreaTrabalho_Ativo = StringUtil.StrToBool( cgiGet( "Z72AreaTrabalho_Ativo"));
               Z1588AreaTrabalho_SS_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1588AreaTrabalho_SS_Codigo"), ",", "."));
               n1588AreaTrabalho_SS_Codigo = ((0==A1588AreaTrabalho_SS_Codigo) ? true : false);
               Z2081AreaTrabalho_VerTA = (short)(context.localUtil.CToN( cgiGet( "Z2081AreaTrabalho_VerTA"), ",", "."));
               n2081AreaTrabalho_VerTA = ((0==A2081AreaTrabalho_VerTA) ? true : false);
               Z2080AreaTrabalho_SelUsrPrestadora = StringUtil.StrToBool( cgiGet( "Z2080AreaTrabalho_SelUsrPrestadora"));
               n2080AreaTrabalho_SelUsrPrestadora = ((false==A2080AreaTrabalho_SelUsrPrestadora) ? true : false);
               Z29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z29Contratante_Codigo"), ",", "."));
               n29Contratante_Codigo = ((0==A29Contratante_Codigo) ? true : false);
               Z830AreaTrabalho_ServicoPadrao = (int)(context.localUtil.CToN( cgiGet( "Z830AreaTrabalho_ServicoPadrao"), ",", "."));
               n830AreaTrabalho_ServicoPadrao = ((0==A830AreaTrabalho_ServicoPadrao) ? true : false);
               Z1216AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( "Z1216AreaTrabalho_OrganizacaoCod"), ",", "."));
               n1216AreaTrabalho_OrganizacaoCod = ((0==A1216AreaTrabalho_OrganizacaoCod) ? true : false);
               A1154AreaTrabalho_TipoPlanilha = (short)(context.localUtil.CToN( cgiGet( "Z1154AreaTrabalho_TipoPlanilha"), ",", "."));
               n1154AreaTrabalho_TipoPlanilha = false;
               n1154AreaTrabalho_TipoPlanilha = ((0==A1154AreaTrabalho_TipoPlanilha) ? true : false);
               A1588AreaTrabalho_SS_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1588AreaTrabalho_SS_Codigo"), ",", "."));
               n1588AreaTrabalho_SS_Codigo = false;
               n1588AreaTrabalho_SS_Codigo = ((0==A1588AreaTrabalho_SS_Codigo) ? true : false);
               A2080AreaTrabalho_SelUsrPrestadora = StringUtil.StrToBool( cgiGet( "Z2080AreaTrabalho_SelUsrPrestadora"));
               n2080AreaTrabalho_SelUsrPrestadora = false;
               n2080AreaTrabalho_SelUsrPrestadora = ((false==A2080AreaTrabalho_SelUsrPrestadora) ? true : false);
               A1216AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( "Z1216AreaTrabalho_OrganizacaoCod"), ",", "."));
               n1216AreaTrabalho_OrganizacaoCod = false;
               n1216AreaTrabalho_OrganizacaoCod = ((0==A1216AreaTrabalho_OrganizacaoCod) ? true : false);
               O72AreaTrabalho_Ativo = StringUtil.StrToBool( cgiGet( "O72AreaTrabalho_Ativo"));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1216AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( "N1216AreaTrabalho_OrganizacaoCod"), ",", "."));
               n1216AreaTrabalho_OrganizacaoCod = ((0==A1216AreaTrabalho_OrganizacaoCod) ? true : false);
               N29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( "N29Contratante_Codigo"), ",", "."));
               n29Contratante_Codigo = ((0==A29Contratante_Codigo) ? true : false);
               N830AreaTrabalho_ServicoPadrao = (int)(context.localUtil.CToN( cgiGet( "N830AreaTrabalho_ServicoPadrao"), ",", "."));
               n830AreaTrabalho_ServicoPadrao = ((0==A830AreaTrabalho_ServicoPadrao) ? true : false);
               AV7AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( "vAREATRABALHO_CODIGO"), ",", "."));
               AV29Insert_AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_AREATRABALHO_ORGANIZACAOCOD"), ",", "."));
               A1216AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( "AREATRABALHO_ORGANIZACAOCOD"), ",", "."));
               n1216AreaTrabalho_OrganizacaoCod = ((0==A1216AreaTrabalho_OrganizacaoCod) ? true : false);
               AV11Insert_Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATANTE_CODIGO"), ",", "."));
               AV26Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATANTE_CODIGO"), ",", "."));
               AV27Insert_AreaTrabalho_ServicoPadrao = (int)(context.localUtil.CToN( cgiGet( "vINSERT_AREATRABALHO_SERVICOPADRAO"), ",", "."));
               A12Contratante_CNPJ = cgiGet( "CONTRATANTE_CNPJ");
               n12Contratante_CNPJ = false;
               A14Contratante_Email = cgiGet( "CONTRATANTE_EMAIL");
               n14Contratante_Email = false;
               A33Contratante_Fax = cgiGet( "CONTRATANTE_FAX");
               n33Contratante_Fax = false;
               A11Contratante_IE = cgiGet( "CONTRATANTE_IE");
               A10Contratante_NomeFantasia = cgiGet( "CONTRATANTE_NOMEFANTASIA");
               A32Contratante_Ramal = cgiGet( "CONTRATANTE_RAMAL");
               n32Contratante_Ramal = false;
               A9Contratante_RazaoSocial = cgiGet( "CONTRATANTE_RAZAOSOCIAL");
               n9Contratante_RazaoSocial = false;
               A31Contratante_Telefone = cgiGet( "CONTRATANTE_TELEFONE");
               A13Contratante_WebSite = cgiGet( "CONTRATANTE_WEBSITE");
               n13Contratante_WebSite = false;
               A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( "MUNICIPIO_CODIGO"), ",", "."));
               A23Estado_UF = cgiGet( "ESTADO_UF");
               A547Contratante_EmailSdaHost = cgiGet( "CONTRATANTE_EMAILSDAHOST");
               n547Contratante_EmailSdaHost = false;
               A548Contratante_EmailSdaUser = cgiGet( "CONTRATANTE_EMAILSDAUSER");
               n548Contratante_EmailSdaUser = false;
               A549Contratante_EmailSdaPass = cgiGet( "CONTRATANTE_EMAILSDAPASS");
               n549Contratante_EmailSdaPass = false;
               A550Contratante_EmailSdaKey = cgiGet( "CONTRATANTE_EMAILSDAKEY");
               n550Contratante_EmailSdaKey = false;
               A552Contratante_EmailSdaPort = (short)(context.localUtil.CToN( cgiGet( "CONTRATANTE_EMAILSDAPORT"), ",", "."));
               n552Contratante_EmailSdaPort = false;
               A551Contratante_EmailSdaAut = StringUtil.StrToBool( cgiGet( "CONTRATANTE_EMAILSDAAUT"));
               n551Contratante_EmailSdaAut = false;
               A1048Contratante_EmailSdaSec = (short)(context.localUtil.CToN( cgiGet( "CONTRATANTE_EMAILSDASEC"), ",", "."));
               n1048Contratante_EmailSdaSec = false;
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV8WWPContext);
               ajax_req_read_hidden_sdt(cgiGet( "vAUDITINGOBJECT"), AV30AuditingObject);
               AV39Causa = cgiGet( "vCAUSA");
               A1154AreaTrabalho_TipoPlanilha = (short)(context.localUtil.CToN( cgiGet( "AREATRABALHO_TIPOPLANILHA"), ",", "."));
               n1154AreaTrabalho_TipoPlanilha = ((0==A1154AreaTrabalho_TipoPlanilha) ? true : false);
               A1588AreaTrabalho_SS_Codigo = (int)(context.localUtil.CToN( cgiGet( "AREATRABALHO_SS_CODIGO"), ",", "."));
               n1588AreaTrabalho_SS_Codigo = ((0==A1588AreaTrabalho_SS_Codigo) ? true : false);
               A2080AreaTrabalho_SelUsrPrestadora = StringUtil.StrToBool( cgiGet( "AREATRABALHO_SELUSRPRESTADORA"));
               n2080AreaTrabalho_SelUsrPrestadora = ((false==A2080AreaTrabalho_SelUsrPrestadora) ? true : false);
               A335Contratante_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATANTE_PESSOACOD"), ",", "."));
               A26Municipio_Nome = cgiGet( "MUNICIPIO_NOME");
               A24Estado_Nome = cgiGet( "ESTADO_NOME");
               A272AreaTrabalho_ContagensQtdGeral = (short)(context.localUtil.CToN( cgiGet( "AREATRABALHO_CONTAGENSQTDGERAL"), ",", "."));
               n272AreaTrabalho_ContagensQtdGeral = false;
               AV40Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_contratante_Width = cgiGet( "DVPANEL_CONTRATANTE_Width");
               Dvpanel_contratante_Height = cgiGet( "DVPANEL_CONTRATANTE_Height");
               Dvpanel_contratante_Cls = cgiGet( "DVPANEL_CONTRATANTE_Cls");
               Dvpanel_contratante_Title = cgiGet( "DVPANEL_CONTRATANTE_Title");
               Dvpanel_contratante_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_CONTRATANTE_Collapsible"));
               Dvpanel_contratante_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_CONTRATANTE_Collapsed"));
               Dvpanel_contratante_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_CONTRATANTE_Enabled"));
               Dvpanel_contratante_Class = cgiGet( "DVPANEL_CONTRATANTE_Class");
               Dvpanel_contratante_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_CONTRATANTE_Autowidth"));
               Dvpanel_contratante_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_CONTRATANTE_Autoheight"));
               Dvpanel_contratante_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_CONTRATANTE_Showheader"));
               Dvpanel_contratante_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_CONTRATANTE_Showcollapseicon"));
               Dvpanel_contratante_Iconposition = cgiGet( "DVPANEL_CONTRATANTE_Iconposition");
               Dvpanel_contratante_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_CONTRATANTE_Autoscroll"));
               Dvpanel_contratante_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_CONTRATANTE_Visible"));
               Dvpanel_areadetrabalho_Width = cgiGet( "DVPANEL_AREADETRABALHO_Width");
               Dvpanel_areadetrabalho_Height = cgiGet( "DVPANEL_AREADETRABALHO_Height");
               Dvpanel_areadetrabalho_Cls = cgiGet( "DVPANEL_AREADETRABALHO_Cls");
               Dvpanel_areadetrabalho_Title = cgiGet( "DVPANEL_AREADETRABALHO_Title");
               Dvpanel_areadetrabalho_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_AREADETRABALHO_Collapsible"));
               Dvpanel_areadetrabalho_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_AREADETRABALHO_Collapsed"));
               Dvpanel_areadetrabalho_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_AREADETRABALHO_Enabled"));
               Dvpanel_areadetrabalho_Class = cgiGet( "DVPANEL_AREADETRABALHO_Class");
               Dvpanel_areadetrabalho_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_AREADETRABALHO_Autowidth"));
               Dvpanel_areadetrabalho_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_AREADETRABALHO_Autoheight"));
               Dvpanel_areadetrabalho_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_AREADETRABALHO_Showheader"));
               Dvpanel_areadetrabalho_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_AREADETRABALHO_Showcollapseicon"));
               Dvpanel_areadetrabalho_Iconposition = cgiGet( "DVPANEL_AREADETRABALHO_Iconposition");
               Dvpanel_areadetrabalho_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_AREADETRABALHO_Autoscroll"));
               Dvpanel_areadetrabalho_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_AREADETRABALHO_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "AreaTrabalho";
               A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV40Pgmname, ""));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1154AreaTrabalho_TipoPlanilha), "Z9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1588AreaTrabalho_SS_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A2080AreaTrabalho_SelUsrPrestadora);
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("areatrabalho:[SecurityCheckFailed value for]"+"AreaTrabalho_Codigo:"+context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("areatrabalho:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("areatrabalho:[SecurityCheckFailed value for]"+"AreaTrabalho_OrganizacaoCod:"+context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9"));
                  GXUtil.WriteLog("areatrabalho:[SecurityCheckFailed value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV40Pgmname, "")));
                  GXUtil.WriteLog("areatrabalho:[SecurityCheckFailed value for]"+"AreaTrabalho_TipoPlanilha:"+context.localUtil.Format( (decimal)(A1154AreaTrabalho_TipoPlanilha), "Z9"));
                  GXUtil.WriteLog("areatrabalho:[SecurityCheckFailed value for]"+"AreaTrabalho_SS_Codigo:"+context.localUtil.Format( (decimal)(A1588AreaTrabalho_SS_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("areatrabalho:[SecurityCheckFailed value for]"+"AreaTrabalho_SelUsrPrestadora:"+StringUtil.BoolToStr( A2080AreaTrabalho_SelUsrPrestadora));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode106 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode106;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound106 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_040( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "AREATRABALHO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11042 */
                           E11042 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12042 */
                           E12042 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VCONTRATANTE_CNPJ.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13042 */
                           E13042 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12042 */
            E12042 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll04106( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes04106( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_cnpj_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_ie_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_ie_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_razaosocial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_nomefantasia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_nomefantasia_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_website_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_website_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_email_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_email_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_telefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_telefone_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_ramal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_ramal_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_fax_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_fax_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavMunicipio_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavMunicipio_codigo.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdahost_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdahost_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdauser_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdauser_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdapass_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdapass_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdaport_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdaport_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratante_emailsdaaut_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratante_emailsdaaut.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratante_emailsdasec_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratante_emailsdasec.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavPodedesativar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavPodedesativar.Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_040( )
      {
         BeforeValidate04106( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls04106( ) ;
            }
            else
            {
               CheckExtendedTable04106( ) ;
               CloseExtendedTableCursors04106( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption040( )
      {
      }

      protected void E11042( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV40Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV41GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GXV1), 8, 0)));
            while ( AV41GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV41GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "AreaTrabalho_OrganizacaoCod") == 0 )
               {
                  AV29Insert_AreaTrabalho_OrganizacaoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Insert_AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Insert_AreaTrabalho_OrganizacaoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Contratante_Codigo") == 0 )
               {
                  AV11Insert_Contratante_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contratante_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "AreaTrabalho_ServicoPadrao") == 0 )
               {
                  AV27Insert_AreaTrabalho_ServicoPadrao = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Insert_AreaTrabalho_ServicoPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Insert_AreaTrabalho_ServicoPadrao), 6, 0)));
               }
               AV41GXV1 = (int)(AV41GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GXV1), 8, 0)));
            }
         }
         chkavPodedesativar.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavPodedesativar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavPodedesativar.Visible), 5, 0)));
         edtContratante_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Codigo_Visible), 5, 0)));
         edtAreaTrabalho_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Codigo_Visible), 5, 0)));
         AV39Causa = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Causa", AV39Causa);
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            AV31ParametrosSistema.Load(1);
            AV32Contratante_EmailSdaHost = AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdahost;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contratante_EmailSdaHost", AV32Contratante_EmailSdaHost);
            AV33Contratante_EmailSdaUser = AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdauser;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratante_EmailSdaUser", AV33Contratante_EmailSdaUser);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdakey)) )
            {
               AV35Contratante_EmailSdaPass = Crypto.Decrypt64( AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdapass, AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdakey);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratante_EmailSdaPass", AV35Contratante_EmailSdaPass);
            }
            AV36Contratante_EmailSdaPort = AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdaport;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contratante_EmailSdaPort), 4, 0)));
            AV37Contratante_EmailSdaAut = AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdaaut;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contratante_EmailSdaAut", AV37Contratante_EmailSdaAut);
            AV34Contratante_EmailSdaSec = AV31ParametrosSistema.gxTpr_Parametrossistema_emailsdasec;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0)));
         }
      }

      protected void E12042( )
      {
         /* After Trn Routine */
         AV8WWPContext.gxTpr_Servicopadrao = (short)(A830AreaTrabalho_ServicoPadrao);
         new wwpbaseobjects.setwwpcontext(context ).execute(  AV8WWPContext) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            new prc_newperfilgestor(context ).execute( ref  A5AreaTrabalho_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         }
         new wwpbaseobjects.audittransaction(context ).execute(  AV30AuditingObject,  AV40Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Pgmname", AV40Pgmname);
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwareatrabalho.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV8WWPContext", AV8WWPContext);
      }

      protected void E13042( )
      {
         /* Contratante_cnpj_Isvalid Routine */
         if ( new prc_validadocumento(context).executeUdp(  AV15Contratante_CNPJ,  "J") )
         {
            GX_msglist.addItem("Documento inv�lido. (Poder� continuar com o teste)!");
         }
         GXt_int1 = AV26Contratante_Codigo;
         new prc_retornacontratantepelocnpj(context ).execute(  AV15Contratante_CNPJ, out  AV20Contratante_Email, out  AV23Contratante_Fax, out  AV18Contratante_IE, out  AV17Contratante_NomeFantasia, out  AV22Contratante_Ramal, out  AV16Contratante_RazaoSocial, out  AV21Contratante_Telefone, out  AV19Contratante_WebSite, ref  AV25Municipio_Codigo, ref  AV24Estado_UF, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratante_CNPJ", AV15Contratante_CNPJ);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratante_Email", AV20Contratante_Email);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratante_Fax", AV23Contratante_Fax);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratante_IE", AV18Contratante_IE);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratante_NomeFantasia", AV17Contratante_NomeFantasia);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratante_Ramal", AV22Contratante_Ramal);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contratante_RazaoSocial", AV16Contratante_RazaoSocial);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratante_Telefone", AV21Contratante_Telefone);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratante_WebSite", AV19Contratante_WebSite);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Estado_UF", AV24Estado_UF);
         AV26Contratante_Codigo = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Contratante_Codigo), 6, 0)));
      }

      protected void ZM04106( short GX_JID )
      {
         if ( ( GX_JID == 72 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z6AreaTrabalho_Descricao = T00043_A6AreaTrabalho_Descricao[0];
               Z642AreaTrabalho_CalculoPFinal = T00043_A642AreaTrabalho_CalculoPFinal[0];
               Z834AreaTrabalho_ValidaOSFM = T00043_A834AreaTrabalho_ValidaOSFM[0];
               Z855AreaTrabalho_DiasParaPagar = T00043_A855AreaTrabalho_DiasParaPagar[0];
               Z987AreaTrabalho_ContratadaUpdBslCod = T00043_A987AreaTrabalho_ContratadaUpdBslCod[0];
               Z1154AreaTrabalho_TipoPlanilha = T00043_A1154AreaTrabalho_TipoPlanilha[0];
               Z72AreaTrabalho_Ativo = T00043_A72AreaTrabalho_Ativo[0];
               Z1588AreaTrabalho_SS_Codigo = T00043_A1588AreaTrabalho_SS_Codigo[0];
               Z2081AreaTrabalho_VerTA = T00043_A2081AreaTrabalho_VerTA[0];
               Z2080AreaTrabalho_SelUsrPrestadora = T00043_A2080AreaTrabalho_SelUsrPrestadora[0];
               Z29Contratante_Codigo = T00043_A29Contratante_Codigo[0];
               Z830AreaTrabalho_ServicoPadrao = T00043_A830AreaTrabalho_ServicoPadrao[0];
               Z1216AreaTrabalho_OrganizacaoCod = T00043_A1216AreaTrabalho_OrganizacaoCod[0];
            }
            else
            {
               Z6AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
               Z642AreaTrabalho_CalculoPFinal = A642AreaTrabalho_CalculoPFinal;
               Z834AreaTrabalho_ValidaOSFM = A834AreaTrabalho_ValidaOSFM;
               Z855AreaTrabalho_DiasParaPagar = A855AreaTrabalho_DiasParaPagar;
               Z987AreaTrabalho_ContratadaUpdBslCod = A987AreaTrabalho_ContratadaUpdBslCod;
               Z1154AreaTrabalho_TipoPlanilha = A1154AreaTrabalho_TipoPlanilha;
               Z72AreaTrabalho_Ativo = A72AreaTrabalho_Ativo;
               Z1588AreaTrabalho_SS_Codigo = A1588AreaTrabalho_SS_Codigo;
               Z2081AreaTrabalho_VerTA = A2081AreaTrabalho_VerTA;
               Z2080AreaTrabalho_SelUsrPrestadora = A2080AreaTrabalho_SelUsrPrestadora;
               Z29Contratante_Codigo = A29Contratante_Codigo;
               Z830AreaTrabalho_ServicoPadrao = A830AreaTrabalho_ServicoPadrao;
               Z1216AreaTrabalho_OrganizacaoCod = A1216AreaTrabalho_OrganizacaoCod;
            }
         }
         if ( GX_JID == -72 )
         {
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z6AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
            Z642AreaTrabalho_CalculoPFinal = A642AreaTrabalho_CalculoPFinal;
            Z834AreaTrabalho_ValidaOSFM = A834AreaTrabalho_ValidaOSFM;
            Z855AreaTrabalho_DiasParaPagar = A855AreaTrabalho_DiasParaPagar;
            Z987AreaTrabalho_ContratadaUpdBslCod = A987AreaTrabalho_ContratadaUpdBslCod;
            Z1154AreaTrabalho_TipoPlanilha = A1154AreaTrabalho_TipoPlanilha;
            Z72AreaTrabalho_Ativo = A72AreaTrabalho_Ativo;
            Z1588AreaTrabalho_SS_Codigo = A1588AreaTrabalho_SS_Codigo;
            Z2081AreaTrabalho_VerTA = A2081AreaTrabalho_VerTA;
            Z2080AreaTrabalho_SelUsrPrestadora = A2080AreaTrabalho_SelUsrPrestadora;
            Z29Contratante_Codigo = A29Contratante_Codigo;
            Z830AreaTrabalho_ServicoPadrao = A830AreaTrabalho_ServicoPadrao;
            Z1216AreaTrabalho_OrganizacaoCod = A1216AreaTrabalho_OrganizacaoCod;
            Z1214Organizacao_Nome = A1214Organizacao_Nome;
            Z272AreaTrabalho_ContagensQtdGeral = A272AreaTrabalho_ContagensQtdGeral;
            Z33Contratante_Fax = A33Contratante_Fax;
            Z32Contratante_Ramal = A32Contratante_Ramal;
            Z31Contratante_Telefone = A31Contratante_Telefone;
            Z14Contratante_Email = A14Contratante_Email;
            Z13Contratante_WebSite = A13Contratante_WebSite;
            Z11Contratante_IE = A11Contratante_IE;
            Z10Contratante_NomeFantasia = A10Contratante_NomeFantasia;
            Z547Contratante_EmailSdaHost = A547Contratante_EmailSdaHost;
            Z548Contratante_EmailSdaUser = A548Contratante_EmailSdaUser;
            Z549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
            Z550Contratante_EmailSdaKey = A550Contratante_EmailSdaKey;
            Z551Contratante_EmailSdaAut = A551Contratante_EmailSdaAut;
            Z552Contratante_EmailSdaPort = A552Contratante_EmailSdaPort;
            Z1048Contratante_EmailSdaSec = A1048Contratante_EmailSdaSec;
            Z25Municipio_Codigo = A25Municipio_Codigo;
            Z335Contratante_PessoaCod = A335Contratante_PessoaCod;
            Z26Municipio_Nome = A26Municipio_Nome;
            Z23Estado_UF = A23Estado_UF;
            Z24Estado_Nome = A24Estado_Nome;
            Z12Contratante_CNPJ = A12Contratante_CNPJ;
            Z9Contratante_RazaoSocial = A9Contratante_RazaoSocial;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAAREATRABALHO_SERVICOPADRAO_html04106( ) ;
         GXVvESTADO_UF_html04106( ) ;
         edtOrganizacao_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtOrganizacao_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtOrganizacao_Nome_Enabled), 5, 0)));
         edtAreaTrabalho_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV40Pgmname = "AreaTrabalho";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Pgmname", AV40Pgmname);
         edtOrganizacao_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtOrganizacao_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtOrganizacao_Nome_Enabled), 5, 0)));
         edtAreaTrabalho_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7AreaTrabalho_Codigo) )
         {
            A5AreaTrabalho_Codigo = AV7AreaTrabalho_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         }
         GXAAREATRABALHO_CONTRATADAUPDBSLCOD_html04106( AV8WWPContext) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contratante_Codigo) )
         {
            edtContratante_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtContratante_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Codigo_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV27Insert_AreaTrabalho_ServicoPadrao) )
         {
            dynAreaTrabalho_ServicoPadrao.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAreaTrabalho_ServicoPadrao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAreaTrabalho_ServicoPadrao.Enabled), 5, 0)));
         }
         else
         {
            dynAreaTrabalho_ServicoPadrao.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAreaTrabalho_ServicoPadrao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAreaTrabalho_ServicoPadrao.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         chkAreaTrabalho_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAreaTrabalho_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkAreaTrabalho_Ativo.Visible), 5, 0)));
         lblTextblockareatrabalho_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockareatrabalho_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockareatrabalho_ativo_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV27Insert_AreaTrabalho_ServicoPadrao) )
         {
            A830AreaTrabalho_ServicoPadrao = AV27Insert_AreaTrabalho_ServicoPadrao;
            n830AreaTrabalho_ServicoPadrao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A830AreaTrabalho_ServicoPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contratante_Codigo) )
         {
            A29Contratante_Codigo = AV11Insert_Contratante_Codigo;
            n29Contratante_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV29Insert_AreaTrabalho_OrganizacaoCod) )
         {
            A1216AreaTrabalho_OrganizacaoCod = AV29Insert_AreaTrabalho_OrganizacaoCod;
            n1216AreaTrabalho_OrganizacaoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1216AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A72AreaTrabalho_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A72AreaTrabalho_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72AreaTrabalho_Ativo", A72AreaTrabalho_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal)) && ( Gx_BScreen == 0 ) )
         {
            A642AreaTrabalho_CalculoPFinal = "MB";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            GXVvMUNICIPIO_CODIGO_html04106( AV24Estado_UF) ;
            /* Using cursor T000411 */
            pr_default.execute(8, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(8) != 101) )
            {
               A272AreaTrabalho_ContagensQtdGeral = T000411_A272AreaTrabalho_ContagensQtdGeral[0];
               n272AreaTrabalho_ContagensQtdGeral = T000411_n272AreaTrabalho_ContagensQtdGeral[0];
            }
            else
            {
               A272AreaTrabalho_ContagensQtdGeral = 0;
               n272AreaTrabalho_ContagensQtdGeral = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A272AreaTrabalho_ContagensQtdGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0)));
            }
            pr_default.close(8);
            /* Using cursor T00044 */
            pr_default.execute(2, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            A33Contratante_Fax = T00044_A33Contratante_Fax[0];
            n33Contratante_Fax = T00044_n33Contratante_Fax[0];
            A32Contratante_Ramal = T00044_A32Contratante_Ramal[0];
            n32Contratante_Ramal = T00044_n32Contratante_Ramal[0];
            A31Contratante_Telefone = T00044_A31Contratante_Telefone[0];
            A14Contratante_Email = T00044_A14Contratante_Email[0];
            n14Contratante_Email = T00044_n14Contratante_Email[0];
            A13Contratante_WebSite = T00044_A13Contratante_WebSite[0];
            n13Contratante_WebSite = T00044_n13Contratante_WebSite[0];
            A11Contratante_IE = T00044_A11Contratante_IE[0];
            A10Contratante_NomeFantasia = T00044_A10Contratante_NomeFantasia[0];
            A547Contratante_EmailSdaHost = T00044_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = T00044_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = T00044_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = T00044_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = T00044_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = T00044_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = T00044_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = T00044_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = T00044_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = T00044_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = T00044_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = T00044_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = T00044_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = T00044_n1048Contratante_EmailSdaSec[0];
            A25Municipio_Codigo = T00044_A25Municipio_Codigo[0];
            n25Municipio_Codigo = T00044_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = T00044_A335Contratante_PessoaCod[0];
            pr_default.close(2);
            /* Using cursor T00047 */
            pr_default.execute(5, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
            A26Municipio_Nome = T00047_A26Municipio_Nome[0];
            A23Estado_UF = T00047_A23Estado_UF[0];
            pr_default.close(5);
            /* Using cursor T00048 */
            pr_default.execute(6, new Object[] {A23Estado_UF});
            A24Estado_Nome = T00048_A24Estado_Nome[0];
            pr_default.close(6);
            /* Using cursor T00049 */
            pr_default.execute(7, new Object[] {A335Contratante_PessoaCod});
            A12Contratante_CNPJ = T00049_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = T00049_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = T00049_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = T00049_n9Contratante_RazaoSocial[0];
            pr_default.close(7);
            /* Using cursor T00046 */
            pr_default.execute(4, new Object[] {n1216AreaTrabalho_OrganizacaoCod, A1216AreaTrabalho_OrganizacaoCod});
            A1214Organizacao_Nome = T00046_A1214Organizacao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
            n1214Organizacao_Nome = T00046_n1214Organizacao_Nome[0];
            pr_default.close(4);
         }
      }

      protected void Load04106( )
      {
         /* Using cursor T000413 */
         pr_default.execute(9, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound106 = 1;
            A6AreaTrabalho_Descricao = T000413_A6AreaTrabalho_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
            A1214Organizacao_Nome = T000413_A1214Organizacao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
            n1214Organizacao_Nome = T000413_n1214Organizacao_Nome[0];
            A24Estado_Nome = T000413_A24Estado_Nome[0];
            A26Municipio_Nome = T000413_A26Municipio_Nome[0];
            A33Contratante_Fax = T000413_A33Contratante_Fax[0];
            n33Contratante_Fax = T000413_n33Contratante_Fax[0];
            A32Contratante_Ramal = T000413_A32Contratante_Ramal[0];
            n32Contratante_Ramal = T000413_n32Contratante_Ramal[0];
            A31Contratante_Telefone = T000413_A31Contratante_Telefone[0];
            A14Contratante_Email = T000413_A14Contratante_Email[0];
            n14Contratante_Email = T000413_n14Contratante_Email[0];
            A13Contratante_WebSite = T000413_A13Contratante_WebSite[0];
            n13Contratante_WebSite = T000413_n13Contratante_WebSite[0];
            A12Contratante_CNPJ = T000413_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = T000413_n12Contratante_CNPJ[0];
            A11Contratante_IE = T000413_A11Contratante_IE[0];
            A10Contratante_NomeFantasia = T000413_A10Contratante_NomeFantasia[0];
            A9Contratante_RazaoSocial = T000413_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = T000413_n9Contratante_RazaoSocial[0];
            A547Contratante_EmailSdaHost = T000413_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = T000413_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = T000413_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = T000413_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = T000413_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = T000413_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = T000413_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = T000413_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = T000413_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = T000413_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = T000413_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = T000413_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = T000413_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = T000413_n1048Contratante_EmailSdaSec[0];
            A642AreaTrabalho_CalculoPFinal = T000413_A642AreaTrabalho_CalculoPFinal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
            A834AreaTrabalho_ValidaOSFM = T000413_A834AreaTrabalho_ValidaOSFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A834AreaTrabalho_ValidaOSFM", A834AreaTrabalho_ValidaOSFM);
            n834AreaTrabalho_ValidaOSFM = T000413_n834AreaTrabalho_ValidaOSFM[0];
            A855AreaTrabalho_DiasParaPagar = T000413_A855AreaTrabalho_DiasParaPagar[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A855AreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0)));
            n855AreaTrabalho_DiasParaPagar = T000413_n855AreaTrabalho_DiasParaPagar[0];
            A987AreaTrabalho_ContratadaUpdBslCod = T000413_A987AreaTrabalho_ContratadaUpdBslCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A987AreaTrabalho_ContratadaUpdBslCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0)));
            n987AreaTrabalho_ContratadaUpdBslCod = T000413_n987AreaTrabalho_ContratadaUpdBslCod[0];
            A1154AreaTrabalho_TipoPlanilha = T000413_A1154AreaTrabalho_TipoPlanilha[0];
            n1154AreaTrabalho_TipoPlanilha = T000413_n1154AreaTrabalho_TipoPlanilha[0];
            A72AreaTrabalho_Ativo = T000413_A72AreaTrabalho_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72AreaTrabalho_Ativo", A72AreaTrabalho_Ativo);
            A1588AreaTrabalho_SS_Codigo = T000413_A1588AreaTrabalho_SS_Codigo[0];
            n1588AreaTrabalho_SS_Codigo = T000413_n1588AreaTrabalho_SS_Codigo[0];
            A2081AreaTrabalho_VerTA = T000413_A2081AreaTrabalho_VerTA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2081AreaTrabalho_VerTA", StringUtil.LTrim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)));
            n2081AreaTrabalho_VerTA = T000413_n2081AreaTrabalho_VerTA[0];
            A2080AreaTrabalho_SelUsrPrestadora = T000413_A2080AreaTrabalho_SelUsrPrestadora[0];
            n2080AreaTrabalho_SelUsrPrestadora = T000413_n2080AreaTrabalho_SelUsrPrestadora[0];
            A29Contratante_Codigo = T000413_A29Contratante_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            n29Contratante_Codigo = T000413_n29Contratante_Codigo[0];
            A830AreaTrabalho_ServicoPadrao = T000413_A830AreaTrabalho_ServicoPadrao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A830AreaTrabalho_ServicoPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0)));
            n830AreaTrabalho_ServicoPadrao = T000413_n830AreaTrabalho_ServicoPadrao[0];
            A1216AreaTrabalho_OrganizacaoCod = T000413_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = T000413_n1216AreaTrabalho_OrganizacaoCod[0];
            A25Municipio_Codigo = T000413_A25Municipio_Codigo[0];
            n25Municipio_Codigo = T000413_n25Municipio_Codigo[0];
            A23Estado_UF = T000413_A23Estado_UF[0];
            A335Contratante_PessoaCod = T000413_A335Contratante_PessoaCod[0];
            A272AreaTrabalho_ContagensQtdGeral = T000413_A272AreaTrabalho_ContagensQtdGeral[0];
            n272AreaTrabalho_ContagensQtdGeral = T000413_n272AreaTrabalho_ContagensQtdGeral[0];
            ZM04106( -72) ;
         }
         pr_default.close(9);
         OnLoadActions04106( ) ;
      }

      protected void OnLoadActions04106( )
      {
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV23Contratante_Fax = A33Contratante_Fax;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratante_Fax", AV23Contratante_Fax);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV22Contratante_Ramal = A32Contratante_Ramal;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratante_Ramal", AV22Contratante_Ramal);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV21Contratante_Telefone = A31Contratante_Telefone;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratante_Telefone", AV21Contratante_Telefone);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV20Contratante_Email = A14Contratante_Email;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratante_Email", AV20Contratante_Email);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV19Contratante_WebSite = A13Contratante_WebSite;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratante_WebSite", AV19Contratante_WebSite);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV18Contratante_IE = A11Contratante_IE;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratante_IE", AV18Contratante_IE);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV17Contratante_NomeFantasia = A10Contratante_NomeFantasia;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratante_NomeFantasia", AV17Contratante_NomeFantasia);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV32Contratante_EmailSdaHost = A547Contratante_EmailSdaHost;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contratante_EmailSdaHost", AV32Contratante_EmailSdaHost);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV33Contratante_EmailSdaUser = A548Contratante_EmailSdaUser;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratante_EmailSdaUser", AV33Contratante_EmailSdaUser);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ! String.IsNullOrEmpty(StringUtil.RTrim( A550Contratante_EmailSdaKey)) )
         {
            AV35Contratante_EmailSdaPass = Crypto.Decrypt64( A549Contratante_EmailSdaPass, A550Contratante_EmailSdaKey);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratante_EmailSdaPass", AV35Contratante_EmailSdaPass);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV37Contratante_EmailSdaAut = A551Contratante_EmailSdaAut;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contratante_EmailSdaAut", AV37Contratante_EmailSdaAut);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV36Contratante_EmailSdaPort = A552Contratante_EmailSdaPort;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contratante_EmailSdaPort), 4, 0)));
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV34Contratante_EmailSdaSec = A1048Contratante_EmailSdaSec;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0)));
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV24Estado_UF = A23Estado_UF;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Estado_UF", AV24Estado_UF);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV25Municipio_Codigo = A25Municipio_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0)));
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV15Contratante_CNPJ = A12Contratante_CNPJ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratante_CNPJ", AV15Contratante_CNPJ);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV16Contratante_RazaoSocial = A9Contratante_RazaoSocial;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contratante_RazaoSocial", AV16Contratante_RazaoSocial);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV26Contratante_Codigo = A29Contratante_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Contratante_Codigo), 6, 0)));
         }
         edtavContratante_cnpj_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_cnpj_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj_Enabled), 5, 0)));
         edtavContratante_email_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_email_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_email_Enabled), 5, 0)));
         edtavContratante_fax_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_fax_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_fax_Enabled), 5, 0)));
         edtavContratante_ie_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_ie_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_ie_Enabled), 5, 0)));
         edtavContratante_nomefantasia_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_nomefantasia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_nomefantasia_Enabled), 5, 0)));
         edtavContratante_ramal_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_ramal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_ramal_Enabled), 5, 0)));
         edtavContratante_razaosocial_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_razaosocial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial_Enabled), 5, 0)));
         edtavContratante_telefone_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_telefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_telefone_Enabled), 5, 0)));
         edtavContratante_website_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_website_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_website_Enabled), 5, 0)));
         dynavMunicipio_codigo.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavMunicipio_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavMunicipio_codigo.Enabled), 5, 0)));
         dynavEstado_uf.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf.Enabled), 5, 0)));
         edtavContratante_emailsdahost_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdahost_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdahost_Enabled), 5, 0)));
         edtavContratante_emailsdauser_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdauser_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdauser_Enabled), 5, 0)));
         edtavContratante_emailsdapass_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdapass_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdapass_Enabled), 5, 0)));
         edtavContratante_emailsdaport_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdaport_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdaport_Enabled), 5, 0)));
         cmbavContratante_emailsdaaut.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratante_emailsdaaut_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratante_emailsdaaut.Enabled), 5, 0)));
         cmbavContratante_emailsdasec.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratante_emailsdasec_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratante_emailsdasec.Enabled), 5, 0)));
         GXVvMUNICIPIO_CODIGO_html04106( AV24Estado_UF) ;
      }

      protected void CheckExtendedTable04106( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T000411 */
         pr_default.execute(8, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            A272AreaTrabalho_ContagensQtdGeral = T000411_A272AreaTrabalho_ContagensQtdGeral[0];
            n272AreaTrabalho_ContagensQtdGeral = T000411_n272AreaTrabalho_ContagensQtdGeral[0];
         }
         else
         {
            A272AreaTrabalho_ContagensQtdGeral = 0;
            n272AreaTrabalho_ContagensQtdGeral = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A272AreaTrabalho_ContagensQtdGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0)));
         }
         pr_default.close(8);
         if ( ! A72AreaTrabalho_Ativo && ( O72AreaTrabalho_Ativo ) && ! new prc_podedesativararea(context).executeUdp( ref  A5AreaTrabalho_Codigo, out  AV39Causa) )
         {
            GX_msglist.addItem(AV39Causa, 1, "AREATRABALHO_ATIVO");
            AnyError = 1;
            GX_FocusControl = chkAreaTrabalho_Ativo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A6AreaTrabalho_Descricao)) )
         {
            GX_msglist.addItem("�rea de Trabalho � obrigat�rio.", 1, "AREATRABALHO_DESCRICAO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00044 */
         pr_default.execute(2, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A29Contratante_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Contratante'.", "ForeignKeyNotFound", 1, "CONTRATANTE_CODIGO");
               AnyError = 1;
               GX_FocusControl = edtContratante_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A33Contratante_Fax = T00044_A33Contratante_Fax[0];
         n33Contratante_Fax = T00044_n33Contratante_Fax[0];
         A32Contratante_Ramal = T00044_A32Contratante_Ramal[0];
         n32Contratante_Ramal = T00044_n32Contratante_Ramal[0];
         A31Contratante_Telefone = T00044_A31Contratante_Telefone[0];
         A14Contratante_Email = T00044_A14Contratante_Email[0];
         n14Contratante_Email = T00044_n14Contratante_Email[0];
         A13Contratante_WebSite = T00044_A13Contratante_WebSite[0];
         n13Contratante_WebSite = T00044_n13Contratante_WebSite[0];
         A11Contratante_IE = T00044_A11Contratante_IE[0];
         A10Contratante_NomeFantasia = T00044_A10Contratante_NomeFantasia[0];
         A547Contratante_EmailSdaHost = T00044_A547Contratante_EmailSdaHost[0];
         n547Contratante_EmailSdaHost = T00044_n547Contratante_EmailSdaHost[0];
         A548Contratante_EmailSdaUser = T00044_A548Contratante_EmailSdaUser[0];
         n548Contratante_EmailSdaUser = T00044_n548Contratante_EmailSdaUser[0];
         A549Contratante_EmailSdaPass = T00044_A549Contratante_EmailSdaPass[0];
         n549Contratante_EmailSdaPass = T00044_n549Contratante_EmailSdaPass[0];
         A550Contratante_EmailSdaKey = T00044_A550Contratante_EmailSdaKey[0];
         n550Contratante_EmailSdaKey = T00044_n550Contratante_EmailSdaKey[0];
         A551Contratante_EmailSdaAut = T00044_A551Contratante_EmailSdaAut[0];
         n551Contratante_EmailSdaAut = T00044_n551Contratante_EmailSdaAut[0];
         A552Contratante_EmailSdaPort = T00044_A552Contratante_EmailSdaPort[0];
         n552Contratante_EmailSdaPort = T00044_n552Contratante_EmailSdaPort[0];
         A1048Contratante_EmailSdaSec = T00044_A1048Contratante_EmailSdaSec[0];
         n1048Contratante_EmailSdaSec = T00044_n1048Contratante_EmailSdaSec[0];
         A25Municipio_Codigo = T00044_A25Municipio_Codigo[0];
         n25Municipio_Codigo = T00044_n25Municipio_Codigo[0];
         A335Contratante_PessoaCod = T00044_A335Contratante_PessoaCod[0];
         pr_default.close(2);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV23Contratante_Fax = A33Contratante_Fax;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratante_Fax", AV23Contratante_Fax);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV22Contratante_Ramal = A32Contratante_Ramal;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratante_Ramal", AV22Contratante_Ramal);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV21Contratante_Telefone = A31Contratante_Telefone;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratante_Telefone", AV21Contratante_Telefone);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV20Contratante_Email = A14Contratante_Email;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratante_Email", AV20Contratante_Email);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV19Contratante_WebSite = A13Contratante_WebSite;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratante_WebSite", AV19Contratante_WebSite);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV18Contratante_IE = A11Contratante_IE;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratante_IE", AV18Contratante_IE);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV17Contratante_NomeFantasia = A10Contratante_NomeFantasia;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratante_NomeFantasia", AV17Contratante_NomeFantasia);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV32Contratante_EmailSdaHost = A547Contratante_EmailSdaHost;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contratante_EmailSdaHost", AV32Contratante_EmailSdaHost);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV33Contratante_EmailSdaUser = A548Contratante_EmailSdaUser;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratante_EmailSdaUser", AV33Contratante_EmailSdaUser);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ! String.IsNullOrEmpty(StringUtil.RTrim( A550Contratante_EmailSdaKey)) )
         {
            AV35Contratante_EmailSdaPass = Crypto.Decrypt64( A549Contratante_EmailSdaPass, A550Contratante_EmailSdaKey);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratante_EmailSdaPass", AV35Contratante_EmailSdaPass);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV37Contratante_EmailSdaAut = A551Contratante_EmailSdaAut;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contratante_EmailSdaAut", AV37Contratante_EmailSdaAut);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV36Contratante_EmailSdaPort = A552Contratante_EmailSdaPort;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contratante_EmailSdaPort), 4, 0)));
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV34Contratante_EmailSdaSec = A1048Contratante_EmailSdaSec;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0)));
         }
         /* Using cursor T00047 */
         pr_default.execute(5, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A25Municipio_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Municipio'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A26Municipio_Nome = T00047_A26Municipio_Nome[0];
         A23Estado_UF = T00047_A23Estado_UF[0];
         pr_default.close(5);
         /* Using cursor T00048 */
         pr_default.execute(6, new Object[] {A23Estado_UF});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A23Estado_UF)) ) )
            {
               GX_msglist.addItem("N�o existe 'Estado'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A24Estado_Nome = T00048_A24Estado_Nome[0];
         pr_default.close(6);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV24Estado_UF = A23Estado_UF;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Estado_UF", AV24Estado_UF);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV25Municipio_Codigo = A25Municipio_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0)));
         }
         /* Using cursor T00049 */
         pr_default.execute(7, new Object[] {A335Contratante_PessoaCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A335Contratante_PessoaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratante_Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A12Contratante_CNPJ = T00049_A12Contratante_CNPJ[0];
         n12Contratante_CNPJ = T00049_n12Contratante_CNPJ[0];
         A9Contratante_RazaoSocial = T00049_A9Contratante_RazaoSocial[0];
         n9Contratante_RazaoSocial = T00049_n9Contratante_RazaoSocial[0];
         pr_default.close(7);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV15Contratante_CNPJ = A12Contratante_CNPJ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratante_CNPJ", AV15Contratante_CNPJ);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV16Contratante_RazaoSocial = A9Contratante_RazaoSocial;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contratante_RazaoSocial", AV16Contratante_RazaoSocial);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV26Contratante_Codigo = A29Contratante_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Contratante_Codigo), 6, 0)));
         }
         /* Using cursor T00045 */
         pr_default.execute(3, new Object[] {n830AreaTrabalho_ServicoPadrao, A830AreaTrabalho_ServicoPadrao});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A830AreaTrabalho_ServicoPadrao) ) )
            {
               GX_msglist.addItem("N�o existe 'Area Trabalho_Servicos'.", "ForeignKeyNotFound", 1, "AREATRABALHO_SERVICOPADRAO");
               AnyError = 1;
               GX_FocusControl = dynAreaTrabalho_ServicoPadrao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(3);
         edtavContratante_cnpj_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_cnpj_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj_Enabled), 5, 0)));
         edtavContratante_email_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_email_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_email_Enabled), 5, 0)));
         edtavContratante_fax_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_fax_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_fax_Enabled), 5, 0)));
         edtavContratante_ie_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_ie_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_ie_Enabled), 5, 0)));
         edtavContratante_nomefantasia_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_nomefantasia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_nomefantasia_Enabled), 5, 0)));
         edtavContratante_ramal_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_ramal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_ramal_Enabled), 5, 0)));
         edtavContratante_razaosocial_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_razaosocial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial_Enabled), 5, 0)));
         edtavContratante_telefone_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_telefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_telefone_Enabled), 5, 0)));
         edtavContratante_website_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_website_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_website_Enabled), 5, 0)));
         dynavMunicipio_codigo.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavMunicipio_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavMunicipio_codigo.Enabled), 5, 0)));
         dynavEstado_uf.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf.Enabled), 5, 0)));
         edtavContratante_emailsdahost_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdahost_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdahost_Enabled), 5, 0)));
         edtavContratante_emailsdauser_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdauser_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdauser_Enabled), 5, 0)));
         edtavContratante_emailsdapass_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdapass_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdapass_Enabled), 5, 0)));
         edtavContratante_emailsdaport_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdaport_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdaport_Enabled), 5, 0)));
         cmbavContratante_emailsdaaut.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratante_emailsdaaut_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratante_emailsdaaut.Enabled), 5, 0)));
         cmbavContratante_emailsdasec.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratante_emailsdasec_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratante_emailsdasec.Enabled), 5, 0)));
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) )
         {
            GX_msglist.addItem("CNPJ � obrigat�rio.", 1, "vCONTRATANTE_CNPJ");
            AnyError = 1;
            GX_FocusControl = edtavContratante_cnpj_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratante_IE)) )
         {
            GX_msglist.addItem("Insc. Estadual � obrigat�rio.", 1, "vCONTRATANTE_IE");
            AnyError = 1;
            GX_FocusControl = edtavContratante_ie_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV16Contratante_RazaoSocial)) )
         {
            GX_msglist.addItem("Raz�o Social � obrigat�rio.", 1, "vCONTRATANTE_RAZAOSOCIAL");
            AnyError = 1;
            GX_FocusControl = edtavContratante_razaosocial_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17Contratante_NomeFantasia)) )
         {
            GX_msglist.addItem("Nome Fantasia � obrigat�rio.", 1, "vCONTRATANTE_NOMEFANTASIA");
            AnyError = 1;
            GX_FocusControl = edtavContratante_nomefantasia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( GxRegex.IsMatch(AV20Contratante_Email,"^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") || String.IsNullOrEmpty(StringUtil.RTrim( AV20Contratante_Email)) ) )
         {
            GX_msglist.addItem("O valor de Contratante_Email n�o coincide com o padr�o especificado", "OutOfRange", 1, "vCONTRATANTE_EMAIL");
            AnyError = 1;
            GX_FocusControl = edtavContratante_email_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21Contratante_Telefone)) )
         {
            GX_msglist.addItem("Telefone � obrigat�rio.", 1, "vCONTRATANTE_TELEFONE");
            AnyError = 1;
            GX_FocusControl = edtavContratante_telefone_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GXVvMUNICIPIO_CODIGO_html04106( AV24Estado_UF) ;
         /* Using cursor T00046 */
         pr_default.execute(4, new Object[] {n1216AreaTrabalho_OrganizacaoCod, A1216AreaTrabalho_OrganizacaoCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A1216AreaTrabalho_OrganizacaoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Area Trabalho_Organizacao'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1214Organizacao_Nome = T00046_A1214Organizacao_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
         n1214Organizacao_Nome = T00046_n1214Organizacao_Nome[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors04106( )
      {
         pr_default.close(8);
         pr_default.close(2);
         pr_default.close(5);
         pr_default.close(6);
         pr_default.close(7);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_79( int A5AreaTrabalho_Codigo )
      {
         /* Using cursor T000415 */
         pr_default.execute(10, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            A272AreaTrabalho_ContagensQtdGeral = T000415_A272AreaTrabalho_ContagensQtdGeral[0];
            n272AreaTrabalho_ContagensQtdGeral = T000415_n272AreaTrabalho_ContagensQtdGeral[0];
         }
         else
         {
            A272AreaTrabalho_ContagensQtdGeral = 0;
            n272AreaTrabalho_ContagensQtdGeral = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A272AreaTrabalho_ContagensQtdGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_73( int A29Contratante_Codigo )
      {
         /* Using cursor T000416 */
         pr_default.execute(11, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         if ( (pr_default.getStatus(11) == 101) )
         {
            if ( ! ( (0==A29Contratante_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Contratante'.", "ForeignKeyNotFound", 1, "CONTRATANTE_CODIGO");
               AnyError = 1;
               GX_FocusControl = edtContratante_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A33Contratante_Fax = T000416_A33Contratante_Fax[0];
         n33Contratante_Fax = T000416_n33Contratante_Fax[0];
         A32Contratante_Ramal = T000416_A32Contratante_Ramal[0];
         n32Contratante_Ramal = T000416_n32Contratante_Ramal[0];
         A31Contratante_Telefone = T000416_A31Contratante_Telefone[0];
         A14Contratante_Email = T000416_A14Contratante_Email[0];
         n14Contratante_Email = T000416_n14Contratante_Email[0];
         A13Contratante_WebSite = T000416_A13Contratante_WebSite[0];
         n13Contratante_WebSite = T000416_n13Contratante_WebSite[0];
         A11Contratante_IE = T000416_A11Contratante_IE[0];
         A10Contratante_NomeFantasia = T000416_A10Contratante_NomeFantasia[0];
         A547Contratante_EmailSdaHost = T000416_A547Contratante_EmailSdaHost[0];
         n547Contratante_EmailSdaHost = T000416_n547Contratante_EmailSdaHost[0];
         A548Contratante_EmailSdaUser = T000416_A548Contratante_EmailSdaUser[0];
         n548Contratante_EmailSdaUser = T000416_n548Contratante_EmailSdaUser[0];
         A549Contratante_EmailSdaPass = T000416_A549Contratante_EmailSdaPass[0];
         n549Contratante_EmailSdaPass = T000416_n549Contratante_EmailSdaPass[0];
         A550Contratante_EmailSdaKey = T000416_A550Contratante_EmailSdaKey[0];
         n550Contratante_EmailSdaKey = T000416_n550Contratante_EmailSdaKey[0];
         A551Contratante_EmailSdaAut = T000416_A551Contratante_EmailSdaAut[0];
         n551Contratante_EmailSdaAut = T000416_n551Contratante_EmailSdaAut[0];
         A552Contratante_EmailSdaPort = T000416_A552Contratante_EmailSdaPort[0];
         n552Contratante_EmailSdaPort = T000416_n552Contratante_EmailSdaPort[0];
         A1048Contratante_EmailSdaSec = T000416_A1048Contratante_EmailSdaSec[0];
         n1048Contratante_EmailSdaSec = T000416_n1048Contratante_EmailSdaSec[0];
         A25Municipio_Codigo = T000416_A25Municipio_Codigo[0];
         n25Municipio_Codigo = T000416_n25Municipio_Codigo[0];
         A335Contratante_PessoaCod = T000416_A335Contratante_PessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A33Contratante_Fax))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A32Contratante_Ramal))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A31Contratante_Telefone))+"\""+","+"\""+GXUtil.EncodeJSConstant( A14Contratante_Email)+"\""+","+"\""+GXUtil.EncodeJSConstant( A13Contratante_WebSite)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A11Contratante_IE))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A10Contratante_NomeFantasia))+"\""+","+"\""+GXUtil.EncodeJSConstant( A547Contratante_EmailSdaHost)+"\""+","+"\""+GXUtil.EncodeJSConstant( A548Contratante_EmailSdaUser)+"\""+","+"\""+GXUtil.EncodeJSConstant( A549Contratante_EmailSdaPass)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A550Contratante_EmailSdaKey))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A551Contratante_EmailSdaAut))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A552Contratante_EmailSdaPort), 4, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1048Contratante_EmailSdaSec), 4, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_76( int A25Municipio_Codigo )
      {
         /* Using cursor T000417 */
         pr_default.execute(12, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            if ( ! ( (0==A25Municipio_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Municipio'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A26Municipio_Nome = T000417_A26Municipio_Nome[0];
         A23Estado_UF = T000417_A23Estado_UF[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A26Municipio_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A23Estado_UF))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void gxLoad_77( String A23Estado_UF )
      {
         /* Using cursor T000418 */
         pr_default.execute(13, new Object[] {A23Estado_UF});
         if ( (pr_default.getStatus(13) == 101) )
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A23Estado_UF)) ) )
            {
               GX_msglist.addItem("N�o existe 'Estado'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A24Estado_Nome = T000418_A24Estado_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A24Estado_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(13) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(13);
      }

      protected void gxLoad_78( int A335Contratante_PessoaCod )
      {
         /* Using cursor T000419 */
         pr_default.execute(14, new Object[] {A335Contratante_PessoaCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            if ( ! ( (0==A335Contratante_PessoaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratante_Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A12Contratante_CNPJ = T000419_A12Contratante_CNPJ[0];
         n12Contratante_CNPJ = T000419_n12Contratante_CNPJ[0];
         A9Contratante_RazaoSocial = T000419_A9Contratante_RazaoSocial[0];
         n9Contratante_RazaoSocial = T000419_n9Contratante_RazaoSocial[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A12Contratante_CNPJ)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A9Contratante_RazaoSocial))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(14);
      }

      protected void gxLoad_74( int A830AreaTrabalho_ServicoPadrao )
      {
         /* Using cursor T000420 */
         pr_default.execute(15, new Object[] {n830AreaTrabalho_ServicoPadrao, A830AreaTrabalho_ServicoPadrao});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (0==A830AreaTrabalho_ServicoPadrao) ) )
            {
               GX_msglist.addItem("N�o existe 'Area Trabalho_Servicos'.", "ForeignKeyNotFound", 1, "AREATRABALHO_SERVICOPADRAO");
               AnyError = 1;
               GX_FocusControl = dynAreaTrabalho_ServicoPadrao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      protected void gxLoad_75( int A1216AreaTrabalho_OrganizacaoCod )
      {
         /* Using cursor T000421 */
         pr_default.execute(16, new Object[] {n1216AreaTrabalho_OrganizacaoCod, A1216AreaTrabalho_OrganizacaoCod});
         if ( (pr_default.getStatus(16) == 101) )
         {
            if ( ! ( (0==A1216AreaTrabalho_OrganizacaoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Area Trabalho_Organizacao'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1214Organizacao_Nome = T000421_A1214Organizacao_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
         n1214Organizacao_Nome = T000421_n1214Organizacao_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1214Organizacao_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(16) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(16);
      }

      protected void GetKey04106( )
      {
         /* Using cursor T000422 */
         pr_default.execute(17, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound106 = 1;
         }
         else
         {
            RcdFound106 = 0;
         }
         pr_default.close(17);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00043 */
         pr_default.execute(1, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM04106( 72) ;
            RcdFound106 = 1;
            A5AreaTrabalho_Codigo = T00043_A5AreaTrabalho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            A6AreaTrabalho_Descricao = T00043_A6AreaTrabalho_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
            A642AreaTrabalho_CalculoPFinal = T00043_A642AreaTrabalho_CalculoPFinal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
            A834AreaTrabalho_ValidaOSFM = T00043_A834AreaTrabalho_ValidaOSFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A834AreaTrabalho_ValidaOSFM", A834AreaTrabalho_ValidaOSFM);
            n834AreaTrabalho_ValidaOSFM = T00043_n834AreaTrabalho_ValidaOSFM[0];
            A855AreaTrabalho_DiasParaPagar = T00043_A855AreaTrabalho_DiasParaPagar[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A855AreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0)));
            n855AreaTrabalho_DiasParaPagar = T00043_n855AreaTrabalho_DiasParaPagar[0];
            A987AreaTrabalho_ContratadaUpdBslCod = T00043_A987AreaTrabalho_ContratadaUpdBslCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A987AreaTrabalho_ContratadaUpdBslCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0)));
            n987AreaTrabalho_ContratadaUpdBslCod = T00043_n987AreaTrabalho_ContratadaUpdBslCod[0];
            A1154AreaTrabalho_TipoPlanilha = T00043_A1154AreaTrabalho_TipoPlanilha[0];
            n1154AreaTrabalho_TipoPlanilha = T00043_n1154AreaTrabalho_TipoPlanilha[0];
            A72AreaTrabalho_Ativo = T00043_A72AreaTrabalho_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72AreaTrabalho_Ativo", A72AreaTrabalho_Ativo);
            A1588AreaTrabalho_SS_Codigo = T00043_A1588AreaTrabalho_SS_Codigo[0];
            n1588AreaTrabalho_SS_Codigo = T00043_n1588AreaTrabalho_SS_Codigo[0];
            A2081AreaTrabalho_VerTA = T00043_A2081AreaTrabalho_VerTA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2081AreaTrabalho_VerTA", StringUtil.LTrim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)));
            n2081AreaTrabalho_VerTA = T00043_n2081AreaTrabalho_VerTA[0];
            A2080AreaTrabalho_SelUsrPrestadora = T00043_A2080AreaTrabalho_SelUsrPrestadora[0];
            n2080AreaTrabalho_SelUsrPrestadora = T00043_n2080AreaTrabalho_SelUsrPrestadora[0];
            A29Contratante_Codigo = T00043_A29Contratante_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            n29Contratante_Codigo = T00043_n29Contratante_Codigo[0];
            A830AreaTrabalho_ServicoPadrao = T00043_A830AreaTrabalho_ServicoPadrao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A830AreaTrabalho_ServicoPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0)));
            n830AreaTrabalho_ServicoPadrao = T00043_n830AreaTrabalho_ServicoPadrao[0];
            A1216AreaTrabalho_OrganizacaoCod = T00043_A1216AreaTrabalho_OrganizacaoCod[0];
            n1216AreaTrabalho_OrganizacaoCod = T00043_n1216AreaTrabalho_OrganizacaoCod[0];
            O72AreaTrabalho_Ativo = A72AreaTrabalho_Ativo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72AreaTrabalho_Ativo", A72AreaTrabalho_Ativo);
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            sMode106 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load04106( ) ;
            if ( AnyError == 1 )
            {
               RcdFound106 = 0;
               InitializeNonKey04106( ) ;
            }
            Gx_mode = sMode106;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound106 = 0;
            InitializeNonKey04106( ) ;
            sMode106 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode106;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey04106( ) ;
         if ( RcdFound106 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound106 = 0;
         /* Using cursor T000423 */
         pr_default.execute(18, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(18) != 101) )
         {
            while ( (pr_default.getStatus(18) != 101) && ( ( T000423_A5AreaTrabalho_Codigo[0] < A5AreaTrabalho_Codigo ) ) )
            {
               pr_default.readNext(18);
            }
            if ( (pr_default.getStatus(18) != 101) && ( ( T000423_A5AreaTrabalho_Codigo[0] > A5AreaTrabalho_Codigo ) ) )
            {
               A5AreaTrabalho_Codigo = T000423_A5AreaTrabalho_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               RcdFound106 = 1;
            }
         }
         pr_default.close(18);
      }

      protected void move_previous( )
      {
         RcdFound106 = 0;
         /* Using cursor T000424 */
         pr_default.execute(19, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(19) != 101) )
         {
            while ( (pr_default.getStatus(19) != 101) && ( ( T000424_A5AreaTrabalho_Codigo[0] > A5AreaTrabalho_Codigo ) ) )
            {
               pr_default.readNext(19);
            }
            if ( (pr_default.getStatus(19) != 101) && ( ( T000424_A5AreaTrabalho_Codigo[0] < A5AreaTrabalho_Codigo ) ) )
            {
               A5AreaTrabalho_Codigo = T000424_A5AreaTrabalho_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               RcdFound106 = 1;
            }
         }
         pr_default.close(19);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey04106( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtAreaTrabalho_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert04106( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound106 == 1 )
            {
               if ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo )
               {
                  A5AreaTrabalho_Codigo = Z5AreaTrabalho_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "AREATRABALHO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtAreaTrabalho_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update04106( ) ;
                  GX_FocusControl = edtAreaTrabalho_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtAreaTrabalho_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert04106( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "AREATRABALHO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtAreaTrabalho_Descricao_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert04106( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo )
         {
            A5AreaTrabalho_Codigo = Z5AreaTrabalho_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAreaTrabalho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtAreaTrabalho_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency04106( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00042 */
            pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AreaTrabalho"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z6AreaTrabalho_Descricao, T00042_A6AreaTrabalho_Descricao[0]) != 0 ) || ( StringUtil.StrCmp(Z642AreaTrabalho_CalculoPFinal, T00042_A642AreaTrabalho_CalculoPFinal[0]) != 0 ) || ( Z834AreaTrabalho_ValidaOSFM != T00042_A834AreaTrabalho_ValidaOSFM[0] ) || ( Z855AreaTrabalho_DiasParaPagar != T00042_A855AreaTrabalho_DiasParaPagar[0] ) || ( Z987AreaTrabalho_ContratadaUpdBslCod != T00042_A987AreaTrabalho_ContratadaUpdBslCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1154AreaTrabalho_TipoPlanilha != T00042_A1154AreaTrabalho_TipoPlanilha[0] ) || ( Z72AreaTrabalho_Ativo != T00042_A72AreaTrabalho_Ativo[0] ) || ( Z1588AreaTrabalho_SS_Codigo != T00042_A1588AreaTrabalho_SS_Codigo[0] ) || ( Z2081AreaTrabalho_VerTA != T00042_A2081AreaTrabalho_VerTA[0] ) || ( Z2080AreaTrabalho_SelUsrPrestadora != T00042_A2080AreaTrabalho_SelUsrPrestadora[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z29Contratante_Codigo != T00042_A29Contratante_Codigo[0] ) || ( Z830AreaTrabalho_ServicoPadrao != T00042_A830AreaTrabalho_ServicoPadrao[0] ) || ( Z1216AreaTrabalho_OrganizacaoCod != T00042_A1216AreaTrabalho_OrganizacaoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z6AreaTrabalho_Descricao, T00042_A6AreaTrabalho_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"AreaTrabalho_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z6AreaTrabalho_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T00042_A6AreaTrabalho_Descricao[0]);
               }
               if ( StringUtil.StrCmp(Z642AreaTrabalho_CalculoPFinal, T00042_A642AreaTrabalho_CalculoPFinal[0]) != 0 )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"AreaTrabalho_CalculoPFinal");
                  GXUtil.WriteLogRaw("Old: ",Z642AreaTrabalho_CalculoPFinal);
                  GXUtil.WriteLogRaw("Current: ",T00042_A642AreaTrabalho_CalculoPFinal[0]);
               }
               if ( Z834AreaTrabalho_ValidaOSFM != T00042_A834AreaTrabalho_ValidaOSFM[0] )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"AreaTrabalho_ValidaOSFM");
                  GXUtil.WriteLogRaw("Old: ",Z834AreaTrabalho_ValidaOSFM);
                  GXUtil.WriteLogRaw("Current: ",T00042_A834AreaTrabalho_ValidaOSFM[0]);
               }
               if ( Z855AreaTrabalho_DiasParaPagar != T00042_A855AreaTrabalho_DiasParaPagar[0] )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"AreaTrabalho_DiasParaPagar");
                  GXUtil.WriteLogRaw("Old: ",Z855AreaTrabalho_DiasParaPagar);
                  GXUtil.WriteLogRaw("Current: ",T00042_A855AreaTrabalho_DiasParaPagar[0]);
               }
               if ( Z987AreaTrabalho_ContratadaUpdBslCod != T00042_A987AreaTrabalho_ContratadaUpdBslCod[0] )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"AreaTrabalho_ContratadaUpdBslCod");
                  GXUtil.WriteLogRaw("Old: ",Z987AreaTrabalho_ContratadaUpdBslCod);
                  GXUtil.WriteLogRaw("Current: ",T00042_A987AreaTrabalho_ContratadaUpdBslCod[0]);
               }
               if ( Z1154AreaTrabalho_TipoPlanilha != T00042_A1154AreaTrabalho_TipoPlanilha[0] )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"AreaTrabalho_TipoPlanilha");
                  GXUtil.WriteLogRaw("Old: ",Z1154AreaTrabalho_TipoPlanilha);
                  GXUtil.WriteLogRaw("Current: ",T00042_A1154AreaTrabalho_TipoPlanilha[0]);
               }
               if ( Z72AreaTrabalho_Ativo != T00042_A72AreaTrabalho_Ativo[0] )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"AreaTrabalho_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z72AreaTrabalho_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00042_A72AreaTrabalho_Ativo[0]);
               }
               if ( Z1588AreaTrabalho_SS_Codigo != T00042_A1588AreaTrabalho_SS_Codigo[0] )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"AreaTrabalho_SS_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z1588AreaTrabalho_SS_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00042_A1588AreaTrabalho_SS_Codigo[0]);
               }
               if ( Z2081AreaTrabalho_VerTA != T00042_A2081AreaTrabalho_VerTA[0] )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"AreaTrabalho_VerTA");
                  GXUtil.WriteLogRaw("Old: ",Z2081AreaTrabalho_VerTA);
                  GXUtil.WriteLogRaw("Current: ",T00042_A2081AreaTrabalho_VerTA[0]);
               }
               if ( Z2080AreaTrabalho_SelUsrPrestadora != T00042_A2080AreaTrabalho_SelUsrPrestadora[0] )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"AreaTrabalho_SelUsrPrestadora");
                  GXUtil.WriteLogRaw("Old: ",Z2080AreaTrabalho_SelUsrPrestadora);
                  GXUtil.WriteLogRaw("Current: ",T00042_A2080AreaTrabalho_SelUsrPrestadora[0]);
               }
               if ( Z29Contratante_Codigo != T00042_A29Contratante_Codigo[0] )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"Contratante_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z29Contratante_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00042_A29Contratante_Codigo[0]);
               }
               if ( Z830AreaTrabalho_ServicoPadrao != T00042_A830AreaTrabalho_ServicoPadrao[0] )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"AreaTrabalho_ServicoPadrao");
                  GXUtil.WriteLogRaw("Old: ",Z830AreaTrabalho_ServicoPadrao);
                  GXUtil.WriteLogRaw("Current: ",T00042_A830AreaTrabalho_ServicoPadrao[0]);
               }
               if ( Z1216AreaTrabalho_OrganizacaoCod != T00042_A1216AreaTrabalho_OrganizacaoCod[0] )
               {
                  GXUtil.WriteLog("areatrabalho:[seudo value changed for attri]"+"AreaTrabalho_OrganizacaoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1216AreaTrabalho_OrganizacaoCod);
                  GXUtil.WriteLogRaw("Current: ",T00042_A1216AreaTrabalho_OrganizacaoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"AreaTrabalho"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert04106( )
      {
         BeforeValidate04106( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable04106( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM04106( 0) ;
            CheckOptimisticConcurrency04106( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm04106( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert04106( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000425 */
                     pr_default.execute(20, new Object[] {A6AreaTrabalho_Descricao, A642AreaTrabalho_CalculoPFinal, n834AreaTrabalho_ValidaOSFM, A834AreaTrabalho_ValidaOSFM, n855AreaTrabalho_DiasParaPagar, A855AreaTrabalho_DiasParaPagar, n987AreaTrabalho_ContratadaUpdBslCod, A987AreaTrabalho_ContratadaUpdBslCod, n1154AreaTrabalho_TipoPlanilha, A1154AreaTrabalho_TipoPlanilha, A72AreaTrabalho_Ativo, n1588AreaTrabalho_SS_Codigo, A1588AreaTrabalho_SS_Codigo, n2081AreaTrabalho_VerTA, A2081AreaTrabalho_VerTA, n2080AreaTrabalho_SelUsrPrestadora, A2080AreaTrabalho_SelUsrPrestadora, n29Contratante_Codigo, A29Contratante_Codigo, n830AreaTrabalho_ServicoPadrao, A830AreaTrabalho_ServicoPadrao, n1216AreaTrabalho_OrganizacaoCod, A1216AreaTrabalho_OrganizacaoCod});
                     A5AreaTrabalho_Codigo = T000425_A5AreaTrabalho_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                     pr_default.close(20);
                     dsDefault.SmartCacheProvider.SetUpdated("AreaTrabalho") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        AV8WWPContext.gxTpr_Updcomboareatrabalho = true;
                        new wwpbaseobjects.setwwpcontext(context ).execute(  AV8WWPContext) ;
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption040( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load04106( ) ;
            }
            EndLevel04106( ) ;
         }
         CloseExtendedTableCursors04106( ) ;
      }

      protected void Update04106( )
      {
         BeforeValidate04106( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable04106( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency04106( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm04106( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate04106( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000426 */
                     pr_default.execute(21, new Object[] {A6AreaTrabalho_Descricao, A642AreaTrabalho_CalculoPFinal, n834AreaTrabalho_ValidaOSFM, A834AreaTrabalho_ValidaOSFM, n855AreaTrabalho_DiasParaPagar, A855AreaTrabalho_DiasParaPagar, n987AreaTrabalho_ContratadaUpdBslCod, A987AreaTrabalho_ContratadaUpdBslCod, n1154AreaTrabalho_TipoPlanilha, A1154AreaTrabalho_TipoPlanilha, A72AreaTrabalho_Ativo, n1588AreaTrabalho_SS_Codigo, A1588AreaTrabalho_SS_Codigo, n2081AreaTrabalho_VerTA, A2081AreaTrabalho_VerTA, n2080AreaTrabalho_SelUsrPrestadora, A2080AreaTrabalho_SelUsrPrestadora, n29Contratante_Codigo, A29Contratante_Codigo, n830AreaTrabalho_ServicoPadrao, A830AreaTrabalho_ServicoPadrao, n1216AreaTrabalho_OrganizacaoCod, A1216AreaTrabalho_OrganizacaoCod, A5AreaTrabalho_Codigo});
                     pr_default.close(21);
                     dsDefault.SmartCacheProvider.SetUpdated("AreaTrabalho") ;
                     if ( (pr_default.getStatus(21) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AreaTrabalho"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate04106( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        AV8WWPContext.gxTpr_Updcomboareatrabalho = true;
                        new wwpbaseobjects.setwwpcontext(context ).execute(  AV8WWPContext) ;
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel04106( ) ;
         }
         CloseExtendedTableCursors04106( ) ;
      }

      protected void DeferredUpdate04106( )
      {
      }

      protected void delete( )
      {
         BeforeValidate04106( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency04106( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls04106( ) ;
            AfterConfirm04106( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete04106( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000427 */
                  pr_default.execute(22, new Object[] {A5AreaTrabalho_Codigo});
                  pr_default.close(22);
                  dsDefault.SmartCacheProvider.SetUpdated("AreaTrabalho") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     AV8WWPContext.gxTpr_Updcomboareatrabalho = true;
                     new wwpbaseobjects.setwwpcontext(context ).execute(  AV8WWPContext) ;
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode106 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel04106( ) ;
         Gx_mode = sMode106;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls04106( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000429 */
            pr_default.execute(23, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               A272AreaTrabalho_ContagensQtdGeral = T000429_A272AreaTrabalho_ContagensQtdGeral[0];
               n272AreaTrabalho_ContagensQtdGeral = T000429_n272AreaTrabalho_ContagensQtdGeral[0];
            }
            else
            {
               A272AreaTrabalho_ContagensQtdGeral = 0;
               n272AreaTrabalho_ContagensQtdGeral = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A272AreaTrabalho_ContagensQtdGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0)));
            }
            pr_default.close(23);
            /* Using cursor T000430 */
            pr_default.execute(24, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            A33Contratante_Fax = T000430_A33Contratante_Fax[0];
            n33Contratante_Fax = T000430_n33Contratante_Fax[0];
            A32Contratante_Ramal = T000430_A32Contratante_Ramal[0];
            n32Contratante_Ramal = T000430_n32Contratante_Ramal[0];
            A31Contratante_Telefone = T000430_A31Contratante_Telefone[0];
            A14Contratante_Email = T000430_A14Contratante_Email[0];
            n14Contratante_Email = T000430_n14Contratante_Email[0];
            A13Contratante_WebSite = T000430_A13Contratante_WebSite[0];
            n13Contratante_WebSite = T000430_n13Contratante_WebSite[0];
            A11Contratante_IE = T000430_A11Contratante_IE[0];
            A10Contratante_NomeFantasia = T000430_A10Contratante_NomeFantasia[0];
            A547Contratante_EmailSdaHost = T000430_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = T000430_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = T000430_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = T000430_n548Contratante_EmailSdaUser[0];
            A549Contratante_EmailSdaPass = T000430_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = T000430_n549Contratante_EmailSdaPass[0];
            A550Contratante_EmailSdaKey = T000430_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = T000430_n550Contratante_EmailSdaKey[0];
            A551Contratante_EmailSdaAut = T000430_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = T000430_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = T000430_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = T000430_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = T000430_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = T000430_n1048Contratante_EmailSdaSec[0];
            A25Municipio_Codigo = T000430_A25Municipio_Codigo[0];
            n25Municipio_Codigo = T000430_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = T000430_A335Contratante_PessoaCod[0];
            pr_default.close(24);
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV23Contratante_Fax = A33Contratante_Fax;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratante_Fax", AV23Contratante_Fax);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV22Contratante_Ramal = A32Contratante_Ramal;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratante_Ramal", AV22Contratante_Ramal);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV21Contratante_Telefone = A31Contratante_Telefone;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratante_Telefone", AV21Contratante_Telefone);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV20Contratante_Email = A14Contratante_Email;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratante_Email", AV20Contratante_Email);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV19Contratante_WebSite = A13Contratante_WebSite;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratante_WebSite", AV19Contratante_WebSite);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV18Contratante_IE = A11Contratante_IE;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratante_IE", AV18Contratante_IE);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV17Contratante_NomeFantasia = A10Contratante_NomeFantasia;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratante_NomeFantasia", AV17Contratante_NomeFantasia);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV32Contratante_EmailSdaHost = A547Contratante_EmailSdaHost;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contratante_EmailSdaHost", AV32Contratante_EmailSdaHost);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV33Contratante_EmailSdaUser = A548Contratante_EmailSdaUser;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratante_EmailSdaUser", AV33Contratante_EmailSdaUser);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ! String.IsNullOrEmpty(StringUtil.RTrim( A550Contratante_EmailSdaKey)) )
            {
               AV35Contratante_EmailSdaPass = Crypto.Decrypt64( A549Contratante_EmailSdaPass, A550Contratante_EmailSdaKey);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratante_EmailSdaPass", AV35Contratante_EmailSdaPass);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV37Contratante_EmailSdaAut = A551Contratante_EmailSdaAut;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contratante_EmailSdaAut", AV37Contratante_EmailSdaAut);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV36Contratante_EmailSdaPort = A552Contratante_EmailSdaPort;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contratante_EmailSdaPort), 4, 0)));
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV34Contratante_EmailSdaSec = A1048Contratante_EmailSdaSec;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0)));
            }
            /* Using cursor T000431 */
            pr_default.execute(25, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
            A26Municipio_Nome = T000431_A26Municipio_Nome[0];
            A23Estado_UF = T000431_A23Estado_UF[0];
            pr_default.close(25);
            /* Using cursor T000432 */
            pr_default.execute(26, new Object[] {A23Estado_UF});
            A24Estado_Nome = T000432_A24Estado_Nome[0];
            pr_default.close(26);
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV24Estado_UF = A23Estado_UF;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Estado_UF", AV24Estado_UF);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV25Municipio_Codigo = A25Municipio_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0)));
            }
            /* Using cursor T000433 */
            pr_default.execute(27, new Object[] {A335Contratante_PessoaCod});
            A12Contratante_CNPJ = T000433_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = T000433_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = T000433_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = T000433_n9Contratante_RazaoSocial[0];
            pr_default.close(27);
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV15Contratante_CNPJ = A12Contratante_CNPJ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratante_CNPJ", AV15Contratante_CNPJ);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV16Contratante_RazaoSocial = A9Contratante_RazaoSocial;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contratante_RazaoSocial", AV16Contratante_RazaoSocial);
            }
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV26Contratante_Codigo = A29Contratante_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Contratante_Codigo), 6, 0)));
            }
            edtavContratante_cnpj_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_cnpj_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj_Enabled), 5, 0)));
            edtavContratante_email_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_email_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_email_Enabled), 5, 0)));
            edtavContratante_fax_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_fax_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_fax_Enabled), 5, 0)));
            edtavContratante_ie_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_ie_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_ie_Enabled), 5, 0)));
            edtavContratante_nomefantasia_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_nomefantasia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_nomefantasia_Enabled), 5, 0)));
            edtavContratante_ramal_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_ramal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_ramal_Enabled), 5, 0)));
            edtavContratante_razaosocial_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_razaosocial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial_Enabled), 5, 0)));
            edtavContratante_telefone_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_telefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_telefone_Enabled), 5, 0)));
            edtavContratante_website_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_website_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_website_Enabled), 5, 0)));
            dynavMunicipio_codigo.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavMunicipio_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavMunicipio_codigo.Enabled), 5, 0)));
            dynavEstado_uf.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf.Enabled), 5, 0)));
            edtavContratante_emailsdahost_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdahost_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdahost_Enabled), 5, 0)));
            edtavContratante_emailsdauser_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdauser_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdauser_Enabled), 5, 0)));
            edtavContratante_emailsdapass_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdapass_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdapass_Enabled), 5, 0)));
            edtavContratante_emailsdaport_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdaport_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdaport_Enabled), 5, 0)));
            cmbavContratante_emailsdaaut.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratante_emailsdaaut_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratante_emailsdaaut.Enabled), 5, 0)));
            cmbavContratante_emailsdasec.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratante_emailsdasec_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratante_emailsdasec.Enabled), 5, 0)));
            GXVvMUNICIPIO_CODIGO_html04106( AV24Estado_UF) ;
            /* Using cursor T000434 */
            pr_default.execute(28, new Object[] {n1216AreaTrabalho_OrganizacaoCod, A1216AreaTrabalho_OrganizacaoCod});
            A1214Organizacao_Nome = T000434_A1214Organizacao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
            n1214Organizacao_Nome = T000434_n1214Organizacao_Nome[0];
            pr_default.close(28);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000435 */
            pr_default.execute(29, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(29) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Grupo de Func�es"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(29);
            /* Using cursor T000436 */
            pr_default.execute(30, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(30) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T236"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(30);
            /* Using cursor T000437 */
            pr_default.execute(31, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(31);
            /* Using cursor T000438 */
            pr_default.execute(32, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario Notifica"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(32);
            /* Using cursor T000439 */
            pr_default.execute(33, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Indicador"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
            /* Using cursor T000440 */
            pr_default.execute(34, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Notifica��es da Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
            /* Using cursor T000441 */
            pr_default.execute(35, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Cat�logo de Servi�os"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
            /* Using cursor T000442 */
            pr_default.execute(36, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(36) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gest�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(36);
            /* Using cursor T000443 */
            pr_default.execute(37, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(37) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gloss�rio"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(37);
            /* Using cursor T000444 */
            pr_default.execute(38, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(38) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Vari�vel Cocomo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(38);
            /* Using cursor T000445 */
            pr_default.execute(39, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(39) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Regras de Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(39);
            /* Using cursor T000446 */
            pr_default.execute(40, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(40) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Par�metros das Planilhas"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(40);
            /* Using cursor T000447 */
            pr_default.execute(41, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(41) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T62"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(41);
            /* Using cursor T000448 */
            pr_default.execute(42, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(42) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Item N�o Mensuravel"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(42);
            /* Using cursor T000449 */
            pr_default.execute(43, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(43) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Referencia INM"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(43);
            /* Using cursor T000450 */
            pr_default.execute(44, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(44) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Lote"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(44);
            /* Using cursor T000451 */
            pr_default.execute(45, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(45) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"N�o Conformidade"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(45);
            /* Using cursor T000452 */
            pr_default.execute(46, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(46) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Status"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(46);
            /* Using cursor T000453 */
            pr_default.execute(47, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(47) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Guia"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(47);
            /* Using cursor T000454 */
            pr_default.execute(48, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(48) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(48);
            /* Using cursor T000455 */
            pr_default.execute(49, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(49) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistema"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(49);
            /* Using cursor T000456 */
            pr_default.execute(50, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(50) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(50);
            /* Using cursor T000457 */
            pr_default.execute(51, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(51) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratada"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(51);
            /* Using cursor T000458 */
            pr_default.execute(52, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(52) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Perfil"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(52);
            /* Using cursor T000459 */
            pr_default.execute(53, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(53) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Proposta"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(53);
            /* Using cursor T000460 */
            pr_default.execute(54, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(54) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Configura��o de Fatores de Impacto na Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(54);
            /* Using cursor T000461 */
            pr_default.execute(55, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(55) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Area Trabalho_Guias"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(55);
         }
      }

      protected void EndLevel04106( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete04106( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(24);
            pr_default.close(28);
            pr_default.close(25);
            pr_default.close(26);
            pr_default.close(27);
            pr_default.close(23);
            context.CommitDataStores( "AreaTrabalho");
            if ( AnyError == 0 )
            {
               ConfirmValues040( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(24);
            pr_default.close(28);
            pr_default.close(25);
            pr_default.close(26);
            pr_default.close(27);
            pr_default.close(23);
            context.RollbackDataStores( "AreaTrabalho");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart04106( )
      {
         /* Scan By routine */
         /* Using cursor T000462 */
         pr_default.execute(56);
         RcdFound106 = 0;
         if ( (pr_default.getStatus(56) != 101) )
         {
            RcdFound106 = 1;
            A5AreaTrabalho_Codigo = T000462_A5AreaTrabalho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext04106( )
      {
         /* Scan next routine */
         pr_default.readNext(56);
         RcdFound106 = 0;
         if ( (pr_default.getStatus(56) != 101) )
         {
            RcdFound106 = 1;
            A5AreaTrabalho_Codigo = T000462_A5AreaTrabalho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd04106( )
      {
         pr_default.close(56);
      }

      protected void AfterConfirm04106( )
      {
         /* After Confirm Rules */
         if ( (0==AV26Contratante_Codigo) )
         {
            GXt_int1 = A29Contratante_Codigo;
            new prc_novapessoacontratante(context ).execute(  AV15Contratante_CNPJ,  AV16Contratante_RazaoSocial,  AV17Contratante_NomeFantasia,  AV18Contratante_IE,  AV21Contratante_Telefone,  AV22Contratante_Ramal,  AV23Contratante_Fax,  AV20Contratante_Email,  AV19Contratante_WebSite,  AV25Municipio_Codigo,  AV32Contratante_EmailSdaHost,  AV33Contratante_EmailSdaUser,  AV35Contratante_EmailSdaPass,  AV36Contratante_EmailSdaPort,  AV37Contratante_EmailSdaAut,  AV34Contratante_EmailSdaSec, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratante_CNPJ", AV15Contratante_CNPJ);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contratante_RazaoSocial", AV16Contratante_RazaoSocial);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratante_NomeFantasia", AV17Contratante_NomeFantasia);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratante_IE", AV18Contratante_IE);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratante_Telefone", AV21Contratante_Telefone);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratante_Ramal", AV22Contratante_Ramal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratante_Fax", AV23Contratante_Fax);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratante_Email", AV20Contratante_Email);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratante_WebSite", AV19Contratante_WebSite);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contratante_EmailSdaHost", AV32Contratante_EmailSdaHost);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratante_EmailSdaUser", AV33Contratante_EmailSdaUser);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratante_EmailSdaPass", AV35Contratante_EmailSdaPass);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contratante_EmailSdaPort), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contratante_EmailSdaAut", AV37Contratante_EmailSdaAut);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0)));
            A29Contratante_Codigo = GXt_int1;
            n29Contratante_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ! (0==AV26Contratante_Codigo) )
            {
               A29Contratante_Codigo = AV26Contratante_Codigo;
               n29Contratante_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            }
         }
      }

      protected void BeforeInsert04106( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate04106( )
      {
         /* Before Update Rules */
         new loadauditareatrabalho(context ).execute(  "Y", ref  AV30AuditingObject,  A5AreaTrabalho_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeDelete04106( )
      {
         /* Before Delete Rules */
         new loadauditareatrabalho(context ).execute(  "Y", ref  AV30AuditingObject,  A5AreaTrabalho_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeComplete04106( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditareatrabalho(context ).execute(  "N", ref  AV30AuditingObject,  A5AreaTrabalho_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditareatrabalho(context ).execute(  "N", ref  AV30AuditingObject,  A5AreaTrabalho_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void BeforeValidate04106( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes04106( )
      {
         edtOrganizacao_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtOrganizacao_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtOrganizacao_Nome_Enabled), 5, 0)));
         edtAreaTrabalho_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Descricao_Enabled), 5, 0)));
         cmbAreaTrabalho_CalculoPFinal.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_CalculoPFinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbAreaTrabalho_CalculoPFinal.Enabled), 5, 0)));
         dynAreaTrabalho_ServicoPadrao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAreaTrabalho_ServicoPadrao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAreaTrabalho_ServicoPadrao.Enabled), 5, 0)));
         cmbAreaTrabalho_ValidaOSFM.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_ValidaOSFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbAreaTrabalho_ValidaOSFM.Enabled), 5, 0)));
         edtAreaTrabalho_DiasParaPagar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_DiasParaPagar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_DiasParaPagar_Enabled), 5, 0)));
         dynAreaTrabalho_ContratadaUpdBslCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAreaTrabalho_ContratadaUpdBslCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAreaTrabalho_ContratadaUpdBslCod.Enabled), 5, 0)));
         cmbAreaTrabalho_VerTA.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_VerTA_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbAreaTrabalho_VerTA.Enabled), 5, 0)));
         chkAreaTrabalho_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAreaTrabalho_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkAreaTrabalho_Ativo.Enabled), 5, 0)));
         edtavContratante_cnpj_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_cnpj_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj_Enabled), 5, 0)));
         edtavContratante_ie_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_ie_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_ie_Enabled), 5, 0)));
         edtavContratante_razaosocial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_razaosocial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial_Enabled), 5, 0)));
         edtavContratante_nomefantasia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_nomefantasia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_nomefantasia_Enabled), 5, 0)));
         edtavContratante_website_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_website_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_website_Enabled), 5, 0)));
         edtavContratante_email_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_email_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_email_Enabled), 5, 0)));
         edtavContratante_telefone_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_telefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_telefone_Enabled), 5, 0)));
         edtavContratante_ramal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_ramal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_ramal_Enabled), 5, 0)));
         edtavContratante_fax_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_fax_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_fax_Enabled), 5, 0)));
         dynavEstado_uf.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf.Enabled), 5, 0)));
         dynavMunicipio_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavMunicipio_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavMunicipio_codigo.Enabled), 5, 0)));
         edtavContratante_emailsdahost_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdahost_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdahost_Enabled), 5, 0)));
         edtavContratante_emailsdauser_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdauser_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdauser_Enabled), 5, 0)));
         edtavContratante_emailsdapass_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdapass_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdapass_Enabled), 5, 0)));
         edtavContratante_emailsdaport_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_emailsdaport_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_emailsdaport_Enabled), 5, 0)));
         cmbavContratante_emailsdaaut.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratante_emailsdaaut_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratante_emailsdaaut.Enabled), 5, 0)));
         cmbavContratante_emailsdasec.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratante_emailsdasec_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratante_emailsdasec.Enabled), 5, 0)));
         edtContratante_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Codigo_Enabled), 5, 0)));
         edtAreaTrabalho_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAreaTrabalho_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues040( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205302127214");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("areatrabalho.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7AreaTrabalho_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z6AreaTrabalho_Descricao", Z6AreaTrabalho_Descricao);
         GxWebStd.gx_hidden_field( context, "Z642AreaTrabalho_CalculoPFinal", StringUtil.RTrim( Z642AreaTrabalho_CalculoPFinal));
         GxWebStd.gx_boolean_hidden_field( context, "Z834AreaTrabalho_ValidaOSFM", Z834AreaTrabalho_ValidaOSFM);
         GxWebStd.gx_hidden_field( context, "Z855AreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z855AreaTrabalho_DiasParaPagar), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z987AreaTrabalho_ContratadaUpdBslCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z987AreaTrabalho_ContratadaUpdBslCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1154AreaTrabalho_TipoPlanilha", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1154AreaTrabalho_TipoPlanilha), 2, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z72AreaTrabalho_Ativo", Z72AreaTrabalho_Ativo);
         GxWebStd.gx_hidden_field( context, "Z1588AreaTrabalho_SS_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1588AreaTrabalho_SS_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2081AreaTrabalho_VerTA", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2081AreaTrabalho_VerTA), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z2080AreaTrabalho_SelUsrPrestadora", Z2080AreaTrabalho_SelUsrPrestadora);
         GxWebStd.gx_hidden_field( context, "Z29Contratante_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z29Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z830AreaTrabalho_ServicoPadrao", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z830AreaTrabalho_ServicoPadrao), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1216AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1216AreaTrabalho_OrganizacaoCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "O72AreaTrabalho_Ativo", O72AreaTrabalho_Ativo);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1216AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N29Contratante_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N830AreaTrabalho_ServicoPadrao", StringUtil.LTrim( StringUtil.NToC( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_AREATRABALHO_ORGANIZACAOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29Insert_AreaTrabalho_OrganizacaoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_ORGANIZACAOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_AREATRABALHO_SERVICOPADRAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27Insert_AreaTrabalho_ServicoPadrao), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_CNPJ", A12Contratante_CNPJ);
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAIL", A14Contratante_Email);
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_FAX", StringUtil.RTrim( A33Contratante_Fax));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_IE", StringUtil.RTrim( A11Contratante_IE));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_NOMEFANTASIA", StringUtil.RTrim( A10Contratante_NomeFantasia));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_RAMAL", StringUtil.RTrim( A32Contratante_Ramal));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_RAZAOSOCIAL", StringUtil.RTrim( A9Contratante_RazaoSocial));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_TELEFONE", StringUtil.RTrim( A31Contratante_Telefone));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_WEBSITE", A13Contratante_WebSite);
         GxWebStd.gx_hidden_field( context, "MUNICIPIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ESTADO_UF", StringUtil.RTrim( A23Estado_UF));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAHOST", A547Contratante_EmailSdaHost);
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAUSER", A548Contratante_EmailSdaUser);
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAPASS", A549Contratante_EmailSdaPass);
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAKEY", StringUtil.RTrim( A550Contratante_EmailSdaKey));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAPORT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A552Contratante_EmailSdaPort), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATANTE_EMAILSDAAUT", A551Contratante_EmailSdaAut);
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDASEC", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1048Contratante_EmailSdaSec), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV8WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITINGOBJECT", AV30AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITINGOBJECT", AV30AuditingObject);
         }
         GxWebStd.gx_hidden_field( context, "vCAUSA", StringUtil.RTrim( AV39Causa));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_TIPOPLANILHA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1154AreaTrabalho_TipoPlanilha), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_SS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1588AreaTrabalho_SS_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "AREATRABALHO_SELUSRPRESTADORA", A2080AreaTrabalho_SelUsrPrestadora);
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "MUNICIPIO_NOME", StringUtil.RTrim( A26Municipio_Nome));
         GxWebStd.gx_hidden_field( context, "ESTADO_NOME", StringUtil.RTrim( A24Estado_Nome));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CONTAGENSQTDGERAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV40Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_CONTRATANTE_Width", StringUtil.RTrim( Dvpanel_contratante_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_CONTRATANTE_Cls", StringUtil.RTrim( Dvpanel_contratante_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_CONTRATANTE_Title", StringUtil.RTrim( Dvpanel_contratante_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_CONTRATANTE_Collapsible", StringUtil.BoolToStr( Dvpanel_contratante_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_CONTRATANTE_Collapsed", StringUtil.BoolToStr( Dvpanel_contratante_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_CONTRATANTE_Enabled", StringUtil.BoolToStr( Dvpanel_contratante_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_CONTRATANTE_Autowidth", StringUtil.BoolToStr( Dvpanel_contratante_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_CONTRATANTE_Autoheight", StringUtil.BoolToStr( Dvpanel_contratante_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_CONTRATANTE_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_contratante_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_CONTRATANTE_Iconposition", StringUtil.RTrim( Dvpanel_contratante_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_CONTRATANTE_Autoscroll", StringUtil.BoolToStr( Dvpanel_contratante_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_AREADETRABALHO_Width", StringUtil.RTrim( Dvpanel_areadetrabalho_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_AREADETRABALHO_Cls", StringUtil.RTrim( Dvpanel_areadetrabalho_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_AREADETRABALHO_Title", StringUtil.RTrim( Dvpanel_areadetrabalho_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_AREADETRABALHO_Collapsible", StringUtil.BoolToStr( Dvpanel_areadetrabalho_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_AREADETRABALHO_Collapsed", StringUtil.BoolToStr( Dvpanel_areadetrabalho_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_AREADETRABALHO_Enabled", StringUtil.BoolToStr( Dvpanel_areadetrabalho_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_AREADETRABALHO_Autowidth", StringUtil.BoolToStr( Dvpanel_areadetrabalho_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_AREADETRABALHO_Autoheight", StringUtil.BoolToStr( Dvpanel_areadetrabalho_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_AREADETRABALHO_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_areadetrabalho_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_AREADETRABALHO_Iconposition", StringUtil.RTrim( Dvpanel_areadetrabalho_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_AREADETRABALHO_Autoscroll", StringUtil.BoolToStr( Dvpanel_areadetrabalho_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "AreaTrabalho";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV40Pgmname, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1154AreaTrabalho_TipoPlanilha), "Z9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1588AreaTrabalho_SS_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A2080AreaTrabalho_SelUsrPrestadora);
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("areatrabalho:[SendSecurityCheck value for]"+"AreaTrabalho_Codigo:"+context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("areatrabalho:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("areatrabalho:[SendSecurityCheck value for]"+"AreaTrabalho_OrganizacaoCod:"+context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9"));
         GXUtil.WriteLog("areatrabalho:[SendSecurityCheck value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV40Pgmname, "")));
         GXUtil.WriteLog("areatrabalho:[SendSecurityCheck value for]"+"AreaTrabalho_TipoPlanilha:"+context.localUtil.Format( (decimal)(A1154AreaTrabalho_TipoPlanilha), "Z9"));
         GXUtil.WriteLog("areatrabalho:[SendSecurityCheck value for]"+"AreaTrabalho_SS_Codigo:"+context.localUtil.Format( (decimal)(A1588AreaTrabalho_SS_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("areatrabalho:[SendSecurityCheck value for]"+"AreaTrabalho_SelUsrPrestadora:"+StringUtil.BoolToStr( A2080AreaTrabalho_SelUsrPrestadora));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("areatrabalho.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7AreaTrabalho_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "AreaTrabalho" ;
      }

      public override String GetPgmdesc( )
      {
         return "�rea de Trabalho" ;
      }

      protected void InitializeNonKey04106( )
      {
         A1216AreaTrabalho_OrganizacaoCod = 0;
         n1216AreaTrabalho_OrganizacaoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1216AreaTrabalho_OrganizacaoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0)));
         A29Contratante_Codigo = 0;
         n29Contratante_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         n29Contratante_Codigo = ((0==A29Contratante_Codigo) ? true : false);
         A830AreaTrabalho_ServicoPadrao = 0;
         n830AreaTrabalho_ServicoPadrao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A830AreaTrabalho_ServicoPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0)));
         n830AreaTrabalho_ServicoPadrao = ((0==A830AreaTrabalho_ServicoPadrao) ? true : false);
         AV15Contratante_CNPJ = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratante_CNPJ", AV15Contratante_CNPJ);
         AV18Contratante_IE = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratante_IE", AV18Contratante_IE);
         AV16Contratante_RazaoSocial = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contratante_RazaoSocial", AV16Contratante_RazaoSocial);
         AV17Contratante_NomeFantasia = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratante_NomeFantasia", AV17Contratante_NomeFantasia);
         AV19Contratante_WebSite = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratante_WebSite", AV19Contratante_WebSite);
         AV20Contratante_Email = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratante_Email", AV20Contratante_Email);
         AV21Contratante_Telefone = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratante_Telefone", AV21Contratante_Telefone);
         AV22Contratante_Ramal = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratante_Ramal", AV22Contratante_Ramal);
         AV23Contratante_Fax = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratante_Fax", AV23Contratante_Fax);
         AV24Estado_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Estado_UF", AV24Estado_UF);
         AV30AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV26Contratante_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Contratante_Codigo), 6, 0)));
         AV25Municipio_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0)));
         A6AreaTrabalho_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
         A1214Organizacao_Nome = "";
         n1214Organizacao_Nome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
         A335Contratante_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A335Contratante_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0)));
         A23Estado_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
         A24Estado_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24Estado_Nome", A24Estado_Nome);
         A25Municipio_Codigo = 0;
         n25Municipio_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
         A26Municipio_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
         A33Contratante_Fax = "";
         n33Contratante_Fax = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33Contratante_Fax", A33Contratante_Fax);
         A32Contratante_Ramal = "";
         n32Contratante_Ramal = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32Contratante_Ramal", A32Contratante_Ramal);
         A31Contratante_Telefone = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31Contratante_Telefone", A31Contratante_Telefone);
         A14Contratante_Email = "";
         n14Contratante_Email = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Contratante_Email", A14Contratante_Email);
         A13Contratante_WebSite = "";
         n13Contratante_WebSite = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Contratante_WebSite", A13Contratante_WebSite);
         A12Contratante_CNPJ = "";
         n12Contratante_CNPJ = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
         A11Contratante_IE = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A11Contratante_IE", A11Contratante_IE);
         A10Contratante_NomeFantasia = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A10Contratante_NomeFantasia", A10Contratante_NomeFantasia);
         A9Contratante_RazaoSocial = "";
         n9Contratante_RazaoSocial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
         A547Contratante_EmailSdaHost = "";
         n547Contratante_EmailSdaHost = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A547Contratante_EmailSdaHost", A547Contratante_EmailSdaHost);
         A548Contratante_EmailSdaUser = "";
         n548Contratante_EmailSdaUser = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A548Contratante_EmailSdaUser", A548Contratante_EmailSdaUser);
         A549Contratante_EmailSdaPass = "";
         n549Contratante_EmailSdaPass = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A549Contratante_EmailSdaPass", A549Contratante_EmailSdaPass);
         A550Contratante_EmailSdaKey = "";
         n550Contratante_EmailSdaKey = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A550Contratante_EmailSdaKey", A550Contratante_EmailSdaKey);
         A551Contratante_EmailSdaAut = false;
         n551Contratante_EmailSdaAut = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A551Contratante_EmailSdaAut", A551Contratante_EmailSdaAut);
         A552Contratante_EmailSdaPort = 0;
         n552Contratante_EmailSdaPort = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A552Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0)));
         A1048Contratante_EmailSdaSec = 0;
         n1048Contratante_EmailSdaSec = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1048Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0)));
         A272AreaTrabalho_ContagensQtdGeral = 0;
         n272AreaTrabalho_ContagensQtdGeral = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A272AreaTrabalho_ContagensQtdGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0)));
         A834AreaTrabalho_ValidaOSFM = false;
         n834AreaTrabalho_ValidaOSFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A834AreaTrabalho_ValidaOSFM", A834AreaTrabalho_ValidaOSFM);
         n834AreaTrabalho_ValidaOSFM = ((false==A834AreaTrabalho_ValidaOSFM) ? true : false);
         A855AreaTrabalho_DiasParaPagar = 0;
         n855AreaTrabalho_DiasParaPagar = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A855AreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0)));
         n855AreaTrabalho_DiasParaPagar = ((0==A855AreaTrabalho_DiasParaPagar) ? true : false);
         A987AreaTrabalho_ContratadaUpdBslCod = 0;
         n987AreaTrabalho_ContratadaUpdBslCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A987AreaTrabalho_ContratadaUpdBslCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A987AreaTrabalho_ContratadaUpdBslCod), 6, 0)));
         n987AreaTrabalho_ContratadaUpdBslCod = ((0==A987AreaTrabalho_ContratadaUpdBslCod) ? true : false);
         A1154AreaTrabalho_TipoPlanilha = 0;
         n1154AreaTrabalho_TipoPlanilha = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1154AreaTrabalho_TipoPlanilha", StringUtil.LTrim( StringUtil.Str( (decimal)(A1154AreaTrabalho_TipoPlanilha), 2, 0)));
         A1588AreaTrabalho_SS_Codigo = 0;
         n1588AreaTrabalho_SS_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1588AreaTrabalho_SS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1588AreaTrabalho_SS_Codigo), 6, 0)));
         A2081AreaTrabalho_VerTA = 0;
         n2081AreaTrabalho_VerTA = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2081AreaTrabalho_VerTA", StringUtil.LTrim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)));
         n2081AreaTrabalho_VerTA = ((0==A2081AreaTrabalho_VerTA) ? true : false);
         A2080AreaTrabalho_SelUsrPrestadora = false;
         n2080AreaTrabalho_SelUsrPrestadora = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2080AreaTrabalho_SelUsrPrestadora", A2080AreaTrabalho_SelUsrPrestadora);
         A642AreaTrabalho_CalculoPFinal = "MB";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
         A72AreaTrabalho_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72AreaTrabalho_Ativo", A72AreaTrabalho_Ativo);
         O72AreaTrabalho_Ativo = A72AreaTrabalho_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72AreaTrabalho_Ativo", A72AreaTrabalho_Ativo);
         Z6AreaTrabalho_Descricao = "";
         Z642AreaTrabalho_CalculoPFinal = "";
         Z834AreaTrabalho_ValidaOSFM = false;
         Z855AreaTrabalho_DiasParaPagar = 0;
         Z987AreaTrabalho_ContratadaUpdBslCod = 0;
         Z1154AreaTrabalho_TipoPlanilha = 0;
         Z72AreaTrabalho_Ativo = false;
         Z1588AreaTrabalho_SS_Codigo = 0;
         Z2081AreaTrabalho_VerTA = 0;
         Z2080AreaTrabalho_SelUsrPrestadora = false;
         Z29Contratante_Codigo = 0;
         Z830AreaTrabalho_ServicoPadrao = 0;
         Z1216AreaTrabalho_OrganizacaoCod = 0;
      }

      protected void InitAll04106( )
      {
         A5AreaTrabalho_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         InitializeNonKey04106( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A72AreaTrabalho_Ativo = i72AreaTrabalho_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72AreaTrabalho_Ativo", A72AreaTrabalho_Ativo);
         A642AreaTrabalho_CalculoPFinal = i642AreaTrabalho_CalculoPFinal;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A642AreaTrabalho_CalculoPFinal", A642AreaTrabalho_CalculoPFinal);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205302127356");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("areatrabalho.js", "?20205302127357");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAreatrabalhotitle_Internalname = "AREATRABALHOTITLE";
         lblTextblockorganizacao_nome_Internalname = "TEXTBLOCKORGANIZACAO_NOME";
         edtOrganizacao_Nome_Internalname = "ORGANIZACAO_NOME";
         lblTextblockareatrabalho_descricao_Internalname = "TEXTBLOCKAREATRABALHO_DESCRICAO";
         edtAreaTrabalho_Descricao_Internalname = "AREATRABALHO_DESCRICAO";
         lblTextblockareatrabalho_calculopfinal_Internalname = "TEXTBLOCKAREATRABALHO_CALCULOPFINAL";
         cmbAreaTrabalho_CalculoPFinal_Internalname = "AREATRABALHO_CALCULOPFINAL";
         lblTextblockareatrabalho_servicopadrao_Internalname = "TEXTBLOCKAREATRABALHO_SERVICOPADRAO";
         dynAreaTrabalho_ServicoPadrao_Internalname = "AREATRABALHO_SERVICOPADRAO";
         lblTextblockareatrabalho_validaosfm_Internalname = "TEXTBLOCKAREATRABALHO_VALIDAOSFM";
         cmbAreaTrabalho_ValidaOSFM_Internalname = "AREATRABALHO_VALIDAOSFM";
         lblTextblockareatrabalho_diasparapagar_Internalname = "TEXTBLOCKAREATRABALHO_DIASPARAPAGAR";
         edtAreaTrabalho_DiasParaPagar_Internalname = "AREATRABALHO_DIASPARAPAGAR";
         lblTextblockareatrabalho_contratadaupdbslcod_Internalname = "TEXTBLOCKAREATRABALHO_CONTRATADAUPDBSLCOD";
         dynAreaTrabalho_ContratadaUpdBslCod_Internalname = "AREATRABALHO_CONTRATADAUPDBSLCOD";
         lblTextblockareatrabalho_verta_Internalname = "TEXTBLOCKAREATRABALHO_VERTA";
         cmbAreaTrabalho_VerTA_Internalname = "AREATRABALHO_VERTA";
         lblTextblockareatrabalho_ativo_Internalname = "TEXTBLOCKAREATRABALHO_ATIVO";
         chkAreaTrabalho_Ativo_Internalname = "AREATRABALHO_ATIVO";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         lblTextblockcontratante_cnpj_Internalname = "TEXTBLOCKCONTRATANTE_CNPJ";
         edtavContratante_cnpj_Internalname = "vCONTRATANTE_CNPJ";
         lblTextblockcontratante_ie_Internalname = "TEXTBLOCKCONTRATANTE_IE";
         edtavContratante_ie_Internalname = "vCONTRATANTE_IE";
         lblTextblockcontratante_razaosocial_Internalname = "TEXTBLOCKCONTRATANTE_RAZAOSOCIAL";
         edtavContratante_razaosocial_Internalname = "vCONTRATANTE_RAZAOSOCIAL";
         lblTextblockcontratante_nomefantasia_Internalname = "TEXTBLOCKCONTRATANTE_NOMEFANTASIA";
         edtavContratante_nomefantasia_Internalname = "vCONTRATANTE_NOMEFANTASIA";
         lblTextblockcontratante_website_Internalname = "TEXTBLOCKCONTRATANTE_WEBSITE";
         edtavContratante_website_Internalname = "vCONTRATANTE_WEBSITE";
         lblTextblockcontratante_email_Internalname = "TEXTBLOCKCONTRATANTE_EMAIL";
         edtavContratante_email_Internalname = "vCONTRATANTE_EMAIL";
         lblTextblockcontratante_telefone_Internalname = "TEXTBLOCKCONTRATANTE_TELEFONE";
         edtavContratante_telefone_Internalname = "vCONTRATANTE_TELEFONE";
         lblTextblockcontratante_ramal_Internalname = "TEXTBLOCKCONTRATANTE_RAMAL";
         edtavContratante_ramal_Internalname = "vCONTRATANTE_RAMAL";
         lblTextblockcontratante_fax_Internalname = "TEXTBLOCKCONTRATANTE_FAX";
         edtavContratante_fax_Internalname = "vCONTRATANTE_FAX";
         lblTextblockestado_uf_Internalname = "TEXTBLOCKESTADO_UF";
         dynavEstado_uf_Internalname = "vESTADO_UF";
         lblTextblockmunicipio_codigo_Internalname = "TEXTBLOCKMUNICIPIO_CODIGO";
         dynavMunicipio_codigo_Internalname = "vMUNICIPIO_CODIGO";
         lblLblemail_Internalname = "LBLEMAIL";
         lblTextblockcontratante_emailsdahost_Internalname = "TEXTBLOCKCONTRATANTE_EMAILSDAHOST";
         edtavContratante_emailsdahost_Internalname = "vCONTRATANTE_EMAILSDAHOST";
         lblTextblockcontratante_emailsdauser_Internalname = "TEXTBLOCKCONTRATANTE_EMAILSDAUSER";
         edtavContratante_emailsdauser_Internalname = "vCONTRATANTE_EMAILSDAUSER";
         lblTextblockcontratante_emailsdapass_Internalname = "TEXTBLOCKCONTRATANTE_EMAILSDAPASS";
         edtavContratante_emailsdapass_Internalname = "vCONTRATANTE_EMAILSDAPASS";
         lblTextblockcontratante_emailsdaport_Internalname = "TEXTBLOCKCONTRATANTE_EMAILSDAPORT";
         edtavContratante_emailsdaport_Internalname = "vCONTRATANTE_EMAILSDAPORT";
         lblTextblockcontratante_emailsdaaut_Internalname = "TEXTBLOCKCONTRATANTE_EMAILSDAAUT";
         cmbavContratante_emailsdaaut_Internalname = "vCONTRATANTE_EMAILSDAAUT";
         lblTextblockcontratante_emailsdasec_Internalname = "TEXTBLOCKCONTRATANTE_EMAILSDASEC";
         cmbavContratante_emailsdasec_Internalname = "vCONTRATANTE_EMAILSDASEC";
         tblContratante_Internalname = "CONTRATANTE";
         Dvpanel_contratante_Internalname = "DVPANEL_CONTRATANTE";
         tblAreadetrabalho_Internalname = "AREADETRABALHO";
         Dvpanel_areadetrabalho_Internalname = "DVPANEL_AREADETRABALHO";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         chkavPodedesativar_Internalname = "vPODEDESATIVAR";
         edtContratante_Codigo_Internalname = "CONTRATANTE_CODIGO";
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_areadetrabalho_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_areadetrabalho_Iconposition = "left";
         Dvpanel_areadetrabalho_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_areadetrabalho_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_areadetrabalho_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_areadetrabalho_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_areadetrabalho_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_areadetrabalho_Title = "�rea de Trabalho";
         Dvpanel_areadetrabalho_Cls = "GXUI-DVelop-Panel";
         Dvpanel_areadetrabalho_Width = "100%";
         Dvpanel_contratante_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_contratante_Iconposition = "left";
         Dvpanel_contratante_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_contratante_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_contratante_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_contratante_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_contratante_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_contratante_Title = "Contratante";
         Dvpanel_contratante_Cls = "GXUI-DVelop-Panel";
         Dvpanel_contratante_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "�rea de Trabalho";
         chkAreaTrabalho_Ativo.Enabled = 1;
         chkAreaTrabalho_Ativo.Visible = 1;
         lblTextblockareatrabalho_ativo_Visible = 1;
         cmbAreaTrabalho_VerTA_Jsonclick = "";
         cmbAreaTrabalho_VerTA.Enabled = 1;
         dynAreaTrabalho_ContratadaUpdBslCod_Jsonclick = "";
         dynAreaTrabalho_ContratadaUpdBslCod.Enabled = 1;
         edtAreaTrabalho_DiasParaPagar_Jsonclick = "";
         edtAreaTrabalho_DiasParaPagar_Enabled = 1;
         cmbAreaTrabalho_ValidaOSFM_Jsonclick = "";
         cmbAreaTrabalho_ValidaOSFM.Enabled = 1;
         dynAreaTrabalho_ServicoPadrao_Jsonclick = "";
         dynAreaTrabalho_ServicoPadrao.Enabled = 1;
         cmbAreaTrabalho_CalculoPFinal_Jsonclick = "";
         cmbAreaTrabalho_CalculoPFinal.Enabled = 1;
         edtAreaTrabalho_Descricao_Jsonclick = "";
         edtAreaTrabalho_Descricao_Enabled = 1;
         edtOrganizacao_Nome_Jsonclick = "";
         edtOrganizacao_Nome_Enabled = 0;
         cmbavContratante_emailsdasec_Jsonclick = "";
         cmbavContratante_emailsdasec.Enabled = 1;
         cmbavContratante_emailsdaaut_Jsonclick = "";
         cmbavContratante_emailsdaaut.Enabled = 1;
         edtavContratante_emailsdaport_Jsonclick = "";
         edtavContratante_emailsdaport_Enabled = 1;
         edtavContratante_emailsdapass_Jsonclick = "";
         edtavContratante_emailsdapass_Enabled = 1;
         edtavContratante_emailsdauser_Jsonclick = "";
         edtavContratante_emailsdauser_Enabled = 1;
         edtavContratante_emailsdahost_Jsonclick = "";
         edtavContratante_emailsdahost_Enabled = 1;
         dynavMunicipio_codigo_Jsonclick = "";
         dynavMunicipio_codigo.Enabled = 0;
         dynavEstado_uf_Jsonclick = "";
         dynavEstado_uf.Enabled = 1;
         edtavContratante_fax_Jsonclick = "";
         edtavContratante_fax_Enabled = 1;
         edtavContratante_ramal_Jsonclick = "";
         edtavContratante_ramal_Enabled = 1;
         edtavContratante_telefone_Jsonclick = "";
         edtavContratante_telefone_Enabled = 1;
         edtavContratante_email_Jsonclick = "";
         edtavContratante_email_Enabled = 1;
         edtavContratante_website_Jsonclick = "";
         edtavContratante_website_Enabled = 1;
         edtavContratante_nomefantasia_Jsonclick = "";
         edtavContratante_nomefantasia_Enabled = 1;
         edtavContratante_razaosocial_Jsonclick = "";
         edtavContratante_razaosocial_Enabled = 1;
         edtavContratante_ie_Jsonclick = "";
         edtavContratante_ie_Enabled = 1;
         edtavContratante_cnpj_Jsonclick = "";
         edtavContratante_cnpj_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtAreaTrabalho_Codigo_Jsonclick = "";
         edtAreaTrabalho_Codigo_Enabled = 0;
         edtAreaTrabalho_Codigo_Visible = 1;
         edtContratante_Codigo_Jsonclick = "";
         edtContratante_Codigo_Enabled = 1;
         edtContratante_Codigo_Visible = 1;
         chkavPodedesativar.Enabled = 0;
         chkavPodedesativar.Visible = 1;
         chkavPodedesativar.Caption = "";
         chkAreaTrabalho_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         GXVvMUNICIPIO_CODIGO_html04106( AV24Estado_UF) ;
         /* End function dynload_actions */
      }

      protected void GXDLAAREATRABALHO_SERVICOPADRAO04106( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAAREATRABALHO_SERVICOPADRAO_data04106( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAAREATRABALHO_SERVICOPADRAO_html04106( )
      {
         int gxdynajaxvalue ;
         GXDLAAREATRABALHO_SERVICOPADRAO_data04106( ) ;
         gxdynajaxindex = 1;
         dynAreaTrabalho_ServicoPadrao.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynAreaTrabalho_ServicoPadrao.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAAREATRABALHO_SERVICOPADRAO_data04106( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000463 */
         pr_default.execute(57);
         while ( (pr_default.getStatus(57) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000463_A830AreaTrabalho_ServicoPadrao[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000463_A605Servico_Sigla[0]));
            pr_default.readNext(57);
         }
         pr_default.close(57);
      }

      protected void GXDLAAREATRABALHO_CONTRATADAUPDBSLCOD04106( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAAREATRABALHO_CONTRATADAUPDBSLCOD_data04106( AV8WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAAREATRABALHO_CONTRATADAUPDBSLCOD_html04106( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLAAREATRABALHO_CONTRATADAUPDBSLCOD_data04106( AV8WWPContext) ;
         gxdynajaxindex = 1;
         dynAreaTrabalho_ContratadaUpdBslCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynAreaTrabalho_ContratadaUpdBslCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAAREATRABALHO_CONTRATADAUPDBSLCOD_data04106( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T000464 */
         pr_default.execute(58, new Object[] {AV8WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(58) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000464_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000464_A41Contratada_PessoaNom[0]));
            pr_default.readNext(58);
         }
         pr_default.close(58);
      }

      protected void GXDLVvESTADO_UF04106( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvESTADO_UF_data04106( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvESTADO_UF_html04106( )
      {
         String gxdynajaxvalue ;
         GXDLVvESTADO_UF_data04106( ) ;
         gxdynajaxindex = 1;
         dynavEstado_uf.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavEstado_uf.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvESTADO_UF_data04106( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000465 */
         pr_default.execute(59);
         while ( (pr_default.getStatus(59) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( T000465_A23Estado_UF[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000465_A24Estado_Nome[0]));
            pr_default.readNext(59);
         }
         pr_default.close(59);
      }

      protected void GXDLVvMUNICIPIO_CODIGO04106( String AV24Estado_UF )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvMUNICIPIO_CODIGO_data04106( AV24Estado_UF) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvMUNICIPIO_CODIGO_html04106( String AV24Estado_UF )
      {
         int gxdynajaxvalue ;
         GXDLVvMUNICIPIO_CODIGO_data04106( AV24Estado_UF) ;
         gxdynajaxindex = 1;
         dynavMunicipio_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavMunicipio_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvMUNICIPIO_CODIGO_data04106( String AV24Estado_UF )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000466 */
         pr_default.execute(60, new Object[] {AV24Estado_UF});
         while ( (pr_default.getStatus(60) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000466_A25Municipio_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000466_A26Municipio_Nome[0]));
            pr_default.readNext(60);
         }
         pr_default.close(60);
      }

      protected void GX8ASACONTRATANTE_CODIGO04106( int AV11Insert_Contratante_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contratante_Codigo) )
         {
            A29Contratante_Codigo = AV11Insert_Contratante_Codigo;
            n29Contratante_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX9ASACONTRATANTE_CODIGO04106( int AV26Contratante_Codigo ,
                                                    String Gx_mode ,
                                                    String AV15Contratante_CNPJ ,
                                                    String AV16Contratante_RazaoSocial ,
                                                    String AV17Contratante_NomeFantasia ,
                                                    String AV18Contratante_IE ,
                                                    String AV21Contratante_Telefone ,
                                                    String AV22Contratante_Ramal ,
                                                    String AV23Contratante_Fax ,
                                                    String AV20Contratante_Email ,
                                                    String AV19Contratante_WebSite ,
                                                    int AV25Municipio_Codigo ,
                                                    String AV32Contratante_EmailSdaHost ,
                                                    String AV33Contratante_EmailSdaUser ,
                                                    String AV35Contratante_EmailSdaPass ,
                                                    short AV36Contratante_EmailSdaPort ,
                                                    bool AV37Contratante_EmailSdaAut ,
                                                    short AV34Contratante_EmailSdaSec )
      {
         if ( (0==AV26Contratante_Codigo) )
         {
            GXt_int1 = A29Contratante_Codigo;
            new prc_novapessoacontratante(context ).execute(  AV15Contratante_CNPJ,  AV16Contratante_RazaoSocial,  AV17Contratante_NomeFantasia,  AV18Contratante_IE,  AV21Contratante_Telefone,  AV22Contratante_Ramal,  AV23Contratante_Fax,  AV20Contratante_Email,  AV19Contratante_WebSite,  AV25Municipio_Codigo,  AV32Contratante_EmailSdaHost,  AV33Contratante_EmailSdaUser,  AV35Contratante_EmailSdaPass,  AV36Contratante_EmailSdaPort,  AV37Contratante_EmailSdaAut,  AV34Contratante_EmailSdaSec, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratante_CNPJ", AV15Contratante_CNPJ);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contratante_RazaoSocial", AV16Contratante_RazaoSocial);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratante_NomeFantasia", AV17Contratante_NomeFantasia);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratante_IE", AV18Contratante_IE);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Contratante_Telefone", AV21Contratante_Telefone);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratante_Ramal", AV22Contratante_Ramal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratante_Fax", AV23Contratante_Fax);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratante_Email", AV20Contratante_Email);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Contratante_WebSite", AV19Contratante_WebSite);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Contratante_EmailSdaHost", AV32Contratante_EmailSdaHost);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratante_EmailSdaUser", AV33Contratante_EmailSdaUser);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratante_EmailSdaPass", AV35Contratante_EmailSdaPass);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contratante_EmailSdaPort), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contratante_EmailSdaAut", AV37Contratante_EmailSdaAut);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0)));
            A29Contratante_Codigo = GXt_int1;
            n29Contratante_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ! (0==AV26Contratante_Codigo) )
            {
               A29Contratante_Codigo = AV26Contratante_Codigo;
               n29Contratante_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_66_04106( wwpbaseobjects.SdtAuditingObject AV30AuditingObject ,
                                  int A5AreaTrabalho_Codigo ,
                                  String Gx_mode )
      {
         new loadauditareatrabalho(context ).execute(  "Y", ref  AV30AuditingObject,  A5AreaTrabalho_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV30AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_67_04106( wwpbaseobjects.SdtAuditingObject AV30AuditingObject ,
                                  int A5AreaTrabalho_Codigo ,
                                  String Gx_mode )
      {
         new loadauditareatrabalho(context ).execute(  "Y", ref  AV30AuditingObject,  A5AreaTrabalho_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV30AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_68_04106( String Gx_mode ,
                                  wwpbaseobjects.SdtAuditingObject AV30AuditingObject ,
                                  int A5AreaTrabalho_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditareatrabalho(context ).execute(  "N", ref  AV30AuditingObject,  A5AreaTrabalho_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV30AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_69_04106( String Gx_mode ,
                                  wwpbaseobjects.SdtAuditingObject AV30AuditingObject ,
                                  int A5AreaTrabalho_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditareatrabalho(context ).execute(  "N", ref  AV30AuditingObject,  A5AreaTrabalho_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV30AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_70_04106( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         new wwpbaseobjects.setwwpcontext(context ).execute(  AV8WWPContext) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Areatrabalho_codigo( int GX_Parm1 ,
                                             short GX_Parm2 )
      {
         A5AreaTrabalho_Codigo = GX_Parm1;
         A272AreaTrabalho_ContagensQtdGeral = GX_Parm2;
         n272AreaTrabalho_ContagensQtdGeral = false;
         /* Using cursor T000468 */
         pr_default.execute(61, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(61) != 101) )
         {
            A272AreaTrabalho_ContagensQtdGeral = T000468_A272AreaTrabalho_ContagensQtdGeral[0];
            n272AreaTrabalho_ContagensQtdGeral = T000468_n272AreaTrabalho_ContagensQtdGeral[0];
         }
         else
         {
            A272AreaTrabalho_ContagensQtdGeral = 0;
            n272AreaTrabalho_ContagensQtdGeral = false;
         }
         pr_default.close(61);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A272AreaTrabalho_ContagensQtdGeral = 0;
            n272AreaTrabalho_ContagensQtdGeral = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A272AreaTrabalho_ContagensQtdGeral), 3, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Areatrabalho_servicopadrao( GXCombobox dynGX_Parm1 )
      {
         dynAreaTrabalho_ServicoPadrao = dynGX_Parm1;
         A830AreaTrabalho_ServicoPadrao = (int)(NumberUtil.Val( dynAreaTrabalho_ServicoPadrao.CurrentValue, "."));
         n830AreaTrabalho_ServicoPadrao = false;
         /* Using cursor T000469 */
         pr_default.execute(62, new Object[] {n830AreaTrabalho_ServicoPadrao, A830AreaTrabalho_ServicoPadrao});
         if ( (pr_default.getStatus(62) == 101) )
         {
            if ( ! ( (0==A830AreaTrabalho_ServicoPadrao) ) )
            {
               GX_msglist.addItem("N�o existe 'Area Trabalho_Servicos'.", "ForeignKeyNotFound", 1, "AREATRABALHO_SERVICOPADRAO");
               AnyError = 1;
               GX_FocusControl = dynAreaTrabalho_ServicoPadrao_Internalname;
            }
         }
         pr_default.close(62);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Areatrabalho_ativo( bool GX_Parm1 ,
                                            bool GX_Parm2 ,
                                            int GX_Parm3 ,
                                            String GX_Parm4 )
      {
         O72AreaTrabalho_Ativo = GX_Parm1;
         A72AreaTrabalho_Ativo = GX_Parm2;
         A5AreaTrabalho_Codigo = GX_Parm3;
         AV39Causa = GX_Parm4;
         if ( ! A72AreaTrabalho_Ativo && ( O72AreaTrabalho_Ativo ) && ! new prc_podedesativararea(context).executeUdp( ref  A5AreaTrabalho_Codigo, out  AV39Causa) )
         {
            GX_msglist.addItem(AV39Causa, 1, "AREATRABALHO_ATIVO");
            AnyError = 1;
            GX_FocusControl = chkAreaTrabalho_Ativo_Internalname;
         }
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contratante_cnpj( String GX_Parm1 ,
                                           String GX_Parm2 )
      {
         Gx_mode = GX_Parm1;
         AV15Contratante_CNPJ = GX_Parm2;
         edtavContratante_cnpj_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         edtavContratante_email_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         edtavContratante_fax_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         edtavContratante_ie_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         edtavContratante_nomefantasia_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         edtavContratante_ramal_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         edtavContratante_razaosocial_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         edtavContratante_telefone_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         edtavContratante_website_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         dynavMunicipio_codigo.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         dynavEstado_uf.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         edtavContratante_emailsdahost_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         edtavContratante_emailsdauser_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         edtavContratante_emailsdapass_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         edtavContratante_emailsdaport_Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         cmbavContratante_emailsdaaut.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         cmbavContratante_emailsdasec.Enabled = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) ? 1 : 0);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15Contratante_CNPJ)) )
         {
            GX_msglist.addItem("CNPJ � obrigat�rio.", 1, "vCONTRATANTE_CNPJ");
            AnyError = 1;
            GX_FocusControl = edtavContratante_cnpj_Internalname;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(edtavContratante_cnpj_Enabled);
         isValidOutput.Add(edtavContratante_email_Enabled);
         isValidOutput.Add(edtavContratante_fax_Enabled);
         isValidOutput.Add(edtavContratante_ie_Enabled);
         isValidOutput.Add(edtavContratante_nomefantasia_Enabled);
         isValidOutput.Add(edtavContratante_ramal_Enabled);
         isValidOutput.Add(edtavContratante_razaosocial_Enabled);
         isValidOutput.Add(edtavContratante_telefone_Enabled);
         isValidOutput.Add(edtavContratante_website_Enabled);
         isValidOutput.Add(dynavMunicipio_codigo.Enabled);
         isValidOutput.Add(dynavEstado_uf.Enabled);
         isValidOutput.Add(edtavContratante_emailsdahost_Enabled);
         isValidOutput.Add(edtavContratante_emailsdauser_Enabled);
         isValidOutput.Add(edtavContratante_emailsdapass_Enabled);
         isValidOutput.Add(edtavContratante_emailsdaport_Enabled);
         isValidOutput.Add(cmbavContratante_emailsdaaut.Enabled);
         isValidOutput.Add(cmbavContratante_emailsdasec.Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Estado_uf( GXCombobox dynGX_Parm1 ,
                                    GXCombobox dynGX_Parm2 )
      {
         dynavEstado_uf = dynGX_Parm1;
         AV24Estado_UF = dynavEstado_uf.CurrentValue;
         dynavMunicipio_codigo = dynGX_Parm2;
         AV25Municipio_Codigo = (int)(NumberUtil.Val( dynavMunicipio_codigo.CurrentValue, "."));
         GXVvMUNICIPIO_CODIGO_html04106( AV24Estado_UF) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynavMunicipio_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0));
         if ( dynavMunicipio_codigo.ItemCount > 0 )
         {
            AV25Municipio_Codigo = (int)(NumberUtil.Val( dynavMunicipio_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0))), "."));
         }
         dynavMunicipio_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0));
         isValidOutput.Add(dynavMunicipio_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratante_codigo( String GX_Parm1 ,
                                            bool GX_Parm2 ,
                                            int GX_Parm3 ,
                                            String GX_Parm4 ,
                                            String GX_Parm5 ,
                                            String GX_Parm6 ,
                                            String GX_Parm7 ,
                                            String GX_Parm8 ,
                                            String GX_Parm9 ,
                                            String GX_Parm10 ,
                                            String GX_Parm11 ,
                                            String GX_Parm12 ,
                                            String GX_Parm13 ,
                                            String GX_Parm14 ,
                                            bool GX_Parm15 ,
                                            short GX_Parm16 ,
                                            short GX_Parm17 ,
                                            int GX_Parm18 ,
                                            String GX_Parm19 ,
                                            int GX_Parm20 ,
                                            String GX_Parm21 ,
                                            String GX_Parm22 ,
                                            String GX_Parm23 ,
                                            String GX_Parm24 ,
                                            String GX_Parm25 ,
                                            String GX_Parm26 ,
                                            String GX_Parm27 ,
                                            String GX_Parm28 ,
                                            String GX_Parm29 ,
                                            String GX_Parm30 ,
                                            String GX_Parm31 ,
                                            String GX_Parm32 ,
                                            GXCombobox cmbGX_Parm33 ,
                                            short GX_Parm34 ,
                                            GXCombobox cmbGX_Parm35 ,
                                            String GX_Parm36 ,
                                            String GX_Parm37 ,
                                            GXCombobox dynGX_Parm38 ,
                                            GXCombobox dynGX_Parm39 ,
                                            String GX_Parm40 ,
                                            String GX_Parm41 ,
                                            int GX_Parm42 )
      {
         Gx_mode = GX_Parm1;
         A72AreaTrabalho_Ativo = GX_Parm2;
         A29Contratante_Codigo = GX_Parm3;
         n29Contratante_Codigo = false;
         A33Contratante_Fax = GX_Parm4;
         n33Contratante_Fax = false;
         A32Contratante_Ramal = GX_Parm5;
         n32Contratante_Ramal = false;
         A31Contratante_Telefone = GX_Parm6;
         A14Contratante_Email = GX_Parm7;
         n14Contratante_Email = false;
         A13Contratante_WebSite = GX_Parm8;
         n13Contratante_WebSite = false;
         A11Contratante_IE = GX_Parm9;
         A10Contratante_NomeFantasia = GX_Parm10;
         A547Contratante_EmailSdaHost = GX_Parm11;
         n547Contratante_EmailSdaHost = false;
         A548Contratante_EmailSdaUser = GX_Parm12;
         n548Contratante_EmailSdaUser = false;
         A549Contratante_EmailSdaPass = GX_Parm13;
         n549Contratante_EmailSdaPass = false;
         A550Contratante_EmailSdaKey = GX_Parm14;
         n550Contratante_EmailSdaKey = false;
         A551Contratante_EmailSdaAut = GX_Parm15;
         n551Contratante_EmailSdaAut = false;
         A552Contratante_EmailSdaPort = GX_Parm16;
         n552Contratante_EmailSdaPort = false;
         A1048Contratante_EmailSdaSec = GX_Parm17;
         n1048Contratante_EmailSdaSec = false;
         A25Municipio_Codigo = GX_Parm18;
         n25Municipio_Codigo = false;
         A23Estado_UF = GX_Parm19;
         A335Contratante_PessoaCod = GX_Parm20;
         A12Contratante_CNPJ = GX_Parm21;
         n12Contratante_CNPJ = false;
         A9Contratante_RazaoSocial = GX_Parm22;
         n9Contratante_RazaoSocial = false;
         AV23Contratante_Fax = GX_Parm23;
         AV22Contratante_Ramal = GX_Parm24;
         AV21Contratante_Telefone = GX_Parm25;
         AV20Contratante_Email = GX_Parm26;
         AV19Contratante_WebSite = GX_Parm27;
         AV18Contratante_IE = GX_Parm28;
         AV17Contratante_NomeFantasia = GX_Parm29;
         AV32Contratante_EmailSdaHost = GX_Parm30;
         AV33Contratante_EmailSdaUser = GX_Parm31;
         AV35Contratante_EmailSdaPass = GX_Parm32;
         cmbavContratante_emailsdaaut = cmbGX_Parm33;
         AV37Contratante_EmailSdaAut = StringUtil.StrToBool( cmbavContratante_emailsdaaut.CurrentValue);
         cmbavContratante_emailsdaaut.CurrentValue = StringUtil.BoolToStr( AV37Contratante_EmailSdaAut);
         AV36Contratante_EmailSdaPort = GX_Parm34;
         cmbavContratante_emailsdasec = cmbGX_Parm35;
         AV34Contratante_EmailSdaSec = (short)(NumberUtil.Val( cmbavContratante_emailsdasec.CurrentValue, "."));
         cmbavContratante_emailsdasec.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0));
         A26Municipio_Nome = GX_Parm36;
         A24Estado_Nome = GX_Parm37;
         dynavEstado_uf = dynGX_Parm38;
         AV24Estado_UF = dynavEstado_uf.CurrentValue;
         dynavMunicipio_codigo = dynGX_Parm39;
         AV25Municipio_Codigo = (int)(NumberUtil.Val( dynavMunicipio_codigo.CurrentValue, "."));
         AV15Contratante_CNPJ = GX_Parm40;
         AV16Contratante_RazaoSocial = GX_Parm41;
         AV26Contratante_Codigo = GX_Parm42;
         /* Using cursor T000470 */
         pr_default.execute(63, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         if ( (pr_default.getStatus(63) == 101) )
         {
            if ( ! ( (0==A29Contratante_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Contratante'.", "ForeignKeyNotFound", 1, "CONTRATANTE_CODIGO");
               AnyError = 1;
               GX_FocusControl = edtContratante_Codigo_Internalname;
            }
         }
         A33Contratante_Fax = T000470_A33Contratante_Fax[0];
         n33Contratante_Fax = T000470_n33Contratante_Fax[0];
         A32Contratante_Ramal = T000470_A32Contratante_Ramal[0];
         n32Contratante_Ramal = T000470_n32Contratante_Ramal[0];
         A31Contratante_Telefone = T000470_A31Contratante_Telefone[0];
         A14Contratante_Email = T000470_A14Contratante_Email[0];
         n14Contratante_Email = T000470_n14Contratante_Email[0];
         A13Contratante_WebSite = T000470_A13Contratante_WebSite[0];
         n13Contratante_WebSite = T000470_n13Contratante_WebSite[0];
         A11Contratante_IE = T000470_A11Contratante_IE[0];
         A10Contratante_NomeFantasia = T000470_A10Contratante_NomeFantasia[0];
         A547Contratante_EmailSdaHost = T000470_A547Contratante_EmailSdaHost[0];
         n547Contratante_EmailSdaHost = T000470_n547Contratante_EmailSdaHost[0];
         A548Contratante_EmailSdaUser = T000470_A548Contratante_EmailSdaUser[0];
         n548Contratante_EmailSdaUser = T000470_n548Contratante_EmailSdaUser[0];
         A549Contratante_EmailSdaPass = T000470_A549Contratante_EmailSdaPass[0];
         n549Contratante_EmailSdaPass = T000470_n549Contratante_EmailSdaPass[0];
         A550Contratante_EmailSdaKey = T000470_A550Contratante_EmailSdaKey[0];
         n550Contratante_EmailSdaKey = T000470_n550Contratante_EmailSdaKey[0];
         A551Contratante_EmailSdaAut = T000470_A551Contratante_EmailSdaAut[0];
         n551Contratante_EmailSdaAut = T000470_n551Contratante_EmailSdaAut[0];
         A552Contratante_EmailSdaPort = T000470_A552Contratante_EmailSdaPort[0];
         n552Contratante_EmailSdaPort = T000470_n552Contratante_EmailSdaPort[0];
         A1048Contratante_EmailSdaSec = T000470_A1048Contratante_EmailSdaSec[0];
         n1048Contratante_EmailSdaSec = T000470_n1048Contratante_EmailSdaSec[0];
         A25Municipio_Codigo = T000470_A25Municipio_Codigo[0];
         n25Municipio_Codigo = T000470_n25Municipio_Codigo[0];
         A335Contratante_PessoaCod = T000470_A335Contratante_PessoaCod[0];
         pr_default.close(63);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV23Contratante_Fax = A33Contratante_Fax;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV22Contratante_Ramal = A32Contratante_Ramal;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV21Contratante_Telefone = A31Contratante_Telefone;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV20Contratante_Email = A14Contratante_Email;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV19Contratante_WebSite = A13Contratante_WebSite;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV18Contratante_IE = A11Contratante_IE;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV17Contratante_NomeFantasia = A10Contratante_NomeFantasia;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV32Contratante_EmailSdaHost = A547Contratante_EmailSdaHost;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV33Contratante_EmailSdaUser = A548Contratante_EmailSdaUser;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ! String.IsNullOrEmpty(StringUtil.RTrim( A550Contratante_EmailSdaKey)) )
         {
            AV35Contratante_EmailSdaPass = Crypto.Decrypt64( A549Contratante_EmailSdaPass, A550Contratante_EmailSdaKey);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV37Contratante_EmailSdaAut = A551Contratante_EmailSdaAut;
            cmbavContratante_emailsdaaut.CurrentValue = StringUtil.BoolToStr( AV37Contratante_EmailSdaAut);
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV36Contratante_EmailSdaPort = A552Contratante_EmailSdaPort;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV34Contratante_EmailSdaSec = A1048Contratante_EmailSdaSec;
            cmbavContratante_emailsdasec.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0));
         }
         /* Using cursor T000471 */
         pr_default.execute(64, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(64) == 101) )
         {
            if ( ! ( (0==A25Municipio_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Municipio'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A26Municipio_Nome = T000471_A26Municipio_Nome[0];
         A23Estado_UF = T000471_A23Estado_UF[0];
         pr_default.close(64);
         /* Using cursor T000472 */
         pr_default.execute(65, new Object[] {A23Estado_UF});
         if ( (pr_default.getStatus(65) == 101) )
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A23Estado_UF)) ) )
            {
               GX_msglist.addItem("N�o existe 'Estado'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A24Estado_Nome = T000472_A24Estado_Nome[0];
         pr_default.close(65);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV24Estado_UF = A23Estado_UF;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV25Municipio_Codigo = A25Municipio_Codigo;
         }
         /* Using cursor T000473 */
         pr_default.execute(66, new Object[] {A335Contratante_PessoaCod});
         if ( (pr_default.getStatus(66) == 101) )
         {
            if ( ! ( (0==A335Contratante_PessoaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratante_Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A12Contratante_CNPJ = T000473_A12Contratante_CNPJ[0];
         n12Contratante_CNPJ = T000473_n12Contratante_CNPJ[0];
         A9Contratante_RazaoSocial = T000473_A9Contratante_RazaoSocial[0];
         n9Contratante_RazaoSocial = T000473_n9Contratante_RazaoSocial[0];
         pr_default.close(66);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV15Contratante_CNPJ = A12Contratante_CNPJ;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV16Contratante_RazaoSocial = A9Contratante_RazaoSocial;
         }
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV26Contratante_Codigo = A29Contratante_Codigo;
         }
         O72AreaTrabalho_Ativo = A72AreaTrabalho_Ativo;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A33Contratante_Fax = "";
            n33Contratante_Fax = false;
            A32Contratante_Ramal = "";
            n32Contratante_Ramal = false;
            A31Contratante_Telefone = "";
            A14Contratante_Email = "";
            n14Contratante_Email = false;
            A13Contratante_WebSite = "";
            n13Contratante_WebSite = false;
            A11Contratante_IE = "";
            A10Contratante_NomeFantasia = "";
            A547Contratante_EmailSdaHost = "";
            n547Contratante_EmailSdaHost = false;
            A548Contratante_EmailSdaUser = "";
            n548Contratante_EmailSdaUser = false;
            A549Contratante_EmailSdaPass = "";
            n549Contratante_EmailSdaPass = false;
            A550Contratante_EmailSdaKey = "";
            n550Contratante_EmailSdaKey = false;
            A551Contratante_EmailSdaAut = false;
            n551Contratante_EmailSdaAut = false;
            A552Contratante_EmailSdaPort = 0;
            n552Contratante_EmailSdaPort = false;
            A1048Contratante_EmailSdaSec = 0;
            n1048Contratante_EmailSdaSec = false;
            A25Municipio_Codigo = 0;
            n25Municipio_Codigo = false;
            A335Contratante_PessoaCod = 0;
            A26Municipio_Nome = "";
            A23Estado_UF = "";
            A24Estado_Nome = "";
            A12Contratante_CNPJ = "";
            n12Contratante_CNPJ = false;
            A9Contratante_RazaoSocial = "";
            n9Contratante_RazaoSocial = false;
         }
         GXVvESTADO_UF_html04106( ) ;
         dynavEstado_uf.CurrentValue = AV24Estado_UF;
         dynavMunicipio_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0));
         isValidOutput.Add(StringUtil.RTrim( A33Contratante_Fax));
         isValidOutput.Add(StringUtil.RTrim( A32Contratante_Ramal));
         isValidOutput.Add(StringUtil.RTrim( A31Contratante_Telefone));
         isValidOutput.Add(A14Contratante_Email);
         isValidOutput.Add(A13Contratante_WebSite);
         isValidOutput.Add(StringUtil.RTrim( A11Contratante_IE));
         isValidOutput.Add(StringUtil.RTrim( A10Contratante_NomeFantasia));
         isValidOutput.Add(A547Contratante_EmailSdaHost);
         isValidOutput.Add(A548Contratante_EmailSdaUser);
         isValidOutput.Add(A549Contratante_EmailSdaPass);
         isValidOutput.Add(StringUtil.RTrim( A550Contratante_EmailSdaKey));
         isValidOutput.Add(A551Contratante_EmailSdaAut);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A552Contratante_EmailSdaPort), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1048Contratante_EmailSdaSec), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( AV23Contratante_Fax));
         isValidOutput.Add(StringUtil.RTrim( AV22Contratante_Ramal));
         isValidOutput.Add(StringUtil.RTrim( AV21Contratante_Telefone));
         isValidOutput.Add(AV20Contratante_Email);
         isValidOutput.Add(AV19Contratante_WebSite);
         isValidOutput.Add(StringUtil.RTrim( AV18Contratante_IE));
         isValidOutput.Add(StringUtil.RTrim( AV17Contratante_NomeFantasia));
         isValidOutput.Add(AV32Contratante_EmailSdaHost);
         isValidOutput.Add(AV33Contratante_EmailSdaUser);
         isValidOutput.Add(AV35Contratante_EmailSdaPass);
         cmbavContratante_emailsdaaut.CurrentValue = StringUtil.BoolToStr( AV37Contratante_EmailSdaAut);
         isValidOutput.Add(cmbavContratante_emailsdaaut);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Contratante_EmailSdaPort), 4, 0, ".", "")));
         cmbavContratante_emailsdasec.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Contratante_EmailSdaSec), 4, 0));
         isValidOutput.Add(cmbavContratante_emailsdasec);
         isValidOutput.Add(StringUtil.RTrim( A26Municipio_Nome));
         isValidOutput.Add(StringUtil.RTrim( A23Estado_UF));
         isValidOutput.Add(StringUtil.RTrim( A24Estado_Nome));
         if ( dynavEstado_uf.ItemCount > 0 )
         {
            AV24Estado_UF = dynavEstado_uf.getValidValue(AV24Estado_UF);
         }
         dynavEstado_uf.CurrentValue = AV24Estado_UF;
         isValidOutput.Add(dynavEstado_uf);
         if ( dynavMunicipio_codigo.ItemCount > 0 )
         {
            AV25Municipio_Codigo = (int)(NumberUtil.Val( dynavMunicipio_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0))), "."));
         }
         dynavMunicipio_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25Municipio_Codigo), 6, 0));
         isValidOutput.Add(dynavMunicipio_codigo);
         isValidOutput.Add(A12Contratante_CNPJ);
         isValidOutput.Add(StringUtil.RTrim( A9Contratante_RazaoSocial));
         isValidOutput.Add(AV15Contratante_CNPJ);
         isValidOutput.Add(StringUtil.RTrim( AV16Contratante_RazaoSocial));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26Contratante_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12042',iparms:[{av:'A830AreaTrabalho_ServicoPadrao',fld:'AREATRABALHO_SERVICOPADRAO',pic:'ZZZZZ9',nv:0},{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV30AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VCONTRATANTE_CNPJ.ISVALID","{handler:'E13042',iparms:[{av:'AV15Contratante_CNPJ',fld:'vCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV20Contratante_Email',fld:'vCONTRATANTE_EMAIL',pic:'',nv:''},{av:'AV23Contratante_Fax',fld:'vCONTRATANTE_FAX',pic:'',nv:''},{av:'AV18Contratante_IE',fld:'vCONTRATANTE_IE',pic:'',nv:''},{av:'AV17Contratante_NomeFantasia',fld:'vCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV22Contratante_Ramal',fld:'vCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV16Contratante_RazaoSocial',fld:'vCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV21Contratante_Telefone',fld:'vCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV19Contratante_WebSite',fld:'vCONTRATANTE_WEBSITE',pic:'',nv:''},{av:'AV25Municipio_Codigo',fld:'vMUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24Estado_UF',fld:'vESTADO_UF',pic:'@!',nv:''}],oparms:[{av:'AV26Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(63);
         pr_default.close(24);
         pr_default.close(62);
         pr_default.close(28);
         pr_default.close(64);
         pr_default.close(25);
         pr_default.close(65);
         pr_default.close(26);
         pr_default.close(66);
         pr_default.close(27);
         pr_default.close(61);
         pr_default.close(23);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z6AreaTrabalho_Descricao = "";
         Z642AreaTrabalho_CalculoPFinal = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV24Estado_UF = "";
         AV15Contratante_CNPJ = "";
         AV16Contratante_RazaoSocial = "";
         AV17Contratante_NomeFantasia = "";
         AV18Contratante_IE = "";
         AV21Contratante_Telefone = "";
         AV22Contratante_Ramal = "";
         AV23Contratante_Fax = "";
         AV20Contratante_Email = "";
         AV19Contratante_WebSite = "";
         AV32Contratante_EmailSdaHost = "";
         AV33Contratante_EmailSdaUser = "";
         AV35Contratante_EmailSdaPass = "";
         AV37Contratante_EmailSdaAut = false;
         A23Estado_UF = "";
         GXKey = "";
         A642AreaTrabalho_CalculoPFinal = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         sStyleString = "";
         lblAreatrabalhotitle_Jsonclick = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratante_cnpj_Jsonclick = "";
         lblTextblockcontratante_ie_Jsonclick = "";
         lblTextblockcontratante_razaosocial_Jsonclick = "";
         lblTextblockcontratante_nomefantasia_Jsonclick = "";
         lblTextblockcontratante_website_Jsonclick = "";
         lblTextblockcontratante_email_Jsonclick = "";
         lblTextblockcontratante_telefone_Jsonclick = "";
         lblTextblockcontratante_ramal_Jsonclick = "";
         lblTextblockcontratante_fax_Jsonclick = "";
         lblTextblockestado_uf_Jsonclick = "";
         lblTextblockmunicipio_codigo_Jsonclick = "";
         lblLblemail_Jsonclick = "";
         lblTextblockcontratante_emailsdahost_Jsonclick = "";
         lblTextblockcontratante_emailsdauser_Jsonclick = "";
         lblTextblockcontratante_emailsdapass_Jsonclick = "";
         lblTextblockcontratante_emailsdaport_Jsonclick = "";
         lblTextblockcontratante_emailsdaaut_Jsonclick = "";
         lblTextblockcontratante_emailsdasec_Jsonclick = "";
         lblTextblockorganizacao_nome_Jsonclick = "";
         A1214Organizacao_Nome = "";
         lblTextblockareatrabalho_descricao_Jsonclick = "";
         A6AreaTrabalho_Descricao = "";
         lblTextblockareatrabalho_calculopfinal_Jsonclick = "";
         lblTextblockareatrabalho_servicopadrao_Jsonclick = "";
         lblTextblockareatrabalho_validaosfm_Jsonclick = "";
         lblTextblockareatrabalho_diasparapagar_Jsonclick = "";
         lblTextblockareatrabalho_contratadaupdbslcod_Jsonclick = "";
         lblTextblockareatrabalho_verta_Jsonclick = "";
         lblTextblockareatrabalho_ativo_Jsonclick = "";
         A12Contratante_CNPJ = "";
         A14Contratante_Email = "";
         A33Contratante_Fax = "";
         A11Contratante_IE = "";
         A10Contratante_NomeFantasia = "";
         A32Contratante_Ramal = "";
         A9Contratante_RazaoSocial = "";
         A31Contratante_Telefone = "";
         A13Contratante_WebSite = "";
         A547Contratante_EmailSdaHost = "";
         A548Contratante_EmailSdaUser = "";
         A549Contratante_EmailSdaPass = "";
         A550Contratante_EmailSdaKey = "";
         AV30AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV39Causa = "";
         A26Municipio_Nome = "";
         A24Estado_Nome = "";
         AV40Pgmname = "";
         Dvpanel_contratante_Height = "";
         Dvpanel_contratante_Class = "";
         Dvpanel_areadetrabalho_Height = "";
         Dvpanel_areadetrabalho_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode106 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV31ParametrosSistema = new SdtParametrosSistema(context);
         Z1214Organizacao_Nome = "";
         Z33Contratante_Fax = "";
         Z32Contratante_Ramal = "";
         Z31Contratante_Telefone = "";
         Z14Contratante_Email = "";
         Z13Contratante_WebSite = "";
         Z11Contratante_IE = "";
         Z10Contratante_NomeFantasia = "";
         Z547Contratante_EmailSdaHost = "";
         Z548Contratante_EmailSdaUser = "";
         Z549Contratante_EmailSdaPass = "";
         Z550Contratante_EmailSdaKey = "";
         Z26Municipio_Nome = "";
         Z23Estado_UF = "";
         Z24Estado_Nome = "";
         Z12Contratante_CNPJ = "";
         Z9Contratante_RazaoSocial = "";
         T000411_A272AreaTrabalho_ContagensQtdGeral = new short[1] ;
         T000411_n272AreaTrabalho_ContagensQtdGeral = new bool[] {false} ;
         T00044_A33Contratante_Fax = new String[] {""} ;
         T00044_n33Contratante_Fax = new bool[] {false} ;
         T00044_A32Contratante_Ramal = new String[] {""} ;
         T00044_n32Contratante_Ramal = new bool[] {false} ;
         T00044_A31Contratante_Telefone = new String[] {""} ;
         T00044_A14Contratante_Email = new String[] {""} ;
         T00044_n14Contratante_Email = new bool[] {false} ;
         T00044_A13Contratante_WebSite = new String[] {""} ;
         T00044_n13Contratante_WebSite = new bool[] {false} ;
         T00044_A11Contratante_IE = new String[] {""} ;
         T00044_A10Contratante_NomeFantasia = new String[] {""} ;
         T00044_A547Contratante_EmailSdaHost = new String[] {""} ;
         T00044_n547Contratante_EmailSdaHost = new bool[] {false} ;
         T00044_A548Contratante_EmailSdaUser = new String[] {""} ;
         T00044_n548Contratante_EmailSdaUser = new bool[] {false} ;
         T00044_A549Contratante_EmailSdaPass = new String[] {""} ;
         T00044_n549Contratante_EmailSdaPass = new bool[] {false} ;
         T00044_A550Contratante_EmailSdaKey = new String[] {""} ;
         T00044_n550Contratante_EmailSdaKey = new bool[] {false} ;
         T00044_A551Contratante_EmailSdaAut = new bool[] {false} ;
         T00044_n551Contratante_EmailSdaAut = new bool[] {false} ;
         T00044_A552Contratante_EmailSdaPort = new short[1] ;
         T00044_n552Contratante_EmailSdaPort = new bool[] {false} ;
         T00044_A1048Contratante_EmailSdaSec = new short[1] ;
         T00044_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         T00044_A25Municipio_Codigo = new int[1] ;
         T00044_n25Municipio_Codigo = new bool[] {false} ;
         T00044_A335Contratante_PessoaCod = new int[1] ;
         T00047_A26Municipio_Nome = new String[] {""} ;
         T00047_A23Estado_UF = new String[] {""} ;
         T00048_A24Estado_Nome = new String[] {""} ;
         T00049_A12Contratante_CNPJ = new String[] {""} ;
         T00049_n12Contratante_CNPJ = new bool[] {false} ;
         T00049_A9Contratante_RazaoSocial = new String[] {""} ;
         T00049_n9Contratante_RazaoSocial = new bool[] {false} ;
         T00046_A1214Organizacao_Nome = new String[] {""} ;
         T00046_n1214Organizacao_Nome = new bool[] {false} ;
         T000413_A5AreaTrabalho_Codigo = new int[1] ;
         T000413_A6AreaTrabalho_Descricao = new String[] {""} ;
         T000413_A1214Organizacao_Nome = new String[] {""} ;
         T000413_n1214Organizacao_Nome = new bool[] {false} ;
         T000413_A24Estado_Nome = new String[] {""} ;
         T000413_A26Municipio_Nome = new String[] {""} ;
         T000413_A33Contratante_Fax = new String[] {""} ;
         T000413_n33Contratante_Fax = new bool[] {false} ;
         T000413_A32Contratante_Ramal = new String[] {""} ;
         T000413_n32Contratante_Ramal = new bool[] {false} ;
         T000413_A31Contratante_Telefone = new String[] {""} ;
         T000413_A14Contratante_Email = new String[] {""} ;
         T000413_n14Contratante_Email = new bool[] {false} ;
         T000413_A13Contratante_WebSite = new String[] {""} ;
         T000413_n13Contratante_WebSite = new bool[] {false} ;
         T000413_A12Contratante_CNPJ = new String[] {""} ;
         T000413_n12Contratante_CNPJ = new bool[] {false} ;
         T000413_A11Contratante_IE = new String[] {""} ;
         T000413_A10Contratante_NomeFantasia = new String[] {""} ;
         T000413_A9Contratante_RazaoSocial = new String[] {""} ;
         T000413_n9Contratante_RazaoSocial = new bool[] {false} ;
         T000413_A547Contratante_EmailSdaHost = new String[] {""} ;
         T000413_n547Contratante_EmailSdaHost = new bool[] {false} ;
         T000413_A548Contratante_EmailSdaUser = new String[] {""} ;
         T000413_n548Contratante_EmailSdaUser = new bool[] {false} ;
         T000413_A549Contratante_EmailSdaPass = new String[] {""} ;
         T000413_n549Contratante_EmailSdaPass = new bool[] {false} ;
         T000413_A550Contratante_EmailSdaKey = new String[] {""} ;
         T000413_n550Contratante_EmailSdaKey = new bool[] {false} ;
         T000413_A551Contratante_EmailSdaAut = new bool[] {false} ;
         T000413_n551Contratante_EmailSdaAut = new bool[] {false} ;
         T000413_A552Contratante_EmailSdaPort = new short[1] ;
         T000413_n552Contratante_EmailSdaPort = new bool[] {false} ;
         T000413_A1048Contratante_EmailSdaSec = new short[1] ;
         T000413_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         T000413_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         T000413_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         T000413_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         T000413_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         T000413_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         T000413_A987AreaTrabalho_ContratadaUpdBslCod = new int[1] ;
         T000413_n987AreaTrabalho_ContratadaUpdBslCod = new bool[] {false} ;
         T000413_A1154AreaTrabalho_TipoPlanilha = new short[1] ;
         T000413_n1154AreaTrabalho_TipoPlanilha = new bool[] {false} ;
         T000413_A72AreaTrabalho_Ativo = new bool[] {false} ;
         T000413_A1588AreaTrabalho_SS_Codigo = new int[1] ;
         T000413_n1588AreaTrabalho_SS_Codigo = new bool[] {false} ;
         T000413_A2081AreaTrabalho_VerTA = new short[1] ;
         T000413_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         T000413_A2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         T000413_n2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         T000413_A29Contratante_Codigo = new int[1] ;
         T000413_n29Contratante_Codigo = new bool[] {false} ;
         T000413_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         T000413_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         T000413_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         T000413_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         T000413_A25Municipio_Codigo = new int[1] ;
         T000413_n25Municipio_Codigo = new bool[] {false} ;
         T000413_A23Estado_UF = new String[] {""} ;
         T000413_A335Contratante_PessoaCod = new int[1] ;
         T000413_A272AreaTrabalho_ContagensQtdGeral = new short[1] ;
         T000413_n272AreaTrabalho_ContagensQtdGeral = new bool[] {false} ;
         T00045_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         T00045_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         T000415_A272AreaTrabalho_ContagensQtdGeral = new short[1] ;
         T000415_n272AreaTrabalho_ContagensQtdGeral = new bool[] {false} ;
         T000416_A33Contratante_Fax = new String[] {""} ;
         T000416_n33Contratante_Fax = new bool[] {false} ;
         T000416_A32Contratante_Ramal = new String[] {""} ;
         T000416_n32Contratante_Ramal = new bool[] {false} ;
         T000416_A31Contratante_Telefone = new String[] {""} ;
         T000416_A14Contratante_Email = new String[] {""} ;
         T000416_n14Contratante_Email = new bool[] {false} ;
         T000416_A13Contratante_WebSite = new String[] {""} ;
         T000416_n13Contratante_WebSite = new bool[] {false} ;
         T000416_A11Contratante_IE = new String[] {""} ;
         T000416_A10Contratante_NomeFantasia = new String[] {""} ;
         T000416_A547Contratante_EmailSdaHost = new String[] {""} ;
         T000416_n547Contratante_EmailSdaHost = new bool[] {false} ;
         T000416_A548Contratante_EmailSdaUser = new String[] {""} ;
         T000416_n548Contratante_EmailSdaUser = new bool[] {false} ;
         T000416_A549Contratante_EmailSdaPass = new String[] {""} ;
         T000416_n549Contratante_EmailSdaPass = new bool[] {false} ;
         T000416_A550Contratante_EmailSdaKey = new String[] {""} ;
         T000416_n550Contratante_EmailSdaKey = new bool[] {false} ;
         T000416_A551Contratante_EmailSdaAut = new bool[] {false} ;
         T000416_n551Contratante_EmailSdaAut = new bool[] {false} ;
         T000416_A552Contratante_EmailSdaPort = new short[1] ;
         T000416_n552Contratante_EmailSdaPort = new bool[] {false} ;
         T000416_A1048Contratante_EmailSdaSec = new short[1] ;
         T000416_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         T000416_A25Municipio_Codigo = new int[1] ;
         T000416_n25Municipio_Codigo = new bool[] {false} ;
         T000416_A335Contratante_PessoaCod = new int[1] ;
         T000417_A26Municipio_Nome = new String[] {""} ;
         T000417_A23Estado_UF = new String[] {""} ;
         T000418_A24Estado_Nome = new String[] {""} ;
         T000419_A12Contratante_CNPJ = new String[] {""} ;
         T000419_n12Contratante_CNPJ = new bool[] {false} ;
         T000419_A9Contratante_RazaoSocial = new String[] {""} ;
         T000419_n9Contratante_RazaoSocial = new bool[] {false} ;
         T000420_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         T000420_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         T000421_A1214Organizacao_Nome = new String[] {""} ;
         T000421_n1214Organizacao_Nome = new bool[] {false} ;
         T000422_A5AreaTrabalho_Codigo = new int[1] ;
         T00043_A5AreaTrabalho_Codigo = new int[1] ;
         T00043_A6AreaTrabalho_Descricao = new String[] {""} ;
         T00043_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         T00043_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         T00043_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         T00043_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         T00043_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         T00043_A987AreaTrabalho_ContratadaUpdBslCod = new int[1] ;
         T00043_n987AreaTrabalho_ContratadaUpdBslCod = new bool[] {false} ;
         T00043_A1154AreaTrabalho_TipoPlanilha = new short[1] ;
         T00043_n1154AreaTrabalho_TipoPlanilha = new bool[] {false} ;
         T00043_A72AreaTrabalho_Ativo = new bool[] {false} ;
         T00043_A1588AreaTrabalho_SS_Codigo = new int[1] ;
         T00043_n1588AreaTrabalho_SS_Codigo = new bool[] {false} ;
         T00043_A2081AreaTrabalho_VerTA = new short[1] ;
         T00043_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         T00043_A2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         T00043_n2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         T00043_A29Contratante_Codigo = new int[1] ;
         T00043_n29Contratante_Codigo = new bool[] {false} ;
         T00043_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         T00043_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         T00043_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         T00043_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         T000423_A5AreaTrabalho_Codigo = new int[1] ;
         T000424_A5AreaTrabalho_Codigo = new int[1] ;
         T00042_A5AreaTrabalho_Codigo = new int[1] ;
         T00042_A6AreaTrabalho_Descricao = new String[] {""} ;
         T00042_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         T00042_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         T00042_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         T00042_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         T00042_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         T00042_A987AreaTrabalho_ContratadaUpdBslCod = new int[1] ;
         T00042_n987AreaTrabalho_ContratadaUpdBslCod = new bool[] {false} ;
         T00042_A1154AreaTrabalho_TipoPlanilha = new short[1] ;
         T00042_n1154AreaTrabalho_TipoPlanilha = new bool[] {false} ;
         T00042_A72AreaTrabalho_Ativo = new bool[] {false} ;
         T00042_A1588AreaTrabalho_SS_Codigo = new int[1] ;
         T00042_n1588AreaTrabalho_SS_Codigo = new bool[] {false} ;
         T00042_A2081AreaTrabalho_VerTA = new short[1] ;
         T00042_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         T00042_A2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         T00042_n2080AreaTrabalho_SelUsrPrestadora = new bool[] {false} ;
         T00042_A29Contratante_Codigo = new int[1] ;
         T00042_n29Contratante_Codigo = new bool[] {false} ;
         T00042_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         T00042_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         T00042_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         T00042_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         T000425_A5AreaTrabalho_Codigo = new int[1] ;
         T000429_A272AreaTrabalho_ContagensQtdGeral = new short[1] ;
         T000429_n272AreaTrabalho_ContagensQtdGeral = new bool[] {false} ;
         T000430_A33Contratante_Fax = new String[] {""} ;
         T000430_n33Contratante_Fax = new bool[] {false} ;
         T000430_A32Contratante_Ramal = new String[] {""} ;
         T000430_n32Contratante_Ramal = new bool[] {false} ;
         T000430_A31Contratante_Telefone = new String[] {""} ;
         T000430_A14Contratante_Email = new String[] {""} ;
         T000430_n14Contratante_Email = new bool[] {false} ;
         T000430_A13Contratante_WebSite = new String[] {""} ;
         T000430_n13Contratante_WebSite = new bool[] {false} ;
         T000430_A11Contratante_IE = new String[] {""} ;
         T000430_A10Contratante_NomeFantasia = new String[] {""} ;
         T000430_A547Contratante_EmailSdaHost = new String[] {""} ;
         T000430_n547Contratante_EmailSdaHost = new bool[] {false} ;
         T000430_A548Contratante_EmailSdaUser = new String[] {""} ;
         T000430_n548Contratante_EmailSdaUser = new bool[] {false} ;
         T000430_A549Contratante_EmailSdaPass = new String[] {""} ;
         T000430_n549Contratante_EmailSdaPass = new bool[] {false} ;
         T000430_A550Contratante_EmailSdaKey = new String[] {""} ;
         T000430_n550Contratante_EmailSdaKey = new bool[] {false} ;
         T000430_A551Contratante_EmailSdaAut = new bool[] {false} ;
         T000430_n551Contratante_EmailSdaAut = new bool[] {false} ;
         T000430_A552Contratante_EmailSdaPort = new short[1] ;
         T000430_n552Contratante_EmailSdaPort = new bool[] {false} ;
         T000430_A1048Contratante_EmailSdaSec = new short[1] ;
         T000430_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         T000430_A25Municipio_Codigo = new int[1] ;
         T000430_n25Municipio_Codigo = new bool[] {false} ;
         T000430_A335Contratante_PessoaCod = new int[1] ;
         T000431_A26Municipio_Nome = new String[] {""} ;
         T000431_A23Estado_UF = new String[] {""} ;
         T000432_A24Estado_Nome = new String[] {""} ;
         T000433_A12Contratante_CNPJ = new String[] {""} ;
         T000433_n12Contratante_CNPJ = new bool[] {false} ;
         T000433_A9Contratante_RazaoSocial = new String[] {""} ;
         T000433_n9Contratante_RazaoSocial = new bool[] {false} ;
         T000434_A1214Organizacao_Nome = new String[] {""} ;
         T000434_n1214Organizacao_Nome = new bool[] {false} ;
         T000435_A619GrupoFuncao_Codigo = new int[1] ;
         T000436_A2175Equipe_Codigo = new int[1] ;
         T000437_A648Projeto_Codigo = new int[1] ;
         T000438_A2077UsuarioNotifica_Codigo = new int[1] ;
         T000439_A2058Indicador_Codigo = new int[1] ;
         T000440_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T000441_A1795CatalogoServico_Codigo = new int[1] ;
         T000442_A1482Gestao_Codigo = new int[1] ;
         T000443_A1347Glosario_Codigo = new int[1] ;
         T000444_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         T000444_A962VariavelCocomo_Sigla = new String[] {""} ;
         T000444_A964VariavelCocomo_Tipo = new String[] {""} ;
         T000444_A992VariavelCocomo_Sequencial = new short[1] ;
         T000445_A860RegrasContagem_Regra = new String[] {""} ;
         T000446_A848ParametrosPln_Codigo = new int[1] ;
         T000447_A351AmbienteTecnologico_Codigo = new int[1] ;
         T000448_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         T000448_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         T000449_A709ReferenciaINM_Codigo = new int[1] ;
         T000450_A596Lote_Codigo = new int[1] ;
         T000451_A426NaoConformidade_Codigo = new int[1] ;
         T000452_A423Status_Codigo = new int[1] ;
         T000453_A93Guia_Codigo = new int[1] ;
         T000454_A192Contagem_Codigo = new int[1] ;
         T000455_A127Sistema_Codigo = new int[1] ;
         T000456_A74Contrato_Codigo = new int[1] ;
         T000457_A39Contratada_Codigo = new int[1] ;
         T000458_A3Perfil_Codigo = new int[1] ;
         T000459_A1685Proposta_Codigo = new int[1] ;
         T000460_A5AreaTrabalho_Codigo = new int[1] ;
         T000460_A2127FatoresImpacto_Codigo = new int[1] ;
         T000461_A5AreaTrabalho_Codigo = new int[1] ;
         T000461_A93Guia_Codigo = new int[1] ;
         T000462_A5AreaTrabalho_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i642AreaTrabalho_CalculoPFinal = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T000463_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         T000463_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         T000463_A605Servico_Sigla = new String[] {""} ;
         T000463_n605Servico_Sigla = new bool[] {false} ;
         T000464_A40Contratada_PessoaCod = new int[1] ;
         T000464_A39Contratada_Codigo = new int[1] ;
         T000464_A41Contratada_PessoaNom = new String[] {""} ;
         T000464_n41Contratada_PessoaNom = new bool[] {false} ;
         T000464_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T000465_A23Estado_UF = new String[] {""} ;
         T000465_A24Estado_Nome = new String[] {""} ;
         T000466_A25Municipio_Codigo = new int[1] ;
         T000466_n25Municipio_Codigo = new bool[] {false} ;
         T000466_A26Municipio_Nome = new String[] {""} ;
         T000466_A23Estado_UF = new String[] {""} ;
         T000468_A272AreaTrabalho_ContagensQtdGeral = new short[1] ;
         T000468_n272AreaTrabalho_ContagensQtdGeral = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T000469_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         T000469_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         T000470_A33Contratante_Fax = new String[] {""} ;
         T000470_n33Contratante_Fax = new bool[] {false} ;
         T000470_A32Contratante_Ramal = new String[] {""} ;
         T000470_n32Contratante_Ramal = new bool[] {false} ;
         T000470_A31Contratante_Telefone = new String[] {""} ;
         T000470_A14Contratante_Email = new String[] {""} ;
         T000470_n14Contratante_Email = new bool[] {false} ;
         T000470_A13Contratante_WebSite = new String[] {""} ;
         T000470_n13Contratante_WebSite = new bool[] {false} ;
         T000470_A11Contratante_IE = new String[] {""} ;
         T000470_A10Contratante_NomeFantasia = new String[] {""} ;
         T000470_A547Contratante_EmailSdaHost = new String[] {""} ;
         T000470_n547Contratante_EmailSdaHost = new bool[] {false} ;
         T000470_A548Contratante_EmailSdaUser = new String[] {""} ;
         T000470_n548Contratante_EmailSdaUser = new bool[] {false} ;
         T000470_A549Contratante_EmailSdaPass = new String[] {""} ;
         T000470_n549Contratante_EmailSdaPass = new bool[] {false} ;
         T000470_A550Contratante_EmailSdaKey = new String[] {""} ;
         T000470_n550Contratante_EmailSdaKey = new bool[] {false} ;
         T000470_A551Contratante_EmailSdaAut = new bool[] {false} ;
         T000470_n551Contratante_EmailSdaAut = new bool[] {false} ;
         T000470_A552Contratante_EmailSdaPort = new short[1] ;
         T000470_n552Contratante_EmailSdaPort = new bool[] {false} ;
         T000470_A1048Contratante_EmailSdaSec = new short[1] ;
         T000470_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         T000470_A25Municipio_Codigo = new int[1] ;
         T000470_n25Municipio_Codigo = new bool[] {false} ;
         T000470_A335Contratante_PessoaCod = new int[1] ;
         T000471_A26Municipio_Nome = new String[] {""} ;
         T000471_A23Estado_UF = new String[] {""} ;
         T000472_A24Estado_Nome = new String[] {""} ;
         T000473_A12Contratante_CNPJ = new String[] {""} ;
         T000473_n12Contratante_CNPJ = new bool[] {false} ;
         T000473_A9Contratante_RazaoSocial = new String[] {""} ;
         T000473_n9Contratante_RazaoSocial = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.areatrabalho__default(),
            new Object[][] {
                new Object[] {
               T00042_A5AreaTrabalho_Codigo, T00042_A6AreaTrabalho_Descricao, T00042_A642AreaTrabalho_CalculoPFinal, T00042_A834AreaTrabalho_ValidaOSFM, T00042_n834AreaTrabalho_ValidaOSFM, T00042_A855AreaTrabalho_DiasParaPagar, T00042_n855AreaTrabalho_DiasParaPagar, T00042_A987AreaTrabalho_ContratadaUpdBslCod, T00042_n987AreaTrabalho_ContratadaUpdBslCod, T00042_A1154AreaTrabalho_TipoPlanilha,
               T00042_n1154AreaTrabalho_TipoPlanilha, T00042_A72AreaTrabalho_Ativo, T00042_A1588AreaTrabalho_SS_Codigo, T00042_n1588AreaTrabalho_SS_Codigo, T00042_A2081AreaTrabalho_VerTA, T00042_n2081AreaTrabalho_VerTA, T00042_A2080AreaTrabalho_SelUsrPrestadora, T00042_n2080AreaTrabalho_SelUsrPrestadora, T00042_A29Contratante_Codigo, T00042_n29Contratante_Codigo,
               T00042_A830AreaTrabalho_ServicoPadrao, T00042_n830AreaTrabalho_ServicoPadrao, T00042_A1216AreaTrabalho_OrganizacaoCod, T00042_n1216AreaTrabalho_OrganizacaoCod
               }
               , new Object[] {
               T00043_A5AreaTrabalho_Codigo, T00043_A6AreaTrabalho_Descricao, T00043_A642AreaTrabalho_CalculoPFinal, T00043_A834AreaTrabalho_ValidaOSFM, T00043_n834AreaTrabalho_ValidaOSFM, T00043_A855AreaTrabalho_DiasParaPagar, T00043_n855AreaTrabalho_DiasParaPagar, T00043_A987AreaTrabalho_ContratadaUpdBslCod, T00043_n987AreaTrabalho_ContratadaUpdBslCod, T00043_A1154AreaTrabalho_TipoPlanilha,
               T00043_n1154AreaTrabalho_TipoPlanilha, T00043_A72AreaTrabalho_Ativo, T00043_A1588AreaTrabalho_SS_Codigo, T00043_n1588AreaTrabalho_SS_Codigo, T00043_A2081AreaTrabalho_VerTA, T00043_n2081AreaTrabalho_VerTA, T00043_A2080AreaTrabalho_SelUsrPrestadora, T00043_n2080AreaTrabalho_SelUsrPrestadora, T00043_A29Contratante_Codigo, T00043_n29Contratante_Codigo,
               T00043_A830AreaTrabalho_ServicoPadrao, T00043_n830AreaTrabalho_ServicoPadrao, T00043_A1216AreaTrabalho_OrganizacaoCod, T00043_n1216AreaTrabalho_OrganizacaoCod
               }
               , new Object[] {
               T00044_A33Contratante_Fax, T00044_n33Contratante_Fax, T00044_A32Contratante_Ramal, T00044_n32Contratante_Ramal, T00044_A31Contratante_Telefone, T00044_A14Contratante_Email, T00044_n14Contratante_Email, T00044_A13Contratante_WebSite, T00044_n13Contratante_WebSite, T00044_A11Contratante_IE,
               T00044_A10Contratante_NomeFantasia, T00044_A547Contratante_EmailSdaHost, T00044_n547Contratante_EmailSdaHost, T00044_A548Contratante_EmailSdaUser, T00044_n548Contratante_EmailSdaUser, T00044_A549Contratante_EmailSdaPass, T00044_n549Contratante_EmailSdaPass, T00044_A550Contratante_EmailSdaKey, T00044_n550Contratante_EmailSdaKey, T00044_A551Contratante_EmailSdaAut,
               T00044_n551Contratante_EmailSdaAut, T00044_A552Contratante_EmailSdaPort, T00044_n552Contratante_EmailSdaPort, T00044_A1048Contratante_EmailSdaSec, T00044_n1048Contratante_EmailSdaSec, T00044_A25Municipio_Codigo, T00044_n25Municipio_Codigo, T00044_A335Contratante_PessoaCod
               }
               , new Object[] {
               T00045_A830AreaTrabalho_ServicoPadrao
               }
               , new Object[] {
               T00046_A1214Organizacao_Nome, T00046_n1214Organizacao_Nome
               }
               , new Object[] {
               T00047_A26Municipio_Nome, T00047_A23Estado_UF
               }
               , new Object[] {
               T00048_A24Estado_Nome
               }
               , new Object[] {
               T00049_A12Contratante_CNPJ, T00049_n12Contratante_CNPJ, T00049_A9Contratante_RazaoSocial, T00049_n9Contratante_RazaoSocial
               }
               , new Object[] {
               T000411_A272AreaTrabalho_ContagensQtdGeral, T000411_n272AreaTrabalho_ContagensQtdGeral
               }
               , new Object[] {
               T000413_A5AreaTrabalho_Codigo, T000413_A6AreaTrabalho_Descricao, T000413_A1214Organizacao_Nome, T000413_n1214Organizacao_Nome, T000413_A24Estado_Nome, T000413_A26Municipio_Nome, T000413_A33Contratante_Fax, T000413_n33Contratante_Fax, T000413_A32Contratante_Ramal, T000413_n32Contratante_Ramal,
               T000413_A31Contratante_Telefone, T000413_A14Contratante_Email, T000413_n14Contratante_Email, T000413_A13Contratante_WebSite, T000413_n13Contratante_WebSite, T000413_A12Contratante_CNPJ, T000413_n12Contratante_CNPJ, T000413_A11Contratante_IE, T000413_A10Contratante_NomeFantasia, T000413_A9Contratante_RazaoSocial,
               T000413_n9Contratante_RazaoSocial, T000413_A547Contratante_EmailSdaHost, T000413_n547Contratante_EmailSdaHost, T000413_A548Contratante_EmailSdaUser, T000413_n548Contratante_EmailSdaUser, T000413_A549Contratante_EmailSdaPass, T000413_n549Contratante_EmailSdaPass, T000413_A550Contratante_EmailSdaKey, T000413_n550Contratante_EmailSdaKey, T000413_A551Contratante_EmailSdaAut,
               T000413_n551Contratante_EmailSdaAut, T000413_A552Contratante_EmailSdaPort, T000413_n552Contratante_EmailSdaPort, T000413_A1048Contratante_EmailSdaSec, T000413_n1048Contratante_EmailSdaSec, T000413_A642AreaTrabalho_CalculoPFinal, T000413_A834AreaTrabalho_ValidaOSFM, T000413_n834AreaTrabalho_ValidaOSFM, T000413_A855AreaTrabalho_DiasParaPagar, T000413_n855AreaTrabalho_DiasParaPagar,
               T000413_A987AreaTrabalho_ContratadaUpdBslCod, T000413_n987AreaTrabalho_ContratadaUpdBslCod, T000413_A1154AreaTrabalho_TipoPlanilha, T000413_n1154AreaTrabalho_TipoPlanilha, T000413_A72AreaTrabalho_Ativo, T000413_A1588AreaTrabalho_SS_Codigo, T000413_n1588AreaTrabalho_SS_Codigo, T000413_A2081AreaTrabalho_VerTA, T000413_n2081AreaTrabalho_VerTA, T000413_A2080AreaTrabalho_SelUsrPrestadora,
               T000413_n2080AreaTrabalho_SelUsrPrestadora, T000413_A29Contratante_Codigo, T000413_n29Contratante_Codigo, T000413_A830AreaTrabalho_ServicoPadrao, T000413_n830AreaTrabalho_ServicoPadrao, T000413_A1216AreaTrabalho_OrganizacaoCod, T000413_n1216AreaTrabalho_OrganizacaoCod, T000413_A25Municipio_Codigo, T000413_n25Municipio_Codigo, T000413_A23Estado_UF,
               T000413_A335Contratante_PessoaCod, T000413_A272AreaTrabalho_ContagensQtdGeral, T000413_n272AreaTrabalho_ContagensQtdGeral
               }
               , new Object[] {
               T000415_A272AreaTrabalho_ContagensQtdGeral, T000415_n272AreaTrabalho_ContagensQtdGeral
               }
               , new Object[] {
               T000416_A33Contratante_Fax, T000416_n33Contratante_Fax, T000416_A32Contratante_Ramal, T000416_n32Contratante_Ramal, T000416_A31Contratante_Telefone, T000416_A14Contratante_Email, T000416_n14Contratante_Email, T000416_A13Contratante_WebSite, T000416_n13Contratante_WebSite, T000416_A11Contratante_IE,
               T000416_A10Contratante_NomeFantasia, T000416_A547Contratante_EmailSdaHost, T000416_n547Contratante_EmailSdaHost, T000416_A548Contratante_EmailSdaUser, T000416_n548Contratante_EmailSdaUser, T000416_A549Contratante_EmailSdaPass, T000416_n549Contratante_EmailSdaPass, T000416_A550Contratante_EmailSdaKey, T000416_n550Contratante_EmailSdaKey, T000416_A551Contratante_EmailSdaAut,
               T000416_n551Contratante_EmailSdaAut, T000416_A552Contratante_EmailSdaPort, T000416_n552Contratante_EmailSdaPort, T000416_A1048Contratante_EmailSdaSec, T000416_n1048Contratante_EmailSdaSec, T000416_A25Municipio_Codigo, T000416_n25Municipio_Codigo, T000416_A335Contratante_PessoaCod
               }
               , new Object[] {
               T000417_A26Municipio_Nome, T000417_A23Estado_UF
               }
               , new Object[] {
               T000418_A24Estado_Nome
               }
               , new Object[] {
               T000419_A12Contratante_CNPJ, T000419_n12Contratante_CNPJ, T000419_A9Contratante_RazaoSocial, T000419_n9Contratante_RazaoSocial
               }
               , new Object[] {
               T000420_A830AreaTrabalho_ServicoPadrao
               }
               , new Object[] {
               T000421_A1214Organizacao_Nome, T000421_n1214Organizacao_Nome
               }
               , new Object[] {
               T000422_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T000423_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T000424_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T000425_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000429_A272AreaTrabalho_ContagensQtdGeral, T000429_n272AreaTrabalho_ContagensQtdGeral
               }
               , new Object[] {
               T000430_A33Contratante_Fax, T000430_n33Contratante_Fax, T000430_A32Contratante_Ramal, T000430_n32Contratante_Ramal, T000430_A31Contratante_Telefone, T000430_A14Contratante_Email, T000430_n14Contratante_Email, T000430_A13Contratante_WebSite, T000430_n13Contratante_WebSite, T000430_A11Contratante_IE,
               T000430_A10Contratante_NomeFantasia, T000430_A547Contratante_EmailSdaHost, T000430_n547Contratante_EmailSdaHost, T000430_A548Contratante_EmailSdaUser, T000430_n548Contratante_EmailSdaUser, T000430_A549Contratante_EmailSdaPass, T000430_n549Contratante_EmailSdaPass, T000430_A550Contratante_EmailSdaKey, T000430_n550Contratante_EmailSdaKey, T000430_A551Contratante_EmailSdaAut,
               T000430_n551Contratante_EmailSdaAut, T000430_A552Contratante_EmailSdaPort, T000430_n552Contratante_EmailSdaPort, T000430_A1048Contratante_EmailSdaSec, T000430_n1048Contratante_EmailSdaSec, T000430_A25Municipio_Codigo, T000430_n25Municipio_Codigo, T000430_A335Contratante_PessoaCod
               }
               , new Object[] {
               T000431_A26Municipio_Nome, T000431_A23Estado_UF
               }
               , new Object[] {
               T000432_A24Estado_Nome
               }
               , new Object[] {
               T000433_A12Contratante_CNPJ, T000433_n12Contratante_CNPJ, T000433_A9Contratante_RazaoSocial, T000433_n9Contratante_RazaoSocial
               }
               , new Object[] {
               T000434_A1214Organizacao_Nome, T000434_n1214Organizacao_Nome
               }
               , new Object[] {
               T000435_A619GrupoFuncao_Codigo
               }
               , new Object[] {
               T000436_A2175Equipe_Codigo
               }
               , new Object[] {
               T000437_A648Projeto_Codigo
               }
               , new Object[] {
               T000438_A2077UsuarioNotifica_Codigo
               }
               , new Object[] {
               T000439_A2058Indicador_Codigo
               }
               , new Object[] {
               T000440_A1412ContagemResultadoNotificacao_Codigo
               }
               , new Object[] {
               T000441_A1795CatalogoServico_Codigo
               }
               , new Object[] {
               T000442_A1482Gestao_Codigo
               }
               , new Object[] {
               T000443_A1347Glosario_Codigo
               }
               , new Object[] {
               T000444_A961VariavelCocomo_AreaTrabalhoCod, T000444_A962VariavelCocomo_Sigla, T000444_A964VariavelCocomo_Tipo, T000444_A992VariavelCocomo_Sequencial
               }
               , new Object[] {
               T000445_A860RegrasContagem_Regra
               }
               , new Object[] {
               T000446_A848ParametrosPln_Codigo
               }
               , new Object[] {
               T000447_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               T000448_A718ItemNaoMensuravel_AreaTrabalhoCod, T000448_A715ItemNaoMensuravel_Codigo
               }
               , new Object[] {
               T000449_A709ReferenciaINM_Codigo
               }
               , new Object[] {
               T000450_A596Lote_Codigo
               }
               , new Object[] {
               T000451_A426NaoConformidade_Codigo
               }
               , new Object[] {
               T000452_A423Status_Codigo
               }
               , new Object[] {
               T000453_A93Guia_Codigo
               }
               , new Object[] {
               T000454_A192Contagem_Codigo
               }
               , new Object[] {
               T000455_A127Sistema_Codigo
               }
               , new Object[] {
               T000456_A74Contrato_Codigo
               }
               , new Object[] {
               T000457_A39Contratada_Codigo
               }
               , new Object[] {
               T000458_A3Perfil_Codigo
               }
               , new Object[] {
               T000459_A1685Proposta_Codigo
               }
               , new Object[] {
               T000460_A5AreaTrabalho_Codigo, T000460_A2127FatoresImpacto_Codigo
               }
               , new Object[] {
               T000461_A5AreaTrabalho_Codigo, T000461_A93Guia_Codigo
               }
               , new Object[] {
               T000462_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T000463_A830AreaTrabalho_ServicoPadrao, T000463_A605Servico_Sigla, T000463_n605Servico_Sigla
               }
               , new Object[] {
               T000464_A40Contratada_PessoaCod, T000464_A39Contratada_Codigo, T000464_A41Contratada_PessoaNom, T000464_n41Contratada_PessoaNom, T000464_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T000465_A23Estado_UF, T000465_A24Estado_Nome
               }
               , new Object[] {
               T000466_A25Municipio_Codigo, T000466_A26Municipio_Nome, T000466_A23Estado_UF
               }
               , new Object[] {
               T000468_A272AreaTrabalho_ContagensQtdGeral, T000468_n272AreaTrabalho_ContagensQtdGeral
               }
               , new Object[] {
               T000469_A830AreaTrabalho_ServicoPadrao
               }
               , new Object[] {
               T000470_A33Contratante_Fax, T000470_n33Contratante_Fax, T000470_A32Contratante_Ramal, T000470_n32Contratante_Ramal, T000470_A31Contratante_Telefone, T000470_A14Contratante_Email, T000470_n14Contratante_Email, T000470_A13Contratante_WebSite, T000470_n13Contratante_WebSite, T000470_A11Contratante_IE,
               T000470_A10Contratante_NomeFantasia, T000470_A547Contratante_EmailSdaHost, T000470_n547Contratante_EmailSdaHost, T000470_A548Contratante_EmailSdaUser, T000470_n548Contratante_EmailSdaUser, T000470_A549Contratante_EmailSdaPass, T000470_n549Contratante_EmailSdaPass, T000470_A550Contratante_EmailSdaKey, T000470_n550Contratante_EmailSdaKey, T000470_A551Contratante_EmailSdaAut,
               T000470_n551Contratante_EmailSdaAut, T000470_A552Contratante_EmailSdaPort, T000470_n552Contratante_EmailSdaPort, T000470_A1048Contratante_EmailSdaSec, T000470_n1048Contratante_EmailSdaSec, T000470_A25Municipio_Codigo, T000470_n25Municipio_Codigo, T000470_A335Contratante_PessoaCod
               }
               , new Object[] {
               T000471_A26Municipio_Nome, T000471_A23Estado_UF
               }
               , new Object[] {
               T000472_A24Estado_Nome
               }
               , new Object[] {
               T000473_A12Contratante_CNPJ, T000473_n12Contratante_CNPJ, T000473_A9Contratante_RazaoSocial, T000473_n9Contratante_RazaoSocial
               }
            }
         );
         Z72AreaTrabalho_Ativo = true;
         O72AreaTrabalho_Ativo = true;
         A72AreaTrabalho_Ativo = true;
         i72AreaTrabalho_Ativo = true;
         Z642AreaTrabalho_CalculoPFinal = "MB";
         A642AreaTrabalho_CalculoPFinal = "MB";
         i642AreaTrabalho_CalculoPFinal = "MB";
         AV40Pgmname = "AreaTrabalho";
      }

      private short Z855AreaTrabalho_DiasParaPagar ;
      private short Z1154AreaTrabalho_TipoPlanilha ;
      private short Z2081AreaTrabalho_VerTA ;
      private short GxWebError ;
      private short AV36Contratante_EmailSdaPort ;
      private short AV34Contratante_EmailSdaSec ;
      private short gxcookieaux ;
      private short A2081AreaTrabalho_VerTA ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A855AreaTrabalho_DiasParaPagar ;
      private short A1154AreaTrabalho_TipoPlanilha ;
      private short A552Contratante_EmailSdaPort ;
      private short A1048Contratante_EmailSdaSec ;
      private short Gx_BScreen ;
      private short A272AreaTrabalho_ContagensQtdGeral ;
      private short RcdFound106 ;
      private short GX_JID ;
      private short Z272AreaTrabalho_ContagensQtdGeral ;
      private short Z552Contratante_EmailSdaPort ;
      private short Z1048Contratante_EmailSdaSec ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7AreaTrabalho_Codigo ;
      private int Z5AreaTrabalho_Codigo ;
      private int Z987AreaTrabalho_ContratadaUpdBslCod ;
      private int Z1588AreaTrabalho_SS_Codigo ;
      private int Z29Contratante_Codigo ;
      private int Z830AreaTrabalho_ServicoPadrao ;
      private int Z1216AreaTrabalho_OrganizacaoCod ;
      private int N1216AreaTrabalho_OrganizacaoCod ;
      private int N29Contratante_Codigo ;
      private int N830AreaTrabalho_ServicoPadrao ;
      private int AV11Insert_Contratante_Codigo ;
      private int AV26Contratante_Codigo ;
      private int AV25Municipio_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int A25Municipio_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int A1216AreaTrabalho_OrganizacaoCod ;
      private int AV7AreaTrabalho_Codigo ;
      private int trnEnded ;
      private int A987AreaTrabalho_ContratadaUpdBslCod ;
      private int edtContratante_Codigo_Visible ;
      private int edtContratante_Codigo_Enabled ;
      private int edtAreaTrabalho_Codigo_Enabled ;
      private int edtAreaTrabalho_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtavContratante_cnpj_Enabled ;
      private int edtavContratante_ie_Enabled ;
      private int edtavContratante_razaosocial_Enabled ;
      private int edtavContratante_nomefantasia_Enabled ;
      private int edtavContratante_website_Enabled ;
      private int edtavContratante_email_Enabled ;
      private int edtavContratante_telefone_Enabled ;
      private int edtavContratante_ramal_Enabled ;
      private int edtavContratante_fax_Enabled ;
      private int edtavContratante_emailsdahost_Enabled ;
      private int edtavContratante_emailsdauser_Enabled ;
      private int edtavContratante_emailsdapass_Enabled ;
      private int edtavContratante_emailsdaport_Enabled ;
      private int edtOrganizacao_Nome_Enabled ;
      private int edtAreaTrabalho_Descricao_Enabled ;
      private int edtAreaTrabalho_DiasParaPagar_Enabled ;
      private int lblTextblockareatrabalho_ativo_Visible ;
      private int A1588AreaTrabalho_SS_Codigo ;
      private int AV29Insert_AreaTrabalho_OrganizacaoCod ;
      private int AV27Insert_AreaTrabalho_ServicoPadrao ;
      private int AV41GXV1 ;
      private int Z25Municipio_Codigo ;
      private int Z335Contratante_PessoaCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private int GXt_int1 ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z642AreaTrabalho_CalculoPFinal ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String AV24Estado_UF ;
      private String Gx_mode ;
      private String AV16Contratante_RazaoSocial ;
      private String AV17Contratante_NomeFantasia ;
      private String AV18Contratante_IE ;
      private String AV21Contratante_Telefone ;
      private String AV22Contratante_Ramal ;
      private String AV23Contratante_Fax ;
      private String A23Estado_UF ;
      private String GXKey ;
      private String A642AreaTrabalho_CalculoPFinal ;
      private String chkAreaTrabalho_Ativo_Internalname ;
      private String chkavPodedesativar_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtAreaTrabalho_Descricao_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String edtContratante_Codigo_Internalname ;
      private String edtContratante_Codigo_Jsonclick ;
      private String edtAreaTrabalho_Codigo_Internalname ;
      private String edtAreaTrabalho_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblAreatrabalhotitle_Internalname ;
      private String lblAreatrabalhotitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblAreadetrabalho_Internalname ;
      private String tblContratante_Internalname ;
      private String lblTextblockcontratante_cnpj_Internalname ;
      private String lblTextblockcontratante_cnpj_Jsonclick ;
      private String edtavContratante_cnpj_Internalname ;
      private String edtavContratante_cnpj_Jsonclick ;
      private String lblTextblockcontratante_ie_Internalname ;
      private String lblTextblockcontratante_ie_Jsonclick ;
      private String edtavContratante_ie_Internalname ;
      private String edtavContratante_ie_Jsonclick ;
      private String lblTextblockcontratante_razaosocial_Internalname ;
      private String lblTextblockcontratante_razaosocial_Jsonclick ;
      private String edtavContratante_razaosocial_Internalname ;
      private String edtavContratante_razaosocial_Jsonclick ;
      private String lblTextblockcontratante_nomefantasia_Internalname ;
      private String lblTextblockcontratante_nomefantasia_Jsonclick ;
      private String edtavContratante_nomefantasia_Internalname ;
      private String edtavContratante_nomefantasia_Jsonclick ;
      private String lblTextblockcontratante_website_Internalname ;
      private String lblTextblockcontratante_website_Jsonclick ;
      private String edtavContratante_website_Internalname ;
      private String edtavContratante_website_Jsonclick ;
      private String lblTextblockcontratante_email_Internalname ;
      private String lblTextblockcontratante_email_Jsonclick ;
      private String edtavContratante_email_Internalname ;
      private String edtavContratante_email_Jsonclick ;
      private String lblTextblockcontratante_telefone_Internalname ;
      private String lblTextblockcontratante_telefone_Jsonclick ;
      private String edtavContratante_telefone_Internalname ;
      private String edtavContratante_telefone_Jsonclick ;
      private String lblTextblockcontratante_ramal_Internalname ;
      private String lblTextblockcontratante_ramal_Jsonclick ;
      private String edtavContratante_ramal_Internalname ;
      private String edtavContratante_ramal_Jsonclick ;
      private String lblTextblockcontratante_fax_Internalname ;
      private String lblTextblockcontratante_fax_Jsonclick ;
      private String edtavContratante_fax_Internalname ;
      private String edtavContratante_fax_Jsonclick ;
      private String lblTextblockestado_uf_Internalname ;
      private String lblTextblockestado_uf_Jsonclick ;
      private String dynavEstado_uf_Internalname ;
      private String dynavEstado_uf_Jsonclick ;
      private String lblTextblockmunicipio_codigo_Internalname ;
      private String lblTextblockmunicipio_codigo_Jsonclick ;
      private String dynavMunicipio_codigo_Internalname ;
      private String dynavMunicipio_codigo_Jsonclick ;
      private String lblLblemail_Internalname ;
      private String lblLblemail_Jsonclick ;
      private String lblTextblockcontratante_emailsdahost_Internalname ;
      private String lblTextblockcontratante_emailsdahost_Jsonclick ;
      private String edtavContratante_emailsdahost_Internalname ;
      private String edtavContratante_emailsdahost_Jsonclick ;
      private String lblTextblockcontratante_emailsdauser_Internalname ;
      private String lblTextblockcontratante_emailsdauser_Jsonclick ;
      private String edtavContratante_emailsdauser_Internalname ;
      private String edtavContratante_emailsdauser_Jsonclick ;
      private String lblTextblockcontratante_emailsdapass_Internalname ;
      private String lblTextblockcontratante_emailsdapass_Jsonclick ;
      private String edtavContratante_emailsdapass_Internalname ;
      private String edtavContratante_emailsdapass_Jsonclick ;
      private String lblTextblockcontratante_emailsdaport_Internalname ;
      private String lblTextblockcontratante_emailsdaport_Jsonclick ;
      private String edtavContratante_emailsdaport_Internalname ;
      private String edtavContratante_emailsdaport_Jsonclick ;
      private String lblTextblockcontratante_emailsdaaut_Internalname ;
      private String lblTextblockcontratante_emailsdaaut_Jsonclick ;
      private String cmbavContratante_emailsdaaut_Internalname ;
      private String cmbavContratante_emailsdaaut_Jsonclick ;
      private String lblTextblockcontratante_emailsdasec_Internalname ;
      private String lblTextblockcontratante_emailsdasec_Jsonclick ;
      private String cmbavContratante_emailsdasec_Internalname ;
      private String cmbavContratante_emailsdasec_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockorganizacao_nome_Internalname ;
      private String lblTextblockorganizacao_nome_Jsonclick ;
      private String edtOrganizacao_Nome_Internalname ;
      private String A1214Organizacao_Nome ;
      private String edtOrganizacao_Nome_Jsonclick ;
      private String lblTextblockareatrabalho_descricao_Internalname ;
      private String lblTextblockareatrabalho_descricao_Jsonclick ;
      private String edtAreaTrabalho_Descricao_Jsonclick ;
      private String lblTextblockareatrabalho_calculopfinal_Internalname ;
      private String lblTextblockareatrabalho_calculopfinal_Jsonclick ;
      private String cmbAreaTrabalho_CalculoPFinal_Internalname ;
      private String cmbAreaTrabalho_CalculoPFinal_Jsonclick ;
      private String lblTextblockareatrabalho_servicopadrao_Internalname ;
      private String lblTextblockareatrabalho_servicopadrao_Jsonclick ;
      private String dynAreaTrabalho_ServicoPadrao_Internalname ;
      private String dynAreaTrabalho_ServicoPadrao_Jsonclick ;
      private String lblTextblockareatrabalho_validaosfm_Internalname ;
      private String lblTextblockareatrabalho_validaosfm_Jsonclick ;
      private String cmbAreaTrabalho_ValidaOSFM_Internalname ;
      private String cmbAreaTrabalho_ValidaOSFM_Jsonclick ;
      private String lblTextblockareatrabalho_diasparapagar_Internalname ;
      private String lblTextblockareatrabalho_diasparapagar_Jsonclick ;
      private String edtAreaTrabalho_DiasParaPagar_Internalname ;
      private String edtAreaTrabalho_DiasParaPagar_Jsonclick ;
      private String lblTextblockareatrabalho_contratadaupdbslcod_Internalname ;
      private String lblTextblockareatrabalho_contratadaupdbslcod_Jsonclick ;
      private String dynAreaTrabalho_ContratadaUpdBslCod_Internalname ;
      private String dynAreaTrabalho_ContratadaUpdBslCod_Jsonclick ;
      private String lblTextblockareatrabalho_verta_Internalname ;
      private String lblTextblockareatrabalho_verta_Jsonclick ;
      private String cmbAreaTrabalho_VerTA_Internalname ;
      private String cmbAreaTrabalho_VerTA_Jsonclick ;
      private String lblTextblockareatrabalho_ativo_Internalname ;
      private String lblTextblockareatrabalho_ativo_Jsonclick ;
      private String A33Contratante_Fax ;
      private String A11Contratante_IE ;
      private String A10Contratante_NomeFantasia ;
      private String A32Contratante_Ramal ;
      private String A9Contratante_RazaoSocial ;
      private String A31Contratante_Telefone ;
      private String A550Contratante_EmailSdaKey ;
      private String AV39Causa ;
      private String A26Municipio_Nome ;
      private String A24Estado_Nome ;
      private String AV40Pgmname ;
      private String Dvpanel_contratante_Width ;
      private String Dvpanel_contratante_Height ;
      private String Dvpanel_contratante_Cls ;
      private String Dvpanel_contratante_Title ;
      private String Dvpanel_contratante_Class ;
      private String Dvpanel_contratante_Iconposition ;
      private String Dvpanel_areadetrabalho_Width ;
      private String Dvpanel_areadetrabalho_Height ;
      private String Dvpanel_areadetrabalho_Cls ;
      private String Dvpanel_areadetrabalho_Title ;
      private String Dvpanel_areadetrabalho_Class ;
      private String Dvpanel_areadetrabalho_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode106 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1214Organizacao_Nome ;
      private String Z33Contratante_Fax ;
      private String Z32Contratante_Ramal ;
      private String Z31Contratante_Telefone ;
      private String Z11Contratante_IE ;
      private String Z10Contratante_NomeFantasia ;
      private String Z550Contratante_EmailSdaKey ;
      private String Z26Municipio_Nome ;
      private String Z23Estado_UF ;
      private String Z24Estado_Nome ;
      private String Z9Contratante_RazaoSocial ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i642AreaTrabalho_CalculoPFinal ;
      private String Dvpanel_contratante_Internalname ;
      private String Dvpanel_areadetrabalho_Internalname ;
      private String gxwrpcisep ;
      private bool Z834AreaTrabalho_ValidaOSFM ;
      private bool Z72AreaTrabalho_Ativo ;
      private bool Z2080AreaTrabalho_SelUsrPrestadora ;
      private bool O72AreaTrabalho_Ativo ;
      private bool entryPointCalled ;
      private bool AV37Contratante_EmailSdaAut ;
      private bool n29Contratante_Codigo ;
      private bool n25Municipio_Codigo ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool n1216AreaTrabalho_OrganizacaoCod ;
      private bool toggleJsOutput ;
      private bool A834AreaTrabalho_ValidaOSFM ;
      private bool n834AreaTrabalho_ValidaOSFM ;
      private bool n2081AreaTrabalho_VerTA ;
      private bool wbErr ;
      private bool n987AreaTrabalho_ContratadaUpdBslCod ;
      private bool AV38PodeDesativar ;
      private bool A72AreaTrabalho_Ativo ;
      private bool n1214Organizacao_Nome ;
      private bool n855AreaTrabalho_DiasParaPagar ;
      private bool n1154AreaTrabalho_TipoPlanilha ;
      private bool n1588AreaTrabalho_SS_Codigo ;
      private bool n2080AreaTrabalho_SelUsrPrestadora ;
      private bool A2080AreaTrabalho_SelUsrPrestadora ;
      private bool n12Contratante_CNPJ ;
      private bool n14Contratante_Email ;
      private bool n33Contratante_Fax ;
      private bool n32Contratante_Ramal ;
      private bool n9Contratante_RazaoSocial ;
      private bool n13Contratante_WebSite ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n549Contratante_EmailSdaPass ;
      private bool n550Contratante_EmailSdaKey ;
      private bool n552Contratante_EmailSdaPort ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n1048Contratante_EmailSdaSec ;
      private bool n272AreaTrabalho_ContagensQtdGeral ;
      private bool Dvpanel_contratante_Collapsible ;
      private bool Dvpanel_contratante_Collapsed ;
      private bool Dvpanel_contratante_Enabled ;
      private bool Dvpanel_contratante_Autowidth ;
      private bool Dvpanel_contratante_Autoheight ;
      private bool Dvpanel_contratante_Showheader ;
      private bool Dvpanel_contratante_Showcollapseicon ;
      private bool Dvpanel_contratante_Autoscroll ;
      private bool Dvpanel_contratante_Visible ;
      private bool Dvpanel_areadetrabalho_Collapsible ;
      private bool Dvpanel_areadetrabalho_Collapsed ;
      private bool Dvpanel_areadetrabalho_Enabled ;
      private bool Dvpanel_areadetrabalho_Autowidth ;
      private bool Dvpanel_areadetrabalho_Autoheight ;
      private bool Dvpanel_areadetrabalho_Showheader ;
      private bool Dvpanel_areadetrabalho_Showcollapseicon ;
      private bool Dvpanel_areadetrabalho_Autoscroll ;
      private bool Dvpanel_areadetrabalho_Visible ;
      private bool returnInSub ;
      private bool Z551Contratante_EmailSdaAut ;
      private bool Gx_longc ;
      private bool i72AreaTrabalho_Ativo ;
      private String Z6AreaTrabalho_Descricao ;
      private String AV15Contratante_CNPJ ;
      private String AV20Contratante_Email ;
      private String AV19Contratante_WebSite ;
      private String AV32Contratante_EmailSdaHost ;
      private String AV33Contratante_EmailSdaUser ;
      private String AV35Contratante_EmailSdaPass ;
      private String A6AreaTrabalho_Descricao ;
      private String A12Contratante_CNPJ ;
      private String A14Contratante_Email ;
      private String A13Contratante_WebSite ;
      private String A547Contratante_EmailSdaHost ;
      private String A548Contratante_EmailSdaUser ;
      private String A549Contratante_EmailSdaPass ;
      private String Z14Contratante_Email ;
      private String Z13Contratante_WebSite ;
      private String Z547Contratante_EmailSdaHost ;
      private String Z548Contratante_EmailSdaUser ;
      private String Z549Contratante_EmailSdaPass ;
      private String Z12Contratante_CNPJ ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbAreaTrabalho_CalculoPFinal ;
      private GXCombobox dynAreaTrabalho_ServicoPadrao ;
      private GXCombobox cmbAreaTrabalho_ValidaOSFM ;
      private GXCombobox dynAreaTrabalho_ContratadaUpdBslCod ;
      private GXCombobox cmbAreaTrabalho_VerTA ;
      private GXCheckbox chkAreaTrabalho_Ativo ;
      private GXCombobox dynavEstado_uf ;
      private GXCombobox dynavMunicipio_codigo ;
      private GXCombobox cmbavContratante_emailsdaaut ;
      private GXCombobox cmbavContratante_emailsdasec ;
      private GXCheckbox chkavPodedesativar ;
      private IDataStoreProvider pr_default ;
      private short[] T000411_A272AreaTrabalho_ContagensQtdGeral ;
      private bool[] T000411_n272AreaTrabalho_ContagensQtdGeral ;
      private String[] T00044_A33Contratante_Fax ;
      private bool[] T00044_n33Contratante_Fax ;
      private String[] T00044_A32Contratante_Ramal ;
      private bool[] T00044_n32Contratante_Ramal ;
      private String[] T00044_A31Contratante_Telefone ;
      private String[] T00044_A14Contratante_Email ;
      private bool[] T00044_n14Contratante_Email ;
      private String[] T00044_A13Contratante_WebSite ;
      private bool[] T00044_n13Contratante_WebSite ;
      private String[] T00044_A11Contratante_IE ;
      private String[] T00044_A10Contratante_NomeFantasia ;
      private String[] T00044_A547Contratante_EmailSdaHost ;
      private bool[] T00044_n547Contratante_EmailSdaHost ;
      private String[] T00044_A548Contratante_EmailSdaUser ;
      private bool[] T00044_n548Contratante_EmailSdaUser ;
      private String[] T00044_A549Contratante_EmailSdaPass ;
      private bool[] T00044_n549Contratante_EmailSdaPass ;
      private String[] T00044_A550Contratante_EmailSdaKey ;
      private bool[] T00044_n550Contratante_EmailSdaKey ;
      private bool[] T00044_A551Contratante_EmailSdaAut ;
      private bool[] T00044_n551Contratante_EmailSdaAut ;
      private short[] T00044_A552Contratante_EmailSdaPort ;
      private bool[] T00044_n552Contratante_EmailSdaPort ;
      private short[] T00044_A1048Contratante_EmailSdaSec ;
      private bool[] T00044_n1048Contratante_EmailSdaSec ;
      private int[] T00044_A25Municipio_Codigo ;
      private bool[] T00044_n25Municipio_Codigo ;
      private int[] T00044_A335Contratante_PessoaCod ;
      private String[] T00047_A26Municipio_Nome ;
      private String[] T00047_A23Estado_UF ;
      private String[] T00048_A24Estado_Nome ;
      private String[] T00049_A12Contratante_CNPJ ;
      private bool[] T00049_n12Contratante_CNPJ ;
      private String[] T00049_A9Contratante_RazaoSocial ;
      private bool[] T00049_n9Contratante_RazaoSocial ;
      private String[] T00046_A1214Organizacao_Nome ;
      private bool[] T00046_n1214Organizacao_Nome ;
      private int[] T000413_A5AreaTrabalho_Codigo ;
      private String[] T000413_A6AreaTrabalho_Descricao ;
      private String[] T000413_A1214Organizacao_Nome ;
      private bool[] T000413_n1214Organizacao_Nome ;
      private String[] T000413_A24Estado_Nome ;
      private String[] T000413_A26Municipio_Nome ;
      private String[] T000413_A33Contratante_Fax ;
      private bool[] T000413_n33Contratante_Fax ;
      private String[] T000413_A32Contratante_Ramal ;
      private bool[] T000413_n32Contratante_Ramal ;
      private String[] T000413_A31Contratante_Telefone ;
      private String[] T000413_A14Contratante_Email ;
      private bool[] T000413_n14Contratante_Email ;
      private String[] T000413_A13Contratante_WebSite ;
      private bool[] T000413_n13Contratante_WebSite ;
      private String[] T000413_A12Contratante_CNPJ ;
      private bool[] T000413_n12Contratante_CNPJ ;
      private String[] T000413_A11Contratante_IE ;
      private String[] T000413_A10Contratante_NomeFantasia ;
      private String[] T000413_A9Contratante_RazaoSocial ;
      private bool[] T000413_n9Contratante_RazaoSocial ;
      private String[] T000413_A547Contratante_EmailSdaHost ;
      private bool[] T000413_n547Contratante_EmailSdaHost ;
      private String[] T000413_A548Contratante_EmailSdaUser ;
      private bool[] T000413_n548Contratante_EmailSdaUser ;
      private String[] T000413_A549Contratante_EmailSdaPass ;
      private bool[] T000413_n549Contratante_EmailSdaPass ;
      private String[] T000413_A550Contratante_EmailSdaKey ;
      private bool[] T000413_n550Contratante_EmailSdaKey ;
      private bool[] T000413_A551Contratante_EmailSdaAut ;
      private bool[] T000413_n551Contratante_EmailSdaAut ;
      private short[] T000413_A552Contratante_EmailSdaPort ;
      private bool[] T000413_n552Contratante_EmailSdaPort ;
      private short[] T000413_A1048Contratante_EmailSdaSec ;
      private bool[] T000413_n1048Contratante_EmailSdaSec ;
      private String[] T000413_A642AreaTrabalho_CalculoPFinal ;
      private bool[] T000413_A834AreaTrabalho_ValidaOSFM ;
      private bool[] T000413_n834AreaTrabalho_ValidaOSFM ;
      private short[] T000413_A855AreaTrabalho_DiasParaPagar ;
      private bool[] T000413_n855AreaTrabalho_DiasParaPagar ;
      private int[] T000413_A987AreaTrabalho_ContratadaUpdBslCod ;
      private bool[] T000413_n987AreaTrabalho_ContratadaUpdBslCod ;
      private short[] T000413_A1154AreaTrabalho_TipoPlanilha ;
      private bool[] T000413_n1154AreaTrabalho_TipoPlanilha ;
      private bool[] T000413_A72AreaTrabalho_Ativo ;
      private int[] T000413_A1588AreaTrabalho_SS_Codigo ;
      private bool[] T000413_n1588AreaTrabalho_SS_Codigo ;
      private short[] T000413_A2081AreaTrabalho_VerTA ;
      private bool[] T000413_n2081AreaTrabalho_VerTA ;
      private bool[] T000413_A2080AreaTrabalho_SelUsrPrestadora ;
      private bool[] T000413_n2080AreaTrabalho_SelUsrPrestadora ;
      private int[] T000413_A29Contratante_Codigo ;
      private bool[] T000413_n29Contratante_Codigo ;
      private int[] T000413_A830AreaTrabalho_ServicoPadrao ;
      private bool[] T000413_n830AreaTrabalho_ServicoPadrao ;
      private int[] T000413_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] T000413_n1216AreaTrabalho_OrganizacaoCod ;
      private int[] T000413_A25Municipio_Codigo ;
      private bool[] T000413_n25Municipio_Codigo ;
      private String[] T000413_A23Estado_UF ;
      private int[] T000413_A335Contratante_PessoaCod ;
      private short[] T000413_A272AreaTrabalho_ContagensQtdGeral ;
      private bool[] T000413_n272AreaTrabalho_ContagensQtdGeral ;
      private int[] T00045_A830AreaTrabalho_ServicoPadrao ;
      private bool[] T00045_n830AreaTrabalho_ServicoPadrao ;
      private short[] T000415_A272AreaTrabalho_ContagensQtdGeral ;
      private bool[] T000415_n272AreaTrabalho_ContagensQtdGeral ;
      private String[] T000416_A33Contratante_Fax ;
      private bool[] T000416_n33Contratante_Fax ;
      private String[] T000416_A32Contratante_Ramal ;
      private bool[] T000416_n32Contratante_Ramal ;
      private String[] T000416_A31Contratante_Telefone ;
      private String[] T000416_A14Contratante_Email ;
      private bool[] T000416_n14Contratante_Email ;
      private String[] T000416_A13Contratante_WebSite ;
      private bool[] T000416_n13Contratante_WebSite ;
      private String[] T000416_A11Contratante_IE ;
      private String[] T000416_A10Contratante_NomeFantasia ;
      private String[] T000416_A547Contratante_EmailSdaHost ;
      private bool[] T000416_n547Contratante_EmailSdaHost ;
      private String[] T000416_A548Contratante_EmailSdaUser ;
      private bool[] T000416_n548Contratante_EmailSdaUser ;
      private String[] T000416_A549Contratante_EmailSdaPass ;
      private bool[] T000416_n549Contratante_EmailSdaPass ;
      private String[] T000416_A550Contratante_EmailSdaKey ;
      private bool[] T000416_n550Contratante_EmailSdaKey ;
      private bool[] T000416_A551Contratante_EmailSdaAut ;
      private bool[] T000416_n551Contratante_EmailSdaAut ;
      private short[] T000416_A552Contratante_EmailSdaPort ;
      private bool[] T000416_n552Contratante_EmailSdaPort ;
      private short[] T000416_A1048Contratante_EmailSdaSec ;
      private bool[] T000416_n1048Contratante_EmailSdaSec ;
      private int[] T000416_A25Municipio_Codigo ;
      private bool[] T000416_n25Municipio_Codigo ;
      private int[] T000416_A335Contratante_PessoaCod ;
      private String[] T000417_A26Municipio_Nome ;
      private String[] T000417_A23Estado_UF ;
      private String[] T000418_A24Estado_Nome ;
      private String[] T000419_A12Contratante_CNPJ ;
      private bool[] T000419_n12Contratante_CNPJ ;
      private String[] T000419_A9Contratante_RazaoSocial ;
      private bool[] T000419_n9Contratante_RazaoSocial ;
      private int[] T000420_A830AreaTrabalho_ServicoPadrao ;
      private bool[] T000420_n830AreaTrabalho_ServicoPadrao ;
      private String[] T000421_A1214Organizacao_Nome ;
      private bool[] T000421_n1214Organizacao_Nome ;
      private int[] T000422_A5AreaTrabalho_Codigo ;
      private int[] T00043_A5AreaTrabalho_Codigo ;
      private String[] T00043_A6AreaTrabalho_Descricao ;
      private String[] T00043_A642AreaTrabalho_CalculoPFinal ;
      private bool[] T00043_A834AreaTrabalho_ValidaOSFM ;
      private bool[] T00043_n834AreaTrabalho_ValidaOSFM ;
      private short[] T00043_A855AreaTrabalho_DiasParaPagar ;
      private bool[] T00043_n855AreaTrabalho_DiasParaPagar ;
      private int[] T00043_A987AreaTrabalho_ContratadaUpdBslCod ;
      private bool[] T00043_n987AreaTrabalho_ContratadaUpdBslCod ;
      private short[] T00043_A1154AreaTrabalho_TipoPlanilha ;
      private bool[] T00043_n1154AreaTrabalho_TipoPlanilha ;
      private bool[] T00043_A72AreaTrabalho_Ativo ;
      private int[] T00043_A1588AreaTrabalho_SS_Codigo ;
      private bool[] T00043_n1588AreaTrabalho_SS_Codigo ;
      private short[] T00043_A2081AreaTrabalho_VerTA ;
      private bool[] T00043_n2081AreaTrabalho_VerTA ;
      private bool[] T00043_A2080AreaTrabalho_SelUsrPrestadora ;
      private bool[] T00043_n2080AreaTrabalho_SelUsrPrestadora ;
      private int[] T00043_A29Contratante_Codigo ;
      private bool[] T00043_n29Contratante_Codigo ;
      private int[] T00043_A830AreaTrabalho_ServicoPadrao ;
      private bool[] T00043_n830AreaTrabalho_ServicoPadrao ;
      private int[] T00043_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] T00043_n1216AreaTrabalho_OrganizacaoCod ;
      private int[] T000423_A5AreaTrabalho_Codigo ;
      private int[] T000424_A5AreaTrabalho_Codigo ;
      private int[] T00042_A5AreaTrabalho_Codigo ;
      private String[] T00042_A6AreaTrabalho_Descricao ;
      private String[] T00042_A642AreaTrabalho_CalculoPFinal ;
      private bool[] T00042_A834AreaTrabalho_ValidaOSFM ;
      private bool[] T00042_n834AreaTrabalho_ValidaOSFM ;
      private short[] T00042_A855AreaTrabalho_DiasParaPagar ;
      private bool[] T00042_n855AreaTrabalho_DiasParaPagar ;
      private int[] T00042_A987AreaTrabalho_ContratadaUpdBslCod ;
      private bool[] T00042_n987AreaTrabalho_ContratadaUpdBslCod ;
      private short[] T00042_A1154AreaTrabalho_TipoPlanilha ;
      private bool[] T00042_n1154AreaTrabalho_TipoPlanilha ;
      private bool[] T00042_A72AreaTrabalho_Ativo ;
      private int[] T00042_A1588AreaTrabalho_SS_Codigo ;
      private bool[] T00042_n1588AreaTrabalho_SS_Codigo ;
      private short[] T00042_A2081AreaTrabalho_VerTA ;
      private bool[] T00042_n2081AreaTrabalho_VerTA ;
      private bool[] T00042_A2080AreaTrabalho_SelUsrPrestadora ;
      private bool[] T00042_n2080AreaTrabalho_SelUsrPrestadora ;
      private int[] T00042_A29Contratante_Codigo ;
      private bool[] T00042_n29Contratante_Codigo ;
      private int[] T00042_A830AreaTrabalho_ServicoPadrao ;
      private bool[] T00042_n830AreaTrabalho_ServicoPadrao ;
      private int[] T00042_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] T00042_n1216AreaTrabalho_OrganizacaoCod ;
      private int[] T000425_A5AreaTrabalho_Codigo ;
      private short[] T000429_A272AreaTrabalho_ContagensQtdGeral ;
      private bool[] T000429_n272AreaTrabalho_ContagensQtdGeral ;
      private String[] T000430_A33Contratante_Fax ;
      private bool[] T000430_n33Contratante_Fax ;
      private String[] T000430_A32Contratante_Ramal ;
      private bool[] T000430_n32Contratante_Ramal ;
      private String[] T000430_A31Contratante_Telefone ;
      private String[] T000430_A14Contratante_Email ;
      private bool[] T000430_n14Contratante_Email ;
      private String[] T000430_A13Contratante_WebSite ;
      private bool[] T000430_n13Contratante_WebSite ;
      private String[] T000430_A11Contratante_IE ;
      private String[] T000430_A10Contratante_NomeFantasia ;
      private String[] T000430_A547Contratante_EmailSdaHost ;
      private bool[] T000430_n547Contratante_EmailSdaHost ;
      private String[] T000430_A548Contratante_EmailSdaUser ;
      private bool[] T000430_n548Contratante_EmailSdaUser ;
      private String[] T000430_A549Contratante_EmailSdaPass ;
      private bool[] T000430_n549Contratante_EmailSdaPass ;
      private String[] T000430_A550Contratante_EmailSdaKey ;
      private bool[] T000430_n550Contratante_EmailSdaKey ;
      private bool[] T000430_A551Contratante_EmailSdaAut ;
      private bool[] T000430_n551Contratante_EmailSdaAut ;
      private short[] T000430_A552Contratante_EmailSdaPort ;
      private bool[] T000430_n552Contratante_EmailSdaPort ;
      private short[] T000430_A1048Contratante_EmailSdaSec ;
      private bool[] T000430_n1048Contratante_EmailSdaSec ;
      private int[] T000430_A25Municipio_Codigo ;
      private bool[] T000430_n25Municipio_Codigo ;
      private int[] T000430_A335Contratante_PessoaCod ;
      private String[] T000431_A26Municipio_Nome ;
      private String[] T000431_A23Estado_UF ;
      private String[] T000432_A24Estado_Nome ;
      private String[] T000433_A12Contratante_CNPJ ;
      private bool[] T000433_n12Contratante_CNPJ ;
      private String[] T000433_A9Contratante_RazaoSocial ;
      private bool[] T000433_n9Contratante_RazaoSocial ;
      private String[] T000434_A1214Organizacao_Nome ;
      private bool[] T000434_n1214Organizacao_Nome ;
      private int[] T000435_A619GrupoFuncao_Codigo ;
      private int[] T000436_A2175Equipe_Codigo ;
      private int[] T000437_A648Projeto_Codigo ;
      private int[] T000438_A2077UsuarioNotifica_Codigo ;
      private int[] T000439_A2058Indicador_Codigo ;
      private long[] T000440_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] T000441_A1795CatalogoServico_Codigo ;
      private int[] T000442_A1482Gestao_Codigo ;
      private int[] T000443_A1347Glosario_Codigo ;
      private int[] T000444_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] T000444_A962VariavelCocomo_Sigla ;
      private String[] T000444_A964VariavelCocomo_Tipo ;
      private short[] T000444_A992VariavelCocomo_Sequencial ;
      private String[] T000445_A860RegrasContagem_Regra ;
      private int[] T000446_A848ParametrosPln_Codigo ;
      private int[] T000447_A351AmbienteTecnologico_Codigo ;
      private int[] T000448_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] T000448_A715ItemNaoMensuravel_Codigo ;
      private int[] T000449_A709ReferenciaINM_Codigo ;
      private int[] T000450_A596Lote_Codigo ;
      private int[] T000451_A426NaoConformidade_Codigo ;
      private int[] T000452_A423Status_Codigo ;
      private int[] T000453_A93Guia_Codigo ;
      private int[] T000454_A192Contagem_Codigo ;
      private int[] T000455_A127Sistema_Codigo ;
      private int[] T000456_A74Contrato_Codigo ;
      private int[] T000457_A39Contratada_Codigo ;
      private int[] T000458_A3Perfil_Codigo ;
      private int[] T000459_A1685Proposta_Codigo ;
      private int[] T000460_A5AreaTrabalho_Codigo ;
      private int[] T000460_A2127FatoresImpacto_Codigo ;
      private int[] T000461_A5AreaTrabalho_Codigo ;
      private int[] T000461_A93Guia_Codigo ;
      private int[] T000462_A5AreaTrabalho_Codigo ;
      private int[] T000463_A830AreaTrabalho_ServicoPadrao ;
      private bool[] T000463_n830AreaTrabalho_ServicoPadrao ;
      private String[] T000463_A605Servico_Sigla ;
      private bool[] T000463_n605Servico_Sigla ;
      private int[] T000464_A40Contratada_PessoaCod ;
      private int[] T000464_A39Contratada_Codigo ;
      private String[] T000464_A41Contratada_PessoaNom ;
      private bool[] T000464_n41Contratada_PessoaNom ;
      private int[] T000464_A52Contratada_AreaTrabalhoCod ;
      private String[] T000465_A23Estado_UF ;
      private String[] T000465_A24Estado_Nome ;
      private int[] T000466_A25Municipio_Codigo ;
      private bool[] T000466_n25Municipio_Codigo ;
      private String[] T000466_A26Municipio_Nome ;
      private String[] T000466_A23Estado_UF ;
      private short[] T000468_A272AreaTrabalho_ContagensQtdGeral ;
      private bool[] T000468_n272AreaTrabalho_ContagensQtdGeral ;
      private int[] T000469_A830AreaTrabalho_ServicoPadrao ;
      private bool[] T000469_n830AreaTrabalho_ServicoPadrao ;
      private String[] T000470_A33Contratante_Fax ;
      private bool[] T000470_n33Contratante_Fax ;
      private String[] T000470_A32Contratante_Ramal ;
      private bool[] T000470_n32Contratante_Ramal ;
      private String[] T000470_A31Contratante_Telefone ;
      private String[] T000470_A14Contratante_Email ;
      private bool[] T000470_n14Contratante_Email ;
      private String[] T000470_A13Contratante_WebSite ;
      private bool[] T000470_n13Contratante_WebSite ;
      private String[] T000470_A11Contratante_IE ;
      private String[] T000470_A10Contratante_NomeFantasia ;
      private String[] T000470_A547Contratante_EmailSdaHost ;
      private bool[] T000470_n547Contratante_EmailSdaHost ;
      private String[] T000470_A548Contratante_EmailSdaUser ;
      private bool[] T000470_n548Contratante_EmailSdaUser ;
      private String[] T000470_A549Contratante_EmailSdaPass ;
      private bool[] T000470_n549Contratante_EmailSdaPass ;
      private String[] T000470_A550Contratante_EmailSdaKey ;
      private bool[] T000470_n550Contratante_EmailSdaKey ;
      private bool[] T000470_A551Contratante_EmailSdaAut ;
      private bool[] T000470_n551Contratante_EmailSdaAut ;
      private short[] T000470_A552Contratante_EmailSdaPort ;
      private bool[] T000470_n552Contratante_EmailSdaPort ;
      private short[] T000470_A1048Contratante_EmailSdaSec ;
      private bool[] T000470_n1048Contratante_EmailSdaSec ;
      private int[] T000470_A25Municipio_Codigo ;
      private bool[] T000470_n25Municipio_Codigo ;
      private int[] T000470_A335Contratante_PessoaCod ;
      private String[] T000471_A26Municipio_Nome ;
      private String[] T000471_A23Estado_UF ;
      private String[] T000472_A24Estado_Nome ;
      private String[] T000473_A12Contratante_CNPJ ;
      private bool[] T000473_n12Contratante_CNPJ ;
      private String[] T000473_A9Contratante_RazaoSocial ;
      private bool[] T000473_n9Contratante_RazaoSocial ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtAuditingObject AV30AuditingObject ;
      private SdtParametrosSistema AV31ParametrosSistema ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class areatrabalho__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new UpdateCursor(def[21])
         ,new UpdateCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new ForEachCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
         ,new ForEachCursor(def[48])
         ,new ForEachCursor(def[49])
         ,new ForEachCursor(def[50])
         ,new ForEachCursor(def[51])
         ,new ForEachCursor(def[52])
         ,new ForEachCursor(def[53])
         ,new ForEachCursor(def[54])
         ,new ForEachCursor(def[55])
         ,new ForEachCursor(def[56])
         ,new ForEachCursor(def[57])
         ,new ForEachCursor(def[58])
         ,new ForEachCursor(def[59])
         ,new ForEachCursor(def[60])
         ,new ForEachCursor(def[61])
         ,new ForEachCursor(def[62])
         ,new ForEachCursor(def[63])
         ,new ForEachCursor(def[64])
         ,new ForEachCursor(def[65])
         ,new ForEachCursor(def[66])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000413 ;
          prmT000413 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT000413 ;
          cmdBufferT000413=" SELECT TM1.[AreaTrabalho_Codigo], TM1.[AreaTrabalho_Descricao], T2.[Organizacao_Nome], T6.[Estado_Nome], T5.[Municipio_Nome], T4.[Contratante_Fax], T4.[Contratante_Ramal], T4.[Contratante_Telefone], T4.[Contratante_Email], T4.[Contratante_WebSite], T7.[Pessoa_Docto] AS Contratante_CNPJ, T4.[Contratante_IE], T4.[Contratante_NomeFantasia], T7.[Pessoa_Nome] AS Contratante_RazaoSocial, T4.[Contratante_EmailSdaHost], T4.[Contratante_EmailSdaUser], T4.[Contratante_EmailSdaPass], T4.[Contratante_EmailSdaKey], T4.[Contratante_EmailSdaAut], T4.[Contratante_EmailSdaPort], T4.[Contratante_EmailSdaSec], TM1.[AreaTrabalho_CalculoPFinal], TM1.[AreaTrabalho_ValidaOSFM], TM1.[AreaTrabalho_DiasParaPagar], TM1.[AreaTrabalho_ContratadaUpdBslCod], TM1.[AreaTrabalho_TipoPlanilha], TM1.[AreaTrabalho_Ativo], TM1.[AreaTrabalho_SS_Codigo], TM1.[AreaTrabalho_VerTA], TM1.[AreaTrabalho_SelUsrPrestadora], TM1.[Contratante_Codigo], TM1.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, TM1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T4.[Municipio_Codigo], T5.[Estado_UF], T4.[Contratante_PessoaCod] AS Contratante_PessoaCod, COALESCE( T3.[AreaTrabalho_ContagensQtdGeral], 0) AS AreaTrabalho_ContagensQtdGeral FROM (((((([AreaTrabalho] TM1 WITH (NOLOCK) LEFT JOIN [Organizacao] T2 WITH (NOLOCK) ON T2.[Organizacao_Codigo] = TM1.[AreaTrabalho_OrganizacaoCod]) LEFT JOIN (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] ) T3 ON T3.[Contagem_AreaTrabalhoCod] = TM1.[AreaTrabalho_Codigo]) LEFT JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = TM1.[Contratante_Codigo]) LEFT JOIN [Municipio] T5 WITH (NOLOCK) ON T5.[Municipio_Codigo] = T4.[Municipio_Codigo]) LEFT JOIN [Estado] T6 WITH "
          + " (NOLOCK) ON T6.[Estado_UF] = T5.[Estado_UF]) LEFT JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T4.[Contratante_PessoaCod]) WHERE TM1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY TM1.[AreaTrabalho_Codigo]  OPTION (FAST 100)" ;
          Object[] prmT000411 ;
          prmT000411 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00044 ;
          prmT00044 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00047 ;
          prmT00047 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00048 ;
          prmT00048 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT00049 ;
          prmT00049 = new Object[] {
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00045 ;
          prmT00045 = new Object[] {
          new Object[] {"@AreaTrabalho_ServicoPadrao",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00046 ;
          prmT00046 = new Object[] {
          new Object[] {"@AreaTrabalho_OrganizacaoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000415 ;
          prmT000415 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000416 ;
          prmT000416 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000417 ;
          prmT000417 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000418 ;
          prmT000418 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT000419 ;
          prmT000419 = new Object[] {
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000420 ;
          prmT000420 = new Object[] {
          new Object[] {"@AreaTrabalho_ServicoPadrao",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000421 ;
          prmT000421 = new Object[] {
          new Object[] {"@AreaTrabalho_OrganizacaoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000422 ;
          prmT000422 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00043 ;
          prmT00043 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000423 ;
          prmT000423 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000424 ;
          prmT000424 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00042 ;
          prmT00042 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000425 ;
          prmT000425 = new Object[] {
          new Object[] {"@AreaTrabalho_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AreaTrabalho_CalculoPFinal",SqlDbType.Char,2,0} ,
          new Object[] {"@AreaTrabalho_ValidaOSFM",SqlDbType.Bit,4,0} ,
          new Object[] {"@AreaTrabalho_DiasParaPagar",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AreaTrabalho_ContratadaUpdBslCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_TipoPlanilha",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AreaTrabalho_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AreaTrabalho_SS_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_VerTA",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AreaTrabalho_SelUsrPrestadora",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_ServicoPadrao",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_OrganizacaoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000426 ;
          prmT000426 = new Object[] {
          new Object[] {"@AreaTrabalho_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AreaTrabalho_CalculoPFinal",SqlDbType.Char,2,0} ,
          new Object[] {"@AreaTrabalho_ValidaOSFM",SqlDbType.Bit,4,0} ,
          new Object[] {"@AreaTrabalho_DiasParaPagar",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AreaTrabalho_ContratadaUpdBslCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_TipoPlanilha",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AreaTrabalho_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AreaTrabalho_SS_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_VerTA",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AreaTrabalho_SelUsrPrestadora",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_ServicoPadrao",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_OrganizacaoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000427 ;
          prmT000427 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000429 ;
          prmT000429 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000430 ;
          prmT000430 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000431 ;
          prmT000431 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000432 ;
          prmT000432 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT000433 ;
          prmT000433 = new Object[] {
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000434 ;
          prmT000434 = new Object[] {
          new Object[] {"@AreaTrabalho_OrganizacaoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000435 ;
          prmT000435 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000436 ;
          prmT000436 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000437 ;
          prmT000437 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000438 ;
          prmT000438 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000439 ;
          prmT000439 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000440 ;
          prmT000440 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000441 ;
          prmT000441 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000442 ;
          prmT000442 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000443 ;
          prmT000443 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000444 ;
          prmT000444 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000445 ;
          prmT000445 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000446 ;
          prmT000446 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000447 ;
          prmT000447 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000448 ;
          prmT000448 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000449 ;
          prmT000449 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000450 ;
          prmT000450 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000451 ;
          prmT000451 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000452 ;
          prmT000452 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000453 ;
          prmT000453 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000454 ;
          prmT000454 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000455 ;
          prmT000455 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000456 ;
          prmT000456 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000457 ;
          prmT000457 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000458 ;
          prmT000458 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000459 ;
          prmT000459 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000460 ;
          prmT000460 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000461 ;
          prmT000461 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000462 ;
          prmT000462 = new Object[] {
          } ;
          Object[] prmT000463 ;
          prmT000463 = new Object[] {
          } ;
          Object[] prmT000464 ;
          prmT000464 = new Object[] {
          new Object[] {"@AV8WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000465 ;
          prmT000465 = new Object[] {
          } ;
          Object[] prmT000466 ;
          prmT000466 = new Object[] {
          new Object[] {"@AV24Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT000468 ;
          prmT000468 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000469 ;
          prmT000469 = new Object[] {
          new Object[] {"@AreaTrabalho_ServicoPadrao",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000470 ;
          prmT000470 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000471 ;
          prmT000471 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000472 ;
          prmT000472 = new Object[] {
          new Object[] {"@Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT000473 ;
          prmT000473 = new Object[] {
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00042", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao], [AreaTrabalho_CalculoPFinal], [AreaTrabalho_ValidaOSFM], [AreaTrabalho_DiasParaPagar], [AreaTrabalho_ContratadaUpdBslCod], [AreaTrabalho_TipoPlanilha], [AreaTrabalho_Ativo], [AreaTrabalho_SS_Codigo], [AreaTrabalho_VerTA], [AreaTrabalho_SelUsrPrestadora], [Contratante_Codigo], [AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, [AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod FROM [AreaTrabalho] WITH (UPDLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00042,1,0,true,false )
             ,new CursorDef("T00043", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao], [AreaTrabalho_CalculoPFinal], [AreaTrabalho_ValidaOSFM], [AreaTrabalho_DiasParaPagar], [AreaTrabalho_ContratadaUpdBslCod], [AreaTrabalho_TipoPlanilha], [AreaTrabalho_Ativo], [AreaTrabalho_SS_Codigo], [AreaTrabalho_VerTA], [AreaTrabalho_SelUsrPrestadora], [Contratante_Codigo], [AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, [AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00043,1,0,true,false )
             ,new CursorDef("T00044", "SELECT [Contratante_Fax], [Contratante_Ramal], [Contratante_Telefone], [Contratante_Email], [Contratante_WebSite], [Contratante_IE], [Contratante_NomeFantasia], [Contratante_EmailSdaHost], [Contratante_EmailSdaUser], [Contratante_EmailSdaPass], [Contratante_EmailSdaKey], [Contratante_EmailSdaAut], [Contratante_EmailSdaPort], [Contratante_EmailSdaSec], [Municipio_Codigo], [Contratante_PessoaCod] AS Contratante_PessoaCod FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00044,1,0,true,false )
             ,new CursorDef("T00045", "SELECT [Servico_Codigo] AS AreaTrabalho_ServicoPadrao FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AreaTrabalho_ServicoPadrao ",true, GxErrorMask.GX_NOMASK, false, this,prmT00045,1,0,true,false )
             ,new CursorDef("T00046", "SELECT [Organizacao_Nome] FROM [Organizacao] WITH (NOLOCK) WHERE [Organizacao_Codigo] = @AreaTrabalho_OrganizacaoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00046,1,0,true,false )
             ,new CursorDef("T00047", "SELECT [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00047,1,0,true,false )
             ,new CursorDef("T00048", "SELECT [Estado_Nome] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmT00048,1,0,true,false )
             ,new CursorDef("T00049", "SELECT [Pessoa_Docto] AS Contratante_CNPJ, [Pessoa_Nome] AS Contratante_RazaoSocial FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratante_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00049,1,0,true,false )
             ,new CursorDef("T000411", "SELECT COALESCE( T1.[AreaTrabalho_ContagensQtdGeral], 0) AS AreaTrabalho_ContagensQtdGeral FROM (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] ) T1 WHERE T1.[Contagem_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000411,1,0,true,false )
             ,new CursorDef("T000413", cmdBufferT000413,true, GxErrorMask.GX_NOMASK, false, this,prmT000413,100,0,true,false )
             ,new CursorDef("T000415", "SELECT COALESCE( T1.[AreaTrabalho_ContagensQtdGeral], 0) AS AreaTrabalho_ContagensQtdGeral FROM (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] ) T1 WHERE T1.[Contagem_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000415,1,0,true,false )
             ,new CursorDef("T000416", "SELECT [Contratante_Fax], [Contratante_Ramal], [Contratante_Telefone], [Contratante_Email], [Contratante_WebSite], [Contratante_IE], [Contratante_NomeFantasia], [Contratante_EmailSdaHost], [Contratante_EmailSdaUser], [Contratante_EmailSdaPass], [Contratante_EmailSdaKey], [Contratante_EmailSdaAut], [Contratante_EmailSdaPort], [Contratante_EmailSdaSec], [Municipio_Codigo], [Contratante_PessoaCod] AS Contratante_PessoaCod FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000416,1,0,true,false )
             ,new CursorDef("T000417", "SELECT [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000417,1,0,true,false )
             ,new CursorDef("T000418", "SELECT [Estado_Nome] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmT000418,1,0,true,false )
             ,new CursorDef("T000419", "SELECT [Pessoa_Docto] AS Contratante_CNPJ, [Pessoa_Nome] AS Contratante_RazaoSocial FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratante_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000419,1,0,true,false )
             ,new CursorDef("T000420", "SELECT [Servico_Codigo] AS AreaTrabalho_ServicoPadrao FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AreaTrabalho_ServicoPadrao ",true, GxErrorMask.GX_NOMASK, false, this,prmT000420,1,0,true,false )
             ,new CursorDef("T000421", "SELECT [Organizacao_Nome] FROM [Organizacao] WITH (NOLOCK) WHERE [Organizacao_Codigo] = @AreaTrabalho_OrganizacaoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000421,1,0,true,false )
             ,new CursorDef("T000422", "SELECT [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000422,1,0,true,false )
             ,new CursorDef("T000423", "SELECT TOP 1 [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE ( [AreaTrabalho_Codigo] > @AreaTrabalho_Codigo) ORDER BY [AreaTrabalho_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000423,1,0,true,true )
             ,new CursorDef("T000424", "SELECT TOP 1 [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE ( [AreaTrabalho_Codigo] < @AreaTrabalho_Codigo) ORDER BY [AreaTrabalho_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000424,1,0,true,true )
             ,new CursorDef("T000425", "INSERT INTO [AreaTrabalho]([AreaTrabalho_Descricao], [AreaTrabalho_CalculoPFinal], [AreaTrabalho_ValidaOSFM], [AreaTrabalho_DiasParaPagar], [AreaTrabalho_ContratadaUpdBslCod], [AreaTrabalho_TipoPlanilha], [AreaTrabalho_Ativo], [AreaTrabalho_SS_Codigo], [AreaTrabalho_VerTA], [AreaTrabalho_SelUsrPrestadora], [Contratante_Codigo], [AreaTrabalho_ServicoPadrao], [AreaTrabalho_OrganizacaoCod]) VALUES(@AreaTrabalho_Descricao, @AreaTrabalho_CalculoPFinal, @AreaTrabalho_ValidaOSFM, @AreaTrabalho_DiasParaPagar, @AreaTrabalho_ContratadaUpdBslCod, @AreaTrabalho_TipoPlanilha, @AreaTrabalho_Ativo, @AreaTrabalho_SS_Codigo, @AreaTrabalho_VerTA, @AreaTrabalho_SelUsrPrestadora, @Contratante_Codigo, @AreaTrabalho_ServicoPadrao, @AreaTrabalho_OrganizacaoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000425)
             ,new CursorDef("T000426", "UPDATE [AreaTrabalho] SET [AreaTrabalho_Descricao]=@AreaTrabalho_Descricao, [AreaTrabalho_CalculoPFinal]=@AreaTrabalho_CalculoPFinal, [AreaTrabalho_ValidaOSFM]=@AreaTrabalho_ValidaOSFM, [AreaTrabalho_DiasParaPagar]=@AreaTrabalho_DiasParaPagar, [AreaTrabalho_ContratadaUpdBslCod]=@AreaTrabalho_ContratadaUpdBslCod, [AreaTrabalho_TipoPlanilha]=@AreaTrabalho_TipoPlanilha, [AreaTrabalho_Ativo]=@AreaTrabalho_Ativo, [AreaTrabalho_SS_Codigo]=@AreaTrabalho_SS_Codigo, [AreaTrabalho_VerTA]=@AreaTrabalho_VerTA, [AreaTrabalho_SelUsrPrestadora]=@AreaTrabalho_SelUsrPrestadora, [Contratante_Codigo]=@Contratante_Codigo, [AreaTrabalho_ServicoPadrao]=@AreaTrabalho_ServicoPadrao, [AreaTrabalho_OrganizacaoCod]=@AreaTrabalho_OrganizacaoCod  WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo", GxErrorMask.GX_NOMASK,prmT000426)
             ,new CursorDef("T000427", "DELETE FROM [AreaTrabalho]  WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo", GxErrorMask.GX_NOMASK,prmT000427)
             ,new CursorDef("T000429", "SELECT COALESCE( T1.[AreaTrabalho_ContagensQtdGeral], 0) AS AreaTrabalho_ContagensQtdGeral FROM (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] ) T1 WHERE T1.[Contagem_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000429,1,0,true,false )
             ,new CursorDef("T000430", "SELECT [Contratante_Fax], [Contratante_Ramal], [Contratante_Telefone], [Contratante_Email], [Contratante_WebSite], [Contratante_IE], [Contratante_NomeFantasia], [Contratante_EmailSdaHost], [Contratante_EmailSdaUser], [Contratante_EmailSdaPass], [Contratante_EmailSdaKey], [Contratante_EmailSdaAut], [Contratante_EmailSdaPort], [Contratante_EmailSdaSec], [Municipio_Codigo], [Contratante_PessoaCod] AS Contratante_PessoaCod FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000430,1,0,true,false )
             ,new CursorDef("T000431", "SELECT [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000431,1,0,true,false )
             ,new CursorDef("T000432", "SELECT [Estado_Nome] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmT000432,1,0,true,false )
             ,new CursorDef("T000433", "SELECT [Pessoa_Docto] AS Contratante_CNPJ, [Pessoa_Nome] AS Contratante_RazaoSocial FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratante_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000433,1,0,true,false )
             ,new CursorDef("T000434", "SELECT [Organizacao_Nome] FROM [Organizacao] WITH (NOLOCK) WHERE [Organizacao_Codigo] = @AreaTrabalho_OrganizacaoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000434,1,0,true,false )
             ,new CursorDef("T000435", "SELECT TOP 1 [GrupoFuncao_Codigo] FROM [Geral_Grupo_Funcao] WITH (NOLOCK) WHERE [GrupoFuncao_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000435,1,0,true,true )
             ,new CursorDef("T000436", "SELECT TOP 1 [Equipe_Codigo] FROM [Equipe] WITH (NOLOCK) WHERE [Equipe_AreaTrabalhoCodigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000436,1,0,true,true )
             ,new CursorDef("T000437", "SELECT TOP 1 [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_AreaTrabalhoCodigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000437,1,0,true,true )
             ,new CursorDef("T000438", "SELECT TOP 1 [UsuarioNotifica_Codigo] FROM [UsuarioNotifica] WITH (NOLOCK) WHERE [UsuarioNotifica_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000438,1,0,true,true )
             ,new CursorDef("T000439", "SELECT TOP 1 [Indicador_Codigo] FROM [Indicador] WITH (NOLOCK) WHERE [Indicador_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000439,1,0,true,true )
             ,new CursorDef("T000440", "SELECT TOP 1 [ContagemResultadoNotificacao_Codigo] FROM [ContagemResultadoNotificacao] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000440,1,0,true,true )
             ,new CursorDef("T000441", "SELECT TOP 1 [CatalogoServico_Codigo] FROM [CatalogoServicos] WITH (NOLOCK) WHERE [CatalogoServico_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000441,1,0,true,true )
             ,new CursorDef("T000442", "SELECT TOP 1 [Gestao_Codigo] FROM [Gestao] WITH (NOLOCK) WHERE [Gestao_AreaCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000442,1,0,true,true )
             ,new CursorDef("T000443", "SELECT TOP 1 [Glosario_Codigo] FROM [Glosario] WITH (NOLOCK) WHERE [Glosario_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000443,1,0,true,true )
             ,new CursorDef("T000444", "SELECT TOP 1 [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE [VariavelCocomo_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000444,1,0,true,true )
             ,new CursorDef("T000445", "SELECT TOP 1 [RegrasContagem_Regra] FROM [RegrasContagem] WITH (NOLOCK) WHERE [RegrasContagem_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000445,1,0,true,true )
             ,new CursorDef("T000446", "SELECT TOP 1 [ParametrosPln_Codigo] FROM [ParametrosPlanilhas] WITH (NOLOCK) WHERE [ParametrosPln_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000446,1,0,true,true )
             ,new CursorDef("T000447", "SELECT TOP 1 [AmbienteTecnologico_Codigo] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000447,1,0,true,true )
             ,new CursorDef("T000448", "SELECT TOP 1 [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Codigo] FROM [ItemNaoMensuravel] WITH (NOLOCK) WHERE [ItemNaoMensuravel_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000448,1,0,true,true )
             ,new CursorDef("T000449", "SELECT TOP 1 [ReferenciaINM_Codigo] FROM [ReferenciaINM] WITH (NOLOCK) WHERE [ReferenciaINM_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000449,1,0,true,true )
             ,new CursorDef("T000450", "SELECT TOP 1 [Lote_Codigo] FROM [Lote] WITH (NOLOCK) WHERE [Lote_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000450,1,0,true,true )
             ,new CursorDef("T000451", "SELECT TOP 1 [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000451,1,0,true,true )
             ,new CursorDef("T000452", "SELECT TOP 1 [Status_Codigo] FROM [Status] WITH (NOLOCK) WHERE [Status_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000452,1,0,true,true )
             ,new CursorDef("T000453", "SELECT TOP 1 [Guia_Codigo] FROM [Guia] WITH (NOLOCK) WHERE [Guia_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000453,1,0,true,true )
             ,new CursorDef("T000454", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000454,1,0,true,true )
             ,new CursorDef("T000455", "SELECT TOP 1 [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000455,1,0,true,true )
             ,new CursorDef("T000456", "SELECT TOP 1 [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000456,1,0,true,true )
             ,new CursorDef("T000457", "SELECT TOP 1 [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000457,1,0,true,true )
             ,new CursorDef("T000458", "SELECT TOP 1 [Perfil_Codigo] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000458,1,0,true,true )
             ,new CursorDef("T000459", "SELECT TOP 1 [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000459,1,0,true,true )
             ,new CursorDef("T000460", "SELECT TOP 1 [AreaTrabalho_Codigo], [FatoresImpacto_Codigo] FROM [FatoresImpacto] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000460,1,0,true,true )
             ,new CursorDef("T000461", "SELECT TOP 1 [AreaTrabalho_Codigo], [Guia_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000461,1,0,true,true )
             ,new CursorDef("T000462", "SELECT [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000462,100,0,true,false )
             ,new CursorDef("T000463", "SELECT [Servico_Codigo] AS AreaTrabalho_ServicoPadrao, [Servico_Sigla] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000463,0,0,true,false )
             ,new CursorDef("T000464", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV8WWPCo_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000464,0,0,true,false )
             ,new CursorDef("T000465", "SELECT [Estado_UF], [Estado_Nome] FROM [Estado] WITH (NOLOCK) ORDER BY [Estado_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000465,0,0,true,false )
             ,new CursorDef("T000466", "SELECT [Municipio_Codigo], [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Estado_UF] = @AV24Estado_UF ORDER BY [Municipio_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000466,0,0,true,false )
             ,new CursorDef("T000468", "SELECT COALESCE( T1.[AreaTrabalho_ContagensQtdGeral], 0) AS AreaTrabalho_ContagensQtdGeral FROM (SELECT COUNT(*) AS AreaTrabalho_ContagensQtdGeral, [Contagem_AreaTrabalhoCod] FROM [Contagem] WITH (NOLOCK) GROUP BY [Contagem_AreaTrabalhoCod] ) T1 WHERE T1.[Contagem_AreaTrabalhoCod] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000468,1,0,true,false )
             ,new CursorDef("T000469", "SELECT [Servico_Codigo] AS AreaTrabalho_ServicoPadrao FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AreaTrabalho_ServicoPadrao ",true, GxErrorMask.GX_NOMASK, false, this,prmT000469,1,0,true,false )
             ,new CursorDef("T000470", "SELECT [Contratante_Fax], [Contratante_Ramal], [Contratante_Telefone], [Contratante_Email], [Contratante_WebSite], [Contratante_IE], [Contratante_NomeFantasia], [Contratante_EmailSdaHost], [Contratante_EmailSdaUser], [Contratante_EmailSdaPass], [Contratante_EmailSdaKey], [Contratante_EmailSdaAut], [Contratante_EmailSdaPort], [Contratante_EmailSdaSec], [Municipio_Codigo], [Contratante_PessoaCod] AS Contratante_PessoaCod FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000470,1,0,true,false )
             ,new CursorDef("T000471", "SELECT [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000471,1,0,true,false )
             ,new CursorDef("T000472", "SELECT [Estado_Nome] FROM [Estado] WITH (NOLOCK) WHERE [Estado_UF] = @Estado_UF ",true, GxErrorMask.GX_NOMASK, false, this,prmT000472,1,0,true,false )
             ,new CursorDef("T000473", "SELECT [Pessoa_Docto] AS Contratante_CNPJ, [Pessoa_Nome] AS Contratante_RazaoSocial FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratante_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000473,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((short[]) buf[14])[0] = rslt.getShort(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((int[]) buf[22])[0] = rslt.getInt(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((short[]) buf[14])[0] = rslt.getShort(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((int[]) buf[22])[0] = rslt.getInt(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 32) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 8 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 20) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 20) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 15) ;
                ((String[]) buf[18])[0] = rslt.getString(13, 100) ;
                ((String[]) buf[19])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((String[]) buf[21])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((String[]) buf[23])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((String[]) buf[25])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((String[]) buf[27])[0] = rslt.getString(18, 32) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(18);
                ((bool[]) buf[29])[0] = rslt.getBool(19) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(19);
                ((short[]) buf[31])[0] = rslt.getShort(20) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(20);
                ((short[]) buf[33])[0] = rslt.getShort(21) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(21);
                ((String[]) buf[35])[0] = rslt.getString(22, 2) ;
                ((bool[]) buf[36])[0] = rslt.getBool(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                ((short[]) buf[38])[0] = rslt.getShort(24) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(24);
                ((int[]) buf[40])[0] = rslt.getInt(25) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(25);
                ((short[]) buf[42])[0] = rslt.getShort(26) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(26);
                ((bool[]) buf[44])[0] = rslt.getBool(27) ;
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(28);
                ((short[]) buf[47])[0] = rslt.getShort(29) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(29);
                ((bool[]) buf[49])[0] = rslt.getBool(30) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(30);
                ((int[]) buf[51])[0] = rslt.getInt(31) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(31);
                ((int[]) buf[53])[0] = rslt.getInt(32) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(32);
                ((int[]) buf[55])[0] = rslt.getInt(33) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(33);
                ((int[]) buf[57])[0] = rslt.getInt(34) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(34);
                ((String[]) buf[59])[0] = rslt.getString(35, 2) ;
                ((int[]) buf[60])[0] = rslt.getInt(36) ;
                ((short[]) buf[61])[0] = rslt.getShort(37) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(37);
                return;
             case 10 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 32) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 24 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 32) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 26 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 27 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 34 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
             case 39 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 45 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 46 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 47 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 48 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 49 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 50 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 51 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 52 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 53 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 54 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 55 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 56 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 57 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 58 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 59 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
       }
       getresults60( cursor, rslt, buf) ;
    }

    public void getresults60( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 60 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                return;
             case 61 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 62 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 63 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 32) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((short[]) buf[21])[0] = rslt.getShort(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                return;
             case 64 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 65 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 66 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[9]);
                }
                stmt.SetParameter(7, (bool)parms[10]);
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[22]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[9]);
                }
                stmt.SetParameter(7, (bool)parms[10]);
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[22]);
                }
                stmt.SetParameter(14, (int)parms[23]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 41 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 42 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 43 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 44 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 45 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 46 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 47 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 48 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 49 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 50 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 51 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 52 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 53 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 54 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 55 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 58 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters60( cursor, stmt, parms) ;
    }

    public void setparameters60( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 60 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 61 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 62 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 63 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 64 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 65 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 66 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
