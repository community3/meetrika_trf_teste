/*
               File: PRC_PontualidadeDemandaReduz
        Description: Indicador Pontualidade Demanda Reduz
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:52.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_pontualidadedemandareduz : GXProcedure
   {
      public prc_pontualidadedemandareduz( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_pontualidadedemandareduz( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                           short aP1_Dias ,
                           decimal aP2_IndP ,
                           ref int aP3_Indicador ,
                           ref String aP4_Indicador_Sigla ,
                           ref short aP5_Faixa ,
                           out decimal aP6_Reduz )
      {
         this.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         this.AV8Dias = aP1_Dias;
         this.AV11IndP = aP2_IndP;
         this.AV14Indicador = aP3_Indicador;
         this.AV19Indicador_Sigla = aP4_Indicador_Sigla;
         this.AV15Faixa = aP5_Faixa;
         this.AV9Reduz = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP3_Indicador=this.AV14Indicador;
         aP4_Indicador_Sigla=this.AV19Indicador_Sigla;
         aP5_Faixa=this.AV15Faixa;
         aP6_Reduz=this.AV9Reduz;
      }

      public decimal executeUdp( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                                 short aP1_Dias ,
                                 decimal aP2_IndP ,
                                 ref int aP3_Indicador ,
                                 ref String aP4_Indicador_Sigla ,
                                 ref short aP5_Faixa )
      {
         this.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         this.AV8Dias = aP1_Dias;
         this.AV11IndP = aP2_IndP;
         this.AV14Indicador = aP3_Indicador;
         this.AV19Indicador_Sigla = aP4_Indicador_Sigla;
         this.AV15Faixa = aP5_Faixa;
         this.AV9Reduz = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP3_Indicador=this.AV14Indicador;
         aP4_Indicador_Sigla=this.AV19Indicador_Sigla;
         aP5_Faixa=this.AV15Faixa;
         aP6_Reduz=this.AV9Reduz;
         return AV9Reduz ;
      }

      public void executeSubmit( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                                 short aP1_Dias ,
                                 decimal aP2_IndP ,
                                 ref int aP3_Indicador ,
                                 ref String aP4_Indicador_Sigla ,
                                 ref short aP5_Faixa ,
                                 out decimal aP6_Reduz )
      {
         prc_pontualidadedemandareduz objprc_pontualidadedemandareduz;
         objprc_pontualidadedemandareduz = new prc_pontualidadedemandareduz();
         objprc_pontualidadedemandareduz.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         objprc_pontualidadedemandareduz.AV8Dias = aP1_Dias;
         objprc_pontualidadedemandareduz.AV11IndP = aP2_IndP;
         objprc_pontualidadedemandareduz.AV14Indicador = aP3_Indicador;
         objprc_pontualidadedemandareduz.AV19Indicador_Sigla = aP4_Indicador_Sigla;
         objprc_pontualidadedemandareduz.AV15Faixa = aP5_Faixa;
         objprc_pontualidadedemandareduz.AV9Reduz = 0 ;
         objprc_pontualidadedemandareduz.context.SetSubmitInitialConfig(context);
         objprc_pontualidadedemandareduz.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_pontualidadedemandareduz);
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP3_Indicador=this.AV14Indicador;
         aP4_Indicador_Sigla=this.AV19Indicador_Sigla;
         aP5_Faixa=this.AV15Faixa;
         aP6_Reduz=this.AV9Reduz;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_pontualidadedemandareduz)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV16Valor = (decimal)(AV8Dias);
         AV17Indice = AV11IndP;
         /* Using cursor P008O2 */
         pr_default.execute(0, new Object[] {A1270ContratoServicosIndicador_CntSrvCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1269ContratoServicosIndicador_Codigo = P008O2_A1269ContratoServicosIndicador_Codigo[0];
            A1345ContratoServicosIndicador_CalculoSob = P008O2_A1345ContratoServicosIndicador_CalculoSob[0];
            n1345ContratoServicosIndicador_CalculoSob = P008O2_n1345ContratoServicosIndicador_CalculoSob[0];
            A1308ContratoServicosIndicador_Tipo = P008O2_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P008O2_n1308ContratoServicosIndicador_Tipo[0];
            A2051ContratoServicosIndicador_Sigla = P008O2_A2051ContratoServicosIndicador_Sigla[0];
            n2051ContratoServicosIndicador_Sigla = P008O2_n2051ContratoServicosIndicador_Sigla[0];
            A2052ContratoServicosIndicador_Formato = P008O2_A2052ContratoServicosIndicador_Formato[0];
            OV15Faixa = AV15Faixa;
            AV14Indicador = A1269ContratoServicosIndicador_Codigo;
            AV19Indicador_Sigla = StringUtil.Trim( A2051ContratoServicosIndicador_Sigla);
            if ( A2052ContratoServicosIndicador_Formato == 0 )
            {
               AV16Valor = (decimal)(AV8Dias*100);
               AV17Indice = (decimal)(AV11IndP*100);
            }
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 A1345ContratoServicosIndicador_CalculoSob ,
                                                 A1301ContratoServicosIndicadorFaixa_Desde ,
                                                 AV16Valor ,
                                                 A1302ContratoServicosIndicadorFaixa_Ate ,
                                                 AV17Indice ,
                                                 A1269ContratoServicosIndicador_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT
                                                 }
            });
            /* Using cursor P008O3 */
            pr_default.execute(1, new Object[] {A1269ContratoServicosIndicador_Codigo, AV16Valor, AV16Valor, AV16Valor, AV17Indice, AV17Indice, AV17Indice});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1302ContratoServicosIndicadorFaixa_Ate = P008O3_A1302ContratoServicosIndicadorFaixa_Ate[0];
               A1301ContratoServicosIndicadorFaixa_Desde = P008O3_A1301ContratoServicosIndicadorFaixa_Desde[0];
               A1300ContratoServicosIndicadorFaixa_Numero = P008O3_A1300ContratoServicosIndicadorFaixa_Numero[0];
               A1303ContratoServicosIndicadorFaixa_Reduz = P008O3_A1303ContratoServicosIndicadorFaixa_Reduz[0];
               A1299ContratoServicosIndicadorFaixa_Codigo = P008O3_A1299ContratoServicosIndicadorFaixa_Codigo[0];
               AV15Faixa = A1300ContratoServicosIndicadorFaixa_Numero;
               AV9Reduz = A1303ContratoServicosIndicadorFaixa_Reduz;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008O2_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P008O2_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P008O2_A1345ContratoServicosIndicador_CalculoSob = new String[] {""} ;
         P008O2_n1345ContratoServicosIndicador_CalculoSob = new bool[] {false} ;
         P008O2_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P008O2_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P008O2_A2051ContratoServicosIndicador_Sigla = new String[] {""} ;
         P008O2_n2051ContratoServicosIndicador_Sigla = new bool[] {false} ;
         P008O2_A2052ContratoServicosIndicador_Formato = new short[1] ;
         A1345ContratoServicosIndicador_CalculoSob = "";
         A1308ContratoServicosIndicador_Tipo = "";
         A2051ContratoServicosIndicador_Sigla = "";
         P008O3_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P008O3_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         P008O3_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         P008O3_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         P008O3_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         P008O3_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_pontualidadedemandareduz__default(),
            new Object[][] {
                new Object[] {
               P008O2_A1270ContratoServicosIndicador_CntSrvCod, P008O2_A1269ContratoServicosIndicador_Codigo, P008O2_A1345ContratoServicosIndicador_CalculoSob, P008O2_n1345ContratoServicosIndicador_CalculoSob, P008O2_A1308ContratoServicosIndicador_Tipo, P008O2_n1308ContratoServicosIndicador_Tipo, P008O2_A2051ContratoServicosIndicador_Sigla, P008O2_n2051ContratoServicosIndicador_Sigla, P008O2_A2052ContratoServicosIndicador_Formato
               }
               , new Object[] {
               P008O3_A1269ContratoServicosIndicador_Codigo, P008O3_A1302ContratoServicosIndicadorFaixa_Ate, P008O3_A1301ContratoServicosIndicadorFaixa_Desde, P008O3_A1300ContratoServicosIndicadorFaixa_Numero, P008O3_A1303ContratoServicosIndicadorFaixa_Reduz, P008O3_A1299ContratoServicosIndicadorFaixa_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8Dias ;
      private short AV15Faixa ;
      private short A2052ContratoServicosIndicador_Formato ;
      private short OV15Faixa ;
      private short A1300ContratoServicosIndicadorFaixa_Numero ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int AV14Indicador ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int A1299ContratoServicosIndicadorFaixa_Codigo ;
      private decimal AV11IndP ;
      private decimal AV9Reduz ;
      private decimal AV16Valor ;
      private decimal AV17Indice ;
      private decimal A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal A1303ContratoServicosIndicadorFaixa_Reduz ;
      private String AV19Indicador_Sigla ;
      private String scmdbuf ;
      private String A1345ContratoServicosIndicador_CalculoSob ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private String A2051ContratoServicosIndicador_Sigla ;
      private bool n1345ContratoServicosIndicador_CalculoSob ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private bool n2051ContratoServicosIndicador_Sigla ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicosIndicador_CntSrvCod ;
      private int aP3_Indicador ;
      private String aP4_Indicador_Sigla ;
      private short aP5_Faixa ;
      private IDataStoreProvider pr_default ;
      private int[] P008O2_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] P008O2_A1269ContratoServicosIndicador_Codigo ;
      private String[] P008O2_A1345ContratoServicosIndicador_CalculoSob ;
      private bool[] P008O2_n1345ContratoServicosIndicador_CalculoSob ;
      private String[] P008O2_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P008O2_n1308ContratoServicosIndicador_Tipo ;
      private String[] P008O2_A2051ContratoServicosIndicador_Sigla ;
      private bool[] P008O2_n2051ContratoServicosIndicador_Sigla ;
      private short[] P008O2_A2052ContratoServicosIndicador_Formato ;
      private int[] P008O3_A1269ContratoServicosIndicador_Codigo ;
      private decimal[] P008O3_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] P008O3_A1301ContratoServicosIndicadorFaixa_Desde ;
      private short[] P008O3_A1300ContratoServicosIndicadorFaixa_Numero ;
      private decimal[] P008O3_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private int[] P008O3_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private decimal aP6_Reduz ;
   }

   public class prc_pontualidadedemandareduz__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P008O3( IGxContext context ,
                                             String A1345ContratoServicosIndicador_CalculoSob ,
                                             decimal A1301ContratoServicosIndicadorFaixa_Desde ,
                                             decimal AV16Valor ,
                                             decimal A1302ContratoServicosIndicadorFaixa_Ate ,
                                             decimal AV17Indice ,
                                             int A1269ContratoServicosIndicador_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Numero], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo)";
         if ( StringUtil.StrCmp(A1345ContratoServicosIndicador_CalculoSob, "D") == 0 )
         {
            sWhereString = sWhereString + " and (( [ContratoServicosIndicadorFaixa_Desde] <= @AV16Valor and [ContratoServicosIndicadorFaixa_Ate] >= @AV16Valor) or ( [ContratoServicosIndicadorFaixa_Desde] <= @AV16Valor and ([ContratoServicosIndicadorFaixa_Ate] = convert(int, 0))))";
         }
         else
         {
            GXv_int1[1] = 1;
            GXv_int1[2] = 1;
            GXv_int1[3] = 1;
         }
         if ( StringUtil.StrCmp(A1345ContratoServicosIndicador_CalculoSob, "P") == 0 )
         {
            sWhereString = sWhereString + " and (( [ContratoServicosIndicadorFaixa_Desde] <= @AV17Indice and [ContratoServicosIndicadorFaixa_Ate] >= @AV17Indice) or ( [ContratoServicosIndicadorFaixa_Desde] <= @AV17Indice and ([ContratoServicosIndicadorFaixa_Ate] = convert(int, 0))))";
         }
         else
         {
            GXv_int1[4] = 1;
            GXv_int1[5] = 1;
            GXv_int1[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContratoServicosIndicador_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P008O3(context, (String)dynConstraints[0] , (decimal)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (decimal)dynConstraints[4] , (int)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008O2 ;
          prmP008O2 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008O3 ;
          prmP008O3 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV16Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV16Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV17Indice",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV17Indice",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV17Indice",SqlDbType.Decimal,6,2}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008O2", "SELECT TOP 1 [ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Codigo], [ContratoServicosIndicador_CalculoSob], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_Sigla], [ContratoServicosIndicador_Formato] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE ([ContratoServicosIndicador_CntSrvCod] = @ContratoServicosIndicador_CntSrvCod) AND ([ContratoServicosIndicador_Tipo] = 'P') ORDER BY [ContratoServicosIndicador_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008O2,1,0,true,true )
             ,new CursorDef("P008O3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008O3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 2) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[13]);
                }
                return;
       }
    }

 }

}
