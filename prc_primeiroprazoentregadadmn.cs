/*
               File: PRC_PrimeiroPrazoEntregaDaDmn
        Description: Prazo Inicial
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:43.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_primeiroprazoentregadadmn : GXProcedure
   {
      public prc_primeiroprazoentregadadmn( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_primeiroprazoentregadadmn( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_LogResponsavel_DemandaCod ,
                           out DateTime aP1_PrazoInicial )
      {
         this.A892LogResponsavel_DemandaCod = aP0_LogResponsavel_DemandaCod;
         this.AV8PrazoInicial = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP0_LogResponsavel_DemandaCod=this.A892LogResponsavel_DemandaCod;
         aP1_PrazoInicial=this.AV8PrazoInicial;
      }

      public DateTime executeUdp( ref int aP0_LogResponsavel_DemandaCod )
      {
         this.A892LogResponsavel_DemandaCod = aP0_LogResponsavel_DemandaCod;
         this.AV8PrazoInicial = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP0_LogResponsavel_DemandaCod=this.A892LogResponsavel_DemandaCod;
         aP1_PrazoInicial=this.AV8PrazoInicial;
         return AV8PrazoInicial ;
      }

      public void executeSubmit( ref int aP0_LogResponsavel_DemandaCod ,
                                 out DateTime aP1_PrazoInicial )
      {
         prc_primeiroprazoentregadadmn objprc_primeiroprazoentregadadmn;
         objprc_primeiroprazoentregadadmn = new prc_primeiroprazoentregadadmn();
         objprc_primeiroprazoentregadadmn.A892LogResponsavel_DemandaCod = aP0_LogResponsavel_DemandaCod;
         objprc_primeiroprazoentregadadmn.AV8PrazoInicial = DateTime.MinValue ;
         objprc_primeiroprazoentregadadmn.context.SetSubmitInitialConfig(context);
         objprc_primeiroprazoentregadadmn.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_primeiroprazoentregadadmn);
         aP0_LogResponsavel_DemandaCod=this.A892LogResponsavel_DemandaCod;
         aP1_PrazoInicial=this.AV8PrazoInicial;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_primeiroprazoentregadadmn)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P007Q2 */
         pr_default.execute(0, new Object[] {n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1177LogResponsavel_Prazo = P007Q2_A1177LogResponsavel_Prazo[0];
            n1177LogResponsavel_Prazo = P007Q2_n1177LogResponsavel_Prazo[0];
            A1797LogResponsavel_Codigo = P007Q2_A1797LogResponsavel_Codigo[0];
            AV8PrazoInicial = A1177LogResponsavel_Prazo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P007Q2_A892LogResponsavel_DemandaCod = new int[1] ;
         P007Q2_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P007Q2_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         P007Q2_n1177LogResponsavel_Prazo = new bool[] {false} ;
         P007Q2_A1797LogResponsavel_Codigo = new long[1] ;
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_primeiroprazoentregadadmn__default(),
            new Object[][] {
                new Object[] {
               P007Q2_A892LogResponsavel_DemandaCod, P007Q2_n892LogResponsavel_DemandaCod, P007Q2_A1177LogResponsavel_Prazo, P007Q2_n1177LogResponsavel_Prazo, P007Q2_A1797LogResponsavel_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A892LogResponsavel_DemandaCod ;
      private long A1797LogResponsavel_Codigo ;
      private String scmdbuf ;
      private DateTime AV8PrazoInicial ;
      private DateTime A1177LogResponsavel_Prazo ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1177LogResponsavel_Prazo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_LogResponsavel_DemandaCod ;
      private IDataStoreProvider pr_default ;
      private int[] P007Q2_A892LogResponsavel_DemandaCod ;
      private bool[] P007Q2_n892LogResponsavel_DemandaCod ;
      private DateTime[] P007Q2_A1177LogResponsavel_Prazo ;
      private bool[] P007Q2_n1177LogResponsavel_Prazo ;
      private long[] P007Q2_A1797LogResponsavel_Codigo ;
      private DateTime aP1_PrazoInicial ;
   }

   public class prc_primeiroprazoentregadadmn__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP007Q2 ;
          prmP007Q2 = new Object[] {
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P007Q2", "SELECT TOP 1 [LogResponsavel_DemandaCod], [LogResponsavel_Prazo], [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @LogResponsavel_DemandaCod ORDER BY [LogResponsavel_DemandaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007Q2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((long[]) buf[4])[0] = rslt.getLong(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
