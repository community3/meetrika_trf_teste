/*
               File: GetWWContagemResultadoEvidenciaFilterData
        Description: Get WWContagem Resultado Evidencia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:57.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontagemresultadoevidenciafilterdata : GXProcedure
   {
      public getwwcontagemresultadoevidenciafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontagemresultadoevidenciafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontagemresultadoevidenciafilterdata objgetwwcontagemresultadoevidenciafilterdata;
         objgetwwcontagemresultadoevidenciafilterdata = new getwwcontagemresultadoevidenciafilterdata();
         objgetwwcontagemresultadoevidenciafilterdata.AV18DDOName = aP0_DDOName;
         objgetwwcontagemresultadoevidenciafilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwcontagemresultadoevidenciafilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontagemresultadoevidenciafilterdata.AV22OptionsJson = "" ;
         objgetwwcontagemresultadoevidenciafilterdata.AV25OptionsDescJson = "" ;
         objgetwwcontagemresultadoevidenciafilterdata.AV27OptionIndexesJson = "" ;
         objgetwwcontagemresultadoevidenciafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontagemresultadoevidenciafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontagemresultadoevidenciafilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontagemresultadoevidenciafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADOEVIDENCIA_NOMEARQOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADOEVIDENCIA_TIPOARQOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWContagemResultadoEvidenciaGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContagemResultadoEvidenciaGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWContagemResultadoEvidenciaGridState"), "");
         }
         AV44GXV1 = 1;
         while ( AV44GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV44GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
            {
               AV10TFContagemResultadoEvidencia_NomeArq = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL") == 0 )
            {
               AV11TFContagemResultadoEvidencia_NomeArq_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ") == 0 )
            {
               AV12TFContagemResultadoEvidencia_TipoArq = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL") == 0 )
            {
               AV13TFContagemResultadoEvidencia_TipoArq_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEVIDENCIA_DATA") == 0 )
            {
               AV14TFContagemResultadoEvidencia_Data = context.localUtil.CToT( AV32GridStateFilterValue.gxTpr_Value, 2);
               AV15TFContagemResultadoEvidencia_Data_To = context.localUtil.CToT( AV32GridStateFilterValue.gxTpr_Valueto, 2);
            }
            AV44GXV1 = (int)(AV44GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
            {
               AV35ContagemResultadoEvidencia_NomeArq1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
               {
                  AV38ContagemResultadoEvidencia_NomeArq2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV39DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV40DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
                  {
                     AV41ContagemResultadoEvidencia_NomeArq3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADOEVIDENCIA_NOMEARQOPTIONS' Routine */
         AV10TFContagemResultadoEvidencia_NomeArq = AV16SearchTxt;
         AV11TFContagemResultadoEvidencia_NomeArq_Sel = "";
         AV46WWContagemResultadoEvidenciaDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 = AV35ContagemResultadoEvidencia_NomeArq1;
         AV48WWContagemResultadoEvidenciaDS_3_Dynamicfiltersenabled2 = AV36DynamicFiltersEnabled2;
         AV49WWContagemResultadoEvidenciaDS_4_Dynamicfiltersselector2 = AV37DynamicFiltersSelector2;
         AV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 = AV38ContagemResultadoEvidencia_NomeArq2;
         AV51WWContagemResultadoEvidenciaDS_6_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV52WWContagemResultadoEvidenciaDS_7_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 = AV41ContagemResultadoEvidencia_NomeArq3;
         AV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq = AV10TFContagemResultadoEvidencia_NomeArq;
         AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel = AV11TFContagemResultadoEvidencia_NomeArq_Sel;
         AV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq = AV12TFContagemResultadoEvidencia_TipoArq;
         AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel = AV13TFContagemResultadoEvidencia_TipoArq_Sel;
         AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data = AV14TFContagemResultadoEvidencia_Data;
         AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to = AV15TFContagemResultadoEvidencia_Data_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV46WWContagemResultadoEvidenciaDS_1_Dynamicfiltersselector1 ,
                                              AV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 ,
                                              AV48WWContagemResultadoEvidenciaDS_3_Dynamicfiltersenabled2 ,
                                              AV49WWContagemResultadoEvidenciaDS_4_Dynamicfiltersselector2 ,
                                              AV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 ,
                                              AV51WWContagemResultadoEvidenciaDS_6_Dynamicfiltersenabled3 ,
                                              AV52WWContagemResultadoEvidenciaDS_7_Dynamicfiltersselector3 ,
                                              AV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 ,
                                              AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel ,
                                              AV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq ,
                                              AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel ,
                                              AV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq ,
                                              AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data ,
                                              AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to ,
                                              A589ContagemResultadoEvidencia_NomeArq ,
                                              A590ContagemResultadoEvidencia_TipoArq ,
                                              A591ContagemResultadoEvidencia_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN
                                              }
         });
         lV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 = StringUtil.PadR( StringUtil.RTrim( AV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1), 50, "%");
         lV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 = StringUtil.PadR( StringUtil.RTrim( AV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2), 50, "%");
         lV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 = StringUtil.PadR( StringUtil.RTrim( AV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3), 50, "%");
         lV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq = StringUtil.PadR( StringUtil.RTrim( AV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq), 50, "%");
         lV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq), 10, "%");
         /* Using cursor P00MV2 */
         pr_default.execute(0, new Object[] {lV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1, lV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2, lV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3, lV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq, AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel, lV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq, AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel, AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data, AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKMV2 = false;
            A589ContagemResultadoEvidencia_NomeArq = P00MV2_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = P00MV2_n589ContagemResultadoEvidencia_NomeArq[0];
            A591ContagemResultadoEvidencia_Data = P00MV2_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = P00MV2_n591ContagemResultadoEvidencia_Data[0];
            A590ContagemResultadoEvidencia_TipoArq = P00MV2_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = P00MV2_n590ContagemResultadoEvidencia_TipoArq[0];
            A586ContagemResultadoEvidencia_Codigo = P00MV2_A586ContagemResultadoEvidencia_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00MV2_A589ContagemResultadoEvidencia_NomeArq[0], A589ContagemResultadoEvidencia_NomeArq) == 0 ) )
            {
               BRKMV2 = false;
               A586ContagemResultadoEvidencia_Codigo = P00MV2_A586ContagemResultadoEvidencia_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKMV2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq)) )
            {
               AV20Option = A589ContagemResultadoEvidencia_NomeArq;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKMV2 )
            {
               BRKMV2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEMRESULTADOEVIDENCIA_TIPOARQOPTIONS' Routine */
         AV12TFContagemResultadoEvidencia_TipoArq = AV16SearchTxt;
         AV13TFContagemResultadoEvidencia_TipoArq_Sel = "";
         AV46WWContagemResultadoEvidenciaDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 = AV35ContagemResultadoEvidencia_NomeArq1;
         AV48WWContagemResultadoEvidenciaDS_3_Dynamicfiltersenabled2 = AV36DynamicFiltersEnabled2;
         AV49WWContagemResultadoEvidenciaDS_4_Dynamicfiltersselector2 = AV37DynamicFiltersSelector2;
         AV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 = AV38ContagemResultadoEvidencia_NomeArq2;
         AV51WWContagemResultadoEvidenciaDS_6_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV52WWContagemResultadoEvidenciaDS_7_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 = AV41ContagemResultadoEvidencia_NomeArq3;
         AV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq = AV10TFContagemResultadoEvidencia_NomeArq;
         AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel = AV11TFContagemResultadoEvidencia_NomeArq_Sel;
         AV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq = AV12TFContagemResultadoEvidencia_TipoArq;
         AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel = AV13TFContagemResultadoEvidencia_TipoArq_Sel;
         AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data = AV14TFContagemResultadoEvidencia_Data;
         AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to = AV15TFContagemResultadoEvidencia_Data_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV46WWContagemResultadoEvidenciaDS_1_Dynamicfiltersselector1 ,
                                              AV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 ,
                                              AV48WWContagemResultadoEvidenciaDS_3_Dynamicfiltersenabled2 ,
                                              AV49WWContagemResultadoEvidenciaDS_4_Dynamicfiltersselector2 ,
                                              AV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 ,
                                              AV51WWContagemResultadoEvidenciaDS_6_Dynamicfiltersenabled3 ,
                                              AV52WWContagemResultadoEvidenciaDS_7_Dynamicfiltersselector3 ,
                                              AV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 ,
                                              AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel ,
                                              AV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq ,
                                              AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel ,
                                              AV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq ,
                                              AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data ,
                                              AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to ,
                                              A589ContagemResultadoEvidencia_NomeArq ,
                                              A590ContagemResultadoEvidencia_TipoArq ,
                                              A591ContagemResultadoEvidencia_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN
                                              }
         });
         lV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 = StringUtil.PadR( StringUtil.RTrim( AV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1), 50, "%");
         lV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 = StringUtil.PadR( StringUtil.RTrim( AV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2), 50, "%");
         lV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 = StringUtil.PadR( StringUtil.RTrim( AV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3), 50, "%");
         lV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq = StringUtil.PadR( StringUtil.RTrim( AV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq), 50, "%");
         lV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq), 10, "%");
         /* Using cursor P00MV3 */
         pr_default.execute(1, new Object[] {lV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1, lV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2, lV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3, lV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq, AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel, lV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq, AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel, AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data, AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKMV4 = false;
            A590ContagemResultadoEvidencia_TipoArq = P00MV3_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = P00MV3_n590ContagemResultadoEvidencia_TipoArq[0];
            A591ContagemResultadoEvidencia_Data = P00MV3_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = P00MV3_n591ContagemResultadoEvidencia_Data[0];
            A589ContagemResultadoEvidencia_NomeArq = P00MV3_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = P00MV3_n589ContagemResultadoEvidencia_NomeArq[0];
            A586ContagemResultadoEvidencia_Codigo = P00MV3_A586ContagemResultadoEvidencia_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00MV3_A590ContagemResultadoEvidencia_TipoArq[0], A590ContagemResultadoEvidencia_TipoArq) == 0 ) )
            {
               BRKMV4 = false;
               A586ContagemResultadoEvidencia_Codigo = P00MV3_A586ContagemResultadoEvidencia_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKMV4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A590ContagemResultadoEvidencia_TipoArq)) )
            {
               AV20Option = A590ContagemResultadoEvidencia_TipoArq;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKMV4 )
            {
               BRKMV4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContagemResultadoEvidencia_NomeArq = "";
         AV11TFContagemResultadoEvidencia_NomeArq_Sel = "";
         AV12TFContagemResultadoEvidencia_TipoArq = "";
         AV13TFContagemResultadoEvidencia_TipoArq_Sel = "";
         AV14TFContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV15TFContagemResultadoEvidencia_Data_To = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV35ContagemResultadoEvidencia_NomeArq1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV38ContagemResultadoEvidencia_NomeArq2 = "";
         AV40DynamicFiltersSelector3 = "";
         AV41ContagemResultadoEvidencia_NomeArq3 = "";
         AV46WWContagemResultadoEvidenciaDS_1_Dynamicfiltersselector1 = "";
         AV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 = "";
         AV49WWContagemResultadoEvidenciaDS_4_Dynamicfiltersselector2 = "";
         AV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 = "";
         AV52WWContagemResultadoEvidenciaDS_7_Dynamicfiltersselector3 = "";
         AV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 = "";
         AV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq = "";
         AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel = "";
         AV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq = "";
         AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel = "";
         AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data = (DateTime)(DateTime.MinValue);
         AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         lV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 = "";
         lV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 = "";
         lV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 = "";
         lV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq = "";
         lV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq = "";
         A589ContagemResultadoEvidencia_NomeArq = "";
         A590ContagemResultadoEvidencia_TipoArq = "";
         A591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         P00MV2_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         P00MV2_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         P00MV2_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00MV2_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         P00MV2_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         P00MV2_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         P00MV2_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         AV20Option = "";
         P00MV3_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         P00MV3_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         P00MV3_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00MV3_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         P00MV3_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         P00MV3_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         P00MV3_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontagemresultadoevidenciafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00MV2_A589ContagemResultadoEvidencia_NomeArq, P00MV2_n589ContagemResultadoEvidencia_NomeArq, P00MV2_A591ContagemResultadoEvidencia_Data, P00MV2_n591ContagemResultadoEvidencia_Data, P00MV2_A590ContagemResultadoEvidencia_TipoArq, P00MV2_n590ContagemResultadoEvidencia_TipoArq, P00MV2_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               P00MV3_A590ContagemResultadoEvidencia_TipoArq, P00MV3_n590ContagemResultadoEvidencia_TipoArq, P00MV3_A591ContagemResultadoEvidencia_Data, P00MV3_n591ContagemResultadoEvidencia_Data, P00MV3_A589ContagemResultadoEvidencia_NomeArq, P00MV3_n589ContagemResultadoEvidencia_NomeArq, P00MV3_A586ContagemResultadoEvidencia_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV44GXV1 ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private long AV28count ;
      private String AV10TFContagemResultadoEvidencia_NomeArq ;
      private String AV11TFContagemResultadoEvidencia_NomeArq_Sel ;
      private String AV12TFContagemResultadoEvidencia_TipoArq ;
      private String AV13TFContagemResultadoEvidencia_TipoArq_Sel ;
      private String AV35ContagemResultadoEvidencia_NomeArq1 ;
      private String AV38ContagemResultadoEvidencia_NomeArq2 ;
      private String AV41ContagemResultadoEvidencia_NomeArq3 ;
      private String AV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 ;
      private String AV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 ;
      private String AV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 ;
      private String AV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq ;
      private String AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel ;
      private String AV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq ;
      private String AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel ;
      private String scmdbuf ;
      private String lV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 ;
      private String lV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 ;
      private String lV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 ;
      private String lV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq ;
      private String lV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private DateTime AV14TFContagemResultadoEvidencia_Data ;
      private DateTime AV15TFContagemResultadoEvidencia_Data_To ;
      private DateTime AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data ;
      private DateTime AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to ;
      private DateTime A591ContagemResultadoEvidencia_Data ;
      private bool returnInSub ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV39DynamicFiltersEnabled3 ;
      private bool AV48WWContagemResultadoEvidenciaDS_3_Dynamicfiltersenabled2 ;
      private bool AV51WWContagemResultadoEvidenciaDS_6_Dynamicfiltersenabled3 ;
      private bool BRKMV2 ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n591ContagemResultadoEvidencia_Data ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool BRKMV4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV40DynamicFiltersSelector3 ;
      private String AV46WWContagemResultadoEvidenciaDS_1_Dynamicfiltersselector1 ;
      private String AV49WWContagemResultadoEvidenciaDS_4_Dynamicfiltersselector2 ;
      private String AV52WWContagemResultadoEvidenciaDS_7_Dynamicfiltersselector3 ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00MV2_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] P00MV2_n589ContagemResultadoEvidencia_NomeArq ;
      private DateTime[] P00MV2_A591ContagemResultadoEvidencia_Data ;
      private bool[] P00MV2_n591ContagemResultadoEvidencia_Data ;
      private String[] P00MV2_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] P00MV2_n590ContagemResultadoEvidencia_TipoArq ;
      private int[] P00MV2_A586ContagemResultadoEvidencia_Codigo ;
      private String[] P00MV3_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] P00MV3_n590ContagemResultadoEvidencia_TipoArq ;
      private DateTime[] P00MV3_A591ContagemResultadoEvidencia_Data ;
      private bool[] P00MV3_n591ContagemResultadoEvidencia_Data ;
      private String[] P00MV3_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] P00MV3_n589ContagemResultadoEvidencia_NomeArq ;
      private int[] P00MV3_A586ContagemResultadoEvidencia_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getwwcontagemresultadoevidenciafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00MV2( IGxContext context ,
                                             String AV46WWContagemResultadoEvidenciaDS_1_Dynamicfiltersselector1 ,
                                             String AV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 ,
                                             bool AV48WWContagemResultadoEvidenciaDS_3_Dynamicfiltersenabled2 ,
                                             String AV49WWContagemResultadoEvidenciaDS_4_Dynamicfiltersselector2 ,
                                             String AV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 ,
                                             bool AV51WWContagemResultadoEvidenciaDS_6_Dynamicfiltersenabled3 ,
                                             String AV52WWContagemResultadoEvidenciaDS_7_Dynamicfiltersselector3 ,
                                             String AV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 ,
                                             String AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel ,
                                             String AV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq ,
                                             String AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel ,
                                             String AV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq ,
                                             DateTime AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data ,
                                             DateTime AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to ,
                                             String A589ContagemResultadoEvidencia_NomeArq ,
                                             String A590ContagemResultadoEvidencia_TipoArq ,
                                             DateTime A591ContagemResultadoEvidencia_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [9] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV46WWContagemResultadoEvidenciaDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV48WWContagemResultadoEvidenciaDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV49WWContagemResultadoEvidenciaDS_4_Dynamicfiltersselector2, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV51WWContagemResultadoEvidenciaDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV52WWContagemResultadoEvidenciaDS_7_Dynamicfiltersselector3, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like @lV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like @lV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] = @AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] = @AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_TipoArq] like @lV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_TipoArq] like @lV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_TipoArq] = @AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_TipoArq] = @AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (DateTime.MinValue==AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Data] >= @AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Data] >= @AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (DateTime.MinValue==AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Data] <= @AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Data] <= @AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultadoEvidencia_NomeArq]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00MV3( IGxContext context ,
                                             String AV46WWContagemResultadoEvidenciaDS_1_Dynamicfiltersselector1 ,
                                             String AV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 ,
                                             bool AV48WWContagemResultadoEvidenciaDS_3_Dynamicfiltersenabled2 ,
                                             String AV49WWContagemResultadoEvidenciaDS_4_Dynamicfiltersselector2 ,
                                             String AV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 ,
                                             bool AV51WWContagemResultadoEvidenciaDS_6_Dynamicfiltersenabled3 ,
                                             String AV52WWContagemResultadoEvidenciaDS_7_Dynamicfiltersselector3 ,
                                             String AV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 ,
                                             String AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel ,
                                             String AV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq ,
                                             String AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel ,
                                             String AV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq ,
                                             DateTime AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data ,
                                             DateTime AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to ,
                                             String A589ContagemResultadoEvidencia_NomeArq ,
                                             String A590ContagemResultadoEvidencia_TipoArq ,
                                             DateTime A591ContagemResultadoEvidencia_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [9] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV46WWContagemResultadoEvidenciaDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( AV48WWContagemResultadoEvidenciaDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV49WWContagemResultadoEvidenciaDS_4_Dynamicfiltersselector2, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV51WWContagemResultadoEvidenciaDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV52WWContagemResultadoEvidenciaDS_7_Dynamicfiltersselector3, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3 + '%')";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like @lV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like @lV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] = @AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] = @AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_TipoArq] like @lV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_TipoArq] like @lV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_TipoArq] = @AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_TipoArq] = @AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (DateTime.MinValue==AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Data] >= @AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Data] >= @AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (DateTime.MinValue==AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Data] <= @AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Data] <= @AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultadoEvidencia_TipoArq]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00MV2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (DateTime)dynConstraints[16] );
               case 1 :
                     return conditional_P00MV3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (DateTime)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00MV2 ;
          prmP00MV2 = new Object[] {
          new Object[] {"@lV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00MV3 ;
          prmP00MV3 = new Object[] {
          new Object[] {"@lV47WWContagemResultadoEvidenciaDS_2_Contagemresultadoevidencia_nomearq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWContagemResultadoEvidenciaDS_5_Contagemresultadoevidencia_nomearq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWContagemResultadoEvidenciaDS_8_Contagemresultadoevidencia_nomearq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWContagemResultadoEvidenciaDS_9_Tfcontagemresultadoevidencia_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWContagemResultadoEvidenciaDS_10_Tfcontagemresultadoevidencia_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56WWContagemResultadoEvidenciaDS_11_Tfcontagemresultadoevidencia_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV57WWContagemResultadoEvidenciaDS_12_Tfcontagemresultadoevidencia_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV58WWContagemResultadoEvidenciaDS_13_Tfcontagemresultadoevidencia_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV59WWContagemResultadoEvidenciaDS_14_Tfcontagemresultadoevidencia_data_to",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00MV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MV2,100,0,true,false )
             ,new CursorDef("P00MV3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MV3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[17]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[17]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontagemresultadoevidenciafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontagemresultadoevidenciafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontagemresultadoevidenciafilterdata") )
          {
             return  ;
          }
          getwwcontagemresultadoevidenciafilterdata worker = new getwwcontagemresultadoevidenciafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
