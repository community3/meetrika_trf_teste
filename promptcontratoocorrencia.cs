/*
               File: PromptContratoOcorrencia
        Description: Selecione Contrato Ocorr�ncia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 2:23:45.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratoocorrencia : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratoocorrencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratoocorrencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratoOcorrencia_Codigo ,
                           ref DateTime aP1_InOutContratoOcorrencia_Data )
      {
         this.AV7InOutContratoOcorrencia_Codigo = aP0_InOutContratoOcorrencia_Codigo;
         this.AV8InOutContratoOcorrencia_Data = aP1_InOutContratoOcorrencia_Data;
         executePrivate();
         aP0_InOutContratoOcorrencia_Codigo=this.AV7InOutContratoOcorrencia_Codigo;
         aP1_InOutContratoOcorrencia_Data=this.AV8InOutContratoOcorrencia_Data;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_79 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_79_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_79_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV30DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30DynamicFiltersOperator1), 4, 0)));
               AV16ContratoOcorrencia_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoOcorrencia_Data1", context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"));
               AV17ContratoOcorrencia_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrencia_Data_To1", context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"));
               AV31ContratoOcorrencia_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrencia_Descricao1", AV31ContratoOcorrencia_Descricao1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV32DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator2), 4, 0)));
               AV20ContratoOcorrencia_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoOcorrencia_Data2", context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"));
               AV21ContratoOcorrencia_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoOcorrencia_Data_To2", context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"));
               AV33ContratoOcorrencia_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContratoOcorrencia_Descricao2", AV33ContratoOcorrencia_Descricao2);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV35TFContrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Numero", AV35TFContrato_Numero);
               AV36TFContrato_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Numero_Sel", AV36TFContrato_Numero_Sel);
               AV39TFContratoOcorrencia_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoOcorrencia_Data", context.localUtil.Format(AV39TFContratoOcorrencia_Data, "99/99/99"));
               AV40TFContratoOcorrencia_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoOcorrencia_Data_To", context.localUtil.Format(AV40TFContratoOcorrencia_Data_To, "99/99/99"));
               AV45TFContratoOcorrencia_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoOcorrencia_Descricao", AV45TFContratoOcorrencia_Descricao);
               AV46TFContratoOcorrencia_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContratoOcorrencia_Descricao_Sel", AV46TFContratoOcorrencia_Descricao_Sel);
               AV37ddo_Contrato_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Contrato_NumeroTitleControlIdToReplace", AV37ddo_Contrato_NumeroTitleControlIdToReplace);
               AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace", AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace);
               AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace", AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace);
               AV55Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV30DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV31ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV32DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV33ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV35TFContrato_Numero, AV36TFContrato_Numero_Sel, AV39TFContratoOcorrencia_Data, AV40TFContratoOcorrencia_Data_To, AV45TFContratoOcorrencia_Descricao, AV46TFContratoOcorrencia_Descricao_Sel, AV37ddo_Contrato_NumeroTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PromptContratoOcorrencia";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("promptcontratoocorrencia:[SendSecurityCheck value for]"+"ContratoOcorrencia_Codigo:"+context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratoOcorrencia_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoOcorrencia_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContratoOcorrencia_Data = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoOcorrencia_Data", context.localUtil.Format(AV8InOutContratoOcorrencia_Data, "99/99/99"));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA782( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV55Pgmname = "PromptContratoOcorrencia";
               context.Gx_err = 0;
               WS782( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE782( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020429223462");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratoocorrencia.aspx") + "?" + UrlEncode("" +AV7InOutContratoOcorrencia_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV8InOutContratoOcorrencia_Data))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIA_DATA1", context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIA_DATA_TO1", context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIA_DESCRICAO1", AV31ContratoOcorrencia_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIA_DATA2", context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIA_DATA_TO2", context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIA_DESCRICAO2", AV33ContratoOcorrencia_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO", StringUtil.RTrim( AV35TFContrato_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO_SEL", StringUtil.RTrim( AV36TFContrato_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIA_DATA", context.localUtil.Format(AV39TFContratoOcorrencia_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIA_DATA_TO", context.localUtil.Format(AV40TFContratoOcorrencia_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIA_DESCRICAO", AV45TFContratoOcorrencia_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIA_DESCRICAO_SEL", AV46TFContratoOcorrencia_Descricao_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_79", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_79), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV48DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV48DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_NUMEROTITLEFILTERDATA", AV34Contrato_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_NUMEROTITLEFILTERDATA", AV34Contrato_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIA_DATATITLEFILTERDATA", AV38ContratoOcorrencia_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIA_DATATITLEFILTERDATA", AV38ContratoOcorrencia_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIA_DESCRICAOTITLEFILTERDATA", AV44ContratoOcorrencia_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIA_DESCRICAOTITLEFILTERDATA", AV44ContratoOcorrencia_DescricaoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV55Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOOCORRENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratoOcorrencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOOCORRENCIA_DATA", context.localUtil.DToC( AV8InOutContratoOcorrencia_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Caption", StringUtil.RTrim( Ddo_contrato_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contrato_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cls", StringUtil.RTrim( Ddo_contrato_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contrato_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_contrato_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contrato_numero_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_contrato_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contrato_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contrato_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_contrato_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Rangefilterfrom", StringUtil.RTrim( Ddo_contrato_numero_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Rangefilterto", StringUtil.RTrim( Ddo_contrato_numero_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Caption", StringUtil.RTrim( Ddo_contratoocorrencia_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencia_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Cls", StringUtil.RTrim( Ddo_contratoocorrencia_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencia_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencia_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencia_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencia_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencia_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencia_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencia_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencia_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencia_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencia_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencia_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoocorrencia_data_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencia_data_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencia_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencia_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencia_data_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencia_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencia_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencia_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencia_data_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencia_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Caption", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Cls", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencia_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencia_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencia_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencia_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencia_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencia_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencia_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencia_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencia_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PromptContratoOcorrencia";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("promptcontratoocorrencia:[SendSecurityCheck value for]"+"ContratoOcorrencia_Codigo:"+context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm782( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratoOcorrencia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contrato Ocorr�ncia" ;
      }

      protected void WB780( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_782( true) ;
         }
         else
         {
            wb_table1_2_782( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_782e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrencia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrencia_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoOcorrencia_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_PromptContratoOcorrencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(89, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_Internalname, StringUtil.RTrim( AV35TFContrato_Numero), StringUtil.RTrim( context.localUtil.Format( AV35TFContrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_sel_Internalname, StringUtil.RTrim( AV36TFContrato_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV36TFContrato_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_79_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencia_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_data_Internalname, context.localUtil.Format(AV39TFContratoOcorrencia_Data, "99/99/99"), context.localUtil.Format( AV39TFContratoOcorrencia_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencia_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencia_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_79_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencia_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_data_to_Internalname, context.localUtil.Format(AV40TFContratoOcorrencia_Data_To, "99/99/99"), context.localUtil.Format( AV40TFContratoOcorrencia_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencia_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencia_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratoocorrencia_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_79_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencia_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencia_dataauxdate_Internalname, context.localUtil.Format(AV41DDO_ContratoOcorrencia_DataAuxDate, "99/99/99"), context.localUtil.Format( AV41DDO_ContratoOcorrencia_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencia_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencia_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_79_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencia_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencia_dataauxdateto_Internalname, context.localUtil.Format(AV42DDO_ContratoOcorrencia_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV42DDO_ContratoOcorrencia_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencia_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencia_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_descricao_Internalname, AV45TFContratoOcorrencia_Descricao, StringUtil.RTrim( context.localUtil.Format( AV45TFContratoOcorrencia_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_descricao_sel_Internalname, AV46TFContratoOcorrencia_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV46TFContratoOcorrencia_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, AV37ddo_Contrato_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", 0, edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIA_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Internalname, AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", 0, edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIA_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_79_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Internalname, AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", 0, edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrencia.htm");
         }
         wbLoad = true;
      }

      protected void START782( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contrato Ocorr�ncia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP780( ) ;
      }

      protected void WS782( )
      {
         START782( ) ;
         EVT782( ) ;
      }

      protected void EVT782( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11782 */
                           E11782 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_NUMERO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12782 */
                           E12782 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIA_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13782 */
                           E13782 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIA_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14782 */
                           E14782 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15782 */
                           E15782 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16782 */
                           E16782 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17782 */
                           E17782 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18782 */
                           E18782 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19782 */
                           E19782 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20782 */
                           E20782 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21782 */
                           E21782 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_79_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
                           SubsflControlProps_792( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV54Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                           A295ContratoOcorrencia_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoOcorrencia_Data_Internalname), 0));
                           A296ContratoOcorrencia_Descricao = StringUtil.Upper( cgiGet( edtContratoOcorrencia_Descricao_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E22782 */
                                 E22782 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23782 */
                                 E23782 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E24782 */
                                 E24782 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV30DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencia_data1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA1"), 0) != AV16ContratoOcorrencia_Data1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencia_data_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA_TO1"), 0) != AV17ContratoOcorrencia_Data_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencia_descricao1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIA_DESCRICAO1"), AV31ContratoOcorrencia_Descricao1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV32DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencia_data2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA2"), 0) != AV20ContratoOcorrencia_Data2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencia_data_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA_TO2"), 0) != AV21ContratoOcorrencia_Data_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencia_descricao2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIA_DESCRICAO2"), AV33ContratoOcorrencia_Descricao2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_numero Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV35TFContrato_Numero) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_numero_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV36TFContrato_Numero_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencia_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DATA"), 0) != AV39TFContratoOcorrencia_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencia_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DATA_TO"), 0) != AV40TFContratoOcorrencia_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencia_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DESCRICAO"), AV45TFContratoOcorrencia_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencia_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DESCRICAO_SEL"), AV46TFContratoOcorrencia_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E25782 */
                                       E25782 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE782( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm782( ) ;
            }
         }
      }

      protected void PA782( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOOCORRENCIA_DATA", "Data", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATOOCORRENCIA_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV30DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV30DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOOCORRENCIA_DATA", "Data", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATOOCORRENCIA_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV32DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator2), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_792( ) ;
         while ( nGXsfl_79_idx <= nRC_GXsfl_79 )
         {
            sendrow_792( ) ;
            nGXsfl_79_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_79_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_79_idx+1));
            sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
            SubsflControlProps_792( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV30DynamicFiltersOperator1 ,
                                       DateTime AV16ContratoOcorrencia_Data1 ,
                                       DateTime AV17ContratoOcorrencia_Data_To1 ,
                                       String AV31ContratoOcorrencia_Descricao1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV32DynamicFiltersOperator2 ,
                                       DateTime AV20ContratoOcorrencia_Data2 ,
                                       DateTime AV21ContratoOcorrencia_Data_To2 ,
                                       String AV33ContratoOcorrencia_Descricao2 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       String AV35TFContrato_Numero ,
                                       String AV36TFContrato_Numero_Sel ,
                                       DateTime AV39TFContratoOcorrencia_Data ,
                                       DateTime AV40TFContratoOcorrencia_Data_To ,
                                       String AV45TFContratoOcorrencia_Descricao ,
                                       String AV46TFContratoOcorrencia_Descricao_Sel ,
                                       String AV37ddo_Contrato_NumeroTitleControlIdToReplace ,
                                       String AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace ,
                                       String AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace ,
                                       String AV55Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF782( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIA_DATA", GetSecureSignedToken( "", A295ContratoOcorrencia_Data));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIA_DATA", context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIA_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A296ContratoOcorrencia_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIA_DESCRICAO", A296ContratoOcorrencia_Descricao);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV30DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV30DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV32DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator2), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF782( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV55Pgmname = "PromptContratoOcorrencia";
         context.Gx_err = 0;
      }

      protected void RF782( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 79;
         /* Execute user event: E23782 */
         E23782 ();
         nGXsfl_79_idx = 1;
         sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
         SubsflControlProps_792( ) ;
         nGXsfl_79_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_792( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16ContratoOcorrencia_Data1 ,
                                                 AV17ContratoOcorrencia_Data_To1 ,
                                                 AV30DynamicFiltersOperator1 ,
                                                 AV31ContratoOcorrencia_Descricao1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20ContratoOcorrencia_Data2 ,
                                                 AV21ContratoOcorrencia_Data_To2 ,
                                                 AV32DynamicFiltersOperator2 ,
                                                 AV33ContratoOcorrencia_Descricao2 ,
                                                 AV36TFContrato_Numero_Sel ,
                                                 AV35TFContrato_Numero ,
                                                 AV39TFContratoOcorrencia_Data ,
                                                 AV40TFContratoOcorrencia_Data_To ,
                                                 AV46TFContratoOcorrencia_Descricao_Sel ,
                                                 AV45TFContratoOcorrencia_Descricao ,
                                                 A295ContratoOcorrencia_Data ,
                                                 A296ContratoOcorrencia_Descricao ,
                                                 A77Contrato_Numero ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV31ContratoOcorrencia_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV31ContratoOcorrencia_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrencia_Descricao1", AV31ContratoOcorrencia_Descricao1);
            lV31ContratoOcorrencia_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV31ContratoOcorrencia_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrencia_Descricao1", AV31ContratoOcorrencia_Descricao1);
            lV33ContratoOcorrencia_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV33ContratoOcorrencia_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContratoOcorrencia_Descricao2", AV33ContratoOcorrencia_Descricao2);
            lV33ContratoOcorrencia_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV33ContratoOcorrencia_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContratoOcorrencia_Descricao2", AV33ContratoOcorrencia_Descricao2);
            lV35TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV35TFContrato_Numero), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Numero", AV35TFContrato_Numero);
            lV45TFContratoOcorrencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV45TFContratoOcorrencia_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoOcorrencia_Descricao", AV45TFContratoOcorrencia_Descricao);
            /* Using cursor H00782 */
            pr_default.execute(0, new Object[] {AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, lV31ContratoOcorrencia_Descricao1, lV31ContratoOcorrencia_Descricao1, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, lV33ContratoOcorrencia_Descricao2, lV33ContratoOcorrencia_Descricao2, lV35TFContrato_Numero, AV36TFContrato_Numero_Sel, AV39TFContratoOcorrencia_Data, AV40TFContratoOcorrencia_Data_To, lV45TFContratoOcorrencia_Descricao, AV46TFContratoOcorrencia_Descricao_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_79_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A74Contrato_Codigo = H00782_A74Contrato_Codigo[0];
               A294ContratoOcorrencia_Codigo = H00782_A294ContratoOcorrencia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
               A296ContratoOcorrencia_Descricao = H00782_A296ContratoOcorrencia_Descricao[0];
               A295ContratoOcorrencia_Data = H00782_A295ContratoOcorrencia_Data[0];
               A77Contrato_Numero = H00782_A77Contrato_Numero[0];
               A77Contrato_Numero = H00782_A77Contrato_Numero[0];
               /* Execute user event: E24782 */
               E24782 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 79;
            WB780( ) ;
         }
         nGXsfl_79_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16ContratoOcorrencia_Data1 ,
                                              AV17ContratoOcorrencia_Data_To1 ,
                                              AV30DynamicFiltersOperator1 ,
                                              AV31ContratoOcorrencia_Descricao1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20ContratoOcorrencia_Data2 ,
                                              AV21ContratoOcorrencia_Data_To2 ,
                                              AV32DynamicFiltersOperator2 ,
                                              AV33ContratoOcorrencia_Descricao2 ,
                                              AV36TFContrato_Numero_Sel ,
                                              AV35TFContrato_Numero ,
                                              AV39TFContratoOcorrencia_Data ,
                                              AV40TFContratoOcorrencia_Data_To ,
                                              AV46TFContratoOcorrencia_Descricao_Sel ,
                                              AV45TFContratoOcorrencia_Descricao ,
                                              A295ContratoOcorrencia_Data ,
                                              A296ContratoOcorrencia_Descricao ,
                                              A77Contrato_Numero ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV31ContratoOcorrencia_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV31ContratoOcorrencia_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrencia_Descricao1", AV31ContratoOcorrencia_Descricao1);
         lV31ContratoOcorrencia_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV31ContratoOcorrencia_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrencia_Descricao1", AV31ContratoOcorrencia_Descricao1);
         lV33ContratoOcorrencia_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV33ContratoOcorrencia_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContratoOcorrencia_Descricao2", AV33ContratoOcorrencia_Descricao2);
         lV33ContratoOcorrencia_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV33ContratoOcorrencia_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContratoOcorrencia_Descricao2", AV33ContratoOcorrencia_Descricao2);
         lV35TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV35TFContrato_Numero), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Numero", AV35TFContrato_Numero);
         lV45TFContratoOcorrencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV45TFContratoOcorrencia_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoOcorrencia_Descricao", AV45TFContratoOcorrencia_Descricao);
         /* Using cursor H00783 */
         pr_default.execute(1, new Object[] {AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, lV31ContratoOcorrencia_Descricao1, lV31ContratoOcorrencia_Descricao1, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, lV33ContratoOcorrencia_Descricao2, lV33ContratoOcorrencia_Descricao2, lV35TFContrato_Numero, AV36TFContrato_Numero_Sel, AV39TFContratoOcorrencia_Data, AV40TFContratoOcorrencia_Data_To, lV45TFContratoOcorrencia_Descricao, AV46TFContratoOcorrencia_Descricao_Sel});
         GRID_nRecordCount = H00783_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV30DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV31ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV32DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV33ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV35TFContrato_Numero, AV36TFContrato_Numero_Sel, AV39TFContratoOcorrencia_Data, AV40TFContratoOcorrencia_Data_To, AV45TFContratoOcorrencia_Descricao, AV46TFContratoOcorrencia_Descricao_Sel, AV37ddo_Contrato_NumeroTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV30DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV31ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV32DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV33ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV35TFContrato_Numero, AV36TFContrato_Numero_Sel, AV39TFContratoOcorrencia_Data, AV40TFContratoOcorrencia_Data_To, AV45TFContratoOcorrencia_Descricao, AV46TFContratoOcorrencia_Descricao_Sel, AV37ddo_Contrato_NumeroTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV30DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV31ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV32DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV33ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV35TFContrato_Numero, AV36TFContrato_Numero_Sel, AV39TFContratoOcorrencia_Data, AV40TFContratoOcorrencia_Data_To, AV45TFContratoOcorrencia_Descricao, AV46TFContratoOcorrencia_Descricao_Sel, AV37ddo_Contrato_NumeroTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV30DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV31ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV32DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV33ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV35TFContrato_Numero, AV36TFContrato_Numero_Sel, AV39TFContratoOcorrencia_Data, AV40TFContratoOcorrencia_Data_To, AV45TFContratoOcorrencia_Descricao, AV46TFContratoOcorrencia_Descricao_Sel, AV37ddo_Contrato_NumeroTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV30DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV31ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV32DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV33ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV35TFContrato_Numero, AV36TFContrato_Numero_Sel, AV39TFContratoOcorrencia_Data, AV40TFContratoOcorrencia_Data_To, AV45TFContratoOcorrencia_Descricao, AV46TFContratoOcorrencia_Descricao_Sel, AV37ddo_Contrato_NumeroTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP780( )
      {
         /* Before Start, stand alone formulas. */
         AV55Pgmname = "PromptContratoOcorrencia";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22782 */
         E22782 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV48DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_NUMEROTITLEFILTERDATA"), AV34Contrato_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIA_DATATITLEFILTERDATA"), AV38ContratoOcorrencia_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIA_DESCRICAOTITLEFILTERDATA"), AV44ContratoOcorrencia_DescricaoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV30DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30DynamicFiltersOperator1), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencia_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia_Data1"}), 1, "vCONTRATOOCORRENCIA_DATA1");
               GX_FocusControl = edtavContratoocorrencia_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16ContratoOcorrencia_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoOcorrencia_Data1", context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"));
            }
            else
            {
               AV16ContratoOcorrencia_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencia_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoOcorrencia_Data1", context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencia_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia_Data_To1"}), 1, "vCONTRATOOCORRENCIA_DATA_TO1");
               GX_FocusControl = edtavContratoocorrencia_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContratoOcorrencia_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrencia_Data_To1", context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"));
            }
            else
            {
               AV17ContratoOcorrencia_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencia_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrencia_Data_To1", context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"));
            }
            AV31ContratoOcorrencia_Descricao1 = StringUtil.Upper( cgiGet( edtavContratoocorrencia_descricao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrencia_Descricao1", AV31ContratoOcorrencia_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV32DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator2), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencia_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia_Data2"}), 1, "vCONTRATOOCORRENCIA_DATA2");
               GX_FocusControl = edtavContratoocorrencia_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20ContratoOcorrencia_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoOcorrencia_Data2", context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"));
            }
            else
            {
               AV20ContratoOcorrencia_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencia_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoOcorrencia_Data2", context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencia_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia_Data_To2"}), 1, "vCONTRATOOCORRENCIA_DATA_TO2");
               GX_FocusControl = edtavContratoocorrencia_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21ContratoOcorrencia_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoOcorrencia_Data_To2", context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"));
            }
            else
            {
               AV21ContratoOcorrencia_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencia_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoOcorrencia_Data_To2", context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"));
            }
            AV33ContratoOcorrencia_Descricao2 = StringUtil.Upper( cgiGet( edtavContratoocorrencia_descricao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContratoOcorrencia_Descricao2", AV33ContratoOcorrencia_Descricao2);
            A294ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrencia_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV35TFContrato_Numero = cgiGet( edtavTfcontrato_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Numero", AV35TFContrato_Numero);
            AV36TFContrato_Numero_Sel = cgiGet( edtavTfcontrato_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Numero_Sel", AV36TFContrato_Numero_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencia_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia_Data"}), 1, "vTFCONTRATOOCORRENCIA_DATA");
               GX_FocusControl = edtavTfcontratoocorrencia_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFContratoOcorrencia_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoOcorrencia_Data", context.localUtil.Format(AV39TFContratoOcorrencia_Data, "99/99/99"));
            }
            else
            {
               AV39TFContratoOcorrencia_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencia_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoOcorrencia_Data", context.localUtil.Format(AV39TFContratoOcorrencia_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencia_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia_Data_To"}), 1, "vTFCONTRATOOCORRENCIA_DATA_TO");
               GX_FocusControl = edtavTfcontratoocorrencia_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFContratoOcorrencia_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoOcorrencia_Data_To", context.localUtil.Format(AV40TFContratoOcorrencia_Data_To, "99/99/99"));
            }
            else
            {
               AV40TFContratoOcorrencia_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencia_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoOcorrencia_Data_To", context.localUtil.Format(AV40TFContratoOcorrencia_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencia_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia_Data Aux Date"}), 1, "vDDO_CONTRATOOCORRENCIA_DATAAUXDATE");
               GX_FocusControl = edtavDdo_contratoocorrencia_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41DDO_ContratoOcorrencia_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DDO_ContratoOcorrencia_DataAuxDate", context.localUtil.Format(AV41DDO_ContratoOcorrencia_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV41DDO_ContratoOcorrencia_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencia_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DDO_ContratoOcorrencia_DataAuxDate", context.localUtil.Format(AV41DDO_ContratoOcorrencia_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencia_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia_Data Aux Date To"}), 1, "vDDO_CONTRATOOCORRENCIA_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_contratoocorrencia_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42DDO_ContratoOcorrencia_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DDO_ContratoOcorrencia_DataAuxDateTo", context.localUtil.Format(AV42DDO_ContratoOcorrencia_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV42DDO_ContratoOcorrencia_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencia_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DDO_ContratoOcorrencia_DataAuxDateTo", context.localUtil.Format(AV42DDO_ContratoOcorrencia_DataAuxDateTo, "99/99/99"));
            }
            AV45TFContratoOcorrencia_Descricao = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencia_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoOcorrencia_Descricao", AV45TFContratoOcorrencia_Descricao);
            AV46TFContratoOcorrencia_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencia_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContratoOcorrencia_Descricao_Sel", AV46TFContratoOcorrencia_Descricao_Sel);
            AV37ddo_Contrato_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Contrato_NumeroTitleControlIdToReplace", AV37ddo_Contrato_NumeroTitleControlIdToReplace);
            AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace", AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace);
            AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace", AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_79 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_79"), ",", "."));
            AV50GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV51GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contrato_numero_Caption = cgiGet( "DDO_CONTRATO_NUMERO_Caption");
            Ddo_contrato_numero_Tooltip = cgiGet( "DDO_CONTRATO_NUMERO_Tooltip");
            Ddo_contrato_numero_Cls = cgiGet( "DDO_CONTRATO_NUMERO_Cls");
            Ddo_contrato_numero_Filteredtext_set = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_set");
            Ddo_contrato_numero_Selectedvalue_set = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_set");
            Ddo_contrato_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_NUMERO_Dropdownoptionstype");
            Ddo_contrato_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace");
            Ddo_contrato_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortasc"));
            Ddo_contrato_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortdsc"));
            Ddo_contrato_numero_Sortedstatus = cgiGet( "DDO_CONTRATO_NUMERO_Sortedstatus");
            Ddo_contrato_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includefilter"));
            Ddo_contrato_numero_Filtertype = cgiGet( "DDO_CONTRATO_NUMERO_Filtertype");
            Ddo_contrato_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Filterisrange"));
            Ddo_contrato_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includedatalist"));
            Ddo_contrato_numero_Datalisttype = cgiGet( "DDO_CONTRATO_NUMERO_Datalisttype");
            Ddo_contrato_numero_Datalistfixedvalues = cgiGet( "DDO_CONTRATO_NUMERO_Datalistfixedvalues");
            Ddo_contrato_numero_Datalistproc = cgiGet( "DDO_CONTRATO_NUMERO_Datalistproc");
            Ddo_contrato_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_numero_Sortasc = cgiGet( "DDO_CONTRATO_NUMERO_Sortasc");
            Ddo_contrato_numero_Sortdsc = cgiGet( "DDO_CONTRATO_NUMERO_Sortdsc");
            Ddo_contrato_numero_Loadingdata = cgiGet( "DDO_CONTRATO_NUMERO_Loadingdata");
            Ddo_contrato_numero_Cleanfilter = cgiGet( "DDO_CONTRATO_NUMERO_Cleanfilter");
            Ddo_contrato_numero_Rangefilterfrom = cgiGet( "DDO_CONTRATO_NUMERO_Rangefilterfrom");
            Ddo_contrato_numero_Rangefilterto = cgiGet( "DDO_CONTRATO_NUMERO_Rangefilterto");
            Ddo_contrato_numero_Noresultsfound = cgiGet( "DDO_CONTRATO_NUMERO_Noresultsfound");
            Ddo_contrato_numero_Searchbuttontext = cgiGet( "DDO_CONTRATO_NUMERO_Searchbuttontext");
            Ddo_contratoocorrencia_data_Caption = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Caption");
            Ddo_contratoocorrencia_data_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Tooltip");
            Ddo_contratoocorrencia_data_Cls = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Cls");
            Ddo_contratoocorrencia_data_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Filteredtext_set");
            Ddo_contratoocorrencia_data_Filteredtextto_set = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Filteredtextto_set");
            Ddo_contratoocorrencia_data_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Dropdownoptionstype");
            Ddo_contratoocorrencia_data_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Titlecontrolidtoreplace");
            Ddo_contratoocorrencia_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Includesortasc"));
            Ddo_contratoocorrencia_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Includesortdsc"));
            Ddo_contratoocorrencia_data_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Sortedstatus");
            Ddo_contratoocorrencia_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Includefilter"));
            Ddo_contratoocorrencia_data_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Filtertype");
            Ddo_contratoocorrencia_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Filterisrange"));
            Ddo_contratoocorrencia_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Includedatalist"));
            Ddo_contratoocorrencia_data_Datalistfixedvalues = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Datalistfixedvalues");
            Ddo_contratoocorrencia_data_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencia_data_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Sortasc");
            Ddo_contratoocorrencia_data_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Sortdsc");
            Ddo_contratoocorrencia_data_Loadingdata = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Loadingdata");
            Ddo_contratoocorrencia_data_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Cleanfilter");
            Ddo_contratoocorrencia_data_Rangefilterfrom = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Rangefilterfrom");
            Ddo_contratoocorrencia_data_Rangefilterto = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Rangefilterto");
            Ddo_contratoocorrencia_data_Noresultsfound = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Noresultsfound");
            Ddo_contratoocorrencia_data_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Searchbuttontext");
            Ddo_contratoocorrencia_descricao_Caption = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Caption");
            Ddo_contratoocorrencia_descricao_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Tooltip");
            Ddo_contratoocorrencia_descricao_Cls = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Cls");
            Ddo_contratoocorrencia_descricao_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filteredtext_set");
            Ddo_contratoocorrencia_descricao_Selectedvalue_set = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Selectedvalue_set");
            Ddo_contratoocorrencia_descricao_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Dropdownoptionstype");
            Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencia_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includesortasc"));
            Ddo_contratoocorrencia_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includesortdsc"));
            Ddo_contratoocorrencia_descricao_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Sortedstatus");
            Ddo_contratoocorrencia_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includefilter"));
            Ddo_contratoocorrencia_descricao_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filtertype");
            Ddo_contratoocorrencia_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filterisrange"));
            Ddo_contratoocorrencia_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includedatalist"));
            Ddo_contratoocorrencia_descricao_Datalisttype = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalisttype");
            Ddo_contratoocorrencia_descricao_Datalistfixedvalues = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalistfixedvalues");
            Ddo_contratoocorrencia_descricao_Datalistproc = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalistproc");
            Ddo_contratoocorrencia_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencia_descricao_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Sortasc");
            Ddo_contratoocorrencia_descricao_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Sortdsc");
            Ddo_contratoocorrencia_descricao_Loadingdata = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Loadingdata");
            Ddo_contratoocorrencia_descricao_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Cleanfilter");
            Ddo_contratoocorrencia_descricao_Rangefilterfrom = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Rangefilterfrom");
            Ddo_contratoocorrencia_descricao_Rangefilterto = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Rangefilterto");
            Ddo_contratoocorrencia_descricao_Noresultsfound = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Noresultsfound");
            Ddo_contratoocorrencia_descricao_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contrato_numero_Activeeventkey = cgiGet( "DDO_CONTRATO_NUMERO_Activeeventkey");
            Ddo_contrato_numero_Filteredtext_get = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_get");
            Ddo_contrato_numero_Selectedvalue_get = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_get");
            Ddo_contratoocorrencia_data_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Activeeventkey");
            Ddo_contratoocorrencia_data_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Filteredtext_get");
            Ddo_contratoocorrencia_data_Filteredtextto_get = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Filteredtextto_get");
            Ddo_contratoocorrencia_descricao_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Activeeventkey");
            Ddo_contratoocorrencia_descricao_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filteredtext_get");
            Ddo_contratoocorrencia_descricao_Selectedvalue_get = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "PromptContratoOcorrencia";
            A294ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrencia_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("promptcontratoocorrencia:[SecurityCheckFailed value for]"+"ContratoOcorrencia_Codigo:"+context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV30DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA1"), 0) != AV16ContratoOcorrencia_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA_TO1"), 0) != AV17ContratoOcorrencia_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIA_DESCRICAO1"), AV31ContratoOcorrencia_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV32DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA2"), 0) != AV20ContratoOcorrencia_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA_TO2"), 0) != AV21ContratoOcorrencia_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIA_DESCRICAO2"), AV33ContratoOcorrencia_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV35TFContrato_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV36TFContrato_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DATA"), 0) != AV39TFContratoOcorrencia_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DATA_TO"), 0) != AV40TFContratoOcorrencia_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DESCRICAO"), AV45TFContratoOcorrencia_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DESCRICAO_SEL"), AV46TFContratoOcorrencia_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22782 */
         E22782 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22782( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOOCORRENCIA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATOOCORRENCIA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfcontrato_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_Visible), 5, 0)));
         edtavTfcontrato_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencia_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_data_Visible), 5, 0)));
         edtavTfcontratoocorrencia_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_data_to_Visible), 5, 0)));
         edtavTfcontratoocorrencia_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_descricao_Visible), 5, 0)));
         edtavTfcontratoocorrencia_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_descricao_sel_Visible), 5, 0)));
         Ddo_contrato_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "TitleControlIdToReplace", Ddo_contrato_numero_Titlecontrolidtoreplace);
         AV37ddo_Contrato_NumeroTitleControlIdToReplace = Ddo_contrato_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Contrato_NumeroTitleControlIdToReplace", AV37ddo_Contrato_NumeroTitleControlIdToReplace);
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencia_data_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrencia_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencia_data_Titlecontrolidtoreplace);
         AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace = Ddo_contratoocorrencia_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace", AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace);
         edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrencia_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace);
         AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace = Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace", AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace);
         edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Contrato Ocorr�ncia";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtContratoOcorrencia_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrencia_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "N� Contrato", 0);
         cmbavOrderedby.addItem("3", "Descri��o", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV48DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV48DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E23782( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV34Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ContratoOcorrencia_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44ContratoOcorrencia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N� Contrato", AV37ddo_Contrato_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContratoOcorrencia_Data_Titleformat = 2;
         edtContratoOcorrencia_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Data_Internalname, "Title", edtContratoOcorrencia_Data_Title);
         edtContratoOcorrencia_Descricao_Titleformat = 2;
         edtContratoOcorrencia_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Descricao_Internalname, "Title", edtContratoOcorrencia_Descricao_Title);
         AV50GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50GridCurrentPage), 10, 0)));
         AV51GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Contrato_NumeroTitleFilterData", AV34Contrato_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38ContratoOcorrencia_DataTitleFilterData", AV38ContratoOcorrencia_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44ContratoOcorrencia_DescricaoTitleFilterData", AV44ContratoOcorrencia_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11782( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV49PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV49PageToGo) ;
         }
      }

      protected void E12782( )
      {
         /* Ddo_contrato_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFContrato_Numero = Ddo_contrato_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Numero", AV35TFContrato_Numero);
            AV36TFContrato_Numero_Sel = Ddo_contrato_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Numero_Sel", AV36TFContrato_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13782( )
      {
         /* Ddo_contratoocorrencia_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencia_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencia_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "SortedStatus", Ddo_contratoocorrencia_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencia_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencia_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "SortedStatus", Ddo_contratoocorrencia_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencia_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFContratoOcorrencia_Data = context.localUtil.CToD( Ddo_contratoocorrencia_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoOcorrencia_Data", context.localUtil.Format(AV39TFContratoOcorrencia_Data, "99/99/99"));
            AV40TFContratoOcorrencia_Data_To = context.localUtil.CToD( Ddo_contratoocorrencia_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoOcorrencia_Data_To", context.localUtil.Format(AV40TFContratoOcorrencia_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14782( )
      {
         /* Ddo_contratoocorrencia_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencia_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencia_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencia_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencia_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencia_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFContratoOcorrencia_Descricao = Ddo_contratoocorrencia_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoOcorrencia_Descricao", AV45TFContratoOcorrencia_Descricao);
            AV46TFContratoOcorrencia_Descricao_Sel = Ddo_contratoocorrencia_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContratoOcorrencia_Descricao_Sel", AV46TFContratoOcorrencia_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E24782( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV54Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 79;
         }
         sendrow_792( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_79_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(79, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E25782 */
         E25782 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25782( )
      {
         /* Enter Routine */
         AV7InOutContratoOcorrencia_Codigo = A294ContratoOcorrencia_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoOcorrencia_Codigo), 6, 0)));
         AV8InOutContratoOcorrencia_Data = A295ContratoOcorrencia_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoOcorrencia_Data", context.localUtil.Format(AV8InOutContratoOcorrencia_Data, "99/99/99"));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratoOcorrencia_Codigo,context.localUtil.Format( AV8InOutContratoOcorrencia_Data, "99/99/99")});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E15782( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E19782( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16782( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV30DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV31ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV32DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV33ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV35TFContrato_Numero, AV36TFContrato_Numero_Sel, AV39TFContratoOcorrencia_Data, AV40TFContratoOcorrencia_Data_To, AV45TFContratoOcorrencia_Descricao, AV46TFContratoOcorrencia_Descricao_Sel, AV37ddo_Contrato_NumeroTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E20782( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17782( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV30DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV31ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV32DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV33ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV35TFContrato_Numero, AV36TFContrato_Numero_Sel, AV39TFContratoOcorrencia_Data, AV40TFContratoOcorrencia_Data_To, AV45TFContratoOcorrencia_Descricao, AV46TFContratoOcorrencia_Descricao_Sel, AV37ddo_Contrato_NumeroTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E21782( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18782( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contrato_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         Ddo_contratoocorrencia_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "SortedStatus", Ddo_contratoocorrencia_data_Sortedstatus);
         Ddo_contratoocorrencia_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencia_descricao_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contrato_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoocorrencia_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "SortedStatus", Ddo_contratoocorrencia_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoocorrencia_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencia_descricao_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencia_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible), 5, 0)));
         edtavContratoocorrencia_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencia_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencia_descricao1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencia_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
         {
            edtavContratoocorrencia_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencia_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencia_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencia_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible), 5, 0)));
         edtavContratoocorrencia_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencia_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencia_descricao2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencia_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
         {
            edtavContratoocorrencia_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencia_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencia_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOOCORRENCIA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20ContratoOcorrencia_Data2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoOcorrencia_Data2", context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"));
         AV21ContratoOcorrencia_Data_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoOcorrencia_Data_To2", context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'CLEANFILTERS' Routine */
         AV35TFContrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Numero", AV35TFContrato_Numero);
         Ddo_contrato_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
         AV36TFContrato_Numero_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Numero_Sel", AV36TFContrato_Numero_Sel);
         Ddo_contrato_numero_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
         AV39TFContratoOcorrencia_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoOcorrencia_Data", context.localUtil.Format(AV39TFContratoOcorrencia_Data, "99/99/99"));
         Ddo_contratoocorrencia_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "FilteredText_set", Ddo_contratoocorrencia_data_Filteredtext_set);
         AV40TFContratoOcorrencia_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoOcorrencia_Data_To", context.localUtil.Format(AV40TFContratoOcorrencia_Data_To, "99/99/99"));
         Ddo_contratoocorrencia_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencia_data_Filteredtextto_set);
         AV45TFContratoOcorrencia_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoOcorrencia_Descricao", AV45TFContratoOcorrencia_Descricao);
         Ddo_contratoocorrencia_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "FilteredText_set", Ddo_contratoocorrencia_descricao_Filteredtext_set);
         AV46TFContratoOcorrencia_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContratoOcorrencia_Descricao_Sel", AV46TFContratoOcorrencia_Descricao_Sel);
         Ddo_contratoocorrencia_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "SelectedValue_set", Ddo_contratoocorrencia_descricao_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CONTRATOOCORRENCIA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16ContratoOcorrencia_Data1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoOcorrencia_Data1", context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"));
         AV17ContratoOcorrencia_Data_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrencia_Data_To1", context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S132( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 )
            {
               AV16ContratoOcorrencia_Data1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoOcorrencia_Data1", context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"));
               AV17ContratoOcorrencia_Data_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrencia_Data_To1", context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
            {
               AV30DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30DynamicFiltersOperator1), 4, 0)));
               AV31ContratoOcorrencia_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrencia_Descricao1", AV31ContratoOcorrencia_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 )
               {
                  AV20ContratoOcorrencia_Data2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoOcorrencia_Data2", context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"));
                  AV21ContratoOcorrencia_Data_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoOcorrencia_Data_To2", context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
               {
                  AV32DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator2), 4, 0)));
                  AV33ContratoOcorrencia_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContratoOcorrencia_Descricao2", AV33ContratoOcorrencia_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContrato_Numero)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = AV35TFContrato_Numero;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContrato_Numero_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV36TFContrato_Numero_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV39TFContratoOcorrencia_Data) && (DateTime.MinValue==AV40TFContratoOcorrencia_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIA_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV39TFContratoOcorrencia_Data, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV40TFContratoOcorrencia_Data_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratoOcorrencia_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIA_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFContratoOcorrencia_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratoOcorrencia_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIA_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFContratoOcorrencia_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV55Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ! ( (DateTime.MinValue==AV16ContratoOcorrencia_Data1) && (DateTime.MinValue==AV17ContratoOcorrencia_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV16ContratoOcorrencia_Data1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV17ContratoOcorrencia_Data_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratoOcorrencia_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31ContratoOcorrencia_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV30DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ! ( (DateTime.MinValue==AV20ContratoOcorrencia_Data2) && (DateTime.MinValue==AV21ContratoOcorrencia_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV20ContratoOcorrencia_Data2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV21ContratoOcorrencia_Data_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContratoOcorrencia_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV33ContratoOcorrencia_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV32DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_782( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_782( true) ;
         }
         else
         {
            wb_table2_5_782( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_782e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_73_782( true) ;
         }
         else
         {
            wb_table3_73_782( false) ;
         }
         return  ;
      }

      protected void wb_table3_73_782e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_782e( true) ;
         }
         else
         {
            wb_table1_2_782e( false) ;
         }
      }

      protected void wb_table3_73_782( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_76_782( true) ;
         }
         else
         {
            wb_table4_76_782( false) ;
         }
         return  ;
      }

      protected void wb_table4_76_782e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_73_782e( true) ;
         }
         else
         {
            wb_table3_73_782e( false) ;
         }
      }

      protected void wb_table4_76_782( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"79\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrencia_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrencia_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrencia_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrencia_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrencia_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrencia_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrencia_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrencia_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A296ContratoOcorrencia_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrencia_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrencia_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 79 )
         {
            wbEnd = 0;
            nRC_GXsfl_79 = (short)(nGXsfl_79_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_76_782e( true) ;
         }
         else
         {
            wb_table4_76_782e( false) ;
         }
      }

      protected void wb_table2_5_782( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratoOcorrencia.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_782( true) ;
         }
         else
         {
            wb_table5_14_782( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_782e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_782e( true) ;
         }
         else
         {
            wb_table2_5_782e( false) ;
         }
      }

      protected void wb_table5_14_782( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_782( true) ;
         }
         else
         {
            wb_table6_19_782( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_782e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_782e( true) ;
         }
         else
         {
            wb_table5_14_782e( false) ;
         }
      }

      protected void wb_table6_19_782( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContratoOcorrencia.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_782( true) ;
         }
         else
         {
            wb_table7_28_782( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_782e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoOcorrencia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_PromptContratoOcorrencia.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_53_782( true) ;
         }
         else
         {
            wb_table8_53_782( false) ;
         }
         return  ;
      }

      protected void wb_table8_53_782e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_782e( true) ;
         }
         else
         {
            wb_table6_19_782e( false) ;
         }
      }

      protected void wb_table8_53_782( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_PromptContratoOcorrencia.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_58_782( true) ;
         }
         else
         {
            wb_table9_58_782( false) ;
         }
         return  ;
      }

      protected void wb_table9_58_782e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencia_descricao2_Internalname, AV33ContratoOcorrencia_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV33ContratoOcorrencia_Descricao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencia_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoocorrencia_descricao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_53_782e( true) ;
         }
         else
         {
            wb_table8_53_782e( false) ;
         }
      }

      protected void wb_table9_58_782( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencia_data2_Internalname, tblTablemergeddynamicfilterscontratoocorrencia_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_79_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencia_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencia_data2_Internalname, context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"), context.localUtil.Format( AV20ContratoOcorrencia_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencia_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencia_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencia_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencia_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_79_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencia_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencia_data_to2_Internalname, context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"), context.localUtil.Format( AV21ContratoOcorrencia_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencia_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencia_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_58_782e( true) ;
         }
         else
         {
            wb_table9_58_782e( false) ;
         }
      }

      protected void wb_table7_28_782( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_79_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV30DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContratoOcorrencia.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_33_782( true) ;
         }
         else
         {
            wb_table10_33_782( false) ;
         }
         return  ;
      }

      protected void wb_table10_33_782e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_79_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencia_descricao1_Internalname, AV31ContratoOcorrencia_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV31ContratoOcorrencia_Descricao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencia_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoocorrencia_descricao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_782e( true) ;
         }
         else
         {
            wb_table7_28_782e( false) ;
         }
      }

      protected void wb_table10_33_782( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencia_data1_Internalname, tblTablemergeddynamicfilterscontratoocorrencia_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_79_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencia_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencia_data1_Internalname, context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"), context.localUtil.Format( AV16ContratoOcorrencia_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencia_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencia_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencia_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencia_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_79_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencia_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencia_data_to1_Internalname, context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"), context.localUtil.Format( AV17ContratoOcorrencia_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencia_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencia_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_33_782e( true) ;
         }
         else
         {
            wb_table10_33_782e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratoOcorrencia_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoOcorrencia_Codigo), 6, 0)));
         AV8InOutContratoOcorrencia_Data = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoOcorrencia_Data", context.localUtil.Format(AV8InOutContratoOcorrencia_Data, "99/99/99"));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA782( ) ;
         WS782( ) ;
         WE782( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204292235071");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratoocorrencia.js", "?20204292235071");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_792( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_79_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_79_idx;
         edtContratoOcorrencia_Data_Internalname = "CONTRATOOCORRENCIA_DATA_"+sGXsfl_79_idx;
         edtContratoOcorrencia_Descricao_Internalname = "CONTRATOOCORRENCIA_DESCRICAO_"+sGXsfl_79_idx;
      }

      protected void SubsflControlProps_fel_792( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_79_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_79_fel_idx;
         edtContratoOcorrencia_Data_Internalname = "CONTRATOOCORRENCIA_DATA_"+sGXsfl_79_fel_idx;
         edtContratoOcorrencia_Descricao_Internalname = "CONTRATOOCORRENCIA_DESCRICAO_"+sGXsfl_79_fel_idx;
      }

      protected void sendrow_792( )
      {
         SubsflControlProps_792( ) ;
         WB780( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_79_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_79_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_79_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 80,'',false,'',79)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV54Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV54Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_79_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrencia_Data_Internalname,context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"),context.localUtil.Format( A295ContratoOcorrencia_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrencia_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrencia_Descricao_Internalname,(String)A296ContratoOcorrencia_Descricao,StringUtil.RTrim( context.localUtil.Format( A296ContratoOcorrencia_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrencia_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)79,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIA_DATA"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, A295ContratoOcorrencia_Data));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIA_DESCRICAO"+"_"+sGXsfl_79_idx, GetSecureSignedToken( sGXsfl_79_idx, StringUtil.RTrim( context.localUtil.Format( A296ContratoOcorrencia_Descricao, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_79_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_79_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_79_idx+1));
            sGXsfl_79_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_79_idx), 4, 0)), 4, "0");
            SubsflControlProps_792( ) ;
         }
         /* End function sendrow_792 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContratoocorrencia_data1_Internalname = "vCONTRATOOCORRENCIA_DATA1";
         lblDynamicfilterscontratoocorrencia_data_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATOOCORRENCIA_DATA_RANGEMIDDLETEXT1";
         edtavContratoocorrencia_data_to1_Internalname = "vCONTRATOOCORRENCIA_DATA_TO1";
         tblTablemergeddynamicfilterscontratoocorrencia_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA1";
         edtavContratoocorrencia_descricao1_Internalname = "vCONTRATOOCORRENCIA_DESCRICAO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContratoocorrencia_data2_Internalname = "vCONTRATOOCORRENCIA_DATA2";
         lblDynamicfilterscontratoocorrencia_data_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATOOCORRENCIA_DATA_RANGEMIDDLETEXT2";
         edtavContratoocorrencia_data_to2_Internalname = "vCONTRATOOCORRENCIA_DATA_TO2";
         tblTablemergeddynamicfilterscontratoocorrencia_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA2";
         edtavContratoocorrencia_descricao2_Internalname = "vCONTRATOOCORRENCIA_DESCRICAO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContratoOcorrencia_Data_Internalname = "CONTRATOOCORRENCIA_DATA";
         edtContratoOcorrencia_Descricao_Internalname = "CONTRATOOCORRENCIA_DESCRICAO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratoOcorrencia_Codigo_Internalname = "CONTRATOOCORRENCIA_CODIGO";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfcontrato_numero_Internalname = "vTFCONTRATO_NUMERO";
         edtavTfcontrato_numero_sel_Internalname = "vTFCONTRATO_NUMERO_SEL";
         edtavTfcontratoocorrencia_data_Internalname = "vTFCONTRATOOCORRENCIA_DATA";
         edtavTfcontratoocorrencia_data_to_Internalname = "vTFCONTRATOOCORRENCIA_DATA_TO";
         edtavDdo_contratoocorrencia_dataauxdate_Internalname = "vDDO_CONTRATOOCORRENCIA_DATAAUXDATE";
         edtavDdo_contratoocorrencia_dataauxdateto_Internalname = "vDDO_CONTRATOOCORRENCIA_DATAAUXDATETO";
         divDdo_contratoocorrencia_dataauxdates_Internalname = "DDO_CONTRATOOCORRENCIA_DATAAUXDATES";
         edtavTfcontratoocorrencia_descricao_Internalname = "vTFCONTRATOOCORRENCIA_DESCRICAO";
         edtavTfcontratoocorrencia_descricao_sel_Internalname = "vTFCONTRATOOCORRENCIA_DESCRICAO_SEL";
         Ddo_contrato_numero_Internalname = "DDO_CONTRATO_NUMERO";
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencia_data_Internalname = "DDO_CONTRATOOCORRENCIA_DATA";
         edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencia_descricao_Internalname = "DDO_CONTRATOOCORRENCIA_DESCRICAO";
         edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoOcorrencia_Descricao_Jsonclick = "";
         edtContratoOcorrencia_Data_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContratoocorrencia_data_to1_Jsonclick = "";
         edtavContratoocorrencia_data1_Jsonclick = "";
         edtavContratoocorrencia_descricao1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratoocorrencia_data_to2_Jsonclick = "";
         edtavContratoocorrencia_data2_Jsonclick = "";
         edtavContratoocorrencia_descricao2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContratoOcorrencia_Descricao_Titleformat = 0;
         edtContratoOcorrencia_Data_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratoocorrencia_descricao2_Visible = 1;
         tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratoocorrencia_descricao1_Visible = 1;
         tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible = 1;
         edtContratoOcorrencia_Descricao_Title = "Descri��o";
         edtContratoOcorrencia_Data_Title = "Data";
         edtContrato_Numero_Title = "N� Contrato";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoocorrencia_descricao_sel_Jsonclick = "";
         edtavTfcontratoocorrencia_descricao_sel_Visible = 1;
         edtavTfcontratoocorrencia_descricao_Jsonclick = "";
         edtavTfcontratoocorrencia_descricao_Visible = 1;
         edtavDdo_contratoocorrencia_dataauxdateto_Jsonclick = "";
         edtavDdo_contratoocorrencia_dataauxdate_Jsonclick = "";
         edtavTfcontratoocorrencia_data_to_Jsonclick = "";
         edtavTfcontratoocorrencia_data_to_Visible = 1;
         edtavTfcontratoocorrencia_data_Jsonclick = "";
         edtavTfcontratoocorrencia_data_Visible = 1;
         edtavTfcontrato_numero_sel_Jsonclick = "";
         edtavTfcontrato_numero_sel_Visible = 1;
         edtavTfcontrato_numero_Jsonclick = "";
         edtavTfcontrato_numero_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtContratoOcorrencia_Codigo_Jsonclick = "";
         edtContratoOcorrencia_Codigo_Visible = 1;
         Ddo_contratoocorrencia_descricao_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencia_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencia_descricao_Rangefilterto = "At�";
         Ddo_contratoocorrencia_descricao_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencia_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencia_descricao_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencia_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencia_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencia_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencia_descricao_Datalistproc = "GetPromptContratoOcorrenciaFilterData";
         Ddo_contratoocorrencia_descricao_Datalistfixedvalues = "";
         Ddo_contratoocorrencia_descricao_Datalisttype = "Dynamic";
         Ddo_contratoocorrencia_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencia_descricao_Filtertype = "Character";
         Ddo_contratoocorrencia_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencia_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencia_descricao_Cls = "ColumnSettings";
         Ddo_contratoocorrencia_descricao_Tooltip = "Op��es";
         Ddo_contratoocorrencia_descricao_Caption = "";
         Ddo_contratoocorrencia_data_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencia_data_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencia_data_Rangefilterto = "At�";
         Ddo_contratoocorrencia_data_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencia_data_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencia_data_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencia_data_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencia_data_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencia_data_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencia_data_Datalistfixedvalues = "";
         Ddo_contratoocorrencia_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencia_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_data_Filtertype = "Date";
         Ddo_contratoocorrencia_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_data_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencia_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencia_data_Cls = "ColumnSettings";
         Ddo_contratoocorrencia_data_Tooltip = "Op��es";
         Ddo_contratoocorrencia_data_Caption = "";
         Ddo_contrato_numero_Searchbuttontext = "Pesquisar";
         Ddo_contrato_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_numero_Rangefilterto = "At�";
         Ddo_contrato_numero_Rangefilterfrom = "Desde";
         Ddo_contrato_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_numero_Loadingdata = "Carregando dados...";
         Ddo_contrato_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_numero_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_numero_Datalistproc = "GetPromptContratoOcorrenciaFilterData";
         Ddo_contrato_numero_Datalistfixedvalues = "";
         Ddo_contrato_numero_Datalisttype = "Dynamic";
         Ddo_contrato_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contrato_numero_Filtertype = "Character";
         Ddo_contrato_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Titlecontrolidtoreplace = "";
         Ddo_contrato_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_numero_Cls = "ColumnSettings";
         Ddo_contrato_numero_Tooltip = "Op��es";
         Ddo_contrato_numero_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contrato Ocorr�ncia";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV37ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV35TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV36TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV39TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV46TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV31ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV30DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV33ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV32DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0}],oparms:[{av:'AV34Contrato_NumeroTitleFilterData',fld:'vCONTRATO_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV38ContratoOcorrencia_DataTitleFilterData',fld:'vCONTRATOOCORRENCIA_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV44ContratoOcorrencia_DescricaoTitleFilterData',fld:'vCONTRATOOCORRENCIA_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContratoOcorrencia_Data_Titleformat',ctrl:'CONTRATOOCORRENCIA_DATA',prop:'Titleformat'},{av:'edtContratoOcorrencia_Data_Title',ctrl:'CONTRATOOCORRENCIA_DATA',prop:'Title'},{av:'edtContratoOcorrencia_Descricao_Titleformat',ctrl:'CONTRATOOCORRENCIA_DESCRICAO',prop:'Titleformat'},{av:'edtContratoOcorrencia_Descricao_Title',ctrl:'CONTRATOOCORRENCIA_DESCRICAO',prop:'Title'},{av:'AV50GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV51GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11782',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV31ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV33ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV35TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV36TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV39TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV46TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATO_NUMERO.ONOPTIONCLICKED","{handler:'E12782',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV31ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV33ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV35TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV36TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV39TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV46TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contrato_numero_Activeeventkey',ctrl:'DDO_CONTRATO_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contrato_numero_Filteredtext_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contrato_numero_Selectedvalue_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'AV35TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV36TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contratoocorrencia_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIA_DATA.ONOPTIONCLICKED","{handler:'E13782',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV31ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV33ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV35TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV36TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV39TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV46TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoocorrencia_data_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencia_data_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencia_data_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencia_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'SortedStatus'},{av:'AV39TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIA_DESCRICAO.ONOPTIONCLICKED","{handler:'E14782',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV31ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV33ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV35TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV36TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV39TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV46TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoocorrencia_descricao_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencia_descricao_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencia_descricao_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencia_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'SortedStatus'},{av:'AV45TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV46TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E24782',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E25782',iparms:[{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A295ContratoOcorrencia_Data',fld:'CONTRATOOCORRENCIA_DATA',pic:'',hsh:true,nv:''}],oparms:[{av:'AV7InOutContratoOcorrencia_Codigo',fld:'vINOUTCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContratoOcorrencia_Data',fld:'vINOUTCONTRATOOCORRENCIA_DATA',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15782',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV31ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV33ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV35TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV36TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV39TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV46TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E19782',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16782',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV31ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV33ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV35TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV36TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV39TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV46TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV30DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV31ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV32DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV33ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA2',prop:'Visible'},{av:'edtavContratoocorrencia_descricao2_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA1',prop:'Visible'},{av:'edtavContratoocorrencia_descricao1_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E20782',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA1',prop:'Visible'},{av:'edtavContratoocorrencia_descricao1_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17782',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV31ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV33ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV35TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV36TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV39TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV46TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV30DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV31ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV32DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV33ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA2',prop:'Visible'},{av:'edtavContratoocorrencia_descricao2_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA1',prop:'Visible'},{av:'edtavContratoocorrencia_descricao1_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E21782',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA2',prop:'Visible'},{av:'edtavContratoocorrencia_descricao2_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18782',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV31ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV33ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV35TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV36TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV39TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV46TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV35TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'Ddo_contrato_numero_Filteredtext_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_set'},{av:'AV36TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Selectedvalue_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_set'},{av:'AV39TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'Ddo_contratoocorrencia_data_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'FilteredText_set'},{av:'AV40TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'Ddo_contratoocorrencia_data_Filteredtextto_set',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'FilteredTextTo_set'},{av:'AV45TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_contratoocorrencia_descricao_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'FilteredText_set'},{av:'AV46TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contratoocorrencia_descricao_Selectedvalue_set',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA1',prop:'Visible'},{av:'edtavContratoocorrencia_descricao1_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV30DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV31ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV32DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV33ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA2',prop:'Visible'},{av:'edtavContratoocorrencia_descricao2_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO2',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutContratoOcorrencia_Data = DateTime.MinValue;
         Gridpaginationbar_Selectedpage = "";
         Ddo_contrato_numero_Activeeventkey = "";
         Ddo_contrato_numero_Filteredtext_get = "";
         Ddo_contrato_numero_Selectedvalue_get = "";
         Ddo_contratoocorrencia_data_Activeeventkey = "";
         Ddo_contratoocorrencia_data_Filteredtext_get = "";
         Ddo_contratoocorrencia_data_Filteredtextto_get = "";
         Ddo_contratoocorrencia_descricao_Activeeventkey = "";
         Ddo_contratoocorrencia_descricao_Filteredtext_get = "";
         Ddo_contratoocorrencia_descricao_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16ContratoOcorrencia_Data1 = DateTime.MinValue;
         AV17ContratoOcorrencia_Data_To1 = DateTime.MinValue;
         AV31ContratoOcorrencia_Descricao1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV20ContratoOcorrencia_Data2 = DateTime.MinValue;
         AV21ContratoOcorrencia_Data_To2 = DateTime.MinValue;
         AV33ContratoOcorrencia_Descricao2 = "";
         AV35TFContrato_Numero = "";
         AV36TFContrato_Numero_Sel = "";
         AV39TFContratoOcorrencia_Data = DateTime.MinValue;
         AV40TFContratoOcorrencia_Data_To = DateTime.MinValue;
         AV45TFContratoOcorrencia_Descricao = "";
         AV46TFContratoOcorrencia_Descricao_Sel = "";
         AV37ddo_Contrato_NumeroTitleControlIdToReplace = "";
         AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace = "";
         AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace = "";
         AV55Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV48DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV34Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ContratoOcorrencia_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44ContratoOcorrencia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contrato_numero_Filteredtext_set = "";
         Ddo_contrato_numero_Selectedvalue_set = "";
         Ddo_contrato_numero_Sortedstatus = "";
         Ddo_contratoocorrencia_data_Filteredtext_set = "";
         Ddo_contratoocorrencia_data_Filteredtextto_set = "";
         Ddo_contratoocorrencia_data_Sortedstatus = "";
         Ddo_contratoocorrencia_descricao_Filteredtext_set = "";
         Ddo_contratoocorrencia_descricao_Selectedvalue_set = "";
         Ddo_contratoocorrencia_descricao_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV41DDO_ContratoOcorrencia_DataAuxDate = DateTime.MinValue;
         AV42DDO_ContratoOcorrencia_DataAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV54Select_GXI = "";
         A77Contrato_Numero = "";
         A295ContratoOcorrencia_Data = DateTime.MinValue;
         A296ContratoOcorrencia_Descricao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV31ContratoOcorrencia_Descricao1 = "";
         lV33ContratoOcorrencia_Descricao2 = "";
         lV35TFContrato_Numero = "";
         lV45TFContratoOcorrencia_Descricao = "";
         H00782_A74Contrato_Codigo = new int[1] ;
         H00782_A294ContratoOcorrencia_Codigo = new int[1] ;
         H00782_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         H00782_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         H00782_A77Contrato_Numero = new String[] {""} ;
         H00783_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfilterscontratoocorrencia_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratoocorrencia_data_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratoocorrencia__default(),
            new Object[][] {
                new Object[] {
               H00782_A74Contrato_Codigo, H00782_A294ContratoOcorrencia_Codigo, H00782_A296ContratoOcorrencia_Descricao, H00782_A295ContratoOcorrencia_Data, H00782_A77Contrato_Numero
               }
               , new Object[] {
               H00783_AGRID_nRecordCount
               }
            }
         );
         AV55Pgmname = "PromptContratoOcorrencia";
         /* GeneXus formulas. */
         AV55Pgmname = "PromptContratoOcorrencia";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_79 ;
      private short nGXsfl_79_idx=1 ;
      private short AV13OrderedBy ;
      private short AV30DynamicFiltersOperator1 ;
      private short AV32DynamicFiltersOperator2 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_79_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContratoOcorrencia_Data_Titleformat ;
      private short edtContratoOcorrencia_Descricao_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratoOcorrencia_Codigo ;
      private int wcpOAV7InOutContratoOcorrencia_Codigo ;
      private int subGrid_Rows ;
      private int A294ContratoOcorrencia_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contrato_numero_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencia_data_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencia_descricao_Datalistupdateminimumcharacters ;
      private int edtContratoOcorrencia_Codigo_Visible ;
      private int edtavTfcontrato_numero_Visible ;
      private int edtavTfcontrato_numero_sel_Visible ;
      private int edtavTfcontratoocorrencia_data_Visible ;
      private int edtavTfcontratoocorrencia_data_to_Visible ;
      private int edtavTfcontratoocorrencia_descricao_Visible ;
      private int edtavTfcontratoocorrencia_descricao_sel_Visible ;
      private int edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A74Contrato_Codigo ;
      private int edtavOrdereddsc_Visible ;
      private int AV49PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible ;
      private int edtavContratoocorrencia_descricao1_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible ;
      private int edtavContratoocorrencia_descricao2_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV50GridCurrentPage ;
      private long AV51GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contrato_numero_Activeeventkey ;
      private String Ddo_contrato_numero_Filteredtext_get ;
      private String Ddo_contrato_numero_Selectedvalue_get ;
      private String Ddo_contratoocorrencia_data_Activeeventkey ;
      private String Ddo_contratoocorrencia_data_Filteredtext_get ;
      private String Ddo_contratoocorrencia_data_Filteredtextto_get ;
      private String Ddo_contratoocorrencia_descricao_Activeeventkey ;
      private String Ddo_contratoocorrencia_descricao_Filteredtext_get ;
      private String Ddo_contratoocorrencia_descricao_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_79_idx="0001" ;
      private String AV35TFContrato_Numero ;
      private String AV36TFContrato_Numero_Sel ;
      private String AV55Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contrato_numero_Caption ;
      private String Ddo_contrato_numero_Tooltip ;
      private String Ddo_contrato_numero_Cls ;
      private String Ddo_contrato_numero_Filteredtext_set ;
      private String Ddo_contrato_numero_Selectedvalue_set ;
      private String Ddo_contrato_numero_Dropdownoptionstype ;
      private String Ddo_contrato_numero_Titlecontrolidtoreplace ;
      private String Ddo_contrato_numero_Sortedstatus ;
      private String Ddo_contrato_numero_Filtertype ;
      private String Ddo_contrato_numero_Datalisttype ;
      private String Ddo_contrato_numero_Datalistfixedvalues ;
      private String Ddo_contrato_numero_Datalistproc ;
      private String Ddo_contrato_numero_Sortasc ;
      private String Ddo_contrato_numero_Sortdsc ;
      private String Ddo_contrato_numero_Loadingdata ;
      private String Ddo_contrato_numero_Cleanfilter ;
      private String Ddo_contrato_numero_Rangefilterfrom ;
      private String Ddo_contrato_numero_Rangefilterto ;
      private String Ddo_contrato_numero_Noresultsfound ;
      private String Ddo_contrato_numero_Searchbuttontext ;
      private String Ddo_contratoocorrencia_data_Caption ;
      private String Ddo_contratoocorrencia_data_Tooltip ;
      private String Ddo_contratoocorrencia_data_Cls ;
      private String Ddo_contratoocorrencia_data_Filteredtext_set ;
      private String Ddo_contratoocorrencia_data_Filteredtextto_set ;
      private String Ddo_contratoocorrencia_data_Dropdownoptionstype ;
      private String Ddo_contratoocorrencia_data_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencia_data_Sortedstatus ;
      private String Ddo_contratoocorrencia_data_Filtertype ;
      private String Ddo_contratoocorrencia_data_Datalistfixedvalues ;
      private String Ddo_contratoocorrencia_data_Sortasc ;
      private String Ddo_contratoocorrencia_data_Sortdsc ;
      private String Ddo_contratoocorrencia_data_Loadingdata ;
      private String Ddo_contratoocorrencia_data_Cleanfilter ;
      private String Ddo_contratoocorrencia_data_Rangefilterfrom ;
      private String Ddo_contratoocorrencia_data_Rangefilterto ;
      private String Ddo_contratoocorrencia_data_Noresultsfound ;
      private String Ddo_contratoocorrencia_data_Searchbuttontext ;
      private String Ddo_contratoocorrencia_descricao_Caption ;
      private String Ddo_contratoocorrencia_descricao_Tooltip ;
      private String Ddo_contratoocorrencia_descricao_Cls ;
      private String Ddo_contratoocorrencia_descricao_Filteredtext_set ;
      private String Ddo_contratoocorrencia_descricao_Selectedvalue_set ;
      private String Ddo_contratoocorrencia_descricao_Dropdownoptionstype ;
      private String Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencia_descricao_Sortedstatus ;
      private String Ddo_contratoocorrencia_descricao_Filtertype ;
      private String Ddo_contratoocorrencia_descricao_Datalisttype ;
      private String Ddo_contratoocorrencia_descricao_Datalistfixedvalues ;
      private String Ddo_contratoocorrencia_descricao_Datalistproc ;
      private String Ddo_contratoocorrencia_descricao_Sortasc ;
      private String Ddo_contratoocorrencia_descricao_Sortdsc ;
      private String Ddo_contratoocorrencia_descricao_Loadingdata ;
      private String Ddo_contratoocorrencia_descricao_Cleanfilter ;
      private String Ddo_contratoocorrencia_descricao_Rangefilterfrom ;
      private String Ddo_contratoocorrencia_descricao_Rangefilterto ;
      private String Ddo_contratoocorrencia_descricao_Noresultsfound ;
      private String Ddo_contratoocorrencia_descricao_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String edtContratoOcorrencia_Codigo_Internalname ;
      private String edtContratoOcorrencia_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfcontrato_numero_Internalname ;
      private String edtavTfcontrato_numero_Jsonclick ;
      private String edtavTfcontrato_numero_sel_Internalname ;
      private String edtavTfcontrato_numero_sel_Jsonclick ;
      private String edtavTfcontratoocorrencia_data_Internalname ;
      private String edtavTfcontratoocorrencia_data_Jsonclick ;
      private String edtavTfcontratoocorrencia_data_to_Internalname ;
      private String edtavTfcontratoocorrencia_data_to_Jsonclick ;
      private String divDdo_contratoocorrencia_dataauxdates_Internalname ;
      private String edtavDdo_contratoocorrencia_dataauxdate_Internalname ;
      private String edtavDdo_contratoocorrencia_dataauxdate_Jsonclick ;
      private String edtavDdo_contratoocorrencia_dataauxdateto_Internalname ;
      private String edtavDdo_contratoocorrencia_dataauxdateto_Jsonclick ;
      private String edtavTfcontratoocorrencia_descricao_Internalname ;
      private String edtavTfcontratoocorrencia_descricao_Jsonclick ;
      private String edtavTfcontratoocorrencia_descricao_sel_Internalname ;
      private String edtavTfcontratoocorrencia_descricao_sel_Jsonclick ;
      private String edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String edtContratoOcorrencia_Data_Internalname ;
      private String edtContratoOcorrencia_Descricao_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV35TFContrato_Numero ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratoocorrencia_data1_Internalname ;
      private String edtavContratoocorrencia_data_to1_Internalname ;
      private String edtavContratoocorrencia_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratoocorrencia_data2_Internalname ;
      private String edtavContratoocorrencia_data_to2_Internalname ;
      private String edtavContratoocorrencia_descricao2_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contrato_numero_Internalname ;
      private String Ddo_contratoocorrencia_data_Internalname ;
      private String Ddo_contratoocorrencia_descricao_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContrato_Numero_Title ;
      private String edtContratoOcorrencia_Data_Title ;
      private String edtContratoOcorrencia_Descricao_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfilterscontratoocorrencia_data1_Internalname ;
      private String tblTablemergeddynamicfilterscontratoocorrencia_data2_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratoocorrencia_descricao2_Jsonclick ;
      private String edtavContratoocorrencia_data2_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencia_data_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratoocorrencia_data_rangemiddletext2_Jsonclick ;
      private String edtavContratoocorrencia_data_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratoocorrencia_descricao1_Jsonclick ;
      private String edtavContratoocorrencia_data1_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencia_data_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratoocorrencia_data_rangemiddletext1_Jsonclick ;
      private String edtavContratoocorrencia_data_to1_Jsonclick ;
      private String sGXsfl_79_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContratoOcorrencia_Data_Jsonclick ;
      private String edtContratoOcorrencia_Descricao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV8InOutContratoOcorrencia_Data ;
      private DateTime wcpOAV8InOutContratoOcorrencia_Data ;
      private DateTime AV16ContratoOcorrencia_Data1 ;
      private DateTime AV17ContratoOcorrencia_Data_To1 ;
      private DateTime AV20ContratoOcorrencia_Data2 ;
      private DateTime AV21ContratoOcorrencia_Data_To2 ;
      private DateTime AV39TFContratoOcorrencia_Data ;
      private DateTime AV40TFContratoOcorrencia_Data_To ;
      private DateTime AV41DDO_ContratoOcorrencia_DataAuxDate ;
      private DateTime AV42DDO_ContratoOcorrencia_DataAuxDateTo ;
      private DateTime A295ContratoOcorrencia_Data ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contrato_numero_Includesortasc ;
      private bool Ddo_contrato_numero_Includesortdsc ;
      private bool Ddo_contrato_numero_Includefilter ;
      private bool Ddo_contrato_numero_Filterisrange ;
      private bool Ddo_contrato_numero_Includedatalist ;
      private bool Ddo_contratoocorrencia_data_Includesortasc ;
      private bool Ddo_contratoocorrencia_data_Includesortdsc ;
      private bool Ddo_contratoocorrencia_data_Includefilter ;
      private bool Ddo_contratoocorrencia_data_Filterisrange ;
      private bool Ddo_contratoocorrencia_data_Includedatalist ;
      private bool Ddo_contratoocorrencia_descricao_Includesortasc ;
      private bool Ddo_contratoocorrencia_descricao_Includesortdsc ;
      private bool Ddo_contratoocorrencia_descricao_Includefilter ;
      private bool Ddo_contratoocorrencia_descricao_Filterisrange ;
      private bool Ddo_contratoocorrencia_descricao_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV31ContratoOcorrencia_Descricao1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV33ContratoOcorrencia_Descricao2 ;
      private String AV45TFContratoOcorrencia_Descricao ;
      private String AV46TFContratoOcorrencia_Descricao_Sel ;
      private String AV37ddo_Contrato_NumeroTitleControlIdToReplace ;
      private String AV43ddo_ContratoOcorrencia_DataTitleControlIdToReplace ;
      private String AV47ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace ;
      private String AV54Select_GXI ;
      private String A296ContratoOcorrencia_Descricao ;
      private String lV31ContratoOcorrencia_Descricao1 ;
      private String lV33ContratoOcorrencia_Descricao2 ;
      private String lV45TFContratoOcorrencia_Descricao ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratoOcorrencia_Codigo ;
      private DateTime aP1_InOutContratoOcorrencia_Data ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private int[] H00782_A74Contrato_Codigo ;
      private int[] H00782_A294ContratoOcorrencia_Codigo ;
      private String[] H00782_A296ContratoOcorrencia_Descricao ;
      private DateTime[] H00782_A295ContratoOcorrencia_Data ;
      private String[] H00782_A77Contrato_Numero ;
      private long[] H00783_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34Contrato_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38ContratoOcorrencia_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44ContratoOcorrencia_DescricaoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV48DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratoocorrencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00782( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16ContratoOcorrencia_Data1 ,
                                             DateTime AV17ContratoOcorrencia_Data_To1 ,
                                             short AV30DynamicFiltersOperator1 ,
                                             String AV31ContratoOcorrencia_Descricao1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20ContratoOcorrencia_Data2 ,
                                             DateTime AV21ContratoOcorrencia_Data_To2 ,
                                             short AV32DynamicFiltersOperator2 ,
                                             String AV33ContratoOcorrencia_Descricao2 ,
                                             String AV36TFContrato_Numero_Sel ,
                                             String AV35TFContrato_Numero ,
                                             DateTime AV39TFContratoOcorrencia_Data ,
                                             DateTime AV40TFContratoOcorrencia_Data_To ,
                                             String AV46TFContratoOcorrencia_Descricao_Sel ,
                                             String AV45TFContratoOcorrencia_Descricao ,
                                             DateTime A295ContratoOcorrencia_Data ,
                                             String A296ContratoOcorrencia_Descricao ,
                                             String A77Contrato_Numero ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [19] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Contrato_Codigo], T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrencia_Descricao], T1.[ContratoOcorrencia_Data], T2.[Contrato_Numero]";
         sFromString = " FROM ([ContratoOcorrencia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV16ContratoOcorrencia_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV16ContratoOcorrencia_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV16ContratoOcorrencia_Data1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV17ContratoOcorrencia_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV17ContratoOcorrencia_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV17ContratoOcorrencia_Data_To1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV30DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratoOcorrencia_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV31ContratoOcorrencia_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV31ContratoOcorrencia_Descricao1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV30DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratoOcorrencia_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV31ContratoOcorrencia_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV31ContratoOcorrencia_Descricao1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV20ContratoOcorrencia_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV20ContratoOcorrencia_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV20ContratoOcorrencia_Data2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV21ContratoOcorrencia_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV21ContratoOcorrencia_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV21ContratoOcorrencia_Data_To2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV32DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContratoOcorrencia_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV33ContratoOcorrencia_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV33ContratoOcorrencia_Descricao2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV32DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContratoOcorrencia_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV33ContratoOcorrencia_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV33ContratoOcorrencia_Descricao2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV35TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV35TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV36TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV36TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV39TFContratoOcorrencia_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV39TFContratoOcorrencia_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV39TFContratoOcorrencia_Data)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV40TFContratoOcorrencia_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV40TFContratoOcorrencia_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV40TFContratoOcorrencia_Data_To)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratoOcorrencia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratoOcorrencia_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV45TFContratoOcorrencia_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV45TFContratoOcorrencia_Descricao)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratoOcorrencia_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] = @AV46TFContratoOcorrencia_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] = @AV46TFContratoOcorrencia_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Descricao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Descricao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00783( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16ContratoOcorrencia_Data1 ,
                                             DateTime AV17ContratoOcorrencia_Data_To1 ,
                                             short AV30DynamicFiltersOperator1 ,
                                             String AV31ContratoOcorrencia_Descricao1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20ContratoOcorrencia_Data2 ,
                                             DateTime AV21ContratoOcorrencia_Data_To2 ,
                                             short AV32DynamicFiltersOperator2 ,
                                             String AV33ContratoOcorrencia_Descricao2 ,
                                             String AV36TFContrato_Numero_Sel ,
                                             String AV35TFContrato_Numero ,
                                             DateTime AV39TFContratoOcorrencia_Data ,
                                             DateTime AV40TFContratoOcorrencia_Data_To ,
                                             String AV46TFContratoOcorrencia_Descricao_Sel ,
                                             String AV45TFContratoOcorrencia_Descricao ,
                                             DateTime A295ContratoOcorrencia_Data ,
                                             String A296ContratoOcorrencia_Descricao ,
                                             String A77Contrato_Numero ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [14] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContratoOcorrencia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV16ContratoOcorrencia_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV16ContratoOcorrencia_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV16ContratoOcorrencia_Data1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV17ContratoOcorrencia_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV17ContratoOcorrencia_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV17ContratoOcorrencia_Data_To1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV30DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratoOcorrencia_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV31ContratoOcorrencia_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV31ContratoOcorrencia_Descricao1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV30DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratoOcorrencia_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV31ContratoOcorrencia_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV31ContratoOcorrencia_Descricao1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV20ContratoOcorrencia_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV20ContratoOcorrencia_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV20ContratoOcorrencia_Data2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV21ContratoOcorrencia_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV21ContratoOcorrencia_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV21ContratoOcorrencia_Data_To2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV32DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContratoOcorrencia_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV33ContratoOcorrencia_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV33ContratoOcorrencia_Descricao2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV32DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContratoOcorrencia_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV33ContratoOcorrencia_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV33ContratoOcorrencia_Descricao2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV35TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV35TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV36TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV36TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV39TFContratoOcorrencia_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV39TFContratoOcorrencia_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV39TFContratoOcorrencia_Data)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV40TFContratoOcorrencia_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV40TFContratoOcorrencia_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV40TFContratoOcorrencia_Data_To)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratoOcorrencia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratoOcorrencia_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV45TFContratoOcorrencia_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV45TFContratoOcorrencia_Descricao)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratoOcorrencia_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] = @AV46TFContratoOcorrencia_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] = @AV46TFContratoOcorrencia_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00782(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_H00783(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00782 ;
          prmH00782 = new Object[] {
          new Object[] {"@AV16ContratoOcorrencia_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17ContratoOcorrencia_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV31ContratoOcorrencia_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV31ContratoOcorrencia_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV20ContratoOcorrencia_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21ContratoOcorrencia_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV33ContratoOcorrencia_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV33ContratoOcorrencia_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV35TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV36TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV39TFContratoOcorrencia_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV40TFContratoOcorrencia_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV45TFContratoOcorrencia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV46TFContratoOcorrencia_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00783 ;
          prmH00783 = new Object[] {
          new Object[] {"@AV16ContratoOcorrencia_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17ContratoOcorrencia_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV31ContratoOcorrencia_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV31ContratoOcorrencia_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV20ContratoOcorrencia_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21ContratoOcorrencia_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV33ContratoOcorrencia_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV33ContratoOcorrencia_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV35TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV36TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV39TFContratoOcorrencia_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV40TFContratoOcorrencia_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV45TFContratoOcorrencia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV46TFContratoOcorrencia_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00782", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00782,11,0,true,false )
             ,new CursorDef("H00783", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00783,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                return;
       }
    }

 }

}
