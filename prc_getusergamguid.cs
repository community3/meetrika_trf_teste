/*
               File: PRC_GetUserGamGuid
        Description: Get User Gam Guid
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:40.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getusergamguid : GXProcedure
   {
      public prc_getusergamguid( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getusergamguid( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo ,
                           out String aP1_Usuario_UserGamGuid )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV9Usuario_UserGamGuid = "" ;
         initialize();
         executePrivate();
         aP1_Usuario_UserGamGuid=this.AV9Usuario_UserGamGuid;
      }

      public String executeUdp( int aP0_Usuario_Codigo )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV9Usuario_UserGamGuid = "" ;
         initialize();
         executePrivate();
         aP1_Usuario_UserGamGuid=this.AV9Usuario_UserGamGuid;
         return AV9Usuario_UserGamGuid ;
      }

      public void executeSubmit( int aP0_Usuario_Codigo ,
                                 out String aP1_Usuario_UserGamGuid )
      {
         prc_getusergamguid objprc_getusergamguid;
         objprc_getusergamguid = new prc_getusergamguid();
         objprc_getusergamguid.A1Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_getusergamguid.AV9Usuario_UserGamGuid = "" ;
         objprc_getusergamguid.context.SetSubmitInitialConfig(context);
         objprc_getusergamguid.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getusergamguid);
         aP1_Usuario_UserGamGuid=this.AV9Usuario_UserGamGuid;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getusergamguid)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00762 */
         pr_default.execute(0, new Object[] {A1Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A341Usuario_UserGamGuid = P00762_A341Usuario_UserGamGuid[0];
            AV9Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00762_A1Usuario_Codigo = new int[1] ;
         P00762_A341Usuario_UserGamGuid = new String[] {""} ;
         A341Usuario_UserGamGuid = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getusergamguid__default(),
            new Object[][] {
                new Object[] {
               P00762_A1Usuario_Codigo, P00762_A341Usuario_UserGamGuid
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1Usuario_Codigo ;
      private String AV9Usuario_UserGamGuid ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00762_A1Usuario_Codigo ;
      private String[] P00762_A341Usuario_UserGamGuid ;
      private String aP1_Usuario_UserGamGuid ;
   }

   public class prc_getusergamguid__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00762 ;
          prmP00762 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00762", "SELECT [Usuario_Codigo], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00762,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
