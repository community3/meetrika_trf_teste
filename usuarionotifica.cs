/*
               File: UsuarioNotifica
        Description: Usuario Notifica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:32:43.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuarionotifica : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"USUARIONOTIFICA_USUARIOCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAUSUARIONOTIFICA_USUARIOCOD57229( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel9"+"_"+"") == 0 )
         {
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A2076UsuarioNotifica_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2076UsuarioNotifica_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A2076UsuarioNotifica_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_14") == 0 )
         {
            A2075UsuarioNotifica_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2075UsuarioNotifica_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_14( A2075UsuarioNotifica_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7UsuarioNotifica_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UsuarioNotifica_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIONOTIFICA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UsuarioNotifica_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynUsuarioNotifica_UsuarioCod.Name = "USUARIONOTIFICA_USUARIOCOD";
         dynUsuarioNotifica_UsuarioCod.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Usuario Notifica", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public usuarionotifica( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public usuarionotifica( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_UsuarioNotifica_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7UsuarioNotifica_Codigo = aP1_UsuarioNotifica_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynUsuarioNotifica_UsuarioCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynUsuarioNotifica_UsuarioCod.ItemCount > 0 )
         {
            A2076UsuarioNotifica_UsuarioCod = (int)(NumberUtil.Val( dynUsuarioNotifica_UsuarioCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2076UsuarioNotifica_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_57229( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_57229e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuarioNotifica_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0, ",", "")), ((edtUsuarioNotifica_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2077UsuarioNotifica_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A2077UsuarioNotifica_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuarioNotifica_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtUsuarioNotifica_Codigo_Visible, edtUsuarioNotifica_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_UsuarioNotifica.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_57229( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_57229( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_57229e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_39_57229( true) ;
         }
         return  ;
      }

      protected void wb_table3_39_57229e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_57229e( true) ;
         }
         else
         {
            wb_table1_2_57229e( false) ;
         }
      }

      protected void wb_table3_39_57229( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_39_57229e( true) ;
         }
         else
         {
            wb_table3_39_57229e( false) ;
         }
      }

      protected void wb_table2_5_57229( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_57229( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_57229e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_57229e( true) ;
         }
         else
         {
            wb_table2_5_57229e( false) ;
         }
      }

      protected void wb_table4_13_57229( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 10, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuarionotifica_usuariocod_Internalname, "Usu�rio", "", "", lblTextblockusuarionotifica_usuariocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynUsuarioNotifica_UsuarioCod, dynUsuarioNotifica_UsuarioCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0)), 1, dynUsuarioNotifica_UsuarioCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynUsuarioNotifica_UsuarioCod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_UsuarioNotifica.htm");
            dynUsuarioNotifica_UsuarioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUsuarioNotifica_UsuarioCod_Internalname, "Values", (String)(dynUsuarioNotifica_UsuarioCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuarionotifica_nostatus_Internalname, "No Status", "", "", lblTextblockusuarionotifica_nostatus_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_24_57229( true) ;
         }
         return  ;
      }

      protected void wb_table5_24_57229e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_57229e( true) ;
         }
         else
         {
            wb_table4_13_57229e( false) ;
         }
      }

      protected void wb_table5_24_57229( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedusuarionotifica_nostatus_Internalname, tblTablemergedusuarionotifica_nostatus_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuarioNotifica_NoStatus_Internalname, A2079UsuarioNotifica_NoStatus, StringUtil.RTrim( context.localUtil.Format( A2079UsuarioNotifica_NoStatus, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuarioNotifica_NoStatus_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuarioNotifica_NoStatus_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_UsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLbl2_Internalname, "��", "", "", lblLbl2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_UsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgStatus_Internalname, context.GetImagePath( "b9f89e2f-f9e1-4533-b84e-c591709915f9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgStatus_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgStatus_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOSTATUS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLbl1_Internalname, "��", "", "", lblLbl1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_UsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblstatus_Internalname, lblLblstatus_Caption, "", "", lblLblstatus_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblLblstatus_Visible, 1, 0, "HLP_UsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_24_57229e( true) ;
         }
         else
         {
            wb_table5_24_57229e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11572 */
         E11572 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynUsuarioNotifica_UsuarioCod.CurrentValue = cgiGet( dynUsuarioNotifica_UsuarioCod_Internalname);
               A2076UsuarioNotifica_UsuarioCod = (int)(NumberUtil.Val( cgiGet( dynUsuarioNotifica_UsuarioCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2076UsuarioNotifica_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0)));
               A2079UsuarioNotifica_NoStatus = cgiGet( edtUsuarioNotifica_NoStatus_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2079UsuarioNotifica_NoStatus", A2079UsuarioNotifica_NoStatus);
               A2077UsuarioNotifica_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuarioNotifica_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
               /* Read saved values. */
               Z2077UsuarioNotifica_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z2077UsuarioNotifica_Codigo"), ",", "."));
               Z2079UsuarioNotifica_NoStatus = cgiGet( "Z2079UsuarioNotifica_NoStatus");
               Z2076UsuarioNotifica_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "Z2076UsuarioNotifica_UsuarioCod"), ",", "."));
               Z2075UsuarioNotifica_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z2075UsuarioNotifica_AreaTrabalhoCod"), ",", "."));
               A2075UsuarioNotifica_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z2075UsuarioNotifica_AreaTrabalhoCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N2075UsuarioNotifica_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N2075UsuarioNotifica_AreaTrabalhoCod"), ",", "."));
               AV7UsuarioNotifica_Codigo = (int)(context.localUtil.CToN( cgiGet( "vUSUARIONOTIFICA_CODIGO"), ",", "."));
               AV11Insert_UsuarioNotifica_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_USUARIONOTIFICA_USUARIOCOD"), ",", "."));
               AV12Insert_UsuarioNotifica_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_USUARIONOTIFICA_AREATRABALHOCOD"), ",", "."));
               A2075UsuarioNotifica_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "USUARIONOTIFICA_AREATRABALHOCOD"), ",", "."));
               AV16Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "UsuarioNotifica";
               A2076UsuarioNotifica_UsuarioCod = (int)(NumberUtil.Val( cgiGet( dynUsuarioNotifica_UsuarioCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2076UsuarioNotifica_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2076UsuarioNotifica_UsuarioCod), "ZZZZZ9");
               A2077UsuarioNotifica_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuarioNotifica_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2077UsuarioNotifica_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A2077UsuarioNotifica_Codigo != Z2077UsuarioNotifica_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("usuarionotifica:[SecurityCheckFailed value for]"+"UsuarioNotifica_UsuarioCod:"+context.localUtil.Format( (decimal)(A2076UsuarioNotifica_UsuarioCod), "ZZZZZ9"));
                  GXUtil.WriteLog("usuarionotifica:[SecurityCheckFailed value for]"+"UsuarioNotifica_Codigo:"+context.localUtil.Format( (decimal)(A2077UsuarioNotifica_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("usuarionotifica:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("usuarionotifica:[SecurityCheckFailed value for]"+"UsuarioNotifica_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A2077UsuarioNotifica_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode229 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode229;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound229 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_570( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "USUARIONOTIFICA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtUsuarioNotifica_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11572 */
                           E11572 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12572 */
                           E12572 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOSTATUS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13572 */
                           E13572 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12572 */
            E12572 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll57229( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes57229( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_570( )
      {
         BeforeValidate57229( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls57229( ) ;
            }
            else
            {
               CheckExtendedTable57229( ) ;
               CloseExtendedTableCursors57229( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption570( )
      {
      }

      protected void E11572( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV16Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV17GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            while ( AV17GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV17GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "UsuarioNotifica_UsuarioCod") == 0 )
               {
                  AV11Insert_UsuarioNotifica_UsuarioCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_UsuarioNotifica_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_UsuarioNotifica_UsuarioCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "UsuarioNotifica_AreaTrabalhoCod") == 0 )
               {
                  AV12Insert_UsuarioNotifica_AreaTrabalhoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_UsuarioNotifica_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_UsuarioNotifica_AreaTrabalhoCod), 6, 0)));
               }
               AV17GXV1 = (int)(AV17GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            }
         }
         edtUsuarioNotifica_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioNotifica_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioNotifica_Codigo_Visible), 5, 0)));
      }

      protected void E12572( )
      {
         /* After Trn Routine */
         AV10WebSession.Remove("Status");
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwusuarionotifica.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E13572( )
      {
         /* 'DoStatus' Routine */
         lblLblstatus_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblstatus_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblLblstatus_Visible), 5, 0)));
         AV10WebSession.Set("Status", A2079UsuarioNotifica_NoStatus);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2079UsuarioNotifica_NoStatus", A2079UsuarioNotifica_NoStatus);
         context.PopUp(formatLink("wp_multiplosstatus.aspx") + "?" + UrlEncode(StringUtil.RTrim(A2079UsuarioNotifica_NoStatus)), new Object[] {"A2079UsuarioNotifica_NoStatus"});
      }

      protected void ZM57229( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2079UsuarioNotifica_NoStatus = T00573_A2079UsuarioNotifica_NoStatus[0];
               Z2076UsuarioNotifica_UsuarioCod = T00573_A2076UsuarioNotifica_UsuarioCod[0];
               Z2075UsuarioNotifica_AreaTrabalhoCod = T00573_A2075UsuarioNotifica_AreaTrabalhoCod[0];
            }
            else
            {
               Z2079UsuarioNotifica_NoStatus = A2079UsuarioNotifica_NoStatus;
               Z2076UsuarioNotifica_UsuarioCod = A2076UsuarioNotifica_UsuarioCod;
               Z2075UsuarioNotifica_AreaTrabalhoCod = A2075UsuarioNotifica_AreaTrabalhoCod;
            }
         }
         if ( GX_JID == -12 )
         {
            Z2077UsuarioNotifica_Codigo = A2077UsuarioNotifica_Codigo;
            Z2079UsuarioNotifica_NoStatus = A2079UsuarioNotifica_NoStatus;
            Z2076UsuarioNotifica_UsuarioCod = A2076UsuarioNotifica_UsuarioCod;
            Z2075UsuarioNotifica_AreaTrabalhoCod = A2075UsuarioNotifica_AreaTrabalhoCod;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAUSUARIONOTIFICA_USUARIOCOD_html57229( ) ;
         dynUsuarioNotifica_UsuarioCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUsuarioNotifica_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynUsuarioNotifica_UsuarioCod.Enabled), 5, 0)));
         edtUsuarioNotifica_NoStatus_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioNotifica_NoStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioNotifica_NoStatus_Enabled), 5, 0)));
         edtUsuarioNotifica_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioNotifica_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioNotifica_Codigo_Enabled), 5, 0)));
         AV16Pgmname = "UsuarioNotifica";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Pgmname", AV16Pgmname);
         dynUsuarioNotifica_UsuarioCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUsuarioNotifica_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynUsuarioNotifica_UsuarioCod.Enabled), 5, 0)));
         edtUsuarioNotifica_NoStatus_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioNotifica_NoStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioNotifica_NoStatus_Enabled), 5, 0)));
         edtUsuarioNotifica_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioNotifica_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioNotifica_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7UsuarioNotifica_Codigo) )
         {
            A2077UsuarioNotifica_Codigo = AV7UsuarioNotifica_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_UsuarioNotifica_UsuarioCod) )
         {
            A2076UsuarioNotifica_UsuarioCod = AV11Insert_UsuarioNotifica_UsuarioCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2076UsuarioNotifica_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_UsuarioNotifica_AreaTrabalhoCod) )
         {
            A2075UsuarioNotifica_AreaTrabalhoCod = AV12Insert_UsuarioNotifica_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2075UsuarioNotifica_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A2075UsuarioNotifica_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2075UsuarioNotifica_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), 6, 0)));
            }
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load57229( )
      {
         /* Using cursor T00576 */
         pr_default.execute(4, new Object[] {A2077UsuarioNotifica_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound229 = 1;
            A2079UsuarioNotifica_NoStatus = T00576_A2079UsuarioNotifica_NoStatus[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2079UsuarioNotifica_NoStatus", A2079UsuarioNotifica_NoStatus);
            A2076UsuarioNotifica_UsuarioCod = T00576_A2076UsuarioNotifica_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2076UsuarioNotifica_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0)));
            A2075UsuarioNotifica_AreaTrabalhoCod = T00576_A2075UsuarioNotifica_AreaTrabalhoCod[0];
            ZM57229( -12) ;
         }
         pr_default.close(4);
         OnLoadActions57229( ) ;
      }

      protected void OnLoadActions57229( )
      {
         GXt_char1 = "";
         new prc_listadestatus(context ).execute(  A2079UsuarioNotifica_NoStatus, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2079UsuarioNotifica_NoStatus", A2079UsuarioNotifica_NoStatus);
         lblLblstatus_Caption = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblstatus_Internalname, "Caption", lblLblstatus_Caption);
      }

      protected void CheckExtendedTable57229( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         GXt_char1 = "";
         new prc_listadestatus(context ).execute(  A2079UsuarioNotifica_NoStatus, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2079UsuarioNotifica_NoStatus", A2079UsuarioNotifica_NoStatus);
         lblLblstatus_Caption = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblstatus_Internalname, "Caption", lblLblstatus_Caption);
         /* Using cursor T00574 */
         pr_default.execute(2, new Object[] {A2076UsuarioNotifica_UsuarioCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario Notifica_Usuario'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor T00575 */
         pr_default.execute(3, new Object[] {A2075UsuarioNotifica_AreaTrabalhoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario Notifica_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors57229( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_13( int A2076UsuarioNotifica_UsuarioCod )
      {
         /* Using cursor T00577 */
         pr_default.execute(5, new Object[] {A2076UsuarioNotifica_UsuarioCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario Notifica_Usuario'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_14( int A2075UsuarioNotifica_AreaTrabalhoCod )
      {
         /* Using cursor T00578 */
         pr_default.execute(6, new Object[] {A2075UsuarioNotifica_AreaTrabalhoCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario Notifica_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey57229( )
      {
         /* Using cursor T00579 */
         pr_default.execute(7, new Object[] {A2077UsuarioNotifica_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound229 = 1;
         }
         else
         {
            RcdFound229 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00573 */
         pr_default.execute(1, new Object[] {A2077UsuarioNotifica_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM57229( 12) ;
            RcdFound229 = 1;
            A2077UsuarioNotifica_Codigo = T00573_A2077UsuarioNotifica_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
            A2079UsuarioNotifica_NoStatus = T00573_A2079UsuarioNotifica_NoStatus[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2079UsuarioNotifica_NoStatus", A2079UsuarioNotifica_NoStatus);
            A2076UsuarioNotifica_UsuarioCod = T00573_A2076UsuarioNotifica_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2076UsuarioNotifica_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0)));
            A2075UsuarioNotifica_AreaTrabalhoCod = T00573_A2075UsuarioNotifica_AreaTrabalhoCod[0];
            Z2077UsuarioNotifica_Codigo = A2077UsuarioNotifica_Codigo;
            sMode229 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load57229( ) ;
            if ( AnyError == 1 )
            {
               RcdFound229 = 0;
               InitializeNonKey57229( ) ;
            }
            Gx_mode = sMode229;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound229 = 0;
            InitializeNonKey57229( ) ;
            sMode229 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode229;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey57229( ) ;
         if ( RcdFound229 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound229 = 0;
         /* Using cursor T005710 */
         pr_default.execute(8, new Object[] {A2077UsuarioNotifica_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T005710_A2077UsuarioNotifica_Codigo[0] < A2077UsuarioNotifica_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T005710_A2077UsuarioNotifica_Codigo[0] > A2077UsuarioNotifica_Codigo ) ) )
            {
               A2077UsuarioNotifica_Codigo = T005710_A2077UsuarioNotifica_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
               RcdFound229 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound229 = 0;
         /* Using cursor T005711 */
         pr_default.execute(9, new Object[] {A2077UsuarioNotifica_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T005711_A2077UsuarioNotifica_Codigo[0] > A2077UsuarioNotifica_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T005711_A2077UsuarioNotifica_Codigo[0] < A2077UsuarioNotifica_Codigo ) ) )
            {
               A2077UsuarioNotifica_Codigo = T005711_A2077UsuarioNotifica_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
               RcdFound229 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey57229( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert57229( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound229 == 1 )
            {
               if ( A2077UsuarioNotifica_Codigo != Z2077UsuarioNotifica_Codigo )
               {
                  A2077UsuarioNotifica_Codigo = Z2077UsuarioNotifica_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "USUARIONOTIFICA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtUsuarioNotifica_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  /* Update record */
                  Update57229( ) ;
               }
            }
            else
            {
               if ( A2077UsuarioNotifica_Codigo != Z2077UsuarioNotifica_Codigo )
               {
                  /* Insert record */
                  Insert57229( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "USUARIONOTIFICA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtUsuarioNotifica_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     Insert57229( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A2077UsuarioNotifica_Codigo != Z2077UsuarioNotifica_Codigo )
         {
            A2077UsuarioNotifica_Codigo = Z2077UsuarioNotifica_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "USUARIONOTIFICA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtUsuarioNotifica_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency57229( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00572 */
            pr_default.execute(0, new Object[] {A2077UsuarioNotifica_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"UsuarioNotifica"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z2079UsuarioNotifica_NoStatus, T00572_A2079UsuarioNotifica_NoStatus[0]) != 0 ) || ( Z2076UsuarioNotifica_UsuarioCod != T00572_A2076UsuarioNotifica_UsuarioCod[0] ) || ( Z2075UsuarioNotifica_AreaTrabalhoCod != T00572_A2075UsuarioNotifica_AreaTrabalhoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z2079UsuarioNotifica_NoStatus, T00572_A2079UsuarioNotifica_NoStatus[0]) != 0 )
               {
                  GXUtil.WriteLog("usuarionotifica:[seudo value changed for attri]"+"UsuarioNotifica_NoStatus");
                  GXUtil.WriteLogRaw("Old: ",Z2079UsuarioNotifica_NoStatus);
                  GXUtil.WriteLogRaw("Current: ",T00572_A2079UsuarioNotifica_NoStatus[0]);
               }
               if ( Z2076UsuarioNotifica_UsuarioCod != T00572_A2076UsuarioNotifica_UsuarioCod[0] )
               {
                  GXUtil.WriteLog("usuarionotifica:[seudo value changed for attri]"+"UsuarioNotifica_UsuarioCod");
                  GXUtil.WriteLogRaw("Old: ",Z2076UsuarioNotifica_UsuarioCod);
                  GXUtil.WriteLogRaw("Current: ",T00572_A2076UsuarioNotifica_UsuarioCod[0]);
               }
               if ( Z2075UsuarioNotifica_AreaTrabalhoCod != T00572_A2075UsuarioNotifica_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("usuarionotifica:[seudo value changed for attri]"+"UsuarioNotifica_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z2075UsuarioNotifica_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T00572_A2075UsuarioNotifica_AreaTrabalhoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"UsuarioNotifica"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert57229( )
      {
         BeforeValidate57229( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable57229( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM57229( 0) ;
            CheckOptimisticConcurrency57229( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm57229( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert57229( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005712 */
                     pr_default.execute(10, new Object[] {A2079UsuarioNotifica_NoStatus, A2076UsuarioNotifica_UsuarioCod, A2075UsuarioNotifica_AreaTrabalhoCod});
                     A2077UsuarioNotifica_Codigo = T005712_A2077UsuarioNotifica_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("UsuarioNotifica") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption570( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load57229( ) ;
            }
            EndLevel57229( ) ;
         }
         CloseExtendedTableCursors57229( ) ;
      }

      protected void Update57229( )
      {
         BeforeValidate57229( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable57229( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency57229( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm57229( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate57229( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005713 */
                     pr_default.execute(11, new Object[] {A2079UsuarioNotifica_NoStatus, A2076UsuarioNotifica_UsuarioCod, A2075UsuarioNotifica_AreaTrabalhoCod, A2077UsuarioNotifica_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("UsuarioNotifica") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"UsuarioNotifica"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate57229( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel57229( ) ;
         }
         CloseExtendedTableCursors57229( ) ;
      }

      protected void DeferredUpdate57229( )
      {
      }

      protected void delete( )
      {
         BeforeValidate57229( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency57229( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls57229( ) ;
            AfterConfirm57229( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete57229( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T005714 */
                  pr_default.execute(12, new Object[] {A2077UsuarioNotifica_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("UsuarioNotifica") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode229 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel57229( ) ;
         Gx_mode = sMode229;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls57229( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_char1 = "";
            new prc_listadestatus(context ).execute(  A2079UsuarioNotifica_NoStatus, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2079UsuarioNotifica_NoStatus", A2079UsuarioNotifica_NoStatus);
            lblLblstatus_Caption = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblstatus_Internalname, "Caption", lblLblstatus_Caption);
         }
      }

      protected void EndLevel57229( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete57229( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "UsuarioNotifica");
            if ( AnyError == 0 )
            {
               ConfirmValues570( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "UsuarioNotifica");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart57229( )
      {
         /* Scan By routine */
         /* Using cursor T005715 */
         pr_default.execute(13);
         RcdFound229 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound229 = 1;
            A2077UsuarioNotifica_Codigo = T005715_A2077UsuarioNotifica_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext57229( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound229 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound229 = 1;
            A2077UsuarioNotifica_Codigo = T005715_A2077UsuarioNotifica_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd57229( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm57229( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert57229( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate57229( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete57229( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete57229( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate57229( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes57229( )
      {
         dynUsuarioNotifica_UsuarioCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUsuarioNotifica_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynUsuarioNotifica_UsuarioCod.Enabled), 5, 0)));
         edtUsuarioNotifica_NoStatus_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioNotifica_NoStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioNotifica_NoStatus_Enabled), 5, 0)));
         edtUsuarioNotifica_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioNotifica_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioNotifica_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues570( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299324452");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("usuarionotifica.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7UsuarioNotifica_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2077UsuarioNotifica_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2079UsuarioNotifica_NoStatus", Z2079UsuarioNotifica_NoStatus);
         GxWebStd.gx_hidden_field( context, "Z2076UsuarioNotifica_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2076UsuarioNotifica_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2075UsuarioNotifica_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2075UsuarioNotifica_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N2075UsuarioNotifica_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vUSUARIONOTIFICA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7UsuarioNotifica_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_USUARIONOTIFICA_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_UsuarioNotifica_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_USUARIONOTIFICA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_UsuarioNotifica_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIONOTIFICA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV16Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIONOTIFICA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UsuarioNotifica_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "UsuarioNotifica";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2076UsuarioNotifica_UsuarioCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2077UsuarioNotifica_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("usuarionotifica:[SendSecurityCheck value for]"+"UsuarioNotifica_UsuarioCod:"+context.localUtil.Format( (decimal)(A2076UsuarioNotifica_UsuarioCod), "ZZZZZ9"));
         GXUtil.WriteLog("usuarionotifica:[SendSecurityCheck value for]"+"UsuarioNotifica_Codigo:"+context.localUtil.Format( (decimal)(A2077UsuarioNotifica_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("usuarionotifica:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("usuarionotifica:[SendSecurityCheck value for]"+"UsuarioNotifica_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("usuarionotifica.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7UsuarioNotifica_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "UsuarioNotifica" ;
      }

      public override String GetPgmdesc( )
      {
         return "Usuario Notifica" ;
      }

      protected void InitializeNonKey57229( )
      {
         A2076UsuarioNotifica_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2076UsuarioNotifica_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0)));
         A2075UsuarioNotifica_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2075UsuarioNotifica_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), 6, 0)));
         A2079UsuarioNotifica_NoStatus = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2079UsuarioNotifica_NoStatus", A2079UsuarioNotifica_NoStatus);
         Z2079UsuarioNotifica_NoStatus = "";
         Z2076UsuarioNotifica_UsuarioCod = 0;
         Z2075UsuarioNotifica_AreaTrabalhoCod = 0;
      }

      protected void InitAll57229( )
      {
         A2077UsuarioNotifica_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2077UsuarioNotifica_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0)));
         InitializeNonKey57229( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2075UsuarioNotifica_AreaTrabalhoCod = i2075UsuarioNotifica_AreaTrabalhoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2075UsuarioNotifica_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), 6, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299324476");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("usuarionotifica.js", "?20205299324477");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockusuarionotifica_usuariocod_Internalname = "TEXTBLOCKUSUARIONOTIFICA_USUARIOCOD";
         dynUsuarioNotifica_UsuarioCod_Internalname = "USUARIONOTIFICA_USUARIOCOD";
         lblTextblockusuarionotifica_nostatus_Internalname = "TEXTBLOCKUSUARIONOTIFICA_NOSTATUS";
         edtUsuarioNotifica_NoStatus_Internalname = "USUARIONOTIFICA_NOSTATUS";
         lblLbl2_Internalname = "LBL2";
         imgStatus_Internalname = "STATUS";
         lblLbl1_Internalname = "LBL1";
         lblLblstatus_Internalname = "LBLSTATUS";
         tblTablemergedusuarionotifica_nostatus_Internalname = "TABLEMERGEDUSUARIONOTIFICA_NOSTATUS";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtUsuarioNotifica_Codigo_Internalname = "USUARIONOTIFICA_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Notifica��es";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Usuario Notifica";
         lblLblstatus_Caption = "Lista de Status";
         lblLblstatus_Visible = 1;
         imgStatus_Visible = 1;
         edtUsuarioNotifica_NoStatus_Jsonclick = "";
         edtUsuarioNotifica_NoStatus_Enabled = 0;
         dynUsuarioNotifica_UsuarioCod_Jsonclick = "";
         dynUsuarioNotifica_UsuarioCod.Enabled = 0;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtUsuarioNotifica_Codigo_Jsonclick = "";
         edtUsuarioNotifica_Codigo_Enabled = 0;
         edtUsuarioNotifica_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAUSUARIONOTIFICA_USUARIOCOD57229( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAUSUARIONOTIFICA_USUARIOCOD_data57229( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAUSUARIONOTIFICA_USUARIOCOD_html57229( )
      {
         int gxdynajaxvalue ;
         GXDLAUSUARIONOTIFICA_USUARIOCOD_data57229( ) ;
         gxdynajaxindex = 1;
         dynUsuarioNotifica_UsuarioCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynUsuarioNotifica_UsuarioCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAUSUARIONOTIFICA_USUARIOCOD_data57229( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T005716 */
         pr_default.execute(14);
         while ( (pr_default.getStatus(14) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T005716_A1Usuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T005716_A58Usuario_PessoaNom[0]));
            pr_default.readNext(14);
         }
         pr_default.close(14);
      }

      public void Valid_Usuarionotifica_usuariocod( GXCombobox dynGX_Parm1 )
      {
         dynUsuarioNotifica_UsuarioCod = dynGX_Parm1;
         A2076UsuarioNotifica_UsuarioCod = (int)(NumberUtil.Val( dynUsuarioNotifica_UsuarioCod.CurrentValue, "."));
         /* Using cursor T005717 */
         pr_default.execute(15, new Object[] {A2076UsuarioNotifica_UsuarioCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario Notifica_Usuario'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Usuarionotifica_nostatus( String GX_Parm1 )
      {
         A2079UsuarioNotifica_NoStatus = GX_Parm1;
         GXt_char1 = "";
         new prc_listadestatus(context ).execute(  A2079UsuarioNotifica_NoStatus, out  GXt_char1) ;
         lblLblstatus_Caption = GXt_char1;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(lblLblstatus_Caption);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7UsuarioNotifica_Codigo',fld:'vUSUARIONOTIFICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12572',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOSTATUS'","{handler:'E13572',iparms:[{av:'A2079UsuarioNotifica_NoStatus',fld:'USUARIONOTIFICA_NOSTATUS',pic:'',nv:''}],oparms:[{av:'lblLblstatus_Visible',ctrl:'LBLSTATUS',prop:'Visible'},{av:'A2079UsuarioNotifica_NoStatus',fld:'USUARIONOTIFICA_NOSTATUS',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z2079UsuarioNotifica_NoStatus = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblockusuarionotifica_usuariocod_Jsonclick = "";
         lblTextblockusuarionotifica_nostatus_Jsonclick = "";
         A2079UsuarioNotifica_NoStatus = "";
         lblLbl2_Jsonclick = "";
         imgStatus_Jsonclick = "";
         lblLbl1_Jsonclick = "";
         lblLblstatus_Jsonclick = "";
         AV16Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode229 = "";
         GX_FocusControl = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         T00576_A2077UsuarioNotifica_Codigo = new int[1] ;
         T00576_A2079UsuarioNotifica_NoStatus = new String[] {""} ;
         T00576_A2076UsuarioNotifica_UsuarioCod = new int[1] ;
         T00576_A2075UsuarioNotifica_AreaTrabalhoCod = new int[1] ;
         T00574_A2076UsuarioNotifica_UsuarioCod = new int[1] ;
         T00575_A2075UsuarioNotifica_AreaTrabalhoCod = new int[1] ;
         T00577_A2076UsuarioNotifica_UsuarioCod = new int[1] ;
         T00578_A2075UsuarioNotifica_AreaTrabalhoCod = new int[1] ;
         T00579_A2077UsuarioNotifica_Codigo = new int[1] ;
         T00573_A2077UsuarioNotifica_Codigo = new int[1] ;
         T00573_A2079UsuarioNotifica_NoStatus = new String[] {""} ;
         T00573_A2076UsuarioNotifica_UsuarioCod = new int[1] ;
         T00573_A2075UsuarioNotifica_AreaTrabalhoCod = new int[1] ;
         T005710_A2077UsuarioNotifica_Codigo = new int[1] ;
         T005711_A2077UsuarioNotifica_Codigo = new int[1] ;
         T00572_A2077UsuarioNotifica_Codigo = new int[1] ;
         T00572_A2079UsuarioNotifica_NoStatus = new String[] {""} ;
         T00572_A2076UsuarioNotifica_UsuarioCod = new int[1] ;
         T00572_A2075UsuarioNotifica_AreaTrabalhoCod = new int[1] ;
         T005712_A2077UsuarioNotifica_Codigo = new int[1] ;
         T005715_A2077UsuarioNotifica_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T005716_A57Usuario_PessoaCod = new int[1] ;
         T005716_A1Usuario_Codigo = new int[1] ;
         T005716_A58Usuario_PessoaNom = new String[] {""} ;
         T005716_n58Usuario_PessoaNom = new bool[] {false} ;
         T005717_A2076UsuarioNotifica_UsuarioCod = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         GXt_char1 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuarionotifica__default(),
            new Object[][] {
                new Object[] {
               T00572_A2077UsuarioNotifica_Codigo, T00572_A2079UsuarioNotifica_NoStatus, T00572_A2076UsuarioNotifica_UsuarioCod, T00572_A2075UsuarioNotifica_AreaTrabalhoCod
               }
               , new Object[] {
               T00573_A2077UsuarioNotifica_Codigo, T00573_A2079UsuarioNotifica_NoStatus, T00573_A2076UsuarioNotifica_UsuarioCod, T00573_A2075UsuarioNotifica_AreaTrabalhoCod
               }
               , new Object[] {
               T00574_A2076UsuarioNotifica_UsuarioCod
               }
               , new Object[] {
               T00575_A2075UsuarioNotifica_AreaTrabalhoCod
               }
               , new Object[] {
               T00576_A2077UsuarioNotifica_Codigo, T00576_A2079UsuarioNotifica_NoStatus, T00576_A2076UsuarioNotifica_UsuarioCod, T00576_A2075UsuarioNotifica_AreaTrabalhoCod
               }
               , new Object[] {
               T00577_A2076UsuarioNotifica_UsuarioCod
               }
               , new Object[] {
               T00578_A2075UsuarioNotifica_AreaTrabalhoCod
               }
               , new Object[] {
               T00579_A2077UsuarioNotifica_Codigo
               }
               , new Object[] {
               T005710_A2077UsuarioNotifica_Codigo
               }
               , new Object[] {
               T005711_A2077UsuarioNotifica_Codigo
               }
               , new Object[] {
               T005712_A2077UsuarioNotifica_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005715_A2077UsuarioNotifica_Codigo
               }
               , new Object[] {
               T005716_A57Usuario_PessoaCod, T005716_A1Usuario_Codigo, T005716_A58Usuario_PessoaNom, T005716_n58Usuario_PessoaNom
               }
               , new Object[] {
               T005717_A2076UsuarioNotifica_UsuarioCod
               }
            }
         );
         AV16Pgmname = "UsuarioNotifica";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound229 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7UsuarioNotifica_Codigo ;
      private int Z2077UsuarioNotifica_Codigo ;
      private int Z2076UsuarioNotifica_UsuarioCod ;
      private int Z2075UsuarioNotifica_AreaTrabalhoCod ;
      private int N2075UsuarioNotifica_AreaTrabalhoCod ;
      private int A2076UsuarioNotifica_UsuarioCod ;
      private int A2075UsuarioNotifica_AreaTrabalhoCod ;
      private int AV7UsuarioNotifica_Codigo ;
      private int trnEnded ;
      private int A2077UsuarioNotifica_Codigo ;
      private int edtUsuarioNotifica_Codigo_Enabled ;
      private int edtUsuarioNotifica_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtUsuarioNotifica_NoStatus_Enabled ;
      private int imgStatus_Visible ;
      private int lblLblstatus_Visible ;
      private int AV11Insert_UsuarioNotifica_UsuarioCod ;
      private int AV12Insert_UsuarioNotifica_AreaTrabalhoCod ;
      private int AV17GXV1 ;
      private int i2075UsuarioNotifica_AreaTrabalhoCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String edtUsuarioNotifica_Codigo_Internalname ;
      private String edtUsuarioNotifica_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockusuarionotifica_usuariocod_Internalname ;
      private String lblTextblockusuarionotifica_usuariocod_Jsonclick ;
      private String dynUsuarioNotifica_UsuarioCod_Internalname ;
      private String dynUsuarioNotifica_UsuarioCod_Jsonclick ;
      private String lblTextblockusuarionotifica_nostatus_Internalname ;
      private String lblTextblockusuarionotifica_nostatus_Jsonclick ;
      private String tblTablemergedusuarionotifica_nostatus_Internalname ;
      private String edtUsuarioNotifica_NoStatus_Internalname ;
      private String edtUsuarioNotifica_NoStatus_Jsonclick ;
      private String lblLbl2_Internalname ;
      private String lblLbl2_Jsonclick ;
      private String imgStatus_Internalname ;
      private String imgStatus_Jsonclick ;
      private String lblLbl1_Internalname ;
      private String lblLbl1_Jsonclick ;
      private String lblLblstatus_Internalname ;
      private String lblLblstatus_Caption ;
      private String lblLblstatus_Jsonclick ;
      private String AV16Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode229 ;
      private String GX_FocusControl ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private String GXt_char1 ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String Z2079UsuarioNotifica_NoStatus ;
      private String A2079UsuarioNotifica_NoStatus ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynUsuarioNotifica_UsuarioCod ;
      private IDataStoreProvider pr_default ;
      private int[] T00576_A2077UsuarioNotifica_Codigo ;
      private String[] T00576_A2079UsuarioNotifica_NoStatus ;
      private int[] T00576_A2076UsuarioNotifica_UsuarioCod ;
      private int[] T00576_A2075UsuarioNotifica_AreaTrabalhoCod ;
      private int[] T00574_A2076UsuarioNotifica_UsuarioCod ;
      private int[] T00575_A2075UsuarioNotifica_AreaTrabalhoCod ;
      private int[] T00577_A2076UsuarioNotifica_UsuarioCod ;
      private int[] T00578_A2075UsuarioNotifica_AreaTrabalhoCod ;
      private int[] T00579_A2077UsuarioNotifica_Codigo ;
      private int[] T00573_A2077UsuarioNotifica_Codigo ;
      private String[] T00573_A2079UsuarioNotifica_NoStatus ;
      private int[] T00573_A2076UsuarioNotifica_UsuarioCod ;
      private int[] T00573_A2075UsuarioNotifica_AreaTrabalhoCod ;
      private int[] T005710_A2077UsuarioNotifica_Codigo ;
      private int[] T005711_A2077UsuarioNotifica_Codigo ;
      private int[] T00572_A2077UsuarioNotifica_Codigo ;
      private String[] T00572_A2079UsuarioNotifica_NoStatus ;
      private int[] T00572_A2076UsuarioNotifica_UsuarioCod ;
      private int[] T00572_A2075UsuarioNotifica_AreaTrabalhoCod ;
      private int[] T005712_A2077UsuarioNotifica_Codigo ;
      private int[] T005715_A2077UsuarioNotifica_Codigo ;
      private int[] T005716_A57Usuario_PessoaCod ;
      private int[] T005716_A1Usuario_Codigo ;
      private String[] T005716_A58Usuario_PessoaNom ;
      private bool[] T005716_n58Usuario_PessoaNom ;
      private int[] T005717_A2076UsuarioNotifica_UsuarioCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class usuarionotifica__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00576 ;
          prmT00576 = new Object[] {
          new Object[] {"@UsuarioNotifica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00574 ;
          prmT00574 = new Object[] {
          new Object[] {"@UsuarioNotifica_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00575 ;
          prmT00575 = new Object[] {
          new Object[] {"@UsuarioNotifica_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00577 ;
          prmT00577 = new Object[] {
          new Object[] {"@UsuarioNotifica_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00578 ;
          prmT00578 = new Object[] {
          new Object[] {"@UsuarioNotifica_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00579 ;
          prmT00579 = new Object[] {
          new Object[] {"@UsuarioNotifica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00573 ;
          prmT00573 = new Object[] {
          new Object[] {"@UsuarioNotifica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005710 ;
          prmT005710 = new Object[] {
          new Object[] {"@UsuarioNotifica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005711 ;
          prmT005711 = new Object[] {
          new Object[] {"@UsuarioNotifica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00572 ;
          prmT00572 = new Object[] {
          new Object[] {"@UsuarioNotifica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005712 ;
          prmT005712 = new Object[] {
          new Object[] {"@UsuarioNotifica_NoStatus",SqlDbType.VarChar,40,0} ,
          new Object[] {"@UsuarioNotifica_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioNotifica_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005713 ;
          prmT005713 = new Object[] {
          new Object[] {"@UsuarioNotifica_NoStatus",SqlDbType.VarChar,40,0} ,
          new Object[] {"@UsuarioNotifica_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioNotifica_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioNotifica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005714 ;
          prmT005714 = new Object[] {
          new Object[] {"@UsuarioNotifica_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005715 ;
          prmT005715 = new Object[] {
          } ;
          Object[] prmT005716 ;
          prmT005716 = new Object[] {
          } ;
          Object[] prmT005717 ;
          prmT005717 = new Object[] {
          new Object[] {"@UsuarioNotifica_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00572", "SELECT [UsuarioNotifica_Codigo], [UsuarioNotifica_NoStatus], [UsuarioNotifica_UsuarioCod] AS UsuarioNotifica_UsuarioCod, [UsuarioNotifica_AreaTrabalhoCod] AS UsuarioNotifica_AreaTrabalhoCod FROM [UsuarioNotifica] WITH (UPDLOCK) WHERE [UsuarioNotifica_Codigo] = @UsuarioNotifica_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00572,1,0,true,false )
             ,new CursorDef("T00573", "SELECT [UsuarioNotifica_Codigo], [UsuarioNotifica_NoStatus], [UsuarioNotifica_UsuarioCod] AS UsuarioNotifica_UsuarioCod, [UsuarioNotifica_AreaTrabalhoCod] AS UsuarioNotifica_AreaTrabalhoCod FROM [UsuarioNotifica] WITH (NOLOCK) WHERE [UsuarioNotifica_Codigo] = @UsuarioNotifica_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00573,1,0,true,false )
             ,new CursorDef("T00574", "SELECT [Usuario_Codigo] AS UsuarioNotifica_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @UsuarioNotifica_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00574,1,0,true,false )
             ,new CursorDef("T00575", "SELECT [AreaTrabalho_Codigo] AS UsuarioNotifica_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @UsuarioNotifica_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00575,1,0,true,false )
             ,new CursorDef("T00576", "SELECT TM1.[UsuarioNotifica_Codigo], TM1.[UsuarioNotifica_NoStatus], TM1.[UsuarioNotifica_UsuarioCod] AS UsuarioNotifica_UsuarioCod, TM1.[UsuarioNotifica_AreaTrabalhoCod] AS UsuarioNotifica_AreaTrabalhoCod FROM [UsuarioNotifica] TM1 WITH (NOLOCK) WHERE TM1.[UsuarioNotifica_Codigo] = @UsuarioNotifica_Codigo ORDER BY TM1.[UsuarioNotifica_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00576,100,0,true,false )
             ,new CursorDef("T00577", "SELECT [Usuario_Codigo] AS UsuarioNotifica_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @UsuarioNotifica_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00577,1,0,true,false )
             ,new CursorDef("T00578", "SELECT [AreaTrabalho_Codigo] AS UsuarioNotifica_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @UsuarioNotifica_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00578,1,0,true,false )
             ,new CursorDef("T00579", "SELECT [UsuarioNotifica_Codigo] FROM [UsuarioNotifica] WITH (NOLOCK) WHERE [UsuarioNotifica_Codigo] = @UsuarioNotifica_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00579,1,0,true,false )
             ,new CursorDef("T005710", "SELECT TOP 1 [UsuarioNotifica_Codigo] FROM [UsuarioNotifica] WITH (NOLOCK) WHERE ( [UsuarioNotifica_Codigo] > @UsuarioNotifica_Codigo) ORDER BY [UsuarioNotifica_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005710,1,0,true,true )
             ,new CursorDef("T005711", "SELECT TOP 1 [UsuarioNotifica_Codigo] FROM [UsuarioNotifica] WITH (NOLOCK) WHERE ( [UsuarioNotifica_Codigo] < @UsuarioNotifica_Codigo) ORDER BY [UsuarioNotifica_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005711,1,0,true,true )
             ,new CursorDef("T005712", "INSERT INTO [UsuarioNotifica]([UsuarioNotifica_NoStatus], [UsuarioNotifica_UsuarioCod], [UsuarioNotifica_AreaTrabalhoCod]) VALUES(@UsuarioNotifica_NoStatus, @UsuarioNotifica_UsuarioCod, @UsuarioNotifica_AreaTrabalhoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT005712)
             ,new CursorDef("T005713", "UPDATE [UsuarioNotifica] SET [UsuarioNotifica_NoStatus]=@UsuarioNotifica_NoStatus, [UsuarioNotifica_UsuarioCod]=@UsuarioNotifica_UsuarioCod, [UsuarioNotifica_AreaTrabalhoCod]=@UsuarioNotifica_AreaTrabalhoCod  WHERE [UsuarioNotifica_Codigo] = @UsuarioNotifica_Codigo", GxErrorMask.GX_NOMASK,prmT005713)
             ,new CursorDef("T005714", "DELETE FROM [UsuarioNotifica]  WHERE [UsuarioNotifica_Codigo] = @UsuarioNotifica_Codigo", GxErrorMask.GX_NOMASK,prmT005714)
             ,new CursorDef("T005715", "SELECT [UsuarioNotifica_Codigo] FROM [UsuarioNotifica] WITH (NOLOCK) ORDER BY [UsuarioNotifica_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005715,100,0,true,false )
             ,new CursorDef("T005716", "SELECT T2.[Pessoa_Codigo] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ORDER BY T2.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005716,0,0,true,false )
             ,new CursorDef("T005717", "SELECT [Usuario_Codigo] AS UsuarioNotifica_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @UsuarioNotifica_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005717,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
