/*
               File: REL_RelatorioComparacaoDemandasXLS
        Description: Relat�rio de Compara��o entre Demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 3:11:36.58
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_relatoriocomparacaodemandasxls : GXProcedure
   {
      public arel_relatoriocomparacaodemandasxls( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_relatoriocomparacaodemandasxls( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_ContagemResultado_ContratadaCod ,
                           int aP2_ContagemResultado_ServicoGrupo ,
                           int aP3_ContagemResultado_Servico ,
                           int aP4_ContagemResultado_CntadaOsVinc ,
                           int aP5_ContagemResultado_SerGrupoVinc ,
                           int aP6_ContagemResultado_CodSrvVnc ,
                           String aP7_GridStateXML ,
                           out String aP8_Filename ,
                           out String aP9_ErrorMessage )
      {
         this.AV46Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV14ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         this.AV36ContagemResultado_ServicoGrupo = aP2_ContagemResultado_ServicoGrupo;
         this.AV35ContagemResultado_Servico = aP3_ContagemResultado_Servico;
         this.AV12ContagemResultado_CntadaOsVinc = aP4_ContagemResultado_CntadaOsVinc;
         this.AV34ContagemResultado_SerGrupoVinc = aP5_ContagemResultado_SerGrupoVinc;
         this.AV13ContagemResultado_CodSrvVnc = aP6_ContagemResultado_CodSrvVnc;
         this.AV62GridStateXML = aP7_GridStateXML;
         this.AV58Filename = "" ;
         this.AV55ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP8_Filename=this.AV58Filename;
         aP9_ErrorMessage=this.AV55ErrorMessage;
      }

      public String executeUdp( int aP0_Contratada_AreaTrabalhoCod ,
                                int aP1_ContagemResultado_ContratadaCod ,
                                int aP2_ContagemResultado_ServicoGrupo ,
                                int aP3_ContagemResultado_Servico ,
                                int aP4_ContagemResultado_CntadaOsVinc ,
                                int aP5_ContagemResultado_SerGrupoVinc ,
                                int aP6_ContagemResultado_CodSrvVnc ,
                                String aP7_GridStateXML ,
                                out String aP8_Filename )
      {
         this.AV46Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV14ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         this.AV36ContagemResultado_ServicoGrupo = aP2_ContagemResultado_ServicoGrupo;
         this.AV35ContagemResultado_Servico = aP3_ContagemResultado_Servico;
         this.AV12ContagemResultado_CntadaOsVinc = aP4_ContagemResultado_CntadaOsVinc;
         this.AV34ContagemResultado_SerGrupoVinc = aP5_ContagemResultado_SerGrupoVinc;
         this.AV13ContagemResultado_CodSrvVnc = aP6_ContagemResultado_CodSrvVnc;
         this.AV62GridStateXML = aP7_GridStateXML;
         this.AV58Filename = "" ;
         this.AV55ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP8_Filename=this.AV58Filename;
         aP9_ErrorMessage=this.AV55ErrorMessage;
         return AV55ErrorMessage ;
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_ContagemResultado_ContratadaCod ,
                                 int aP2_ContagemResultado_ServicoGrupo ,
                                 int aP3_ContagemResultado_Servico ,
                                 int aP4_ContagemResultado_CntadaOsVinc ,
                                 int aP5_ContagemResultado_SerGrupoVinc ,
                                 int aP6_ContagemResultado_CodSrvVnc ,
                                 String aP7_GridStateXML ,
                                 out String aP8_Filename ,
                                 out String aP9_ErrorMessage )
      {
         arel_relatoriocomparacaodemandasxls objarel_relatoriocomparacaodemandasxls;
         objarel_relatoriocomparacaodemandasxls = new arel_relatoriocomparacaodemandasxls();
         objarel_relatoriocomparacaodemandasxls.AV46Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objarel_relatoriocomparacaodemandasxls.AV14ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         objarel_relatoriocomparacaodemandasxls.AV36ContagemResultado_ServicoGrupo = aP2_ContagemResultado_ServicoGrupo;
         objarel_relatoriocomparacaodemandasxls.AV35ContagemResultado_Servico = aP3_ContagemResultado_Servico;
         objarel_relatoriocomparacaodemandasxls.AV12ContagemResultado_CntadaOsVinc = aP4_ContagemResultado_CntadaOsVinc;
         objarel_relatoriocomparacaodemandasxls.AV34ContagemResultado_SerGrupoVinc = aP5_ContagemResultado_SerGrupoVinc;
         objarel_relatoriocomparacaodemandasxls.AV13ContagemResultado_CodSrvVnc = aP6_ContagemResultado_CodSrvVnc;
         objarel_relatoriocomparacaodemandasxls.AV62GridStateXML = aP7_GridStateXML;
         objarel_relatoriocomparacaodemandasxls.AV58Filename = "" ;
         objarel_relatoriocomparacaodemandasxls.AV55ErrorMessage = "" ;
         objarel_relatoriocomparacaodemandasxls.context.SetSubmitInitialConfig(context);
         objarel_relatoriocomparacaodemandasxls.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_relatoriocomparacaodemandasxls);
         aP8_Filename=this.AV58Filename;
         aP9_ErrorMessage=this.AV55ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_relatoriocomparacaodemandasxls)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV66WWPContext) ;
         AV8CellRow = 1;
         AV59FirstColumn = 1;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'OPENDOCUMENT' Routine */
         AV65Random = (int)(NumberUtil.Random( )*10000);
         AV58Filename = "RelatorioComparacaoDemandas-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV65Random), 8, 0)) + ".xlsx";
         AV57ExcelDocument.Open(AV58Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV57ExcelDocument.Clear();
      }

      protected void S131( )
      {
         /* 'WRITEMAINTITLE' Routine */
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Relat�rio de Compara��o entre Demandas";
         AV8CellRow = (int)(AV8CellRow+2);
      }

      protected void S141( )
      {
         /* 'WRITEFILTERS' Routine */
         AV8CellRow = (int)(AV8CellRow+1);
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Color = 3;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Text = "Filtros:";
         if ( ! ( (0==AV46Contratada_AreaTrabalhoCod) ) )
         {
            AV8CellRow = (int)(AV8CellRow+1);
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Bold = 1;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Color = 3;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Text = "�rea de Trabalho";
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
            /* Execute user subroutine: 'BUSCA.AREA' */
            S151 ();
            if (returnInSub) return;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV71AreaTrabalho_Descricao;
         }
         if ( ! ( (0==AV14ContagemResultado_ContratadaCod) ) )
         {
            AV8CellRow = (int)(AV8CellRow+1);
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Bold = 1;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Color = 3;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Text = "Contratada";
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
            AV69Contratada_CodigoAux = AV14ContagemResultado_ContratadaCod;
            /* Execute user subroutine: 'BUSCA.CONTRATADA' */
            S161 ();
            if (returnInSub) return;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV70Contratada_Sigla;
         }
         if ( ! ( (0==AV36ContagemResultado_ServicoGrupo) ) )
         {
            AV8CellRow = (int)(AV8CellRow+1);
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Bold = 1;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Color = 3;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Text = "Grupo de Servi�os";
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
            AV69Contratada_CodigoAux = AV14ContagemResultado_ContratadaCod;
            AV74ServicoGrupo_CodigoAux = AV36ContagemResultado_ServicoGrupo;
            /* Execute user subroutine: 'BUSCA.GRUPO.SERVICO' */
            S171 ();
            if (returnInSub) return;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV75ServicoGrupo_Descricao;
         }
         if ( ! ( (0==AV35ContagemResultado_Servico) ) )
         {
            AV8CellRow = (int)(AV8CellRow+1);
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Bold = 1;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Color = 3;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Text = "Servi�o";
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
            AV72Servico_codigoAux = AV35ContagemResultado_Servico;
            /* Execute user subroutine: 'BUSCA.SERVICO' */
            S181 ();
            if (returnInSub) return;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV73Servico_Sigla;
         }
         if ( ! ( (0==AV12ContagemResultado_CntadaOsVinc) ) )
         {
            AV8CellRow = (int)(AV8CellRow+1);
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Bold = 1;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Color = 3;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Text = "Contratada";
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
            AV69Contratada_CodigoAux = AV12ContagemResultado_CntadaOsVinc;
            /* Execute user subroutine: 'BUSCA.CONTRATADA' */
            S161 ();
            if (returnInSub) return;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV70Contratada_Sigla;
         }
         if ( ! ( (0==AV34ContagemResultado_SerGrupoVinc) ) )
         {
            AV8CellRow = (int)(AV8CellRow+1);
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Bold = 1;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Color = 3;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Text = "Grupo de Servi�os";
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Number = AV34ContagemResultado_SerGrupoVinc;
            /* Execute user subroutine: 'BUSCA.GRUPO.SERVICO' */
            S171 ();
            if (returnInSub) return;
            AV69Contratada_CodigoAux = AV12ContagemResultado_CntadaOsVinc;
            AV74ServicoGrupo_CodigoAux = AV34ContagemResultado_SerGrupoVinc;
            /* Execute user subroutine: 'BUSCA.GRUPO.SERVICO' */
            S171 ();
            if (returnInSub) return;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV75ServicoGrupo_Descricao;
         }
         if ( ! ( (0==AV13ContagemResultado_CodSrvVnc) ) )
         {
            AV8CellRow = (int)(AV8CellRow+1);
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Bold = 1;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Color = 3;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Text = "Servi�o";
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Number = AV13ContagemResultado_CodSrvVnc;
            AV72Servico_codigoAux = AV13ContagemResultado_CodSrvVnc;
            /* Execute user subroutine: 'BUSCA.SERVICO' */
            S181 ();
            if (returnInSub) return;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV73Servico_Sigla;
         }
         AV60GridState.gxTpr_Dynamicfilters.FromXml(AV62GridStateXML, "");
         if ( AV60GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV61GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV60GridState.gxTpr_Dynamicfilters.Item(1));
            AV52DynamicFiltersSelector1 = AV61GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV49DynamicFiltersOperator1 = AV61GridStateDynamicFilter.gxTpr_Operator;
               AV27ContagemResultado_DemandaFM1 = AV61GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27ContagemResultado_DemandaFM1)) )
               {
                  AV8CellRow = (int)(AV8CellRow+1);
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                  if ( AV49DynamicFiltersOperator1 == 0 )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                  }
                  else if ( AV49DynamicFiltersOperator1 == 1 )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                  }
                  else if ( AV49DynamicFiltersOperator1 == 2 )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                  }
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV27ContagemResultado_DemandaFM1;
               }
            }
            else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV49DynamicFiltersOperator1 = AV61GridStateDynamicFilter.gxTpr_Operator;
               AV18ContagemResultado_DataDmn1 = context.localUtil.CToD( AV61GridStateDynamicFilter.gxTpr_Value, 2);
               AV15ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV61GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV18ContagemResultado_DataDmn1) || ! (DateTime.MinValue==AV15ContagemResultado_DataDmn_To1) )
               {
                  AV8CellRow = (int)(AV8CellRow+1);
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                  if ( AV49DynamicFiltersOperator1 == 0 )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Data da Demanda (Per�odo)";
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Bold = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Color = 3;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Text = "�";
                  }
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV18ContagemResultado_DataDmn1 ) ;
                  AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV15ContagemResultado_DataDmn_To1 ) ;
                  AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
            {
               AV49DynamicFiltersOperator1 = AV61GridStateDynamicFilter.gxTpr_Operator;
               AV24ContagemResultado_DataEntregaReal1 = context.localUtil.CToT( AV61GridStateDynamicFilter.gxTpr_Value, 2);
               AV21ContagemResultado_DataEntregaReal_To1 = context.localUtil.CToT( AV61GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV24ContagemResultado_DataEntregaReal1) || ! (DateTime.MinValue==AV21ContagemResultado_DataEntregaReal_To1) )
               {
                  AV8CellRow = (int)(AV8CellRow+1);
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                  if ( AV49DynamicFiltersOperator1 == 0 )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Data de Entrega (Per�odo)";
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Bold = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Color = 3;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Text = "�";
                  }
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                  AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Date = AV24ContagemResultado_DataEntregaReal1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Italic = 1;
                  AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Date = AV21ContagemResultado_DataEntregaReal_To1;
               }
            }
            else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV43ContagemResultado_StatusDmn1 = AV61GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContagemResultado_StatusDmn1)) )
               {
                  AV8CellRow = (int)(AV8CellRow+1);
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Status da Demanda";
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContagemResultado_StatusDmn1)) )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV43ContagemResultado_StatusDmn1);
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
            {
               AV40ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( AV61GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV40ContagemResultado_StatusCnt1) )
               {
                  AV8CellRow = (int)(AV8CellRow+1);
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Status do Servi�o";
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                  if ( ! (0==AV40ContagemResultado_StatusCnt1) )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV40ContagemResultado_StatusCnt1);
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV49DynamicFiltersOperator1 = AV61GridStateDynamicFilter.gxTpr_Operator;
               AV30ContagemResultado_Descricao1 = AV61GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContagemResultado_Descricao1)) )
               {
                  AV8CellRow = (int)(AV8CellRow+1);
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                  if ( AV49DynamicFiltersOperator1 == 0 )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "T�tulo (Come�a com)";
                  }
                  else if ( AV49DynamicFiltersOperator1 == 1 )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "T�tulo (Cont�m)";
                  }
                  else if ( AV49DynamicFiltersOperator1 == 2 )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "T�tulo (Igual)";
                  }
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV30ContagemResultado_Descricao1;
               }
            }
            else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV37ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV61GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV37ContagemResultado_SistemaCod1) )
               {
                  AV8CellRow = (int)(AV8CellRow+1);
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Sistema";
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                  AV76Sistema_CodigoAux = AV37ContagemResultado_SistemaCod1;
                  /* Execute user subroutine: 'BUSCA.SISTEMA' */
                  S191 ();
                  if (returnInSub) return;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV77Sistema_Sigla;
               }
            }
            else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV49DynamicFiltersOperator1 = AV61GridStateDynamicFilter.gxTpr_Operator;
               AV9ContagemResultado_Agrupador1 = AV61GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9ContagemResultado_Agrupador1)) )
               {
                  AV8CellRow = (int)(AV8CellRow+1);
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                  if ( AV49DynamicFiltersOperator1 == 0 )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Agrupador (Igual)";
                  }
                  else if ( AV49DynamicFiltersOperator1 == 1 )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Agrupador (Come�a com)";
                  }
                  else if ( AV49DynamicFiltersOperator1 == 2 )
                  {
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Agrupador (Cont�m)";
                  }
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                  AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV9ContagemResultado_Agrupador1;
               }
            }
            if ( AV60GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV47DynamicFiltersEnabled2 = true;
               AV61GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV60GridState.gxTpr_Dynamicfilters.Item(2));
               AV53DynamicFiltersSelector2 = AV61GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
               {
                  AV50DynamicFiltersOperator2 = AV61GridStateDynamicFilter.gxTpr_Operator;
                  AV28ContagemResultado_DemandaFM2 = AV61GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28ContagemResultado_DemandaFM2)) )
                  {
                     AV8CellRow = (int)(AV8CellRow+1);
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                     if ( AV50DynamicFiltersOperator2 == 0 )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                     }
                     else if ( AV50DynamicFiltersOperator2 == 1 )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                     }
                     else if ( AV50DynamicFiltersOperator2 == 2 )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                     }
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV28ContagemResultado_DemandaFM2;
                  }
               }
               else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV50DynamicFiltersOperator2 = AV61GridStateDynamicFilter.gxTpr_Operator;
                  AV19ContagemResultado_DataDmn2 = context.localUtil.CToD( AV61GridStateDynamicFilter.gxTpr_Value, 2);
                  AV16ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV61GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV19ContagemResultado_DataDmn2) || ! (DateTime.MinValue==AV16ContagemResultado_DataDmn_To2) )
                  {
                     AV8CellRow = (int)(AV8CellRow+1);
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                     if ( AV50DynamicFiltersOperator2 == 0 )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Data da Demanda (Per�odo)";
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Bold = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Color = 3;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Text = "�";
                     }
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV19ContagemResultado_DataDmn2 ) ;
                     AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV16ContagemResultado_DataDmn_To2 ) ;
                     AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
               {
                  AV50DynamicFiltersOperator2 = AV61GridStateDynamicFilter.gxTpr_Operator;
                  AV25ContagemResultado_DataEntregaReal2 = context.localUtil.CToT( AV61GridStateDynamicFilter.gxTpr_Value, 2);
                  AV22ContagemResultado_DataEntregaReal_To2 = context.localUtil.CToT( AV61GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV25ContagemResultado_DataEntregaReal2) || ! (DateTime.MinValue==AV22ContagemResultado_DataEntregaReal_To2) )
                  {
                     AV8CellRow = (int)(AV8CellRow+1);
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                     if ( AV50DynamicFiltersOperator2 == 0 )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Data de Entrega (Per�odo)";
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Bold = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Color = 3;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Text = "�";
                     }
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                     AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Date = AV25ContagemResultado_DataEntregaReal2;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Italic = 1;
                     AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Date = AV22ContagemResultado_DataEntregaReal_To2;
                  }
               }
               else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV44ContagemResultado_StatusDmn2 = AV61GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContagemResultado_StatusDmn2)) )
                  {
                     AV8CellRow = (int)(AV8CellRow+1);
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Status da Demanda";
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContagemResultado_StatusDmn2)) )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV44ContagemResultado_StatusDmn2);
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
               {
                  AV41ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( AV61GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV41ContagemResultado_StatusCnt2) )
                  {
                     AV8CellRow = (int)(AV8CellRow+1);
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Status do Servi�o";
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                     if ( ! (0==AV41ContagemResultado_StatusCnt2) )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV41ContagemResultado_StatusCnt2);
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV50DynamicFiltersOperator2 = AV61GridStateDynamicFilter.gxTpr_Operator;
                  AV31ContagemResultado_Descricao2 = AV61GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContagemResultado_Descricao2)) )
                  {
                     AV8CellRow = (int)(AV8CellRow+1);
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                     if ( AV50DynamicFiltersOperator2 == 0 )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "T�tulo (Come�a com)";
                     }
                     else if ( AV50DynamicFiltersOperator2 == 1 )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "T�tulo (Cont�m)";
                     }
                     else if ( AV50DynamicFiltersOperator2 == 2 )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "T�tulo (Igual)";
                     }
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV31ContagemResultado_Descricao2;
                  }
               }
               else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV38ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV61GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV38ContagemResultado_SistemaCod2) )
                  {
                     AV8CellRow = (int)(AV8CellRow+1);
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Sistema";
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                     AV76Sistema_CodigoAux = AV38ContagemResultado_SistemaCod2;
                     /* Execute user subroutine: 'BUSCA.SISTEMA' */
                     S191 ();
                     if (returnInSub) return;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV77Sistema_Sigla;
                  }
               }
               else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV50DynamicFiltersOperator2 = AV61GridStateDynamicFilter.gxTpr_Operator;
                  AV10ContagemResultado_Agrupador2 = AV61GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10ContagemResultado_Agrupador2)) )
                  {
                     AV8CellRow = (int)(AV8CellRow+1);
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                     if ( AV50DynamicFiltersOperator2 == 0 )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Agrupador (Igual)";
                     }
                     else if ( AV50DynamicFiltersOperator2 == 1 )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Agrupador (Come�a com)";
                     }
                     else if ( AV50DynamicFiltersOperator2 == 2 )
                     {
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Agrupador (Cont�m)";
                     }
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                     AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV10ContagemResultado_Agrupador2;
                  }
               }
               if ( AV60GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV48DynamicFiltersEnabled3 = true;
                  AV61GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV60GridState.gxTpr_Dynamicfilters.Item(3));
                  AV54DynamicFiltersSelector3 = AV61GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV61GridStateDynamicFilter.gxTpr_Operator;
                     AV29ContagemResultado_DemandaFM3 = AV61GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29ContagemResultado_DemandaFM3)) )
                     {
                        AV8CellRow = (int)(AV8CellRow+1);
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                        if ( AV51DynamicFiltersOperator3 == 0 )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 1 )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 2 )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                        }
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV29ContagemResultado_DemandaFM3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV61GridStateDynamicFilter.gxTpr_Operator;
                     AV20ContagemResultado_DataDmn3 = context.localUtil.CToD( AV61GridStateDynamicFilter.gxTpr_Value, 2);
                     AV17ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV61GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV20ContagemResultado_DataDmn3) || ! (DateTime.MinValue==AV17ContagemResultado_DataDmn_To3) )
                     {
                        AV8CellRow = (int)(AV8CellRow+1);
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                        if ( AV51DynamicFiltersOperator3 == 0 )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Data da Demanda (Per�odo)";
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Bold = 1;
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Color = 3;
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Text = "�";
                        }
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV20ContagemResultado_DataDmn3 ) ;
                        AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV17ContagemResultado_DataDmn_To3 ) ;
                        AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV61GridStateDynamicFilter.gxTpr_Operator;
                     AV26ContagemResultado_DataEntregaReal3 = context.localUtil.CToT( AV61GridStateDynamicFilter.gxTpr_Value, 2);
                     AV23ContagemResultado_DataEntregaReal_To3 = context.localUtil.CToT( AV61GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV26ContagemResultado_DataEntregaReal3) || ! (DateTime.MinValue==AV23ContagemResultado_DataEntregaReal_To3) )
                     {
                        AV8CellRow = (int)(AV8CellRow+1);
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                        if ( AV51DynamicFiltersOperator3 == 0 )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Data de Entrega (Per�odo)";
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Bold = 1;
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Color = 3;
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Text = "�";
                        }
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                        AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Date = AV26ContagemResultado_DataEntregaReal3;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Italic = 1;
                        AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Date = AV23ContagemResultado_DataEntregaReal_To3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV45ContagemResultado_StatusDmn3 = AV61GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45ContagemResultado_StatusDmn3)) )
                     {
                        AV8CellRow = (int)(AV8CellRow+1);
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Status da Demanda";
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45ContagemResultado_StatusDmn3)) )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV45ContagemResultado_StatusDmn3);
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
                  {
                     AV42ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( AV61GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV42ContagemResultado_StatusCnt3) )
                     {
                        AV8CellRow = (int)(AV8CellRow+1);
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Status do Servi�o";
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                        if ( ! (0==AV42ContagemResultado_StatusCnt3) )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV42ContagemResultado_StatusCnt3);
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV61GridStateDynamicFilter.gxTpr_Operator;
                     AV32ContagemResultado_Descricao3 = AV61GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContagemResultado_Descricao3)) )
                     {
                        AV8CellRow = (int)(AV8CellRow+1);
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                        if ( AV51DynamicFiltersOperator3 == 0 )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "T�tulo (Come�a com)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 1 )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "T�tulo (Cont�m)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 2 )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "T�tulo (Igual)";
                        }
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV32ContagemResultado_Descricao3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV39ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV61GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV39ContagemResultado_SistemaCod3) )
                     {
                        AV8CellRow = (int)(AV8CellRow+1);
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Sistema";
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                        AV76Sistema_CodigoAux = AV39ContagemResultado_SistemaCod3;
                        /* Execute user subroutine: 'BUSCA.SISTEMA' */
                        S191 ();
                        if (returnInSub) return;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV77Sistema_Sigla;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV61GridStateDynamicFilter.gxTpr_Operator;
                     AV11ContagemResultado_Agrupador3 = AV61GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11ContagemResultado_Agrupador3)) )
                     {
                        AV8CellRow = (int)(AV8CellRow+1);
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Bold = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Color = 3;
                        if ( AV51DynamicFiltersOperator3 == 0 )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Agrupador (Igual)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 1 )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Agrupador (Come�a com)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 2 )
                        {
                           AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn, 1, 1).Text = "Agrupador (Cont�m)";
                        }
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Italic = 1;
                        AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = AV11ContagemResultado_Agrupador3;
                     }
                  }
               }
            }
         }
         AV8CellRow = (int)(AV8CellRow+2);
      }

      protected void S201( )
      {
         /* 'WRITECOLUMNTITLES' Routine */
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Text = "OS";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Text = "Data da Demanda";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Text = "Data do Esfor�o";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Text = "Prestadora";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+4, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+4, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+4, 1, 1).Text = "T�tulo";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+5, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+5, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+5, 1, 1).Text = "Esfor�o";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+6, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+6, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+6, 1, 1).Text = "OS";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+7, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+7, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+7, 1, 1).Text = "Data da Demanda";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+8, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+8, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+8, 1, 1).Text = "Data do Esfor�o";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+9, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+9, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+9, 1, 1).Text = "Prestadora";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+10, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+10, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+10, 1, 1).Text = "T�tulo";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+11, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+11, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+11, 1, 1).Text = "Esfor�o";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+12, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+12, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+12, 1, 1).Text = "Diferen�a";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+13, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+13, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+13, 1, 1).Text = "Percentual";
      }

      protected void S211( )
      {
         /* 'WRITEDATA' Routine */
         AV81WP_RelatorioComparacaoDemandasDS_1_Contratada_areatrabalhocod = AV46Contratada_AreaTrabalhoCod;
         AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod = AV14ContagemResultado_ContratadaCod;
         AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo = AV36ContagemResultado_ServicoGrupo;
         AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico = AV35ContagemResultado_Servico;
         AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1 = AV52DynamicFiltersSelector1;
         AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 = AV49DynamicFiltersOperator1;
         AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 = AV27ContagemResultado_DemandaFM1;
         AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1 = AV18ContagemResultado_DataDmn1;
         AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1 = AV15ContagemResultado_DataDmn_To1;
         AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1 = AV24ContagemResultado_DataEntregaReal1;
         AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1 = AV21ContagemResultado_DataEntregaReal_To1;
         AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1 = AV43ContagemResultado_StatusDmn1;
         AV93WP_RelatorioComparacaoDemandasDS_13_Contagemresultado_statuscnt1 = AV40ContagemResultado_StatusCnt1;
         AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 = AV30ContagemResultado_Descricao1;
         AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1 = AV37ContagemResultado_SistemaCod1;
         AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 = AV9ContagemResultado_Agrupador1;
         AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2 = AV53DynamicFiltersSelector2;
         AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 = AV50DynamicFiltersOperator2;
         AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 = AV28ContagemResultado_DemandaFM2;
         AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2 = AV19ContagemResultado_DataDmn2;
         AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2 = AV16ContagemResultado_DataDmn_To2;
         AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2 = AV25ContagemResultado_DataEntregaReal2;
         AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2 = AV22ContagemResultado_DataEntregaReal_To2;
         AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2 = AV44ContagemResultado_StatusDmn2;
         AV106WP_RelatorioComparacaoDemandasDS_26_Contagemresultado_statuscnt2 = AV41ContagemResultado_StatusCnt2;
         AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 = AV31ContagemResultado_Descricao2;
         AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2 = AV38ContagemResultado_SistemaCod2;
         AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 = AV10ContagemResultado_Agrupador2;
         AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 = AV48DynamicFiltersEnabled3;
         AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3 = AV54DynamicFiltersSelector3;
         AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 = AV29ContagemResultado_DemandaFM3;
         AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3 = AV20ContagemResultado_DataDmn3;
         AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3 = AV17ContagemResultado_DataDmn_To3;
         AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3 = AV26ContagemResultado_DataEntregaReal3;
         AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3 = AV23ContagemResultado_DataEntregaReal_To3;
         AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3 = AV45ContagemResultado_StatusDmn3;
         AV119WP_RelatorioComparacaoDemandasDS_39_Contagemresultado_statuscnt3 = AV42ContagemResultado_StatusCnt3;
         AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 = AV32ContagemResultado_Descricao3;
         AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3 = AV39ContagemResultado_SistemaCod3;
         AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 = AV11ContagemResultado_Agrupador3;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod ,
                                              AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo ,
                                              AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico ,
                                              AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1 ,
                                              AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 ,
                                              AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 ,
                                              AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1 ,
                                              AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1 ,
                                              AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1 ,
                                              AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1 ,
                                              AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1 ,
                                              AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 ,
                                              AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1 ,
                                              AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 ,
                                              AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 ,
                                              AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2 ,
                                              AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 ,
                                              AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 ,
                                              AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2 ,
                                              AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2 ,
                                              AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2 ,
                                              AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2 ,
                                              AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2 ,
                                              AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 ,
                                              AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2 ,
                                              AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 ,
                                              AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 ,
                                              AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3 ,
                                              AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 ,
                                              AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 ,
                                              AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3 ,
                                              AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3 ,
                                              AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3 ,
                                              AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3 ,
                                              AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3 ,
                                              AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 ,
                                              AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3 ,
                                              AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A764ContagemResultado_ServicoGrupo ,
                                              A601ContagemResultado_Servico ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A2017ContagemResultado_DataEntregaReal ,
                                              A484ContagemResultado_StatusDmn ,
                                              A494ContagemResultado_Descricao ,
                                              A489ContagemResultado_SistemaCod ,
                                              A1046ContagemResultado_Agrupador ,
                                              A602ContagemResultado_OSVinculada ,
                                              A456ContagemResultado_Codigo ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV66WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1), "%", "");
         lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1), "%", "");
         lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1), "%", "");
         lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1), "%", "");
         lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1), 15, "%");
         lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1), 15, "%");
         lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2), "%", "");
         lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2), "%", "");
         lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2), "%", "");
         lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2), "%", "");
         lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2), 15, "%");
         lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2), 15, "%");
         lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3), "%", "");
         lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3), "%", "");
         lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3), "%", "");
         lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3), "%", "");
         lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3), 15, "%");
         lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3), 15, "%");
         /* Using cursor P00YN3 */
         pr_default.execute(0, new Object[] {AV66WWPContext.gxTpr_Areatrabalho_codigo, AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod, AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo, AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico, AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1, lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1, lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1, AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1, AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1, AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1, AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1, AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1, lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1, lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1, AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1, AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1, AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1, lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1, lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1, AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2, lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2, lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2, AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2, AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2, AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2, AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2, AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2, lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2, lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2, AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2, AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2, AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2, lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2, lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2, AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3, lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3, lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3, AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3, AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3, AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3, AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3, AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3, lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3, lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3, AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3, AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3, AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3, lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3, lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00YN3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YN3_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P00YN3_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P00YN3_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00YN3_n602ContagemResultado_OSVinculada[0];
            A1046ContagemResultado_Agrupador = P00YN3_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YN3_n1046ContagemResultado_Agrupador[0];
            A489ContagemResultado_SistemaCod = P00YN3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YN3_n489ContagemResultado_SistemaCod[0];
            A494ContagemResultado_Descricao = P00YN3_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YN3_n494ContagemResultado_Descricao[0];
            A484ContagemResultado_StatusDmn = P00YN3_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YN3_n484ContagemResultado_StatusDmn[0];
            A2017ContagemResultado_DataEntregaReal = P00YN3_A2017ContagemResultado_DataEntregaReal[0];
            n2017ContagemResultado_DataEntregaReal = P00YN3_n2017ContagemResultado_DataEntregaReal[0];
            A471ContagemResultado_DataDmn = P00YN3_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00YN3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YN3_n493ContagemResultado_DemandaFM[0];
            A601ContagemResultado_Servico = P00YN3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YN3_n601ContagemResultado_Servico[0];
            A764ContagemResultado_ServicoGrupo = P00YN3_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00YN3_n764ContagemResultado_ServicoGrupo[0];
            A490ContagemResultado_ContratadaCod = P00YN3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YN3_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00YN3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00YN3_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P00YN3_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00YN3_n803ContagemResultado_ContratadaSigla[0];
            A1326ContagemResultado_ContratadaTipoFab = P00YN3_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00YN3_n1326ContagemResultado_ContratadaTipoFab[0];
            A457ContagemResultado_Demanda = P00YN3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YN3_n457ContagemResultado_Demanda[0];
            A684ContagemResultado_PFBFSUltima = P00YN3_A684ContagemResultado_PFBFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P00YN3_A682ContagemResultado_PFBFMUltima[0];
            A40000ContagemResultado_DataCnt = P00YN3_A40000ContagemResultado_DataCnt[0];
            n40000ContagemResultado_DataCnt = P00YN3_n40000ContagemResultado_DataCnt[0];
            A601ContagemResultado_Servico = P00YN3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YN3_n601ContagemResultado_Servico[0];
            A764ContagemResultado_ServicoGrupo = P00YN3_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00YN3_n764ContagemResultado_ServicoGrupo[0];
            A684ContagemResultado_PFBFSUltima = P00YN3_A684ContagemResultado_PFBFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P00YN3_A682ContagemResultado_PFBFMUltima[0];
            A40000ContagemResultado_DataCnt = P00YN3_A40000ContagemResultado_DataCnt[0];
            n40000ContagemResultado_DataCnt = P00YN3_n40000ContagemResultado_DataCnt[0];
            A52Contratada_AreaTrabalhoCod = P00YN3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00YN3_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P00YN3_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00YN3_n803ContagemResultado_ContratadaSigla[0];
            A1326ContagemResultado_ContratadaTipoFab = P00YN3_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00YN3_n1326ContagemResultado_ContratadaTipoFab[0];
            if ( new prc_filtrorelatoriocomparacaodemandas(context).executeUdp(  A456ContagemResultado_Codigo) )
            {
               AV8CellRow = (int)(AV8CellRow+1);
               /* Execute user subroutine: 'BEFOREWRITELINE' */
               S222 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  returnInSub = true;
                  if (true) return;
               }
               AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+0, 1, 1).Text = StringUtil.Format( "%1/%2", A493ContagemResultado_DemandaFM, A457ContagemResultado_Demanda, "", "", "", "", "", "", "");
               GXt_dtime1 = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
               AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
               AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+1, 1, 1).Date = GXt_dtime1;
               AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Color = 3;
               GXt_dtime1 = DateTimeUtil.ResetTime( A40000ContagemResultado_DataCnt ) ;
               AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
               AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+2, 1, 1).Date = GXt_dtime1;
               AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+3, 1, 1).Text = A803ContagemResultado_ContratadaSigla;
               AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+4, 1, 1).Text = A494ContagemResultado_Descricao;
               AV56Esforco = ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M")==0) ? A682ContagemResultado_PFBFMUltima : ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S")==0) ? A684ContagemResultado_PFBFSUltima : (decimal)(0)));
               AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+5, 1, 1).Number = (double)(AV56Esforco);
               /* Execute user subroutine: 'AFTERWRITELINE' */
               S232 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  returnInSub = true;
                  if (true) return;
               }
               AV33ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
               /* Execute user subroutine: 'CARREGA.DEMANDA.VINCULADA' */
               S242 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  returnInSub = true;
                  if (true) return;
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV8CellRow = (int)(AV8CellRow+2);
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+11, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+11, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+11, 1, 1).Text = "Total Geral";
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+12, 1, 1).Bold = 1;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+12, 1, 1).Color = 11;
         AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+12, 1, 1).Number = (double)(AV68TotalGeral);
      }

      protected void S242( )
      {
         /* 'CARREGA.DEMANDA.VINCULADA' Routine */
         AV81WP_RelatorioComparacaoDemandasDS_1_Contratada_areatrabalhocod = AV46Contratada_AreaTrabalhoCod;
         AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod = AV12ContagemResultado_CntadaOsVinc;
         AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo = AV34ContagemResultado_SerGrupoVinc;
         AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico = AV13ContagemResultado_CodSrvVnc;
         AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1 = AV52DynamicFiltersSelector1;
         AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 = AV49DynamicFiltersOperator1;
         AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 = AV27ContagemResultado_DemandaFM1;
         AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1 = AV18ContagemResultado_DataDmn1;
         AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1 = AV15ContagemResultado_DataDmn_To1;
         AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1 = AV24ContagemResultado_DataEntregaReal1;
         AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1 = AV21ContagemResultado_DataEntregaReal_To1;
         AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1 = AV43ContagemResultado_StatusDmn1;
         AV93WP_RelatorioComparacaoDemandasDS_13_Contagemresultado_statuscnt1 = AV40ContagemResultado_StatusCnt1;
         AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 = AV30ContagemResultado_Descricao1;
         AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1 = AV37ContagemResultado_SistemaCod1;
         AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 = AV9ContagemResultado_Agrupador1;
         AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 = AV47DynamicFiltersEnabled2;
         AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2 = AV53DynamicFiltersSelector2;
         AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 = AV50DynamicFiltersOperator2;
         AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 = AV28ContagemResultado_DemandaFM2;
         AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2 = AV19ContagemResultado_DataDmn2;
         AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2 = AV16ContagemResultado_DataDmn_To2;
         AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2 = AV25ContagemResultado_DataEntregaReal2;
         AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2 = AV22ContagemResultado_DataEntregaReal_To2;
         AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2 = AV44ContagemResultado_StatusDmn2;
         AV106WP_RelatorioComparacaoDemandasDS_26_Contagemresultado_statuscnt2 = AV41ContagemResultado_StatusCnt2;
         AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 = AV31ContagemResultado_Descricao2;
         AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2 = AV38ContagemResultado_SistemaCod2;
         AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 = AV10ContagemResultado_Agrupador2;
         AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 = AV48DynamicFiltersEnabled3;
         AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3 = AV54DynamicFiltersSelector3;
         AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 = AV29ContagemResultado_DemandaFM3;
         AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3 = AV20ContagemResultado_DataDmn3;
         AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3 = AV17ContagemResultado_DataDmn_To3;
         AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3 = AV26ContagemResultado_DataEntregaReal3;
         AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3 = AV23ContagemResultado_DataEntregaReal_To3;
         AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3 = AV45ContagemResultado_StatusDmn3;
         AV119WP_RelatorioComparacaoDemandasDS_39_Contagemresultado_statuscnt3 = AV42ContagemResultado_StatusCnt3;
         AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 = AV32ContagemResultado_Descricao3;
         AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3 = AV39ContagemResultado_SistemaCod3;
         AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 = AV11ContagemResultado_Agrupador3;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod ,
                                              AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo ,
                                              AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico ,
                                              AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1 ,
                                              AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 ,
                                              AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 ,
                                              AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1 ,
                                              AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1 ,
                                              AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1 ,
                                              AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1 ,
                                              AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1 ,
                                              AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 ,
                                              AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1 ,
                                              AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 ,
                                              AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 ,
                                              AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2 ,
                                              AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 ,
                                              AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 ,
                                              AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2 ,
                                              AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2 ,
                                              AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2 ,
                                              AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2 ,
                                              AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2 ,
                                              AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 ,
                                              AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2 ,
                                              AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 ,
                                              AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 ,
                                              AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3 ,
                                              AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 ,
                                              AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 ,
                                              AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3 ,
                                              AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3 ,
                                              AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3 ,
                                              AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3 ,
                                              AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3 ,
                                              AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 ,
                                              AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3 ,
                                              AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A764ContagemResultado_ServicoGrupo ,
                                              A601ContagemResultado_Servico ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A2017ContagemResultado_DataEntregaReal ,
                                              A484ContagemResultado_StatusDmn ,
                                              A494ContagemResultado_Descricao ,
                                              A489ContagemResultado_SistemaCod ,
                                              A1046ContagemResultado_Agrupador ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV66WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A456ContagemResultado_Codigo ,
                                              AV33ContagemResultado_OSVinculada },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1), "%", "");
         lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1), "%", "");
         lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1), "%", "");
         lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1), "%", "");
         lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1), 15, "%");
         lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1), 15, "%");
         lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2), "%", "");
         lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2), "%", "");
         lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2), "%", "");
         lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2), "%", "");
         lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2), 15, "%");
         lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2), 15, "%");
         lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3), "%", "");
         lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3), "%", "");
         lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3), "%", "");
         lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3), "%", "");
         lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3), 15, "%");
         lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3), 15, "%");
         /* Using cursor P00YN5 */
         pr_default.execute(1, new Object[] {AV66WWPContext.gxTpr_Areatrabalho_codigo, AV33ContagemResultado_OSVinculada, AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod, AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo, AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico, AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1, lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1, lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1, AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1, AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1, AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1, AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1, AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1, lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1, lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1, AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1, AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1, AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1, lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1, lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1, AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2, lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2, lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2, AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2, AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2, AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2, AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2, AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2, lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2, lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2, AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2, AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2, AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2, lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2, lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2, AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3, lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3, lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3, AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3, AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3, AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3, AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3, AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3, lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3, lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3, AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3, AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3, AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3, lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3, lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00YN5_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YN5_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P00YN5_A456ContagemResultado_Codigo[0];
            A1046ContagemResultado_Agrupador = P00YN5_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YN5_n1046ContagemResultado_Agrupador[0];
            A489ContagemResultado_SistemaCod = P00YN5_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YN5_n489ContagemResultado_SistemaCod[0];
            A494ContagemResultado_Descricao = P00YN5_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YN5_n494ContagemResultado_Descricao[0];
            A484ContagemResultado_StatusDmn = P00YN5_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YN5_n484ContagemResultado_StatusDmn[0];
            A2017ContagemResultado_DataEntregaReal = P00YN5_A2017ContagemResultado_DataEntregaReal[0];
            n2017ContagemResultado_DataEntregaReal = P00YN5_n2017ContagemResultado_DataEntregaReal[0];
            A471ContagemResultado_DataDmn = P00YN5_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00YN5_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YN5_n493ContagemResultado_DemandaFM[0];
            A601ContagemResultado_Servico = P00YN5_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YN5_n601ContagemResultado_Servico[0];
            A764ContagemResultado_ServicoGrupo = P00YN5_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00YN5_n764ContagemResultado_ServicoGrupo[0];
            A490ContagemResultado_ContratadaCod = P00YN5_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YN5_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00YN5_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00YN5_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P00YN5_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00YN5_n803ContagemResultado_ContratadaSigla[0];
            A1326ContagemResultado_ContratadaTipoFab = P00YN5_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00YN5_n1326ContagemResultado_ContratadaTipoFab[0];
            A457ContagemResultado_Demanda = P00YN5_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YN5_n457ContagemResultado_Demanda[0];
            A684ContagemResultado_PFBFSUltima = P00YN5_A684ContagemResultado_PFBFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P00YN5_A682ContagemResultado_PFBFMUltima[0];
            A40000ContagemResultado_DataCnt = P00YN5_A40000ContagemResultado_DataCnt[0];
            n40000ContagemResultado_DataCnt = P00YN5_n40000ContagemResultado_DataCnt[0];
            A601ContagemResultado_Servico = P00YN5_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YN5_n601ContagemResultado_Servico[0];
            A764ContagemResultado_ServicoGrupo = P00YN5_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00YN5_n764ContagemResultado_ServicoGrupo[0];
            A684ContagemResultado_PFBFSUltima = P00YN5_A684ContagemResultado_PFBFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P00YN5_A682ContagemResultado_PFBFMUltima[0];
            A40000ContagemResultado_DataCnt = P00YN5_A40000ContagemResultado_DataCnt[0];
            n40000ContagemResultado_DataCnt = P00YN5_n40000ContagemResultado_DataCnt[0];
            A52Contratada_AreaTrabalhoCod = P00YN5_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00YN5_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P00YN5_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00YN5_n803ContagemResultado_ContratadaSigla[0];
            A1326ContagemResultado_ContratadaTipoFab = P00YN5_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00YN5_n1326ContagemResultado_ContratadaTipoFab[0];
            /* Execute user subroutine: 'BEFOREWRITELINE' */
            S222 ();
            if ( returnInSub )
            {
               pr_default.close(1);
               returnInSub = true;
               if (true) return;
            }
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+6, 1, 1).Text = StringUtil.Format( "%1/%2", A493ContagemResultado_DemandaFM, A457ContagemResultado_Demanda, "", "", "", "", "", "", "");
            GXt_dtime1 = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
            AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+7, 1, 1).Date = GXt_dtime1;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+8, 1, 1).Color = 3;
            GXt_dtime1 = DateTimeUtil.ResetTime( A40000ContagemResultado_DataCnt ) ;
            AV57ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+8, 1, 1).Date = GXt_dtime1;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+9, 1, 1).Text = A803ContagemResultado_ContratadaSigla;
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+10, 1, 1).Text = A494ContagemResultado_Descricao;
            AV67Esforco2 = ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M")==0) ? A682ContagemResultado_PFBFMUltima : ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S")==0) ? A684ContagemResultado_PFBFSUltima : (decimal)(0)));
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+11, 1, 1).Number = (double)(AV67Esforco2);
            AV68TotalGeral = (decimal)(AV68TotalGeral+((AV56Esforco-AV67Esforco2)));
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+12, 1, 1).Text = StringUtil.Format( "=ABS(F%1-L%2)", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8CellRow), 8, 0)), StringUtil.LTrim( StringUtil.Str( (decimal)(AV8CellRow), 8, 0)), "", "", "", "", "", "", "");
            AV57ExcelDocument.get_Cells(AV8CellRow, AV59FirstColumn+13, 1, 1).Text = StringUtil.Format( "=(M%1/F%2)*100", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8CellRow), 8, 0)), StringUtil.LTrim( StringUtil.Str( (decimal)(AV8CellRow), 8, 0)), "", "", "", "", "", "", "");
            /* Execute user subroutine: 'AFTERWRITELINE' */
            S232 ();
            if ( returnInSub )
            {
               pr_default.close(1);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void S251( )
      {
         /* 'CLOSEDOCUMENT' Routine */
         AV57ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV57ExcelDocument.Close();
      }

      protected void S121( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV57ExcelDocument.ErrCode != 0 )
         {
            AV58Filename = "";
            AV55ErrorMessage = AV57ExcelDocument.ErrDescription;
            AV57ExcelDocument.Close();
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'BEFOREWRITELINE' Routine */
      }

      protected void S232( )
      {
         /* 'AFTERWRITELINE' Routine */
      }

      protected void S151( )
      {
         /* 'BUSCA.AREA' Routine */
         AV71AreaTrabalho_Descricao = "";
         /* Using cursor P00YN6 */
         pr_default.execute(2, new Object[] {AV46Contratada_AreaTrabalhoCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A5AreaTrabalho_Codigo = P00YN6_A5AreaTrabalho_Codigo[0];
            A6AreaTrabalho_Descricao = P00YN6_A6AreaTrabalho_Descricao[0];
            AV71AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
      }

      protected void S161( )
      {
         /* 'BUSCA.CONTRATADA' Routine */
         AV70Contratada_Sigla = "";
         /* Using cursor P00YN7 */
         pr_default.execute(3, new Object[] {AV69Contratada_CodigoAux});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A39Contratada_Codigo = P00YN7_A39Contratada_Codigo[0];
            A438Contratada_Sigla = P00YN7_A438Contratada_Sigla[0];
            AV70Contratada_Sigla = A438Contratada_Sigla;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
      }

      protected void S171( )
      {
         /* 'BUSCA.GRUPO.SERVICO' Routine */
         AV75ServicoGrupo_Descricao = "";
         /* Using cursor P00YN8 */
         pr_default.execute(4, new Object[] {AV69Contratada_CodigoAux, AV74ServicoGrupo_CodigoAux});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A155Servico_Codigo = P00YN8_A155Servico_Codigo[0];
            A74Contrato_Codigo = P00YN8_A74Contrato_Codigo[0];
            A157ServicoGrupo_Codigo = P00YN8_A157ServicoGrupo_Codigo[0];
            A39Contratada_Codigo = P00YN8_A39Contratada_Codigo[0];
            A158ServicoGrupo_Descricao = P00YN8_A158ServicoGrupo_Descricao[0];
            A160ContratoServicos_Codigo = P00YN8_A160ContratoServicos_Codigo[0];
            A157ServicoGrupo_Codigo = P00YN8_A157ServicoGrupo_Codigo[0];
            A158ServicoGrupo_Descricao = P00YN8_A158ServicoGrupo_Descricao[0];
            A39Contratada_Codigo = P00YN8_A39Contratada_Codigo[0];
            AV75ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void S181( )
      {
         /* 'BUSCA.SERVICO' Routine */
         AV73Servico_Sigla = "";
         /* Using cursor P00YN9 */
         pr_default.execute(5, new Object[] {AV72Servico_codigoAux});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A155Servico_Codigo = P00YN9_A155Servico_Codigo[0];
            A605Servico_Sigla = P00YN9_A605Servico_Sigla[0];
            AV73Servico_Sigla = A605Servico_Sigla;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(5);
      }

      protected void S191( )
      {
         /* 'BUSCA.SISTEMA' Routine */
         AV77Sistema_Sigla = "";
         /* Using cursor P00YN10 */
         pr_default.execute(6, new Object[] {AV76Sistema_CodigoAux});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A127Sistema_Codigo = P00YN10_A127Sistema_Codigo[0];
            A129Sistema_Sigla = P00YN10_A129Sistema_Sigla[0];
            AV77Sistema_Sigla = A129Sistema_Sigla;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(6);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV66WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV57ExcelDocument = new ExcelDocumentI();
         AV71AreaTrabalho_Descricao = "";
         AV70Contratada_Sigla = "";
         AV75ServicoGrupo_Descricao = "";
         AV73Servico_Sigla = "";
         AV60GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV61GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV52DynamicFiltersSelector1 = "";
         AV27ContagemResultado_DemandaFM1 = "";
         AV18ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV15ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV24ContagemResultado_DataEntregaReal1 = (DateTime)(DateTime.MinValue);
         AV21ContagemResultado_DataEntregaReal_To1 = (DateTime)(DateTime.MinValue);
         AV43ContagemResultado_StatusDmn1 = "";
         AV30ContagemResultado_Descricao1 = "";
         AV77Sistema_Sigla = "";
         AV9ContagemResultado_Agrupador1 = "";
         AV53DynamicFiltersSelector2 = "";
         AV28ContagemResultado_DemandaFM2 = "";
         AV19ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV16ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV25ContagemResultado_DataEntregaReal2 = (DateTime)(DateTime.MinValue);
         AV22ContagemResultado_DataEntregaReal_To2 = (DateTime)(DateTime.MinValue);
         AV44ContagemResultado_StatusDmn2 = "";
         AV31ContagemResultado_Descricao2 = "";
         AV10ContagemResultado_Agrupador2 = "";
         AV54DynamicFiltersSelector3 = "";
         AV29ContagemResultado_DemandaFM3 = "";
         AV20ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV17ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV26ContagemResultado_DataEntregaReal3 = (DateTime)(DateTime.MinValue);
         AV23ContagemResultado_DataEntregaReal_To3 = (DateTime)(DateTime.MinValue);
         AV45ContagemResultado_StatusDmn3 = "";
         AV32ContagemResultado_Descricao3 = "";
         AV11ContagemResultado_Agrupador3 = "";
         AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1 = "";
         AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 = "";
         AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1 = (DateTime)(DateTime.MinValue);
         AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1 = (DateTime)(DateTime.MinValue);
         AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1 = "";
         AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 = "";
         AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 = "";
         AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2 = "";
         AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 = "";
         AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2 = (DateTime)(DateTime.MinValue);
         AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2 = (DateTime)(DateTime.MinValue);
         AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2 = "";
         AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 = "";
         AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 = "";
         AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3 = "";
         AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 = "";
         AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3 = (DateTime)(DateTime.MinValue);
         AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3 = (DateTime)(DateTime.MinValue);
         AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3 = "";
         AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 = "";
         AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 = "";
         scmdbuf = "";
         lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 = "";
         lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 = "";
         lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 = "";
         lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 = "";
         lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 = "";
         lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 = "";
         lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 = "";
         lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 = "";
         lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         A484ContagemResultado_StatusDmn = "";
         A494ContagemResultado_Descricao = "";
         A1046ContagemResultado_Agrupador = "";
         P00YN3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YN3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YN3_A456ContagemResultado_Codigo = new int[1] ;
         P00YN3_A602ContagemResultado_OSVinculada = new int[1] ;
         P00YN3_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00YN3_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YN3_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YN3_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YN3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YN3_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YN3_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YN3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YN3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YN3_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P00YN3_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P00YN3_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YN3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YN3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YN3_A601ContagemResultado_Servico = new int[1] ;
         P00YN3_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YN3_A764ContagemResultado_ServicoGrupo = new int[1] ;
         P00YN3_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         P00YN3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YN3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YN3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00YN3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00YN3_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00YN3_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00YN3_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00YN3_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00YN3_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YN3_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YN3_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00YN3_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00YN3_A40000ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00YN3_n40000ContagemResultado_DataCnt = new bool[] {false} ;
         A803ContagemResultado_ContratadaSigla = "";
         A1326ContagemResultado_ContratadaTipoFab = "";
         A457ContagemResultado_Demanda = "";
         A40000ContagemResultado_DataCnt = DateTime.MinValue;
         P00YN5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YN5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YN5_A456ContagemResultado_Codigo = new int[1] ;
         P00YN5_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YN5_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YN5_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YN5_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YN5_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YN5_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YN5_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YN5_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YN5_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P00YN5_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P00YN5_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YN5_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YN5_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YN5_A601ContagemResultado_Servico = new int[1] ;
         P00YN5_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YN5_A764ContagemResultado_ServicoGrupo = new int[1] ;
         P00YN5_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         P00YN5_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YN5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YN5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00YN5_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00YN5_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00YN5_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00YN5_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00YN5_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00YN5_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YN5_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YN5_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00YN5_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00YN5_A40000ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00YN5_n40000ContagemResultado_DataCnt = new bool[] {false} ;
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         P00YN6_A5AreaTrabalho_Codigo = new int[1] ;
         P00YN6_A6AreaTrabalho_Descricao = new String[] {""} ;
         A6AreaTrabalho_Descricao = "";
         P00YN7_A39Contratada_Codigo = new int[1] ;
         P00YN7_A438Contratada_Sigla = new String[] {""} ;
         A438Contratada_Sigla = "";
         P00YN8_A155Servico_Codigo = new int[1] ;
         P00YN8_A74Contrato_Codigo = new int[1] ;
         P00YN8_A157ServicoGrupo_Codigo = new int[1] ;
         P00YN8_A39Contratada_Codigo = new int[1] ;
         P00YN8_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00YN8_A160ContratoServicos_Codigo = new int[1] ;
         A158ServicoGrupo_Descricao = "";
         P00YN9_A155Servico_Codigo = new int[1] ;
         P00YN9_A605Servico_Sigla = new String[] {""} ;
         A605Servico_Sigla = "";
         P00YN10_A127Sistema_Codigo = new int[1] ;
         P00YN10_A129Sistema_Sigla = new String[] {""} ;
         A129Sistema_Sigla = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_relatoriocomparacaodemandasxls__default(),
            new Object[][] {
                new Object[] {
               P00YN3_A1553ContagemResultado_CntSrvCod, P00YN3_n1553ContagemResultado_CntSrvCod, P00YN3_A456ContagemResultado_Codigo, P00YN3_A602ContagemResultado_OSVinculada, P00YN3_n602ContagemResultado_OSVinculada, P00YN3_A1046ContagemResultado_Agrupador, P00YN3_n1046ContagemResultado_Agrupador, P00YN3_A489ContagemResultado_SistemaCod, P00YN3_n489ContagemResultado_SistemaCod, P00YN3_A494ContagemResultado_Descricao,
               P00YN3_n494ContagemResultado_Descricao, P00YN3_A484ContagemResultado_StatusDmn, P00YN3_n484ContagemResultado_StatusDmn, P00YN3_A2017ContagemResultado_DataEntregaReal, P00YN3_n2017ContagemResultado_DataEntregaReal, P00YN3_A471ContagemResultado_DataDmn, P00YN3_A493ContagemResultado_DemandaFM, P00YN3_n493ContagemResultado_DemandaFM, P00YN3_A601ContagemResultado_Servico, P00YN3_n601ContagemResultado_Servico,
               P00YN3_A764ContagemResultado_ServicoGrupo, P00YN3_n764ContagemResultado_ServicoGrupo, P00YN3_A490ContagemResultado_ContratadaCod, P00YN3_n490ContagemResultado_ContratadaCod, P00YN3_A52Contratada_AreaTrabalhoCod, P00YN3_n52Contratada_AreaTrabalhoCod, P00YN3_A803ContagemResultado_ContratadaSigla, P00YN3_n803ContagemResultado_ContratadaSigla, P00YN3_A1326ContagemResultado_ContratadaTipoFab, P00YN3_n1326ContagemResultado_ContratadaTipoFab,
               P00YN3_A457ContagemResultado_Demanda, P00YN3_n457ContagemResultado_Demanda, P00YN3_A684ContagemResultado_PFBFSUltima, P00YN3_A682ContagemResultado_PFBFMUltima, P00YN3_A40000ContagemResultado_DataCnt, P00YN3_n40000ContagemResultado_DataCnt
               }
               , new Object[] {
               P00YN5_A1553ContagemResultado_CntSrvCod, P00YN5_n1553ContagemResultado_CntSrvCod, P00YN5_A456ContagemResultado_Codigo, P00YN5_A1046ContagemResultado_Agrupador, P00YN5_n1046ContagemResultado_Agrupador, P00YN5_A489ContagemResultado_SistemaCod, P00YN5_n489ContagemResultado_SistemaCod, P00YN5_A494ContagemResultado_Descricao, P00YN5_n494ContagemResultado_Descricao, P00YN5_A484ContagemResultado_StatusDmn,
               P00YN5_n484ContagemResultado_StatusDmn, P00YN5_A2017ContagemResultado_DataEntregaReal, P00YN5_n2017ContagemResultado_DataEntregaReal, P00YN5_A471ContagemResultado_DataDmn, P00YN5_A493ContagemResultado_DemandaFM, P00YN5_n493ContagemResultado_DemandaFM, P00YN5_A601ContagemResultado_Servico, P00YN5_n601ContagemResultado_Servico, P00YN5_A764ContagemResultado_ServicoGrupo, P00YN5_n764ContagemResultado_ServicoGrupo,
               P00YN5_A490ContagemResultado_ContratadaCod, P00YN5_n490ContagemResultado_ContratadaCod, P00YN5_A52Contratada_AreaTrabalhoCod, P00YN5_n52Contratada_AreaTrabalhoCod, P00YN5_A803ContagemResultado_ContratadaSigla, P00YN5_n803ContagemResultado_ContratadaSigla, P00YN5_A1326ContagemResultado_ContratadaTipoFab, P00YN5_n1326ContagemResultado_ContratadaTipoFab, P00YN5_A457ContagemResultado_Demanda, P00YN5_n457ContagemResultado_Demanda,
               P00YN5_A684ContagemResultado_PFBFSUltima, P00YN5_A682ContagemResultado_PFBFMUltima, P00YN5_A40000ContagemResultado_DataCnt, P00YN5_n40000ContagemResultado_DataCnt
               }
               , new Object[] {
               P00YN6_A5AreaTrabalho_Codigo, P00YN6_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00YN7_A39Contratada_Codigo, P00YN7_A438Contratada_Sigla
               }
               , new Object[] {
               P00YN8_A155Servico_Codigo, P00YN8_A74Contrato_Codigo, P00YN8_A157ServicoGrupo_Codigo, P00YN8_A39Contratada_Codigo, P00YN8_A158ServicoGrupo_Descricao, P00YN8_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P00YN9_A155Servico_Codigo, P00YN9_A605Servico_Sigla
               }
               , new Object[] {
               P00YN10_A127Sistema_Codigo, P00YN10_A129Sistema_Sigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV49DynamicFiltersOperator1 ;
      private short AV40ContagemResultado_StatusCnt1 ;
      private short AV50DynamicFiltersOperator2 ;
      private short AV41ContagemResultado_StatusCnt2 ;
      private short AV51DynamicFiltersOperator3 ;
      private short AV42ContagemResultado_StatusCnt3 ;
      private short AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 ;
      private short AV93WP_RelatorioComparacaoDemandasDS_13_Contagemresultado_statuscnt1 ;
      private short AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 ;
      private short AV106WP_RelatorioComparacaoDemandasDS_26_Contagemresultado_statuscnt2 ;
      private short AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 ;
      private short AV119WP_RelatorioComparacaoDemandasDS_39_Contagemresultado_statuscnt3 ;
      private int AV46Contratada_AreaTrabalhoCod ;
      private int AV14ContagemResultado_ContratadaCod ;
      private int AV36ContagemResultado_ServicoGrupo ;
      private int AV35ContagemResultado_Servico ;
      private int AV12ContagemResultado_CntadaOsVinc ;
      private int AV34ContagemResultado_SerGrupoVinc ;
      private int AV13ContagemResultado_CodSrvVnc ;
      private int AV8CellRow ;
      private int AV59FirstColumn ;
      private int AV65Random ;
      private int AV69Contratada_CodigoAux ;
      private int AV74ServicoGrupo_CodigoAux ;
      private int AV72Servico_codigoAux ;
      private int AV37ContagemResultado_SistemaCod1 ;
      private int AV76Sistema_CodigoAux ;
      private int AV38ContagemResultado_SistemaCod2 ;
      private int AV39ContagemResultado_SistemaCod3 ;
      private int AV81WP_RelatorioComparacaoDemandasDS_1_Contratada_areatrabalhocod ;
      private int AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod ;
      private int AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo ;
      private int AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico ;
      private int AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1 ;
      private int AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2 ;
      private int AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3 ;
      private int AV66WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A764ContagemResultado_ServicoGrupo ;
      private int A601ContagemResultado_Servico ;
      private int A489ContagemResultado_SistemaCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A456ContagemResultado_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int AV33ContagemResultado_OSVinculada ;
      private int A5AreaTrabalho_Codigo ;
      private int A39Contratada_Codigo ;
      private int A155Servico_Codigo ;
      private int A74Contrato_Codigo ;
      private int A157ServicoGrupo_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A127Sistema_Codigo ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal AV56Esforco ;
      private decimal AV68TotalGeral ;
      private decimal AV67Esforco2 ;
      private String AV70Contratada_Sigla ;
      private String AV73Servico_Sigla ;
      private String AV43ContagemResultado_StatusDmn1 ;
      private String AV77Sistema_Sigla ;
      private String AV9ContagemResultado_Agrupador1 ;
      private String AV44ContagemResultado_StatusDmn2 ;
      private String AV10ContagemResultado_Agrupador2 ;
      private String AV45ContagemResultado_StatusDmn3 ;
      private String AV11ContagemResultado_Agrupador3 ;
      private String AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1 ;
      private String AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 ;
      private String AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2 ;
      private String AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 ;
      private String AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3 ;
      private String AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 ;
      private String scmdbuf ;
      private String lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 ;
      private String lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 ;
      private String lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1046ContagemResultado_Agrupador ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String A438Contratada_Sigla ;
      private String A605Servico_Sigla ;
      private String A129Sistema_Sigla ;
      private DateTime AV24ContagemResultado_DataEntregaReal1 ;
      private DateTime AV21ContagemResultado_DataEntregaReal_To1 ;
      private DateTime AV25ContagemResultado_DataEntregaReal2 ;
      private DateTime AV22ContagemResultado_DataEntregaReal_To2 ;
      private DateTime AV26ContagemResultado_DataEntregaReal3 ;
      private DateTime AV23ContagemResultado_DataEntregaReal_To3 ;
      private DateTime AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1 ;
      private DateTime AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1 ;
      private DateTime AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2 ;
      private DateTime AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2 ;
      private DateTime AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3 ;
      private DateTime AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3 ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private DateTime GXt_dtime1 ;
      private DateTime AV18ContagemResultado_DataDmn1 ;
      private DateTime AV15ContagemResultado_DataDmn_To1 ;
      private DateTime AV19ContagemResultado_DataDmn2 ;
      private DateTime AV16ContagemResultado_DataDmn_To2 ;
      private DateTime AV20ContagemResultado_DataDmn3 ;
      private DateTime AV17ContagemResultado_DataDmn_To3 ;
      private DateTime AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1 ;
      private DateTime AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1 ;
      private DateTime AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2 ;
      private DateTime AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2 ;
      private DateTime AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3 ;
      private DateTime AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A40000ContagemResultado_DataCnt ;
      private bool returnInSub ;
      private bool AV47DynamicFiltersEnabled2 ;
      private bool AV48DynamicFiltersEnabled3 ;
      private bool AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 ;
      private bool AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n601ContagemResultado_Servico ;
      private bool n764ContagemResultado_ServicoGrupo ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n457ContagemResultado_Demanda ;
      private bool n40000ContagemResultado_DataCnt ;
      private String AV62GridStateXML ;
      private String AV55ErrorMessage ;
      private String AV58Filename ;
      private String AV71AreaTrabalho_Descricao ;
      private String AV75ServicoGrupo_Descricao ;
      private String AV52DynamicFiltersSelector1 ;
      private String AV27ContagemResultado_DemandaFM1 ;
      private String AV30ContagemResultado_Descricao1 ;
      private String AV53DynamicFiltersSelector2 ;
      private String AV28ContagemResultado_DemandaFM2 ;
      private String AV31ContagemResultado_Descricao2 ;
      private String AV54DynamicFiltersSelector3 ;
      private String AV29ContagemResultado_DemandaFM3 ;
      private String AV32ContagemResultado_Descricao3 ;
      private String AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1 ;
      private String AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 ;
      private String AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 ;
      private String AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2 ;
      private String AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 ;
      private String AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 ;
      private String AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3 ;
      private String AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 ;
      private String AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 ;
      private String lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 ;
      private String lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 ;
      private String lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 ;
      private String lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 ;
      private String lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 ;
      private String lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A6AreaTrabalho_Descricao ;
      private String A158ServicoGrupo_Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00YN3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YN3_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YN3_A456ContagemResultado_Codigo ;
      private int[] P00YN3_A602ContagemResultado_OSVinculada ;
      private bool[] P00YN3_n602ContagemResultado_OSVinculada ;
      private String[] P00YN3_A1046ContagemResultado_Agrupador ;
      private bool[] P00YN3_n1046ContagemResultado_Agrupador ;
      private int[] P00YN3_A489ContagemResultado_SistemaCod ;
      private bool[] P00YN3_n489ContagemResultado_SistemaCod ;
      private String[] P00YN3_A494ContagemResultado_Descricao ;
      private bool[] P00YN3_n494ContagemResultado_Descricao ;
      private String[] P00YN3_A484ContagemResultado_StatusDmn ;
      private bool[] P00YN3_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00YN3_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P00YN3_n2017ContagemResultado_DataEntregaReal ;
      private DateTime[] P00YN3_A471ContagemResultado_DataDmn ;
      private String[] P00YN3_A493ContagemResultado_DemandaFM ;
      private bool[] P00YN3_n493ContagemResultado_DemandaFM ;
      private int[] P00YN3_A601ContagemResultado_Servico ;
      private bool[] P00YN3_n601ContagemResultado_Servico ;
      private int[] P00YN3_A764ContagemResultado_ServicoGrupo ;
      private bool[] P00YN3_n764ContagemResultado_ServicoGrupo ;
      private int[] P00YN3_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YN3_n490ContagemResultado_ContratadaCod ;
      private int[] P00YN3_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00YN3_n52Contratada_AreaTrabalhoCod ;
      private String[] P00YN3_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00YN3_n803ContagemResultado_ContratadaSigla ;
      private String[] P00YN3_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00YN3_n1326ContagemResultado_ContratadaTipoFab ;
      private String[] P00YN3_A457ContagemResultado_Demanda ;
      private bool[] P00YN3_n457ContagemResultado_Demanda ;
      private decimal[] P00YN3_A684ContagemResultado_PFBFSUltima ;
      private decimal[] P00YN3_A682ContagemResultado_PFBFMUltima ;
      private DateTime[] P00YN3_A40000ContagemResultado_DataCnt ;
      private bool[] P00YN3_n40000ContagemResultado_DataCnt ;
      private int[] P00YN5_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YN5_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YN5_A456ContagemResultado_Codigo ;
      private String[] P00YN5_A1046ContagemResultado_Agrupador ;
      private bool[] P00YN5_n1046ContagemResultado_Agrupador ;
      private int[] P00YN5_A489ContagemResultado_SistemaCod ;
      private bool[] P00YN5_n489ContagemResultado_SistemaCod ;
      private String[] P00YN5_A494ContagemResultado_Descricao ;
      private bool[] P00YN5_n494ContagemResultado_Descricao ;
      private String[] P00YN5_A484ContagemResultado_StatusDmn ;
      private bool[] P00YN5_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00YN5_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P00YN5_n2017ContagemResultado_DataEntregaReal ;
      private DateTime[] P00YN5_A471ContagemResultado_DataDmn ;
      private String[] P00YN5_A493ContagemResultado_DemandaFM ;
      private bool[] P00YN5_n493ContagemResultado_DemandaFM ;
      private int[] P00YN5_A601ContagemResultado_Servico ;
      private bool[] P00YN5_n601ContagemResultado_Servico ;
      private int[] P00YN5_A764ContagemResultado_ServicoGrupo ;
      private bool[] P00YN5_n764ContagemResultado_ServicoGrupo ;
      private int[] P00YN5_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YN5_n490ContagemResultado_ContratadaCod ;
      private int[] P00YN5_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00YN5_n52Contratada_AreaTrabalhoCod ;
      private String[] P00YN5_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00YN5_n803ContagemResultado_ContratadaSigla ;
      private String[] P00YN5_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00YN5_n1326ContagemResultado_ContratadaTipoFab ;
      private String[] P00YN5_A457ContagemResultado_Demanda ;
      private bool[] P00YN5_n457ContagemResultado_Demanda ;
      private decimal[] P00YN5_A684ContagemResultado_PFBFSUltima ;
      private decimal[] P00YN5_A682ContagemResultado_PFBFMUltima ;
      private DateTime[] P00YN5_A40000ContagemResultado_DataCnt ;
      private bool[] P00YN5_n40000ContagemResultado_DataCnt ;
      private int[] P00YN6_A5AreaTrabalho_Codigo ;
      private String[] P00YN6_A6AreaTrabalho_Descricao ;
      private int[] P00YN7_A39Contratada_Codigo ;
      private String[] P00YN7_A438Contratada_Sigla ;
      private int[] P00YN8_A155Servico_Codigo ;
      private int[] P00YN8_A74Contrato_Codigo ;
      private int[] P00YN8_A157ServicoGrupo_Codigo ;
      private int[] P00YN8_A39Contratada_Codigo ;
      private String[] P00YN8_A158ServicoGrupo_Descricao ;
      private int[] P00YN8_A160ContratoServicos_Codigo ;
      private int[] P00YN9_A155Servico_Codigo ;
      private String[] P00YN9_A605Servico_Sigla ;
      private int[] P00YN10_A127Sistema_Codigo ;
      private String[] P00YN10_A129Sistema_Sigla ;
      private String aP8_Filename ;
      private String aP9_ErrorMessage ;
      private ExcelDocumentI AV57ExcelDocument ;
      private wwpbaseobjects.SdtWWPGridState AV60GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV61GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV66WWPContext ;
   }

   public class arel_relatoriocomparacaodemandasxls__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00YN3( IGxContext context ,
                                             int AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod ,
                                             int AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo ,
                                             int AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico ,
                                             String AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1 ,
                                             short AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 ,
                                             String AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 ,
                                             DateTime AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1 ,
                                             DateTime AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1 ,
                                             DateTime AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1 ,
                                             DateTime AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1 ,
                                             String AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1 ,
                                             String AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 ,
                                             int AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1 ,
                                             String AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 ,
                                             bool AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 ,
                                             String AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2 ,
                                             short AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 ,
                                             String AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 ,
                                             DateTime AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2 ,
                                             DateTime AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2 ,
                                             DateTime AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2 ,
                                             DateTime AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2 ,
                                             String AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2 ,
                                             String AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 ,
                                             int AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2 ,
                                             String AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 ,
                                             bool AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 ,
                                             String AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3 ,
                                             short AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 ,
                                             String AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 ,
                                             DateTime AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3 ,
                                             DateTime AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3 ,
                                             DateTime AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3 ,
                                             DateTime AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3 ,
                                             String AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3 ,
                                             String AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 ,
                                             int AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3 ,
                                             String AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A764ContagemResultado_ServicoGrupo ,
                                             int A601ContagemResultado_Servico ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A2017ContagemResultado_DataEntregaReal ,
                                             String A484ContagemResultado_StatusDmn ,
                                             String A494ContagemResultado_Descricao ,
                                             int A489ContagemResultado_SistemaCod ,
                                             String A1046ContagemResultado_Agrupador ,
                                             int A602ContagemResultado_OSVinculada ,
                                             int A456ContagemResultado_Codigo ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV66WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [49] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_SistemaCod], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntregaReal], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T2.[Servico_Codigo] AS ContagemResultado_Servico, T3.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T5.[Contratada_AreaTrabalhoCod], T5.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T5.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_Demanda], COALESCE( T4.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T4.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T4.[ContagemResultado_DataCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataCnt FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (Not (T1.[ContagemResultado_OSVinculada] = convert(int, 0)))";
         scmdbuf = scmdbuf + " and (T5.[Contratada_AreaTrabalhoCod] = @AV66WWPC_1Areatrabalho_codigo)";
         if ( ! (0==AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo) )
         {
            sWhereString = sWhereString + " and (T3.[ServicoGrupo_Codigo] = @AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (0==AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like '%' + @lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] >= @AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] <= @AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like '%' + @lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] >= @AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] <= @AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2)";
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int2[32] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int2[33] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int2[34] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int2[35] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like '%' + @lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int2[36] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int2[37] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int2[38] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] >= @AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3)";
         }
         else
         {
            GXv_int2[39] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] <= @AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3)";
         }
         else
         {
            GXv_int2[40] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int2[41] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int2[42] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int2[43] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int2[44] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int2[45] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int2[46] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int2[47] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int2[48] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P00YN5( IGxContext context ,
                                             int AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod ,
                                             int AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo ,
                                             int AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico ,
                                             String AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1 ,
                                             short AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 ,
                                             String AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1 ,
                                             DateTime AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1 ,
                                             DateTime AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1 ,
                                             DateTime AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1 ,
                                             DateTime AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1 ,
                                             String AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1 ,
                                             String AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1 ,
                                             int AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1 ,
                                             String AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1 ,
                                             bool AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 ,
                                             String AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2 ,
                                             short AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 ,
                                             String AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2 ,
                                             DateTime AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2 ,
                                             DateTime AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2 ,
                                             DateTime AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2 ,
                                             DateTime AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2 ,
                                             String AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2 ,
                                             String AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2 ,
                                             int AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2 ,
                                             String AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2 ,
                                             bool AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 ,
                                             String AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3 ,
                                             short AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 ,
                                             String AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3 ,
                                             DateTime AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3 ,
                                             DateTime AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3 ,
                                             DateTime AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3 ,
                                             DateTime AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3 ,
                                             String AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3 ,
                                             String AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3 ,
                                             int AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3 ,
                                             String AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3 ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A764ContagemResultado_ServicoGrupo ,
                                             int A601ContagemResultado_Servico ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A2017ContagemResultado_DataEntregaReal ,
                                             String A484ContagemResultado_StatusDmn ,
                                             String A494ContagemResultado_Descricao ,
                                             int A489ContagemResultado_SistemaCod ,
                                             String A1046ContagemResultado_Agrupador ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV66WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int A456ContagemResultado_Codigo ,
                                             int AV33ContagemResultado_OSVinculada )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [50] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_SistemaCod], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntregaReal], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T2.[Servico_Codigo] AS ContagemResultado_Servico, T3.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T5.[Contratada_AreaTrabalhoCod], T5.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T5.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_Demanda], COALESCE( T4.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T4.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T4.[ContagemResultado_DataCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataCnt FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T5.[Contratada_AreaTrabalhoCod] = @AV66WWPC_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Codigo] = @AV33ContagemResultado_OSVinculada)";
         if ( ! (0==AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (0==AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo) )
         {
            sWhereString = sWhereString + " and (T3.[ServicoGrupo_Codigo] = @AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (0==AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like '%' + @lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] >= @AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] <= @AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WP_RelatorioComparacaoDemandasDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV86WP_RelatorioComparacaoDemandasDS_6_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like '%' + @lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] >= @AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2)";
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] <= @AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2)";
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int4[29] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int4[32] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int4[33] = 1;
         }
         if ( AV97WP_RelatorioComparacaoDemandasDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV98WP_RelatorioComparacaoDemandasDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV99WP_RelatorioComparacaoDemandasDS_19_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int4[34] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int4[35] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int4[36] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like '%' + @lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int4[37] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int4[38] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int4[39] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] >= @AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3)";
         }
         else
         {
            GXv_int4[40] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataEntregaReal] <= @AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3)";
         }
         else
         {
            GXv_int4[41] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int4[42] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int4[43] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int4[44] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int4[45] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int4[46] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int4[47] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like @lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int4[48] = 1;
         }
         if ( AV110WP_RelatorioComparacaoDemandasDS_30_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV111WP_RelatorioComparacaoDemandasDS_31_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV112WP_RelatorioComparacaoDemandasDS_32_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int4[49] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00YN3(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (bool)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (bool)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (String)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (int)dynConstraints[48] , (int)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] );
               case 1 :
                     return conditional_P00YN5(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (bool)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (bool)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (String)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (int)dynConstraints[48] , (int)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00YN6 ;
          prmP00YN6 = new Object[] {
          new Object[] {"@AV46Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YN7 ;
          prmP00YN7 = new Object[] {
          new Object[] {"@AV69Contratada_CodigoAux",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YN8 ;
          prmP00YN8 = new Object[] {
          new Object[] {"@AV69Contratada_CodigoAux",SqlDbType.Int,6,0} ,
          new Object[] {"@AV74ServicoGrupo_CodigoAux",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YN9 ;
          prmP00YN9 = new Object[] {
          new Object[] {"@AV72Servico_codigoAux",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YN10 ;
          prmP00YN10 = new Object[] {
          new Object[] {"@AV76Sistema_CodigoAux",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YN3 ;
          prmP00YN3 = new Object[] {
          new Object[] {"@AV66WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00YN5 ;
          prmP00YN5 = new Object[] {
          new Object[] {"@AV66WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV33ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82WP_RelatorioComparacaoDemandasDS_2_Contagemresultado_contratadacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV83WP_RelatorioComparacaoDemandasDS_3_Contagemresultado_servicogrupo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV84WP_RelatorioComparacaoDemandasDS_4_Contagemresultado_servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV87WP_RelatorioComparacaoDemandasDS_7_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV88WP_RelatorioComparacaoDemandasDS_8_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WP_RelatorioComparacaoDemandasDS_9_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV90WP_RelatorioComparacaoDemandasDS_10_Contagemresultado_dataentregareal1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV91WP_RelatorioComparacaoDemandasDS_11_Contagemresultado_dataentregareal_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV92WP_RelatorioComparacaoDemandasDS_12_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV94WP_RelatorioComparacaoDemandasDS_14_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV95WP_RelatorioComparacaoDemandasDS_15_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV96WP_RelatorioComparacaoDemandasDS_16_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV100WP_RelatorioComparacaoDemandasDS_20_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WP_RelatorioComparacaoDemandasDS_21_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WP_RelatorioComparacaoDemandasDS_22_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WP_RelatorioComparacaoDemandasDS_23_Contagemresultado_dataentregareal2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV104WP_RelatorioComparacaoDemandasDS_24_Contagemresultado_dataentregareal_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV105WP_RelatorioComparacaoDemandasDS_25_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV107WP_RelatorioComparacaoDemandasDS_27_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV108WP_RelatorioComparacaoDemandasDS_28_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV109WP_RelatorioComparacaoDemandasDS_29_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV113WP_RelatorioComparacaoDemandasDS_33_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV114WP_RelatorioComparacaoDemandasDS_34_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115WP_RelatorioComparacaoDemandasDS_35_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV116WP_RelatorioComparacaoDemandasDS_36_Contagemresultado_dataentregareal3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV117WP_RelatorioComparacaoDemandasDS_37_Contagemresultado_dataentregareal_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV118WP_RelatorioComparacaoDemandasDS_38_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV120WP_RelatorioComparacaoDemandasDS_40_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV121WP_RelatorioComparacaoDemandasDS_41_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV122WP_RelatorioComparacaoDemandasDS_42_Contagemresultado_agrupador3",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00YN3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YN3,100,0,true,false )
             ,new CursorDef("P00YN5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YN5,100,0,true,false )
             ,new CursorDef("P00YN6", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV46Contratada_AreaTrabalhoCod ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YN6,1,0,false,true )
             ,new CursorDef("P00YN7", "SELECT [Contratada_Codigo], [Contratada_Sigla] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @AV69Contratada_CodigoAux ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YN7,1,0,false,true )
             ,new CursorDef("P00YN8", "SELECT T1.[Servico_Codigo], T1.[Contrato_Codigo], T2.[ServicoGrupo_Codigo], T4.[Contratada_Codigo], T3.[ServicoGrupo_Descricao], T1.[ContratoServicos_Codigo] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T3 WITH (NOLOCK) ON T3.[ServicoGrupo_Codigo] = T2.[ServicoGrupo_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T4.[Contratada_Codigo] = @AV69Contratada_CodigoAux) AND (T2.[ServicoGrupo_Codigo] = @AV74ServicoGrupo_CodigoAux) ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YN8,100,0,false,false )
             ,new CursorDef("P00YN9", "SELECT [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV72Servico_codigoAux ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YN9,1,0,false,true )
             ,new CursorDef("P00YN10", "SELECT [Sistema_Codigo], [Sistema_Sigla] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV76Sistema_CodigoAux ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YN10,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(9) ;
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(18) ;
                ((decimal[]) buf[33])[0] = rslt.getDecimal(19) ;
                ((DateTime[]) buf[34])[0] = rslt.getGXDate(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(8) ;
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((decimal[]) buf[30])[0] = rslt.getDecimal(17) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(18) ;
                ((DateTime[]) buf[32])[0] = rslt.getGXDate(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[59]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[72]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[78]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[81]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[82]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[83]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[86]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[87]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[88]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[89]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[90]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[91]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[92]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[94]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[95]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[96]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[97]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[59]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[75]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[76]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[78]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[82]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[83]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[86]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[87]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[88]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[89]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[90]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[91]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[92]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[94]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[95]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[96]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[97]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[98]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[99]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
