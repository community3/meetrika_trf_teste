/*
               File: PRC_SrvVncDaDmn
        Description: Servi�os da demanda
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:17.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_srvvncdadmn : GXProcedure
   {
      public prc_srvvncdadmn( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_srvvncdadmn( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           out String aP1_Srvs ,
                           out int aP2_IdLink1 ,
                           out int aP3_IdLink2 ,
                           ref int aP4_IdLink3 ,
                           out String aP5_Txt1 ,
                           out String aP6_Txt2 ,
                           out String aP7_Txt3 )
      {
         this.AV13ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9Srvs = "" ;
         this.AV10IdLink1 = 0 ;
         this.AV11IdLink2 = 0 ;
         this.AV12IdLink3 = aP4_IdLink3;
         this.AV16Txt1 = "" ;
         this.AV15Txt2 = "" ;
         this.AV14Txt3 = "" ;
         initialize();
         executePrivate();
         aP1_Srvs=this.AV9Srvs;
         aP2_IdLink1=this.AV10IdLink1;
         aP3_IdLink2=this.AV11IdLink2;
         aP4_IdLink3=this.AV12IdLink3;
         aP5_Txt1=this.AV16Txt1;
         aP6_Txt2=this.AV15Txt2;
         aP7_Txt3=this.AV14Txt3;
      }

      public String executeUdp( int aP0_ContagemResultado_Codigo ,
                                out String aP1_Srvs ,
                                out int aP2_IdLink1 ,
                                out int aP3_IdLink2 ,
                                ref int aP4_IdLink3 ,
                                out String aP5_Txt1 ,
                                out String aP6_Txt2 )
      {
         this.AV13ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9Srvs = "" ;
         this.AV10IdLink1 = 0 ;
         this.AV11IdLink2 = 0 ;
         this.AV12IdLink3 = aP4_IdLink3;
         this.AV16Txt1 = "" ;
         this.AV15Txt2 = "" ;
         this.AV14Txt3 = "" ;
         initialize();
         executePrivate();
         aP1_Srvs=this.AV9Srvs;
         aP2_IdLink1=this.AV10IdLink1;
         aP3_IdLink2=this.AV11IdLink2;
         aP4_IdLink3=this.AV12IdLink3;
         aP5_Txt1=this.AV16Txt1;
         aP6_Txt2=this.AV15Txt2;
         aP7_Txt3=this.AV14Txt3;
         return AV14Txt3 ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 out String aP1_Srvs ,
                                 out int aP2_IdLink1 ,
                                 out int aP3_IdLink2 ,
                                 ref int aP4_IdLink3 ,
                                 out String aP5_Txt1 ,
                                 out String aP6_Txt2 ,
                                 out String aP7_Txt3 )
      {
         prc_srvvncdadmn objprc_srvvncdadmn;
         objprc_srvvncdadmn = new prc_srvvncdadmn();
         objprc_srvvncdadmn.AV13ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_srvvncdadmn.AV9Srvs = "" ;
         objprc_srvvncdadmn.AV10IdLink1 = 0 ;
         objprc_srvvncdadmn.AV11IdLink2 = 0 ;
         objprc_srvvncdadmn.AV12IdLink3 = aP4_IdLink3;
         objprc_srvvncdadmn.AV16Txt1 = "" ;
         objprc_srvvncdadmn.AV15Txt2 = "" ;
         objprc_srvvncdadmn.AV14Txt3 = "" ;
         objprc_srvvncdadmn.context.SetSubmitInitialConfig(context);
         objprc_srvvncdadmn.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_srvvncdadmn);
         aP1_Srvs=this.AV9Srvs;
         aP2_IdLink1=this.AV10IdLink1;
         aP3_IdLink2=this.AV11IdLink2;
         aP4_IdLink3=this.AV12IdLink3;
         aP5_Txt1=this.AV16Txt1;
         aP6_Txt2=this.AV15Txt2;
         aP7_Txt3=this.AV14Txt3;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_srvvncdadmn)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8s = 1;
         /* Using cursor P005P2 */
         pr_default.execute(0, new Object[] {AV13ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P005P2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P005P2_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P005P2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P005P2_n601ContagemResultado_Servico[0];
            A602ContagemResultado_OSVinculada = P005P2_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P005P2_n602ContagemResultado_OSVinculada[0];
            A801ContagemResultado_ServicoSigla = P005P2_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P005P2_n801ContagemResultado_ServicoSigla[0];
            A456ContagemResultado_Codigo = P005P2_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P005P2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P005P2_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P005P2_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P005P2_n801ContagemResultado_ServicoSigla[0];
            if ( AV8s == 1 )
            {
               AV9Srvs = AV9Srvs + StringUtil.Substring( A801ContagemResultado_ServicoSigla, 1, 1);
               AV16Txt1 = A801ContagemResultado_ServicoSigla;
               AV10IdLink1 = A456ContagemResultado_Codigo;
            }
            else if ( AV8s == 2 )
            {
               AV9Srvs = AV9Srvs + StringUtil.Substring( A801ContagemResultado_ServicoSigla, 1, 1);
               AV15Txt2 = A801ContagemResultado_ServicoSigla;
               AV11IdLink2 = A456ContagemResultado_Codigo;
            }
            else if ( AV8s == 3 )
            {
               AV9Srvs = AV9Srvs + StringUtil.Substring( A801ContagemResultado_ServicoSigla, 1, 1);
               AV14Txt3 = A801ContagemResultado_ServicoSigla;
               AV12IdLink3 = A456ContagemResultado_Codigo;
            }
            else
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            AV8s = (short)(AV8s+1);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P005P2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P005P2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P005P2_A601ContagemResultado_Servico = new int[1] ;
         P005P2_n601ContagemResultado_Servico = new bool[] {false} ;
         P005P2_A602ContagemResultado_OSVinculada = new int[1] ;
         P005P2_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P005P2_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P005P2_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P005P2_A456ContagemResultado_Codigo = new int[1] ;
         A801ContagemResultado_ServicoSigla = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_srvvncdadmn__default(),
            new Object[][] {
                new Object[] {
               P005P2_A1553ContagemResultado_CntSrvCod, P005P2_n1553ContagemResultado_CntSrvCod, P005P2_A601ContagemResultado_Servico, P005P2_n601ContagemResultado_Servico, P005P2_A602ContagemResultado_OSVinculada, P005P2_n602ContagemResultado_OSVinculada, P005P2_A801ContagemResultado_ServicoSigla, P005P2_n801ContagemResultado_ServicoSigla, P005P2_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8s ;
      private int AV13ContagemResultado_Codigo ;
      private int AV12IdLink3 ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A602ContagemResultado_OSVinculada ;
      private int A456ContagemResultado_Codigo ;
      private int AV10IdLink1 ;
      private int AV11IdLink2 ;
      private String AV14Txt3 ;
      private String scmdbuf ;
      private String A801ContagemResultado_ServicoSigla ;
      private String AV9Srvs ;
      private String AV16Txt1 ;
      private String AV15Txt2 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n801ContagemResultado_ServicoSigla ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP4_IdLink3 ;
      private IDataStoreProvider pr_default ;
      private int[] P005P2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P005P2_n1553ContagemResultado_CntSrvCod ;
      private int[] P005P2_A601ContagemResultado_Servico ;
      private bool[] P005P2_n601ContagemResultado_Servico ;
      private int[] P005P2_A602ContagemResultado_OSVinculada ;
      private bool[] P005P2_n602ContagemResultado_OSVinculada ;
      private String[] P005P2_A801ContagemResultado_ServicoSigla ;
      private bool[] P005P2_n801ContagemResultado_ServicoSigla ;
      private int[] P005P2_A456ContagemResultado_Codigo ;
      private String aP1_Srvs ;
      private int aP2_IdLink1 ;
      private int aP3_IdLink2 ;
      private String aP5_Txt1 ;
      private String aP6_Txt2 ;
      private String aP7_Txt3 ;
   }

   public class prc_srvvncdadmn__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005P2 ;
          prmP005P2 = new Object[] {
          new Object[] {"@AV13ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005P2", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_OSVinculada], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) WHERE T1.[ContagemResultado_OSVinculada] = @AV13ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_OSVinculada] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005P2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
