/*
               File: ContratanteUsuario_BC
        Description: Contratante Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:29:20.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratanteusuario_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratanteusuario_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratanteusuario_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow0D15( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey0D15( ) ;
         standaloneModal( ) ;
         AddRow0D15( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E110D2 */
            E110D2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
               Z60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_0D0( )
      {
         BeforeValidate0D15( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0D15( ) ;
            }
            else
            {
               CheckExtendedTable0D15( ) ;
               if ( AnyError == 0 )
               {
                  ZM0D15( 2) ;
                  ZM0D15( 3) ;
                  ZM0D15( 4) ;
                  ZM0D15( 5) ;
                  ZM0D15( 6) ;
               }
               CloseExtendedTableCursors0D15( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E120D2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         AV12AreaTrabalho_Codigo = AV9WWPContext.gxTpr_Areatrabalho_codigo;
      }

      protected void E110D2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            new prc_tirardagestaodecontratos(context ).execute( ref  AV8ContratanteUsuario_UsuarioCod) ;
         }
      }

      protected void ZM0D15( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            Z1728ContratanteUsuario_EhFiscal = A1728ContratanteUsuario_EhFiscal;
            Z1020ContratanteUsuario_AreaTrabalhoCod = A1020ContratanteUsuario_AreaTrabalhoCod;
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z1020ContratanteUsuario_AreaTrabalhoCod = A1020ContratanteUsuario_AreaTrabalhoCod;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z65ContratanteUsuario_ContratanteFan = A65ContratanteUsuario_ContratanteFan;
            Z340ContratanteUsuario_ContratantePesCod = A340ContratanteUsuario_ContratantePesCod;
            Z1020ContratanteUsuario_AreaTrabalhoCod = A1020ContratanteUsuario_AreaTrabalhoCod;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z2Usuario_Nome = A2Usuario_Nome;
            Z1019ContratanteUsuario_UsuarioEhContratante = A1019ContratanteUsuario_UsuarioEhContratante;
            Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            Z61ContratanteUsuario_UsuarioPessoaCod = A61ContratanteUsuario_UsuarioPessoaCod;
            Z1020ContratanteUsuario_AreaTrabalhoCod = A1020ContratanteUsuario_AreaTrabalhoCod;
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z64ContratanteUsuario_ContratanteRaz = A64ContratanteUsuario_ContratanteRaz;
            Z1020ContratanteUsuario_AreaTrabalhoCod = A1020ContratanteUsuario_AreaTrabalhoCod;
         }
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z62ContratanteUsuario_UsuarioPessoaNom = A62ContratanteUsuario_UsuarioPessoaNom;
            Z492ContratanteUsuario_UsuarioPessoaDoc = A492ContratanteUsuario_UsuarioPessoaDoc;
            Z1020ContratanteUsuario_AreaTrabalhoCod = A1020ContratanteUsuario_AreaTrabalhoCod;
         }
         if ( GX_JID == -1 )
         {
            Z1728ContratanteUsuario_EhFiscal = A1728ContratanteUsuario_EhFiscal;
            Z63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
            Z60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
            Z65ContratanteUsuario_ContratanteFan = A65ContratanteUsuario_ContratanteFan;
            Z340ContratanteUsuario_ContratantePesCod = A340ContratanteUsuario_ContratantePesCod;
            Z64ContratanteUsuario_ContratanteRaz = A64ContratanteUsuario_ContratanteRaz;
            Z2Usuario_Nome = A2Usuario_Nome;
            Z1019ContratanteUsuario_UsuarioEhContratante = A1019ContratanteUsuario_UsuarioEhContratante;
            Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            Z61ContratanteUsuario_UsuarioPessoaCod = A61ContratanteUsuario_UsuarioPessoaCod;
            Z62ContratanteUsuario_UsuarioPessoaNom = A62ContratanteUsuario_UsuarioPessoaNom;
            Z492ContratanteUsuario_UsuarioPessoaDoc = A492ContratanteUsuario_UsuarioPessoaDoc;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load0D15( )
      {
         /* Using cursor BC000D10 */
         pr_default.execute(7, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound15 = 1;
            A65ContratanteUsuario_ContratanteFan = BC000D10_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = BC000D10_n65ContratanteUsuario_ContratanteFan[0];
            A64ContratanteUsuario_ContratanteRaz = BC000D10_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = BC000D10_n64ContratanteUsuario_ContratanteRaz[0];
            A2Usuario_Nome = BC000D10_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000D10_n2Usuario_Nome[0];
            A62ContratanteUsuario_UsuarioPessoaNom = BC000D10_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = BC000D10_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A492ContratanteUsuario_UsuarioPessoaDoc = BC000D10_A492ContratanteUsuario_UsuarioPessoaDoc[0];
            n492ContratanteUsuario_UsuarioPessoaDoc = BC000D10_n492ContratanteUsuario_UsuarioPessoaDoc[0];
            A1019ContratanteUsuario_UsuarioEhContratante = BC000D10_A1019ContratanteUsuario_UsuarioEhContratante[0];
            n1019ContratanteUsuario_UsuarioEhContratante = BC000D10_n1019ContratanteUsuario_UsuarioEhContratante[0];
            A341Usuario_UserGamGuid = BC000D10_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = BC000D10_n341Usuario_UserGamGuid[0];
            A1728ContratanteUsuario_EhFiscal = BC000D10_A1728ContratanteUsuario_EhFiscal[0];
            n1728ContratanteUsuario_EhFiscal = BC000D10_n1728ContratanteUsuario_EhFiscal[0];
            A340ContratanteUsuario_ContratantePesCod = BC000D10_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = BC000D10_n340ContratanteUsuario_ContratantePesCod[0];
            A61ContratanteUsuario_UsuarioPessoaCod = BC000D10_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = BC000D10_n61ContratanteUsuario_UsuarioPessoaCod[0];
            ZM0D15( -1) ;
         }
         pr_default.close(7);
         OnLoadActions0D15( ) ;
      }

      protected void OnLoadActions0D15( )
      {
         /* Using cursor BC000D5 */
         pr_default.execute(2, new Object[] {A63ContratanteUsuario_ContratanteCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = BC000D5_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = BC000D5_n1020ContratanteUsuario_AreaTrabalhoCod[0];
         }
         else
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = 0;
            n1020ContratanteUsuario_AreaTrabalhoCod = false;
         }
         pr_default.close(2);
      }

      protected void CheckExtendedTable0D15( )
      {
         standaloneModal( ) ;
         /* Using cursor BC000D5 */
         pr_default.execute(2, new Object[] {A63ContratanteUsuario_ContratanteCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = BC000D5_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = BC000D5_n1020ContratanteUsuario_AreaTrabalhoCod[0];
         }
         else
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = 0;
            n1020ContratanteUsuario_AreaTrabalhoCod = false;
         }
         pr_default.close(2);
         /* Using cursor BC000D6 */
         pr_default.execute(3, new Object[] {A63ContratanteUsuario_ContratanteCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratante Usuario_Contratante'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
            AnyError = 1;
         }
         A65ContratanteUsuario_ContratanteFan = BC000D6_A65ContratanteUsuario_ContratanteFan[0];
         n65ContratanteUsuario_ContratanteFan = BC000D6_n65ContratanteUsuario_ContratanteFan[0];
         A340ContratanteUsuario_ContratantePesCod = BC000D6_A340ContratanteUsuario_ContratantePesCod[0];
         n340ContratanteUsuario_ContratantePesCod = BC000D6_n340ContratanteUsuario_ContratantePesCod[0];
         pr_default.close(3);
         /* Using cursor BC000D8 */
         pr_default.execute(5, new Object[] {n340ContratanteUsuario_ContratantePesCod, A340ContratanteUsuario_ContratantePesCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A64ContratanteUsuario_ContratanteRaz = BC000D8_A64ContratanteUsuario_ContratanteRaz[0];
         n64ContratanteUsuario_ContratanteRaz = BC000D8_n64ContratanteUsuario_ContratanteRaz[0];
         pr_default.close(5);
         /* Using cursor BC000D7 */
         pr_default.execute(4, new Object[] {A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratante Usuario_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_USUARIOCOD");
            AnyError = 1;
         }
         A2Usuario_Nome = BC000D7_A2Usuario_Nome[0];
         n2Usuario_Nome = BC000D7_n2Usuario_Nome[0];
         A1019ContratanteUsuario_UsuarioEhContratante = BC000D7_A1019ContratanteUsuario_UsuarioEhContratante[0];
         n1019ContratanteUsuario_UsuarioEhContratante = BC000D7_n1019ContratanteUsuario_UsuarioEhContratante[0];
         A341Usuario_UserGamGuid = BC000D7_A341Usuario_UserGamGuid[0];
         n341Usuario_UserGamGuid = BC000D7_n341Usuario_UserGamGuid[0];
         A61ContratanteUsuario_UsuarioPessoaCod = BC000D7_A61ContratanteUsuario_UsuarioPessoaCod[0];
         n61ContratanteUsuario_UsuarioPessoaCod = BC000D7_n61ContratanteUsuario_UsuarioPessoaCod[0];
         pr_default.close(4);
         /* Using cursor BC000D9 */
         pr_default.execute(6, new Object[] {n61ContratanteUsuario_UsuarioPessoaCod, A61ContratanteUsuario_UsuarioPessoaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A62ContratanteUsuario_UsuarioPessoaNom = BC000D9_A62ContratanteUsuario_UsuarioPessoaNom[0];
         n62ContratanteUsuario_UsuarioPessoaNom = BC000D9_n62ContratanteUsuario_UsuarioPessoaNom[0];
         A492ContratanteUsuario_UsuarioPessoaDoc = BC000D9_A492ContratanteUsuario_UsuarioPessoaDoc[0];
         n492ContratanteUsuario_UsuarioPessoaDoc = BC000D9_n492ContratanteUsuario_UsuarioPessoaDoc[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors0D15( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(5);
         pr_default.close(4);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0D15( )
      {
         /* Using cursor BC000D11 */
         pr_default.execute(8, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound15 = 1;
         }
         else
         {
            RcdFound15 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC000D3 */
         pr_default.execute(1, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0D15( 1) ;
            RcdFound15 = 1;
            A1728ContratanteUsuario_EhFiscal = BC000D3_A1728ContratanteUsuario_EhFiscal[0];
            n1728ContratanteUsuario_EhFiscal = BC000D3_n1728ContratanteUsuario_EhFiscal[0];
            A63ContratanteUsuario_ContratanteCod = BC000D3_A63ContratanteUsuario_ContratanteCod[0];
            A60ContratanteUsuario_UsuarioCod = BC000D3_A60ContratanteUsuario_UsuarioCod[0];
            Z63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
            Z60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
            sMode15 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load0D15( ) ;
            if ( AnyError == 1 )
            {
               RcdFound15 = 0;
               InitializeNonKey0D15( ) ;
            }
            Gx_mode = sMode15;
         }
         else
         {
            RcdFound15 = 0;
            InitializeNonKey0D15( ) ;
            sMode15 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode15;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0D15( ) ;
         if ( RcdFound15 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_0D0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency0D15( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000D2 */
            pr_default.execute(0, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratanteUsuario"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1728ContratanteUsuario_EhFiscal != BC000D2_A1728ContratanteUsuario_EhFiscal[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratanteUsuario"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0D15( )
      {
         BeforeValidate0D15( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0D15( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0D15( 0) ;
            CheckOptimisticConcurrency0D15( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0D15( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0D15( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000D12 */
                     pr_default.execute(9, new Object[] {n1728ContratanteUsuario_EhFiscal, A1728ContratanteUsuario_EhFiscal, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuario") ;
                     if ( (pr_default.getStatus(9) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0D15( ) ;
            }
            EndLevel0D15( ) ;
         }
         CloseExtendedTableCursors0D15( ) ;
      }

      protected void Update0D15( )
      {
         BeforeValidate0D15( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0D15( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0D15( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0D15( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0D15( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000D13 */
                     pr_default.execute(10, new Object[] {n1728ContratanteUsuario_EhFiscal, A1728ContratanteUsuario_EhFiscal, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuario") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratanteUsuario"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0D15( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0D15( ) ;
         }
         CloseExtendedTableCursors0D15( ) ;
      }

      protected void DeferredUpdate0D15( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate0D15( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0D15( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0D15( ) ;
            AfterConfirm0D15( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0D15( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000D14 */
                  pr_default.execute(11, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuario") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode15 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0D15( ) ;
         Gx_mode = sMode15;
      }

      protected void OnDeleteControls0D15( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000D16 */
            pr_default.execute(12, new Object[] {A63ContratanteUsuario_ContratanteCod});
            if ( (pr_default.getStatus(12) != 101) )
            {
               A1020ContratanteUsuario_AreaTrabalhoCod = BC000D16_A1020ContratanteUsuario_AreaTrabalhoCod[0];
               n1020ContratanteUsuario_AreaTrabalhoCod = BC000D16_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            }
            else
            {
               A1020ContratanteUsuario_AreaTrabalhoCod = 0;
               n1020ContratanteUsuario_AreaTrabalhoCod = false;
            }
            pr_default.close(12);
            /* Using cursor BC000D17 */
            pr_default.execute(13, new Object[] {A63ContratanteUsuario_ContratanteCod});
            A65ContratanteUsuario_ContratanteFan = BC000D17_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = BC000D17_n65ContratanteUsuario_ContratanteFan[0];
            A340ContratanteUsuario_ContratantePesCod = BC000D17_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = BC000D17_n340ContratanteUsuario_ContratantePesCod[0];
            pr_default.close(13);
            /* Using cursor BC000D18 */
            pr_default.execute(14, new Object[] {n340ContratanteUsuario_ContratantePesCod, A340ContratanteUsuario_ContratantePesCod});
            A64ContratanteUsuario_ContratanteRaz = BC000D18_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = BC000D18_n64ContratanteUsuario_ContratanteRaz[0];
            pr_default.close(14);
            /* Using cursor BC000D19 */
            pr_default.execute(15, new Object[] {A60ContratanteUsuario_UsuarioCod});
            A2Usuario_Nome = BC000D19_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000D19_n2Usuario_Nome[0];
            A1019ContratanteUsuario_UsuarioEhContratante = BC000D19_A1019ContratanteUsuario_UsuarioEhContratante[0];
            n1019ContratanteUsuario_UsuarioEhContratante = BC000D19_n1019ContratanteUsuario_UsuarioEhContratante[0];
            A341Usuario_UserGamGuid = BC000D19_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = BC000D19_n341Usuario_UserGamGuid[0];
            A61ContratanteUsuario_UsuarioPessoaCod = BC000D19_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = BC000D19_n61ContratanteUsuario_UsuarioPessoaCod[0];
            pr_default.close(15);
            /* Using cursor BC000D20 */
            pr_default.execute(16, new Object[] {n61ContratanteUsuario_UsuarioPessoaCod, A61ContratanteUsuario_UsuarioPessoaCod});
            A62ContratanteUsuario_UsuarioPessoaNom = BC000D20_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = BC000D20_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A492ContratanteUsuario_UsuarioPessoaDoc = BC000D20_A492ContratanteUsuario_UsuarioPessoaDoc[0];
            n492ContratanteUsuario_UsuarioPessoaDoc = BC000D20_n492ContratanteUsuario_UsuarioPessoaDoc[0];
            pr_default.close(16);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000D21 */
            pr_default.execute(17, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas do Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
            /* Using cursor BC000D22 */
            pr_default.execute(18, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gpo Obj Ctrl Responsavel"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
            /* Using cursor BC000D23 */
            pr_default.execute(19, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico Responsavel"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
         }
      }

      protected void EndLevel0D15( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0D15( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0D15( )
      {
         /* Scan By routine */
         /* Using cursor BC000D24 */
         pr_default.execute(20, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
         RcdFound15 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound15 = 1;
            A65ContratanteUsuario_ContratanteFan = BC000D24_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = BC000D24_n65ContratanteUsuario_ContratanteFan[0];
            A64ContratanteUsuario_ContratanteRaz = BC000D24_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = BC000D24_n64ContratanteUsuario_ContratanteRaz[0];
            A2Usuario_Nome = BC000D24_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000D24_n2Usuario_Nome[0];
            A62ContratanteUsuario_UsuarioPessoaNom = BC000D24_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = BC000D24_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A492ContratanteUsuario_UsuarioPessoaDoc = BC000D24_A492ContratanteUsuario_UsuarioPessoaDoc[0];
            n492ContratanteUsuario_UsuarioPessoaDoc = BC000D24_n492ContratanteUsuario_UsuarioPessoaDoc[0];
            A1019ContratanteUsuario_UsuarioEhContratante = BC000D24_A1019ContratanteUsuario_UsuarioEhContratante[0];
            n1019ContratanteUsuario_UsuarioEhContratante = BC000D24_n1019ContratanteUsuario_UsuarioEhContratante[0];
            A341Usuario_UserGamGuid = BC000D24_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = BC000D24_n341Usuario_UserGamGuid[0];
            A1728ContratanteUsuario_EhFiscal = BC000D24_A1728ContratanteUsuario_EhFiscal[0];
            n1728ContratanteUsuario_EhFiscal = BC000D24_n1728ContratanteUsuario_EhFiscal[0];
            A63ContratanteUsuario_ContratanteCod = BC000D24_A63ContratanteUsuario_ContratanteCod[0];
            A60ContratanteUsuario_UsuarioCod = BC000D24_A60ContratanteUsuario_UsuarioCod[0];
            A340ContratanteUsuario_ContratantePesCod = BC000D24_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = BC000D24_n340ContratanteUsuario_ContratantePesCod[0];
            A61ContratanteUsuario_UsuarioPessoaCod = BC000D24_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = BC000D24_n61ContratanteUsuario_UsuarioPessoaCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0D15( )
      {
         /* Scan next routine */
         pr_default.readNext(20);
         RcdFound15 = 0;
         ScanKeyLoad0D15( ) ;
      }

      protected void ScanKeyLoad0D15( )
      {
         sMode15 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound15 = 1;
            A65ContratanteUsuario_ContratanteFan = BC000D24_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = BC000D24_n65ContratanteUsuario_ContratanteFan[0];
            A64ContratanteUsuario_ContratanteRaz = BC000D24_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = BC000D24_n64ContratanteUsuario_ContratanteRaz[0];
            A2Usuario_Nome = BC000D24_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000D24_n2Usuario_Nome[0];
            A62ContratanteUsuario_UsuarioPessoaNom = BC000D24_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = BC000D24_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A492ContratanteUsuario_UsuarioPessoaDoc = BC000D24_A492ContratanteUsuario_UsuarioPessoaDoc[0];
            n492ContratanteUsuario_UsuarioPessoaDoc = BC000D24_n492ContratanteUsuario_UsuarioPessoaDoc[0];
            A1019ContratanteUsuario_UsuarioEhContratante = BC000D24_A1019ContratanteUsuario_UsuarioEhContratante[0];
            n1019ContratanteUsuario_UsuarioEhContratante = BC000D24_n1019ContratanteUsuario_UsuarioEhContratante[0];
            A341Usuario_UserGamGuid = BC000D24_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = BC000D24_n341Usuario_UserGamGuid[0];
            A1728ContratanteUsuario_EhFiscal = BC000D24_A1728ContratanteUsuario_EhFiscal[0];
            n1728ContratanteUsuario_EhFiscal = BC000D24_n1728ContratanteUsuario_EhFiscal[0];
            A63ContratanteUsuario_ContratanteCod = BC000D24_A63ContratanteUsuario_ContratanteCod[0];
            A60ContratanteUsuario_UsuarioCod = BC000D24_A60ContratanteUsuario_UsuarioCod[0];
            A340ContratanteUsuario_ContratantePesCod = BC000D24_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = BC000D24_n340ContratanteUsuario_ContratantePesCod[0];
            A61ContratanteUsuario_UsuarioPessoaCod = BC000D24_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = BC000D24_n61ContratanteUsuario_UsuarioPessoaCod[0];
         }
         Gx_mode = sMode15;
      }

      protected void ScanKeyEnd0D15( )
      {
         pr_default.close(20);
      }

      protected void AfterConfirm0D15( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0D15( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0D15( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0D15( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0D15( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0D15( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0D15( )
      {
      }

      protected void AddRow0D15( )
      {
         VarsToRow15( bcContratanteUsuario) ;
      }

      protected void ReadRow0D15( )
      {
         RowToVars15( bcContratanteUsuario, 1) ;
      }

      protected void InitializeNonKey0D15( )
      {
         A1020ContratanteUsuario_AreaTrabalhoCod = 0;
         n1020ContratanteUsuario_AreaTrabalhoCod = false;
         A340ContratanteUsuario_ContratantePesCod = 0;
         n340ContratanteUsuario_ContratantePesCod = false;
         A65ContratanteUsuario_ContratanteFan = "";
         n65ContratanteUsuario_ContratanteFan = false;
         A64ContratanteUsuario_ContratanteRaz = "";
         n64ContratanteUsuario_ContratanteRaz = false;
         A2Usuario_Nome = "";
         n2Usuario_Nome = false;
         A61ContratanteUsuario_UsuarioPessoaCod = 0;
         n61ContratanteUsuario_UsuarioPessoaCod = false;
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         n62ContratanteUsuario_UsuarioPessoaNom = false;
         A492ContratanteUsuario_UsuarioPessoaDoc = "";
         n492ContratanteUsuario_UsuarioPessoaDoc = false;
         A1019ContratanteUsuario_UsuarioEhContratante = false;
         n1019ContratanteUsuario_UsuarioEhContratante = false;
         A341Usuario_UserGamGuid = "";
         n341Usuario_UserGamGuid = false;
         A1728ContratanteUsuario_EhFiscal = false;
         n1728ContratanteUsuario_EhFiscal = false;
         Z1728ContratanteUsuario_EhFiscal = false;
      }

      protected void InitAll0D15( )
      {
         A63ContratanteUsuario_ContratanteCod = 0;
         A60ContratanteUsuario_UsuarioCod = 0;
         InitializeNonKey0D15( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow15( SdtContratanteUsuario obj15 )
      {
         obj15.gxTpr_Mode = Gx_mode;
         obj15.gxTpr_Contratanteusuario_areatrabalhocod = A1020ContratanteUsuario_AreaTrabalhoCod;
         obj15.gxTpr_Contratanteusuario_contratantepescod = A340ContratanteUsuario_ContratantePesCod;
         obj15.gxTpr_Contratanteusuario_contratantefan = A65ContratanteUsuario_ContratanteFan;
         obj15.gxTpr_Contratanteusuario_contratanteraz = A64ContratanteUsuario_ContratanteRaz;
         obj15.gxTpr_Usuario_nome = A2Usuario_Nome;
         obj15.gxTpr_Contratanteusuario_usuariopessoacod = A61ContratanteUsuario_UsuarioPessoaCod;
         obj15.gxTpr_Contratanteusuario_usuariopessoanom = A62ContratanteUsuario_UsuarioPessoaNom;
         obj15.gxTpr_Contratanteusuario_usuariopessoadoc = A492ContratanteUsuario_UsuarioPessoaDoc;
         obj15.gxTpr_Contratanteusuario_usuarioehcontratante = A1019ContratanteUsuario_UsuarioEhContratante;
         obj15.gxTpr_Usuario_usergamguid = A341Usuario_UserGamGuid;
         obj15.gxTpr_Contratanteusuario_ehfiscal = A1728ContratanteUsuario_EhFiscal;
         obj15.gxTpr_Contratanteusuario_contratantecod = A63ContratanteUsuario_ContratanteCod;
         obj15.gxTpr_Contratanteusuario_usuariocod = A60ContratanteUsuario_UsuarioCod;
         obj15.gxTpr_Contratanteusuario_contratantecod_Z = Z63ContratanteUsuario_ContratanteCod;
         obj15.gxTpr_Contratanteusuario_contratantepescod_Z = Z340ContratanteUsuario_ContratantePesCod;
         obj15.gxTpr_Contratanteusuario_contratantefan_Z = Z65ContratanteUsuario_ContratanteFan;
         obj15.gxTpr_Contratanteusuario_contratanteraz_Z = Z64ContratanteUsuario_ContratanteRaz;
         obj15.gxTpr_Contratanteusuario_usuariocod_Z = Z60ContratanteUsuario_UsuarioCod;
         obj15.gxTpr_Usuario_nome_Z = Z2Usuario_Nome;
         obj15.gxTpr_Contratanteusuario_usuariopessoacod_Z = Z61ContratanteUsuario_UsuarioPessoaCod;
         obj15.gxTpr_Contratanteusuario_usuariopessoanom_Z = Z62ContratanteUsuario_UsuarioPessoaNom;
         obj15.gxTpr_Contratanteusuario_usuariopessoadoc_Z = Z492ContratanteUsuario_UsuarioPessoaDoc;
         obj15.gxTpr_Contratanteusuario_usuarioehcontratante_Z = Z1019ContratanteUsuario_UsuarioEhContratante;
         obj15.gxTpr_Contratanteusuario_areatrabalhocod_Z = Z1020ContratanteUsuario_AreaTrabalhoCod;
         obj15.gxTpr_Usuario_usergamguid_Z = Z341Usuario_UserGamGuid;
         obj15.gxTpr_Contratanteusuario_ehfiscal_Z = Z1728ContratanteUsuario_EhFiscal;
         obj15.gxTpr_Contratanteusuario_contratantepescod_N = (short)(Convert.ToInt16(n340ContratanteUsuario_ContratantePesCod));
         obj15.gxTpr_Contratanteusuario_contratantefan_N = (short)(Convert.ToInt16(n65ContratanteUsuario_ContratanteFan));
         obj15.gxTpr_Contratanteusuario_contratanteraz_N = (short)(Convert.ToInt16(n64ContratanteUsuario_ContratanteRaz));
         obj15.gxTpr_Usuario_nome_N = (short)(Convert.ToInt16(n2Usuario_Nome));
         obj15.gxTpr_Contratanteusuario_usuariopessoacod_N = (short)(Convert.ToInt16(n61ContratanteUsuario_UsuarioPessoaCod));
         obj15.gxTpr_Contratanteusuario_usuariopessoanom_N = (short)(Convert.ToInt16(n62ContratanteUsuario_UsuarioPessoaNom));
         obj15.gxTpr_Contratanteusuario_usuariopessoadoc_N = (short)(Convert.ToInt16(n492ContratanteUsuario_UsuarioPessoaDoc));
         obj15.gxTpr_Contratanteusuario_usuarioehcontratante_N = (short)(Convert.ToInt16(n1019ContratanteUsuario_UsuarioEhContratante));
         obj15.gxTpr_Contratanteusuario_areatrabalhocod_N = (short)(Convert.ToInt16(n1020ContratanteUsuario_AreaTrabalhoCod));
         obj15.gxTpr_Usuario_usergamguid_N = (short)(Convert.ToInt16(n341Usuario_UserGamGuid));
         obj15.gxTpr_Contratanteusuario_ehfiscal_N = (short)(Convert.ToInt16(n1728ContratanteUsuario_EhFiscal));
         obj15.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow15( SdtContratanteUsuario obj15 )
      {
         obj15.gxTpr_Contratanteusuario_contratantecod = A63ContratanteUsuario_ContratanteCod;
         obj15.gxTpr_Contratanteusuario_usuariocod = A60ContratanteUsuario_UsuarioCod;
         return  ;
      }

      public void RowToVars15( SdtContratanteUsuario obj15 ,
                               int forceLoad )
      {
         Gx_mode = obj15.gxTpr_Mode;
         A1020ContratanteUsuario_AreaTrabalhoCod = obj15.gxTpr_Contratanteusuario_areatrabalhocod;
         n1020ContratanteUsuario_AreaTrabalhoCod = false;
         A340ContratanteUsuario_ContratantePesCod = obj15.gxTpr_Contratanteusuario_contratantepescod;
         n340ContratanteUsuario_ContratantePesCod = false;
         A65ContratanteUsuario_ContratanteFan = obj15.gxTpr_Contratanteusuario_contratantefan;
         n65ContratanteUsuario_ContratanteFan = false;
         A64ContratanteUsuario_ContratanteRaz = obj15.gxTpr_Contratanteusuario_contratanteraz;
         n64ContratanteUsuario_ContratanteRaz = false;
         A2Usuario_Nome = obj15.gxTpr_Usuario_nome;
         n2Usuario_Nome = false;
         A61ContratanteUsuario_UsuarioPessoaCod = obj15.gxTpr_Contratanteusuario_usuariopessoacod;
         n61ContratanteUsuario_UsuarioPessoaCod = false;
         A62ContratanteUsuario_UsuarioPessoaNom = obj15.gxTpr_Contratanteusuario_usuariopessoanom;
         n62ContratanteUsuario_UsuarioPessoaNom = false;
         A492ContratanteUsuario_UsuarioPessoaDoc = obj15.gxTpr_Contratanteusuario_usuariopessoadoc;
         n492ContratanteUsuario_UsuarioPessoaDoc = false;
         A1019ContratanteUsuario_UsuarioEhContratante = obj15.gxTpr_Contratanteusuario_usuarioehcontratante;
         n1019ContratanteUsuario_UsuarioEhContratante = false;
         A341Usuario_UserGamGuid = obj15.gxTpr_Usuario_usergamguid;
         n341Usuario_UserGamGuid = false;
         A1728ContratanteUsuario_EhFiscal = obj15.gxTpr_Contratanteusuario_ehfiscal;
         n1728ContratanteUsuario_EhFiscal = false;
         A63ContratanteUsuario_ContratanteCod = obj15.gxTpr_Contratanteusuario_contratantecod;
         A60ContratanteUsuario_UsuarioCod = obj15.gxTpr_Contratanteusuario_usuariocod;
         Z63ContratanteUsuario_ContratanteCod = obj15.gxTpr_Contratanteusuario_contratantecod_Z;
         Z340ContratanteUsuario_ContratantePesCod = obj15.gxTpr_Contratanteusuario_contratantepescod_Z;
         Z65ContratanteUsuario_ContratanteFan = obj15.gxTpr_Contratanteusuario_contratantefan_Z;
         Z64ContratanteUsuario_ContratanteRaz = obj15.gxTpr_Contratanteusuario_contratanteraz_Z;
         Z60ContratanteUsuario_UsuarioCod = obj15.gxTpr_Contratanteusuario_usuariocod_Z;
         Z2Usuario_Nome = obj15.gxTpr_Usuario_nome_Z;
         Z61ContratanteUsuario_UsuarioPessoaCod = obj15.gxTpr_Contratanteusuario_usuariopessoacod_Z;
         Z62ContratanteUsuario_UsuarioPessoaNom = obj15.gxTpr_Contratanteusuario_usuariopessoanom_Z;
         Z492ContratanteUsuario_UsuarioPessoaDoc = obj15.gxTpr_Contratanteusuario_usuariopessoadoc_Z;
         Z1019ContratanteUsuario_UsuarioEhContratante = obj15.gxTpr_Contratanteusuario_usuarioehcontratante_Z;
         Z1020ContratanteUsuario_AreaTrabalhoCod = obj15.gxTpr_Contratanteusuario_areatrabalhocod_Z;
         Z341Usuario_UserGamGuid = obj15.gxTpr_Usuario_usergamguid_Z;
         Z1728ContratanteUsuario_EhFiscal = obj15.gxTpr_Contratanteusuario_ehfiscal_Z;
         n340ContratanteUsuario_ContratantePesCod = (bool)(Convert.ToBoolean(obj15.gxTpr_Contratanteusuario_contratantepescod_N));
         n65ContratanteUsuario_ContratanteFan = (bool)(Convert.ToBoolean(obj15.gxTpr_Contratanteusuario_contratantefan_N));
         n64ContratanteUsuario_ContratanteRaz = (bool)(Convert.ToBoolean(obj15.gxTpr_Contratanteusuario_contratanteraz_N));
         n2Usuario_Nome = (bool)(Convert.ToBoolean(obj15.gxTpr_Usuario_nome_N));
         n61ContratanteUsuario_UsuarioPessoaCod = (bool)(Convert.ToBoolean(obj15.gxTpr_Contratanteusuario_usuariopessoacod_N));
         n62ContratanteUsuario_UsuarioPessoaNom = (bool)(Convert.ToBoolean(obj15.gxTpr_Contratanteusuario_usuariopessoanom_N));
         n492ContratanteUsuario_UsuarioPessoaDoc = (bool)(Convert.ToBoolean(obj15.gxTpr_Contratanteusuario_usuariopessoadoc_N));
         n1019ContratanteUsuario_UsuarioEhContratante = (bool)(Convert.ToBoolean(obj15.gxTpr_Contratanteusuario_usuarioehcontratante_N));
         n1020ContratanteUsuario_AreaTrabalhoCod = (bool)(Convert.ToBoolean(obj15.gxTpr_Contratanteusuario_areatrabalhocod_N));
         n341Usuario_UserGamGuid = (bool)(Convert.ToBoolean(obj15.gxTpr_Usuario_usergamguid_N));
         n1728ContratanteUsuario_EhFiscal = (bool)(Convert.ToBoolean(obj15.gxTpr_Contratanteusuario_ehfiscal_N));
         Gx_mode = obj15.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A63ContratanteUsuario_ContratanteCod = (int)getParm(obj,0);
         A60ContratanteUsuario_UsuarioCod = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey0D15( ) ;
         ScanKeyStart0D15( ) ;
         if ( RcdFound15 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC000D16 */
            pr_default.execute(12, new Object[] {A63ContratanteUsuario_ContratanteCod});
            if ( (pr_default.getStatus(12) != 101) )
            {
               A1020ContratanteUsuario_AreaTrabalhoCod = BC000D16_A1020ContratanteUsuario_AreaTrabalhoCod[0];
               n1020ContratanteUsuario_AreaTrabalhoCod = BC000D16_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            }
            else
            {
               A1020ContratanteUsuario_AreaTrabalhoCod = 0;
               n1020ContratanteUsuario_AreaTrabalhoCod = false;
            }
            pr_default.close(12);
            /* Using cursor BC000D17 */
            pr_default.execute(13, new Object[] {A63ContratanteUsuario_ContratanteCod});
            if ( (pr_default.getStatus(13) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratante Usuario_Contratante'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
               AnyError = 1;
            }
            A65ContratanteUsuario_ContratanteFan = BC000D17_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = BC000D17_n65ContratanteUsuario_ContratanteFan[0];
            A340ContratanteUsuario_ContratantePesCod = BC000D17_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = BC000D17_n340ContratanteUsuario_ContratantePesCod[0];
            pr_default.close(13);
            /* Using cursor BC000D18 */
            pr_default.execute(14, new Object[] {n340ContratanteUsuario_ContratantePesCod, A340ContratanteUsuario_ContratantePesCod});
            if ( (pr_default.getStatus(14) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A64ContratanteUsuario_ContratanteRaz = BC000D18_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = BC000D18_n64ContratanteUsuario_ContratanteRaz[0];
            pr_default.close(14);
            /* Using cursor BC000D19 */
            pr_default.execute(15, new Object[] {A60ContratanteUsuario_UsuarioCod});
            if ( (pr_default.getStatus(15) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratante Usuario_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_USUARIOCOD");
               AnyError = 1;
            }
            A2Usuario_Nome = BC000D19_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000D19_n2Usuario_Nome[0];
            A1019ContratanteUsuario_UsuarioEhContratante = BC000D19_A1019ContratanteUsuario_UsuarioEhContratante[0];
            n1019ContratanteUsuario_UsuarioEhContratante = BC000D19_n1019ContratanteUsuario_UsuarioEhContratante[0];
            A341Usuario_UserGamGuid = BC000D19_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = BC000D19_n341Usuario_UserGamGuid[0];
            A61ContratanteUsuario_UsuarioPessoaCod = BC000D19_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = BC000D19_n61ContratanteUsuario_UsuarioPessoaCod[0];
            pr_default.close(15);
            /* Using cursor BC000D20 */
            pr_default.execute(16, new Object[] {n61ContratanteUsuario_UsuarioPessoaCod, A61ContratanteUsuario_UsuarioPessoaCod});
            if ( (pr_default.getStatus(16) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A62ContratanteUsuario_UsuarioPessoaNom = BC000D20_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = BC000D20_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A492ContratanteUsuario_UsuarioPessoaDoc = BC000D20_A492ContratanteUsuario_UsuarioPessoaDoc[0];
            n492ContratanteUsuario_UsuarioPessoaDoc = BC000D20_n492ContratanteUsuario_UsuarioPessoaDoc[0];
            pr_default.close(16);
         }
         else
         {
            Gx_mode = "UPD";
            Z63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
            Z60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
         }
         ZM0D15( -1) ;
         OnLoadActions0D15( ) ;
         AddRow0D15( ) ;
         ScanKeyEnd0D15( ) ;
         if ( RcdFound15 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars15( bcContratanteUsuario, 0) ;
         ScanKeyStart0D15( ) ;
         if ( RcdFound15 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC000D16 */
            pr_default.execute(12, new Object[] {A63ContratanteUsuario_ContratanteCod});
            if ( (pr_default.getStatus(12) != 101) )
            {
               A1020ContratanteUsuario_AreaTrabalhoCod = BC000D16_A1020ContratanteUsuario_AreaTrabalhoCod[0];
               n1020ContratanteUsuario_AreaTrabalhoCod = BC000D16_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            }
            else
            {
               A1020ContratanteUsuario_AreaTrabalhoCod = 0;
               n1020ContratanteUsuario_AreaTrabalhoCod = false;
            }
            pr_default.close(12);
            /* Using cursor BC000D17 */
            pr_default.execute(13, new Object[] {A63ContratanteUsuario_ContratanteCod});
            if ( (pr_default.getStatus(13) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratante Usuario_Contratante'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
               AnyError = 1;
            }
            A65ContratanteUsuario_ContratanteFan = BC000D17_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = BC000D17_n65ContratanteUsuario_ContratanteFan[0];
            A340ContratanteUsuario_ContratantePesCod = BC000D17_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = BC000D17_n340ContratanteUsuario_ContratantePesCod[0];
            pr_default.close(13);
            /* Using cursor BC000D18 */
            pr_default.execute(14, new Object[] {n340ContratanteUsuario_ContratantePesCod, A340ContratanteUsuario_ContratantePesCod});
            if ( (pr_default.getStatus(14) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A64ContratanteUsuario_ContratanteRaz = BC000D18_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = BC000D18_n64ContratanteUsuario_ContratanteRaz[0];
            pr_default.close(14);
            /* Using cursor BC000D19 */
            pr_default.execute(15, new Object[] {A60ContratanteUsuario_UsuarioCod});
            if ( (pr_default.getStatus(15) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratante Usuario_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_USUARIOCOD");
               AnyError = 1;
            }
            A2Usuario_Nome = BC000D19_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000D19_n2Usuario_Nome[0];
            A1019ContratanteUsuario_UsuarioEhContratante = BC000D19_A1019ContratanteUsuario_UsuarioEhContratante[0];
            n1019ContratanteUsuario_UsuarioEhContratante = BC000D19_n1019ContratanteUsuario_UsuarioEhContratante[0];
            A341Usuario_UserGamGuid = BC000D19_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = BC000D19_n341Usuario_UserGamGuid[0];
            A61ContratanteUsuario_UsuarioPessoaCod = BC000D19_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = BC000D19_n61ContratanteUsuario_UsuarioPessoaCod[0];
            pr_default.close(15);
            /* Using cursor BC000D20 */
            pr_default.execute(16, new Object[] {n61ContratanteUsuario_UsuarioPessoaCod, A61ContratanteUsuario_UsuarioPessoaCod});
            if ( (pr_default.getStatus(16) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A62ContratanteUsuario_UsuarioPessoaNom = BC000D20_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = BC000D20_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A492ContratanteUsuario_UsuarioPessoaDoc = BC000D20_A492ContratanteUsuario_UsuarioPessoaDoc[0];
            n492ContratanteUsuario_UsuarioPessoaDoc = BC000D20_n492ContratanteUsuario_UsuarioPessoaDoc[0];
            pr_default.close(16);
         }
         else
         {
            Gx_mode = "UPD";
            Z63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
            Z60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
         }
         ZM0D15( -1) ;
         OnLoadActions0D15( ) ;
         AddRow0D15( ) ;
         ScanKeyEnd0D15( ) ;
         if ( RcdFound15 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars15( bcContratanteUsuario, 0) ;
         nKeyPressed = 1;
         GetKey0D15( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert0D15( ) ;
         }
         else
         {
            if ( RcdFound15 == 1 )
            {
               if ( ( A63ContratanteUsuario_ContratanteCod != Z63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != Z60ContratanteUsuario_UsuarioCod ) )
               {
                  A63ContratanteUsuario_ContratanteCod = Z63ContratanteUsuario_ContratanteCod;
                  A60ContratanteUsuario_UsuarioCod = Z60ContratanteUsuario_UsuarioCod;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update0D15( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A63ContratanteUsuario_ContratanteCod != Z63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != Z60ContratanteUsuario_UsuarioCod ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0D15( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0D15( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow15( bcContratanteUsuario) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars15( bcContratanteUsuario, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey0D15( ) ;
         if ( RcdFound15 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A63ContratanteUsuario_ContratanteCod != Z63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != Z60ContratanteUsuario_UsuarioCod ) )
            {
               A63ContratanteUsuario_ContratanteCod = Z63ContratanteUsuario_ContratanteCod;
               A60ContratanteUsuario_UsuarioCod = Z60ContratanteUsuario_UsuarioCod;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A63ContratanteUsuario_ContratanteCod != Z63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != Z60ContratanteUsuario_UsuarioCod ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(13);
         pr_default.close(15);
         pr_default.close(14);
         pr_default.close(16);
         context.RollbackDataStores( "ContratanteUsuario_BC");
         VarsToRow15( bcContratanteUsuario) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratanteUsuario.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratanteUsuario.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratanteUsuario )
         {
            bcContratanteUsuario = (SdtContratanteUsuario)(sdt);
            if ( StringUtil.StrCmp(bcContratanteUsuario.gxTpr_Mode, "") == 0 )
            {
               bcContratanteUsuario.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow15( bcContratanteUsuario) ;
            }
            else
            {
               RowToVars15( bcContratanteUsuario, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratanteUsuario.gxTpr_Mode, "") == 0 )
            {
               bcContratanteUsuario.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars15( bcContratanteUsuario, 1) ;
         return  ;
      }

      public SdtContratanteUsuario ContratanteUsuario_BC
      {
         get {
            return bcContratanteUsuario ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(15);
         pr_default.close(14);
         pr_default.close(16);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         Z65ContratanteUsuario_ContratanteFan = "";
         A65ContratanteUsuario_ContratanteFan = "";
         Z2Usuario_Nome = "";
         A2Usuario_Nome = "";
         Z341Usuario_UserGamGuid = "";
         A341Usuario_UserGamGuid = "";
         Z64ContratanteUsuario_ContratanteRaz = "";
         A64ContratanteUsuario_ContratanteRaz = "";
         Z62ContratanteUsuario_UsuarioPessoaNom = "";
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         Z492ContratanteUsuario_UsuarioPessoaDoc = "";
         A492ContratanteUsuario_UsuarioPessoaDoc = "";
         BC000D10_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         BC000D10_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         BC000D10_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         BC000D10_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         BC000D10_A2Usuario_Nome = new String[] {""} ;
         BC000D10_n2Usuario_Nome = new bool[] {false} ;
         BC000D10_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         BC000D10_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         BC000D10_A492ContratanteUsuario_UsuarioPessoaDoc = new String[] {""} ;
         BC000D10_n492ContratanteUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         BC000D10_A1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         BC000D10_n1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         BC000D10_A341Usuario_UserGamGuid = new String[] {""} ;
         BC000D10_n341Usuario_UserGamGuid = new bool[] {false} ;
         BC000D10_A1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         BC000D10_n1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         BC000D10_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC000D10_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC000D10_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         BC000D10_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         BC000D10_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         BC000D10_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         BC000D5_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         BC000D5_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         BC000D6_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         BC000D6_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         BC000D6_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         BC000D6_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         BC000D8_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         BC000D8_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         BC000D7_A2Usuario_Nome = new String[] {""} ;
         BC000D7_n2Usuario_Nome = new bool[] {false} ;
         BC000D7_A1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         BC000D7_n1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         BC000D7_A341Usuario_UserGamGuid = new String[] {""} ;
         BC000D7_n341Usuario_UserGamGuid = new bool[] {false} ;
         BC000D7_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         BC000D7_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         BC000D9_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         BC000D9_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         BC000D9_A492ContratanteUsuario_UsuarioPessoaDoc = new String[] {""} ;
         BC000D9_n492ContratanteUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         BC000D11_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC000D11_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC000D3_A1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         BC000D3_n1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         BC000D3_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC000D3_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         sMode15 = "";
         BC000D2_A1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         BC000D2_n1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         BC000D2_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC000D2_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC000D16_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         BC000D16_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         BC000D17_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         BC000D17_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         BC000D17_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         BC000D17_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         BC000D18_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         BC000D18_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         BC000D19_A2Usuario_Nome = new String[] {""} ;
         BC000D19_n2Usuario_Nome = new bool[] {false} ;
         BC000D19_A1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         BC000D19_n1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         BC000D19_A341Usuario_UserGamGuid = new String[] {""} ;
         BC000D19_n341Usuario_UserGamGuid = new bool[] {false} ;
         BC000D19_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         BC000D19_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         BC000D20_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         BC000D20_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         BC000D20_A492ContratanteUsuario_UsuarioPessoaDoc = new String[] {""} ;
         BC000D20_n492ContratanteUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         BC000D21_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC000D21_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC000D21_A127Sistema_Codigo = new int[1] ;
         BC000D22_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         BC000D23_A1548ServicoResponsavel_Codigo = new int[1] ;
         BC000D24_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         BC000D24_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         BC000D24_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         BC000D24_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         BC000D24_A2Usuario_Nome = new String[] {""} ;
         BC000D24_n2Usuario_Nome = new bool[] {false} ;
         BC000D24_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         BC000D24_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         BC000D24_A492ContratanteUsuario_UsuarioPessoaDoc = new String[] {""} ;
         BC000D24_n492ContratanteUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         BC000D24_A1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         BC000D24_n1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         BC000D24_A341Usuario_UserGamGuid = new String[] {""} ;
         BC000D24_n341Usuario_UserGamGuid = new bool[] {false} ;
         BC000D24_A1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         BC000D24_n1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         BC000D24_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC000D24_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC000D24_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         BC000D24_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         BC000D24_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         BC000D24_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratanteusuario_bc__default(),
            new Object[][] {
                new Object[] {
               BC000D2_A1728ContratanteUsuario_EhFiscal, BC000D2_n1728ContratanteUsuario_EhFiscal, BC000D2_A63ContratanteUsuario_ContratanteCod, BC000D2_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               BC000D3_A1728ContratanteUsuario_EhFiscal, BC000D3_n1728ContratanteUsuario_EhFiscal, BC000D3_A63ContratanteUsuario_ContratanteCod, BC000D3_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               BC000D5_A1020ContratanteUsuario_AreaTrabalhoCod, BC000D5_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               BC000D6_A65ContratanteUsuario_ContratanteFan, BC000D6_n65ContratanteUsuario_ContratanteFan, BC000D6_A340ContratanteUsuario_ContratantePesCod, BC000D6_n340ContratanteUsuario_ContratantePesCod
               }
               , new Object[] {
               BC000D7_A2Usuario_Nome, BC000D7_n2Usuario_Nome, BC000D7_A1019ContratanteUsuario_UsuarioEhContratante, BC000D7_n1019ContratanteUsuario_UsuarioEhContratante, BC000D7_A341Usuario_UserGamGuid, BC000D7_n341Usuario_UserGamGuid, BC000D7_A61ContratanteUsuario_UsuarioPessoaCod, BC000D7_n61ContratanteUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               BC000D8_A64ContratanteUsuario_ContratanteRaz, BC000D8_n64ContratanteUsuario_ContratanteRaz
               }
               , new Object[] {
               BC000D9_A62ContratanteUsuario_UsuarioPessoaNom, BC000D9_n62ContratanteUsuario_UsuarioPessoaNom, BC000D9_A492ContratanteUsuario_UsuarioPessoaDoc, BC000D9_n492ContratanteUsuario_UsuarioPessoaDoc
               }
               , new Object[] {
               BC000D10_A65ContratanteUsuario_ContratanteFan, BC000D10_n65ContratanteUsuario_ContratanteFan, BC000D10_A64ContratanteUsuario_ContratanteRaz, BC000D10_n64ContratanteUsuario_ContratanteRaz, BC000D10_A2Usuario_Nome, BC000D10_n2Usuario_Nome, BC000D10_A62ContratanteUsuario_UsuarioPessoaNom, BC000D10_n62ContratanteUsuario_UsuarioPessoaNom, BC000D10_A492ContratanteUsuario_UsuarioPessoaDoc, BC000D10_n492ContratanteUsuario_UsuarioPessoaDoc,
               BC000D10_A1019ContratanteUsuario_UsuarioEhContratante, BC000D10_n1019ContratanteUsuario_UsuarioEhContratante, BC000D10_A341Usuario_UserGamGuid, BC000D10_n341Usuario_UserGamGuid, BC000D10_A1728ContratanteUsuario_EhFiscal, BC000D10_n1728ContratanteUsuario_EhFiscal, BC000D10_A63ContratanteUsuario_ContratanteCod, BC000D10_A60ContratanteUsuario_UsuarioCod, BC000D10_A340ContratanteUsuario_ContratantePesCod, BC000D10_n340ContratanteUsuario_ContratantePesCod,
               BC000D10_A61ContratanteUsuario_UsuarioPessoaCod, BC000D10_n61ContratanteUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               BC000D11_A63ContratanteUsuario_ContratanteCod, BC000D11_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000D16_A1020ContratanteUsuario_AreaTrabalhoCod, BC000D16_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               BC000D17_A65ContratanteUsuario_ContratanteFan, BC000D17_n65ContratanteUsuario_ContratanteFan, BC000D17_A340ContratanteUsuario_ContratantePesCod, BC000D17_n340ContratanteUsuario_ContratantePesCod
               }
               , new Object[] {
               BC000D18_A64ContratanteUsuario_ContratanteRaz, BC000D18_n64ContratanteUsuario_ContratanteRaz
               }
               , new Object[] {
               BC000D19_A2Usuario_Nome, BC000D19_n2Usuario_Nome, BC000D19_A1019ContratanteUsuario_UsuarioEhContratante, BC000D19_n1019ContratanteUsuario_UsuarioEhContratante, BC000D19_A341Usuario_UserGamGuid, BC000D19_n341Usuario_UserGamGuid, BC000D19_A61ContratanteUsuario_UsuarioPessoaCod, BC000D19_n61ContratanteUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               BC000D20_A62ContratanteUsuario_UsuarioPessoaNom, BC000D20_n62ContratanteUsuario_UsuarioPessoaNom, BC000D20_A492ContratanteUsuario_UsuarioPessoaDoc, BC000D20_n492ContratanteUsuario_UsuarioPessoaDoc
               }
               , new Object[] {
               BC000D21_A63ContratanteUsuario_ContratanteCod, BC000D21_A60ContratanteUsuario_UsuarioCod, BC000D21_A127Sistema_Codigo
               }
               , new Object[] {
               BC000D22_A1837GpoObjCtrlResponsavel_Codigo
               }
               , new Object[] {
               BC000D23_A1548ServicoResponsavel_Codigo
               }
               , new Object[] {
               BC000D24_A65ContratanteUsuario_ContratanteFan, BC000D24_n65ContratanteUsuario_ContratanteFan, BC000D24_A64ContratanteUsuario_ContratanteRaz, BC000D24_n64ContratanteUsuario_ContratanteRaz, BC000D24_A2Usuario_Nome, BC000D24_n2Usuario_Nome, BC000D24_A62ContratanteUsuario_UsuarioPessoaNom, BC000D24_n62ContratanteUsuario_UsuarioPessoaNom, BC000D24_A492ContratanteUsuario_UsuarioPessoaDoc, BC000D24_n492ContratanteUsuario_UsuarioPessoaDoc,
               BC000D24_A1019ContratanteUsuario_UsuarioEhContratante, BC000D24_n1019ContratanteUsuario_UsuarioEhContratante, BC000D24_A341Usuario_UserGamGuid, BC000D24_n341Usuario_UserGamGuid, BC000D24_A1728ContratanteUsuario_EhFiscal, BC000D24_n1728ContratanteUsuario_EhFiscal, BC000D24_A63ContratanteUsuario_ContratanteCod, BC000D24_A60ContratanteUsuario_UsuarioCod, BC000D24_A340ContratanteUsuario_ContratantePesCod, BC000D24_n340ContratanteUsuario_ContratantePesCod,
               BC000D24_A61ContratanteUsuario_UsuarioPessoaCod, BC000D24_n61ContratanteUsuario_UsuarioPessoaCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E120D2 */
         E120D2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound15 ;
      private int trnEnded ;
      private int Z63ContratanteUsuario_ContratanteCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int Z60ContratanteUsuario_UsuarioCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int AV12AreaTrabalho_Codigo ;
      private int AV8ContratanteUsuario_UsuarioCod ;
      private int Z1020ContratanteUsuario_AreaTrabalhoCod ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private int Z340ContratanteUsuario_ContratantePesCod ;
      private int A340ContratanteUsuario_ContratantePesCod ;
      private int Z61ContratanteUsuario_UsuarioPessoaCod ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z65ContratanteUsuario_ContratanteFan ;
      private String A65ContratanteUsuario_ContratanteFan ;
      private String Z2Usuario_Nome ;
      private String A2Usuario_Nome ;
      private String Z341Usuario_UserGamGuid ;
      private String A341Usuario_UserGamGuid ;
      private String Z64ContratanteUsuario_ContratanteRaz ;
      private String A64ContratanteUsuario_ContratanteRaz ;
      private String Z62ContratanteUsuario_UsuarioPessoaNom ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String sMode15 ;
      private bool Z1728ContratanteUsuario_EhFiscal ;
      private bool A1728ContratanteUsuario_EhFiscal ;
      private bool Z1019ContratanteUsuario_UsuarioEhContratante ;
      private bool A1019ContratanteUsuario_UsuarioEhContratante ;
      private bool n65ContratanteUsuario_ContratanteFan ;
      private bool n64ContratanteUsuario_ContratanteRaz ;
      private bool n2Usuario_Nome ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool n492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool n1019ContratanteUsuario_UsuarioEhContratante ;
      private bool n341Usuario_UserGamGuid ;
      private bool n1728ContratanteUsuario_EhFiscal ;
      private bool n340ContratanteUsuario_ContratantePesCod ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private String Z492ContratanteUsuario_UsuarioPessoaDoc ;
      private String A492ContratanteUsuario_UsuarioPessoaDoc ;
      private IGxSession AV11WebSession ;
      private SdtContratanteUsuario bcContratanteUsuario ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC000D10_A65ContratanteUsuario_ContratanteFan ;
      private bool[] BC000D10_n65ContratanteUsuario_ContratanteFan ;
      private String[] BC000D10_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] BC000D10_n64ContratanteUsuario_ContratanteRaz ;
      private String[] BC000D10_A2Usuario_Nome ;
      private bool[] BC000D10_n2Usuario_Nome ;
      private String[] BC000D10_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] BC000D10_n62ContratanteUsuario_UsuarioPessoaNom ;
      private String[] BC000D10_A492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool[] BC000D10_n492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool[] BC000D10_A1019ContratanteUsuario_UsuarioEhContratante ;
      private bool[] BC000D10_n1019ContratanteUsuario_UsuarioEhContratante ;
      private String[] BC000D10_A341Usuario_UserGamGuid ;
      private bool[] BC000D10_n341Usuario_UserGamGuid ;
      private bool[] BC000D10_A1728ContratanteUsuario_EhFiscal ;
      private bool[] BC000D10_n1728ContratanteUsuario_EhFiscal ;
      private int[] BC000D10_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC000D10_A60ContratanteUsuario_UsuarioCod ;
      private int[] BC000D10_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] BC000D10_n340ContratanteUsuario_ContratantePesCod ;
      private int[] BC000D10_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] BC000D10_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] BC000D5_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] BC000D5_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private String[] BC000D6_A65ContratanteUsuario_ContratanteFan ;
      private bool[] BC000D6_n65ContratanteUsuario_ContratanteFan ;
      private int[] BC000D6_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] BC000D6_n340ContratanteUsuario_ContratantePesCod ;
      private String[] BC000D8_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] BC000D8_n64ContratanteUsuario_ContratanteRaz ;
      private String[] BC000D7_A2Usuario_Nome ;
      private bool[] BC000D7_n2Usuario_Nome ;
      private bool[] BC000D7_A1019ContratanteUsuario_UsuarioEhContratante ;
      private bool[] BC000D7_n1019ContratanteUsuario_UsuarioEhContratante ;
      private String[] BC000D7_A341Usuario_UserGamGuid ;
      private bool[] BC000D7_n341Usuario_UserGamGuid ;
      private int[] BC000D7_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] BC000D7_n61ContratanteUsuario_UsuarioPessoaCod ;
      private String[] BC000D9_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] BC000D9_n62ContratanteUsuario_UsuarioPessoaNom ;
      private String[] BC000D9_A492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool[] BC000D9_n492ContratanteUsuario_UsuarioPessoaDoc ;
      private int[] BC000D11_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC000D11_A60ContratanteUsuario_UsuarioCod ;
      private bool[] BC000D3_A1728ContratanteUsuario_EhFiscal ;
      private bool[] BC000D3_n1728ContratanteUsuario_EhFiscal ;
      private int[] BC000D3_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC000D3_A60ContratanteUsuario_UsuarioCod ;
      private bool[] BC000D2_A1728ContratanteUsuario_EhFiscal ;
      private bool[] BC000D2_n1728ContratanteUsuario_EhFiscal ;
      private int[] BC000D2_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC000D2_A60ContratanteUsuario_UsuarioCod ;
      private int[] BC000D16_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] BC000D16_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private String[] BC000D17_A65ContratanteUsuario_ContratanteFan ;
      private bool[] BC000D17_n65ContratanteUsuario_ContratanteFan ;
      private int[] BC000D17_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] BC000D17_n340ContratanteUsuario_ContratantePesCod ;
      private String[] BC000D18_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] BC000D18_n64ContratanteUsuario_ContratanteRaz ;
      private String[] BC000D19_A2Usuario_Nome ;
      private bool[] BC000D19_n2Usuario_Nome ;
      private bool[] BC000D19_A1019ContratanteUsuario_UsuarioEhContratante ;
      private bool[] BC000D19_n1019ContratanteUsuario_UsuarioEhContratante ;
      private String[] BC000D19_A341Usuario_UserGamGuid ;
      private bool[] BC000D19_n341Usuario_UserGamGuid ;
      private int[] BC000D19_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] BC000D19_n61ContratanteUsuario_UsuarioPessoaCod ;
      private String[] BC000D20_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] BC000D20_n62ContratanteUsuario_UsuarioPessoaNom ;
      private String[] BC000D20_A492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool[] BC000D20_n492ContratanteUsuario_UsuarioPessoaDoc ;
      private int[] BC000D21_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC000D21_A60ContratanteUsuario_UsuarioCod ;
      private int[] BC000D21_A127Sistema_Codigo ;
      private int[] BC000D22_A1837GpoObjCtrlResponsavel_Codigo ;
      private int[] BC000D23_A1548ServicoResponsavel_Codigo ;
      private String[] BC000D24_A65ContratanteUsuario_ContratanteFan ;
      private bool[] BC000D24_n65ContratanteUsuario_ContratanteFan ;
      private String[] BC000D24_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] BC000D24_n64ContratanteUsuario_ContratanteRaz ;
      private String[] BC000D24_A2Usuario_Nome ;
      private bool[] BC000D24_n2Usuario_Nome ;
      private String[] BC000D24_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] BC000D24_n62ContratanteUsuario_UsuarioPessoaNom ;
      private String[] BC000D24_A492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool[] BC000D24_n492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool[] BC000D24_A1019ContratanteUsuario_UsuarioEhContratante ;
      private bool[] BC000D24_n1019ContratanteUsuario_UsuarioEhContratante ;
      private String[] BC000D24_A341Usuario_UserGamGuid ;
      private bool[] BC000D24_n341Usuario_UserGamGuid ;
      private bool[] BC000D24_A1728ContratanteUsuario_EhFiscal ;
      private bool[] BC000D24_n1728ContratanteUsuario_EhFiscal ;
      private int[] BC000D24_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC000D24_A60ContratanteUsuario_UsuarioCod ;
      private int[] BC000D24_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] BC000D24_n340ContratanteUsuario_ContratantePesCod ;
      private int[] BC000D24_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] BC000D24_n61ContratanteUsuario_UsuarioPessoaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
   }

   public class contratanteusuario_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC000D10 ;
          prmBC000D10 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D5 ;
          prmBC000D5 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D6 ;
          prmBC000D6 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D8 ;
          prmBC000D8 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratantePesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D7 ;
          prmBC000D7 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D9 ;
          prmBC000D9 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D11 ;
          prmBC000D11 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D3 ;
          prmBC000D3 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D2 ;
          prmBC000D2 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D12 ;
          prmBC000D12 = new Object[] {
          new Object[] {"@ContratanteUsuario_EhFiscal",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D13 ;
          prmBC000D13 = new Object[] {
          new Object[] {"@ContratanteUsuario_EhFiscal",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D14 ;
          prmBC000D14 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D21 ;
          prmBC000D21 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D22 ;
          prmBC000D22 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D23 ;
          prmBC000D23 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D24 ;
          prmBC000D24 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D16 ;
          prmBC000D16 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D17 ;
          prmBC000D17 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D18 ;
          prmBC000D18 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratantePesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D19 ;
          prmBC000D19 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000D20 ;
          prmBC000D20 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC000D2", "SELECT [ContratanteUsuario_EhFiscal], [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [ContratanteUsuario] WITH (UPDLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D2,1,0,true,false )
             ,new CursorDef("BC000D3", "SELECT [ContratanteUsuario_EhFiscal], [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D3,1,0,true,false )
             ,new CursorDef("BC000D5", "SELECT COALESCE( T1.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D5,1,0,true,false )
             ,new CursorDef("BC000D6", "SELECT [Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, [Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D6,1,0,true,false )
             ,new CursorDef("BC000D7", "SELECT [Usuario_Nome], [Usuario_EhContratante] AS ContratanteUsuario_UsuarioEhContratante, [Usuario_UserGamGuid], [Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D7,1,0,true,false )
             ,new CursorDef("BC000D8", "SELECT [Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_ContratantePesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D8,1,0,true,false )
             ,new CursorDef("BC000D9", "SELECT [Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, [Pessoa_Docto] AS ContratanteUsuario_UsuarioPessoaDoc FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D9,1,0,true,false )
             ,new CursorDef("BC000D10", "SELECT T2.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, T3.[Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz, T4.[Usuario_Nome], T5.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T5.[Pessoa_Docto] AS ContratanteUsuario_UsuarioPessoaDoc, T4.[Usuario_EhContratante] AS ContratanteUsuario_UsuarioEhContratante, T4.[Usuario_UserGamGuid], TM1.[ContratanteUsuario_EhFiscal], TM1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, TM1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T2.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T4.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod FROM (((([ContratanteUsuario] TM1 WITH (NOLOCK) INNER JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = TM1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = TM1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE TM1.[ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod and TM1.[ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod ORDER BY TM1.[ContratanteUsuario_ContratanteCod], TM1.[ContratanteUsuario_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D10,100,0,true,false )
             ,new CursorDef("BC000D11", "SELECT [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D11,1,0,true,false )
             ,new CursorDef("BC000D12", "INSERT INTO [ContratanteUsuario]([ContratanteUsuario_EhFiscal], [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod]) VALUES(@ContratanteUsuario_EhFiscal, @ContratanteUsuario_ContratanteCod, @ContratanteUsuario_UsuarioCod)", GxErrorMask.GX_NOMASK,prmBC000D12)
             ,new CursorDef("BC000D13", "UPDATE [ContratanteUsuario] SET [ContratanteUsuario_EhFiscal]=@ContratanteUsuario_EhFiscal  WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod", GxErrorMask.GX_NOMASK,prmBC000D13)
             ,new CursorDef("BC000D14", "DELETE FROM [ContratanteUsuario]  WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod", GxErrorMask.GX_NOMASK,prmBC000D14)
             ,new CursorDef("BC000D16", "SELECT COALESCE( T1.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D16,1,0,true,false )
             ,new CursorDef("BC000D17", "SELECT [Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, [Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D17,1,0,true,false )
             ,new CursorDef("BC000D18", "SELECT [Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_ContratantePesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D18,1,0,true,false )
             ,new CursorDef("BC000D19", "SELECT [Usuario_Nome], [Usuario_EhContratante] AS ContratanteUsuario_UsuarioEhContratante, [Usuario_UserGamGuid], [Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D19,1,0,true,false )
             ,new CursorDef("BC000D20", "SELECT [Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, [Pessoa_Docto] AS ContratanteUsuario_UsuarioPessoaDoc FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D20,1,0,true,false )
             ,new CursorDef("BC000D21", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, [Sistema_Codigo] FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D21,1,0,true,true )
             ,new CursorDef("BC000D22", "SELECT TOP 1 [GpoObjCtrlResponsavel_Codigo] FROM [GpoObjCtrlResponsavel] WITH (NOLOCK) WHERE [GpoObjCtrlResponsavel_CteCteCod] = @ContratanteUsuario_ContratanteCod AND [GpoObjCtrlResponsavel_CteUsrCod] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D22,1,0,true,true )
             ,new CursorDef("BC000D23", "SELECT TOP 1 [ServicoResponsavel_Codigo] FROM [ServicoResponsavel] WITH (NOLOCK) WHERE [ServicoResponsavel_CteCteCod] = @ContratanteUsuario_ContratanteCod AND [ServicoResponsavel_CteUsrCod] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D23,1,0,true,true )
             ,new CursorDef("BC000D24", "SELECT T2.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, T3.[Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz, T4.[Usuario_Nome], T5.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T5.[Pessoa_Docto] AS ContratanteUsuario_UsuarioPessoaDoc, T4.[Usuario_EhContratante] AS ContratanteUsuario_UsuarioEhContratante, T4.[Usuario_UserGamGuid], TM1.[ContratanteUsuario_EhFiscal], TM1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, TM1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T2.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T4.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod FROM (((([ContratanteUsuario] TM1 WITH (NOLOCK) INNER JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = TM1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = TM1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE TM1.[ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod and TM1.[ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod ORDER BY TM1.[ContratanteUsuario_ContratanteCod], TM1.[ContratanteUsuario_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000D24,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 40) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((bool[]) buf[10])[0] = rslt.getBool(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 40) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((bool[]) buf[14])[0] = rslt.getBool(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 40) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((bool[]) buf[10])[0] = rslt.getBool(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 40) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((bool[]) buf[14])[0] = rslt.getBool(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
