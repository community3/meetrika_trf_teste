/*
               File: PRC_ReordenaFluxo
        Description: PRC_Reordena Fluxo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:30.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_reordenafluxo : GXProcedure
   {
      public prc_reordenafluxo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_reordenafluxo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Tabela ,
                           int aP1_Servico ,
                           short aP2_Desde ,
                           short aP3_Ate ,
                           String aP4_Direcao )
      {
         this.AV8Tabela = aP0_Tabela;
         this.AV10Servico = aP1_Servico;
         this.AV9Desde = aP2_Desde;
         this.AV14Ate = aP3_Ate;
         this.AV12Direcao = aP4_Direcao;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_Tabela ,
                                 int aP1_Servico ,
                                 short aP2_Desde ,
                                 short aP3_Ate ,
                                 String aP4_Direcao )
      {
         prc_reordenafluxo objprc_reordenafluxo;
         objprc_reordenafluxo = new prc_reordenafluxo();
         objprc_reordenafluxo.AV8Tabela = aP0_Tabela;
         objprc_reordenafluxo.AV10Servico = aP1_Servico;
         objprc_reordenafluxo.AV9Desde = aP2_Desde;
         objprc_reordenafluxo.AV14Ate = aP3_Ate;
         objprc_reordenafluxo.AV12Direcao = aP4_Direcao;
         objprc_reordenafluxo.context.SetSubmitInitialConfig(context);
         objprc_reordenafluxo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_reordenafluxo);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_reordenafluxo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV12Direcao, "+") == 0 )
         {
            AV13Valor = (decimal)(1);
            AV15Novo = AV9Desde;
         }
         else
         {
            AV13Valor = (decimal)(-1);
            AV15Novo = AV14Ate;
         }
         if ( StringUtil.StrCmp(AV8Tabela, "S") == 0 )
         {
            /* Using cursor P00B32 */
            pr_default.execute(0, new Object[] {AV10Servico, AV9Desde, AV14Ate});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1532ServicoFluxo_Ordem = P00B32_A1532ServicoFluxo_Ordem[0];
               n1532ServicoFluxo_Ordem = P00B32_n1532ServicoFluxo_Ordem[0];
               A1522ServicoFluxo_ServicoCod = P00B32_A1522ServicoFluxo_ServicoCod[0];
               A1528ServicoFluxo_Codigo = P00B32_A1528ServicoFluxo_Codigo[0];
               A1532ServicoFluxo_Ordem = (short)(A1532ServicoFluxo_Ordem+AV13Valor);
               n1532ServicoFluxo_Ordem = false;
               BatchSize = 100;
               pr_default.initializeBatch( 1, BatchSize, this, "Executebatchp00b33");
               /* Using cursor P00B33 */
               pr_default.addRecord(1, new Object[] {n1532ServicoFluxo_Ordem, A1532ServicoFluxo_Ordem, A1528ServicoFluxo_Codigo});
               if ( pr_default.recordCount(1) == pr_default.getBatchSize(1) )
               {
                  Executebatchp00b33( ) ;
               }
               dsDefault.SmartCacheProvider.SetUpdated("ServicoFluxo") ;
               pr_default.readNext(0);
            }
            if ( pr_default.getBatchSize(1) > 0 )
            {
               Executebatchp00b33( ) ;
            }
            pr_default.close(0);
            /* Using cursor P00B34 */
            pr_default.execute(2, new Object[] {AV10Servico});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1532ServicoFluxo_Ordem = P00B34_A1532ServicoFluxo_Ordem[0];
               n1532ServicoFluxo_Ordem = P00B34_n1532ServicoFluxo_Ordem[0];
               A1522ServicoFluxo_ServicoCod = P00B34_A1522ServicoFluxo_ServicoCod[0];
               A1528ServicoFluxo_Codigo = P00B34_A1528ServicoFluxo_Codigo[0];
               A1532ServicoFluxo_Ordem = AV15Novo;
               n1532ServicoFluxo_Ordem = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00B35 */
               pr_default.execute(3, new Object[] {n1532ServicoFluxo_Ordem, A1532ServicoFluxo_Ordem, A1528ServicoFluxo_Codigo});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ServicoFluxo") ;
               if (true) break;
               /* Using cursor P00B36 */
               pr_default.execute(4, new Object[] {n1532ServicoFluxo_Ordem, A1532ServicoFluxo_Ordem, A1528ServicoFluxo_Codigo});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ServicoFluxo") ;
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         this.cleanup();
      }

      protected void Executebatchp00b33( )
      {
         /* Using cursor P00B33 */
         pr_default.executeBatch(1);
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ReordenaFluxo");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00B32_A1532ServicoFluxo_Ordem = new short[1] ;
         P00B32_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         P00B32_A1522ServicoFluxo_ServicoCod = new int[1] ;
         P00B32_A1528ServicoFluxo_Codigo = new int[1] ;
         P00B33_A1532ServicoFluxo_Ordem = new short[1] ;
         P00B33_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         P00B33_A1528ServicoFluxo_Codigo = new int[1] ;
         P00B34_A1532ServicoFluxo_Ordem = new short[1] ;
         P00B34_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         P00B34_A1522ServicoFluxo_ServicoCod = new int[1] ;
         P00B34_A1528ServicoFluxo_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_reordenafluxo__default(),
            new Object[][] {
                new Object[] {
               P00B32_A1532ServicoFluxo_Ordem, P00B32_n1532ServicoFluxo_Ordem, P00B32_A1522ServicoFluxo_ServicoCod, P00B32_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P00B34_A1532ServicoFluxo_Ordem, P00B34_n1532ServicoFluxo_Ordem, P00B34_A1522ServicoFluxo_ServicoCod, P00B34_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9Desde ;
      private short AV14Ate ;
      private short AV15Novo ;
      private short A1532ServicoFluxo_Ordem ;
      private int AV10Servico ;
      private int A1522ServicoFluxo_ServicoCod ;
      private int A1528ServicoFluxo_Codigo ;
      private int BatchSize ;
      private decimal AV13Valor ;
      private String AV8Tabela ;
      private String AV12Direcao ;
      private String scmdbuf ;
      private bool n1532ServicoFluxo_Ordem ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P00B32_A1532ServicoFluxo_Ordem ;
      private bool[] P00B32_n1532ServicoFluxo_Ordem ;
      private int[] P00B32_A1522ServicoFluxo_ServicoCod ;
      private int[] P00B32_A1528ServicoFluxo_Codigo ;
      private short[] P00B33_A1532ServicoFluxo_Ordem ;
      private bool[] P00B33_n1532ServicoFluxo_Ordem ;
      private int[] P00B33_A1528ServicoFluxo_Codigo ;
      private short[] P00B34_A1532ServicoFluxo_Ordem ;
      private bool[] P00B34_n1532ServicoFluxo_Ordem ;
      private int[] P00B34_A1522ServicoFluxo_ServicoCod ;
      private int[] P00B34_A1528ServicoFluxo_Codigo ;
   }

   public class prc_reordenafluxo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new BatchUpdateCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00B32 ;
          prmP00B32 = new Object[] {
          new Object[] {"@AV10Servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Desde",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV14Ate",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00B33 ;
          prmP00B33 = new Object[] {
          new Object[] {"@ServicoFluxo_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B34 ;
          prmP00B34 = new Object[] {
          new Object[] {"@AV10Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B35 ;
          prmP00B35 = new Object[] {
          new Object[] {"@ServicoFluxo_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B36 ;
          prmP00B36 = new Object[] {
          new Object[] {"@ServicoFluxo_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00B32", "SELECT [ServicoFluxo_Ordem], [ServicoFluxo_ServicoCod], [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (UPDLOCK) WHERE ([ServicoFluxo_ServicoCod] = @AV10Servico and [ServicoFluxo_Ordem] >= @AV9Desde) AND ([ServicoFluxo_Ordem] <= @AV14Ate) ORDER BY [ServicoFluxo_ServicoCod], [ServicoFluxo_Ordem] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B32,1,0,true,false )
             ,new CursorDef("P00B33", "UPDATE [ServicoFluxo] SET [ServicoFluxo_Ordem]=@ServicoFluxo_Ordem  WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B33)
             ,new CursorDef("P00B34", "SELECT TOP 1 [ServicoFluxo_Ordem], [ServicoFluxo_ServicoCod], [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (UPDLOCK) WHERE [ServicoFluxo_ServicoCod] = @AV10Servico and [ServicoFluxo_Ordem] = 0 ORDER BY [ServicoFluxo_ServicoCod], [ServicoFluxo_Ordem] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B34,1,0,true,true )
             ,new CursorDef("P00B35", "UPDATE [ServicoFluxo] SET [ServicoFluxo_Ordem]=@ServicoFluxo_Ordem  WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B35)
             ,new CursorDef("P00B36", "UPDATE [ServicoFluxo] SET [ServicoFluxo_Ordem]=@ServicoFluxo_Ordem  WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B36)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
