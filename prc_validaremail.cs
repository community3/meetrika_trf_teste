/*
               File: PRC_ValidarEmail
        Description: PRC_Validar Email
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:38.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_validaremail : GXProcedure
   {
      public prc_validaremail( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_validaremail( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_pEmail ,
                           out bool aP1_Erro )
      {
         this.AV11pEmail = aP0_pEmail;
         this.AV9Erro = false ;
         initialize();
         executePrivate();
         aP1_Erro=this.AV9Erro;
      }

      public bool executeUdp( String aP0_pEmail )
      {
         this.AV11pEmail = aP0_pEmail;
         this.AV9Erro = false ;
         initialize();
         executePrivate();
         aP1_Erro=this.AV9Erro;
         return AV9Erro ;
      }

      public void executeSubmit( String aP0_pEmail ,
                                 out bool aP1_Erro )
      {
         prc_validaremail objprc_validaremail;
         objprc_validaremail = new prc_validaremail();
         objprc_validaremail.AV11pEmail = aP0_pEmail;
         objprc_validaremail.AV9Erro = false ;
         objprc_validaremail.context.SetSubmitInitialConfig(context);
         objprc_validaremail.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_validaremail);
         aP1_Erro=this.AV9Erro;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_validaremail)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10Flag = GxRegex.IsMatch(AV8Email,"\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
         if ( AV10Flag )
         {
            AV9Erro = false;
         }
         else
         {
            AV9Erro = true;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Email = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private bool AV9Erro ;
      private bool AV10Flag ;
      private String AV11pEmail ;
      private String AV8Email ;
      private bool aP1_Erro ;
   }

}
