/*
               File: WWFuncaoAPFEvidencia
        Description:  Evidencia da Funcao de Transa��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:1:58.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwfuncaoapfevidencia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwfuncaoapfevidencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwfuncaoapfevidencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_88 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_88_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_88_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17FuncaoAPFEvidencia_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPFEvidencia_Descricao1", AV17FuncaoAPFEvidencia_Descricao1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21FuncaoAPFEvidencia_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPFEvidencia_Descricao2", AV21FuncaoAPFEvidencia_Descricao2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25FuncaoAPFEvidencia_Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPFEvidencia_Descricao3", AV25FuncaoAPFEvidencia_Descricao3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV34TFFuncaoAPFEvidencia_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFFuncaoAPFEvidencia_Descricao", AV34TFFuncaoAPFEvidencia_Descricao);
               AV35TFFuncaoAPFEvidencia_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFFuncaoAPFEvidencia_Descricao_Sel", AV35TFFuncaoAPFEvidencia_Descricao_Sel);
               AV38TFFuncaoAPFEvidencia_NomeArq = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFuncaoAPFEvidencia_NomeArq", AV38TFFuncaoAPFEvidencia_NomeArq);
               AV39TFFuncaoAPFEvidencia_NomeArq_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoAPFEvidencia_NomeArq_Sel", AV39TFFuncaoAPFEvidencia_NomeArq_Sel);
               AV42TFFuncaoAPFEvidencia_TipoArq = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFFuncaoAPFEvidencia_TipoArq", AV42TFFuncaoAPFEvidencia_TipoArq);
               AV43TFFuncaoAPFEvidencia_TipoArq_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFuncaoAPFEvidencia_TipoArq_Sel", AV43TFFuncaoAPFEvidencia_TipoArq_Sel);
               AV46TFFuncaoAPFEvidencia_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFuncaoAPFEvidencia_Data", context.localUtil.TToC( AV46TFFuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
               AV47TFFuncaoAPFEvidencia_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFEvidencia_Data_To", context.localUtil.TToC( AV47TFFuncaoAPFEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace", AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace);
               AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace", AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace);
               AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace", AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace);
               AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace", AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace);
               AV78Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A406FuncaoAPFEvidencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A360FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n360FuncaoAPF_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPFEvidencia_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPFEvidencia_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPFEvidencia_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFFuncaoAPFEvidencia_Descricao, AV35TFFuncaoAPFEvidencia_Descricao_Sel, AV38TFFuncaoAPFEvidencia_NomeArq, AV39TFFuncaoAPFEvidencia_NomeArq_Sel, AV42TFFuncaoAPFEvidencia_TipoArq, AV43TFFuncaoAPFEvidencia_TipoArq_Sel, AV46TFFuncaoAPFEvidencia_Data, AV47TFFuncaoAPFEvidencia_Data_To, AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAAD2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTAD2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205181315881");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwfuncaoapfevidencia.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPFEVIDENCIA_DESCRICAO1", AV17FuncaoAPFEvidencia_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPFEVIDENCIA_DESCRICAO2", AV21FuncaoAPFEvidencia_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPFEVIDENCIA_DESCRICAO3", AV25FuncaoAPFEvidencia_Descricao3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFEVIDENCIA_DESCRICAO", AV34TFFuncaoAPFEvidencia_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL", AV35TFFuncaoAPFEvidencia_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ", StringUtil.RTrim( AV38TFFuncaoAPFEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL", StringUtil.RTrim( AV39TFFuncaoAPFEvidencia_NomeArq_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFEVIDENCIA_TIPOARQ", StringUtil.RTrim( AV42TFFuncaoAPFEvidencia_TipoArq));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL", StringUtil.RTrim( AV43TFFuncaoAPFEvidencia_TipoArq_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFEVIDENCIA_DATA", context.localUtil.TToC( AV46TFFuncaoAPFEvidencia_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFEVIDENCIA_DATA_TO", context.localUtil.TToC( AV47TFFuncaoAPFEvidencia_Data_To, 10, 8, 0, 3, "/", ":", " "));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_88", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_88), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV51DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV51DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPFEVIDENCIA_DESCRICAOTITLEFILTERDATA", AV33FuncaoAPFEvidencia_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPFEVIDENCIA_DESCRICAOTITLEFILTERDATA", AV33FuncaoAPFEvidencia_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA", AV37FuncaoAPFEvidencia_NomeArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA", AV37FuncaoAPFEvidencia_NomeArqTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPFEVIDENCIA_TIPOARQTITLEFILTERDATA", AV41FuncaoAPFEvidencia_TipoArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPFEVIDENCIA_TIPOARQTITLEFILTERDATA", AV41FuncaoAPFEvidencia_TipoArqTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPFEVIDENCIA_DATATITLEFILTERDATA", AV45FuncaoAPFEvidencia_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPFEVIDENCIA_DATATITLEFILTERDATA", AV45FuncaoAPFEvidencia_DataTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV78Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Caption", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Cls", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapfevidencia_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Caption", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Tooltip", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Cls", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filtertype", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalisttype", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalistproc", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapfevidencia_nomearq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Sortasc", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Loadingdata", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Caption", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Tooltip", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Cls", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_tipoarq_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_tipoarq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_tipoarq_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filtertype", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_tipoarq_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_tipoarq_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Datalisttype", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Datalistproc", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapfevidencia_tipoarq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Sortasc", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Loadingdata", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Caption", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Tooltip", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Cls", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Filtertype", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Sortasc", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFEVIDENCIA_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_data_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEAD2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTAD2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwfuncaoapfevidencia.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWFuncaoAPFEvidencia" ;
      }

      public override String GetPgmdesc( )
      {
         return " Evidencia da Funcao de Transa��o" ;
      }

      protected void WBAD0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_AD2( true) ;
         }
         else
         {
            wb_table1_2_AD2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AD2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(101, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(102, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapfevidencia_descricao_Internalname, AV34TFFuncaoAPFEvidencia_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", 0, edtavTffuncaoapfevidencia_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoAPFEvidencia.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapfevidencia_descricao_sel_Internalname, AV35TFFuncaoAPFEvidencia_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", 0, edtavTffuncaoapfevidencia_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoAPFEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfevidencia_nomearq_Internalname, StringUtil.RTrim( AV38TFFuncaoAPFEvidencia_NomeArq), StringUtil.RTrim( context.localUtil.Format( AV38TFFuncaoAPFEvidencia_NomeArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfevidencia_nomearq_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfevidencia_nomearq_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWFuncaoAPFEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfevidencia_nomearq_sel_Internalname, StringUtil.RTrim( AV39TFFuncaoAPFEvidencia_NomeArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV39TFFuncaoAPFEvidencia_NomeArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfevidencia_nomearq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfevidencia_nomearq_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWFuncaoAPFEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfevidencia_tipoarq_Internalname, StringUtil.RTrim( AV42TFFuncaoAPFEvidencia_TipoArq), StringUtil.RTrim( context.localUtil.Format( AV42TFFuncaoAPFEvidencia_TipoArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfevidencia_tipoarq_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfevidencia_tipoarq_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWFuncaoAPFEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfevidencia_tipoarq_sel_Internalname, StringUtil.RTrim( AV43TFFuncaoAPFEvidencia_TipoArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV43TFFuncaoAPFEvidencia_TipoArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfevidencia_tipoarq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfevidencia_tipoarq_sel_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWFuncaoAPFEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_88_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTffuncaoapfevidencia_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfevidencia_data_Internalname, context.localUtil.TToC( AV46TFFuncaoAPFEvidencia_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV46TFFuncaoAPFEvidencia_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfevidencia_data_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfevidencia_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoAPFEvidencia.htm");
            GxWebStd.gx_bitmap( context, edtavTffuncaoapfevidencia_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTffuncaoapfevidencia_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_88_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTffuncaoapfevidencia_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfevidencia_data_to_Internalname, context.localUtil.TToC( AV47TFFuncaoAPFEvidencia_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV47TFFuncaoAPFEvidencia_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfevidencia_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfevidencia_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoAPFEvidencia.htm");
            GxWebStd.gx_bitmap( context, edtavTffuncaoapfevidencia_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTffuncaoapfevidencia_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_funcaoapfevidencia_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_88_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_funcaoapfevidencia_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_funcaoapfevidencia_dataauxdate_Internalname, context.localUtil.Format(AV48DDO_FuncaoAPFEvidencia_DataAuxDate, "99/99/99"), context.localUtil.Format( AV48DDO_FuncaoAPFEvidencia_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_funcaoapfevidencia_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoAPFEvidencia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_funcaoapfevidencia_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_88_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_funcaoapfevidencia_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_funcaoapfevidencia_dataauxdateto_Internalname, context.localUtil.Format(AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_funcaoapfevidencia_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoAPFEvidencia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_funcaoapfevidencia_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPFEVIDENCIA_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Internalname, AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoAPFEvidencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname, AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", 0, edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoAPFEvidencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPFEVIDENCIA_TIPOARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Internalname, AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoAPFEvidencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPFEVIDENCIA_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfevidencia_datatitlecontrolidtoreplace_Internalname, AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, edtavDdo_funcaoapfevidencia_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoAPFEvidencia.htm");
         }
         wbLoad = true;
      }

      protected void STARTAD2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Evidencia da Funcao de Transa��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPAD0( ) ;
      }

      protected void WSAD2( )
      {
         STARTAD2( ) ;
         EVTAD2( ) ;
      }

      protected void EVTAD2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11AD2 */
                              E11AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12AD2 */
                              E12AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13AD2 */
                              E13AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14AD2 */
                              E14AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFEVIDENCIA_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15AD2 */
                              E15AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16AD2 */
                              E16AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17AD2 */
                              E17AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18AD2 */
                              E18AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19AD2 */
                              E19AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20AD2 */
                              E20AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21AD2 */
                              E21AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22AD2 */
                              E22AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23AD2 */
                              E23AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24AD2 */
                              E24AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25AD2 */
                              E25AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26AD2 */
                              E26AD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_88_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
                              SubsflControlProps_882( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV76Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV77Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A406FuncaoAPFEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFEvidencia_Codigo_Internalname), ",", "."));
                              A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                              A407FuncaoAPFEvidencia_Descricao = cgiGet( edtFuncaoAPFEvidencia_Descricao_Internalname);
                              n407FuncaoAPFEvidencia_Descricao = false;
                              A409FuncaoAPFEvidencia_NomeArq = cgiGet( edtFuncaoAPFEvidencia_NomeArq_Internalname);
                              n409FuncaoAPFEvidencia_NomeArq = false;
                              A410FuncaoAPFEvidencia_TipoArq = cgiGet( edtFuncaoAPFEvidencia_TipoArq_Internalname);
                              n410FuncaoAPFEvidencia_TipoArq = false;
                              A411FuncaoAPFEvidencia_Data = context.localUtil.CToT( cgiGet( edtFuncaoAPFEvidencia_Data_Internalname), 0);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27AD2 */
                                    E27AD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28AD2 */
                                    E28AD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29AD2 */
                                    E29AD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapfevidencia_descricao1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPFEVIDENCIA_DESCRICAO1"), AV17FuncaoAPFEvidencia_Descricao1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapfevidencia_descricao2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPFEVIDENCIA_DESCRICAO2"), AV21FuncaoAPFEvidencia_Descricao2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaoapfevidencia_descricao3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPFEVIDENCIA_DESCRICAO3"), AV25FuncaoAPFEvidencia_Descricao3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaoapfevidencia_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_DESCRICAO"), AV34TFFuncaoAPFEvidencia_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaoapfevidencia_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL"), AV35TFFuncaoAPFEvidencia_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaoapfevidencia_nomearq Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ"), AV38TFFuncaoAPFEvidencia_NomeArq) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaoapfevidencia_nomearq_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL"), AV39TFFuncaoAPFEvidencia_NomeArq_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaoapfevidencia_tipoarq Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_TIPOARQ"), AV42TFFuncaoAPFEvidencia_TipoArq) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaoapfevidencia_tipoarq_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL"), AV43TFFuncaoAPFEvidencia_TipoArq_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaoapfevidencia_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_DATA"), 0) != AV46TFFuncaoAPFEvidencia_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaoapfevidencia_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_DATA_TO"), 0) != AV47TFFuncaoAPFEvidencia_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEAD2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAAD2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPFEVIDENCIA_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPFEVIDENCIA_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPFEVIDENCIA_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_882( ) ;
         while ( nGXsfl_88_idx <= nRC_GXsfl_88 )
         {
            sendrow_882( ) ;
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17FuncaoAPFEvidencia_Descricao1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21FuncaoAPFEvidencia_Descricao2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25FuncaoAPFEvidencia_Descricao3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV34TFFuncaoAPFEvidencia_Descricao ,
                                       String AV35TFFuncaoAPFEvidencia_Descricao_Sel ,
                                       String AV38TFFuncaoAPFEvidencia_NomeArq ,
                                       String AV39TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                       String AV42TFFuncaoAPFEvidencia_TipoArq ,
                                       String AV43TFFuncaoAPFEvidencia_TipoArq_Sel ,
                                       DateTime AV46TFFuncaoAPFEvidencia_Data ,
                                       DateTime AV47TFFuncaoAPFEvidencia_Data_To ,
                                       String AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace ,
                                       String AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace ,
                                       String AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace ,
                                       String AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace ,
                                       String AV78Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A406FuncaoAPFEvidencia_Codigo ,
                                       int A165FuncaoAPF_Codigo ,
                                       int A360FuncaoAPF_SistemaCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFAD2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFEVIDENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A406FuncaoAPFEvidencia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFEVIDENCIA_DESCRICAO", GetSecureSignedToken( "", A407FuncaoAPFEvidencia_Descricao));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFEVIDENCIA_DESCRICAO", A407FuncaoAPFEvidencia_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFEVIDENCIA_NOMEARQ", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A409FuncaoAPFEvidencia_NomeArq, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFEVIDENCIA_NOMEARQ", StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFEVIDENCIA_TIPOARQ", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A410FuncaoAPFEvidencia_TipoArq, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFEVIDENCIA_TIPOARQ", StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFEVIDENCIA_DATA", GetSecureSignedToken( "", context.localUtil.Format( A411FuncaoAPFEvidencia_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFEVIDENCIA_DATA", context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 10, 8, 0, 3, "/", ":", " "));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAD2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV78Pgmname = "WWFuncaoAPFEvidencia";
         context.Gx_err = 0;
      }

      protected void RFAD2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 88;
         /* Execute user event: E28AD2 */
         E28AD2 ();
         nGXsfl_88_idx = 1;
         sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
         SubsflControlProps_882( ) ;
         nGXsfl_88_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_882( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 ,
                                                 AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 ,
                                                 AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ,
                                                 AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 ,
                                                 AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 ,
                                                 AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 ,
                                                 AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ,
                                                 AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 ,
                                                 AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 ,
                                                 AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 ,
                                                 AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ,
                                                 AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel ,
                                                 AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ,
                                                 AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel ,
                                                 AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ,
                                                 AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel ,
                                                 AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ,
                                                 AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data ,
                                                 AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to ,
                                                 A407FuncaoAPFEvidencia_Descricao ,
                                                 A409FuncaoAPFEvidencia_NomeArq ,
                                                 A410FuncaoAPFEvidencia_TipoArq ,
                                                 A411FuncaoAPFEvidencia_Data ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1), "%", "");
            lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1), "%", "");
            lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2), "%", "");
            lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2), "%", "");
            lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3), "%", "");
            lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3), "%", "");
            lV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = StringUtil.Concat( StringUtil.RTrim( AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao), "%", "");
            lV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq), 50, "%");
            lV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq), 10, "%");
            /* Using cursor H00AD2 */
            pr_default.execute(0, new Object[] {lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1, lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1, lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2, lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2, lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3, lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3, lV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao, AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel, lV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq, AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel, lV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq, AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel, AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data, AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_88_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A360FuncaoAPF_SistemaCod = H00AD2_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H00AD2_n360FuncaoAPF_SistemaCod[0];
               A411FuncaoAPFEvidencia_Data = H00AD2_A411FuncaoAPFEvidencia_Data[0];
               A410FuncaoAPFEvidencia_TipoArq = H00AD2_A410FuncaoAPFEvidencia_TipoArq[0];
               n410FuncaoAPFEvidencia_TipoArq = H00AD2_n410FuncaoAPFEvidencia_TipoArq[0];
               A409FuncaoAPFEvidencia_NomeArq = H00AD2_A409FuncaoAPFEvidencia_NomeArq[0];
               n409FuncaoAPFEvidencia_NomeArq = H00AD2_n409FuncaoAPFEvidencia_NomeArq[0];
               A407FuncaoAPFEvidencia_Descricao = H00AD2_A407FuncaoAPFEvidencia_Descricao[0];
               n407FuncaoAPFEvidencia_Descricao = H00AD2_n407FuncaoAPFEvidencia_Descricao[0];
               A165FuncaoAPF_Codigo = H00AD2_A165FuncaoAPF_Codigo[0];
               A406FuncaoAPFEvidencia_Codigo = H00AD2_A406FuncaoAPFEvidencia_Codigo[0];
               A360FuncaoAPF_SistemaCod = H00AD2_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H00AD2_n360FuncaoAPF_SistemaCod[0];
               /* Execute user event: E29AD2 */
               E29AD2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 88;
            WBAD0( ) ;
         }
         nGXsfl_88_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = AV17FuncaoAPFEvidencia_Descricao1;
         AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = AV21FuncaoAPFEvidencia_Descricao2;
         AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = AV25FuncaoAPFEvidencia_Descricao3;
         AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = AV34TFFuncaoAPFEvidencia_Descricao;
         AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel = AV35TFFuncaoAPFEvidencia_Descricao_Sel;
         AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = AV38TFFuncaoAPFEvidencia_NomeArq;
         AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel = AV39TFFuncaoAPFEvidencia_NomeArq_Sel;
         AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = AV42TFFuncaoAPFEvidencia_TipoArq;
         AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel = AV43TFFuncaoAPFEvidencia_TipoArq_Sel;
         AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data = AV46TFFuncaoAPFEvidencia_Data;
         AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to = AV47TFFuncaoAPFEvidencia_Data_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 ,
                                              AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ,
                                              AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 ,
                                              AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 ,
                                              AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 ,
                                              AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ,
                                              AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 ,
                                              AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 ,
                                              AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 ,
                                              AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ,
                                              AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel ,
                                              AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ,
                                              AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel ,
                                              AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ,
                                              AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel ,
                                              AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ,
                                              AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data ,
                                              AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to ,
                                              A407FuncaoAPFEvidencia_Descricao ,
                                              A409FuncaoAPFEvidencia_NomeArq ,
                                              A410FuncaoAPFEvidencia_TipoArq ,
                                              A411FuncaoAPFEvidencia_Data ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1), "%", "");
         lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1), "%", "");
         lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2), "%", "");
         lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2), "%", "");
         lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3), "%", "");
         lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3), "%", "");
         lV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = StringUtil.Concat( StringUtil.RTrim( AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao), "%", "");
         lV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = StringUtil.PadR( StringUtil.RTrim( AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq), 50, "%");
         lV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = StringUtil.PadR( StringUtil.RTrim( AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq), 10, "%");
         /* Using cursor H00AD3 */
         pr_default.execute(1, new Object[] {lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1, lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1, lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2, lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2, lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3, lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3, lV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao, AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel, lV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq, AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel, lV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq, AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel, AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data, AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to});
         GRID_nRecordCount = H00AD3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = AV17FuncaoAPFEvidencia_Descricao1;
         AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = AV21FuncaoAPFEvidencia_Descricao2;
         AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = AV25FuncaoAPFEvidencia_Descricao3;
         AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = AV34TFFuncaoAPFEvidencia_Descricao;
         AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel = AV35TFFuncaoAPFEvidencia_Descricao_Sel;
         AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = AV38TFFuncaoAPFEvidencia_NomeArq;
         AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel = AV39TFFuncaoAPFEvidencia_NomeArq_Sel;
         AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = AV42TFFuncaoAPFEvidencia_TipoArq;
         AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel = AV43TFFuncaoAPFEvidencia_TipoArq_Sel;
         AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data = AV46TFFuncaoAPFEvidencia_Data;
         AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to = AV47TFFuncaoAPFEvidencia_Data_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPFEvidencia_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPFEvidencia_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPFEvidencia_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFFuncaoAPFEvidencia_Descricao, AV35TFFuncaoAPFEvidencia_Descricao_Sel, AV38TFFuncaoAPFEvidencia_NomeArq, AV39TFFuncaoAPFEvidencia_NomeArq_Sel, AV42TFFuncaoAPFEvidencia_TipoArq, AV43TFFuncaoAPFEvidencia_TipoArq_Sel, AV46TFFuncaoAPFEvidencia_Data, AV47TFFuncaoAPFEvidencia_Data_To, AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = AV17FuncaoAPFEvidencia_Descricao1;
         AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = AV21FuncaoAPFEvidencia_Descricao2;
         AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = AV25FuncaoAPFEvidencia_Descricao3;
         AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = AV34TFFuncaoAPFEvidencia_Descricao;
         AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel = AV35TFFuncaoAPFEvidencia_Descricao_Sel;
         AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = AV38TFFuncaoAPFEvidencia_NomeArq;
         AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel = AV39TFFuncaoAPFEvidencia_NomeArq_Sel;
         AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = AV42TFFuncaoAPFEvidencia_TipoArq;
         AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel = AV43TFFuncaoAPFEvidencia_TipoArq_Sel;
         AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data = AV46TFFuncaoAPFEvidencia_Data;
         AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to = AV47TFFuncaoAPFEvidencia_Data_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPFEvidencia_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPFEvidencia_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPFEvidencia_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFFuncaoAPFEvidencia_Descricao, AV35TFFuncaoAPFEvidencia_Descricao_Sel, AV38TFFuncaoAPFEvidencia_NomeArq, AV39TFFuncaoAPFEvidencia_NomeArq_Sel, AV42TFFuncaoAPFEvidencia_TipoArq, AV43TFFuncaoAPFEvidencia_TipoArq_Sel, AV46TFFuncaoAPFEvidencia_Data, AV47TFFuncaoAPFEvidencia_Data_To, AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = AV17FuncaoAPFEvidencia_Descricao1;
         AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = AV21FuncaoAPFEvidencia_Descricao2;
         AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = AV25FuncaoAPFEvidencia_Descricao3;
         AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = AV34TFFuncaoAPFEvidencia_Descricao;
         AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel = AV35TFFuncaoAPFEvidencia_Descricao_Sel;
         AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = AV38TFFuncaoAPFEvidencia_NomeArq;
         AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel = AV39TFFuncaoAPFEvidencia_NomeArq_Sel;
         AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = AV42TFFuncaoAPFEvidencia_TipoArq;
         AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel = AV43TFFuncaoAPFEvidencia_TipoArq_Sel;
         AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data = AV46TFFuncaoAPFEvidencia_Data;
         AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to = AV47TFFuncaoAPFEvidencia_Data_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPFEvidencia_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPFEvidencia_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPFEvidencia_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFFuncaoAPFEvidencia_Descricao, AV35TFFuncaoAPFEvidencia_Descricao_Sel, AV38TFFuncaoAPFEvidencia_NomeArq, AV39TFFuncaoAPFEvidencia_NomeArq_Sel, AV42TFFuncaoAPFEvidencia_TipoArq, AV43TFFuncaoAPFEvidencia_TipoArq_Sel, AV46TFFuncaoAPFEvidencia_Data, AV47TFFuncaoAPFEvidencia_Data_To, AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = AV17FuncaoAPFEvidencia_Descricao1;
         AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = AV21FuncaoAPFEvidencia_Descricao2;
         AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = AV25FuncaoAPFEvidencia_Descricao3;
         AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = AV34TFFuncaoAPFEvidencia_Descricao;
         AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel = AV35TFFuncaoAPFEvidencia_Descricao_Sel;
         AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = AV38TFFuncaoAPFEvidencia_NomeArq;
         AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel = AV39TFFuncaoAPFEvidencia_NomeArq_Sel;
         AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = AV42TFFuncaoAPFEvidencia_TipoArq;
         AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel = AV43TFFuncaoAPFEvidencia_TipoArq_Sel;
         AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data = AV46TFFuncaoAPFEvidencia_Data;
         AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to = AV47TFFuncaoAPFEvidencia_Data_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPFEvidencia_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPFEvidencia_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPFEvidencia_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFFuncaoAPFEvidencia_Descricao, AV35TFFuncaoAPFEvidencia_Descricao_Sel, AV38TFFuncaoAPFEvidencia_NomeArq, AV39TFFuncaoAPFEvidencia_NomeArq_Sel, AV42TFFuncaoAPFEvidencia_TipoArq, AV43TFFuncaoAPFEvidencia_TipoArq_Sel, AV46TFFuncaoAPFEvidencia_Data, AV47TFFuncaoAPFEvidencia_Data_To, AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = AV17FuncaoAPFEvidencia_Descricao1;
         AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = AV21FuncaoAPFEvidencia_Descricao2;
         AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = AV25FuncaoAPFEvidencia_Descricao3;
         AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = AV34TFFuncaoAPFEvidencia_Descricao;
         AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel = AV35TFFuncaoAPFEvidencia_Descricao_Sel;
         AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = AV38TFFuncaoAPFEvidencia_NomeArq;
         AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel = AV39TFFuncaoAPFEvidencia_NomeArq_Sel;
         AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = AV42TFFuncaoAPFEvidencia_TipoArq;
         AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel = AV43TFFuncaoAPFEvidencia_TipoArq_Sel;
         AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data = AV46TFFuncaoAPFEvidencia_Data;
         AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to = AV47TFFuncaoAPFEvidencia_Data_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPFEvidencia_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPFEvidencia_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPFEvidencia_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFFuncaoAPFEvidencia_Descricao, AV35TFFuncaoAPFEvidencia_Descricao_Sel, AV38TFFuncaoAPFEvidencia_NomeArq, AV39TFFuncaoAPFEvidencia_NomeArq_Sel, AV42TFFuncaoAPFEvidencia_TipoArq, AV43TFFuncaoAPFEvidencia_TipoArq_Sel, AV46TFFuncaoAPFEvidencia_Data, AV47TFFuncaoAPFEvidencia_Data_To, AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUPAD0( )
      {
         /* Before Start, stand alone formulas. */
         AV78Pgmname = "WWFuncaoAPFEvidencia";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E27AD2 */
         E27AD2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV51DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPFEVIDENCIA_DESCRICAOTITLEFILTERDATA"), AV33FuncaoAPFEvidencia_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA"), AV37FuncaoAPFEvidencia_NomeArqTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPFEVIDENCIA_TIPOARQTITLEFILTERDATA"), AV41FuncaoAPFEvidencia_TipoArqTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPFEVIDENCIA_DATATITLEFILTERDATA"), AV45FuncaoAPFEvidencia_DataTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17FuncaoAPFEvidencia_Descricao1 = cgiGet( edtavFuncaoapfevidencia_descricao1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPFEvidencia_Descricao1", AV17FuncaoAPFEvidencia_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21FuncaoAPFEvidencia_Descricao2 = cgiGet( edtavFuncaoapfevidencia_descricao2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPFEvidencia_Descricao2", AV21FuncaoAPFEvidencia_Descricao2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25FuncaoAPFEvidencia_Descricao3 = cgiGet( edtavFuncaoapfevidencia_descricao3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPFEvidencia_Descricao3", AV25FuncaoAPFEvidencia_Descricao3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV34TFFuncaoAPFEvidencia_Descricao = cgiGet( edtavTffuncaoapfevidencia_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFFuncaoAPFEvidencia_Descricao", AV34TFFuncaoAPFEvidencia_Descricao);
            AV35TFFuncaoAPFEvidencia_Descricao_Sel = cgiGet( edtavTffuncaoapfevidencia_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFFuncaoAPFEvidencia_Descricao_Sel", AV35TFFuncaoAPFEvidencia_Descricao_Sel);
            AV38TFFuncaoAPFEvidencia_NomeArq = cgiGet( edtavTffuncaoapfevidencia_nomearq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFuncaoAPFEvidencia_NomeArq", AV38TFFuncaoAPFEvidencia_NomeArq);
            AV39TFFuncaoAPFEvidencia_NomeArq_Sel = cgiGet( edtavTffuncaoapfevidencia_nomearq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoAPFEvidencia_NomeArq_Sel", AV39TFFuncaoAPFEvidencia_NomeArq_Sel);
            AV42TFFuncaoAPFEvidencia_TipoArq = cgiGet( edtavTffuncaoapfevidencia_tipoarq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFFuncaoAPFEvidencia_TipoArq", AV42TFFuncaoAPFEvidencia_TipoArq);
            AV43TFFuncaoAPFEvidencia_TipoArq_Sel = cgiGet( edtavTffuncaoapfevidencia_tipoarq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFuncaoAPFEvidencia_TipoArq_Sel", AV43TFFuncaoAPFEvidencia_TipoArq_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTffuncaoapfevidencia_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFFuncao APFEvidencia_Data"}), 1, "vTFFUNCAOAPFEVIDENCIA_DATA");
               GX_FocusControl = edtavTffuncaoapfevidencia_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFFuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFuncaoAPFEvidencia_Data", context.localUtil.TToC( AV46TFFuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV46TFFuncaoAPFEvidencia_Data = context.localUtil.CToT( cgiGet( edtavTffuncaoapfevidencia_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFuncaoAPFEvidencia_Data", context.localUtil.TToC( AV46TFFuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTffuncaoapfevidencia_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFFuncao APFEvidencia_Data_To"}), 1, "vTFFUNCAOAPFEVIDENCIA_DATA_TO");
               GX_FocusControl = edtavTffuncaoapfevidencia_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFFuncaoAPFEvidencia_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFEvidencia_Data_To", context.localUtil.TToC( AV47TFFuncaoAPFEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV47TFFuncaoAPFEvidencia_Data_To = context.localUtil.CToT( cgiGet( edtavTffuncaoapfevidencia_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFEvidencia_Data_To", context.localUtil.TToC( AV47TFFuncaoAPFEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_funcaoapfevidencia_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Funcao APFEvidencia_Data Aux Date"}), 1, "vDDO_FUNCAOAPFEVIDENCIA_DATAAUXDATE");
               GX_FocusControl = edtavDdo_funcaoapfevidencia_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48DDO_FuncaoAPFEvidencia_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DDO_FuncaoAPFEvidencia_DataAuxDate", context.localUtil.Format(AV48DDO_FuncaoAPFEvidencia_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV48DDO_FuncaoAPFEvidencia_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_funcaoapfevidencia_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DDO_FuncaoAPFEvidencia_DataAuxDate", context.localUtil.Format(AV48DDO_FuncaoAPFEvidencia_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_funcaoapfevidencia_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Funcao APFEvidencia_Data Aux Date To"}), 1, "vDDO_FUNCAOAPFEVIDENCIA_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_funcaoapfevidencia_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo", context.localUtil.Format(AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_funcaoapfevidencia_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo", context.localUtil.Format(AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo, "99/99/99"));
            }
            AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace", AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace);
            AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace", AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace);
            AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace", AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace);
            AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfevidencia_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace", AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_88 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_88"), ",", "."));
            AV53GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV54GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_funcaoapfevidencia_descricao_Caption = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Caption");
            Ddo_funcaoapfevidencia_descricao_Tooltip = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Tooltip");
            Ddo_funcaoapfevidencia_descricao_Cls = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Cls");
            Ddo_funcaoapfevidencia_descricao_Filteredtext_set = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filteredtext_set");
            Ddo_funcaoapfevidencia_descricao_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Selectedvalue_set");
            Ddo_funcaoapfevidencia_descricao_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Dropdownoptionstype");
            Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_funcaoapfevidencia_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includesortasc"));
            Ddo_funcaoapfevidencia_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includesortdsc"));
            Ddo_funcaoapfevidencia_descricao_Sortedstatus = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Sortedstatus");
            Ddo_funcaoapfevidencia_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includefilter"));
            Ddo_funcaoapfevidencia_descricao_Filtertype = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filtertype");
            Ddo_funcaoapfevidencia_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filterisrange"));
            Ddo_funcaoapfevidencia_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Includedatalist"));
            Ddo_funcaoapfevidencia_descricao_Datalisttype = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Datalisttype");
            Ddo_funcaoapfevidencia_descricao_Datalistproc = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Datalistproc");
            Ddo_funcaoapfevidencia_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapfevidencia_descricao_Sortasc = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Sortasc");
            Ddo_funcaoapfevidencia_descricao_Sortdsc = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Sortdsc");
            Ddo_funcaoapfevidencia_descricao_Loadingdata = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Loadingdata");
            Ddo_funcaoapfevidencia_descricao_Cleanfilter = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Cleanfilter");
            Ddo_funcaoapfevidencia_descricao_Noresultsfound = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Noresultsfound");
            Ddo_funcaoapfevidencia_descricao_Searchbuttontext = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Searchbuttontext");
            Ddo_funcaoapfevidencia_nomearq_Caption = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Caption");
            Ddo_funcaoapfevidencia_nomearq_Tooltip = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Tooltip");
            Ddo_funcaoapfevidencia_nomearq_Cls = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Cls");
            Ddo_funcaoapfevidencia_nomearq_Filteredtext_set = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filteredtext_set");
            Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Selectedvalue_set");
            Ddo_funcaoapfevidencia_nomearq_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Dropdownoptionstype");
            Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Titlecontrolidtoreplace");
            Ddo_funcaoapfevidencia_nomearq_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includesortasc"));
            Ddo_funcaoapfevidencia_nomearq_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includesortdsc"));
            Ddo_funcaoapfevidencia_nomearq_Sortedstatus = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Sortedstatus");
            Ddo_funcaoapfevidencia_nomearq_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includefilter"));
            Ddo_funcaoapfevidencia_nomearq_Filtertype = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filtertype");
            Ddo_funcaoapfevidencia_nomearq_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filterisrange"));
            Ddo_funcaoapfevidencia_nomearq_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includedatalist"));
            Ddo_funcaoapfevidencia_nomearq_Datalisttype = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalisttype");
            Ddo_funcaoapfevidencia_nomearq_Datalistproc = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalistproc");
            Ddo_funcaoapfevidencia_nomearq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapfevidencia_nomearq_Sortasc = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Sortasc");
            Ddo_funcaoapfevidencia_nomearq_Sortdsc = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Sortdsc");
            Ddo_funcaoapfevidencia_nomearq_Loadingdata = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Loadingdata");
            Ddo_funcaoapfevidencia_nomearq_Cleanfilter = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Cleanfilter");
            Ddo_funcaoapfevidencia_nomearq_Noresultsfound = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Noresultsfound");
            Ddo_funcaoapfevidencia_nomearq_Searchbuttontext = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Searchbuttontext");
            Ddo_funcaoapfevidencia_tipoarq_Caption = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Caption");
            Ddo_funcaoapfevidencia_tipoarq_Tooltip = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Tooltip");
            Ddo_funcaoapfevidencia_tipoarq_Cls = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Cls");
            Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filteredtext_set");
            Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Selectedvalue_set");
            Ddo_funcaoapfevidencia_tipoarq_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Dropdownoptionstype");
            Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Titlecontrolidtoreplace");
            Ddo_funcaoapfevidencia_tipoarq_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includesortasc"));
            Ddo_funcaoapfevidencia_tipoarq_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includesortdsc"));
            Ddo_funcaoapfevidencia_tipoarq_Sortedstatus = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Sortedstatus");
            Ddo_funcaoapfevidencia_tipoarq_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includefilter"));
            Ddo_funcaoapfevidencia_tipoarq_Filtertype = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filtertype");
            Ddo_funcaoapfevidencia_tipoarq_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filterisrange"));
            Ddo_funcaoapfevidencia_tipoarq_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Includedatalist"));
            Ddo_funcaoapfevidencia_tipoarq_Datalisttype = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Datalisttype");
            Ddo_funcaoapfevidencia_tipoarq_Datalistproc = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Datalistproc");
            Ddo_funcaoapfevidencia_tipoarq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapfevidencia_tipoarq_Sortasc = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Sortasc");
            Ddo_funcaoapfevidencia_tipoarq_Sortdsc = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Sortdsc");
            Ddo_funcaoapfevidencia_tipoarq_Loadingdata = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Loadingdata");
            Ddo_funcaoapfevidencia_tipoarq_Cleanfilter = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Cleanfilter");
            Ddo_funcaoapfevidencia_tipoarq_Noresultsfound = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Noresultsfound");
            Ddo_funcaoapfevidencia_tipoarq_Searchbuttontext = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Searchbuttontext");
            Ddo_funcaoapfevidencia_data_Caption = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Caption");
            Ddo_funcaoapfevidencia_data_Tooltip = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Tooltip");
            Ddo_funcaoapfevidencia_data_Cls = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Cls");
            Ddo_funcaoapfevidencia_data_Filteredtext_set = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Filteredtext_set");
            Ddo_funcaoapfevidencia_data_Filteredtextto_set = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Filteredtextto_set");
            Ddo_funcaoapfevidencia_data_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Dropdownoptionstype");
            Ddo_funcaoapfevidencia_data_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Titlecontrolidtoreplace");
            Ddo_funcaoapfevidencia_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Includesortasc"));
            Ddo_funcaoapfevidencia_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Includesortdsc"));
            Ddo_funcaoapfevidencia_data_Sortedstatus = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Sortedstatus");
            Ddo_funcaoapfevidencia_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Includefilter"));
            Ddo_funcaoapfevidencia_data_Filtertype = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Filtertype");
            Ddo_funcaoapfevidencia_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Filterisrange"));
            Ddo_funcaoapfevidencia_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Includedatalist"));
            Ddo_funcaoapfevidencia_data_Sortasc = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Sortasc");
            Ddo_funcaoapfevidencia_data_Sortdsc = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Sortdsc");
            Ddo_funcaoapfevidencia_data_Cleanfilter = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Cleanfilter");
            Ddo_funcaoapfevidencia_data_Rangefilterfrom = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Rangefilterfrom");
            Ddo_funcaoapfevidencia_data_Rangefilterto = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Rangefilterto");
            Ddo_funcaoapfevidencia_data_Searchbuttontext = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_funcaoapfevidencia_descricao_Activeeventkey = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Activeeventkey");
            Ddo_funcaoapfevidencia_descricao_Filteredtext_get = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Filteredtext_get");
            Ddo_funcaoapfevidencia_descricao_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO_Selectedvalue_get");
            Ddo_funcaoapfevidencia_nomearq_Activeeventkey = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Activeeventkey");
            Ddo_funcaoapfevidencia_nomearq_Filteredtext_get = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filteredtext_get");
            Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Selectedvalue_get");
            Ddo_funcaoapfevidencia_tipoarq_Activeeventkey = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Activeeventkey");
            Ddo_funcaoapfevidencia_tipoarq_Filteredtext_get = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Filteredtext_get");
            Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ_Selectedvalue_get");
            Ddo_funcaoapfevidencia_data_Activeeventkey = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Activeeventkey");
            Ddo_funcaoapfevidencia_data_Filteredtext_get = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Filteredtext_get");
            Ddo_funcaoapfevidencia_data_Filteredtextto_get = cgiGet( "DDO_FUNCAOAPFEVIDENCIA_DATA_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPFEVIDENCIA_DESCRICAO1"), AV17FuncaoAPFEvidencia_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPFEVIDENCIA_DESCRICAO2"), AV21FuncaoAPFEvidencia_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPFEVIDENCIA_DESCRICAO3"), AV25FuncaoAPFEvidencia_Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_DESCRICAO"), AV34TFFuncaoAPFEvidencia_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL"), AV35TFFuncaoAPFEvidencia_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ"), AV38TFFuncaoAPFEvidencia_NomeArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL"), AV39TFFuncaoAPFEvidencia_NomeArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_TIPOARQ"), AV42TFFuncaoAPFEvidencia_TipoArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL"), AV43TFFuncaoAPFEvidencia_TipoArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_DATA"), 0) != AV46TFFuncaoAPFEvidencia_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFFUNCAOAPFEVIDENCIA_DATA_TO"), 0) != AV47TFFuncaoAPFEvidencia_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E27AD2 */
         E27AD2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27AD2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "FUNCAOAPFEVIDENCIA_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "FUNCAOAPFEVIDENCIA_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "FUNCAOAPFEVIDENCIA_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTffuncaoapfevidencia_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfevidencia_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_descricao_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfevidencia_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_descricao_sel_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_nomearq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfevidencia_nomearq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_nomearq_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_nomearq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfevidencia_nomearq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_nomearq_sel_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_tipoarq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfevidencia_tipoarq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_tipoarq_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_tipoarq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfevidencia_tipoarq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_tipoarq_sel_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfevidencia_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_data_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfevidencia_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_data_to_Visible), 5, 0)));
         Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFEvidencia_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_descricao_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace);
         AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace = Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace", AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace);
         edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFEvidencia_NomeArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_nomearq_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace);
         AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace = Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace", AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace);
         edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFEvidencia_TipoArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace);
         AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace = Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace", AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace);
         edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapfevidencia_data_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFEvidencia_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_data_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfevidencia_data_Titlecontrolidtoreplace);
         AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace = Ddo_funcaoapfevidencia_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace", AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace);
         edtavDdo_funcaoapfevidencia_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapfevidencia_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfevidencia_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Evidencia da Funcao de Transa��o";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Descri��o", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Tipo", 0);
         cmbavOrderedby.addItem("4", "Hora Upload", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV51DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV51DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E28AD2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV33FuncaoAPFEvidencia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37FuncaoAPFEvidencia_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41FuncaoAPFEvidencia_TipoArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45FuncaoAPFEvidencia_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoAPFEvidencia_Descricao_Titleformat = 2;
         edtFuncaoAPFEvidencia_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Descricao_Internalname, "Title", edtFuncaoAPFEvidencia_Descricao_Title);
         edtFuncaoAPFEvidencia_NomeArq_Titleformat = 2;
         edtFuncaoAPFEvidencia_NomeArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_NomeArq_Internalname, "Title", edtFuncaoAPFEvidencia_NomeArq_Title);
         edtFuncaoAPFEvidencia_TipoArq_Titleformat = 2;
         edtFuncaoAPFEvidencia_TipoArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_TipoArq_Internalname, "Title", edtFuncaoAPFEvidencia_TipoArq_Title);
         edtFuncaoAPFEvidencia_Data_Titleformat = 2;
         edtFuncaoAPFEvidencia_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Hora Upload", AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFEvidencia_Data_Internalname, "Title", edtFuncaoAPFEvidencia_Data_Title);
         AV53GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53GridCurrentPage), 10, 0)));
         AV54GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54GridPageCount), 10, 0)));
         AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = AV17FuncaoAPFEvidencia_Descricao1;
         AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = AV21FuncaoAPFEvidencia_Descricao2;
         AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = AV25FuncaoAPFEvidencia_Descricao3;
         AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = AV34TFFuncaoAPFEvidencia_Descricao;
         AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel = AV35TFFuncaoAPFEvidencia_Descricao_Sel;
         AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = AV38TFFuncaoAPFEvidencia_NomeArq;
         AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel = AV39TFFuncaoAPFEvidencia_NomeArq_Sel;
         AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = AV42TFFuncaoAPFEvidencia_TipoArq;
         AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel = AV43TFFuncaoAPFEvidencia_TipoArq_Sel;
         AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data = AV46TFFuncaoAPFEvidencia_Data;
         AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to = AV47TFFuncaoAPFEvidencia_Data_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33FuncaoAPFEvidencia_DescricaoTitleFilterData", AV33FuncaoAPFEvidencia_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37FuncaoAPFEvidencia_NomeArqTitleFilterData", AV37FuncaoAPFEvidencia_NomeArqTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV41FuncaoAPFEvidencia_TipoArqTitleFilterData", AV41FuncaoAPFEvidencia_TipoArqTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45FuncaoAPFEvidencia_DataTitleFilterData", AV45FuncaoAPFEvidencia_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11AD2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV52PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV52PageToGo) ;
         }
      }

      protected void E12AD2( )
      {
         /* Ddo_funcaoapfevidencia_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapfevidencia_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_descricao_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapfevidencia_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_descricao_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFFuncaoAPFEvidencia_Descricao = Ddo_funcaoapfevidencia_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFFuncaoAPFEvidencia_Descricao", AV34TFFuncaoAPFEvidencia_Descricao);
            AV35TFFuncaoAPFEvidencia_Descricao_Sel = Ddo_funcaoapfevidencia_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFFuncaoAPFEvidencia_Descricao_Sel", AV35TFFuncaoAPFEvidencia_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13AD2( )
      {
         /* Ddo_funcaoapfevidencia_nomearq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_nomearq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapfevidencia_nomearq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_nomearq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_nomearq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapfevidencia_nomearq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_nomearq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_nomearq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFFuncaoAPFEvidencia_NomeArq = Ddo_funcaoapfevidencia_nomearq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFuncaoAPFEvidencia_NomeArq", AV38TFFuncaoAPFEvidencia_NomeArq);
            AV39TFFuncaoAPFEvidencia_NomeArq_Sel = Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoAPFEvidencia_NomeArq_Sel", AV39TFFuncaoAPFEvidencia_NomeArq_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14AD2( )
      {
         /* Ddo_funcaoapfevidencia_tipoarq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_tipoarq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapfevidencia_tipoarq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_tipoarq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_tipoarq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapfevidencia_tipoarq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_tipoarq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_tipoarq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV42TFFuncaoAPFEvidencia_TipoArq = Ddo_funcaoapfevidencia_tipoarq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFFuncaoAPFEvidencia_TipoArq", AV42TFFuncaoAPFEvidencia_TipoArq);
            AV43TFFuncaoAPFEvidencia_TipoArq_Sel = Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFuncaoAPFEvidencia_TipoArq_Sel", AV43TFFuncaoAPFEvidencia_TipoArq_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15AD2( )
      {
         /* Ddo_funcaoapfevidencia_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapfevidencia_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_data_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapfevidencia_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_data_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFFuncaoAPFEvidencia_Data = context.localUtil.CToT( Ddo_funcaoapfevidencia_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFuncaoAPFEvidencia_Data", context.localUtil.TToC( AV46TFFuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
            AV47TFFuncaoAPFEvidencia_Data_To = context.localUtil.CToT( Ddo_funcaoapfevidencia_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFEvidencia_Data_To", context.localUtil.TToC( AV47TFFuncaoAPFEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV47TFFuncaoAPFEvidencia_Data_To) )
            {
               AV47TFFuncaoAPFEvidencia_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV47TFFuncaoAPFEvidencia_Data_To)), (short)(DateTimeUtil.Month( AV47TFFuncaoAPFEvidencia_Data_To)), (short)(DateTimeUtil.Day( AV47TFFuncaoAPFEvidencia_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFEvidencia_Data_To", context.localUtil.TToC( AV47TFFuncaoAPFEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E29AD2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV76Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("funcaoapfevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A406FuncaoAPFEvidencia_Codigo) + "," + UrlEncode("" +A165FuncaoAPF_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV77Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("funcaoapfevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A406FuncaoAPFEvidencia_Codigo) + "," + UrlEncode("" +A165FuncaoAPF_Codigo);
         edtFuncaoAPFEvidencia_Descricao_Link = formatLink("viewfuncaoapfevidencia.aspx") + "?" + UrlEncode("" +A406FuncaoAPFEvidencia_Codigo) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A360FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 88;
         }
         sendrow_882( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_88_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(88, GridRow);
         }
      }

      protected void E16AD2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22AD2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E17AD2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPFEvidencia_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPFEvidencia_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPFEvidencia_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFFuncaoAPFEvidencia_Descricao, AV35TFFuncaoAPFEvidencia_Descricao_Sel, AV38TFFuncaoAPFEvidencia_NomeArq, AV39TFFuncaoAPFEvidencia_NomeArq_Sel, AV42TFFuncaoAPFEvidencia_TipoArq, AV43TFFuncaoAPFEvidencia_TipoArq_Sel, AV46TFFuncaoAPFEvidencia_Data, AV47TFFuncaoAPFEvidencia_Data_To, AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E23AD2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24AD2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E18AD2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPFEvidencia_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPFEvidencia_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPFEvidencia_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFFuncaoAPFEvidencia_Descricao, AV35TFFuncaoAPFEvidencia_Descricao_Sel, AV38TFFuncaoAPFEvidencia_NomeArq, AV39TFFuncaoAPFEvidencia_NomeArq_Sel, AV42TFFuncaoAPFEvidencia_TipoArq, AV43TFFuncaoAPFEvidencia_TipoArq_Sel, AV46TFFuncaoAPFEvidencia_Data, AV47TFFuncaoAPFEvidencia_Data_To, AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E25AD2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19AD2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPFEvidencia_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPFEvidencia_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPFEvidencia_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFFuncaoAPFEvidencia_Descricao, AV35TFFuncaoAPFEvidencia_Descricao_Sel, AV38TFFuncaoAPFEvidencia_NomeArq, AV39TFFuncaoAPFEvidencia_NomeArq_Sel, AV42TFFuncaoAPFEvidencia_TipoArq, AV43TFFuncaoAPFEvidencia_TipoArq_Sel, AV46TFFuncaoAPFEvidencia_Data, AV47TFFuncaoAPFEvidencia_Data_To, AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace, AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace, AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A406FuncaoAPFEvidencia_Codigo, A165FuncaoAPF_Codigo, A360FuncaoAPF_SistemaCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E26AD2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20AD2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E21AD2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("funcaoapfevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_funcaoapfevidencia_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_descricao_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_descricao_Sortedstatus);
         Ddo_funcaoapfevidencia_nomearq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_nomearq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_nomearq_Sortedstatus);
         Ddo_funcaoapfevidencia_tipoarq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_tipoarq_Sortedstatus);
         Ddo_funcaoapfevidencia_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_data_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_data_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_funcaoapfevidencia_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_descricao_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_funcaoapfevidencia_nomearq_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_nomearq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_nomearq_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_funcaoapfevidencia_tipoarq_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_tipoarq_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_funcaoapfevidencia_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_data_Internalname, "SortedStatus", Ddo_funcaoapfevidencia_data_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncaoapfevidencia_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapfevidencia_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfevidencia_descricao1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
         {
            edtavFuncaoapfevidencia_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapfevidencia_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfevidencia_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavFuncaoapfevidencia_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapfevidencia_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfevidencia_descricao2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
         {
            edtavFuncaoapfevidencia_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapfevidencia_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfevidencia_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavFuncaoapfevidencia_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapfevidencia_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfevidencia_descricao3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
         {
            edtavFuncaoapfevidencia_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapfevidencia_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfevidencia_descricao3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "FUNCAOAPFEVIDENCIA_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21FuncaoAPFEvidencia_Descricao2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPFEvidencia_Descricao2", AV21FuncaoAPFEvidencia_Descricao2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "FUNCAOAPFEVIDENCIA_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25FuncaoAPFEvidencia_Descricao3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPFEvidencia_Descricao3", AV25FuncaoAPFEvidencia_Descricao3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV34TFFuncaoAPFEvidencia_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFFuncaoAPFEvidencia_Descricao", AV34TFFuncaoAPFEvidencia_Descricao);
         Ddo_funcaoapfevidencia_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_descricao_Internalname, "FilteredText_set", Ddo_funcaoapfevidencia_descricao_Filteredtext_set);
         AV35TFFuncaoAPFEvidencia_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFFuncaoAPFEvidencia_Descricao_Sel", AV35TFFuncaoAPFEvidencia_Descricao_Sel);
         Ddo_funcaoapfevidencia_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_descricao_Internalname, "SelectedValue_set", Ddo_funcaoapfevidencia_descricao_Selectedvalue_set);
         AV38TFFuncaoAPFEvidencia_NomeArq = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFuncaoAPFEvidencia_NomeArq", AV38TFFuncaoAPFEvidencia_NomeArq);
         Ddo_funcaoapfevidencia_nomearq_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_nomearq_Internalname, "FilteredText_set", Ddo_funcaoapfevidencia_nomearq_Filteredtext_set);
         AV39TFFuncaoAPFEvidencia_NomeArq_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoAPFEvidencia_NomeArq_Sel", AV39TFFuncaoAPFEvidencia_NomeArq_Sel);
         Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_nomearq_Internalname, "SelectedValue_set", Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set);
         AV42TFFuncaoAPFEvidencia_TipoArq = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFFuncaoAPFEvidencia_TipoArq", AV42TFFuncaoAPFEvidencia_TipoArq);
         Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "FilteredText_set", Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set);
         AV43TFFuncaoAPFEvidencia_TipoArq_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFuncaoAPFEvidencia_TipoArq_Sel", AV43TFFuncaoAPFEvidencia_TipoArq_Sel);
         Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "SelectedValue_set", Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set);
         AV46TFFuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFuncaoAPFEvidencia_Data", context.localUtil.TToC( AV46TFFuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_funcaoapfevidencia_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_data_Internalname, "FilteredText_set", Ddo_funcaoapfevidencia_data_Filteredtext_set);
         AV47TFFuncaoAPFEvidencia_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFEvidencia_Data_To", context.localUtil.TToC( AV47TFFuncaoAPFEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_funcaoapfevidencia_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_data_Internalname, "FilteredTextTo_set", Ddo_funcaoapfevidencia_data_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "FUNCAOAPFEVIDENCIA_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17FuncaoAPFEvidencia_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPFEvidencia_Descricao1", AV17FuncaoAPFEvidencia_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV78Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV78Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV78Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV79GXV1 = 1;
         while ( AV79GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV79GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
            {
               AV34TFFuncaoAPFEvidencia_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFFuncaoAPFEvidencia_Descricao", AV34TFFuncaoAPFEvidencia_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFFuncaoAPFEvidencia_Descricao)) )
               {
                  Ddo_funcaoapfevidencia_descricao_Filteredtext_set = AV34TFFuncaoAPFEvidencia_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_descricao_Internalname, "FilteredText_set", Ddo_funcaoapfevidencia_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL") == 0 )
            {
               AV35TFFuncaoAPFEvidencia_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFFuncaoAPFEvidencia_Descricao_Sel", AV35TFFuncaoAPFEvidencia_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFFuncaoAPFEvidencia_Descricao_Sel)) )
               {
                  Ddo_funcaoapfevidencia_descricao_Selectedvalue_set = AV35TFFuncaoAPFEvidencia_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_descricao_Internalname, "SelectedValue_set", Ddo_funcaoapfevidencia_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_NOMEARQ") == 0 )
            {
               AV38TFFuncaoAPFEvidencia_NomeArq = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFuncaoAPFEvidencia_NomeArq", AV38TFFuncaoAPFEvidencia_NomeArq);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFFuncaoAPFEvidencia_NomeArq)) )
               {
                  Ddo_funcaoapfevidencia_nomearq_Filteredtext_set = AV38TFFuncaoAPFEvidencia_NomeArq;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_nomearq_Internalname, "FilteredText_set", Ddo_funcaoapfevidencia_nomearq_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL") == 0 )
            {
               AV39TFFuncaoAPFEvidencia_NomeArq_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoAPFEvidencia_NomeArq_Sel", AV39TFFuncaoAPFEvidencia_NomeArq_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFFuncaoAPFEvidencia_NomeArq_Sel)) )
               {
                  Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set = AV39TFFuncaoAPFEvidencia_NomeArq_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_nomearq_Internalname, "SelectedValue_set", Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_TIPOARQ") == 0 )
            {
               AV42TFFuncaoAPFEvidencia_TipoArq = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFFuncaoAPFEvidencia_TipoArq", AV42TFFuncaoAPFEvidencia_TipoArq);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFFuncaoAPFEvidencia_TipoArq)) )
               {
                  Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set = AV42TFFuncaoAPFEvidencia_TipoArq;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "FilteredText_set", Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL") == 0 )
            {
               AV43TFFuncaoAPFEvidencia_TipoArq_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFuncaoAPFEvidencia_TipoArq_Sel", AV43TFFuncaoAPFEvidencia_TipoArq_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFuncaoAPFEvidencia_TipoArq_Sel)) )
               {
                  Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set = AV43TFFuncaoAPFEvidencia_TipoArq_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_tipoarq_Internalname, "SelectedValue_set", Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_DATA") == 0 )
            {
               AV46TFFuncaoAPFEvidencia_Data = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFuncaoAPFEvidencia_Data", context.localUtil.TToC( AV46TFFuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
               AV47TFFuncaoAPFEvidencia_Data_To = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFEvidencia_Data_To", context.localUtil.TToC( AV47TFFuncaoAPFEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV46TFFuncaoAPFEvidencia_Data) )
               {
                  AV48DDO_FuncaoAPFEvidencia_DataAuxDate = DateTimeUtil.ResetTime(AV46TFFuncaoAPFEvidencia_Data);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DDO_FuncaoAPFEvidencia_DataAuxDate", context.localUtil.Format(AV48DDO_FuncaoAPFEvidencia_DataAuxDate, "99/99/99"));
                  Ddo_funcaoapfevidencia_data_Filteredtext_set = context.localUtil.DToC( AV48DDO_FuncaoAPFEvidencia_DataAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_data_Internalname, "FilteredText_set", Ddo_funcaoapfevidencia_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV47TFFuncaoAPFEvidencia_Data_To) )
               {
                  AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo = DateTimeUtil.ResetTime(AV47TFFuncaoAPFEvidencia_Data_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo", context.localUtil.Format(AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo, "99/99/99"));
                  Ddo_funcaoapfevidencia_data_Filteredtextto_set = context.localUtil.DToC( AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfevidencia_data_Internalname, "FilteredTextTo_set", Ddo_funcaoapfevidencia_data_Filteredtextto_set);
               }
            }
            AV79GXV1 = (int)(AV79GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17FuncaoAPFEvidencia_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPFEvidencia_Descricao1", AV17FuncaoAPFEvidencia_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21FuncaoAPFEvidencia_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPFEvidencia_Descricao2", AV21FuncaoAPFEvidencia_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25FuncaoAPFEvidencia_Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPFEvidencia_Descricao3", AV25FuncaoAPFEvidencia_Descricao3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV78Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFFuncaoAPFEvidencia_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV34TFFuncaoAPFEvidencia_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFFuncaoAPFEvidencia_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV35TFFuncaoAPFEvidencia_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFFuncaoAPFEvidencia_NomeArq)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_NOMEARQ";
            AV11GridStateFilterValue.gxTpr_Value = AV38TFFuncaoAPFEvidencia_NomeArq;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFFuncaoAPFEvidencia_NomeArq_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFFuncaoAPFEvidencia_NomeArq_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFFuncaoAPFEvidencia_TipoArq)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_TIPOARQ";
            AV11GridStateFilterValue.gxTpr_Value = AV42TFFuncaoAPFEvidencia_TipoArq;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFuncaoAPFEvidencia_TipoArq_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV43TFFuncaoAPFEvidencia_TipoArq_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV46TFFuncaoAPFEvidencia_Data) && (DateTime.MinValue==AV47TFFuncaoAPFEvidencia_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV46TFFuncaoAPFEvidencia_Data, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV47TFFuncaoAPFEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV78Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17FuncaoAPFEvidencia_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17FuncaoAPFEvidencia_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21FuncaoAPFEvidencia_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21FuncaoAPFEvidencia_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25FuncaoAPFEvidencia_Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25FuncaoAPFEvidencia_Descricao3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV78Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "FuncaoAPFEvidencia";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_AD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_AD2( true) ;
         }
         else
         {
            wb_table2_8_AD2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_AD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_82_AD2( true) ;
         }
         else
         {
            wb_table3_82_AD2( false) ;
         }
         return  ;
      }

      protected void wb_table3_82_AD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AD2e( true) ;
         }
         else
         {
            wb_table1_2_AD2e( false) ;
         }
      }

      protected void wb_table3_82_AD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_85_AD2( true) ;
         }
         else
         {
            wb_table4_85_AD2( false) ;
         }
         return  ;
      }

      protected void wb_table4_85_AD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_82_AD2e( true) ;
         }
         else
         {
            wb_table3_82_AD2e( false) ;
         }
      }

      protected void wb_table4_85_AD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"88\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "APFEvidencia_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Fun��o de Transa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFEvidencia_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFEvidencia_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFEvidencia_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFEvidencia_NomeArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFEvidencia_NomeArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFEvidencia_NomeArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFEvidencia_TipoArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFEvidencia_TipoArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFEvidencia_TipoArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFEvidencia_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFEvidencia_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFEvidencia_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A407FuncaoAPFEvidencia_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFEvidencia_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFEvidencia_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtFuncaoAPFEvidencia_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFEvidencia_NomeArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFEvidencia_NomeArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFEvidencia_TipoArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFEvidencia_TipoArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFEvidencia_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFEvidencia_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 88 )
         {
            wbEnd = 0;
            nRC_GXsfl_88 = (short)(nGXsfl_88_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_85_AD2e( true) ;
         }
         else
         {
            wb_table4_85_AD2e( false) ;
         }
      }

      protected void wb_table2_8_AD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFuncaoapfevidenciatitle_Internalname, "Evid�ncias", "", "", lblFuncaoapfevidenciatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_13_AD2( true) ;
         }
         else
         {
            wb_table5_13_AD2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_AD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWFuncaoAPFEvidencia.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_AD2( true) ;
         }
         else
         {
            wb_table6_23_AD2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_AD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_AD2e( true) ;
         }
         else
         {
            wb_table2_8_AD2e( false) ;
         }
      }

      protected void wb_table6_23_AD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_AD2( true) ;
         }
         else
         {
            wb_table7_28_AD2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_AD2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_AD2e( true) ;
         }
         else
         {
            wb_table6_23_AD2e( false) ;
         }
      }

      protected void wb_table7_28_AD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWFuncaoAPFEvidencia.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_AD2( true) ;
         }
         else
         {
            wb_table8_37_AD2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_AD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPFEvidencia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWFuncaoAPFEvidencia.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_54_AD2( true) ;
         }
         else
         {
            wb_table9_54_AD2( false) ;
         }
         return  ;
      }

      protected void wb_table9_54_AD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPFEvidencia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWFuncaoAPFEvidencia.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_71_AD2( true) ;
         }
         else
         {
            wb_table10_71_AD2( false) ;
         }
         return  ;
      }

      protected void wb_table10_71_AD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_AD2e( true) ;
         }
         else
         {
            wb_table7_28_AD2e( false) ;
         }
      }

      protected void wb_table10_71_AD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWFuncaoAPFEvidencia.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapfevidencia_descricao3_Internalname, AV25FuncaoAPFEvidencia_Descricao3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", 0, edtavFuncaoapfevidencia_descricao3_Visible, 1, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "", "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_71_AD2e( true) ;
         }
         else
         {
            wb_table10_71_AD2e( false) ;
         }
      }

      protected void wb_table9_54_AD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWFuncaoAPFEvidencia.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapfevidencia_descricao2_Internalname, AV21FuncaoAPFEvidencia_Descricao2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, edtavFuncaoapfevidencia_descricao2_Visible, 1, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "", "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_54_AD2e( true) ;
         }
         else
         {
            wb_table9_54_AD2e( false) ;
         }
      }

      protected void wb_table8_37_AD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWFuncaoAPFEvidencia.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapfevidencia_descricao1_Internalname, AV17FuncaoAPFEvidencia_Descricao1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, edtavFuncaoapfevidencia_descricao1_Visible, 1, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "", "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_AD2e( true) ;
         }
         else
         {
            wb_table8_37_AD2e( false) ;
         }
      }

      protected void wb_table5_13_AD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoAPFEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_AD2e( true) ;
         }
         else
         {
            wb_table5_13_AD2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAD2( ) ;
         WSAD2( ) ;
         WEAD2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020518132113");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwfuncaoapfevidencia.js", "?2020518132113");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_idx;
         edtFuncaoAPFEvidencia_Codigo_Internalname = "FUNCAOAPFEVIDENCIA_CODIGO_"+sGXsfl_88_idx;
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO_"+sGXsfl_88_idx;
         edtFuncaoAPFEvidencia_Descricao_Internalname = "FUNCAOAPFEVIDENCIA_DESCRICAO_"+sGXsfl_88_idx;
         edtFuncaoAPFEvidencia_NomeArq_Internalname = "FUNCAOAPFEVIDENCIA_NOMEARQ_"+sGXsfl_88_idx;
         edtFuncaoAPFEvidencia_TipoArq_Internalname = "FUNCAOAPFEVIDENCIA_TIPOARQ_"+sGXsfl_88_idx;
         edtFuncaoAPFEvidencia_Data_Internalname = "FUNCAOAPFEVIDENCIA_DATA_"+sGXsfl_88_idx;
      }

      protected void SubsflControlProps_fel_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_fel_idx;
         edtFuncaoAPFEvidencia_Codigo_Internalname = "FUNCAOAPFEVIDENCIA_CODIGO_"+sGXsfl_88_fel_idx;
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO_"+sGXsfl_88_fel_idx;
         edtFuncaoAPFEvidencia_Descricao_Internalname = "FUNCAOAPFEVIDENCIA_DESCRICAO_"+sGXsfl_88_fel_idx;
         edtFuncaoAPFEvidencia_NomeArq_Internalname = "FUNCAOAPFEVIDENCIA_NOMEARQ_"+sGXsfl_88_fel_idx;
         edtFuncaoAPFEvidencia_TipoArq_Internalname = "FUNCAOAPFEVIDENCIA_TIPOARQ_"+sGXsfl_88_fel_idx;
         edtFuncaoAPFEvidencia_Data_Internalname = "FUNCAOAPFEVIDENCIA_DATA_"+sGXsfl_88_fel_idx;
      }

      protected void sendrow_882( )
      {
         SubsflControlProps_882( ) ;
         WBAD0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_88_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_88_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_88_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV76Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV76Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV77Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV77Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFEvidencia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A406FuncaoAPFEvidencia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A406FuncaoAPFEvidencia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFEvidencia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFEvidencia_Descricao_Internalname,(String)A407FuncaoAPFEvidencia_Descricao,(String)A407FuncaoAPFEvidencia_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtFuncaoAPFEvidencia_Descricao_Link,(String)"",(String)"",(String)"",(String)edtFuncaoAPFEvidencia_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)88,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFEvidencia_NomeArq_Internalname,StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFEvidencia_NomeArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFEvidencia_TipoArq_Internalname,StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFEvidencia_TipoArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"TipoArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFEvidencia_Data_Internalname,context.localUtil.TToC( A411FuncaoAPFEvidencia_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A411FuncaoAPFEvidencia_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFEvidencia_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFEVIDENCIA_CODIGO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A406FuncaoAPFEvidencia_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFEVIDENCIA_DESCRICAO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, A407FuncaoAPFEvidencia_Descricao));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFEVIDENCIA_NOMEARQ"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A409FuncaoAPFEvidencia_NomeArq, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFEVIDENCIA_TIPOARQ"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A410FuncaoAPFEvidencia_TipoArq, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFEVIDENCIA_DATA"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( A411FuncaoAPFEvidencia_Data, "99/99/99 99:99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         /* End function sendrow_882 */
      }

      protected void init_default_properties( )
      {
         lblFuncaoapfevidenciatitle_Internalname = "FUNCAOAPFEVIDENCIATITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavFuncaoapfevidencia_descricao1_Internalname = "vFUNCAOAPFEVIDENCIA_DESCRICAO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavFuncaoapfevidencia_descricao2_Internalname = "vFUNCAOAPFEVIDENCIA_DESCRICAO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavFuncaoapfevidencia_descricao3_Internalname = "vFUNCAOAPFEVIDENCIA_DESCRICAO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtFuncaoAPFEvidencia_Codigo_Internalname = "FUNCAOAPFEVIDENCIA_CODIGO";
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO";
         edtFuncaoAPFEvidencia_Descricao_Internalname = "FUNCAOAPFEVIDENCIA_DESCRICAO";
         edtFuncaoAPFEvidencia_NomeArq_Internalname = "FUNCAOAPFEVIDENCIA_NOMEARQ";
         edtFuncaoAPFEvidencia_TipoArq_Internalname = "FUNCAOAPFEVIDENCIA_TIPOARQ";
         edtFuncaoAPFEvidencia_Data_Internalname = "FUNCAOAPFEVIDENCIA_DATA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTffuncaoapfevidencia_descricao_Internalname = "vTFFUNCAOAPFEVIDENCIA_DESCRICAO";
         edtavTffuncaoapfevidencia_descricao_sel_Internalname = "vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL";
         edtavTffuncaoapfevidencia_nomearq_Internalname = "vTFFUNCAOAPFEVIDENCIA_NOMEARQ";
         edtavTffuncaoapfevidencia_nomearq_sel_Internalname = "vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL";
         edtavTffuncaoapfevidencia_tipoarq_Internalname = "vTFFUNCAOAPFEVIDENCIA_TIPOARQ";
         edtavTffuncaoapfevidencia_tipoarq_sel_Internalname = "vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL";
         edtavTffuncaoapfevidencia_data_Internalname = "vTFFUNCAOAPFEVIDENCIA_DATA";
         edtavTffuncaoapfevidencia_data_to_Internalname = "vTFFUNCAOAPFEVIDENCIA_DATA_TO";
         edtavDdo_funcaoapfevidencia_dataauxdate_Internalname = "vDDO_FUNCAOAPFEVIDENCIA_DATAAUXDATE";
         edtavDdo_funcaoapfevidencia_dataauxdateto_Internalname = "vDDO_FUNCAOAPFEVIDENCIA_DATAAUXDATETO";
         divDdo_funcaoapfevidencia_dataauxdates_Internalname = "DDO_FUNCAOAPFEVIDENCIA_DATAAUXDATES";
         Ddo_funcaoapfevidencia_descricao_Internalname = "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO";
         edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapfevidencia_nomearq_Internalname = "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ";
         edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapfevidencia_tipoarq_Internalname = "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ";
         edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapfevidencia_data_Internalname = "DDO_FUNCAOAPFEVIDENCIA_DATA";
         edtavDdo_funcaoapfevidencia_datatitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPFEVIDENCIA_DATATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtFuncaoAPFEvidencia_Data_Jsonclick = "";
         edtFuncaoAPFEvidencia_TipoArq_Jsonclick = "";
         edtFuncaoAPFEvidencia_NomeArq_Jsonclick = "";
         edtFuncaoAPFEvidencia_Descricao_Jsonclick = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         edtFuncaoAPFEvidencia_Codigo_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtFuncaoAPFEvidencia_Descricao_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtFuncaoAPFEvidencia_Data_Titleformat = 0;
         edtFuncaoAPFEvidencia_TipoArq_Titleformat = 0;
         edtFuncaoAPFEvidencia_NomeArq_Titleformat = 0;
         edtFuncaoAPFEvidencia_Descricao_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavFuncaoapfevidencia_descricao3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavFuncaoapfevidencia_descricao2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavFuncaoapfevidencia_descricao1_Visible = 1;
         edtFuncaoAPFEvidencia_Data_Title = "Hora Upload";
         edtFuncaoAPFEvidencia_TipoArq_Title = "Tipo";
         edtFuncaoAPFEvidencia_NomeArq_Title = "Nome";
         edtFuncaoAPFEvidencia_Descricao_Title = "Descri��o";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_funcaoapfevidencia_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfevidencia_dataauxdateto_Jsonclick = "";
         edtavDdo_funcaoapfevidencia_dataauxdate_Jsonclick = "";
         edtavTffuncaoapfevidencia_data_to_Jsonclick = "";
         edtavTffuncaoapfevidencia_data_to_Visible = 1;
         edtavTffuncaoapfevidencia_data_Jsonclick = "";
         edtavTffuncaoapfevidencia_data_Visible = 1;
         edtavTffuncaoapfevidencia_tipoarq_sel_Jsonclick = "";
         edtavTffuncaoapfevidencia_tipoarq_sel_Visible = 1;
         edtavTffuncaoapfevidencia_tipoarq_Jsonclick = "";
         edtavTffuncaoapfevidencia_tipoarq_Visible = 1;
         edtavTffuncaoapfevidencia_nomearq_sel_Jsonclick = "";
         edtavTffuncaoapfevidencia_nomearq_sel_Visible = 1;
         edtavTffuncaoapfevidencia_nomearq_Jsonclick = "";
         edtavTffuncaoapfevidencia_nomearq_Visible = 1;
         edtavTffuncaoapfevidencia_descricao_sel_Visible = 1;
         edtavTffuncaoapfevidencia_descricao_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_funcaoapfevidencia_data_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfevidencia_data_Rangefilterto = "At�";
         Ddo_funcaoapfevidencia_data_Rangefilterfrom = "Desde";
         Ddo_funcaoapfevidencia_data_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfevidencia_data_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfevidencia_data_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfevidencia_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaoapfevidencia_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_data_Filtertype = "Date";
         Ddo_funcaoapfevidencia_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_data_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfevidencia_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfevidencia_data_Cls = "ColumnSettings";
         Ddo_funcaoapfevidencia_data_Tooltip = "Op��es";
         Ddo_funcaoapfevidencia_data_Caption = "";
         Ddo_funcaoapfevidencia_tipoarq_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfevidencia_tipoarq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapfevidencia_tipoarq_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfevidencia_tipoarq_Loadingdata = "Carregando dados...";
         Ddo_funcaoapfevidencia_tipoarq_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfevidencia_tipoarq_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfevidencia_tipoarq_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapfevidencia_tipoarq_Datalistproc = "GetWWFuncaoAPFEvidenciaFilterData";
         Ddo_funcaoapfevidencia_tipoarq_Datalisttype = "Dynamic";
         Ddo_funcaoapfevidencia_tipoarq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_tipoarq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapfevidencia_tipoarq_Filtertype = "Character";
         Ddo_funcaoapfevidencia_tipoarq_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_tipoarq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_tipoarq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfevidencia_tipoarq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfevidencia_tipoarq_Cls = "ColumnSettings";
         Ddo_funcaoapfevidencia_tipoarq_Tooltip = "Op��es";
         Ddo_funcaoapfevidencia_tipoarq_Caption = "";
         Ddo_funcaoapfevidencia_nomearq_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfevidencia_nomearq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapfevidencia_nomearq_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfevidencia_nomearq_Loadingdata = "Carregando dados...";
         Ddo_funcaoapfevidencia_nomearq_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfevidencia_nomearq_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfevidencia_nomearq_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapfevidencia_nomearq_Datalistproc = "GetWWFuncaoAPFEvidenciaFilterData";
         Ddo_funcaoapfevidencia_nomearq_Datalisttype = "Dynamic";
         Ddo_funcaoapfevidencia_nomearq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_nomearq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapfevidencia_nomearq_Filtertype = "Character";
         Ddo_funcaoapfevidencia_nomearq_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_nomearq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_nomearq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfevidencia_nomearq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfevidencia_nomearq_Cls = "ColumnSettings";
         Ddo_funcaoapfevidencia_nomearq_Tooltip = "Op��es";
         Ddo_funcaoapfevidencia_nomearq_Caption = "";
         Ddo_funcaoapfevidencia_descricao_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfevidencia_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapfevidencia_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfevidencia_descricao_Loadingdata = "Carregando dados...";
         Ddo_funcaoapfevidencia_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfevidencia_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfevidencia_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapfevidencia_descricao_Datalistproc = "GetWWFuncaoAPFEvidenciaFilterData";
         Ddo_funcaoapfevidencia_descricao_Datalisttype = "Dynamic";
         Ddo_funcaoapfevidencia_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapfevidencia_descricao_Filtertype = "Character";
         Ddo_funcaoapfevidencia_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfevidencia_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfevidencia_descricao_Cls = "ColumnSettings";
         Ddo_funcaoapfevidencia_descricao_Tooltip = "Op��es";
         Ddo_funcaoapfevidencia_descricao_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Evidencia da Funcao de Transa��o";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV33FuncaoAPFEvidencia_DescricaoTitleFilterData',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV37FuncaoAPFEvidencia_NomeArqTitleFilterData',fld:'vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA',pic:'',nv:null},{av:'AV41FuncaoAPFEvidencia_TipoArqTitleFilterData',fld:'vFUNCAOAPFEVIDENCIA_TIPOARQTITLEFILTERDATA',pic:'',nv:null},{av:'AV45FuncaoAPFEvidencia_DataTitleFilterData',fld:'vFUNCAOAPFEVIDENCIA_DATATITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPFEvidencia_Descricao_Titleformat',ctrl:'FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'Titleformat'},{av:'edtFuncaoAPFEvidencia_Descricao_Title',ctrl:'FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'Title'},{av:'edtFuncaoAPFEvidencia_NomeArq_Titleformat',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Titleformat'},{av:'edtFuncaoAPFEvidencia_NomeArq_Title',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Title'},{av:'edtFuncaoAPFEvidencia_TipoArq_Titleformat',ctrl:'FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'Titleformat'},{av:'edtFuncaoAPFEvidencia_TipoArq_Title',ctrl:'FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'Title'},{av:'edtFuncaoAPFEvidencia_Data_Titleformat',ctrl:'FUNCAOAPFEVIDENCIA_DATA',prop:'Titleformat'},{av:'edtFuncaoAPFEvidencia_Data_Title',ctrl:'FUNCAOAPFEVIDENCIA_DATA',prop:'Title'},{av:'AV53GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV54GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11AD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FUNCAOAPFEVIDENCIA_DESCRICAO.ONOPTIONCLICKED","{handler:'E12AD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapfevidencia_descricao_Activeeventkey',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfevidencia_descricao_Filteredtext_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_funcaoapfevidencia_descricao_Selectedvalue_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfevidencia_descricao_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'SortedStatus'},{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_funcaoapfevidencia_nomearq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_funcaoapfevidencia_tipoarq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_funcaoapfevidencia_data_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPFEVIDENCIA_NOMEARQ.ONOPTIONCLICKED","{handler:'E13AD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapfevidencia_nomearq_Activeeventkey',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfevidencia_nomearq_Filteredtext_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'FilteredText_get'},{av:'Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfevidencia_nomearq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'SortedStatus'},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_funcaoapfevidencia_descricao_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_funcaoapfevidencia_tipoarq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_funcaoapfevidencia_data_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPFEVIDENCIA_TIPOARQ.ONOPTIONCLICKED","{handler:'E14AD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapfevidencia_tipoarq_Activeeventkey',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfevidencia_tipoarq_Filteredtext_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'FilteredText_get'},{av:'Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfevidencia_tipoarq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'SortedStatus'},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'Ddo_funcaoapfevidencia_descricao_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_funcaoapfevidencia_nomearq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_funcaoapfevidencia_data_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPFEVIDENCIA_DATA.ONOPTIONCLICKED","{handler:'E15AD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapfevidencia_data_Activeeventkey',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DATA',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfevidencia_data_Filteredtext_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DATA',prop:'FilteredText_get'},{av:'Ddo_funcaoapfevidencia_data_Filteredtextto_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfevidencia_data_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DATA',prop:'SortedStatus'},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_funcaoapfevidencia_descricao_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_funcaoapfevidencia_nomearq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_funcaoapfevidencia_tipoarq_Sortedstatus',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E29AD2',iparms:[{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtFuncaoAPFEvidencia_Descricao_Link',ctrl:'FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E16AD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22AD2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E17AD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavFuncaoapfevidencia_descricao2_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaoapfevidencia_descricao3_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavFuncaoapfevidencia_descricao1_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E23AD2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapfevidencia_descricao1_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E24AD2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E18AD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavFuncaoapfevidencia_descricao2_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaoapfevidencia_descricao3_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavFuncaoapfevidencia_descricao1_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E25AD2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapfevidencia_descricao2_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E19AD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavFuncaoapfevidencia_descricao2_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaoapfevidencia_descricao3_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavFuncaoapfevidencia_descricao1_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26AD2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapfevidencia_descricao3_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E20AD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV34TFFuncaoAPFEvidencia_Descricao',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'Ddo_funcaoapfevidencia_descricao_Filteredtext_set',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'FilteredText_set'},{av:'AV35TFFuncaoAPFEvidencia_Descricao_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_funcaoapfevidencia_descricao_Selectedvalue_set',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DESCRICAO',prop:'SelectedValue_set'},{av:'AV38TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'Ddo_funcaoapfevidencia_nomearq_Filteredtext_set',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'FilteredText_set'},{av:'AV39TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'SelectedValue_set'},{av:'AV42TFFuncaoAPFEvidencia_TipoArq',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'FilteredText_set'},{av:'AV43TFFuncaoAPFEvidencia_TipoArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set',ctrl:'DDO_FUNCAOAPFEVIDENCIA_TIPOARQ',prop:'SelectedValue_set'},{av:'AV46TFFuncaoAPFEvidencia_Data',fld:'vTFFUNCAOAPFEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_funcaoapfevidencia_data_Filteredtext_set',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DATA',prop:'FilteredText_set'},{av:'AV47TFFuncaoAPFEvidencia_Data_To',fld:'vTFFUNCAOAPFEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_funcaoapfevidencia_data_Filteredtextto_set',ctrl:'DDO_FUNCAOAPFEVIDENCIA_DATA',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPFEvidencia_Descricao1',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncaoapfevidencia_descricao1_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPFEvidencia_Descricao2',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPFEvidencia_Descricao3',fld:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavFuncaoapfevidencia_descricao2_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaoapfevidencia_descricao3_Visible',ctrl:'vFUNCAOAPFEVIDENCIA_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E21AD2',iparms:[{av:'A406FuncaoAPFEvidencia_Codigo',fld:'FUNCAOAPFEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_funcaoapfevidencia_descricao_Activeeventkey = "";
         Ddo_funcaoapfevidencia_descricao_Filteredtext_get = "";
         Ddo_funcaoapfevidencia_descricao_Selectedvalue_get = "";
         Ddo_funcaoapfevidencia_nomearq_Activeeventkey = "";
         Ddo_funcaoapfevidencia_nomearq_Filteredtext_get = "";
         Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get = "";
         Ddo_funcaoapfevidencia_tipoarq_Activeeventkey = "";
         Ddo_funcaoapfevidencia_tipoarq_Filteredtext_get = "";
         Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_get = "";
         Ddo_funcaoapfevidencia_data_Activeeventkey = "";
         Ddo_funcaoapfevidencia_data_Filteredtext_get = "";
         Ddo_funcaoapfevidencia_data_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17FuncaoAPFEvidencia_Descricao1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21FuncaoAPFEvidencia_Descricao2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25FuncaoAPFEvidencia_Descricao3 = "";
         AV34TFFuncaoAPFEvidencia_Descricao = "";
         AV35TFFuncaoAPFEvidencia_Descricao_Sel = "";
         AV38TFFuncaoAPFEvidencia_NomeArq = "";
         AV39TFFuncaoAPFEvidencia_NomeArq_Sel = "";
         AV42TFFuncaoAPFEvidencia_TipoArq = "";
         AV43TFFuncaoAPFEvidencia_TipoArq_Sel = "";
         AV46TFFuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         AV47TFFuncaoAPFEvidencia_Data_To = (DateTime)(DateTime.MinValue);
         AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace = "";
         AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace = "";
         AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace = "";
         AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace = "";
         AV78Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV51DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV33FuncaoAPFEvidencia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37FuncaoAPFEvidencia_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41FuncaoAPFEvidencia_TipoArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45FuncaoAPFEvidencia_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaoapfevidencia_descricao_Filteredtext_set = "";
         Ddo_funcaoapfevidencia_descricao_Selectedvalue_set = "";
         Ddo_funcaoapfevidencia_descricao_Sortedstatus = "";
         Ddo_funcaoapfevidencia_nomearq_Filteredtext_set = "";
         Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set = "";
         Ddo_funcaoapfevidencia_nomearq_Sortedstatus = "";
         Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set = "";
         Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set = "";
         Ddo_funcaoapfevidencia_tipoarq_Sortedstatus = "";
         Ddo_funcaoapfevidencia_data_Filteredtext_set = "";
         Ddo_funcaoapfevidencia_data_Filteredtextto_set = "";
         Ddo_funcaoapfevidencia_data_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV48DDO_FuncaoAPFEvidencia_DataAuxDate = DateTime.MinValue;
         AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV76Update_GXI = "";
         AV29Delete = "";
         AV77Delete_GXI = "";
         A407FuncaoAPFEvidencia_Descricao = "";
         A409FuncaoAPFEvidencia_NomeArq = "";
         A410FuncaoAPFEvidencia_TipoArq = "";
         A411FuncaoAPFEvidencia_Data = (DateTime)(DateTime.MinValue);
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = "";
         lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = "";
         lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = "";
         lV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = "";
         lV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = "";
         lV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = "";
         AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 = "";
         AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 = "";
         AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 = "";
         AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 = "";
         AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 = "";
         AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 = "";
         AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel = "";
         AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao = "";
         AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel = "";
         AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq = "";
         AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel = "";
         AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq = "";
         AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data = (DateTime)(DateTime.MinValue);
         AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to = (DateTime)(DateTime.MinValue);
         H00AD2_A360FuncaoAPF_SistemaCod = new int[1] ;
         H00AD2_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H00AD2_A411FuncaoAPFEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         H00AD2_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         H00AD2_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         H00AD2_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         H00AD2_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         H00AD2_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         H00AD2_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         H00AD2_A165FuncaoAPF_Codigo = new int[1] ;
         H00AD2_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         H00AD3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFuncaoapfevidenciatitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwfuncaoapfevidencia__default(),
            new Object[][] {
                new Object[] {
               H00AD2_A360FuncaoAPF_SistemaCod, H00AD2_n360FuncaoAPF_SistemaCod, H00AD2_A411FuncaoAPFEvidencia_Data, H00AD2_A410FuncaoAPFEvidencia_TipoArq, H00AD2_n410FuncaoAPFEvidencia_TipoArq, H00AD2_A409FuncaoAPFEvidencia_NomeArq, H00AD2_n409FuncaoAPFEvidencia_NomeArq, H00AD2_A407FuncaoAPFEvidencia_Descricao, H00AD2_n407FuncaoAPFEvidencia_Descricao, H00AD2_A165FuncaoAPF_Codigo,
               H00AD2_A406FuncaoAPFEvidencia_Codigo
               }
               , new Object[] {
               H00AD3_AGRID_nRecordCount
               }
            }
         );
         AV78Pgmname = "WWFuncaoAPFEvidencia";
         /* GeneXus formulas. */
         AV78Pgmname = "WWFuncaoAPFEvidencia";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_88 ;
      private short nGXsfl_88_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_88_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 ;
      private short AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 ;
      private short AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 ;
      private short edtFuncaoAPFEvidencia_Descricao_Titleformat ;
      private short edtFuncaoAPFEvidencia_NomeArq_Titleformat ;
      private short edtFuncaoAPFEvidencia_TipoArq_Titleformat ;
      private short edtFuncaoAPFEvidencia_Data_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A406FuncaoAPFEvidencia_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int A360FuncaoAPF_SistemaCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_funcaoapfevidencia_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_funcaoapfevidencia_nomearq_Datalistupdateminimumcharacters ;
      private int Ddo_funcaoapfevidencia_tipoarq_Datalistupdateminimumcharacters ;
      private int edtavTffuncaoapfevidencia_descricao_Visible ;
      private int edtavTffuncaoapfevidencia_descricao_sel_Visible ;
      private int edtavTffuncaoapfevidencia_nomearq_Visible ;
      private int edtavTffuncaoapfevidencia_nomearq_sel_Visible ;
      private int edtavTffuncaoapfevidencia_tipoarq_Visible ;
      private int edtavTffuncaoapfevidencia_tipoarq_sel_Visible ;
      private int edtavTffuncaoapfevidencia_data_Visible ;
      private int edtavTffuncaoapfevidencia_data_to_Visible ;
      private int edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapfevidencia_datatitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV52PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavFuncaoapfevidencia_descricao1_Visible ;
      private int edtavFuncaoapfevidencia_descricao2_Visible ;
      private int edtavFuncaoapfevidencia_descricao3_Visible ;
      private int AV79GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV53GridCurrentPage ;
      private long AV54GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_funcaoapfevidencia_descricao_Activeeventkey ;
      private String Ddo_funcaoapfevidencia_descricao_Filteredtext_get ;
      private String Ddo_funcaoapfevidencia_descricao_Selectedvalue_get ;
      private String Ddo_funcaoapfevidencia_nomearq_Activeeventkey ;
      private String Ddo_funcaoapfevidencia_nomearq_Filteredtext_get ;
      private String Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get ;
      private String Ddo_funcaoapfevidencia_tipoarq_Activeeventkey ;
      private String Ddo_funcaoapfevidencia_tipoarq_Filteredtext_get ;
      private String Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_get ;
      private String Ddo_funcaoapfevidencia_data_Activeeventkey ;
      private String Ddo_funcaoapfevidencia_data_Filteredtext_get ;
      private String Ddo_funcaoapfevidencia_data_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_88_idx="0001" ;
      private String AV38TFFuncaoAPFEvidencia_NomeArq ;
      private String AV39TFFuncaoAPFEvidencia_NomeArq_Sel ;
      private String AV42TFFuncaoAPFEvidencia_TipoArq ;
      private String AV43TFFuncaoAPFEvidencia_TipoArq_Sel ;
      private String AV78Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_funcaoapfevidencia_descricao_Caption ;
      private String Ddo_funcaoapfevidencia_descricao_Tooltip ;
      private String Ddo_funcaoapfevidencia_descricao_Cls ;
      private String Ddo_funcaoapfevidencia_descricao_Filteredtext_set ;
      private String Ddo_funcaoapfevidencia_descricao_Selectedvalue_set ;
      private String Ddo_funcaoapfevidencia_descricao_Dropdownoptionstype ;
      private String Ddo_funcaoapfevidencia_descricao_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfevidencia_descricao_Sortedstatus ;
      private String Ddo_funcaoapfevidencia_descricao_Filtertype ;
      private String Ddo_funcaoapfevidencia_descricao_Datalisttype ;
      private String Ddo_funcaoapfevidencia_descricao_Datalistproc ;
      private String Ddo_funcaoapfevidencia_descricao_Sortasc ;
      private String Ddo_funcaoapfevidencia_descricao_Sortdsc ;
      private String Ddo_funcaoapfevidencia_descricao_Loadingdata ;
      private String Ddo_funcaoapfevidencia_descricao_Cleanfilter ;
      private String Ddo_funcaoapfevidencia_descricao_Noresultsfound ;
      private String Ddo_funcaoapfevidencia_descricao_Searchbuttontext ;
      private String Ddo_funcaoapfevidencia_nomearq_Caption ;
      private String Ddo_funcaoapfevidencia_nomearq_Tooltip ;
      private String Ddo_funcaoapfevidencia_nomearq_Cls ;
      private String Ddo_funcaoapfevidencia_nomearq_Filteredtext_set ;
      private String Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set ;
      private String Ddo_funcaoapfevidencia_nomearq_Dropdownoptionstype ;
      private String Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfevidencia_nomearq_Sortedstatus ;
      private String Ddo_funcaoapfevidencia_nomearq_Filtertype ;
      private String Ddo_funcaoapfevidencia_nomearq_Datalisttype ;
      private String Ddo_funcaoapfevidencia_nomearq_Datalistproc ;
      private String Ddo_funcaoapfevidencia_nomearq_Sortasc ;
      private String Ddo_funcaoapfevidencia_nomearq_Sortdsc ;
      private String Ddo_funcaoapfevidencia_nomearq_Loadingdata ;
      private String Ddo_funcaoapfevidencia_nomearq_Cleanfilter ;
      private String Ddo_funcaoapfevidencia_nomearq_Noresultsfound ;
      private String Ddo_funcaoapfevidencia_nomearq_Searchbuttontext ;
      private String Ddo_funcaoapfevidencia_tipoarq_Caption ;
      private String Ddo_funcaoapfevidencia_tipoarq_Tooltip ;
      private String Ddo_funcaoapfevidencia_tipoarq_Cls ;
      private String Ddo_funcaoapfevidencia_tipoarq_Filteredtext_set ;
      private String Ddo_funcaoapfevidencia_tipoarq_Selectedvalue_set ;
      private String Ddo_funcaoapfevidencia_tipoarq_Dropdownoptionstype ;
      private String Ddo_funcaoapfevidencia_tipoarq_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfevidencia_tipoarq_Sortedstatus ;
      private String Ddo_funcaoapfevidencia_tipoarq_Filtertype ;
      private String Ddo_funcaoapfevidencia_tipoarq_Datalisttype ;
      private String Ddo_funcaoapfevidencia_tipoarq_Datalistproc ;
      private String Ddo_funcaoapfevidencia_tipoarq_Sortasc ;
      private String Ddo_funcaoapfevidencia_tipoarq_Sortdsc ;
      private String Ddo_funcaoapfevidencia_tipoarq_Loadingdata ;
      private String Ddo_funcaoapfevidencia_tipoarq_Cleanfilter ;
      private String Ddo_funcaoapfevidencia_tipoarq_Noresultsfound ;
      private String Ddo_funcaoapfevidencia_tipoarq_Searchbuttontext ;
      private String Ddo_funcaoapfevidencia_data_Caption ;
      private String Ddo_funcaoapfevidencia_data_Tooltip ;
      private String Ddo_funcaoapfevidencia_data_Cls ;
      private String Ddo_funcaoapfevidencia_data_Filteredtext_set ;
      private String Ddo_funcaoapfevidencia_data_Filteredtextto_set ;
      private String Ddo_funcaoapfevidencia_data_Dropdownoptionstype ;
      private String Ddo_funcaoapfevidencia_data_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfevidencia_data_Sortedstatus ;
      private String Ddo_funcaoapfevidencia_data_Filtertype ;
      private String Ddo_funcaoapfevidencia_data_Sortasc ;
      private String Ddo_funcaoapfevidencia_data_Sortdsc ;
      private String Ddo_funcaoapfevidencia_data_Cleanfilter ;
      private String Ddo_funcaoapfevidencia_data_Rangefilterfrom ;
      private String Ddo_funcaoapfevidencia_data_Rangefilterto ;
      private String Ddo_funcaoapfevidencia_data_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTffuncaoapfevidencia_descricao_Internalname ;
      private String edtavTffuncaoapfevidencia_descricao_sel_Internalname ;
      private String edtavTffuncaoapfevidencia_nomearq_Internalname ;
      private String edtavTffuncaoapfevidencia_nomearq_Jsonclick ;
      private String edtavTffuncaoapfevidencia_nomearq_sel_Internalname ;
      private String edtavTffuncaoapfevidencia_nomearq_sel_Jsonclick ;
      private String edtavTffuncaoapfevidencia_tipoarq_Internalname ;
      private String edtavTffuncaoapfevidencia_tipoarq_Jsonclick ;
      private String edtavTffuncaoapfevidencia_tipoarq_sel_Internalname ;
      private String edtavTffuncaoapfevidencia_tipoarq_sel_Jsonclick ;
      private String edtavTffuncaoapfevidencia_data_Internalname ;
      private String edtavTffuncaoapfevidencia_data_Jsonclick ;
      private String edtavTffuncaoapfevidencia_data_to_Internalname ;
      private String edtavTffuncaoapfevidencia_data_to_Jsonclick ;
      private String divDdo_funcaoapfevidencia_dataauxdates_Internalname ;
      private String edtavDdo_funcaoapfevidencia_dataauxdate_Internalname ;
      private String edtavDdo_funcaoapfevidencia_dataauxdate_Jsonclick ;
      private String edtavDdo_funcaoapfevidencia_dataauxdateto_Internalname ;
      private String edtavDdo_funcaoapfevidencia_dataauxdateto_Jsonclick ;
      private String edtavDdo_funcaoapfevidencia_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapfevidencia_tipoarqtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapfevidencia_datatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtFuncaoAPFEvidencia_Codigo_Internalname ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPFEvidencia_Descricao_Internalname ;
      private String A409FuncaoAPFEvidencia_NomeArq ;
      private String edtFuncaoAPFEvidencia_NomeArq_Internalname ;
      private String A410FuncaoAPFEvidencia_TipoArq ;
      private String edtFuncaoAPFEvidencia_TipoArq_Internalname ;
      private String edtFuncaoAPFEvidencia_Data_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ;
      private String lV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ;
      private String AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel ;
      private String AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ;
      private String AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel ;
      private String AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavFuncaoapfevidencia_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavFuncaoapfevidencia_descricao2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavFuncaoapfevidencia_descricao3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_funcaoapfevidencia_descricao_Internalname ;
      private String Ddo_funcaoapfevidencia_nomearq_Internalname ;
      private String Ddo_funcaoapfevidencia_tipoarq_Internalname ;
      private String Ddo_funcaoapfevidencia_data_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtFuncaoAPFEvidencia_Descricao_Title ;
      private String edtFuncaoAPFEvidencia_NomeArq_Title ;
      private String edtFuncaoAPFEvidencia_TipoArq_Title ;
      private String edtFuncaoAPFEvidencia_Data_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtFuncaoAPFEvidencia_Descricao_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblFuncaoapfevidenciatitle_Internalname ;
      private String lblFuncaoapfevidenciatitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_88_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFuncaoAPFEvidencia_Codigo_Jsonclick ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String edtFuncaoAPFEvidencia_Descricao_Jsonclick ;
      private String edtFuncaoAPFEvidencia_NomeArq_Jsonclick ;
      private String edtFuncaoAPFEvidencia_TipoArq_Jsonclick ;
      private String edtFuncaoAPFEvidencia_Data_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV46TFFuncaoAPFEvidencia_Data ;
      private DateTime AV47TFFuncaoAPFEvidencia_Data_To ;
      private DateTime A411FuncaoAPFEvidencia_Data ;
      private DateTime AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data ;
      private DateTime AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to ;
      private DateTime AV48DDO_FuncaoAPFEvidencia_DataAuxDate ;
      private DateTime AV49DDO_FuncaoAPFEvidencia_DataAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_funcaoapfevidencia_descricao_Includesortasc ;
      private bool Ddo_funcaoapfevidencia_descricao_Includesortdsc ;
      private bool Ddo_funcaoapfevidencia_descricao_Includefilter ;
      private bool Ddo_funcaoapfevidencia_descricao_Filterisrange ;
      private bool Ddo_funcaoapfevidencia_descricao_Includedatalist ;
      private bool Ddo_funcaoapfevidencia_nomearq_Includesortasc ;
      private bool Ddo_funcaoapfevidencia_nomearq_Includesortdsc ;
      private bool Ddo_funcaoapfevidencia_nomearq_Includefilter ;
      private bool Ddo_funcaoapfevidencia_nomearq_Filterisrange ;
      private bool Ddo_funcaoapfevidencia_nomearq_Includedatalist ;
      private bool Ddo_funcaoapfevidencia_tipoarq_Includesortasc ;
      private bool Ddo_funcaoapfevidencia_tipoarq_Includesortdsc ;
      private bool Ddo_funcaoapfevidencia_tipoarq_Includefilter ;
      private bool Ddo_funcaoapfevidencia_tipoarq_Filterisrange ;
      private bool Ddo_funcaoapfevidencia_tipoarq_Includedatalist ;
      private bool Ddo_funcaoapfevidencia_data_Includesortasc ;
      private bool Ddo_funcaoapfevidencia_data_Includesortdsc ;
      private bool Ddo_funcaoapfevidencia_data_Includefilter ;
      private bool Ddo_funcaoapfevidencia_data_Filterisrange ;
      private bool Ddo_funcaoapfevidencia_data_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n407FuncaoAPFEvidencia_Descricao ;
      private bool n409FuncaoAPFEvidencia_NomeArq ;
      private bool n410FuncaoAPFEvidencia_TipoArq ;
      private bool AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 ;
      private bool AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV17FuncaoAPFEvidencia_Descricao1 ;
      private String AV21FuncaoAPFEvidencia_Descricao2 ;
      private String AV25FuncaoAPFEvidencia_Descricao3 ;
      private String A407FuncaoAPFEvidencia_Descricao ;
      private String lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ;
      private String lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ;
      private String lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ;
      private String AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ;
      private String AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ;
      private String AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV34TFFuncaoAPFEvidencia_Descricao ;
      private String AV35TFFuncaoAPFEvidencia_Descricao_Sel ;
      private String AV36ddo_FuncaoAPFEvidencia_DescricaoTitleControlIdToReplace ;
      private String AV40ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace ;
      private String AV44ddo_FuncaoAPFEvidencia_TipoArqTitleControlIdToReplace ;
      private String AV50ddo_FuncaoAPFEvidencia_DataTitleControlIdToReplace ;
      private String AV76Update_GXI ;
      private String AV77Delete_GXI ;
      private String lV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ;
      private String AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 ;
      private String AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 ;
      private String AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 ;
      private String AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel ;
      private String AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00AD2_A360FuncaoAPF_SistemaCod ;
      private bool[] H00AD2_n360FuncaoAPF_SistemaCod ;
      private DateTime[] H00AD2_A411FuncaoAPFEvidencia_Data ;
      private String[] H00AD2_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] H00AD2_n410FuncaoAPFEvidencia_TipoArq ;
      private String[] H00AD2_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] H00AD2_n409FuncaoAPFEvidencia_NomeArq ;
      private String[] H00AD2_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] H00AD2_n407FuncaoAPFEvidencia_Descricao ;
      private int[] H00AD2_A165FuncaoAPF_Codigo ;
      private int[] H00AD2_A406FuncaoAPFEvidencia_Codigo ;
      private long[] H00AD3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33FuncaoAPFEvidencia_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37FuncaoAPFEvidencia_NomeArqTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV41FuncaoAPFEvidencia_TipoArqTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45FuncaoAPFEvidencia_DataTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV51DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwfuncaoapfevidencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00AD2( IGxContext context ,
                                             String AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ,
                                             bool AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 ,
                                             String AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 ,
                                             short AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 ,
                                             String AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ,
                                             bool AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 ,
                                             String AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 ,
                                             short AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 ,
                                             String AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ,
                                             String AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel ,
                                             String AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ,
                                             String AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel ,
                                             String AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ,
                                             String AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel ,
                                             String AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ,
                                             DateTime AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data ,
                                             DateTime AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to ,
                                             String A407FuncaoAPFEvidencia_Descricao ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             String A410FuncaoAPFEvidencia_TipoArq ,
                                             DateTime A411FuncaoAPFEvidencia_Data ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [19] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[FuncaoAPF_SistemaCod], T1.[FuncaoAPFEvidencia_Data], T1.[FuncaoAPFEvidencia_TipoArq], T1.[FuncaoAPFEvidencia_NomeArq], T1.[FuncaoAPFEvidencia_Descricao], T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFEvidencia_Codigo]";
         sFromString = " FROM ([FuncaoAPFEvidencia] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like @lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like @lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like '%' + @lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like '%' + @lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like @lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like @lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like '%' + @lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like '%' + @lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like @lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like @lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like '%' + @lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like '%' + @lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like @lV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like @lV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] = @AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] = @AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] like @lV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_NomeArq] like @lV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] = @AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_NomeArq] = @AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_TipoArq] like @lV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_TipoArq] like @lV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_TipoArq] = @AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_TipoArq] = @AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Data] >= @AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Data] >= @AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Data] <= @AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Data] <= @AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFEvidencia_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFEvidencia_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFEvidencia_NomeArq]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFEvidencia_NomeArq] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFEvidencia_TipoArq]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFEvidencia_TipoArq] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFEvidencia_Data]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFEvidencia_Data] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFEvidencia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00AD3( IGxContext context ,
                                             String AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1 ,
                                             bool AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 ,
                                             String AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2 ,
                                             short AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 ,
                                             String AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2 ,
                                             bool AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 ,
                                             String AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3 ,
                                             short AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 ,
                                             String AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3 ,
                                             String AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel ,
                                             String AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao ,
                                             String AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel ,
                                             String AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq ,
                                             String AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel ,
                                             String AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq ,
                                             DateTime AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data ,
                                             DateTime AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to ,
                                             String A407FuncaoAPFEvidencia_Descricao ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             String A410FuncaoAPFEvidencia_TipoArq ,
                                             DateTime A411FuncaoAPFEvidencia_Data ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [14] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([FuncaoAPFEvidencia] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         if ( ( StringUtil.StrCmp(AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like @lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like @lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWFuncaoAPFEvidenciaDS_1_Dynamicfiltersselector1, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV58WWFuncaoAPFEvidenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like '%' + @lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like '%' + @lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like @lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like @lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV60WWFuncaoAPFEvidenciaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWFuncaoAPFEvidenciaDS_5_Dynamicfiltersselector2, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV62WWFuncaoAPFEvidenciaDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like '%' + @lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like '%' + @lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like @lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like @lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV64WWFuncaoAPFEvidenciaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWFuncaoAPFEvidenciaDS_9_Dynamicfiltersselector3, "FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 ) && ( AV66WWFuncaoAPFEvidenciaDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like '%' + @lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like '%' + @lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] like @lV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] like @lV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Descricao] = @AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Descricao] = @AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] like @lV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_NomeArq] like @lV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] = @AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_NomeArq] = @AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_TipoArq] like @lV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_TipoArq] like @lV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_TipoArq] = @AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_TipoArq] = @AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Data] >= @AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Data] >= @AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_Data] <= @AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFEvidencia_Data] <= @AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00AD2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
               case 1 :
                     return conditional_H00AD3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AD2 ;
          prmH00AD2 = new Object[] {
          new Object[] {"@lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00AD3 ;
          prmH00AD3 = new Object[] {
          new Object[] {"@lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV59WWFuncaoAPFEvidenciaDS_3_Funcaoapfevidencia_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV63WWFuncaoAPFEvidenciaDS_7_Funcaoapfevidencia_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV67WWFuncaoAPFEvidenciaDS_11_Funcaoapfevidencia_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV68WWFuncaoAPFEvidenciaDS_12_Tffuncaoapfevidencia_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV69WWFuncaoAPFEvidenciaDS_13_Tffuncaoapfevidencia_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV70WWFuncaoAPFEvidenciaDS_14_Tffuncaoapfevidencia_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWFuncaoAPFEvidenciaDS_15_Tffuncaoapfevidencia_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWFuncaoAPFEvidenciaDS_16_Tffuncaoapfevidencia_tipoarq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV73WWFuncaoAPFEvidenciaDS_17_Tffuncaoapfevidencia_tipoarq_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV74WWFuncaoAPFEvidenciaDS_18_Tffuncaoapfevidencia_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV75WWFuncaoAPFEvidenciaDS_19_Tffuncaoapfevidencia_data_to",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AD2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AD2,11,0,true,false )
             ,new CursorDef("H00AD3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AD3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[27]);
                }
                return;
       }
    }

 }

}
