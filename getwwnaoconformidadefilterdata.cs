/*
               File: GetWWNaoConformidadeFilterData
        Description: Get WWNao Conformidade Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:53.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwnaoconformidadefilterdata : GXProcedure
   {
      public getwwnaoconformidadefilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwnaoconformidadefilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
         return AV26OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwnaoconformidadefilterdata objgetwwnaoconformidadefilterdata;
         objgetwwnaoconformidadefilterdata = new getwwnaoconformidadefilterdata();
         objgetwwnaoconformidadefilterdata.AV17DDOName = aP0_DDOName;
         objgetwwnaoconformidadefilterdata.AV15SearchTxt = aP1_SearchTxt;
         objgetwwnaoconformidadefilterdata.AV16SearchTxtTo = aP2_SearchTxtTo;
         objgetwwnaoconformidadefilterdata.AV21OptionsJson = "" ;
         objgetwwnaoconformidadefilterdata.AV24OptionsDescJson = "" ;
         objgetwwnaoconformidadefilterdata.AV26OptionIndexesJson = "" ;
         objgetwwnaoconformidadefilterdata.context.SetSubmitInitialConfig(context);
         objgetwwnaoconformidadefilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwnaoconformidadefilterdata);
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwnaoconformidadefilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV20Options = (IGxCollection)(new GxSimpleCollection());
         AV23OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV25OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV17DDOName), "DDO_NAOCONFORMIDADE_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADNAOCONFORMIDADE_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV21OptionsJson = AV20Options.ToJSonString(false);
         AV24OptionsDescJson = AV23OptionsDesc.ToJSonString(false);
         AV26OptionIndexesJson = AV25OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV28Session.Get("WWNaoConformidadeGridState"), "") == 0 )
         {
            AV30GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWNaoConformidadeGridState"), "");
         }
         else
         {
            AV30GridState.FromXml(AV28Session.Get("WWNaoConformidadeGridState"), "");
         }
         AV44GXV1 = 1;
         while ( AV44GXV1 <= AV30GridState.gxTpr_Filtervalues.Count )
         {
            AV31GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV30GridState.gxTpr_Filtervalues.Item(AV44GXV1));
            if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "NAOCONFORMIDADE_AREATRABALHOCOD") == 0 )
            {
               AV33NaoConformidade_AreaTrabalhoCod = (int)(NumberUtil.Val( AV31GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFNAOCONFORMIDADE_NOME") == 0 )
            {
               AV10TFNaoConformidade_Nome = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFNAOCONFORMIDADE_NOME_SEL") == 0 )
            {
               AV11TFNaoConformidade_Nome_Sel = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFNAOCONFORMIDADE_TIPO_SEL") == 0 )
            {
               AV12TFNaoConformidade_Tipo_SelsJson = AV31GridStateFilterValue.gxTpr_Value;
               AV13TFNaoConformidade_Tipo_Sels.FromJSonString(AV12TFNaoConformidade_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFNAOCONFORMIDADE_EHIMPEDITIVA_SEL") == 0 )
            {
               AV14TFNaoConformidade_EhImpeditiva_Sel = (short)(NumberUtil.Val( AV31GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFNAOCONFORMIDADE_GLOSAVEL_SEL") == 0 )
            {
               AV41TFNaoConformidade_Glosavel_Sel = (short)(NumberUtil.Val( AV31GridStateFilterValue.gxTpr_Value, "."));
            }
            AV44GXV1 = (int)(AV44GXV1+1);
         }
         if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV32GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "NAOCONFORMIDADE_NOME") == 0 )
            {
               AV35NaoConformidade_Nome1 = AV32GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "NAOCONFORMIDADE_TIPO") == 0 )
            {
               AV36NaoConformidade_Tipo1 = (short)(NumberUtil.Val( AV32GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV37DynamicFiltersEnabled2 = true;
               AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(2));
               AV38DynamicFiltersSelector2 = AV32GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "NAOCONFORMIDADE_NOME") == 0 )
               {
                  AV39NaoConformidade_Nome2 = AV32GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "NAOCONFORMIDADE_TIPO") == 0 )
               {
                  AV40NaoConformidade_Tipo2 = (short)(NumberUtil.Val( AV32GridStateDynamicFilter.gxTpr_Value, "."));
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADNAOCONFORMIDADE_NOMEOPTIONS' Routine */
         AV10TFNaoConformidade_Nome = AV15SearchTxt;
         AV11TFNaoConformidade_Nome_Sel = "";
         AV46WWNaoConformidadeDS_1_Naoconformidade_areatrabalhocod = AV33NaoConformidade_AreaTrabalhoCod;
         AV47WWNaoConformidadeDS_2_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV48WWNaoConformidadeDS_3_Naoconformidade_nome1 = AV35NaoConformidade_Nome1;
         AV49WWNaoConformidadeDS_4_Naoconformidade_tipo1 = AV36NaoConformidade_Tipo1;
         AV50WWNaoConformidadeDS_5_Dynamicfiltersenabled2 = AV37DynamicFiltersEnabled2;
         AV51WWNaoConformidadeDS_6_Dynamicfiltersselector2 = AV38DynamicFiltersSelector2;
         AV52WWNaoConformidadeDS_7_Naoconformidade_nome2 = AV39NaoConformidade_Nome2;
         AV53WWNaoConformidadeDS_8_Naoconformidade_tipo2 = AV40NaoConformidade_Tipo2;
         AV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome = AV10TFNaoConformidade_Nome;
         AV55WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel = AV11TFNaoConformidade_Nome_Sel;
         AV56WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels = AV13TFNaoConformidade_Tipo_Sels;
         AV57WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel = AV14TFNaoConformidade_EhImpeditiva_Sel;
         AV58WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel = AV41TFNaoConformidade_Glosavel_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A429NaoConformidade_Tipo ,
                                              AV56WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels ,
                                              AV47WWNaoConformidadeDS_2_Dynamicfiltersselector1 ,
                                              AV48WWNaoConformidadeDS_3_Naoconformidade_nome1 ,
                                              AV49WWNaoConformidadeDS_4_Naoconformidade_tipo1 ,
                                              AV50WWNaoConformidadeDS_5_Dynamicfiltersenabled2 ,
                                              AV51WWNaoConformidadeDS_6_Dynamicfiltersselector2 ,
                                              AV52WWNaoConformidadeDS_7_Naoconformidade_nome2 ,
                                              AV53WWNaoConformidadeDS_8_Naoconformidade_tipo2 ,
                                              AV55WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel ,
                                              AV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome ,
                                              AV56WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels.Count ,
                                              AV57WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel ,
                                              AV58WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel ,
                                              A427NaoConformidade_Nome ,
                                              A1121NaoConformidade_EhImpeditiva ,
                                              A2029NaoConformidade_Glosavel ,
                                              A428NaoConformidade_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV48WWNaoConformidadeDS_3_Naoconformidade_nome1 = StringUtil.PadR( StringUtil.RTrim( AV48WWNaoConformidadeDS_3_Naoconformidade_nome1), 50, "%");
         lV52WWNaoConformidadeDS_7_Naoconformidade_nome2 = StringUtil.PadR( StringUtil.RTrim( AV52WWNaoConformidadeDS_7_Naoconformidade_nome2), 50, "%");
         lV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome = StringUtil.PadR( StringUtil.RTrim( AV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome), 50, "%");
         /* Using cursor P00M52 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV48WWNaoConformidadeDS_3_Naoconformidade_nome1, AV49WWNaoConformidadeDS_4_Naoconformidade_tipo1, lV52WWNaoConformidadeDS_7_Naoconformidade_nome2, AV53WWNaoConformidadeDS_8_Naoconformidade_tipo2, lV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome, AV55WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKM52 = false;
            A428NaoConformidade_AreaTrabalhoCod = P00M52_A428NaoConformidade_AreaTrabalhoCod[0];
            A427NaoConformidade_Nome = P00M52_A427NaoConformidade_Nome[0];
            A2029NaoConformidade_Glosavel = P00M52_A2029NaoConformidade_Glosavel[0];
            n2029NaoConformidade_Glosavel = P00M52_n2029NaoConformidade_Glosavel[0];
            A1121NaoConformidade_EhImpeditiva = P00M52_A1121NaoConformidade_EhImpeditiva[0];
            n1121NaoConformidade_EhImpeditiva = P00M52_n1121NaoConformidade_EhImpeditiva[0];
            A429NaoConformidade_Tipo = P00M52_A429NaoConformidade_Tipo[0];
            A426NaoConformidade_Codigo = P00M52_A426NaoConformidade_Codigo[0];
            AV27count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00M52_A427NaoConformidade_Nome[0], A427NaoConformidade_Nome) == 0 ) )
            {
               BRKM52 = false;
               A426NaoConformidade_Codigo = P00M52_A426NaoConformidade_Codigo[0];
               AV27count = (long)(AV27count+1);
               BRKM52 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A427NaoConformidade_Nome)) )
            {
               AV19Option = A427NaoConformidade_Nome;
               AV20Options.Add(AV19Option, 0);
               AV25OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV27count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV20Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKM52 )
            {
               BRKM52 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV20Options = new GxSimpleCollection();
         AV23OptionsDesc = new GxSimpleCollection();
         AV25OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV28Session = context.GetSession();
         AV30GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV31GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFNaoConformidade_Nome = "";
         AV11TFNaoConformidade_Nome_Sel = "";
         AV12TFNaoConformidade_Tipo_SelsJson = "";
         AV13TFNaoConformidade_Tipo_Sels = new GxSimpleCollection();
         AV32GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV35NaoConformidade_Nome1 = "";
         AV38DynamicFiltersSelector2 = "";
         AV39NaoConformidade_Nome2 = "";
         AV47WWNaoConformidadeDS_2_Dynamicfiltersselector1 = "";
         AV48WWNaoConformidadeDS_3_Naoconformidade_nome1 = "";
         AV51WWNaoConformidadeDS_6_Dynamicfiltersselector2 = "";
         AV52WWNaoConformidadeDS_7_Naoconformidade_nome2 = "";
         AV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome = "";
         AV55WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel = "";
         AV56WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV48WWNaoConformidadeDS_3_Naoconformidade_nome1 = "";
         lV52WWNaoConformidadeDS_7_Naoconformidade_nome2 = "";
         lV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome = "";
         A427NaoConformidade_Nome = "";
         P00M52_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         P00M52_A427NaoConformidade_Nome = new String[] {""} ;
         P00M52_A2029NaoConformidade_Glosavel = new bool[] {false} ;
         P00M52_n2029NaoConformidade_Glosavel = new bool[] {false} ;
         P00M52_A1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         P00M52_n1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         P00M52_A429NaoConformidade_Tipo = new short[1] ;
         P00M52_A426NaoConformidade_Codigo = new int[1] ;
         AV19Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwnaoconformidadefilterdata__default(),
            new Object[][] {
                new Object[] {
               P00M52_A428NaoConformidade_AreaTrabalhoCod, P00M52_A427NaoConformidade_Nome, P00M52_A2029NaoConformidade_Glosavel, P00M52_n2029NaoConformidade_Glosavel, P00M52_A1121NaoConformidade_EhImpeditiva, P00M52_n1121NaoConformidade_EhImpeditiva, P00M52_A429NaoConformidade_Tipo, P00M52_A426NaoConformidade_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFNaoConformidade_EhImpeditiva_Sel ;
      private short AV41TFNaoConformidade_Glosavel_Sel ;
      private short AV36NaoConformidade_Tipo1 ;
      private short AV40NaoConformidade_Tipo2 ;
      private short AV49WWNaoConformidadeDS_4_Naoconformidade_tipo1 ;
      private short AV53WWNaoConformidadeDS_8_Naoconformidade_tipo2 ;
      private short AV57WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel ;
      private short AV58WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel ;
      private short A429NaoConformidade_Tipo ;
      private int AV44GXV1 ;
      private int AV33NaoConformidade_AreaTrabalhoCod ;
      private int AV46WWNaoConformidadeDS_1_Naoconformidade_areatrabalhocod ;
      private int AV56WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A428NaoConformidade_AreaTrabalhoCod ;
      private int A426NaoConformidade_Codigo ;
      private long AV27count ;
      private String AV10TFNaoConformidade_Nome ;
      private String AV11TFNaoConformidade_Nome_Sel ;
      private String AV35NaoConformidade_Nome1 ;
      private String AV39NaoConformidade_Nome2 ;
      private String AV48WWNaoConformidadeDS_3_Naoconformidade_nome1 ;
      private String AV52WWNaoConformidadeDS_7_Naoconformidade_nome2 ;
      private String AV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome ;
      private String AV55WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel ;
      private String scmdbuf ;
      private String lV48WWNaoConformidadeDS_3_Naoconformidade_nome1 ;
      private String lV52WWNaoConformidadeDS_7_Naoconformidade_nome2 ;
      private String lV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome ;
      private String A427NaoConformidade_Nome ;
      private bool returnInSub ;
      private bool AV37DynamicFiltersEnabled2 ;
      private bool AV50WWNaoConformidadeDS_5_Dynamicfiltersenabled2 ;
      private bool A1121NaoConformidade_EhImpeditiva ;
      private bool A2029NaoConformidade_Glosavel ;
      private bool BRKM52 ;
      private bool n2029NaoConformidade_Glosavel ;
      private bool n1121NaoConformidade_EhImpeditiva ;
      private String AV26OptionIndexesJson ;
      private String AV21OptionsJson ;
      private String AV24OptionsDescJson ;
      private String AV12TFNaoConformidade_Tipo_SelsJson ;
      private String AV17DDOName ;
      private String AV15SearchTxt ;
      private String AV16SearchTxtTo ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV38DynamicFiltersSelector2 ;
      private String AV47WWNaoConformidadeDS_2_Dynamicfiltersselector1 ;
      private String AV51WWNaoConformidadeDS_6_Dynamicfiltersselector2 ;
      private String AV19Option ;
      private IGxSession AV28Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00M52_A428NaoConformidade_AreaTrabalhoCod ;
      private String[] P00M52_A427NaoConformidade_Nome ;
      private bool[] P00M52_A2029NaoConformidade_Glosavel ;
      private bool[] P00M52_n2029NaoConformidade_Glosavel ;
      private bool[] P00M52_A1121NaoConformidade_EhImpeditiva ;
      private bool[] P00M52_n1121NaoConformidade_EhImpeditiva ;
      private short[] P00M52_A429NaoConformidade_Tipo ;
      private int[] P00M52_A426NaoConformidade_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV13TFNaoConformidade_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV56WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV30GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV31GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV32GridStateDynamicFilter ;
   }

   public class getwwnaoconformidadefilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00M52( IGxContext context ,
                                             short A429NaoConformidade_Tipo ,
                                             IGxCollection AV56WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels ,
                                             String AV47WWNaoConformidadeDS_2_Dynamicfiltersselector1 ,
                                             String AV48WWNaoConformidadeDS_3_Naoconformidade_nome1 ,
                                             short AV49WWNaoConformidadeDS_4_Naoconformidade_tipo1 ,
                                             bool AV50WWNaoConformidadeDS_5_Dynamicfiltersenabled2 ,
                                             String AV51WWNaoConformidadeDS_6_Dynamicfiltersselector2 ,
                                             String AV52WWNaoConformidadeDS_7_Naoconformidade_nome2 ,
                                             short AV53WWNaoConformidadeDS_8_Naoconformidade_tipo2 ,
                                             String AV55WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel ,
                                             String AV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome ,
                                             int AV56WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels_Count ,
                                             short AV57WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel ,
                                             short AV58WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel ,
                                             String A427NaoConformidade_Nome ,
                                             bool A1121NaoConformidade_EhImpeditiva ,
                                             bool A2029NaoConformidade_Glosavel ,
                                             int A428NaoConformidade_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [NaoConformidade_AreaTrabalhoCod], [NaoConformidade_Nome], [NaoConformidade_Glosavel], [NaoConformidade_EhImpeditiva], [NaoConformidade_Tipo], [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([NaoConformidade_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV47WWNaoConformidadeDS_2_Dynamicfiltersselector1, "NAOCONFORMIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWNaoConformidadeDS_3_Naoconformidade_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like '%' + @lV48WWNaoConformidadeDS_3_Naoconformidade_nome1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWNaoConformidadeDS_2_Dynamicfiltersselector1, "NAOCONFORMIDADE_TIPO") == 0 ) && ( ! (0==AV49WWNaoConformidadeDS_4_Naoconformidade_tipo1) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Tipo] = @AV49WWNaoConformidadeDS_4_Naoconformidade_tipo1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV50WWNaoConformidadeDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWNaoConformidadeDS_6_Dynamicfiltersselector2, "NAOCONFORMIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWNaoConformidadeDS_7_Naoconformidade_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like '%' + @lV52WWNaoConformidadeDS_7_Naoconformidade_nome2)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV50WWNaoConformidadeDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWNaoConformidadeDS_6_Dynamicfiltersselector2, "NAOCONFORMIDADE_TIPO") == 0 ) && ( ! (0==AV53WWNaoConformidadeDS_8_Naoconformidade_tipo2) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Tipo] = @AV53WWNaoConformidadeDS_8_Naoconformidade_tipo2)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV55WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome)) ) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] like @lV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Nome] = @AV55WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV56WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV56WWNaoConformidadeDS_11_Tfnaoconformidade_tipo_sels, "[NaoConformidade_Tipo] IN (", ")") + ")";
         }
         if ( AV57WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel == 1 )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_EhImpeditiva] = 1)";
         }
         if ( AV57WWNaoConformidadeDS_12_Tfnaoconformidade_ehimpeditiva_sel == 2 )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_EhImpeditiva] = 0)";
         }
         if ( AV58WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel == 1 )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Glosavel] = 1)";
         }
         if ( AV58WWNaoConformidadeDS_13_Tfnaoconformidade_glosavel_sel == 2 )
         {
            sWhereString = sWhereString + " and ([NaoConformidade_Glosavel] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [NaoConformidade_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00M52(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (bool)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00M52 ;
          prmP00M52 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV48WWNaoConformidadeDS_3_Naoconformidade_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV49WWNaoConformidadeDS_4_Naoconformidade_tipo1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV52WWNaoConformidadeDS_7_Naoconformidade_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53WWNaoConformidadeDS_8_Naoconformidade_tipo2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV54WWNaoConformidadeDS_9_Tfnaoconformidade_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWNaoConformidadeDS_10_Tfnaoconformidade_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00M52", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00M52,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwnaoconformidadefilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwnaoconformidadefilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwnaoconformidadefilterdata") )
          {
             return  ;
          }
          getwwnaoconformidadefilterdata worker = new getwwnaoconformidadefilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
