/*
               File: type_SdtRequisito
        Description: Requisito
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:32:19.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Requisito" )]
   [XmlType(TypeName =  "Requisito" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtRequisito : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtRequisito( )
      {
         /* Constructor for serialization */
         gxTv_SdtRequisito_Requisito_identificador = "";
         gxTv_SdtRequisito_Requisito_titulo = "";
         gxTv_SdtRequisito_Requisito_descricao = "";
         gxTv_SdtRequisito_Proposta_objetivo = "";
         gxTv_SdtRequisito_Requisito_referenciatecnica = "";
         gxTv_SdtRequisito_Requisito_agrupador = "";
         gxTv_SdtRequisito_Requisito_restricao = "";
         gxTv_SdtRequisito_Requisito_datahomologacao = DateTime.MinValue;
         gxTv_SdtRequisito_Requisito_cadastro = (DateTime)(DateTime.MinValue);
         gxTv_SdtRequisito_Mode = "";
         gxTv_SdtRequisito_Requisito_identificador_Z = "";
         gxTv_SdtRequisito_Requisito_titulo_Z = "";
         gxTv_SdtRequisito_Requisito_agrupador_Z = "";
         gxTv_SdtRequisito_Requisito_datahomologacao_Z = DateTime.MinValue;
         gxTv_SdtRequisito_Requisito_cadastro_Z = (DateTime)(DateTime.MinValue);
      }

      public SdtRequisito( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1919Requisito_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1919Requisito_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Requisito_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Requisito");
         metadata.Set("BT", "Requisito");
         metadata.Set("PK", "[ \"Requisito_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Requisito_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Proposta_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Requisito_Codigo\" ],\"FKMap\":[ \"Requisito_ReqCod-Requisito_Codigo\" ] },{ \"FK\":[ \"TipoRequisito_Codigo\" ],\"FKMap\":[ \"Requisito_TipoReqCod-TipoRequisito_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_identificador_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_titulo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_tiporeqcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_agrupador_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_ordem_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_pontuacao_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_datahomologacao_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_status_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_cadastro_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_reqcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_prioridade_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_identificador_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_titulo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_tiporeqcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_referenciatecnica_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_agrupador_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_restricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_ordem_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_pontuacao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_datahomologacao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_cadastro_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_reqcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_prioridade_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtRequisito deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtRequisito)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtRequisito obj ;
         obj = this;
         obj.gxTpr_Requisito_codigo = deserialized.gxTpr_Requisito_codigo;
         obj.gxTpr_Requisito_identificador = deserialized.gxTpr_Requisito_identificador;
         obj.gxTpr_Requisito_titulo = deserialized.gxTpr_Requisito_titulo;
         obj.gxTpr_Requisito_descricao = deserialized.gxTpr_Requisito_descricao;
         obj.gxTpr_Proposta_codigo = deserialized.gxTpr_Proposta_codigo;
         obj.gxTpr_Proposta_objetivo = deserialized.gxTpr_Proposta_objetivo;
         obj.gxTpr_Requisito_tiporeqcod = deserialized.gxTpr_Requisito_tiporeqcod;
         obj.gxTpr_Requisito_referenciatecnica = deserialized.gxTpr_Requisito_referenciatecnica;
         obj.gxTpr_Requisito_agrupador = deserialized.gxTpr_Requisito_agrupador;
         obj.gxTpr_Requisito_restricao = deserialized.gxTpr_Requisito_restricao;
         obj.gxTpr_Requisito_ordem = deserialized.gxTpr_Requisito_ordem;
         obj.gxTpr_Requisito_pontuacao = deserialized.gxTpr_Requisito_pontuacao;
         obj.gxTpr_Requisito_datahomologacao = deserialized.gxTpr_Requisito_datahomologacao;
         obj.gxTpr_Requisito_status = deserialized.gxTpr_Requisito_status;
         obj.gxTpr_Requisito_cadastro = deserialized.gxTpr_Requisito_cadastro;
         obj.gxTpr_Requisito_reqcod = deserialized.gxTpr_Requisito_reqcod;
         obj.gxTpr_Requisito_prioridade = deserialized.gxTpr_Requisito_prioridade;
         obj.gxTpr_Requisito_ativo = deserialized.gxTpr_Requisito_ativo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Requisito_codigo_Z = deserialized.gxTpr_Requisito_codigo_Z;
         obj.gxTpr_Requisito_identificador_Z = deserialized.gxTpr_Requisito_identificador_Z;
         obj.gxTpr_Requisito_titulo_Z = deserialized.gxTpr_Requisito_titulo_Z;
         obj.gxTpr_Proposta_codigo_Z = deserialized.gxTpr_Proposta_codigo_Z;
         obj.gxTpr_Requisito_tiporeqcod_Z = deserialized.gxTpr_Requisito_tiporeqcod_Z;
         obj.gxTpr_Requisito_agrupador_Z = deserialized.gxTpr_Requisito_agrupador_Z;
         obj.gxTpr_Requisito_ordem_Z = deserialized.gxTpr_Requisito_ordem_Z;
         obj.gxTpr_Requisito_pontuacao_Z = deserialized.gxTpr_Requisito_pontuacao_Z;
         obj.gxTpr_Requisito_datahomologacao_Z = deserialized.gxTpr_Requisito_datahomologacao_Z;
         obj.gxTpr_Requisito_status_Z = deserialized.gxTpr_Requisito_status_Z;
         obj.gxTpr_Requisito_cadastro_Z = deserialized.gxTpr_Requisito_cadastro_Z;
         obj.gxTpr_Requisito_reqcod_Z = deserialized.gxTpr_Requisito_reqcod_Z;
         obj.gxTpr_Requisito_prioridade_Z = deserialized.gxTpr_Requisito_prioridade_Z;
         obj.gxTpr_Requisito_ativo_Z = deserialized.gxTpr_Requisito_ativo_Z;
         obj.gxTpr_Requisito_identificador_N = deserialized.gxTpr_Requisito_identificador_N;
         obj.gxTpr_Requisito_titulo_N = deserialized.gxTpr_Requisito_titulo_N;
         obj.gxTpr_Requisito_descricao_N = deserialized.gxTpr_Requisito_descricao_N;
         obj.gxTpr_Proposta_codigo_N = deserialized.gxTpr_Proposta_codigo_N;
         obj.gxTpr_Requisito_tiporeqcod_N = deserialized.gxTpr_Requisito_tiporeqcod_N;
         obj.gxTpr_Requisito_referenciatecnica_N = deserialized.gxTpr_Requisito_referenciatecnica_N;
         obj.gxTpr_Requisito_agrupador_N = deserialized.gxTpr_Requisito_agrupador_N;
         obj.gxTpr_Requisito_restricao_N = deserialized.gxTpr_Requisito_restricao_N;
         obj.gxTpr_Requisito_ordem_N = deserialized.gxTpr_Requisito_ordem_N;
         obj.gxTpr_Requisito_pontuacao_N = deserialized.gxTpr_Requisito_pontuacao_N;
         obj.gxTpr_Requisito_datahomologacao_N = deserialized.gxTpr_Requisito_datahomologacao_N;
         obj.gxTpr_Requisito_cadastro_N = deserialized.gxTpr_Requisito_cadastro_N;
         obj.gxTpr_Requisito_reqcod_N = deserialized.gxTpr_Requisito_reqcod_N;
         obj.gxTpr_Requisito_prioridade_N = deserialized.gxTpr_Requisito_prioridade_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Codigo") )
               {
                  gxTv_SdtRequisito_Requisito_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Identificador") )
               {
                  gxTv_SdtRequisito_Requisito_identificador = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Titulo") )
               {
                  gxTv_SdtRequisito_Requisito_titulo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Descricao") )
               {
                  gxTv_SdtRequisito_Requisito_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Codigo") )
               {
                  gxTv_SdtRequisito_Proposta_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Objetivo") )
               {
                  gxTv_SdtRequisito_Proposta_objetivo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_TipoReqCod") )
               {
                  gxTv_SdtRequisito_Requisito_tiporeqcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_ReferenciaTecnica") )
               {
                  gxTv_SdtRequisito_Requisito_referenciatecnica = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Agrupador") )
               {
                  gxTv_SdtRequisito_Requisito_agrupador = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Restricao") )
               {
                  gxTv_SdtRequisito_Requisito_restricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Ordem") )
               {
                  gxTv_SdtRequisito_Requisito_ordem = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Pontuacao") )
               {
                  gxTv_SdtRequisito_Requisito_pontuacao = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_DataHomologacao") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtRequisito_Requisito_datahomologacao = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtRequisito_Requisito_datahomologacao = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Status") )
               {
                  gxTv_SdtRequisito_Requisito_status = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Cadastro") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtRequisito_Requisito_cadastro = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtRequisito_Requisito_cadastro = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_ReqCod") )
               {
                  gxTv_SdtRequisito_Requisito_reqcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Prioridade") )
               {
                  gxTv_SdtRequisito_Requisito_prioridade = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Ativo") )
               {
                  gxTv_SdtRequisito_Requisito_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtRequisito_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtRequisito_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Codigo_Z") )
               {
                  gxTv_SdtRequisito_Requisito_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Identificador_Z") )
               {
                  gxTv_SdtRequisito_Requisito_identificador_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Titulo_Z") )
               {
                  gxTv_SdtRequisito_Requisito_titulo_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Codigo_Z") )
               {
                  gxTv_SdtRequisito_Proposta_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_TipoReqCod_Z") )
               {
                  gxTv_SdtRequisito_Requisito_tiporeqcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Agrupador_Z") )
               {
                  gxTv_SdtRequisito_Requisito_agrupador_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Ordem_Z") )
               {
                  gxTv_SdtRequisito_Requisito_ordem_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Pontuacao_Z") )
               {
                  gxTv_SdtRequisito_Requisito_pontuacao_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_DataHomologacao_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtRequisito_Requisito_datahomologacao_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtRequisito_Requisito_datahomologacao_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Status_Z") )
               {
                  gxTv_SdtRequisito_Requisito_status_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Cadastro_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtRequisito_Requisito_cadastro_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtRequisito_Requisito_cadastro_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_ReqCod_Z") )
               {
                  gxTv_SdtRequisito_Requisito_reqcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Prioridade_Z") )
               {
                  gxTv_SdtRequisito_Requisito_prioridade_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Ativo_Z") )
               {
                  gxTv_SdtRequisito_Requisito_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Identificador_N") )
               {
                  gxTv_SdtRequisito_Requisito_identificador_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Titulo_N") )
               {
                  gxTv_SdtRequisito_Requisito_titulo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Descricao_N") )
               {
                  gxTv_SdtRequisito_Requisito_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Codigo_N") )
               {
                  gxTv_SdtRequisito_Proposta_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_TipoReqCod_N") )
               {
                  gxTv_SdtRequisito_Requisito_tiporeqcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_ReferenciaTecnica_N") )
               {
                  gxTv_SdtRequisito_Requisito_referenciatecnica_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Agrupador_N") )
               {
                  gxTv_SdtRequisito_Requisito_agrupador_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Restricao_N") )
               {
                  gxTv_SdtRequisito_Requisito_restricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Ordem_N") )
               {
                  gxTv_SdtRequisito_Requisito_ordem_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Pontuacao_N") )
               {
                  gxTv_SdtRequisito_Requisito_pontuacao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_DataHomologacao_N") )
               {
                  gxTv_SdtRequisito_Requisito_datahomologacao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Cadastro_N") )
               {
                  gxTv_SdtRequisito_Requisito_cadastro_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_ReqCod_N") )
               {
                  gxTv_SdtRequisito_Requisito_reqcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Prioridade_N") )
               {
                  gxTv_SdtRequisito_Requisito_prioridade_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Requisito";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Requisito_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Identificador", StringUtil.RTrim( gxTv_SdtRequisito_Requisito_identificador));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Titulo", StringUtil.RTrim( gxTv_SdtRequisito_Requisito_titulo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Descricao", StringUtil.RTrim( gxTv_SdtRequisito_Requisito_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Proposta_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Proposta_codigo), 9, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Proposta_Objetivo", StringUtil.RTrim( gxTv_SdtRequisito_Proposta_objetivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_TipoReqCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_tiporeqcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_ReferenciaTecnica", StringUtil.RTrim( gxTv_SdtRequisito_Requisito_referenciatecnica));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Agrupador", StringUtil.RTrim( gxTv_SdtRequisito_Requisito_agrupador));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Restricao", StringUtil.RTrim( gxTv_SdtRequisito_Requisito_restricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Ordem", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_ordem), 3, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Pontuacao", StringUtil.Trim( StringUtil.Str( gxTv_SdtRequisito_Requisito_pontuacao, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtRequisito_Requisito_datahomologacao) )
         {
            oWriter.WriteStartElement("Requisito_DataHomologacao");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtRequisito_Requisito_datahomologacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtRequisito_Requisito_datahomologacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtRequisito_Requisito_datahomologacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Requisito_DataHomologacao", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("Requisito_Status", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_status), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtRequisito_Requisito_cadastro) )
         {
            oWriter.WriteStartElement("Requisito_Cadastro");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtRequisito_Requisito_cadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtRequisito_Requisito_cadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtRequisito_Requisito_cadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtRequisito_Requisito_cadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtRequisito_Requisito_cadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtRequisito_Requisito_cadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Requisito_Cadastro", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("Requisito_ReqCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_reqcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Prioridade", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_prioridade), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Requisito_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtRequisito_Requisito_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtRequisito_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Identificador_Z", StringUtil.RTrim( gxTv_SdtRequisito_Requisito_identificador_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Titulo_Z", StringUtil.RTrim( gxTv_SdtRequisito_Requisito_titulo_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Proposta_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Proposta_codigo_Z), 9, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_TipoReqCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_tiporeqcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Agrupador_Z", StringUtil.RTrim( gxTv_SdtRequisito_Requisito_agrupador_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Ordem_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_ordem_Z), 3, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Pontuacao_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtRequisito_Requisito_pontuacao_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtRequisito_Requisito_datahomologacao_Z) )
            {
               oWriter.WriteStartElement("Requisito_DataHomologacao_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtRequisito_Requisito_datahomologacao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtRequisito_Requisito_datahomologacao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtRequisito_Requisito_datahomologacao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Requisito_DataHomologacao_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("Requisito_Status_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_status_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtRequisito_Requisito_cadastro_Z) )
            {
               oWriter.WriteStartElement("Requisito_Cadastro_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtRequisito_Requisito_cadastro_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtRequisito_Requisito_cadastro_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtRequisito_Requisito_cadastro_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtRequisito_Requisito_cadastro_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtRequisito_Requisito_cadastro_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtRequisito_Requisito_cadastro_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Requisito_Cadastro_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("Requisito_ReqCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_reqcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Prioridade_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_prioridade_Z), 2, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtRequisito_Requisito_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Identificador_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_identificador_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Titulo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_titulo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Proposta_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Proposta_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_TipoReqCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_tiporeqcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_ReferenciaTecnica_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_referenciatecnica_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Agrupador_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_agrupador_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Restricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_restricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Ordem_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_ordem_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Pontuacao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_pontuacao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_DataHomologacao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_datahomologacao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Cadastro_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_cadastro_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_ReqCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_reqcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Requisito_Prioridade_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRequisito_Requisito_prioridade_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Requisito_Codigo", gxTv_SdtRequisito_Requisito_codigo, false);
         AddObjectProperty("Requisito_Identificador", gxTv_SdtRequisito_Requisito_identificador, false);
         AddObjectProperty("Requisito_Titulo", gxTv_SdtRequisito_Requisito_titulo, false);
         AddObjectProperty("Requisito_Descricao", gxTv_SdtRequisito_Requisito_descricao, false);
         AddObjectProperty("Proposta_Codigo", gxTv_SdtRequisito_Proposta_codigo, false);
         AddObjectProperty("Proposta_Objetivo", gxTv_SdtRequisito_Proposta_objetivo, false);
         AddObjectProperty("Requisito_TipoReqCod", gxTv_SdtRequisito_Requisito_tiporeqcod, false);
         AddObjectProperty("Requisito_ReferenciaTecnica", gxTv_SdtRequisito_Requisito_referenciatecnica, false);
         AddObjectProperty("Requisito_Agrupador", gxTv_SdtRequisito_Requisito_agrupador, false);
         AddObjectProperty("Requisito_Restricao", gxTv_SdtRequisito_Requisito_restricao, false);
         AddObjectProperty("Requisito_Ordem", gxTv_SdtRequisito_Requisito_ordem, false);
         AddObjectProperty("Requisito_Pontuacao", gxTv_SdtRequisito_Requisito_pontuacao, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtRequisito_Requisito_datahomologacao)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtRequisito_Requisito_datahomologacao)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtRequisito_Requisito_datahomologacao)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Requisito_DataHomologacao", sDateCnv, false);
         AddObjectProperty("Requisito_Status", gxTv_SdtRequisito_Requisito_status, false);
         datetime_STZ = gxTv_SdtRequisito_Requisito_cadastro;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Requisito_Cadastro", sDateCnv, false);
         AddObjectProperty("Requisito_ReqCod", gxTv_SdtRequisito_Requisito_reqcod, false);
         AddObjectProperty("Requisito_Prioridade", gxTv_SdtRequisito_Requisito_prioridade, false);
         AddObjectProperty("Requisito_Ativo", gxTv_SdtRequisito_Requisito_ativo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtRequisito_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtRequisito_Initialized, false);
            AddObjectProperty("Requisito_Codigo_Z", gxTv_SdtRequisito_Requisito_codigo_Z, false);
            AddObjectProperty("Requisito_Identificador_Z", gxTv_SdtRequisito_Requisito_identificador_Z, false);
            AddObjectProperty("Requisito_Titulo_Z", gxTv_SdtRequisito_Requisito_titulo_Z, false);
            AddObjectProperty("Proposta_Codigo_Z", gxTv_SdtRequisito_Proposta_codigo_Z, false);
            AddObjectProperty("Requisito_TipoReqCod_Z", gxTv_SdtRequisito_Requisito_tiporeqcod_Z, false);
            AddObjectProperty("Requisito_Agrupador_Z", gxTv_SdtRequisito_Requisito_agrupador_Z, false);
            AddObjectProperty("Requisito_Ordem_Z", gxTv_SdtRequisito_Requisito_ordem_Z, false);
            AddObjectProperty("Requisito_Pontuacao_Z", gxTv_SdtRequisito_Requisito_pontuacao_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtRequisito_Requisito_datahomologacao_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtRequisito_Requisito_datahomologacao_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtRequisito_Requisito_datahomologacao_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Requisito_DataHomologacao_Z", sDateCnv, false);
            AddObjectProperty("Requisito_Status_Z", gxTv_SdtRequisito_Requisito_status_Z, false);
            datetime_STZ = gxTv_SdtRequisito_Requisito_cadastro_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Requisito_Cadastro_Z", sDateCnv, false);
            AddObjectProperty("Requisito_ReqCod_Z", gxTv_SdtRequisito_Requisito_reqcod_Z, false);
            AddObjectProperty("Requisito_Prioridade_Z", gxTv_SdtRequisito_Requisito_prioridade_Z, false);
            AddObjectProperty("Requisito_Ativo_Z", gxTv_SdtRequisito_Requisito_ativo_Z, false);
            AddObjectProperty("Requisito_Identificador_N", gxTv_SdtRequisito_Requisito_identificador_N, false);
            AddObjectProperty("Requisito_Titulo_N", gxTv_SdtRequisito_Requisito_titulo_N, false);
            AddObjectProperty("Requisito_Descricao_N", gxTv_SdtRequisito_Requisito_descricao_N, false);
            AddObjectProperty("Proposta_Codigo_N", gxTv_SdtRequisito_Proposta_codigo_N, false);
            AddObjectProperty("Requisito_TipoReqCod_N", gxTv_SdtRequisito_Requisito_tiporeqcod_N, false);
            AddObjectProperty("Requisito_ReferenciaTecnica_N", gxTv_SdtRequisito_Requisito_referenciatecnica_N, false);
            AddObjectProperty("Requisito_Agrupador_N", gxTv_SdtRequisito_Requisito_agrupador_N, false);
            AddObjectProperty("Requisito_Restricao_N", gxTv_SdtRequisito_Requisito_restricao_N, false);
            AddObjectProperty("Requisito_Ordem_N", gxTv_SdtRequisito_Requisito_ordem_N, false);
            AddObjectProperty("Requisito_Pontuacao_N", gxTv_SdtRequisito_Requisito_pontuacao_N, false);
            AddObjectProperty("Requisito_DataHomologacao_N", gxTv_SdtRequisito_Requisito_datahomologacao_N, false);
            AddObjectProperty("Requisito_Cadastro_N", gxTv_SdtRequisito_Requisito_cadastro_N, false);
            AddObjectProperty("Requisito_ReqCod_N", gxTv_SdtRequisito_Requisito_reqcod_N, false);
            AddObjectProperty("Requisito_Prioridade_N", gxTv_SdtRequisito_Requisito_prioridade_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Requisito_Codigo" )]
      [  XmlElement( ElementName = "Requisito_Codigo"   )]
      public int gxTpr_Requisito_codigo
      {
         get {
            return gxTv_SdtRequisito_Requisito_codigo ;
         }

         set {
            if ( gxTv_SdtRequisito_Requisito_codigo != value )
            {
               gxTv_SdtRequisito_Mode = "INS";
               this.gxTv_SdtRequisito_Requisito_codigo_Z_SetNull( );
               this.gxTv_SdtRequisito_Requisito_identificador_Z_SetNull( );
               this.gxTv_SdtRequisito_Requisito_titulo_Z_SetNull( );
               this.gxTv_SdtRequisito_Proposta_codigo_Z_SetNull( );
               this.gxTv_SdtRequisito_Requisito_tiporeqcod_Z_SetNull( );
               this.gxTv_SdtRequisito_Requisito_agrupador_Z_SetNull( );
               this.gxTv_SdtRequisito_Requisito_ordem_Z_SetNull( );
               this.gxTv_SdtRequisito_Requisito_pontuacao_Z_SetNull( );
               this.gxTv_SdtRequisito_Requisito_datahomologacao_Z_SetNull( );
               this.gxTv_SdtRequisito_Requisito_status_Z_SetNull( );
               this.gxTv_SdtRequisito_Requisito_cadastro_Z_SetNull( );
               this.gxTv_SdtRequisito_Requisito_reqcod_Z_SetNull( );
               this.gxTv_SdtRequisito_Requisito_prioridade_Z_SetNull( );
               this.gxTv_SdtRequisito_Requisito_ativo_Z_SetNull( );
            }
            gxTv_SdtRequisito_Requisito_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Identificador" )]
      [  XmlElement( ElementName = "Requisito_Identificador"   )]
      public String gxTpr_Requisito_identificador
      {
         get {
            return gxTv_SdtRequisito_Requisito_identificador ;
         }

         set {
            gxTv_SdtRequisito_Requisito_identificador_N = 0;
            gxTv_SdtRequisito_Requisito_identificador = (String)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_identificador_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_identificador_N = 1;
         gxTv_SdtRequisito_Requisito_identificador = "";
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_identificador_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Titulo" )]
      [  XmlElement( ElementName = "Requisito_Titulo"   )]
      public String gxTpr_Requisito_titulo
      {
         get {
            return gxTv_SdtRequisito_Requisito_titulo ;
         }

         set {
            gxTv_SdtRequisito_Requisito_titulo_N = 0;
            gxTv_SdtRequisito_Requisito_titulo = (String)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_titulo_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_titulo_N = 1;
         gxTv_SdtRequisito_Requisito_titulo = "";
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_titulo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Descricao" )]
      [  XmlElement( ElementName = "Requisito_Descricao"   )]
      public String gxTpr_Requisito_descricao
      {
         get {
            return gxTv_SdtRequisito_Requisito_descricao ;
         }

         set {
            gxTv_SdtRequisito_Requisito_descricao_N = 0;
            gxTv_SdtRequisito_Requisito_descricao = (String)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_descricao_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_descricao_N = 1;
         gxTv_SdtRequisito_Requisito_descricao = "";
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Codigo" )]
      [  XmlElement( ElementName = "Proposta_Codigo"   )]
      public int gxTpr_Proposta_codigo
      {
         get {
            return gxTv_SdtRequisito_Proposta_codigo ;
         }

         set {
            gxTv_SdtRequisito_Proposta_codigo_N = 0;
            gxTv_SdtRequisito_Proposta_codigo = (int)(value);
         }

      }

      public void gxTv_SdtRequisito_Proposta_codigo_SetNull( )
      {
         gxTv_SdtRequisito_Proposta_codigo_N = 1;
         gxTv_SdtRequisito_Proposta_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Proposta_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Objetivo" )]
      [  XmlElement( ElementName = "Proposta_Objetivo"   )]
      public String gxTpr_Proposta_objetivo
      {
         get {
            return gxTv_SdtRequisito_Proposta_objetivo ;
         }

         set {
            gxTv_SdtRequisito_Proposta_objetivo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_TipoReqCod" )]
      [  XmlElement( ElementName = "Requisito_TipoReqCod"   )]
      public int gxTpr_Requisito_tiporeqcod
      {
         get {
            return gxTv_SdtRequisito_Requisito_tiporeqcod ;
         }

         set {
            gxTv_SdtRequisito_Requisito_tiporeqcod_N = 0;
            gxTv_SdtRequisito_Requisito_tiporeqcod = (int)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_tiporeqcod_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_tiporeqcod_N = 1;
         gxTv_SdtRequisito_Requisito_tiporeqcod = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_tiporeqcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_ReferenciaTecnica" )]
      [  XmlElement( ElementName = "Requisito_ReferenciaTecnica"   )]
      public String gxTpr_Requisito_referenciatecnica
      {
         get {
            return gxTv_SdtRequisito_Requisito_referenciatecnica ;
         }

         set {
            gxTv_SdtRequisito_Requisito_referenciatecnica_N = 0;
            gxTv_SdtRequisito_Requisito_referenciatecnica = (String)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_referenciatecnica_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_referenciatecnica_N = 1;
         gxTv_SdtRequisito_Requisito_referenciatecnica = "";
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_referenciatecnica_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Agrupador" )]
      [  XmlElement( ElementName = "Requisito_Agrupador"   )]
      public String gxTpr_Requisito_agrupador
      {
         get {
            return gxTv_SdtRequisito_Requisito_agrupador ;
         }

         set {
            gxTv_SdtRequisito_Requisito_agrupador_N = 0;
            gxTv_SdtRequisito_Requisito_agrupador = (String)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_agrupador_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_agrupador_N = 1;
         gxTv_SdtRequisito_Requisito_agrupador = "";
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_agrupador_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Restricao" )]
      [  XmlElement( ElementName = "Requisito_Restricao"   )]
      public String gxTpr_Requisito_restricao
      {
         get {
            return gxTv_SdtRequisito_Requisito_restricao ;
         }

         set {
            gxTv_SdtRequisito_Requisito_restricao_N = 0;
            gxTv_SdtRequisito_Requisito_restricao = (String)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_restricao_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_restricao_N = 1;
         gxTv_SdtRequisito_Requisito_restricao = "";
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_restricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Ordem" )]
      [  XmlElement( ElementName = "Requisito_Ordem"   )]
      public short gxTpr_Requisito_ordem
      {
         get {
            return gxTv_SdtRequisito_Requisito_ordem ;
         }

         set {
            gxTv_SdtRequisito_Requisito_ordem_N = 0;
            gxTv_SdtRequisito_Requisito_ordem = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_ordem_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_ordem_N = 1;
         gxTv_SdtRequisito_Requisito_ordem = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_ordem_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Pontuacao" )]
      [  XmlElement( ElementName = "Requisito_Pontuacao"   )]
      public double gxTpr_Requisito_pontuacao_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtRequisito_Requisito_pontuacao) ;
         }

         set {
            gxTv_SdtRequisito_Requisito_pontuacao_N = 0;
            gxTv_SdtRequisito_Requisito_pontuacao = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Requisito_pontuacao
      {
         get {
            return gxTv_SdtRequisito_Requisito_pontuacao ;
         }

         set {
            gxTv_SdtRequisito_Requisito_pontuacao_N = 0;
            gxTv_SdtRequisito_Requisito_pontuacao = (decimal)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_pontuacao_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_pontuacao_N = 1;
         gxTv_SdtRequisito_Requisito_pontuacao = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_pontuacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_DataHomologacao" )]
      [  XmlElement( ElementName = "Requisito_DataHomologacao"  , IsNullable=true )]
      public string gxTpr_Requisito_datahomologacao_Nullable
      {
         get {
            if ( gxTv_SdtRequisito_Requisito_datahomologacao == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtRequisito_Requisito_datahomologacao).value ;
         }

         set {
            gxTv_SdtRequisito_Requisito_datahomologacao_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtRequisito_Requisito_datahomologacao = DateTime.MinValue;
            else
               gxTv_SdtRequisito_Requisito_datahomologacao = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Requisito_datahomologacao
      {
         get {
            return gxTv_SdtRequisito_Requisito_datahomologacao ;
         }

         set {
            gxTv_SdtRequisito_Requisito_datahomologacao_N = 0;
            gxTv_SdtRequisito_Requisito_datahomologacao = (DateTime)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_datahomologacao_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_datahomologacao_N = 1;
         gxTv_SdtRequisito_Requisito_datahomologacao = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_datahomologacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Status" )]
      [  XmlElement( ElementName = "Requisito_Status"   )]
      public short gxTpr_Requisito_status
      {
         get {
            return gxTv_SdtRequisito_Requisito_status ;
         }

         set {
            gxTv_SdtRequisito_Requisito_status = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Cadastro" )]
      [  XmlElement( ElementName = "Requisito_Cadastro"  , IsNullable=true )]
      public string gxTpr_Requisito_cadastro_Nullable
      {
         get {
            if ( gxTv_SdtRequisito_Requisito_cadastro == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtRequisito_Requisito_cadastro).value ;
         }

         set {
            gxTv_SdtRequisito_Requisito_cadastro_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtRequisito_Requisito_cadastro = DateTime.MinValue;
            else
               gxTv_SdtRequisito_Requisito_cadastro = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Requisito_cadastro
      {
         get {
            return gxTv_SdtRequisito_Requisito_cadastro ;
         }

         set {
            gxTv_SdtRequisito_Requisito_cadastro_N = 0;
            gxTv_SdtRequisito_Requisito_cadastro = (DateTime)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_cadastro_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_cadastro_N = 1;
         gxTv_SdtRequisito_Requisito_cadastro = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_cadastro_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_ReqCod" )]
      [  XmlElement( ElementName = "Requisito_ReqCod"   )]
      public int gxTpr_Requisito_reqcod
      {
         get {
            return gxTv_SdtRequisito_Requisito_reqcod ;
         }

         set {
            gxTv_SdtRequisito_Requisito_reqcod_N = 0;
            gxTv_SdtRequisito_Requisito_reqcod = (int)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_reqcod_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_reqcod_N = 1;
         gxTv_SdtRequisito_Requisito_reqcod = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_reqcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Prioridade" )]
      [  XmlElement( ElementName = "Requisito_Prioridade"   )]
      public short gxTpr_Requisito_prioridade
      {
         get {
            return gxTv_SdtRequisito_Requisito_prioridade ;
         }

         set {
            gxTv_SdtRequisito_Requisito_prioridade_N = 0;
            gxTv_SdtRequisito_Requisito_prioridade = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_prioridade_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_prioridade_N = 1;
         gxTv_SdtRequisito_Requisito_prioridade = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_prioridade_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Ativo" )]
      [  XmlElement( ElementName = "Requisito_Ativo"   )]
      public bool gxTpr_Requisito_ativo
      {
         get {
            return gxTv_SdtRequisito_Requisito_ativo ;
         }

         set {
            gxTv_SdtRequisito_Requisito_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtRequisito_Mode ;
         }

         set {
            gxTv_SdtRequisito_Mode = (String)(value);
         }

      }

      public void gxTv_SdtRequisito_Mode_SetNull( )
      {
         gxTv_SdtRequisito_Mode = "";
         return  ;
      }

      public bool gxTv_SdtRequisito_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtRequisito_Initialized ;
         }

         set {
            gxTv_SdtRequisito_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Initialized_SetNull( )
      {
         gxTv_SdtRequisito_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Codigo_Z" )]
      [  XmlElement( ElementName = "Requisito_Codigo_Z"   )]
      public int gxTpr_Requisito_codigo_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_codigo_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_codigo_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Identificador_Z" )]
      [  XmlElement( ElementName = "Requisito_Identificador_Z"   )]
      public String gxTpr_Requisito_identificador_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_identificador_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_identificador_Z = (String)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_identificador_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_identificador_Z = "";
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_identificador_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Titulo_Z" )]
      [  XmlElement( ElementName = "Requisito_Titulo_Z"   )]
      public String gxTpr_Requisito_titulo_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_titulo_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_titulo_Z = (String)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_titulo_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_titulo_Z = "";
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_titulo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Codigo_Z" )]
      [  XmlElement( ElementName = "Proposta_Codigo_Z"   )]
      public int gxTpr_Proposta_codigo_Z
      {
         get {
            return gxTv_SdtRequisito_Proposta_codigo_Z ;
         }

         set {
            gxTv_SdtRequisito_Proposta_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtRequisito_Proposta_codigo_Z_SetNull( )
      {
         gxTv_SdtRequisito_Proposta_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Proposta_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_TipoReqCod_Z" )]
      [  XmlElement( ElementName = "Requisito_TipoReqCod_Z"   )]
      public int gxTpr_Requisito_tiporeqcod_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_tiporeqcod_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_tiporeqcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_tiporeqcod_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_tiporeqcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_tiporeqcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Agrupador_Z" )]
      [  XmlElement( ElementName = "Requisito_Agrupador_Z"   )]
      public String gxTpr_Requisito_agrupador_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_agrupador_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_agrupador_Z = (String)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_agrupador_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_agrupador_Z = "";
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_agrupador_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Ordem_Z" )]
      [  XmlElement( ElementName = "Requisito_Ordem_Z"   )]
      public short gxTpr_Requisito_ordem_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_ordem_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_ordem_Z = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_ordem_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_ordem_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_ordem_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Pontuacao_Z" )]
      [  XmlElement( ElementName = "Requisito_Pontuacao_Z"   )]
      public double gxTpr_Requisito_pontuacao_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtRequisito_Requisito_pontuacao_Z) ;
         }

         set {
            gxTv_SdtRequisito_Requisito_pontuacao_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Requisito_pontuacao_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_pontuacao_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_pontuacao_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_pontuacao_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_pontuacao_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_pontuacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_DataHomologacao_Z" )]
      [  XmlElement( ElementName = "Requisito_DataHomologacao_Z"  , IsNullable=true )]
      public string gxTpr_Requisito_datahomologacao_Z_Nullable
      {
         get {
            if ( gxTv_SdtRequisito_Requisito_datahomologacao_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtRequisito_Requisito_datahomologacao_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtRequisito_Requisito_datahomologacao_Z = DateTime.MinValue;
            else
               gxTv_SdtRequisito_Requisito_datahomologacao_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Requisito_datahomologacao_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_datahomologacao_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_datahomologacao_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_datahomologacao_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_datahomologacao_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_datahomologacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Status_Z" )]
      [  XmlElement( ElementName = "Requisito_Status_Z"   )]
      public short gxTpr_Requisito_status_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_status_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_status_Z = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_status_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_status_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_status_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Cadastro_Z" )]
      [  XmlElement( ElementName = "Requisito_Cadastro_Z"  , IsNullable=true )]
      public string gxTpr_Requisito_cadastro_Z_Nullable
      {
         get {
            if ( gxTv_SdtRequisito_Requisito_cadastro_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtRequisito_Requisito_cadastro_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtRequisito_Requisito_cadastro_Z = DateTime.MinValue;
            else
               gxTv_SdtRequisito_Requisito_cadastro_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Requisito_cadastro_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_cadastro_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_cadastro_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_cadastro_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_cadastro_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_cadastro_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_ReqCod_Z" )]
      [  XmlElement( ElementName = "Requisito_ReqCod_Z"   )]
      public int gxTpr_Requisito_reqcod_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_reqcod_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_reqcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_reqcod_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_reqcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_reqcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Prioridade_Z" )]
      [  XmlElement( ElementName = "Requisito_Prioridade_Z"   )]
      public short gxTpr_Requisito_prioridade_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_prioridade_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_prioridade_Z = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_prioridade_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_prioridade_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_prioridade_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Ativo_Z" )]
      [  XmlElement( ElementName = "Requisito_Ativo_Z"   )]
      public bool gxTpr_Requisito_ativo_Z
      {
         get {
            return gxTv_SdtRequisito_Requisito_ativo_Z ;
         }

         set {
            gxTv_SdtRequisito_Requisito_ativo_Z = value;
         }

      }

      public void gxTv_SdtRequisito_Requisito_ativo_Z_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Identificador_N" )]
      [  XmlElement( ElementName = "Requisito_Identificador_N"   )]
      public short gxTpr_Requisito_identificador_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_identificador_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_identificador_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_identificador_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_identificador_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_identificador_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Titulo_N" )]
      [  XmlElement( ElementName = "Requisito_Titulo_N"   )]
      public short gxTpr_Requisito_titulo_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_titulo_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_titulo_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_titulo_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_titulo_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_titulo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Descricao_N" )]
      [  XmlElement( ElementName = "Requisito_Descricao_N"   )]
      public short gxTpr_Requisito_descricao_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_descricao_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_descricao_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Codigo_N" )]
      [  XmlElement( ElementName = "Proposta_Codigo_N"   )]
      public short gxTpr_Proposta_codigo_N
      {
         get {
            return gxTv_SdtRequisito_Proposta_codigo_N ;
         }

         set {
            gxTv_SdtRequisito_Proposta_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Proposta_codigo_N_SetNull( )
      {
         gxTv_SdtRequisito_Proposta_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Proposta_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_TipoReqCod_N" )]
      [  XmlElement( ElementName = "Requisito_TipoReqCod_N"   )]
      public short gxTpr_Requisito_tiporeqcod_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_tiporeqcod_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_tiporeqcod_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_tiporeqcod_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_tiporeqcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_tiporeqcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_ReferenciaTecnica_N" )]
      [  XmlElement( ElementName = "Requisito_ReferenciaTecnica_N"   )]
      public short gxTpr_Requisito_referenciatecnica_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_referenciatecnica_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_referenciatecnica_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_referenciatecnica_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_referenciatecnica_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_referenciatecnica_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Agrupador_N" )]
      [  XmlElement( ElementName = "Requisito_Agrupador_N"   )]
      public short gxTpr_Requisito_agrupador_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_agrupador_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_agrupador_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_agrupador_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_agrupador_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_agrupador_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Restricao_N" )]
      [  XmlElement( ElementName = "Requisito_Restricao_N"   )]
      public short gxTpr_Requisito_restricao_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_restricao_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_restricao_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_restricao_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_restricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_restricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Ordem_N" )]
      [  XmlElement( ElementName = "Requisito_Ordem_N"   )]
      public short gxTpr_Requisito_ordem_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_ordem_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_ordem_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_ordem_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_ordem_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_ordem_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Pontuacao_N" )]
      [  XmlElement( ElementName = "Requisito_Pontuacao_N"   )]
      public short gxTpr_Requisito_pontuacao_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_pontuacao_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_pontuacao_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_pontuacao_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_pontuacao_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_pontuacao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_DataHomologacao_N" )]
      [  XmlElement( ElementName = "Requisito_DataHomologacao_N"   )]
      public short gxTpr_Requisito_datahomologacao_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_datahomologacao_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_datahomologacao_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_datahomologacao_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_datahomologacao_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_datahomologacao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Cadastro_N" )]
      [  XmlElement( ElementName = "Requisito_Cadastro_N"   )]
      public short gxTpr_Requisito_cadastro_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_cadastro_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_cadastro_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_cadastro_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_cadastro_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_cadastro_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_ReqCod_N" )]
      [  XmlElement( ElementName = "Requisito_ReqCod_N"   )]
      public short gxTpr_Requisito_reqcod_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_reqcod_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_reqcod_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_reqcod_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_reqcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_reqcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Prioridade_N" )]
      [  XmlElement( ElementName = "Requisito_Prioridade_N"   )]
      public short gxTpr_Requisito_prioridade_N
      {
         get {
            return gxTv_SdtRequisito_Requisito_prioridade_N ;
         }

         set {
            gxTv_SdtRequisito_Requisito_prioridade_N = (short)(value);
         }

      }

      public void gxTv_SdtRequisito_Requisito_prioridade_N_SetNull( )
      {
         gxTv_SdtRequisito_Requisito_prioridade_N = 0;
         return  ;
      }

      public bool gxTv_SdtRequisito_Requisito_prioridade_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtRequisito_Requisito_identificador = "";
         gxTv_SdtRequisito_Requisito_titulo = "";
         gxTv_SdtRequisito_Requisito_descricao = "";
         gxTv_SdtRequisito_Proposta_objetivo = "";
         gxTv_SdtRequisito_Requisito_referenciatecnica = "";
         gxTv_SdtRequisito_Requisito_agrupador = "";
         gxTv_SdtRequisito_Requisito_restricao = "";
         gxTv_SdtRequisito_Requisito_datahomologacao = DateTime.MinValue;
         gxTv_SdtRequisito_Requisito_cadastro = DateTimeUtil.ServerNow( (IGxContext)(context), "DEFAULT");
         gxTv_SdtRequisito_Mode = "";
         gxTv_SdtRequisito_Requisito_identificador_Z = "";
         gxTv_SdtRequisito_Requisito_titulo_Z = "";
         gxTv_SdtRequisito_Requisito_agrupador_Z = "";
         gxTv_SdtRequisito_Requisito_datahomologacao_Z = DateTime.MinValue;
         gxTv_SdtRequisito_Requisito_cadastro_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtRequisito_Requisito_ativo = true;
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "requisito", "GeneXus.Programs.requisito_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtRequisito_Requisito_ordem ;
      private short gxTv_SdtRequisito_Requisito_status ;
      private short gxTv_SdtRequisito_Requisito_prioridade ;
      private short gxTv_SdtRequisito_Initialized ;
      private short gxTv_SdtRequisito_Requisito_ordem_Z ;
      private short gxTv_SdtRequisito_Requisito_status_Z ;
      private short gxTv_SdtRequisito_Requisito_prioridade_Z ;
      private short gxTv_SdtRequisito_Requisito_identificador_N ;
      private short gxTv_SdtRequisito_Requisito_titulo_N ;
      private short gxTv_SdtRequisito_Requisito_descricao_N ;
      private short gxTv_SdtRequisito_Proposta_codigo_N ;
      private short gxTv_SdtRequisito_Requisito_tiporeqcod_N ;
      private short gxTv_SdtRequisito_Requisito_referenciatecnica_N ;
      private short gxTv_SdtRequisito_Requisito_agrupador_N ;
      private short gxTv_SdtRequisito_Requisito_restricao_N ;
      private short gxTv_SdtRequisito_Requisito_ordem_N ;
      private short gxTv_SdtRequisito_Requisito_pontuacao_N ;
      private short gxTv_SdtRequisito_Requisito_datahomologacao_N ;
      private short gxTv_SdtRequisito_Requisito_cadastro_N ;
      private short gxTv_SdtRequisito_Requisito_reqcod_N ;
      private short gxTv_SdtRequisito_Requisito_prioridade_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtRequisito_Requisito_codigo ;
      private int gxTv_SdtRequisito_Proposta_codigo ;
      private int gxTv_SdtRequisito_Requisito_tiporeqcod ;
      private int gxTv_SdtRequisito_Requisito_reqcod ;
      private int gxTv_SdtRequisito_Requisito_codigo_Z ;
      private int gxTv_SdtRequisito_Proposta_codigo_Z ;
      private int gxTv_SdtRequisito_Requisito_tiporeqcod_Z ;
      private int gxTv_SdtRequisito_Requisito_reqcod_Z ;
      private decimal gxTv_SdtRequisito_Requisito_pontuacao ;
      private decimal gxTv_SdtRequisito_Requisito_pontuacao_Z ;
      private String gxTv_SdtRequisito_Mode ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtRequisito_Requisito_cadastro ;
      private DateTime gxTv_SdtRequisito_Requisito_cadastro_Z ;
      private DateTime datetime_STZ ;
      private DateTime gxTv_SdtRequisito_Requisito_datahomologacao ;
      private DateTime gxTv_SdtRequisito_Requisito_datahomologacao_Z ;
      private bool gxTv_SdtRequisito_Requisito_ativo ;
      private bool gxTv_SdtRequisito_Requisito_ativo_Z ;
      private String gxTv_SdtRequisito_Requisito_descricao ;
      private String gxTv_SdtRequisito_Proposta_objetivo ;
      private String gxTv_SdtRequisito_Requisito_referenciatecnica ;
      private String gxTv_SdtRequisito_Requisito_restricao ;
      private String gxTv_SdtRequisito_Requisito_identificador ;
      private String gxTv_SdtRequisito_Requisito_titulo ;
      private String gxTv_SdtRequisito_Requisito_agrupador ;
      private String gxTv_SdtRequisito_Requisito_identificador_Z ;
      private String gxTv_SdtRequisito_Requisito_titulo_Z ;
      private String gxTv_SdtRequisito_Requisito_agrupador_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Requisito", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtRequisito_RESTInterface : GxGenericCollectionItem<SdtRequisito>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtRequisito_RESTInterface( ) : base()
      {
      }

      public SdtRequisito_RESTInterface( SdtRequisito psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Requisito_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Requisito_codigo
      {
         get {
            return sdt.gxTpr_Requisito_codigo ;
         }

         set {
            sdt.gxTpr_Requisito_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_Identificador" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Requisito_identificador
      {
         get {
            return sdt.gxTpr_Requisito_identificador ;
         }

         set {
            sdt.gxTpr_Requisito_identificador = (String)(value);
         }

      }

      [DataMember( Name = "Requisito_Titulo" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Requisito_titulo
      {
         get {
            return sdt.gxTpr_Requisito_titulo ;
         }

         set {
            sdt.gxTpr_Requisito_titulo = (String)(value);
         }

      }

      [DataMember( Name = "Requisito_Descricao" , Order = 3 )]
      public String gxTpr_Requisito_descricao
      {
         get {
            return sdt.gxTpr_Requisito_descricao ;
         }

         set {
            sdt.gxTpr_Requisito_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Proposta_Codigo" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Proposta_codigo
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Proposta_codigo), 9, 0)) ;
         }

         set {
            sdt.gxTpr_Proposta_codigo = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Proposta_Objetivo" , Order = 5 )]
      public String gxTpr_Proposta_objetivo
      {
         get {
            return sdt.gxTpr_Proposta_objetivo ;
         }

         set {
            sdt.gxTpr_Proposta_objetivo = (String)(value);
         }

      }

      [DataMember( Name = "Requisito_TipoReqCod" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Requisito_tiporeqcod
      {
         get {
            return sdt.gxTpr_Requisito_tiporeqcod ;
         }

         set {
            sdt.gxTpr_Requisito_tiporeqcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_ReferenciaTecnica" , Order = 7 )]
      public String gxTpr_Requisito_referenciatecnica
      {
         get {
            return sdt.gxTpr_Requisito_referenciatecnica ;
         }

         set {
            sdt.gxTpr_Requisito_referenciatecnica = (String)(value);
         }

      }

      [DataMember( Name = "Requisito_Agrupador" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Requisito_agrupador
      {
         get {
            return sdt.gxTpr_Requisito_agrupador ;
         }

         set {
            sdt.gxTpr_Requisito_agrupador = (String)(value);
         }

      }

      [DataMember( Name = "Requisito_Restricao" , Order = 9 )]
      public String gxTpr_Requisito_restricao
      {
         get {
            return sdt.gxTpr_Requisito_restricao ;
         }

         set {
            sdt.gxTpr_Requisito_restricao = (String)(value);
         }

      }

      [DataMember( Name = "Requisito_Ordem" , Order = 10 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Requisito_ordem
      {
         get {
            return sdt.gxTpr_Requisito_ordem ;
         }

         set {
            sdt.gxTpr_Requisito_ordem = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_Pontuacao" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Requisito_pontuacao
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Requisito_pontuacao, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Requisito_pontuacao = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Requisito_DataHomologacao" , Order = 12 )]
      [GxSeudo()]
      public String gxTpr_Requisito_datahomologacao
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Requisito_datahomologacao) ;
         }

         set {
            sdt.gxTpr_Requisito_datahomologacao = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Requisito_Status" , Order = 13 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Requisito_status
      {
         get {
            return sdt.gxTpr_Requisito_status ;
         }

         set {
            sdt.gxTpr_Requisito_status = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_Cadastro" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Requisito_cadastro
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Requisito_cadastro) ;
         }

         set {
            sdt.gxTpr_Requisito_cadastro = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "Requisito_ReqCod" , Order = 15 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Requisito_reqcod
      {
         get {
            return sdt.gxTpr_Requisito_reqcod ;
         }

         set {
            sdt.gxTpr_Requisito_reqcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_Prioridade" , Order = 16 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Requisito_prioridade
      {
         get {
            return sdt.gxTpr_Requisito_prioridade ;
         }

         set {
            sdt.gxTpr_Requisito_prioridade = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_Ativo" , Order = 17 )]
      [GxSeudo()]
      public bool gxTpr_Requisito_ativo
      {
         get {
            return sdt.gxTpr_Requisito_ativo ;
         }

         set {
            sdt.gxTpr_Requisito_ativo = value;
         }

      }

      public SdtRequisito sdt
      {
         get {
            return (SdtRequisito)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtRequisito() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 48 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
