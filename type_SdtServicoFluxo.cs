/*
               File: type_SdtServicoFluxo
        Description: Processo do Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:41.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ServicoFluxo" )]
   [XmlType(TypeName =  "ServicoFluxo" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtServicoFluxo : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtServicoFluxo( )
      {
         /* Constructor for serialization */
         gxTv_SdtServicoFluxo_Servicofluxo_servicosigla = "";
         gxTv_SdtServicoFluxo_Servicofluxo_srvposnome = "";
         gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla = "";
         gxTv_SdtServicoFluxo_Mode = "";
         gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_Z = "";
         gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_Z = "";
         gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_Z = "";
      }

      public SdtServicoFluxo( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1528ServicoFluxo_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1528ServicoFluxo_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ServicoFluxo_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ServicoFluxo");
         metadata.Set("BT", "ServicoFluxo");
         metadata.Set("PK", "[ \"ServicoFluxo_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ServicoFluxo_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Servico_Codigo\" ],\"FKMap\":[ \"ServicoFluxo_ServicoCod-Servico_Codigo\" ] },{ \"FK\":[ \"Servico_Codigo\" ],\"FKMap\":[ \"ServicoFluxo_ServicoPos-Servico_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_servicocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_servicosigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_servicotphrq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_ordem_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_servicopos_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_srvposnome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_srvpossigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_srvposprctmp_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_srvposprcpgm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_srvposprccnc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_servicosigla_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_servicotphrq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_ordem_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_servicopos_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_srvposnome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_srvpossigla_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_srvposprctmp_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_srvposprcpgm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicofluxo_srvposprccnc_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtServicoFluxo deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtServicoFluxo)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtServicoFluxo obj ;
         obj = this;
         obj.gxTpr_Servicofluxo_codigo = deserialized.gxTpr_Servicofluxo_codigo;
         obj.gxTpr_Servicofluxo_servicocod = deserialized.gxTpr_Servicofluxo_servicocod;
         obj.gxTpr_Servicofluxo_servicosigla = deserialized.gxTpr_Servicofluxo_servicosigla;
         obj.gxTpr_Servicofluxo_servicotphrq = deserialized.gxTpr_Servicofluxo_servicotphrq;
         obj.gxTpr_Servicofluxo_ordem = deserialized.gxTpr_Servicofluxo_ordem;
         obj.gxTpr_Servicofluxo_servicopos = deserialized.gxTpr_Servicofluxo_servicopos;
         obj.gxTpr_Servicofluxo_srvposnome = deserialized.gxTpr_Servicofluxo_srvposnome;
         obj.gxTpr_Servicofluxo_srvpossigla = deserialized.gxTpr_Servicofluxo_srvpossigla;
         obj.gxTpr_Servicofluxo_srvposprctmp = deserialized.gxTpr_Servicofluxo_srvposprctmp;
         obj.gxTpr_Servicofluxo_srvposprcpgm = deserialized.gxTpr_Servicofluxo_srvposprcpgm;
         obj.gxTpr_Servicofluxo_srvposprccnc = deserialized.gxTpr_Servicofluxo_srvposprccnc;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Servicofluxo_codigo_Z = deserialized.gxTpr_Servicofluxo_codigo_Z;
         obj.gxTpr_Servicofluxo_servicocod_Z = deserialized.gxTpr_Servicofluxo_servicocod_Z;
         obj.gxTpr_Servicofluxo_servicosigla_Z = deserialized.gxTpr_Servicofluxo_servicosigla_Z;
         obj.gxTpr_Servicofluxo_servicotphrq_Z = deserialized.gxTpr_Servicofluxo_servicotphrq_Z;
         obj.gxTpr_Servicofluxo_ordem_Z = deserialized.gxTpr_Servicofluxo_ordem_Z;
         obj.gxTpr_Servicofluxo_servicopos_Z = deserialized.gxTpr_Servicofluxo_servicopos_Z;
         obj.gxTpr_Servicofluxo_srvposnome_Z = deserialized.gxTpr_Servicofluxo_srvposnome_Z;
         obj.gxTpr_Servicofluxo_srvpossigla_Z = deserialized.gxTpr_Servicofluxo_srvpossigla_Z;
         obj.gxTpr_Servicofluxo_srvposprctmp_Z = deserialized.gxTpr_Servicofluxo_srvposprctmp_Z;
         obj.gxTpr_Servicofluxo_srvposprcpgm_Z = deserialized.gxTpr_Servicofluxo_srvposprcpgm_Z;
         obj.gxTpr_Servicofluxo_srvposprccnc_Z = deserialized.gxTpr_Servicofluxo_srvposprccnc_Z;
         obj.gxTpr_Servicofluxo_servicosigla_N = deserialized.gxTpr_Servicofluxo_servicosigla_N;
         obj.gxTpr_Servicofluxo_servicotphrq_N = deserialized.gxTpr_Servicofluxo_servicotphrq_N;
         obj.gxTpr_Servicofluxo_ordem_N = deserialized.gxTpr_Servicofluxo_ordem_N;
         obj.gxTpr_Servicofluxo_servicopos_N = deserialized.gxTpr_Servicofluxo_servicopos_N;
         obj.gxTpr_Servicofluxo_srvposnome_N = deserialized.gxTpr_Servicofluxo_srvposnome_N;
         obj.gxTpr_Servicofluxo_srvpossigla_N = deserialized.gxTpr_Servicofluxo_srvpossigla_N;
         obj.gxTpr_Servicofluxo_srvposprctmp_N = deserialized.gxTpr_Servicofluxo_srvposprctmp_N;
         obj.gxTpr_Servicofluxo_srvposprcpgm_N = deserialized.gxTpr_Servicofluxo_srvposprcpgm_N;
         obj.gxTpr_Servicofluxo_srvposprccnc_N = deserialized.gxTpr_Servicofluxo_srvposprccnc_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_Codigo") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_ServicoCod") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_servicocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_ServicoSigla") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_servicosigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_ServicoTpHrq") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_Ordem") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_ordem = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_ServicoPos") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_servicopos = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosNome") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvposnome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosSigla") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosPrcTmp") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosPrcPgm") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosPrcCnc") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtServicoFluxo_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtServicoFluxo_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_Codigo_Z") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_ServicoCod_Z") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_servicocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_ServicoSigla_Z") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_ServicoTpHrq_Z") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_Ordem_Z") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_ordem_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_ServicoPos_Z") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_servicopos_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosNome_Z") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosSigla_Z") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosPrcTmp_Z") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosPrcPgm_Z") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosPrcCnc_Z") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_ServicoSigla_N") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_ServicoTpHrq_N") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_Ordem_N") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_ordem_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_ServicoPos_N") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_servicopos_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosNome_N") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosSigla_N") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosPrcTmp_N") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosPrcPgm_N") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoFluxo_SrvPosPrcCnc_N") )
               {
                  gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ServicoFluxo";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ServicoFluxo_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoFluxo_ServicoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_servicocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoFluxo_ServicoSigla", StringUtil.RTrim( gxTv_SdtServicoFluxo_Servicofluxo_servicosigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoFluxo_ServicoTpHrq", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoFluxo_Ordem", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_ordem), 3, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoFluxo_ServicoPos", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_servicopos), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoFluxo_SrvPosNome", StringUtil.RTrim( gxTv_SdtServicoFluxo_Servicofluxo_srvposnome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoFluxo_SrvPosSigla", StringUtil.RTrim( gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoFluxo_SrvPosPrcTmp", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoFluxo_SrvPosPrcPgm", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoFluxo_SrvPosPrcCnc", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtServicoFluxo_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_ServicoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_servicocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_ServicoSigla_Z", StringUtil.RTrim( gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_ServicoTpHrq_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_Ordem_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_ordem_Z), 3, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_ServicoPos_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_servicopos_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_SrvPosNome_Z", StringUtil.RTrim( gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_SrvPosSigla_Z", StringUtil.RTrim( gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_SrvPosPrcTmp_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_SrvPosPrcPgm_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_SrvPosPrcCnc_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_ServicoSigla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_ServicoTpHrq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_Ordem_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_ordem_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_ServicoPos_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_servicopos_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_SrvPosNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_SrvPosSigla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_SrvPosPrcTmp_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_SrvPosPrcPgm_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoFluxo_SrvPosPrcCnc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ServicoFluxo_Codigo", gxTv_SdtServicoFluxo_Servicofluxo_codigo, false);
         AddObjectProperty("ServicoFluxo_ServicoCod", gxTv_SdtServicoFluxo_Servicofluxo_servicocod, false);
         AddObjectProperty("ServicoFluxo_ServicoSigla", gxTv_SdtServicoFluxo_Servicofluxo_servicosigla, false);
         AddObjectProperty("ServicoFluxo_ServicoTpHrq", gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq, false);
         AddObjectProperty("ServicoFluxo_Ordem", gxTv_SdtServicoFluxo_Servicofluxo_ordem, false);
         AddObjectProperty("ServicoFluxo_ServicoPos", gxTv_SdtServicoFluxo_Servicofluxo_servicopos, false);
         AddObjectProperty("ServicoFluxo_SrvPosNome", gxTv_SdtServicoFluxo_Servicofluxo_srvposnome, false);
         AddObjectProperty("ServicoFluxo_SrvPosSigla", gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla, false);
         AddObjectProperty("ServicoFluxo_SrvPosPrcTmp", gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp, false);
         AddObjectProperty("ServicoFluxo_SrvPosPrcPgm", gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm, false);
         AddObjectProperty("ServicoFluxo_SrvPosPrcCnc", gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtServicoFluxo_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtServicoFluxo_Initialized, false);
            AddObjectProperty("ServicoFluxo_Codigo_Z", gxTv_SdtServicoFluxo_Servicofluxo_codigo_Z, false);
            AddObjectProperty("ServicoFluxo_ServicoCod_Z", gxTv_SdtServicoFluxo_Servicofluxo_servicocod_Z, false);
            AddObjectProperty("ServicoFluxo_ServicoSigla_Z", gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_Z, false);
            AddObjectProperty("ServicoFluxo_ServicoTpHrq_Z", gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_Z, false);
            AddObjectProperty("ServicoFluxo_Ordem_Z", gxTv_SdtServicoFluxo_Servicofluxo_ordem_Z, false);
            AddObjectProperty("ServicoFluxo_ServicoPos_Z", gxTv_SdtServicoFluxo_Servicofluxo_servicopos_Z, false);
            AddObjectProperty("ServicoFluxo_SrvPosNome_Z", gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_Z, false);
            AddObjectProperty("ServicoFluxo_SrvPosSigla_Z", gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_Z, false);
            AddObjectProperty("ServicoFluxo_SrvPosPrcTmp_Z", gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_Z, false);
            AddObjectProperty("ServicoFluxo_SrvPosPrcPgm_Z", gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_Z, false);
            AddObjectProperty("ServicoFluxo_SrvPosPrcCnc_Z", gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_Z, false);
            AddObjectProperty("ServicoFluxo_ServicoSigla_N", gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_N, false);
            AddObjectProperty("ServicoFluxo_ServicoTpHrq_N", gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_N, false);
            AddObjectProperty("ServicoFluxo_Ordem_N", gxTv_SdtServicoFluxo_Servicofluxo_ordem_N, false);
            AddObjectProperty("ServicoFluxo_ServicoPos_N", gxTv_SdtServicoFluxo_Servicofluxo_servicopos_N, false);
            AddObjectProperty("ServicoFluxo_SrvPosNome_N", gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_N, false);
            AddObjectProperty("ServicoFluxo_SrvPosSigla_N", gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_N, false);
            AddObjectProperty("ServicoFluxo_SrvPosPrcTmp_N", gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_N, false);
            AddObjectProperty("ServicoFluxo_SrvPosPrcPgm_N", gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_N, false);
            AddObjectProperty("ServicoFluxo_SrvPosPrcCnc_N", gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_Codigo" )]
      [  XmlElement( ElementName = "ServicoFluxo_Codigo"   )]
      public int gxTpr_Servicofluxo_codigo
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_codigo ;
         }

         set {
            if ( gxTv_SdtServicoFluxo_Servicofluxo_codigo != value )
            {
               gxTv_SdtServicoFluxo_Mode = "INS";
               this.gxTv_SdtServicoFluxo_Servicofluxo_codigo_Z_SetNull( );
               this.gxTv_SdtServicoFluxo_Servicofluxo_servicocod_Z_SetNull( );
               this.gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_Z_SetNull( );
               this.gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_Z_SetNull( );
               this.gxTv_SdtServicoFluxo_Servicofluxo_ordem_Z_SetNull( );
               this.gxTv_SdtServicoFluxo_Servicofluxo_servicopos_Z_SetNull( );
               this.gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_Z_SetNull( );
               this.gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_Z_SetNull( );
               this.gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_Z_SetNull( );
               this.gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_Z_SetNull( );
               this.gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_Z_SetNull( );
            }
            gxTv_SdtServicoFluxo_Servicofluxo_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ServicoFluxo_ServicoCod" )]
      [  XmlElement( ElementName = "ServicoFluxo_ServicoCod"   )]
      public int gxTpr_Servicofluxo_servicocod
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_servicocod ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_servicocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ServicoFluxo_ServicoSigla" )]
      [  XmlElement( ElementName = "ServicoFluxo_ServicoSigla"   )]
      public String gxTpr_Servicofluxo_servicosigla
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_servicosigla ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_N = 0;
            gxTv_SdtServicoFluxo_Servicofluxo_servicosigla = (String)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_N = 1;
         gxTv_SdtServicoFluxo_Servicofluxo_servicosigla = "";
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_ServicoTpHrq" )]
      [  XmlElement( ElementName = "ServicoFluxo_ServicoTpHrq"   )]
      public short gxTpr_Servicofluxo_servicotphrq
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_N = 0;
            gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_N = 1;
         gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_Ordem" )]
      [  XmlElement( ElementName = "ServicoFluxo_Ordem"   )]
      public short gxTpr_Servicofluxo_ordem
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_ordem ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_ordem_N = 0;
            gxTv_SdtServicoFluxo_Servicofluxo_ordem = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_ordem_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_ordem_N = 1;
         gxTv_SdtServicoFluxo_Servicofluxo_ordem = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_ordem_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_ServicoPos" )]
      [  XmlElement( ElementName = "ServicoFluxo_ServicoPos"   )]
      public int gxTpr_Servicofluxo_servicopos
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_servicopos ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_servicopos_N = 0;
            gxTv_SdtServicoFluxo_Servicofluxo_servicopos = (int)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_servicopos_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_servicopos_N = 1;
         gxTv_SdtServicoFluxo_Servicofluxo_servicopos = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_servicopos_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosNome" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosNome"   )]
      public String gxTpr_Servicofluxo_srvposnome
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvposnome ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_N = 0;
            gxTv_SdtServicoFluxo_Servicofluxo_srvposnome = (String)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_N = 1;
         gxTv_SdtServicoFluxo_Servicofluxo_srvposnome = "";
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosSigla" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosSigla"   )]
      public String gxTpr_Servicofluxo_srvpossigla
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_N = 0;
            gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla = (String)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_N = 1;
         gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla = "";
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosPrcTmp" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosPrcTmp"   )]
      public short gxTpr_Servicofluxo_srvposprctmp
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_N = 0;
            gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_N = 1;
         gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosPrcPgm" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosPrcPgm"   )]
      public short gxTpr_Servicofluxo_srvposprcpgm
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_N = 0;
            gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_N = 1;
         gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosPrcCnc" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosPrcCnc"   )]
      public short gxTpr_Servicofluxo_srvposprccnc
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_N = 0;
            gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_N = 1;
         gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtServicoFluxo_Mode ;
         }

         set {
            gxTv_SdtServicoFluxo_Mode = (String)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Mode_SetNull( )
      {
         gxTv_SdtServicoFluxo_Mode = "";
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtServicoFluxo_Initialized ;
         }

         set {
            gxTv_SdtServicoFluxo_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Initialized_SetNull( )
      {
         gxTv_SdtServicoFluxo_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_Codigo_Z" )]
      [  XmlElement( ElementName = "ServicoFluxo_Codigo_Z"   )]
      public int gxTpr_Servicofluxo_codigo_Z
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_codigo_Z ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_codigo_Z_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_ServicoCod_Z" )]
      [  XmlElement( ElementName = "ServicoFluxo_ServicoCod_Z"   )]
      public int gxTpr_Servicofluxo_servicocod_Z
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_servicocod_Z ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_servicocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_servicocod_Z_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_servicocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_servicocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_ServicoSigla_Z" )]
      [  XmlElement( ElementName = "ServicoFluxo_ServicoSigla_Z"   )]
      public String gxTpr_Servicofluxo_servicosigla_Z
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_Z ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_Z_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_ServicoTpHrq_Z" )]
      [  XmlElement( ElementName = "ServicoFluxo_ServicoTpHrq_Z"   )]
      public short gxTpr_Servicofluxo_servicotphrq_Z
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_Z ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_Z = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_Z_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_Ordem_Z" )]
      [  XmlElement( ElementName = "ServicoFluxo_Ordem_Z"   )]
      public short gxTpr_Servicofluxo_ordem_Z
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_ordem_Z ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_ordem_Z = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_ordem_Z_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_ordem_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_ordem_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_ServicoPos_Z" )]
      [  XmlElement( ElementName = "ServicoFluxo_ServicoPos_Z"   )]
      public int gxTpr_Servicofluxo_servicopos_Z
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_servicopos_Z ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_servicopos_Z = (int)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_servicopos_Z_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_servicopos_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_servicopos_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosNome_Z" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosNome_Z"   )]
      public String gxTpr_Servicofluxo_srvposnome_Z
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_Z ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_Z = (String)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_Z_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_Z = "";
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosSigla_Z" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosSigla_Z"   )]
      public String gxTpr_Servicofluxo_srvpossigla_Z
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_Z ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_Z_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosPrcTmp_Z" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosPrcTmp_Z"   )]
      public short gxTpr_Servicofluxo_srvposprctmp_Z
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_Z ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_Z = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_Z_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosPrcPgm_Z" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosPrcPgm_Z"   )]
      public short gxTpr_Servicofluxo_srvposprcpgm_Z
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_Z ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_Z = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_Z_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosPrcCnc_Z" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosPrcCnc_Z"   )]
      public short gxTpr_Servicofluxo_srvposprccnc_Z
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_Z ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_Z = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_Z_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_ServicoSigla_N" )]
      [  XmlElement( ElementName = "ServicoFluxo_ServicoSigla_N"   )]
      public short gxTpr_Servicofluxo_servicosigla_N
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_N ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_N_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_ServicoTpHrq_N" )]
      [  XmlElement( ElementName = "ServicoFluxo_ServicoTpHrq_N"   )]
      public short gxTpr_Servicofluxo_servicotphrq_N
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_N ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_N_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_Ordem_N" )]
      [  XmlElement( ElementName = "ServicoFluxo_Ordem_N"   )]
      public short gxTpr_Servicofluxo_ordem_N
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_ordem_N ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_ordem_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_ordem_N_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_ordem_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_ordem_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_ServicoPos_N" )]
      [  XmlElement( ElementName = "ServicoFluxo_ServicoPos_N"   )]
      public short gxTpr_Servicofluxo_servicopos_N
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_servicopos_N ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_servicopos_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_servicopos_N_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_servicopos_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_servicopos_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosNome_N" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosNome_N"   )]
      public short gxTpr_Servicofluxo_srvposnome_N
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_N ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_N_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosSigla_N" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosSigla_N"   )]
      public short gxTpr_Servicofluxo_srvpossigla_N
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_N ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_N_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosPrcTmp_N" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosPrcTmp_N"   )]
      public short gxTpr_Servicofluxo_srvposprctmp_N
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_N ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_N_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosPrcPgm_N" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosPrcPgm_N"   )]
      public short gxTpr_Servicofluxo_srvposprcpgm_N
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_N ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_N_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoFluxo_SrvPosPrcCnc_N" )]
      [  XmlElement( ElementName = "ServicoFluxo_SrvPosPrcCnc_N"   )]
      public short gxTpr_Servicofluxo_srvposprccnc_N
      {
         get {
            return gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_N ;
         }

         set {
            gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_N_SetNull( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtServicoFluxo_Servicofluxo_servicosigla = "";
         gxTv_SdtServicoFluxo_Servicofluxo_srvposnome = "";
         gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla = "";
         gxTv_SdtServicoFluxo_Mode = "";
         gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_Z = "";
         gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_Z = "";
         gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_Z = "";
         gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq = 1;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "servicofluxo", "GeneXus.Programs.servicofluxo_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_ordem ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc ;
      private short gxTv_SdtServicoFluxo_Initialized ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_Z ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_ordem_Z ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_Z ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_Z ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_Z ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_N ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_servicotphrq_N ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_ordem_N ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_servicopos_N ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_N ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_N ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_srvposprctmp_N ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_srvposprcpgm_N ;
      private short gxTv_SdtServicoFluxo_Servicofluxo_srvposprccnc_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtServicoFluxo_Servicofluxo_codigo ;
      private int gxTv_SdtServicoFluxo_Servicofluxo_servicocod ;
      private int gxTv_SdtServicoFluxo_Servicofluxo_servicopos ;
      private int gxTv_SdtServicoFluxo_Servicofluxo_codigo_Z ;
      private int gxTv_SdtServicoFluxo_Servicofluxo_servicocod_Z ;
      private int gxTv_SdtServicoFluxo_Servicofluxo_servicopos_Z ;
      private String gxTv_SdtServicoFluxo_Servicofluxo_servicosigla ;
      private String gxTv_SdtServicoFluxo_Servicofluxo_srvposnome ;
      private String gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla ;
      private String gxTv_SdtServicoFluxo_Mode ;
      private String gxTv_SdtServicoFluxo_Servicofluxo_servicosigla_Z ;
      private String gxTv_SdtServicoFluxo_Servicofluxo_srvposnome_Z ;
      private String gxTv_SdtServicoFluxo_Servicofluxo_srvpossigla_Z ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ServicoFluxo", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtServicoFluxo_RESTInterface : GxGenericCollectionItem<SdtServicoFluxo>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtServicoFluxo_RESTInterface( ) : base()
      {
      }

      public SdtServicoFluxo_RESTInterface( SdtServicoFluxo psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ServicoFluxo_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servicofluxo_codigo
      {
         get {
            return sdt.gxTpr_Servicofluxo_codigo ;
         }

         set {
            sdt.gxTpr_Servicofluxo_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoFluxo_ServicoCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servicofluxo_servicocod
      {
         get {
            return sdt.gxTpr_Servicofluxo_servicocod ;
         }

         set {
            sdt.gxTpr_Servicofluxo_servicocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoFluxo_ServicoSigla" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Servicofluxo_servicosigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servicofluxo_servicosigla) ;
         }

         set {
            sdt.gxTpr_Servicofluxo_servicosigla = (String)(value);
         }

      }

      [DataMember( Name = "ServicoFluxo_ServicoTpHrq" , Order = 3 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Servicofluxo_servicotphrq
      {
         get {
            return sdt.gxTpr_Servicofluxo_servicotphrq ;
         }

         set {
            sdt.gxTpr_Servicofluxo_servicotphrq = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoFluxo_Ordem" , Order = 4 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Servicofluxo_ordem
      {
         get {
            return sdt.gxTpr_Servicofluxo_ordem ;
         }

         set {
            sdt.gxTpr_Servicofluxo_ordem = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoFluxo_ServicoPos" , Order = 5 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servicofluxo_servicopos
      {
         get {
            return sdt.gxTpr_Servicofluxo_servicopos ;
         }

         set {
            sdt.gxTpr_Servicofluxo_servicopos = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoFluxo_SrvPosNome" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Servicofluxo_srvposnome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servicofluxo_srvposnome) ;
         }

         set {
            sdt.gxTpr_Servicofluxo_srvposnome = (String)(value);
         }

      }

      [DataMember( Name = "ServicoFluxo_SrvPosSigla" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Servicofluxo_srvpossigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servicofluxo_srvpossigla) ;
         }

         set {
            sdt.gxTpr_Servicofluxo_srvpossigla = (String)(value);
         }

      }

      [DataMember( Name = "ServicoFluxo_SrvPosPrcTmp" , Order = 8 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Servicofluxo_srvposprctmp
      {
         get {
            return sdt.gxTpr_Servicofluxo_srvposprctmp ;
         }

         set {
            sdt.gxTpr_Servicofluxo_srvposprctmp = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoFluxo_SrvPosPrcPgm" , Order = 9 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Servicofluxo_srvposprcpgm
      {
         get {
            return sdt.gxTpr_Servicofluxo_srvposprcpgm ;
         }

         set {
            sdt.gxTpr_Servicofluxo_srvposprcpgm = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoFluxo_SrvPosPrcCnc" , Order = 10 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Servicofluxo_srvposprccnc
      {
         get {
            return sdt.gxTpr_Servicofluxo_srvposprccnc ;
         }

         set {
            sdt.gxTpr_Servicofluxo_srvposprccnc = (short)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtServicoFluxo sdt
      {
         get {
            return (SdtServicoFluxo)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtServicoFluxo() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 33 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
