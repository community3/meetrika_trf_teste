/*
               File: PRC_Disponivel
        Description: Pode executar ou tem pendencias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:56.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_disponivel : GXProcedure
   {
      public prc_disponivel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_disponivel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_UserId ,
                           out bool aP1_Disponivel )
      {
         this.AV9UserId = aP0_UserId;
         this.AV8Disponivel = false ;
         initialize();
         executePrivate();
         aP1_Disponivel=this.AV8Disponivel;
      }

      public bool executeUdp( int aP0_UserId )
      {
         this.AV9UserId = aP0_UserId;
         this.AV8Disponivel = false ;
         initialize();
         executePrivate();
         aP1_Disponivel=this.AV8Disponivel;
         return AV8Disponivel ;
      }

      public void executeSubmit( int aP0_UserId ,
                                 out bool aP1_Disponivel )
      {
         prc_disponivel objprc_disponivel;
         objprc_disponivel = new prc_disponivel();
         objprc_disponivel.AV9UserId = aP0_UserId;
         objprc_disponivel.AV8Disponivel = false ;
         objprc_disponivel.context.SetSubmitInitialConfig(context);
         objprc_disponivel.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_disponivel);
         aP1_Disponivel=this.AV8Disponivel;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_disponivel)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Disponivel = true;
         /* Using cursor P00TE2 */
         pr_default.execute(0, new Object[] {AV9UserId});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A890ContagemResultado_Responsavel = P00TE2_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00TE2_n890ContagemResultado_Responsavel[0];
            A484ContagemResultado_StatusDmn = P00TE2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00TE2_n484ContagemResultado_StatusDmn[0];
            A456ContagemResultado_Codigo = P00TE2_A456ContagemResultado_Codigo[0];
            AV8Disponivel = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00TE2_A890ContagemResultado_Responsavel = new int[1] ;
         P00TE2_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00TE2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00TE2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00TE2_A456ContagemResultado_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_disponivel__default(),
            new Object[][] {
                new Object[] {
               P00TE2_A890ContagemResultado_Responsavel, P00TE2_n890ContagemResultado_Responsavel, P00TE2_A484ContagemResultado_StatusDmn, P00TE2_n484ContagemResultado_StatusDmn, P00TE2_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9UserId ;
      private int A890ContagemResultado_Responsavel ;
      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private bool AV8Disponivel ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n484ContagemResultado_StatusDmn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00TE2_A890ContagemResultado_Responsavel ;
      private bool[] P00TE2_n890ContagemResultado_Responsavel ;
      private String[] P00TE2_A484ContagemResultado_StatusDmn ;
      private bool[] P00TE2_n484ContagemResultado_StatusDmn ;
      private int[] P00TE2_A456ContagemResultado_Codigo ;
      private bool aP1_Disponivel ;
   }

   public class prc_disponivel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00TE2 ;
          prmP00TE2 = new Object[] {
          new Object[] {"@AV9UserId",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00TE2", "SELECT TOP 1 [ContagemResultado_Responsavel], [ContagemResultado_StatusDmn], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_StatusDmn] = 'E' or [ContagemResultado_StatusDmn] = 'A' or [ContagemResultado_StatusDmn] = 'D') AND ([ContagemResultado_Responsavel] = @AV9UserId) ORDER BY [ContagemResultado_StatusDmn] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TE2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
