/*
               File: Rel_OrdemServico
        Description: Rel_Ordem Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:22.54
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_ordemservico : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV15ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_ordemservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_ordemservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo )
      {
         this.AV15ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo )
      {
         arel_ordemservico objarel_ordemservico;
         objarel_ordemservico = new arel_ordemservico();
         objarel_ordemservico.AV15ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objarel_ordemservico.context.SetSubmitInitialConfig(context);
         objarel_ordemservico.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_ordemservico);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_ordemservico)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV196WWPContext) ;
            AV186TextoLink = StringUtil.Trim( AV171QrCodeUrl) + ", informando o c�digo " + StringUtil.Trim( AV192Verificador);
            AV9AssinadoDtHr = context.localUtil.DToC( Gx_date, 2, "/") + " em " + StringUtil.Substring( Gx_time, 1, 5) + ", conforme hor�rio oficial de Brasilia.";
            AV169QrCode_Image = AV170QrCodePath;
            AV207Qrcode_image_GXI = GeneXus.Utils.GXDbFile.PathToUrl( AV170QrCodePath);
            AV157PaginaImpar = true;
            /* Using cursor P00CO4 */
            pr_default.execute(0, new Object[] {AV15ContagemResultado_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P00CO4_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00CO4_n1553ContagemResultado_CntSrvCod[0];
               A1603ContagemResultado_CntCod = P00CO4_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00CO4_n1603ContagemResultado_CntCod[0];
               A1604ContagemResultado_CntPrpCod = P00CO4_A1604ContagemResultado_CntPrpCod[0];
               n1604ContagemResultado_CntPrpCod = P00CO4_n1604ContagemResultado_CntPrpCod[0];
               A1605ContagemResultado_CntPrpPesCod = P00CO4_A1605ContagemResultado_CntPrpPesCod[0];
               n1605ContagemResultado_CntPrpPesCod = P00CO4_n1605ContagemResultado_CntPrpPesCod[0];
               A601ContagemResultado_Servico = P00CO4_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00CO4_n601ContagemResultado_Servico[0];
               A456ContagemResultado_Codigo = P00CO4_A456ContagemResultado_Codigo[0];
               A1612ContagemResultado_CntNum = P00CO4_A1612ContagemResultado_CntNum[0];
               n1612ContagemResultado_CntNum = P00CO4_n1612ContagemResultado_CntNum[0];
               A1606ContagemResultado_CntPrpPesNom = P00CO4_A1606ContagemResultado_CntPrpPesNom[0];
               n1606ContagemResultado_CntPrpPesNom = P00CO4_n1606ContagemResultado_CntPrpPesNom[0];
               A801ContagemResultado_ServicoSigla = P00CO4_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P00CO4_n801ContagemResultado_ServicoSigla[0];
               A1452ContagemResultado_SS = P00CO4_A1452ContagemResultado_SS[0];
               n1452ContagemResultado_SS = P00CO4_n1452ContagemResultado_SS[0];
               A489ContagemResultado_SistemaCod = P00CO4_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P00CO4_n489ContagemResultado_SistemaCod[0];
               A509ContagemrResultado_SistemaSigla = P00CO4_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P00CO4_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P00CO4_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P00CO4_n515ContagemResultado_SistemaCoord[0];
               A457ContagemResultado_Demanda = P00CO4_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P00CO4_n457ContagemResultado_Demanda[0];
               A493ContagemResultado_DemandaFM = P00CO4_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P00CO4_n493ContagemResultado_DemandaFM[0];
               A1624ContagemResultado_DatVgnInc = P00CO4_A1624ContagemResultado_DatVgnInc[0];
               n1624ContagemResultado_DatVgnInc = P00CO4_n1624ContagemResultado_DatVgnInc[0];
               A39Contratada_Codigo = P00CO4_A39Contratada_Codigo[0];
               n39Contratada_Codigo = P00CO4_n39Contratada_Codigo[0];
               A494ContagemResultado_Descricao = P00CO4_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P00CO4_n494ContagemResultado_Descricao[0];
               A514ContagemResultado_Observacao = P00CO4_A514ContagemResultado_Observacao[0];
               n514ContagemResultado_Observacao = P00CO4_n514ContagemResultado_Observacao[0];
               A1625ContagemResultado_DatIncTA = P00CO4_A1625ContagemResultado_DatIncTA[0];
               n1625ContagemResultado_DatIncTA = P00CO4_n1625ContagemResultado_DatIncTA[0];
               A1603ContagemResultado_CntCod = P00CO4_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00CO4_n1603ContagemResultado_CntCod[0];
               A601ContagemResultado_Servico = P00CO4_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00CO4_n601ContagemResultado_Servico[0];
               A1604ContagemResultado_CntPrpCod = P00CO4_A1604ContagemResultado_CntPrpCod[0];
               n1604ContagemResultado_CntPrpCod = P00CO4_n1604ContagemResultado_CntPrpCod[0];
               A1612ContagemResultado_CntNum = P00CO4_A1612ContagemResultado_CntNum[0];
               n1612ContagemResultado_CntNum = P00CO4_n1612ContagemResultado_CntNum[0];
               A1624ContagemResultado_DatVgnInc = P00CO4_A1624ContagemResultado_DatVgnInc[0];
               n1624ContagemResultado_DatVgnInc = P00CO4_n1624ContagemResultado_DatVgnInc[0];
               A39Contratada_Codigo = P00CO4_A39Contratada_Codigo[0];
               n39Contratada_Codigo = P00CO4_n39Contratada_Codigo[0];
               A1605ContagemResultado_CntPrpPesCod = P00CO4_A1605ContagemResultado_CntPrpPesCod[0];
               n1605ContagemResultado_CntPrpPesCod = P00CO4_n1605ContagemResultado_CntPrpPesCod[0];
               A1606ContagemResultado_CntPrpPesNom = P00CO4_A1606ContagemResultado_CntPrpPesNom[0];
               n1606ContagemResultado_CntPrpPesNom = P00CO4_n1606ContagemResultado_CntPrpPesNom[0];
               A801ContagemResultado_ServicoSigla = P00CO4_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P00CO4_n801ContagemResultado_ServicoSigla[0];
               A509ContagemrResultado_SistemaSigla = P00CO4_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P00CO4_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P00CO4_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P00CO4_n515ContagemResultado_SistemaCoord[0];
               A1625ContagemResultado_DatIncTA = P00CO4_A1625ContagemResultado_DatIncTA[0];
               n1625ContagemResultado_DatIncTA = P00CO4_n1625ContagemResultado_DatIncTA[0];
               OV177SS = AV177SS;
               AV106Contrato_Numero = A1612ContagemResultado_CntNum;
               AV168Preposto = StringUtil.Trim( A1606ContagemResultado_CntPrpPesNom);
               AV200ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
               AV104Contrato_DataInicioTA = A1625ContagemResultado_DatIncTA;
               AV177SS = A1452ContagemResultado_SS;
               AV174Sistema_Codigo = A489ContagemResultado_SistemaCod;
               AV176Sistema_Sigla = A509ContagemrResultado_SistemaSigla;
               AV175Sistema_Coordenacao = A515ContagemResultado_SistemaCoord;
               AV150Os_Header = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM)) + (String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "/"+StringUtil.Trim( A457ContagemResultado_Demanda));
               if ( (DateTime.MinValue==AV104Contrato_DataInicioTA) )
               {
                  AV105Contrato_DataVigenciaInicio = A1624ContagemResultado_DatVgnInc;
               }
               else
               {
                  AV105Contrato_DataVigenciaInicio = AV104Contrato_DataInicioTA;
               }
               AV199Contratada_Codigo = A39Contratada_Codigo;
               /* Execute user subroutine: 'DADOSCONTRATADA' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
               HCO0( false, 232) ;
               getPrinter().GxDrawLine(17, Gx_line+123, 781, Gx_line+123, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(17, Gx_line+5, 781, Gx_line+5, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(15, Gx_line+175, 779, Gx_line+175, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("1.  Identifica��o da Empresa Contratada ", 17, Gx_line+9, 359, Gx_line+28, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("2. Informa��es sobre os Servi�os a serem Realizados", 17, Gx_line+126, 442, Gx_line+145, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("3. Descri��o do Servi�o", 15, Gx_line+178, 440, Gx_line+197, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV176Sistema_Sigla, "@!")), 92, Gx_line+144, 275, Gx_line+161, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV103Contratada_PessoaNom, "@!")), 159, Gx_line+33, 889, Gx_line+50, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102Contratada_PessoaCNPJ, "")), 76, Gx_line+54, 186, Gx_line+71, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV161Pessoa_Endereco, "")), 17, Gx_line+93, 747, Gx_line+110, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV164Pessoa_Telefone, "")), 367, Gx_line+54, 477, Gx_line+71, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Nome da Empresa:", 17, Gx_line+33, 146, Gx_line+51, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("CNPJ:", 17, Gx_line+54, 63, Gx_line+72, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Endere�o:", 16, Gx_line+75, 93, Gx_line+93, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Telefone:", 276, Gx_line+53, 355, Gx_line+71, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Sistema:", 17, Gx_line+144, 86, Gx_line+162, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Identifica��o do Servi�o:", 17, Gx_line+199, 190, Gx_line+218, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV200ContagemResultado_ServicoSigla, "@!")), 197, Gx_line+201, 422, Gx_line+218, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+232);
               HCO0( false, 27) ;
               getPrinter().GxDrawLine(17, Gx_line+5, 781, Gx_line+5, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("4. Produtos/Servi�os Esperados", 17, Gx_line+8, 442, Gx_line+27, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+27);
               AV194vrLinha = 1;
               AV110Demanda = A457ContagemResultado_Demanda;
               AV114DmnDescricao = StringUtil.Trim( A494ContagemResultado_Descricao);
               AV201nlin = (short)(LVCharUtil.LinesWrap( AV114DmnDescricao, 120));
               AV134i = 1;
               while ( AV134i <= AV201nlin )
               {
                  AV112Descricao = LVCharUtil.GetLineWrap( AV114DmnDescricao, AV134i, 120);
                  HCO0( false, 21) ;
                  getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 17, Gx_line+2, 536, Gx_line+18, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+21);
                  AV134i = (short)(AV134i+1);
               }
               HCO0( false, 83) ;
               getPrinter().GxDrawLine(17, Gx_line+5, 781, Gx_line+5, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("5. Formas de Avalia��o dos Servi�os Executados", 17, Gx_line+8, 442, Gx_line+27, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+83);
               HCO0( false, 30) ;
               getPrinter().GxDrawLine(17, Gx_line+5, 781, Gx_line+5, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("6. Informa��es Adicionais", 17, Gx_line+8, 442, Gx_line+27, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+30);
               AV202ContagemResultado_Observacao = StringUtil.Trim( A514ContagemResultado_Observacao);
               AV201nlin = (short)(LVCharUtil.LinesWrap( AV202ContagemResultado_Observacao, 120));
               AV134i = 1;
               while ( AV134i <= AV201nlin )
               {
                  AV112Descricao = LVCharUtil.GetLineWrap( AV202ContagemResultado_Observacao, AV134i, 120);
                  HCO0( false, 22) ;
                  getPrinter().GxAttris("Courier New", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112Descricao, "")), 17, Gx_line+2, 536, Gx_line+18, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+22);
                  AV134i = (short)(AV134i+1);
               }
               HCO0( false, 59) ;
               getPrinter().GxDrawLine(17, Gx_line+9, 781, Gx_line+9, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("7. Per�odo de Execu��o", 17, Gx_line+12, 442, Gx_line+31, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV108DataFim, "99/99/99"), 315, Gx_line+31, 385, Gx_line+48, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV109DataInicio, "99/99/99"), 119, Gx_line+31, 189, Gx_line+48, 2, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Data de In�cio:", 17, Gx_line+30, 117, Gx_line+48, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Data de T�rmino:", 195, Gx_line+30, 313, Gx_line+48, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+59);
               HCO0( false, 60) ;
               getPrinter().GxDrawLine(17, Gx_line+5, 781, Gx_line+5, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("8. Cronograma F�sico/Financeiro", 17, Gx_line+8, 442, Gx_line+27, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+60);
               HCO0( false, 119) ;
               getPrinter().GxDrawLine(18, Gx_line+5, 782, Gx_line+5, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Gestor do Contrato", 18, Gx_line+8, 443, Gx_line+27, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Mat. Siape:", 282, Gx_line+66, 361, Gx_line+84, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura:", 17, Gx_line+87, 94, Gx_line+105, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("E-Mail:", 18, Gx_line+67, 64, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Nome:", 18, Gx_line+46, 147, Gx_line+64, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Data da autoriza��o:", 282, Gx_line+88, 422, Gx_line+106, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Telefone:", 282, Gx_line+45, 361, Gx_line+63, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 7, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Declaro ci�ncia do disposto nesta OS e autorizo a execu��o dos servi�os indicados", 18, Gx_line+28, 372, Gx_line+42, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+119);
               HCO0( false, 126) ;
               getPrinter().GxDrawLine(18, Gx_line+5, 782, Gx_line+5, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Respons�vel pela Demanda", 17, Gx_line+8, 442, Gx_line+27, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Telefone:", 282, Gx_line+45, 361, Gx_line+63, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Data da concord�ncia:", 282, Gx_line+88, 422, Gx_line+106, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Nome:", 17, Gx_line+46, 146, Gx_line+64, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("E-Mail:", 17, Gx_line+67, 67, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura:", 17, Gx_line+87, 94, Gx_line+105, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Mat. Siape:", 282, Gx_line+66, 361, Gx_line+84, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 7, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Declaro concord�ncia com o disposto nesta OS", 17, Gx_line+28, 219, Gx_line+42, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+126);
               HCO0( false, 138) ;
               getPrinter().GxDrawLine(17, Gx_line+0, 781, Gx_line+0, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Respons�vel pela Execu��o da OS", 17, Gx_line+3, 442, Gx_line+22, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Telefone:", 296, Gx_line+72, 375, Gx_line+90, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Data da concord�ncia:", 296, Gx_line+115, 436, Gx_line+133, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Nome:", 17, Gx_line+73, 146, Gx_line+91, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("E-Mail:", 17, Gx_line+94, 73, Gx_line+112, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura:", 17, Gx_line+114, 94, Gx_line+132, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Mat. Siape:", 296, Gx_line+93, 375, Gx_line+111, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 7, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Declaro concord�ncia em executar as atividades descritas nesta 0.S, de acordo", 17, Gx_line+23, 357, Gx_line+37, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText("com as especifica��es estabelecidas pelo Minist�rio da Educa��o e os ", 17, Gx_line+40, 357, Gx_line+54, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("instrumentos contratuais.", 17, Gx_line+53, 357, Gx_line+67, 1, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+138);
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HCO0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'DADOSCONTRATADA' Routine */
         /* Using cursor P00CO5 */
         pr_default.execute(1, new Object[] {AV199Contratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A40Contratada_PessoaCod = P00CO5_A40Contratada_PessoaCod[0];
            A503Pessoa_MunicipioCod = P00CO5_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P00CO5_n503Pessoa_MunicipioCod[0];
            A39Contratada_Codigo = P00CO5_A39Contratada_Codigo[0];
            n39Contratada_Codigo = P00CO5_n39Contratada_Codigo[0];
            A41Contratada_PessoaNom = P00CO5_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00CO5_n41Contratada_PessoaNom[0];
            A524Contratada_OS = P00CO5_A524Contratada_OS[0];
            n524Contratada_OS = P00CO5_n524Contratada_OS[0];
            A42Contratada_PessoaCNPJ = P00CO5_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00CO5_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = P00CO5_A518Pessoa_IE[0];
            n518Pessoa_IE = P00CO5_n518Pessoa_IE[0];
            A519Pessoa_Endereco = P00CO5_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00CO5_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P00CO5_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00CO5_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = P00CO5_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00CO5_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = P00CO5_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00CO5_n523Pessoa_Fax[0];
            A520Pessoa_UF = P00CO5_A520Pessoa_UF[0];
            n520Pessoa_UF = P00CO5_n520Pessoa_UF[0];
            A26Municipio_Nome = P00CO5_A26Municipio_Nome[0];
            n26Municipio_Nome = P00CO5_n26Municipio_Nome[0];
            A503Pessoa_MunicipioCod = P00CO5_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P00CO5_n503Pessoa_MunicipioCod[0];
            A41Contratada_PessoaNom = P00CO5_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00CO5_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00CO5_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00CO5_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = P00CO5_A518Pessoa_IE[0];
            n518Pessoa_IE = P00CO5_n518Pessoa_IE[0];
            A519Pessoa_Endereco = P00CO5_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00CO5_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P00CO5_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00CO5_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = P00CO5_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00CO5_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = P00CO5_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00CO5_n523Pessoa_Fax[0];
            A520Pessoa_UF = P00CO5_A520Pessoa_UF[0];
            n520Pessoa_UF = P00CO5_n520Pessoa_UF[0];
            A26Municipio_Nome = P00CO5_A26Municipio_Nome[0];
            n26Municipio_Nome = P00CO5_n26Municipio_Nome[0];
            AV103Contratada_PessoaNom = A41Contratada_PessoaNom;
            AV101Contratada_OS = A524Contratada_OS;
            if ( StringUtil.StringSearch( A42Contratada_PessoaCNPJ, ".", 1) == 0 )
            {
               AV102Contratada_PessoaCNPJ = StringUtil.Substring( A42Contratada_PessoaCNPJ, 1, 2) + "." + StringUtil.Substring( A42Contratada_PessoaCNPJ, 3, 3) + "." + StringUtil.Substring( A42Contratada_PessoaCNPJ, 6, 3) + "/" + StringUtil.Substring( A42Contratada_PessoaCNPJ, 9, 4) + "-" + StringUtil.Substring( A42Contratada_PessoaCNPJ, 13, 2);
            }
            else
            {
               AV102Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            }
            if ( StringUtil.StringSearch( A518Pessoa_IE, ".", 1) == 0 )
            {
               AV163Pessoa_IE = StringUtil.Substring( A518Pessoa_IE, 1, 2) + "." + StringUtil.Substring( A518Pessoa_IE, 3, 3) + "." + StringUtil.Substring( A518Pessoa_IE, 6, 3) + "/" + StringUtil.Substring( A518Pessoa_IE, 9, 3) + "-" + StringUtil.Substring( A518Pessoa_IE, 12, 2);
            }
            else
            {
               AV163Pessoa_IE = A518Pessoa_IE;
            }
            AV161Pessoa_Endereco = A519Pessoa_Endereco;
            AV160Pessoa_CEP = A521Pessoa_CEP;
            AV164Pessoa_Telefone = A522Pessoa_Telefone;
            AV162Pessoa_Fax = A523Pessoa_Fax;
            AV165Pessoa_UF = A520Pessoa_UF;
            AV144Municipio_Nome = A26Municipio_Nome;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      protected void HCO0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 667, Gx_line+2, 711, Gx_line+29, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV156Pagina), "ZZZ9")), 716, Gx_line+2, 746, Gx_line+29, 1, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Documento:", 29, Gx_line+4, 90, Gx_line+18, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("001 - Modelo OS", 29, Gx_line+20, 113, Gx_line+34, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Data:", 392, Gx_line+4, 420, Gx_line+18, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 392, Gx_line+19, 441, Gx_line+34, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+38);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               AV185SubTitulo = "TERMO DE ACEITE DO PRODUTO OU SERVI�O";
               AV124Emissao = AV108DataFim;
               AV193Volume = "O Valor total de servi�o representado neste documento � de:";
               AV187TituloOS = "O.S. No.:";
               AV135Identificacao1 = "Este Termo de Aceite tem como objetivo a presta��o de servi�os t�cnicos especializados para as";
               AV136Identificacao2 = "atividades de an�lise e mensura��o de software com base na t�cnica de contagem de pontos de fun��o";
               AV137Identificacao3 = "do sistema acima referenciado no �mbito do Minist�rio da Sa�de.";
               AV148Os = AV150Os_Header;
               getPrinter().GxDrawLine(314, Gx_line+168, 314, Gx_line+217, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(485, Gx_line+168, 485, Gx_line+217, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(661, Gx_line+168, 661, Gx_line+218, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(176, Gx_line+168, 176, Gx_line+216, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(14, Gx_line+218, 14, Gx_line+253, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(14, Gx_line+168, 14, Gx_line+254, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(14, Gx_line+254, 778, Gx_line+254, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(314, Gx_line+218, 314, Gx_line+254, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(661, Gx_line+217, 661, Gx_line+253, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(485, Gx_line+218, 485, Gx_line+254, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(176, Gx_line+218, 176, Gx_line+254, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(14, Gx_line+168, 778, Gx_line+168, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(777, Gx_line+168, 777, Gx_line+254, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(14, Gx_line+217, 778, Gx_line+217, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawBitMap(context.GetImagePath( "6ded99f5-9d9f-4337-aeda-7b9046b912b6", "", context.GetTheme( )), 349, Gx_line+0, 429, Gx_line+80) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV187TituloOS, "")), 17, Gx_line+185, 174, Gx_line+202, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("DATA DO CONTRATO", 498, Gx_line+185, 647, Gx_line+203, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("N� DO CONTRATO", 325, Gx_line+185, 472, Gx_line+203, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("DATA DE EMISS�O", 179, Gx_line+185, 310, Gx_line+203, 1, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV105Contrato_DataVigenciaInicio, "99/99/99"), 499, Gx_line+226, 648, Gx_line+243, 1, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV106Contrato_Numero, "")), 323, Gx_line+225, 470, Gx_line+242, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV150Os_Header, "")), 17, Gx_line+223, 174, Gx_line+240, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("N� DA SS:", 671, Gx_line+185, 760, Gx_line+203, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV124Emissao, "99/99/99"), 182, Gx_line+225, 301, Gx_line+242, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("MINIST�RIO DA SA�DE", 317, Gx_line+83, 462, Gx_line+102, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText("SECRETARIA DE GEST�O ESTRAT�GICA E PARTICIPATIVA", 217, Gx_line+100, 562, Gx_line+119, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText("DEPARTAMENTO DE INFORM�TICA DO SUS - DATASUS", 217, Gx_line+117, 556, Gx_line+136, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV185SubTitulo, "")), 217, Gx_line+138, 551, Gx_line+158, 1+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV177SS), "ZZZZZZZ9")), 671, Gx_line+228, 760, Gx_line+243, 1, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+269);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
         add_metrics3( ) ;
         add_metrics4( ) ;
         add_metrics5( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Arial", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Calibri", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics3( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics4( )
      {
         getPrinter().setMetrics("Courier New", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics5( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, true, 56, 14, 70, 118,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 18, 22, 35, 35, 56, 42, 12, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 18, 18, 37, 37, 37, 35, 64, 42, 42, 45, 45, 42, 38, 49, 45, 18, 32, 42, 35, 53, 45, 49, 42, 49, 45, 42, 38, 45, 42, 61, 42, 42, 38, 18, 18, 18, 30, 35, 21, 35, 35, 32, 35, 35, 18, 35, 35, 14, 14, 32, 14, 52, 35, 35, 35, 35, 21, 32, 18, 35, 32, 45, 32, 32, 29, 21, 16, 21, 37, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 35, 35, 34, 35, 16, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 20, 21, 35, 34, 21, 21, 20, 23, 35, 53, 53, 53, 38, 42, 42, 42, 42, 42, 42, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 32, 35, 35, 35, 35, 18, 18, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 38, 35, 35, 35, 35, 32, 35, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV196WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV186TextoLink = "";
         AV171QrCodeUrl = "";
         AV192Verificador = "";
         AV9AssinadoDtHr = "";
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         AV169QrCode_Image = "";
         AV170QrCodePath = "";
         AV207Qrcode_image_GXI = "";
         scmdbuf = "";
         P00CO4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00CO4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00CO4_A1603ContagemResultado_CntCod = new int[1] ;
         P00CO4_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00CO4_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P00CO4_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         P00CO4_A1605ContagemResultado_CntPrpPesCod = new int[1] ;
         P00CO4_n1605ContagemResultado_CntPrpPesCod = new bool[] {false} ;
         P00CO4_A601ContagemResultado_Servico = new int[1] ;
         P00CO4_n601ContagemResultado_Servico = new bool[] {false} ;
         P00CO4_A456ContagemResultado_Codigo = new int[1] ;
         P00CO4_A1612ContagemResultado_CntNum = new String[] {""} ;
         P00CO4_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P00CO4_A1606ContagemResultado_CntPrpPesNom = new String[] {""} ;
         P00CO4_n1606ContagemResultado_CntPrpPesNom = new bool[] {false} ;
         P00CO4_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00CO4_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00CO4_A1452ContagemResultado_SS = new int[1] ;
         P00CO4_n1452ContagemResultado_SS = new bool[] {false} ;
         P00CO4_A489ContagemResultado_SistemaCod = new int[1] ;
         P00CO4_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00CO4_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00CO4_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00CO4_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P00CO4_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P00CO4_A457ContagemResultado_Demanda = new String[] {""} ;
         P00CO4_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00CO4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00CO4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00CO4_A1624ContagemResultado_DatVgnInc = new DateTime[] {DateTime.MinValue} ;
         P00CO4_n1624ContagemResultado_DatVgnInc = new bool[] {false} ;
         P00CO4_A39Contratada_Codigo = new int[1] ;
         P00CO4_n39Contratada_Codigo = new bool[] {false} ;
         P00CO4_A494ContagemResultado_Descricao = new String[] {""} ;
         P00CO4_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00CO4_A514ContagemResultado_Observacao = new String[] {""} ;
         P00CO4_n514ContagemResultado_Observacao = new bool[] {false} ;
         P00CO4_A1625ContagemResultado_DatIncTA = new DateTime[] {DateTime.MinValue} ;
         P00CO4_n1625ContagemResultado_DatIncTA = new bool[] {false} ;
         A1612ContagemResultado_CntNum = "";
         A1606ContagemResultado_CntPrpPesNom = "";
         A801ContagemResultado_ServicoSigla = "";
         A509ContagemrResultado_SistemaSigla = "";
         A515ContagemResultado_SistemaCoord = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A1624ContagemResultado_DatVgnInc = DateTime.MinValue;
         A494ContagemResultado_Descricao = "";
         A514ContagemResultado_Observacao = "";
         A1625ContagemResultado_DatIncTA = DateTime.MinValue;
         AV177SS = 0;
         AV106Contrato_Numero = "";
         AV168Preposto = "";
         AV200ContagemResultado_ServicoSigla = "";
         AV104Contrato_DataInicioTA = DateTime.MinValue;
         AV176Sistema_Sigla = "";
         AV175Sistema_Coordenacao = "";
         AV150Os_Header = "";
         AV105Contrato_DataVigenciaInicio = DateTime.MinValue;
         AV103Contratada_PessoaNom = "";
         AV102Contratada_PessoaCNPJ = "";
         AV161Pessoa_Endereco = "";
         AV164Pessoa_Telefone = "";
         AV110Demanda = "";
         AV114DmnDescricao = "";
         AV112Descricao = "";
         AV202ContagemResultado_Observacao = "";
         AV108DataFim = DateTime.MinValue;
         AV109DataInicio = DateTime.MinValue;
         P00CO5_A40Contratada_PessoaCod = new int[1] ;
         P00CO5_A503Pessoa_MunicipioCod = new int[1] ;
         P00CO5_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P00CO5_A39Contratada_Codigo = new int[1] ;
         P00CO5_n39Contratada_Codigo = new bool[] {false} ;
         P00CO5_A41Contratada_PessoaNom = new String[] {""} ;
         P00CO5_n41Contratada_PessoaNom = new bool[] {false} ;
         P00CO5_A524Contratada_OS = new int[1] ;
         P00CO5_n524Contratada_OS = new bool[] {false} ;
         P00CO5_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00CO5_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00CO5_A518Pessoa_IE = new String[] {""} ;
         P00CO5_n518Pessoa_IE = new bool[] {false} ;
         P00CO5_A519Pessoa_Endereco = new String[] {""} ;
         P00CO5_n519Pessoa_Endereco = new bool[] {false} ;
         P00CO5_A521Pessoa_CEP = new String[] {""} ;
         P00CO5_n521Pessoa_CEP = new bool[] {false} ;
         P00CO5_A522Pessoa_Telefone = new String[] {""} ;
         P00CO5_n522Pessoa_Telefone = new bool[] {false} ;
         P00CO5_A523Pessoa_Fax = new String[] {""} ;
         P00CO5_n523Pessoa_Fax = new bool[] {false} ;
         P00CO5_A520Pessoa_UF = new String[] {""} ;
         P00CO5_n520Pessoa_UF = new bool[] {false} ;
         P00CO5_A26Municipio_Nome = new String[] {""} ;
         P00CO5_n26Municipio_Nome = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A518Pessoa_IE = "";
         A519Pessoa_Endereco = "";
         A521Pessoa_CEP = "";
         A522Pessoa_Telefone = "";
         A523Pessoa_Fax = "";
         A520Pessoa_UF = "";
         A26Municipio_Nome = "";
         AV163Pessoa_IE = "";
         AV160Pessoa_CEP = "";
         AV162Pessoa_Fax = "";
         AV165Pessoa_UF = "";
         AV144Municipio_Nome = "";
         AV185SubTitulo = "";
         AV124Emissao = DateTime.MinValue;
         AV193Volume = "";
         AV187TituloOS = "";
         AV135Identificacao1 = "";
         AV136Identificacao2 = "";
         AV137Identificacao3 = "";
         AV148Os = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_ordemservico__default(),
            new Object[][] {
                new Object[] {
               P00CO4_A1553ContagemResultado_CntSrvCod, P00CO4_n1553ContagemResultado_CntSrvCod, P00CO4_A1603ContagemResultado_CntCod, P00CO4_n1603ContagemResultado_CntCod, P00CO4_A1604ContagemResultado_CntPrpCod, P00CO4_n1604ContagemResultado_CntPrpCod, P00CO4_A1605ContagemResultado_CntPrpPesCod, P00CO4_n1605ContagemResultado_CntPrpPesCod, P00CO4_A601ContagemResultado_Servico, P00CO4_n601ContagemResultado_Servico,
               P00CO4_A456ContagemResultado_Codigo, P00CO4_A1612ContagemResultado_CntNum, P00CO4_n1612ContagemResultado_CntNum, P00CO4_A1606ContagemResultado_CntPrpPesNom, P00CO4_n1606ContagemResultado_CntPrpPesNom, P00CO4_A801ContagemResultado_ServicoSigla, P00CO4_n801ContagemResultado_ServicoSigla, P00CO4_A1452ContagemResultado_SS, P00CO4_n1452ContagemResultado_SS, P00CO4_A489ContagemResultado_SistemaCod,
               P00CO4_n489ContagemResultado_SistemaCod, P00CO4_A509ContagemrResultado_SistemaSigla, P00CO4_n509ContagemrResultado_SistemaSigla, P00CO4_A515ContagemResultado_SistemaCoord, P00CO4_n515ContagemResultado_SistemaCoord, P00CO4_A457ContagemResultado_Demanda, P00CO4_n457ContagemResultado_Demanda, P00CO4_A493ContagemResultado_DemandaFM, P00CO4_n493ContagemResultado_DemandaFM, P00CO4_A1624ContagemResultado_DatVgnInc,
               P00CO4_n1624ContagemResultado_DatVgnInc, P00CO4_A39Contratada_Codigo, P00CO4_n39Contratada_Codigo, P00CO4_A494ContagemResultado_Descricao, P00CO4_n494ContagemResultado_Descricao, P00CO4_A514ContagemResultado_Observacao, P00CO4_n514ContagemResultado_Observacao, P00CO4_A1625ContagemResultado_DatIncTA, P00CO4_n1625ContagemResultado_DatIncTA
               }
               , new Object[] {
               P00CO5_A40Contratada_PessoaCod, P00CO5_A503Pessoa_MunicipioCod, P00CO5_n503Pessoa_MunicipioCod, P00CO5_A39Contratada_Codigo, P00CO5_A41Contratada_PessoaNom, P00CO5_n41Contratada_PessoaNom, P00CO5_A524Contratada_OS, P00CO5_n524Contratada_OS, P00CO5_A42Contratada_PessoaCNPJ, P00CO5_n42Contratada_PessoaCNPJ,
               P00CO5_A518Pessoa_IE, P00CO5_n518Pessoa_IE, P00CO5_A519Pessoa_Endereco, P00CO5_n519Pessoa_Endereco, P00CO5_A521Pessoa_CEP, P00CO5_n521Pessoa_CEP, P00CO5_A522Pessoa_Telefone, P00CO5_n522Pessoa_Telefone, P00CO5_A523Pessoa_Fax, P00CO5_n523Pessoa_Fax,
               P00CO5_A520Pessoa_UF, P00CO5_n520Pessoa_UF, P00CO5_A26Municipio_Nome, P00CO5_n26Municipio_Nome
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV194vrLinha ;
      private short AV201nlin ;
      private short AV134i ;
      private short AV156Pagina ;
      private int AV15ContagemResultado_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A1605ContagemResultado_CntPrpPesCod ;
      private int A601ContagemResultado_Servico ;
      private int A456ContagemResultado_Codigo ;
      private int A1452ContagemResultado_SS ;
      private int A489ContagemResultado_SistemaCod ;
      private int A39Contratada_Codigo ;
      private int OV177SS ;
      private int AV177SS ;
      private int AV174Sistema_Codigo ;
      private int AV199Contratada_Codigo ;
      private int Gx_OldLine ;
      private int A40Contratada_PessoaCod ;
      private int A503Pessoa_MunicipioCod ;
      private int A524Contratada_OS ;
      private int AV101Contratada_OS ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV186TextoLink ;
      private String AV192Verificador ;
      private String AV9AssinadoDtHr ;
      private String Gx_time ;
      private String scmdbuf ;
      private String A1612ContagemResultado_CntNum ;
      private String A1606ContagemResultado_CntPrpPesNom ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String AV106Contrato_Numero ;
      private String AV168Preposto ;
      private String AV200ContagemResultado_ServicoSigla ;
      private String AV176Sistema_Sigla ;
      private String AV103Contratada_PessoaNom ;
      private String AV164Pessoa_Telefone ;
      private String A41Contratada_PessoaNom ;
      private String A518Pessoa_IE ;
      private String A521Pessoa_CEP ;
      private String A522Pessoa_Telefone ;
      private String A523Pessoa_Fax ;
      private String A520Pessoa_UF ;
      private String A26Municipio_Nome ;
      private String AV163Pessoa_IE ;
      private String AV160Pessoa_CEP ;
      private String AV162Pessoa_Fax ;
      private String AV165Pessoa_UF ;
      private String AV144Municipio_Nome ;
      private String AV193Volume ;
      private String AV187TituloOS ;
      private String AV135Identificacao1 ;
      private String AV136Identificacao2 ;
      private String AV137Identificacao3 ;
      private DateTime Gx_date ;
      private DateTime A1624ContagemResultado_DatVgnInc ;
      private DateTime A1625ContagemResultado_DatIncTA ;
      private DateTime AV104Contrato_DataInicioTA ;
      private DateTime AV105Contrato_DataVigenciaInicio ;
      private DateTime AV108DataFim ;
      private DateTime AV109DataInicio ;
      private DateTime AV124Emissao ;
      private bool entryPointCalled ;
      private bool AV157PaginaImpar ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n1605ContagemResultado_CntPrpPesCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n1606ContagemResultado_CntPrpPesNom ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n1452ContagemResultado_SS ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1624ContagemResultado_DatVgnInc ;
      private bool n39Contratada_Codigo ;
      private bool n494ContagemResultado_Descricao ;
      private bool n514ContagemResultado_Observacao ;
      private bool n1625ContagemResultado_DatIncTA ;
      private bool returnInSub ;
      private bool n503Pessoa_MunicipioCod ;
      private bool n41Contratada_PessoaNom ;
      private bool n524Contratada_OS ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n518Pessoa_IE ;
      private bool n519Pessoa_Endereco ;
      private bool n521Pessoa_CEP ;
      private bool n522Pessoa_Telefone ;
      private bool n523Pessoa_Fax ;
      private bool n520Pessoa_UF ;
      private bool n26Municipio_Nome ;
      private String AV170QrCodePath ;
      private String A514ContagemResultado_Observacao ;
      private String AV202ContagemResultado_Observacao ;
      private String AV171QrCodeUrl ;
      private String AV207Qrcode_image_GXI ;
      private String A515ContagemResultado_SistemaCoord ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String AV175Sistema_Coordenacao ;
      private String AV150Os_Header ;
      private String AV102Contratada_PessoaCNPJ ;
      private String AV161Pessoa_Endereco ;
      private String AV110Demanda ;
      private String AV114DmnDescricao ;
      private String AV112Descricao ;
      private String A42Contratada_PessoaCNPJ ;
      private String A519Pessoa_Endereco ;
      private String AV185SubTitulo ;
      private String AV148Os ;
      private String AV169QrCode_Image ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00CO4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00CO4_n1553ContagemResultado_CntSrvCod ;
      private int[] P00CO4_A1603ContagemResultado_CntCod ;
      private bool[] P00CO4_n1603ContagemResultado_CntCod ;
      private int[] P00CO4_A1604ContagemResultado_CntPrpCod ;
      private bool[] P00CO4_n1604ContagemResultado_CntPrpCod ;
      private int[] P00CO4_A1605ContagemResultado_CntPrpPesCod ;
      private bool[] P00CO4_n1605ContagemResultado_CntPrpPesCod ;
      private int[] P00CO4_A601ContagemResultado_Servico ;
      private bool[] P00CO4_n601ContagemResultado_Servico ;
      private int[] P00CO4_A456ContagemResultado_Codigo ;
      private String[] P00CO4_A1612ContagemResultado_CntNum ;
      private bool[] P00CO4_n1612ContagemResultado_CntNum ;
      private String[] P00CO4_A1606ContagemResultado_CntPrpPesNom ;
      private bool[] P00CO4_n1606ContagemResultado_CntPrpPesNom ;
      private String[] P00CO4_A801ContagemResultado_ServicoSigla ;
      private bool[] P00CO4_n801ContagemResultado_ServicoSigla ;
      private int[] P00CO4_A1452ContagemResultado_SS ;
      private bool[] P00CO4_n1452ContagemResultado_SS ;
      private int[] P00CO4_A489ContagemResultado_SistemaCod ;
      private bool[] P00CO4_n489ContagemResultado_SistemaCod ;
      private String[] P00CO4_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00CO4_n509ContagemrResultado_SistemaSigla ;
      private String[] P00CO4_A515ContagemResultado_SistemaCoord ;
      private bool[] P00CO4_n515ContagemResultado_SistemaCoord ;
      private String[] P00CO4_A457ContagemResultado_Demanda ;
      private bool[] P00CO4_n457ContagemResultado_Demanda ;
      private String[] P00CO4_A493ContagemResultado_DemandaFM ;
      private bool[] P00CO4_n493ContagemResultado_DemandaFM ;
      private DateTime[] P00CO4_A1624ContagemResultado_DatVgnInc ;
      private bool[] P00CO4_n1624ContagemResultado_DatVgnInc ;
      private int[] P00CO4_A39Contratada_Codigo ;
      private bool[] P00CO4_n39Contratada_Codigo ;
      private String[] P00CO4_A494ContagemResultado_Descricao ;
      private bool[] P00CO4_n494ContagemResultado_Descricao ;
      private String[] P00CO4_A514ContagemResultado_Observacao ;
      private bool[] P00CO4_n514ContagemResultado_Observacao ;
      private DateTime[] P00CO4_A1625ContagemResultado_DatIncTA ;
      private bool[] P00CO4_n1625ContagemResultado_DatIncTA ;
      private int[] P00CO5_A40Contratada_PessoaCod ;
      private int[] P00CO5_A503Pessoa_MunicipioCod ;
      private bool[] P00CO5_n503Pessoa_MunicipioCod ;
      private int[] P00CO5_A39Contratada_Codigo ;
      private bool[] P00CO5_n39Contratada_Codigo ;
      private String[] P00CO5_A41Contratada_PessoaNom ;
      private bool[] P00CO5_n41Contratada_PessoaNom ;
      private int[] P00CO5_A524Contratada_OS ;
      private bool[] P00CO5_n524Contratada_OS ;
      private String[] P00CO5_A42Contratada_PessoaCNPJ ;
      private bool[] P00CO5_n42Contratada_PessoaCNPJ ;
      private String[] P00CO5_A518Pessoa_IE ;
      private bool[] P00CO5_n518Pessoa_IE ;
      private String[] P00CO5_A519Pessoa_Endereco ;
      private bool[] P00CO5_n519Pessoa_Endereco ;
      private String[] P00CO5_A521Pessoa_CEP ;
      private bool[] P00CO5_n521Pessoa_CEP ;
      private String[] P00CO5_A522Pessoa_Telefone ;
      private bool[] P00CO5_n522Pessoa_Telefone ;
      private String[] P00CO5_A523Pessoa_Fax ;
      private bool[] P00CO5_n523Pessoa_Fax ;
      private String[] P00CO5_A520Pessoa_UF ;
      private bool[] P00CO5_n520Pessoa_UF ;
      private String[] P00CO5_A26Municipio_Nome ;
      private bool[] P00CO5_n26Municipio_Nome ;
      private wwpbaseobjects.SdtWWPContext AV196WWPContext ;
   }

   public class arel_ordemservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CO4 ;
          prmP00CO4 = new Object[] {
          new Object[] {"@AV15ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00CO4 ;
          cmdBufferP00CO4=" SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T3.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T4.[Usuario_PessoaCod] AS ContagemResultado_CntPrpPesCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T3.[Contrato_Numero] AS ContagemResultado_CntNum, T5.[Pessoa_Nome] AS ContagemResultado_CntPrpPesNom, T6.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_SS], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T7.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T7.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T3.[Contrato_DataVigenciaInicio] AS ContagemResultado_DatVgnInc, T3.[Contratada_Codigo], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Observacao], COALESCE( T8.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DatIncTA FROM (((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T3.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Sistema] T7 WITH (NOLOCK) ON T7.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]),  (SELECT T9.[ContratoTermoAditivo_DataInicio], T9.[ContratoTermoAditivo_Codigo], T10.[GXC4] AS GXC4 FROM [ContratoTermoAditivo] T9 WITH (NOLOCK),  (SELECT MAX([ContratoTermoAditivo_Codigo]) "
          + " AS GXC4 FROM [ContratoTermoAditivo] WITH (NOLOCK) ) T10 WHERE T9.[ContratoTermoAditivo_Codigo] = T10.[GXC4] ) T8 WHERE T1.[ContagemResultado_Codigo] = @AV15ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo]" ;
          Object[] prmP00CO5 ;
          prmP00CO5 = new Object[] {
          new Object[] {"@AV199Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CO4", cmdBufferP00CO4,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CO4,1,0,true,true )
             ,new CursorDef("P00CO5", "SELECT TOP 1 T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_OS], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Pessoa_IE], T2.[Pessoa_Endereco], T2.[Pessoa_CEP], T2.[Pessoa_Telefone], T2.[Pessoa_Fax], T3.[Estado_UF] AS Pessoa_UF, T3.[Municipio_Nome] FROM (([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Pessoa_MunicipioCod]) WHERE T1.[Contratada_Codigo] = @AV199Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CO5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((String[]) buf[11])[0] = rslt.getString(7, 20) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getString(12, 25) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((String[]) buf[23])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((String[]) buf[25])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((String[]) buf[33])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((String[]) buf[35])[0] = rslt.getLongVarchar(19) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[37])[0] = rslt.getGXDate(20) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(20);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 2) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
