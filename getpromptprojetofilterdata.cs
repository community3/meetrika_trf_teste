/*
               File: GetPromptProjetoFilterData
        Description: Get Prompt Projeto Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/15/2020 20:47:58.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptprojetofilterdata : GXProcedure
   {
      public getpromptprojetofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptprojetofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptprojetofilterdata objgetpromptprojetofilterdata;
         objgetpromptprojetofilterdata = new getpromptprojetofilterdata();
         objgetpromptprojetofilterdata.AV18DDOName = aP0_DDOName;
         objgetpromptprojetofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetpromptprojetofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptprojetofilterdata.AV22OptionsJson = "" ;
         objgetpromptprojetofilterdata.AV25OptionsDescJson = "" ;
         objgetpromptprojetofilterdata.AV27OptionIndexesJson = "" ;
         objgetpromptprojetofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptprojetofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptprojetofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptprojetofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_PROJETO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADPROJETO_SIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("PromptProjetoGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptProjetoGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("PromptProjetoGridState"), "");
         }
         AV53GXV1 = 1;
         while ( AV53GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV53GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFPROJETO_SIGLA") == 0 )
            {
               AV10TFProjeto_Sigla = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFPROJETO_SIGLA_SEL") == 0 )
            {
               AV11TFProjeto_Sigla_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFPROJETO_PRAZO") == 0 )
            {
               AV12TFProjeto_Prazo = (short)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
               AV13TFProjeto_Prazo_To = (short)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFPROJETO_STATUS_SEL") == 0 )
            {
               AV14TFProjeto_Status_SelsJson = AV32GridStateFilterValue.gxTpr_Value;
               AV15TFProjeto_Status_Sels.FromJSonString(AV14TFProjeto_Status_SelsJson);
            }
            AV53GXV1 = (int)(AV53GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 )
            {
               AV35Projeto_Sigla1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "PROJETO_TIPOCONTAGEM") == 0 )
            {
               AV36Projeto_TipoContagem1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 )
            {
               AV37Projeto_TecnicaContagem1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "PROJETO_STATUS") == 0 )
            {
               AV38Projeto_Status1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV39DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV40DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 )
               {
                  AV41Projeto_Sigla2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "PROJETO_TIPOCONTAGEM") == 0 )
               {
                  AV42Projeto_TipoContagem2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 )
               {
                  AV43Projeto_TecnicaContagem2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "PROJETO_STATUS") == 0 )
               {
                  AV44Projeto_Status2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV45DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV46DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 )
                  {
                     AV47Projeto_Sigla3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "PROJETO_TIPOCONTAGEM") == 0 )
                  {
                     AV48Projeto_TipoContagem3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 )
                  {
                     AV49Projeto_TecnicaContagem3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "PROJETO_STATUS") == 0 )
                  {
                     AV50Projeto_Status3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADPROJETO_SIGLAOPTIONS' Routine */
         AV10TFProjeto_Sigla = AV16SearchTxt;
         AV11TFProjeto_Sigla_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A658Projeto_Status ,
                                              AV15TFProjeto_Status_Sels ,
                                              AV34DynamicFiltersSelector1 ,
                                              AV35Projeto_Sigla1 ,
                                              AV36Projeto_TipoContagem1 ,
                                              AV37Projeto_TecnicaContagem1 ,
                                              AV38Projeto_Status1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41Projeto_Sigla2 ,
                                              AV42Projeto_TipoContagem2 ,
                                              AV43Projeto_TecnicaContagem2 ,
                                              AV44Projeto_Status2 ,
                                              AV45DynamicFiltersEnabled3 ,
                                              AV46DynamicFiltersSelector3 ,
                                              AV47Projeto_Sigla3 ,
                                              AV48Projeto_TipoContagem3 ,
                                              AV49Projeto_TecnicaContagem3 ,
                                              AV50Projeto_Status3 ,
                                              AV11TFProjeto_Sigla_Sel ,
                                              AV10TFProjeto_Sigla ,
                                              AV12TFProjeto_Prazo ,
                                              AV13TFProjeto_Prazo_To ,
                                              AV15TFProjeto_Status_Sels.Count ,
                                              A650Projeto_Sigla ,
                                              A651Projeto_TipoContagem ,
                                              A652Projeto_TecnicaContagem ,
                                              A656Projeto_Prazo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV35Projeto_Sigla1 = StringUtil.PadR( StringUtil.RTrim( AV35Projeto_Sigla1), 15, "%");
         lV41Projeto_Sigla2 = StringUtil.PadR( StringUtil.RTrim( AV41Projeto_Sigla2), 15, "%");
         lV47Projeto_Sigla3 = StringUtil.PadR( StringUtil.RTrim( AV47Projeto_Sigla3), 15, "%");
         lV10TFProjeto_Sigla = StringUtil.PadR( StringUtil.RTrim( AV10TFProjeto_Sigla), 15, "%");
         /* Using cursor P00O53 */
         pr_default.execute(0, new Object[] {lV35Projeto_Sigla1, AV36Projeto_TipoContagem1, AV37Projeto_TecnicaContagem1, AV38Projeto_Status1, lV41Projeto_Sigla2, AV42Projeto_TipoContagem2, AV43Projeto_TecnicaContagem2, AV44Projeto_Status2, lV47Projeto_Sigla3, AV48Projeto_TipoContagem3, AV49Projeto_TecnicaContagem3, AV50Projeto_Status3, lV10TFProjeto_Sigla, AV11TFProjeto_Sigla_Sel, AV12TFProjeto_Prazo, AV13TFProjeto_Prazo_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKO52 = false;
            A650Projeto_Sigla = P00O53_A650Projeto_Sigla[0];
            A658Projeto_Status = P00O53_A658Projeto_Status[0];
            A652Projeto_TecnicaContagem = P00O53_A652Projeto_TecnicaContagem[0];
            A651Projeto_TipoContagem = P00O53_A651Projeto_TipoContagem[0];
            A648Projeto_Codigo = P00O53_A648Projeto_Codigo[0];
            A656Projeto_Prazo = P00O53_A656Projeto_Prazo[0];
            n656Projeto_Prazo = P00O53_n656Projeto_Prazo[0];
            A656Projeto_Prazo = P00O53_A656Projeto_Prazo[0];
            n656Projeto_Prazo = P00O53_n656Projeto_Prazo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00O53_A650Projeto_Sigla[0], A650Projeto_Sigla) == 0 ) )
            {
               BRKO52 = false;
               A648Projeto_Codigo = P00O53_A648Projeto_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKO52 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A650Projeto_Sigla)) )
            {
               AV20Option = A650Projeto_Sigla;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKO52 )
            {
               BRKO52 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFProjeto_Sigla = "";
         AV11TFProjeto_Sigla_Sel = "";
         AV14TFProjeto_Status_SelsJson = "";
         AV15TFProjeto_Status_Sels = new GxSimpleCollection();
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV35Projeto_Sigla1 = "";
         AV36Projeto_TipoContagem1 = "";
         AV37Projeto_TecnicaContagem1 = "";
         AV38Projeto_Status1 = "A";
         AV40DynamicFiltersSelector2 = "";
         AV41Projeto_Sigla2 = "";
         AV42Projeto_TipoContagem2 = "";
         AV43Projeto_TecnicaContagem2 = "";
         AV44Projeto_Status2 = "A";
         AV46DynamicFiltersSelector3 = "";
         AV47Projeto_Sigla3 = "";
         AV48Projeto_TipoContagem3 = "";
         AV49Projeto_TecnicaContagem3 = "";
         AV50Projeto_Status3 = "A";
         scmdbuf = "";
         lV35Projeto_Sigla1 = "";
         lV41Projeto_Sigla2 = "";
         lV47Projeto_Sigla3 = "";
         lV10TFProjeto_Sigla = "";
         A658Projeto_Status = "";
         A650Projeto_Sigla = "";
         A651Projeto_TipoContagem = "";
         A652Projeto_TecnicaContagem = "";
         P00O53_A650Projeto_Sigla = new String[] {""} ;
         P00O53_A658Projeto_Status = new String[] {""} ;
         P00O53_A652Projeto_TecnicaContagem = new String[] {""} ;
         P00O53_A651Projeto_TipoContagem = new String[] {""} ;
         P00O53_A648Projeto_Codigo = new int[1] ;
         P00O53_A656Projeto_Prazo = new short[1] ;
         P00O53_n656Projeto_Prazo = new bool[] {false} ;
         AV20Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptprojetofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00O53_A650Projeto_Sigla, P00O53_A658Projeto_Status, P00O53_A652Projeto_TecnicaContagem, P00O53_A651Projeto_TipoContagem, P00O53_A648Projeto_Codigo, P00O53_A656Projeto_Prazo, P00O53_n656Projeto_Prazo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFProjeto_Prazo ;
      private short AV13TFProjeto_Prazo_To ;
      private short A656Projeto_Prazo ;
      private int AV53GXV1 ;
      private int AV15TFProjeto_Status_Sels_Count ;
      private int A648Projeto_Codigo ;
      private long AV28count ;
      private String AV10TFProjeto_Sigla ;
      private String AV11TFProjeto_Sigla_Sel ;
      private String AV35Projeto_Sigla1 ;
      private String AV36Projeto_TipoContagem1 ;
      private String AV37Projeto_TecnicaContagem1 ;
      private String AV38Projeto_Status1 ;
      private String AV41Projeto_Sigla2 ;
      private String AV42Projeto_TipoContagem2 ;
      private String AV43Projeto_TecnicaContagem2 ;
      private String AV44Projeto_Status2 ;
      private String AV47Projeto_Sigla3 ;
      private String AV48Projeto_TipoContagem3 ;
      private String AV49Projeto_TecnicaContagem3 ;
      private String AV50Projeto_Status3 ;
      private String scmdbuf ;
      private String lV35Projeto_Sigla1 ;
      private String lV41Projeto_Sigla2 ;
      private String lV47Projeto_Sigla3 ;
      private String lV10TFProjeto_Sigla ;
      private String A658Projeto_Status ;
      private String A650Projeto_Sigla ;
      private String A651Projeto_TipoContagem ;
      private String A652Projeto_TecnicaContagem ;
      private bool returnInSub ;
      private bool AV39DynamicFiltersEnabled2 ;
      private bool AV45DynamicFiltersEnabled3 ;
      private bool BRKO52 ;
      private bool n656Projeto_Prazo ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV14TFProjeto_Status_SelsJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV40DynamicFiltersSelector2 ;
      private String AV46DynamicFiltersSelector3 ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00O53_A650Projeto_Sigla ;
      private String[] P00O53_A658Projeto_Status ;
      private String[] P00O53_A652Projeto_TecnicaContagem ;
      private String[] P00O53_A651Projeto_TipoContagem ;
      private int[] P00O53_A648Projeto_Codigo ;
      private short[] P00O53_A656Projeto_Prazo ;
      private bool[] P00O53_n656Projeto_Prazo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV15TFProjeto_Status_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getpromptprojetofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00O53( IGxContext context ,
                                             String A658Projeto_Status ,
                                             IGxCollection AV15TFProjeto_Status_Sels ,
                                             String AV34DynamicFiltersSelector1 ,
                                             String AV35Projeto_Sigla1 ,
                                             String AV36Projeto_TipoContagem1 ,
                                             String AV37Projeto_TecnicaContagem1 ,
                                             String AV38Projeto_Status1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             String AV41Projeto_Sigla2 ,
                                             String AV42Projeto_TipoContagem2 ,
                                             String AV43Projeto_TecnicaContagem2 ,
                                             String AV44Projeto_Status2 ,
                                             bool AV45DynamicFiltersEnabled3 ,
                                             String AV46DynamicFiltersSelector3 ,
                                             String AV47Projeto_Sigla3 ,
                                             String AV48Projeto_TipoContagem3 ,
                                             String AV49Projeto_TecnicaContagem3 ,
                                             String AV50Projeto_Status3 ,
                                             String AV11TFProjeto_Sigla_Sel ,
                                             String AV10TFProjeto_Sigla ,
                                             short AV12TFProjeto_Prazo ,
                                             short AV13TFProjeto_Prazo_To ,
                                             int AV15TFProjeto_Status_Sels_Count ,
                                             String A650Projeto_Sigla ,
                                             String A651Projeto_TipoContagem ,
                                             String A652Projeto_TecnicaContagem ,
                                             short A656Projeto_Prazo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [16] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Projeto_Sigla], T1.[Projeto_Status], T1.[Projeto_TecnicaContagem], T1.[Projeto_TipoContagem], T1.[Projeto_Codigo], COALESCE( T2.[Projeto_Prazo], 0) AS Projeto_Prazo FROM ([Projeto] T1 WITH (NOLOCK) LEFT JOIN (SELECT SUM([Sistema_Prazo]) AS Projeto_Prazo, [Sistema_ProjetoCod] FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T2 ON T2.[Sistema_ProjetoCod] = T1.[Projeto_Codigo])";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Projeto_Sigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV35Projeto_Sigla1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV35Projeto_Sigla1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Projeto_TipoContagem1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV36Projeto_TipoContagem1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV36Projeto_TipoContagem1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Projeto_TecnicaContagem1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV37Projeto_TecnicaContagem1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV37Projeto_TecnicaContagem1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Projeto_Status1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV38Projeto_Status1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV38Projeto_Status1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Projeto_Sigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV41Projeto_Sigla2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV41Projeto_Sigla2 + '%')";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Projeto_TipoContagem2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV42Projeto_TipoContagem2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV42Projeto_TipoContagem2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Projeto_TecnicaContagem2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV43Projeto_TecnicaContagem2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV43Projeto_TecnicaContagem2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Projeto_Status2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV44Projeto_Status2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV44Projeto_Status2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "PROJETO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Projeto_Sigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like '%' + @lV47Projeto_Sigla3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like '%' + @lV47Projeto_Sigla3 + '%')";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "PROJETO_TIPOCONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Projeto_TipoContagem3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TipoContagem] = @AV48Projeto_TipoContagem3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TipoContagem] = @AV48Projeto_TipoContagem3)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "PROJETO_TECNICACONTAGEM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49Projeto_TecnicaContagem3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_TecnicaContagem] = @AV49Projeto_TecnicaContagem3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_TecnicaContagem] = @AV49Projeto_TecnicaContagem3)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "PROJETO_STATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50Projeto_Status3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Status] = @AV50Projeto_Status3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Status] = @AV50Projeto_Status3)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFProjeto_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFProjeto_Sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] like @lV10TFProjeto_Sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] like @lV10TFProjeto_Sigla)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFProjeto_Sigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Projeto_Sigla] = @AV11TFProjeto_Sigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Projeto_Sigla] = @AV11TFProjeto_Sigla_Sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV12TFProjeto_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Prazo], 0) >= @AV12TFProjeto_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Prazo], 0) >= @AV12TFProjeto_Prazo)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV13TFProjeto_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (COALESCE( T2.[Projeto_Prazo], 0) <= @AV13TFProjeto_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (COALESCE( T2.[Projeto_Prazo], 0) <= @AV13TFProjeto_Prazo_To)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV15TFProjeto_Status_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15TFProjeto_Status_Sels, "T1.[Projeto_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV15TFProjeto_Status_Sels, "T1.[Projeto_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Projeto_Sigla]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00O53(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (short)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00O53 ;
          prmP00O53 = new Object[] {
          new Object[] {"@lV35Projeto_Sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV36Projeto_TipoContagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV37Projeto_TecnicaContagem1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV38Projeto_Status1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV41Projeto_Sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV42Projeto_TipoContagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV43Projeto_TecnicaContagem2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV44Projeto_Status2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV47Projeto_Sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV48Projeto_TipoContagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV49Projeto_TecnicaContagem3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV50Projeto_Status3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV10TFProjeto_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV11TFProjeto_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV12TFProjeto_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV13TFProjeto_Prazo_To",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00O53", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00O53,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[31]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptprojetofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptprojetofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptprojetofilterdata") )
          {
             return  ;
          }
          getpromptprojetofilterdata worker = new getpromptprojetofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
