/*
               File: ContagemContagemItemWC
        Description: Contagem Contagem Item WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:48:58.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemcontagemitemwc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemcontagemitemwc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemcontagemitemwc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contagem_Codigo )
      {
         this.AV7Contagem_Codigo = aP0_Contagem_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbFuncaoAPF_Tipo = new GXCombobox();
         cmbContagemItem_TipoUnidade = new GXCombobox();
         cmbFuncaoAPF_Complexidade = new GXCombobox();
         cmbContagemItem_FSTipoUnidade = new GXCombobox();
         cmbContagemItem_FSValidacao = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contagem_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Contagem_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_92 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_92_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_92_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV59Contagem_FabricaSoftwareCod1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59Contagem_FabricaSoftwareCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Contagem_FabricaSoftwareCod1), 6, 0)));
                  AV19FuncaoAPF_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
                  AV60ContagemItem_FSReferenciaTecnicaNom1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ContagemItem_FSReferenciaTecnicaNom1", AV60ContagemItem_FSReferenciaTecnicaNom1);
                  AV61ContagemItem_FMReferenciaTecnicaNom1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ContagemItem_FMReferenciaTecnicaNom1", AV61ContagemItem_FMReferenciaTecnicaNom1);
                  AV21DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
                  AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV62Contagem_FabricaSoftwareCod2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62Contagem_FabricaSoftwareCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Contagem_FabricaSoftwareCod2), 6, 0)));
                  AV24FuncaoAPF_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24FuncaoAPF_Nome2", AV24FuncaoAPF_Nome2);
                  AV63ContagemItem_FSReferenciaTecnicaNom2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63ContagemItem_FSReferenciaTecnicaNom2", AV63ContagemItem_FSReferenciaTecnicaNom2);
                  AV64ContagemItem_FMReferenciaTecnicaNom2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ContagemItem_FMReferenciaTecnicaNom2", AV64ContagemItem_FMReferenciaTecnicaNom2);
                  AV26DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                  AV65Contagem_FabricaSoftwareCod3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65Contagem_FabricaSoftwareCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Contagem_FabricaSoftwareCod3), 6, 0)));
                  AV29FuncaoAPF_Nome3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29FuncaoAPF_Nome3", AV29FuncaoAPF_Nome3);
                  AV66ContagemItem_FSReferenciaTecnicaNom3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ContagemItem_FSReferenciaTecnicaNom3", AV66ContagemItem_FSReferenciaTecnicaNom3);
                  AV67ContagemItem_FMReferenciaTecnicaNom3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemItem_FMReferenciaTecnicaNom3", AV67ContagemItem_FMReferenciaTecnicaNom3);
                  AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
                  AV25DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV7Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contagem_Codigo), 6, 0)));
                  AV260Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV31DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
                  AV30DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A224ContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A192Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n165FuncaoAPF_Codigo = false;
                  AV68FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV59Contagem_FabricaSoftwareCod1, AV19FuncaoAPF_Nome1, AV60ContagemItem_FSReferenciaTecnicaNom1, AV61ContagemItem_FMReferenciaTecnicaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV62Contagem_FabricaSoftwareCod2, AV24FuncaoAPF_Nome2, AV63ContagemItem_FSReferenciaTecnicaNom2, AV64ContagemItem_FMReferenciaTecnicaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV65Contagem_FabricaSoftwareCod3, AV29FuncaoAPF_Nome3, AV66ContagemItem_FSReferenciaTecnicaNom3, AV67ContagemItem_FMReferenciaTecnicaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contagem_Codigo, AV260Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo, A165FuncaoAPF_Codigo, AV68FuncaoAPF_SistemaCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  forbiddenHiddens = sPrefix + "hsh" + "ContagemContagemItemWC";
                  forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9");
                  GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
                  GXUtil.WriteLog("contagemcontagemitemwc:[SendSecurityCheck value for]"+"Contagem_Codigo:"+context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA512( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV260Pgmname = "ContagemContagemItemWC";
               context.Gx_err = 0;
               WS512( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Contagem Item WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812485896");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemcontagemitemwc.aspx") + "?" + UrlEncode("" +AV7Contagem_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEM_FABRICASOFTWARECOD1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59Contagem_FabricaSoftwareCod1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPF_NOME1", AV19FuncaoAPF_Nome1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM1", StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM1", StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEM_FABRICASOFTWARECOD2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62Contagem_FabricaSoftwareCod2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPF_NOME2", AV24FuncaoAPF_Nome2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM2", StringUtil.RTrim( AV63ContagemItem_FSReferenciaTecnicaNom2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM2", StringUtil.RTrim( AV64ContagemItem_FMReferenciaTecnicaNom2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV26DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEM_FABRICASOFTWARECOD3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65Contagem_FabricaSoftwareCod3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPF_NOME3", AV29FuncaoAPF_Nome3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM3", StringUtil.RTrim( AV66ContagemItem_FSReferenciaTecnicaNom3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM3", StringUtil.RTrim( AV67ContagemItem_FMReferenciaTecnicaNom3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV25DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_92", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_92), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV253GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV254GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Contagem_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Contagem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contagem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV260Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV31DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV30DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContagemContagemItemWC";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemcontagemitemwc:[SendSecurityCheck value for]"+"Contagem_Codigo:"+context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm512( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemcontagemitemwc.js", "?202051812485957");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemContagemItemWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Contagem Item WC" ;
      }

      protected void WB510( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemcontagemitemwc.aspx");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_512( true) ;
         }
         else
         {
            wb_table1_2_512( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_512e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContagem_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemContagemItemWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(126, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV25DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(127, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"");
         }
         wbLoad = true;
      }

      protected void START512( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Contagem Item WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP510( ) ;
            }
         }
      }

      protected void WS512( )
      {
         START512( ) ;
         EVT512( ) ;
      }

      protected void EVT512( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11512 */
                                    E11512 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12512 */
                                    E12512 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13512 */
                                    E13512 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14512 */
                                    E14512 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15512 */
                                    E15512 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16512 */
                                    E16512 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17512 */
                                    E17512 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18512 */
                                    E18512 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19512 */
                                    E19512 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20512 */
                                    E20512 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21512 */
                                    E21512 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22512 */
                                    E22512 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP510( ) ;
                              }
                              nGXsfl_92_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
                              SubsflControlProps_922( ) ;
                              AV33Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)) ? AV257Update_GXI : context.convertURL( context.PathToRelativeUrl( AV33Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV258Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              AV38Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV38Display)) ? AV259Display_GXI : context.convertURL( context.PathToRelativeUrl( AV38Display))));
                              A224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_Lancamento_Internalname), ",", "."));
                              A207Contagem_FabricaSoftwareCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_FabricaSoftwareCod_Internalname), ",", "."));
                              A208Contagem_FabricaSoftwarePessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_FabricaSoftwarePessoaCod_Internalname), ",", "."));
                              A209Contagem_FabricaSoftwarePessoaNom = StringUtil.Upper( cgiGet( edtContagem_FabricaSoftwarePessoaNom_Internalname));
                              A216Contagem_UsuarioAuditorCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioAuditorCod_Internalname), ",", "."));
                              A217Contagem_UsuarioAuditorPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioAuditorPessoaCod_Internalname), ",", "."));
                              A218Contagem_UsuarioAuditorPessoaNom = StringUtil.Upper( cgiGet( edtContagem_UsuarioAuditorPessoaNom_Internalname));
                              A225ContagemItem_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_Sequencial_Internalname), ",", "."));
                              n225ContagemItem_Sequencial = false;
                              A229ContagemItem_Requisito = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_Requisito_Internalname), ",", "."));
                              n229ContagemItem_Requisito = false;
                              A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                              n165FuncaoAPF_Codigo = false;
                              A950ContagemItem_PFB = context.localUtil.CToN( cgiGet( edtContagemItem_PFB_Internalname), ",", ".");
                              n950ContagemItem_PFB = false;
                              A951ContagemItem_PFL = context.localUtil.CToN( cgiGet( edtContagemItem_PFL_Internalname), ",", ".");
                              n951ContagemItem_PFL = false;
                              A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
                              cmbFuncaoAPF_Tipo.Name = cmbFuncaoAPF_Tipo_Internalname;
                              cmbFuncaoAPF_Tipo.CurrentValue = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
                              A184FuncaoAPF_Tipo = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
                              cmbContagemItem_TipoUnidade.Name = cmbContagemItem_TipoUnidade_Internalname;
                              cmbContagemItem_TipoUnidade.CurrentValue = cgiGet( cmbContagemItem_TipoUnidade_Internalname);
                              A952ContagemItem_TipoUnidade = cgiGet( cmbContagemItem_TipoUnidade_Internalname);
                              A953ContagemItem_Evidencias = cgiGet( edtContagemItem_Evidencias_Internalname);
                              n953ContagemItem_Evidencias = false;
                              A954ContagemItem_QtdINM = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_QtdINM_Internalname), ",", "."));
                              n954ContagemItem_QtdINM = false;
                              A955ContagemItem_CP = cgiGet( edtContagemItem_CP_Internalname);
                              n955ContagemItem_CP = false;
                              A956ContagemItem_RA = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_RA_Internalname), ",", "."));
                              n956ContagemItem_RA = false;
                              A957ContagemItem_DER = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_DER_Internalname), ",", "."));
                              n957ContagemItem_DER = false;
                              A958ContagemItem_Funcao = cgiGet( edtContagemItem_Funcao_Internalname);
                              n958ContagemItem_Funcao = false;
                              cmbFuncaoAPF_Complexidade.Name = cmbFuncaoAPF_Complexidade_Internalname;
                              cmbFuncaoAPF_Complexidade.CurrentValue = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
                              A185FuncaoAPF_Complexidade = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
                              cmbContagemItem_FSTipoUnidade.Name = cmbContagemItem_FSTipoUnidade_Internalname;
                              cmbContagemItem_FSTipoUnidade.CurrentValue = cgiGet( cmbContagemItem_FSTipoUnidade_Internalname);
                              A228ContagemItem_FSTipoUnidade = cgiGet( cmbContagemItem_FSTipoUnidade_Internalname);
                              n228ContagemItem_FSTipoUnidade = false;
                              cmbContagemItem_FSValidacao.Name = cmbContagemItem_FSValidacao_Internalname;
                              cmbContagemItem_FSValidacao.CurrentValue = cgiGet( cmbContagemItem_FSValidacao_Internalname);
                              A267ContagemItem_FSValidacao = (short)(NumberUtil.Val( cgiGet( cmbContagemItem_FSValidacao_Internalname), "."));
                              n267ContagemItem_FSValidacao = false;
                              A226ContagemItem_FSReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FSReferenciaTecnicaCod_Internalname), ",", "."));
                              n226ContagemItem_FSReferenciaTecnicaCod = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23512 */
                                          E23512 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24512 */
                                          E24512 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25512 */
                                          E25512 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagem_fabricasoftwarecod1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTAGEM_FABRICASOFTWARECOD1"), ",", ".") != Convert.ToDecimal( AV59Contagem_FabricaSoftwareCod1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapf_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_NOME1"), AV19FuncaoAPF_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemitem_fsreferenciatecnicanom1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM1"), AV60ContagemItem_FSReferenciaTecnicaNom1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemitem_fmreferenciatecnicanom1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM1"), AV61ContagemItem_FMReferenciaTecnicaNom1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagem_fabricasoftwarecod2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTAGEM_FABRICASOFTWARECOD2"), ",", ".") != Convert.ToDecimal( AV62Contagem_FabricaSoftwareCod2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapf_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_NOME2"), AV24FuncaoAPF_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemitem_fsreferenciatecnicanom2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM2"), AV63ContagemItem_FSReferenciaTecnicaNom2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemitem_fmreferenciatecnicanom2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM2"), AV64ContagemItem_FMReferenciaTecnicaNom2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagem_fabricasoftwarecod3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTAGEM_FABRICASOFTWARECOD3"), ",", ".") != Convert.ToDecimal( AV65Contagem_FabricaSoftwareCod3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapf_nome3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_NOME3"), AV29FuncaoAPF_Nome3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemitem_fsreferenciatecnicanom3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM3"), AV66ContagemItem_FSReferenciaTecnicaNom3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contagemitem_fmreferenciatecnicanom3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM3"), AV67ContagemItem_FMReferenciaTecnicaNom3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP510( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE512( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm512( ) ;
            }
         }
      }

      protected void PA512( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEM_FABRICASOFTWARECOD", "Contratada F�brica", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_NOME", "Fun��o de Transa��o", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMITEM_FSREFERENCIATECNICANOM", "Tecnica Nom", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMITEM_FMREFERENCIATECNICANOM", "Tecnica Nom", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("4", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("5", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEM_FABRICASOFTWARECOD", "Contratada F�brica", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_NOME", "Fun��o de Transa��o", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMITEM_FSREFERENCIATECNICANOM", "Tecnica Nom", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMITEM_FMREFERENCIATECNICANOM", "Tecnica Nom", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            cmbavDynamicfiltersoperator2.addItem("3", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("4", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("5", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEM_FABRICASOFTWARECOD", "Contratada F�brica", 0);
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPF_NOME", "Fun��o de Transa��o", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMITEM_FSREFERENCIATECNICANOM", "Tecnica Nom", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMITEM_FMREFERENCIATECNICANOM", "Tecnica Nom", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
            cmbavDynamicfiltersoperator3.addItem("3", "<", 0);
            cmbavDynamicfiltersoperator3.addItem("4", "=", 0);
            cmbavDynamicfiltersoperator3.addItem("5", ">", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "FUNCAOAPF_TIPO_" + sGXsfl_92_idx;
            cmbFuncaoAPF_Tipo.Name = GXCCtl;
            cmbFuncaoAPF_Tipo.WebTags = "";
            cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
            cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
            cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
            cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
            if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
            {
               A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            }
            GXCCtl = "CONTAGEMITEM_TIPOUNIDADE_" + sGXsfl_92_idx;
            cmbContagemItem_TipoUnidade.Name = GXCCtl;
            cmbContagemItem_TipoUnidade.WebTags = "";
            if ( cmbContagemItem_TipoUnidade.ItemCount > 0 )
            {
               A952ContagemItem_TipoUnidade = cmbContagemItem_TipoUnidade.getValidValue(A952ContagemItem_TipoUnidade);
            }
            GXCCtl = "FUNCAOAPF_COMPLEXIDADE_" + sGXsfl_92_idx;
            cmbFuncaoAPF_Complexidade.Name = GXCCtl;
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            }
            GXCCtl = "CONTAGEMITEM_FSTIPOUNIDADE_" + sGXsfl_92_idx;
            cmbContagemItem_FSTipoUnidade.Name = GXCCtl;
            cmbContagemItem_FSTipoUnidade.WebTags = "";
            cmbContagemItem_FSTipoUnidade.addItem("EE", "Entrada Externa - EE", 0);
            cmbContagemItem_FSTipoUnidade.addItem("CE", "Consulta Externa - CE", 0);
            cmbContagemItem_FSTipoUnidade.addItem("SE", "Sa�da Externa - SE", 0);
            cmbContagemItem_FSTipoUnidade.addItem("ALI", "Arquivo L�gico Interno - ALI", 0);
            cmbContagemItem_FSTipoUnidade.addItem("aie", "Arquivo de Interface Externa - AIE", 0);
            cmbContagemItem_FSTipoUnidade.addItem("DC", "Dados de C�digo - DC", 0);
            if ( cmbContagemItem_FSTipoUnidade.ItemCount > 0 )
            {
               A228ContagemItem_FSTipoUnidade = cmbContagemItem_FSTipoUnidade.getValidValue(A228ContagemItem_FSTipoUnidade);
               n228ContagemItem_FSTipoUnidade = false;
            }
            GXCCtl = "CONTAGEMITEM_FSVALIDACAO_" + sGXsfl_92_idx;
            cmbContagemItem_FSValidacao.Name = GXCCtl;
            cmbContagemItem_FSValidacao.WebTags = "";
            cmbContagemItem_FSValidacao.addItem("1", "Aprovado", 0);
            cmbContagemItem_FSValidacao.addItem("2", "N�o Aprovado", 0);
            if ( cmbContagemItem_FSValidacao.ItemCount > 0 )
            {
               A267ContagemItem_FSValidacao = (short)(NumberUtil.Val( cmbContagemItem_FSValidacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0))), "."));
               n267ContagemItem_FSValidacao = false;
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_922( ) ;
         while ( nGXsfl_92_idx <= nRC_GXsfl_92 )
         {
            sendrow_922( ) ;
            nGXsfl_92_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_92_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_92_idx+1));
            sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
            SubsflControlProps_922( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       int AV59Contagem_FabricaSoftwareCod1 ,
                                       String AV19FuncaoAPF_Nome1 ,
                                       String AV60ContagemItem_FSReferenciaTecnicaNom1 ,
                                       String AV61ContagemItem_FMReferenciaTecnicaNom1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       int AV62Contagem_FabricaSoftwareCod2 ,
                                       String AV24FuncaoAPF_Nome2 ,
                                       String AV63ContagemItem_FSReferenciaTecnicaNom2 ,
                                       String AV64ContagemItem_FMReferenciaTecnicaNom2 ,
                                       String AV26DynamicFiltersSelector3 ,
                                       short AV27DynamicFiltersOperator3 ,
                                       int AV65Contagem_FabricaSoftwareCod3 ,
                                       String AV29FuncaoAPF_Nome3 ,
                                       String AV66ContagemItem_FSReferenciaTecnicaNom3 ,
                                       String AV67ContagemItem_FMReferenciaTecnicaNom3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV25DynamicFiltersEnabled3 ,
                                       int AV7Contagem_Codigo ,
                                       String AV260Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV31DynamicFiltersIgnoreFirst ,
                                       bool AV30DynamicFiltersRemoving ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A224ContagemItem_Lancamento ,
                                       int A192Contagem_Codigo ,
                                       int A165FuncaoAPF_Codigo ,
                                       int AV68FuncaoAPF_SistemaCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF512( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_LANCAMENTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_LANCAMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_FABRICASOFTWARECOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A207Contagem_FabricaSoftwareCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_FABRICASOFTWARECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_FABRICASOFTWAREPESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_FABRICASOFTWAREPESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_FABRICASOFTWAREPESSOANOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A209Contagem_FabricaSoftwarePessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_FABRICASOFTWAREPESSOANOM", StringUtil.RTrim( A209Contagem_FabricaSoftwarePessoaNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_USUARIOAUDITORCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A216Contagem_UsuarioAuditorCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_USUARIOAUDITORCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_USUARIOAUDITORPESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_USUARIOAUDITORPESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_USUARIOAUDITORPESSOANOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A218Contagem_UsuarioAuditorPessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEM_USUARIOAUDITORPESSOANOM", StringUtil.RTrim( A218Contagem_UsuarioAuditorPessoaNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_SEQUENCIAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A225ContagemItem_Sequencial), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_REQUISITO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A229ContagemItem_Requisito), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_REQUISITO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A229ContagemItem_Requisito), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_PFB", GetSecureSignedToken( sPrefix, context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_PFB", StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_PFL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_PFL", StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_TIPOUNIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_TIPOUNIDADE", A952ContagemItem_TipoUnidade);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_EVIDENCIAS", GetSecureSignedToken( sPrefix, A953ContagemItem_Evidencias));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_EVIDENCIAS", A953ContagemItem_Evidencias);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_QTDINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A954ContagemItem_QtdINM), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_QTDINM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A954ContagemItem_QtdINM), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_CP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A955ContagemItem_CP, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_CP", StringUtil.RTrim( A955ContagemItem_CP));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_RA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_RA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A956ContagemItem_RA), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_DER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A957ContagemItem_DER), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FUNCAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A958ContagemItem_Funcao, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_FUNCAO", A958ContagemItem_Funcao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_COMPLEXIDADE", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSTIPOUNIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A228ContagemItem_FSTipoUnidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_FSTIPOUNIDADE", A228ContagemItem_FSTipoUnidade);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSVALIDACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A267ContagemItem_FSValidacao), "9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_FSVALIDACAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A267ContagemItem_FSValidacao), 1, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSREFERENCIATECNICACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMITEM_FSREFERENCIATECNICACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF512( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV260Pgmname = "ContagemContagemItemWC";
         context.Gx_err = 0;
      }

      protected void RF512( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 92;
         /* Execute user event: E24512 */
         E24512 ();
         nGXsfl_92_idx = 1;
         sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
         SubsflControlProps_922( ) ;
         nGXsfl_92_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_922( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV59Contagem_FabricaSoftwareCod1 ,
                                                 AV19FuncaoAPF_Nome1 ,
                                                 AV60ContagemItem_FSReferenciaTecnicaNom1 ,
                                                 AV61ContagemItem_FMReferenciaTecnicaNom1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV22DynamicFiltersOperator2 ,
                                                 AV62Contagem_FabricaSoftwareCod2 ,
                                                 AV24FuncaoAPF_Nome2 ,
                                                 AV63ContagemItem_FSReferenciaTecnicaNom2 ,
                                                 AV64ContagemItem_FMReferenciaTecnicaNom2 ,
                                                 AV25DynamicFiltersEnabled3 ,
                                                 AV26DynamicFiltersSelector3 ,
                                                 AV27DynamicFiltersOperator3 ,
                                                 AV65Contagem_FabricaSoftwareCod3 ,
                                                 AV29FuncaoAPF_Nome3 ,
                                                 AV66ContagemItem_FSReferenciaTecnicaNom3 ,
                                                 AV67ContagemItem_FMReferenciaTecnicaNom3 ,
                                                 A207Contagem_FabricaSoftwareCod ,
                                                 A166FuncaoAPF_Nome ,
                                                 A269ContagemItem_FSReferenciaTecnicaNom ,
                                                 A257ContagemItem_FMReferenciaTecnicaNom ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A192Contagem_Codigo ,
                                                 AV7Contagem_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV19FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV19FuncaoAPF_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
            lV19FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV19FuncaoAPF_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
            lV60ContagemItem_FSReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ContagemItem_FSReferenciaTecnicaNom1", AV60ContagemItem_FSReferenciaTecnicaNom1);
            lV60ContagemItem_FSReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ContagemItem_FSReferenciaTecnicaNom1", AV60ContagemItem_FSReferenciaTecnicaNom1);
            lV61ContagemItem_FMReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ContagemItem_FMReferenciaTecnicaNom1", AV61ContagemItem_FMReferenciaTecnicaNom1);
            lV61ContagemItem_FMReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ContagemItem_FMReferenciaTecnicaNom1", AV61ContagemItem_FMReferenciaTecnicaNom1);
            lV24FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV24FuncaoAPF_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24FuncaoAPF_Nome2", AV24FuncaoAPF_Nome2);
            lV24FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV24FuncaoAPF_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24FuncaoAPF_Nome2", AV24FuncaoAPF_Nome2);
            lV63ContagemItem_FSReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV63ContagemItem_FSReferenciaTecnicaNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63ContagemItem_FSReferenciaTecnicaNom2", AV63ContagemItem_FSReferenciaTecnicaNom2);
            lV63ContagemItem_FSReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV63ContagemItem_FSReferenciaTecnicaNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63ContagemItem_FSReferenciaTecnicaNom2", AV63ContagemItem_FSReferenciaTecnicaNom2);
            lV64ContagemItem_FMReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV64ContagemItem_FMReferenciaTecnicaNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ContagemItem_FMReferenciaTecnicaNom2", AV64ContagemItem_FMReferenciaTecnicaNom2);
            lV64ContagemItem_FMReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV64ContagemItem_FMReferenciaTecnicaNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ContagemItem_FMReferenciaTecnicaNom2", AV64ContagemItem_FMReferenciaTecnicaNom2);
            lV29FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV29FuncaoAPF_Nome3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29FuncaoAPF_Nome3", AV29FuncaoAPF_Nome3);
            lV29FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV29FuncaoAPF_Nome3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29FuncaoAPF_Nome3", AV29FuncaoAPF_Nome3);
            lV66ContagemItem_FSReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV66ContagemItem_FSReferenciaTecnicaNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ContagemItem_FSReferenciaTecnicaNom3", AV66ContagemItem_FSReferenciaTecnicaNom3);
            lV66ContagemItem_FSReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV66ContagemItem_FSReferenciaTecnicaNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ContagemItem_FSReferenciaTecnicaNom3", AV66ContagemItem_FSReferenciaTecnicaNom3);
            lV67ContagemItem_FMReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV67ContagemItem_FMReferenciaTecnicaNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemItem_FMReferenciaTecnicaNom3", AV67ContagemItem_FMReferenciaTecnicaNom3);
            lV67ContagemItem_FMReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV67ContagemItem_FMReferenciaTecnicaNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemItem_FMReferenciaTecnicaNom3", AV67ContagemItem_FMReferenciaTecnicaNom3);
            /* Using cursor H00512 */
            pr_default.execute(0, new Object[] {AV7Contagem_Codigo, AV59Contagem_FabricaSoftwareCod1, AV59Contagem_FabricaSoftwareCod1, AV59Contagem_FabricaSoftwareCod1, lV19FuncaoAPF_Nome1, lV19FuncaoAPF_Nome1, lV60ContagemItem_FSReferenciaTecnicaNom1, lV60ContagemItem_FSReferenciaTecnicaNom1, lV61ContagemItem_FMReferenciaTecnicaNom1, lV61ContagemItem_FMReferenciaTecnicaNom1, AV62Contagem_FabricaSoftwareCod2, AV62Contagem_FabricaSoftwareCod2, AV62Contagem_FabricaSoftwareCod2, lV24FuncaoAPF_Nome2, lV24FuncaoAPF_Nome2, lV63ContagemItem_FSReferenciaTecnicaNom2, lV63ContagemItem_FSReferenciaTecnicaNom2, lV64ContagemItem_FMReferenciaTecnicaNom2, lV64ContagemItem_FMReferenciaTecnicaNom2, AV65Contagem_FabricaSoftwareCod3, AV65Contagem_FabricaSoftwareCod3, AV65Contagem_FabricaSoftwareCod3, lV29FuncaoAPF_Nome3, lV29FuncaoAPF_Nome3, lV66ContagemItem_FSReferenciaTecnicaNom3, lV66ContagemItem_FSReferenciaTecnicaNom3, lV67ContagemItem_FMReferenciaTecnicaNom3, lV67ContagemItem_FMReferenciaTecnicaNom3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_92_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A255ContagemItem_FMReferenciaTecnicaCod = H00512_A255ContagemItem_FMReferenciaTecnicaCod[0];
               n255ContagemItem_FMReferenciaTecnicaCod = H00512_n255ContagemItem_FMReferenciaTecnicaCod[0];
               A257ContagemItem_FMReferenciaTecnicaNom = H00512_A257ContagemItem_FMReferenciaTecnicaNom[0];
               n257ContagemItem_FMReferenciaTecnicaNom = H00512_n257ContagemItem_FMReferenciaTecnicaNom[0];
               A269ContagemItem_FSReferenciaTecnicaNom = H00512_A269ContagemItem_FSReferenciaTecnicaNom[0];
               n269ContagemItem_FSReferenciaTecnicaNom = H00512_n269ContagemItem_FSReferenciaTecnicaNom[0];
               A192Contagem_Codigo = H00512_A192Contagem_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
               A226ContagemItem_FSReferenciaTecnicaCod = H00512_A226ContagemItem_FSReferenciaTecnicaCod[0];
               n226ContagemItem_FSReferenciaTecnicaCod = H00512_n226ContagemItem_FSReferenciaTecnicaCod[0];
               A267ContagemItem_FSValidacao = H00512_A267ContagemItem_FSValidacao[0];
               n267ContagemItem_FSValidacao = H00512_n267ContagemItem_FSValidacao[0];
               A228ContagemItem_FSTipoUnidade = H00512_A228ContagemItem_FSTipoUnidade[0];
               n228ContagemItem_FSTipoUnidade = H00512_n228ContagemItem_FSTipoUnidade[0];
               A958ContagemItem_Funcao = H00512_A958ContagemItem_Funcao[0];
               n958ContagemItem_Funcao = H00512_n958ContagemItem_Funcao[0];
               A957ContagemItem_DER = H00512_A957ContagemItem_DER[0];
               n957ContagemItem_DER = H00512_n957ContagemItem_DER[0];
               A956ContagemItem_RA = H00512_A956ContagemItem_RA[0];
               n956ContagemItem_RA = H00512_n956ContagemItem_RA[0];
               A955ContagemItem_CP = H00512_A955ContagemItem_CP[0];
               n955ContagemItem_CP = H00512_n955ContagemItem_CP[0];
               A954ContagemItem_QtdINM = H00512_A954ContagemItem_QtdINM[0];
               n954ContagemItem_QtdINM = H00512_n954ContagemItem_QtdINM[0];
               A953ContagemItem_Evidencias = H00512_A953ContagemItem_Evidencias[0];
               n953ContagemItem_Evidencias = H00512_n953ContagemItem_Evidencias[0];
               A952ContagemItem_TipoUnidade = H00512_A952ContagemItem_TipoUnidade[0];
               A184FuncaoAPF_Tipo = H00512_A184FuncaoAPF_Tipo[0];
               A166FuncaoAPF_Nome = H00512_A166FuncaoAPF_Nome[0];
               A951ContagemItem_PFL = H00512_A951ContagemItem_PFL[0];
               n951ContagemItem_PFL = H00512_n951ContagemItem_PFL[0];
               A950ContagemItem_PFB = H00512_A950ContagemItem_PFB[0];
               n950ContagemItem_PFB = H00512_n950ContagemItem_PFB[0];
               A229ContagemItem_Requisito = H00512_A229ContagemItem_Requisito[0];
               n229ContagemItem_Requisito = H00512_n229ContagemItem_Requisito[0];
               A225ContagemItem_Sequencial = H00512_A225ContagemItem_Sequencial[0];
               n225ContagemItem_Sequencial = H00512_n225ContagemItem_Sequencial[0];
               A218Contagem_UsuarioAuditorPessoaNom = H00512_A218Contagem_UsuarioAuditorPessoaNom[0];
               A217Contagem_UsuarioAuditorPessoaCod = H00512_A217Contagem_UsuarioAuditorPessoaCod[0];
               A216Contagem_UsuarioAuditorCod = H00512_A216Contagem_UsuarioAuditorCod[0];
               A209Contagem_FabricaSoftwarePessoaNom = H00512_A209Contagem_FabricaSoftwarePessoaNom[0];
               A208Contagem_FabricaSoftwarePessoaCod = H00512_A208Contagem_FabricaSoftwarePessoaCod[0];
               A207Contagem_FabricaSoftwareCod = H00512_A207Contagem_FabricaSoftwareCod[0];
               A224ContagemItem_Lancamento = H00512_A224ContagemItem_Lancamento[0];
               A165FuncaoAPF_Codigo = H00512_A165FuncaoAPF_Codigo[0];
               n165FuncaoAPF_Codigo = H00512_n165FuncaoAPF_Codigo[0];
               A257ContagemItem_FMReferenciaTecnicaNom = H00512_A257ContagemItem_FMReferenciaTecnicaNom[0];
               n257ContagemItem_FMReferenciaTecnicaNom = H00512_n257ContagemItem_FMReferenciaTecnicaNom[0];
               A269ContagemItem_FSReferenciaTecnicaNom = H00512_A269ContagemItem_FSReferenciaTecnicaNom[0];
               n269ContagemItem_FSReferenciaTecnicaNom = H00512_n269ContagemItem_FSReferenciaTecnicaNom[0];
               A184FuncaoAPF_Tipo = H00512_A184FuncaoAPF_Tipo[0];
               A166FuncaoAPF_Nome = H00512_A166FuncaoAPF_Nome[0];
               GXt_char1 = A185FuncaoAPF_Complexidade;
               new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
               A185FuncaoAPF_Complexidade = GXt_char1;
               /* Execute user event: E25512 */
               E25512 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 92;
            WB510( ) ;
         }
         nGXsfl_92_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV59Contagem_FabricaSoftwareCod1 ,
                                              AV19FuncaoAPF_Nome1 ,
                                              AV60ContagemItem_FSReferenciaTecnicaNom1 ,
                                              AV61ContagemItem_FMReferenciaTecnicaNom1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV22DynamicFiltersOperator2 ,
                                              AV62Contagem_FabricaSoftwareCod2 ,
                                              AV24FuncaoAPF_Nome2 ,
                                              AV63ContagemItem_FSReferenciaTecnicaNom2 ,
                                              AV64ContagemItem_FMReferenciaTecnicaNom2 ,
                                              AV25DynamicFiltersEnabled3 ,
                                              AV26DynamicFiltersSelector3 ,
                                              AV27DynamicFiltersOperator3 ,
                                              AV65Contagem_FabricaSoftwareCod3 ,
                                              AV29FuncaoAPF_Nome3 ,
                                              AV66ContagemItem_FSReferenciaTecnicaNom3 ,
                                              AV67ContagemItem_FMReferenciaTecnicaNom3 ,
                                              A207Contagem_FabricaSoftwareCod ,
                                              A166FuncaoAPF_Nome ,
                                              A269ContagemItem_FSReferenciaTecnicaNom ,
                                              A257ContagemItem_FMReferenciaTecnicaNom ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A192Contagem_Codigo ,
                                              AV7Contagem_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV19FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV19FuncaoAPF_Nome1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
         lV19FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV19FuncaoAPF_Nome1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
         lV60ContagemItem_FSReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ContagemItem_FSReferenciaTecnicaNom1", AV60ContagemItem_FSReferenciaTecnicaNom1);
         lV60ContagemItem_FSReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ContagemItem_FSReferenciaTecnicaNom1", AV60ContagemItem_FSReferenciaTecnicaNom1);
         lV61ContagemItem_FMReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ContagemItem_FMReferenciaTecnicaNom1", AV61ContagemItem_FMReferenciaTecnicaNom1);
         lV61ContagemItem_FMReferenciaTecnicaNom1 = StringUtil.PadR( StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ContagemItem_FMReferenciaTecnicaNom1", AV61ContagemItem_FMReferenciaTecnicaNom1);
         lV24FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV24FuncaoAPF_Nome2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24FuncaoAPF_Nome2", AV24FuncaoAPF_Nome2);
         lV24FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV24FuncaoAPF_Nome2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24FuncaoAPF_Nome2", AV24FuncaoAPF_Nome2);
         lV63ContagemItem_FSReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV63ContagemItem_FSReferenciaTecnicaNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63ContagemItem_FSReferenciaTecnicaNom2", AV63ContagemItem_FSReferenciaTecnicaNom2);
         lV63ContagemItem_FSReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV63ContagemItem_FSReferenciaTecnicaNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63ContagemItem_FSReferenciaTecnicaNom2", AV63ContagemItem_FSReferenciaTecnicaNom2);
         lV64ContagemItem_FMReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV64ContagemItem_FMReferenciaTecnicaNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ContagemItem_FMReferenciaTecnicaNom2", AV64ContagemItem_FMReferenciaTecnicaNom2);
         lV64ContagemItem_FMReferenciaTecnicaNom2 = StringUtil.PadR( StringUtil.RTrim( AV64ContagemItem_FMReferenciaTecnicaNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ContagemItem_FMReferenciaTecnicaNom2", AV64ContagemItem_FMReferenciaTecnicaNom2);
         lV29FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV29FuncaoAPF_Nome3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29FuncaoAPF_Nome3", AV29FuncaoAPF_Nome3);
         lV29FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV29FuncaoAPF_Nome3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29FuncaoAPF_Nome3", AV29FuncaoAPF_Nome3);
         lV66ContagemItem_FSReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV66ContagemItem_FSReferenciaTecnicaNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ContagemItem_FSReferenciaTecnicaNom3", AV66ContagemItem_FSReferenciaTecnicaNom3);
         lV66ContagemItem_FSReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV66ContagemItem_FSReferenciaTecnicaNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ContagemItem_FSReferenciaTecnicaNom3", AV66ContagemItem_FSReferenciaTecnicaNom3);
         lV67ContagemItem_FMReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV67ContagemItem_FMReferenciaTecnicaNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemItem_FMReferenciaTecnicaNom3", AV67ContagemItem_FMReferenciaTecnicaNom3);
         lV67ContagemItem_FMReferenciaTecnicaNom3 = StringUtil.PadR( StringUtil.RTrim( AV67ContagemItem_FMReferenciaTecnicaNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemItem_FMReferenciaTecnicaNom3", AV67ContagemItem_FMReferenciaTecnicaNom3);
         /* Using cursor H00513 */
         pr_default.execute(1, new Object[] {AV7Contagem_Codigo, AV59Contagem_FabricaSoftwareCod1, AV59Contagem_FabricaSoftwareCod1, AV59Contagem_FabricaSoftwareCod1, lV19FuncaoAPF_Nome1, lV19FuncaoAPF_Nome1, lV60ContagemItem_FSReferenciaTecnicaNom1, lV60ContagemItem_FSReferenciaTecnicaNom1, lV61ContagemItem_FMReferenciaTecnicaNom1, lV61ContagemItem_FMReferenciaTecnicaNom1, AV62Contagem_FabricaSoftwareCod2, AV62Contagem_FabricaSoftwareCod2, AV62Contagem_FabricaSoftwareCod2, lV24FuncaoAPF_Nome2, lV24FuncaoAPF_Nome2, lV63ContagemItem_FSReferenciaTecnicaNom2, lV63ContagemItem_FSReferenciaTecnicaNom2, lV64ContagemItem_FMReferenciaTecnicaNom2, lV64ContagemItem_FMReferenciaTecnicaNom2, AV65Contagem_FabricaSoftwareCod3, AV65Contagem_FabricaSoftwareCod3, AV65Contagem_FabricaSoftwareCod3, lV29FuncaoAPF_Nome3, lV29FuncaoAPF_Nome3, lV66ContagemItem_FSReferenciaTecnicaNom3, lV66ContagemItem_FSReferenciaTecnicaNom3, lV67ContagemItem_FMReferenciaTecnicaNom3, lV67ContagemItem_FMReferenciaTecnicaNom3});
         GRID_nRecordCount = H00513_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV59Contagem_FabricaSoftwareCod1, AV19FuncaoAPF_Nome1, AV60ContagemItem_FSReferenciaTecnicaNom1, AV61ContagemItem_FMReferenciaTecnicaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV62Contagem_FabricaSoftwareCod2, AV24FuncaoAPF_Nome2, AV63ContagemItem_FSReferenciaTecnicaNom2, AV64ContagemItem_FMReferenciaTecnicaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV65Contagem_FabricaSoftwareCod3, AV29FuncaoAPF_Nome3, AV66ContagemItem_FSReferenciaTecnicaNom3, AV67ContagemItem_FMReferenciaTecnicaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contagem_Codigo, AV260Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo, A165FuncaoAPF_Codigo, AV68FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV59Contagem_FabricaSoftwareCod1, AV19FuncaoAPF_Nome1, AV60ContagemItem_FSReferenciaTecnicaNom1, AV61ContagemItem_FMReferenciaTecnicaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV62Contagem_FabricaSoftwareCod2, AV24FuncaoAPF_Nome2, AV63ContagemItem_FSReferenciaTecnicaNom2, AV64ContagemItem_FMReferenciaTecnicaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV65Contagem_FabricaSoftwareCod3, AV29FuncaoAPF_Nome3, AV66ContagemItem_FSReferenciaTecnicaNom3, AV67ContagemItem_FMReferenciaTecnicaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contagem_Codigo, AV260Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo, A165FuncaoAPF_Codigo, AV68FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV59Contagem_FabricaSoftwareCod1, AV19FuncaoAPF_Nome1, AV60ContagemItem_FSReferenciaTecnicaNom1, AV61ContagemItem_FMReferenciaTecnicaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV62Contagem_FabricaSoftwareCod2, AV24FuncaoAPF_Nome2, AV63ContagemItem_FSReferenciaTecnicaNom2, AV64ContagemItem_FMReferenciaTecnicaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV65Contagem_FabricaSoftwareCod3, AV29FuncaoAPF_Nome3, AV66ContagemItem_FSReferenciaTecnicaNom3, AV67ContagemItem_FMReferenciaTecnicaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contagem_Codigo, AV260Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo, A165FuncaoAPF_Codigo, AV68FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV59Contagem_FabricaSoftwareCod1, AV19FuncaoAPF_Nome1, AV60ContagemItem_FSReferenciaTecnicaNom1, AV61ContagemItem_FMReferenciaTecnicaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV62Contagem_FabricaSoftwareCod2, AV24FuncaoAPF_Nome2, AV63ContagemItem_FSReferenciaTecnicaNom2, AV64ContagemItem_FMReferenciaTecnicaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV65Contagem_FabricaSoftwareCod3, AV29FuncaoAPF_Nome3, AV66ContagemItem_FSReferenciaTecnicaNom3, AV67ContagemItem_FMReferenciaTecnicaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contagem_Codigo, AV260Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo, A165FuncaoAPF_Codigo, AV68FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV59Contagem_FabricaSoftwareCod1, AV19FuncaoAPF_Nome1, AV60ContagemItem_FSReferenciaTecnicaNom1, AV61ContagemItem_FMReferenciaTecnicaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV62Contagem_FabricaSoftwareCod2, AV24FuncaoAPF_Nome2, AV63ContagemItem_FSReferenciaTecnicaNom2, AV64ContagemItem_FMReferenciaTecnicaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV65Contagem_FabricaSoftwareCod3, AV29FuncaoAPF_Nome3, AV66ContagemItem_FSReferenciaTecnicaNom3, AV67ContagemItem_FMReferenciaTecnicaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contagem_Codigo, AV260Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo, A165FuncaoAPF_Codigo, AV68FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP510( )
      {
         /* Before Start, stand alone formulas. */
         AV260Pgmname = "ContagemContagemItemWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23512 */
         E23512 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEM_FABRICASOFTWARECOD1");
               GX_FocusControl = edtavContagem_fabricasoftwarecod1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59Contagem_FabricaSoftwareCod1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59Contagem_FabricaSoftwareCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Contagem_FabricaSoftwareCod1), 6, 0)));
            }
            else
            {
               AV59Contagem_FabricaSoftwareCod1 = (int)(context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59Contagem_FabricaSoftwareCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Contagem_FabricaSoftwareCod1), 6, 0)));
            }
            AV19FuncaoAPF_Nome1 = cgiGet( edtavFuncaoapf_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
            AV60ContagemItem_FSReferenciaTecnicaNom1 = StringUtil.Upper( cgiGet( edtavContagemitem_fsreferenciatecnicanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ContagemItem_FSReferenciaTecnicaNom1", AV60ContagemItem_FSReferenciaTecnicaNom1);
            AV61ContagemItem_FMReferenciaTecnicaNom1 = StringUtil.Upper( cgiGet( edtavContagemitem_fmreferenciatecnicanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ContagemItem_FMReferenciaTecnicaNom1", AV61ContagemItem_FMReferenciaTecnicaNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEM_FABRICASOFTWARECOD2");
               GX_FocusControl = edtavContagem_fabricasoftwarecod2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62Contagem_FabricaSoftwareCod2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62Contagem_FabricaSoftwareCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Contagem_FabricaSoftwareCod2), 6, 0)));
            }
            else
            {
               AV62Contagem_FabricaSoftwareCod2 = (int)(context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62Contagem_FabricaSoftwareCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Contagem_FabricaSoftwareCod2), 6, 0)));
            }
            AV24FuncaoAPF_Nome2 = cgiGet( edtavFuncaoapf_nome2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24FuncaoAPF_Nome2", AV24FuncaoAPF_Nome2);
            AV63ContagemItem_FSReferenciaTecnicaNom2 = StringUtil.Upper( cgiGet( edtavContagemitem_fsreferenciatecnicanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63ContagemItem_FSReferenciaTecnicaNom2", AV63ContagemItem_FSReferenciaTecnicaNom2);
            AV64ContagemItem_FMReferenciaTecnicaNom2 = StringUtil.Upper( cgiGet( edtavContagemitem_fmreferenciatecnicanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ContagemItem_FMReferenciaTecnicaNom2", AV64ContagemItem_FMReferenciaTecnicaNom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV26DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEM_FABRICASOFTWARECOD3");
               GX_FocusControl = edtavContagem_fabricasoftwarecod3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65Contagem_FabricaSoftwareCod3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65Contagem_FabricaSoftwareCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Contagem_FabricaSoftwareCod3), 6, 0)));
            }
            else
            {
               AV65Contagem_FabricaSoftwareCod3 = (int)(context.localUtil.CToN( cgiGet( edtavContagem_fabricasoftwarecod3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65Contagem_FabricaSoftwareCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Contagem_FabricaSoftwareCod3), 6, 0)));
            }
            AV29FuncaoAPF_Nome3 = cgiGet( edtavFuncaoapf_nome3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29FuncaoAPF_Nome3", AV29FuncaoAPF_Nome3);
            AV66ContagemItem_FSReferenciaTecnicaNom3 = StringUtil.Upper( cgiGet( edtavContagemitem_fsreferenciatecnicanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ContagemItem_FSReferenciaTecnicaNom3", AV66ContagemItem_FSReferenciaTecnicaNom3);
            AV67ContagemItem_FMReferenciaTecnicaNom3 = StringUtil.Upper( cgiGet( edtavContagemitem_fmreferenciatecnicanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemItem_FMReferenciaTecnicaNom3", AV67ContagemItem_FMReferenciaTecnicaNom3);
            A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV25DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_92 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_92"), ",", "."));
            AV253GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV254GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contagem_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContagemContagemItemWC";
            A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contagemcontagemitemwc:[SecurityCheckFailed value for]"+"Contagem_Codigo:"+context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTAGEM_FABRICASOFTWARECOD1"), ",", ".") != Convert.ToDecimal( AV59Contagem_FabricaSoftwareCod1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_NOME1"), AV19FuncaoAPF_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM1"), AV60ContagemItem_FSReferenciaTecnicaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM1"), AV61ContagemItem_FMReferenciaTecnicaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTAGEM_FABRICASOFTWARECOD2"), ",", ".") != Convert.ToDecimal( AV62Contagem_FabricaSoftwareCod2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_NOME2"), AV24FuncaoAPF_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM2"), AV63ContagemItem_FSReferenciaTecnicaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM2"), AV64ContagemItem_FMReferenciaTecnicaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTAGEM_FABRICASOFTWARECOD3"), ",", ".") != Convert.ToDecimal( AV65Contagem_FabricaSoftwareCod3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_NOME3"), AV29FuncaoAPF_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMITEM_FSREFERENCIATECNICANOM3"), AV66ContagemItem_FSReferenciaTecnicaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTAGEMITEM_FMREFERENCIATECNICANOM3"), AV67ContagemItem_FMReferenciaTecnicaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23512 */
         E23512 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23512( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CONTAGEM_FABRICASOFTWARECOD";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "CONTAGEM_FABRICASOFTWARECOD";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersSelector3 = "CONTAGEM_FABRICASOFTWARECOD";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtContagem_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Contratada F�brica", 0);
         cmbavOrderedby.addItem("2", "Item do Lan�amento", 0);
         cmbavOrderedby.addItem("3", "Pessoa Contratada", 0);
         cmbavOrderedby.addItem("4", "F�brica )", 0);
         cmbavOrderedby.addItem("5", "Usu�rio Auditor", 0);
         cmbavOrderedby.addItem("6", "Pessoa Auditor", 0);
         cmbavOrderedby.addItem("7", "do Auditor", 0);
         cmbavOrderedby.addItem("8", "Sequencial", 0);
         cmbavOrderedby.addItem("9", "de Software)", 0);
         cmbavOrderedby.addItem("10", "Fun��o de Transa��o", 0);
         cmbavOrderedby.addItem("11", "PFB", 0);
         cmbavOrderedby.addItem("12", "PFL", 0);
         cmbavOrderedby.addItem("13", "Fun��o de Transa��o", 0);
         cmbavOrderedby.addItem("14", "Tipo", 0);
         cmbavOrderedby.addItem("15", "Tipo", 0);
         cmbavOrderedby.addItem("16", "Evidencias", 0);
         cmbavOrderedby.addItem("17", "INM", 0);
         cmbavOrderedby.addItem("18", "CP", 0);
         cmbavOrderedby.addItem("19", "A", 0);
         cmbavOrderedby.addItem("20", "DER", 0);
         cmbavOrderedby.addItem("21", "de Dados", 0);
         cmbavOrderedby.addItem("22", "F�b. Software)", 0);
         cmbavOrderedby.addItem("23", "de Software", 0);
         cmbavOrderedby.addItem("24", "FS Cod.", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E24512( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("4", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("5", ">", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
               cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "<", 0);
               cmbavDynamicfiltersoperator2.addItem("4", "=", 0);
               cmbavDynamicfiltersoperator2.addItem("5", ">", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            if ( AV25DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "<", 0);
                  cmbavDynamicfiltersoperator3.addItem("4", "=", 0);
                  cmbavDynamicfiltersoperator3.addItem("5", ">", 0);
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemItem_Lancamento_Titleformat = 2;
         edtContagemItem_Lancamento_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV14OrderedBy==2) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Item do Lan�amento", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_Lancamento_Internalname, "Title", edtContagemItem_Lancamento_Title);
         edtContagem_FabricaSoftwareCod_Titleformat = 2;
         edtContagem_FabricaSoftwareCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV14OrderedBy==1) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Contratada F�brica", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_FabricaSoftwareCod_Internalname, "Title", edtContagem_FabricaSoftwareCod_Title);
         edtContagem_FabricaSoftwarePessoaCod_Titleformat = 2;
         edtContagem_FabricaSoftwarePessoaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV14OrderedBy==3) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Pessoa Contratada", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_FabricaSoftwarePessoaCod_Internalname, "Title", edtContagem_FabricaSoftwarePessoaCod_Title);
         edtContagem_FabricaSoftwarePessoaNom_Titleformat = 2;
         edtContagem_FabricaSoftwarePessoaNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV14OrderedBy==4) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "F�brica )", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_FabricaSoftwarePessoaNom_Internalname, "Title", edtContagem_FabricaSoftwarePessoaNom_Title);
         edtContagem_UsuarioAuditorCod_Titleformat = 2;
         edtContagem_UsuarioAuditorCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV14OrderedBy==5) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Usu�rio Auditor", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_UsuarioAuditorCod_Internalname, "Title", edtContagem_UsuarioAuditorCod_Title);
         edtContagem_UsuarioAuditorPessoaCod_Titleformat = 2;
         edtContagem_UsuarioAuditorPessoaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV14OrderedBy==6) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Pessoa Auditor", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_UsuarioAuditorPessoaCod_Internalname, "Title", edtContagem_UsuarioAuditorPessoaCod_Title);
         edtContagem_UsuarioAuditorPessoaNom_Titleformat = 2;
         edtContagem_UsuarioAuditorPessoaNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV14OrderedBy==7) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "do Auditor", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_UsuarioAuditorPessoaNom_Internalname, "Title", edtContagem_UsuarioAuditorPessoaNom_Title);
         edtContagemItem_Sequencial_Titleformat = 2;
         edtContagemItem_Sequencial_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV14OrderedBy==8) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Sequencial", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_Sequencial_Internalname, "Title", edtContagemItem_Sequencial_Title);
         edtContagemItem_Requisito_Titleformat = 2;
         edtContagemItem_Requisito_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV14OrderedBy==9) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "de Software)", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_Requisito_Internalname, "Title", edtContagemItem_Requisito_Title);
         edtFuncaoAPF_Codigo_Titleformat = 2;
         edtFuncaoAPF_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 10);' >%5</span>", ((AV14OrderedBy==10) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Fun��o de Transa��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_Codigo_Internalname, "Title", edtFuncaoAPF_Codigo_Title);
         edtContagemItem_PFB_Titleformat = 2;
         edtContagemItem_PFB_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 11);' >%5</span>", ((AV14OrderedBy==11) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "PFB", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_PFB_Internalname, "Title", edtContagemItem_PFB_Title);
         edtContagemItem_PFL_Titleformat = 2;
         edtContagemItem_PFL_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 12);' >%5</span>", ((AV14OrderedBy==12) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "PFL", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_PFL_Internalname, "Title", edtContagemItem_PFL_Title);
         edtFuncaoAPF_Nome_Titleformat = 2;
         edtFuncaoAPF_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 13);' >%5</span>", ((AV14OrderedBy==13) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Fun��o de Transa��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_Nome_Internalname, "Title", edtFuncaoAPF_Nome_Title);
         cmbFuncaoAPF_Tipo_Titleformat = 2;
         cmbFuncaoAPF_Tipo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 14);' >%5</span>", ((AV14OrderedBy==14) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tipo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Tipo_Internalname, "Title", cmbFuncaoAPF_Tipo.Title.Text);
         cmbContagemItem_TipoUnidade_Titleformat = 2;
         cmbContagemItem_TipoUnidade.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 15);' >%5</span>", ((AV14OrderedBy==15) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tipo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemItem_TipoUnidade_Internalname, "Title", cmbContagemItem_TipoUnidade.Title.Text);
         edtContagemItem_Evidencias_Titleformat = 2;
         edtContagemItem_Evidencias_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 16);' >%5</span>", ((AV14OrderedBy==16) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Evidencias", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_Evidencias_Internalname, "Title", edtContagemItem_Evidencias_Title);
         edtContagemItem_QtdINM_Titleformat = 2;
         edtContagemItem_QtdINM_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 17);' >%5</span>", ((AV14OrderedBy==17) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "INM", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_QtdINM_Internalname, "Title", edtContagemItem_QtdINM_Title);
         edtContagemItem_CP_Titleformat = 2;
         edtContagemItem_CP_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 18);' >%5</span>", ((AV14OrderedBy==18) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "CP", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_CP_Internalname, "Title", edtContagemItem_CP_Title);
         edtContagemItem_RA_Titleformat = 2;
         edtContagemItem_RA_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 19);' >%5</span>", ((AV14OrderedBy==19) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "A", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_RA_Internalname, "Title", edtContagemItem_RA_Title);
         edtContagemItem_DER_Titleformat = 2;
         edtContagemItem_DER_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 20);' >%5</span>", ((AV14OrderedBy==20) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "DER", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_DER_Internalname, "Title", edtContagemItem_DER_Title);
         edtContagemItem_Funcao_Titleformat = 2;
         edtContagemItem_Funcao_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 21);' >%5</span>", ((AV14OrderedBy==21) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "de Dados", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_Funcao_Internalname, "Title", edtContagemItem_Funcao_Title);
         cmbContagemItem_FSTipoUnidade_Titleformat = 2;
         cmbContagemItem_FSTipoUnidade.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 22);' >%5</span>", ((AV14OrderedBy==22) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "F�b. Software)", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemItem_FSTipoUnidade_Internalname, "Title", cmbContagemItem_FSTipoUnidade.Title.Text);
         cmbContagemItem_FSValidacao_Titleformat = 2;
         cmbContagemItem_FSValidacao.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 23);' >%5</span>", ((AV14OrderedBy==23) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "de Software", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemItem_FSValidacao_Internalname, "Title", cmbContagemItem_FSValidacao.Title.Text);
         edtContagemItem_FSReferenciaTecnicaCod_Titleformat = 2;
         edtContagemItem_FSReferenciaTecnicaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 24);' >%5</span>", ((AV14OrderedBy==24) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "FS Cod.", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_FSReferenciaTecnicaCod_Internalname, "Title", edtContagemItem_FSReferenciaTecnicaCod_Title);
         AV253GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV253GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV253GridCurrentPage), 10, 0)));
         AV254GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV254GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV254GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11512( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV252PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV252PageToGo) ;
         }
      }

      private void E25512( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("contagemitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A224ContagemItem_Lancamento) + "," + UrlEncode("" +A192Contagem_Codigo);
            AV33Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV33Update);
            AV257Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV33Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV33Update);
            AV257Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("contagemitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A224ContagemItem_Lancamento) + "," + UrlEncode("" +A192Contagem_Codigo);
            AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV32Delete);
            AV258Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV32Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV32Delete);
            AV258Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewcontagemitem.aspx") + "?" + UrlEncode("" +A224ContagemItem_Lancamento) + "," + UrlEncode(StringUtil.RTrim(""));
            AV38Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV38Display);
            AV259Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV38Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV38Display);
            AV259Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtContagem_FabricaSoftwareCod_Link = formatLink("viewcontagemitem.aspx") + "?" + UrlEncode("" +A224ContagemItem_Lancamento) + "," + UrlEncode(StringUtil.RTrim(""));
         edtFuncaoAPF_Nome_Link = formatLink("viewfuncaoapf.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV68FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 92;
         }
         sendrow_922( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_92_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(92, GridRow);
         }
      }

      protected void E12512( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E18512( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV59Contagem_FabricaSoftwareCod1, AV19FuncaoAPF_Nome1, AV60ContagemItem_FSReferenciaTecnicaNom1, AV61ContagemItem_FMReferenciaTecnicaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV62Contagem_FabricaSoftwareCod2, AV24FuncaoAPF_Nome2, AV63ContagemItem_FSReferenciaTecnicaNom2, AV64ContagemItem_FMReferenciaTecnicaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV65Contagem_FabricaSoftwareCod3, AV29FuncaoAPF_Nome3, AV66ContagemItem_FSReferenciaTecnicaNom3, AV67ContagemItem_FMReferenciaTecnicaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contagem_Codigo, AV260Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo, A165FuncaoAPF_Codigo, AV68FuncaoAPF_SistemaCod, sPrefix) ;
      }

      protected void E13512( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV59Contagem_FabricaSoftwareCod1, AV19FuncaoAPF_Nome1, AV60ContagemItem_FSReferenciaTecnicaNom1, AV61ContagemItem_FMReferenciaTecnicaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV62Contagem_FabricaSoftwareCod2, AV24FuncaoAPF_Nome2, AV63ContagemItem_FSReferenciaTecnicaNom2, AV64ContagemItem_FMReferenciaTecnicaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV65Contagem_FabricaSoftwareCod3, AV29FuncaoAPF_Nome3, AV66ContagemItem_FSReferenciaTecnicaNom3, AV67ContagemItem_FMReferenciaTecnicaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contagem_Codigo, AV260Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo, A165FuncaoAPF_Codigo, AV68FuncaoAPF_SistemaCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19512( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20512( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV25DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV59Contagem_FabricaSoftwareCod1, AV19FuncaoAPF_Nome1, AV60ContagemItem_FSReferenciaTecnicaNom1, AV61ContagemItem_FMReferenciaTecnicaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV62Contagem_FabricaSoftwareCod2, AV24FuncaoAPF_Nome2, AV63ContagemItem_FSReferenciaTecnicaNom2, AV64ContagemItem_FMReferenciaTecnicaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV65Contagem_FabricaSoftwareCod3, AV29FuncaoAPF_Nome3, AV66ContagemItem_FSReferenciaTecnicaNom3, AV67ContagemItem_FMReferenciaTecnicaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contagem_Codigo, AV260Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo, A165FuncaoAPF_Codigo, AV68FuncaoAPF_SistemaCod, sPrefix) ;
      }

      protected void E14512( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV59Contagem_FabricaSoftwareCod1, AV19FuncaoAPF_Nome1, AV60ContagemItem_FSReferenciaTecnicaNom1, AV61ContagemItem_FMReferenciaTecnicaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV62Contagem_FabricaSoftwareCod2, AV24FuncaoAPF_Nome2, AV63ContagemItem_FSReferenciaTecnicaNom2, AV64ContagemItem_FMReferenciaTecnicaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV65Contagem_FabricaSoftwareCod3, AV29FuncaoAPF_Nome3, AV66ContagemItem_FSReferenciaTecnicaNom3, AV67ContagemItem_FMReferenciaTecnicaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contagem_Codigo, AV260Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo, A165FuncaoAPF_Codigo, AV68FuncaoAPF_SistemaCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21512( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E15512( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV59Contagem_FabricaSoftwareCod1, AV19FuncaoAPF_Nome1, AV60ContagemItem_FSReferenciaTecnicaNom1, AV61ContagemItem_FMReferenciaTecnicaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV62Contagem_FabricaSoftwareCod2, AV24FuncaoAPF_Nome2, AV63ContagemItem_FSReferenciaTecnicaNom2, AV64ContagemItem_FMReferenciaTecnicaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV65Contagem_FabricaSoftwareCod3, AV29FuncaoAPF_Nome3, AV66ContagemItem_FSReferenciaTecnicaNom3, AV67ContagemItem_FMReferenciaTecnicaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contagem_Codigo, AV260Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo, A165FuncaoAPF_Codigo, AV68FuncaoAPF_SistemaCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E22512( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E16512( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E17512( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contagemitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV7Contagem_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContagem_fabricasoftwarecod1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagem_fabricasoftwarecod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_fabricasoftwarecod1_Visible), 5, 0)));
         edtavFuncaoapf_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
         edtavContagemitem_fsreferenciatecnicanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemitem_fsreferenciatecnicanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fsreferenciatecnicanom1_Visible), 5, 0)));
         edtavContagemitem_fmreferenciatecnicanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemitem_fmreferenciatecnicanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fmreferenciatecnicanom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
         {
            edtavContagem_fabricasoftwarecod1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagem_fabricasoftwarecod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_fabricasoftwarecod1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
         {
            edtavContagemitem_fsreferenciatecnicanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemitem_fsreferenciatecnicanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fsreferenciatecnicanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
         {
            edtavContagemitem_fmreferenciatecnicanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemitem_fmreferenciatecnicanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fmreferenciatecnicanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContagem_fabricasoftwarecod2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagem_fabricasoftwarecod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_fabricasoftwarecod2_Visible), 5, 0)));
         edtavFuncaoapf_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
         edtavContagemitem_fsreferenciatecnicanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemitem_fsreferenciatecnicanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fsreferenciatecnicanom2_Visible), 5, 0)));
         edtavContagemitem_fmreferenciatecnicanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemitem_fmreferenciatecnicanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fmreferenciatecnicanom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
         {
            edtavContagem_fabricasoftwarecod2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagem_fabricasoftwarecod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_fabricasoftwarecod2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
         {
            edtavContagemitem_fsreferenciatecnicanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemitem_fsreferenciatecnicanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fsreferenciatecnicanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
         {
            edtavContagemitem_fmreferenciatecnicanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemitem_fmreferenciatecnicanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fmreferenciatecnicanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContagem_fabricasoftwarecod3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagem_fabricasoftwarecod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_fabricasoftwarecod3_Visible), 5, 0)));
         edtavFuncaoapf_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome3_Visible), 5, 0)));
         edtavContagemitem_fsreferenciatecnicanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemitem_fsreferenciatecnicanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fsreferenciatecnicanom3_Visible), 5, 0)));
         edtavContagemitem_fmreferenciatecnicanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemitem_fmreferenciatecnicanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fmreferenciatecnicanom3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
         {
            edtavContagem_fabricasoftwarecod3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagem_fabricasoftwarecod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagem_fabricasoftwarecod3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
         {
            edtavContagemitem_fsreferenciatecnicanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemitem_fsreferenciatecnicanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fsreferenciatecnicanom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
         {
            edtavContagemitem_fmreferenciatecnicanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemitem_fmreferenciatecnicanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemitem_fmreferenciatecnicanom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "CONTAGEM_FABRICASOFTWARECOD";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV62Contagem_FabricaSoftwareCod2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62Contagem_FabricaSoftwareCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Contagem_FabricaSoftwareCod2), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         AV26DynamicFiltersSelector3 = "CONTAGEM_FABRICASOFTWARECOD";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         AV65Contagem_FabricaSoftwareCod3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65Contagem_FabricaSoftwareCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Contagem_FabricaSoftwareCod3), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV16DynamicFiltersSelector1 = "CONTAGEM_FABRICASOFTWARECOD";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV59Contagem_FabricaSoftwareCod1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59Contagem_FabricaSoftwareCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Contagem_FabricaSoftwareCod1), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV34Session.Get(AV260Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV260Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV34Session.Get(AV260Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV59Contagem_FabricaSoftwareCod1 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59Contagem_FabricaSoftwareCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Contagem_FabricaSoftwareCod1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV19FuncaoAPF_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV60ContagemItem_FSReferenciaTecnicaNom1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60ContagemItem_FSReferenciaTecnicaNom1", AV60ContagemItem_FSReferenciaTecnicaNom1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV61ContagemItem_FMReferenciaTecnicaNom1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ContagemItem_FMReferenciaTecnicaNom1", AV61ContagemItem_FMReferenciaTecnicaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV62Contagem_FabricaSoftwareCod2 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62Contagem_FabricaSoftwareCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Contagem_FabricaSoftwareCod2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24FuncaoAPF_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24FuncaoAPF_Nome2", AV24FuncaoAPF_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV63ContagemItem_FSReferenciaTecnicaNom2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63ContagemItem_FSReferenciaTecnicaNom2", AV63ContagemItem_FSReferenciaTecnicaNom2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV64ContagemItem_FMReferenciaTecnicaNom2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ContagemItem_FMReferenciaTecnicaNom2", AV64ContagemItem_FMReferenciaTecnicaNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV25DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV26DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV65Contagem_FabricaSoftwareCod3 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65Contagem_FabricaSoftwareCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Contagem_FabricaSoftwareCod3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV29FuncaoAPF_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29FuncaoAPF_Nome3", AV29FuncaoAPF_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV66ContagemItem_FSReferenciaTecnicaNom3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ContagemItem_FSReferenciaTecnicaNom3", AV66ContagemItem_FSReferenciaTecnicaNom3);
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV67ContagemItem_FMReferenciaTecnicaNom3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67ContagemItem_FMReferenciaTecnicaNom3", AV67ContagemItem_FMReferenciaTecnicaNom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV30DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV34Session.Get(AV260Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV260Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV31DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ! (0==AV59Contagem_FabricaSoftwareCod1) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV59Contagem_FabricaSoftwareCod1), 6, 0);
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPF_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19FuncaoAPF_Nome1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV60ContagemItem_FSReferenciaTecnicaNom1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV61ContagemItem_FMReferenciaTecnicaNom1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ! (0==AV62Contagem_FabricaSoftwareCod2) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV62Contagem_FabricaSoftwareCod2), 6, 0);
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPF_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV24FuncaoAPF_Nome2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemItem_FSReferenciaTecnicaNom2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV63ContagemItem_FSReferenciaTecnicaNom2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemItem_FMReferenciaTecnicaNom2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV64ContagemItem_FMReferenciaTecnicaNom2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV25DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV26DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ! (0==AV65Contagem_FabricaSoftwareCod3) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV65Contagem_FabricaSoftwareCod3), 6, 0);
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29FuncaoAPF_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV29FuncaoAPF_Nome3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV66ContagemItem_FSReferenciaTecnicaNom3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV66ContagemItem_FSReferenciaTecnicaNom3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV67ContagemItem_FMReferenciaTecnicaNom3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV67ContagemItem_FMReferenciaTecnicaNom3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV260Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContagemItem";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Contagem_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Contagem_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV34Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_512( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_512( true) ;
         }
         else
         {
            wb_table2_8_512( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_512e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_89_512( true) ;
         }
         else
         {
            wb_table3_89_512( false) ;
         }
         return  ;
      }

      protected void wb_table3_89_512e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_512e( true) ;
         }
         else
         {
            wb_table1_2_512e( false) ;
         }
      }

      protected void wb_table3_89_512( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"92\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_Lancamento_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_Lancamento_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_Lancamento_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_FabricaSoftwareCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_FabricaSoftwareCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_FabricaSoftwareCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_FabricaSoftwarePessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_FabricaSoftwarePessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_FabricaSoftwarePessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_FabricaSoftwarePessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_FabricaSoftwarePessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_FabricaSoftwarePessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_UsuarioAuditorCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_UsuarioAuditorCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_UsuarioAuditorCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_UsuarioAuditorPessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_UsuarioAuditorPessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_UsuarioAuditorPessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_UsuarioAuditorPessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_UsuarioAuditorPessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_UsuarioAuditorPessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_Sequencial_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_Sequencial_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_Sequencial_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_Requisito_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_Requisito_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_Requisito_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_PFB_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_PFB_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_PFB_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_PFL_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_PFL_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_PFL_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoAPF_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoAPF_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoAPF_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemItem_TipoUnidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemItem_TipoUnidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemItem_TipoUnidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_Evidencias_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_Evidencias_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_Evidencias_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_QtdINM_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_QtdINM_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_QtdINM_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_CP_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_CP_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_CP_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_RA_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_RA_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_RA_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_DER_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_DER_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_DER_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_Funcao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_Funcao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_Funcao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Complexidade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemItem_FSTipoUnidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemItem_FSTipoUnidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemItem_FSTipoUnidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemItem_FSValidacao_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemItem_FSValidacao.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemItem_FSValidacao.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_FSReferenciaTecnicaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_FSReferenciaTecnicaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_FSReferenciaTecnicaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV33Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV38Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_Lancamento_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_Lancamento_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_FabricaSoftwareCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_FabricaSoftwareCod_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagem_FabricaSoftwareCod_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_FabricaSoftwarePessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_FabricaSoftwarePessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A209Contagem_FabricaSoftwarePessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_FabricaSoftwarePessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_FabricaSoftwarePessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_UsuarioAuditorCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_UsuarioAuditorCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_UsuarioAuditorPessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_UsuarioAuditorPessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A218Contagem_UsuarioAuditorPessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_UsuarioAuditorPessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_UsuarioAuditorPessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A225ContagemItem_Sequencial), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_Sequencial_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_Sequencial_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A229ContagemItem_Requisito), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_Requisito_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_Requisito_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_PFB_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_PFB_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_PFL_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_PFL_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A166FuncaoAPF_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtFuncaoAPF_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A184FuncaoAPF_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoAPF_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoAPF_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A952ContagemItem_TipoUnidade);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemItem_TipoUnidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemItem_TipoUnidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A953ContagemItem_Evidencias);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_Evidencias_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_Evidencias_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A954ContagemItem_QtdINM), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_QtdINM_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_QtdINM_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A955ContagemItem_CP));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_CP_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_CP_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A956ContagemItem_RA), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_RA_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_RA_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A957ContagemItem_DER), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_DER_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_DER_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A958ContagemItem_Funcao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_Funcao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_Funcao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A228ContagemItem_FSTipoUnidade);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemItem_FSTipoUnidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemItem_FSTipoUnidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A267ContagemItem_FSValidacao), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemItem_FSValidacao.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemItem_FSValidacao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_FSReferenciaTecnicaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_FSReferenciaTecnicaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 92 )
         {
            wbEnd = 0;
            nRC_GXsfl_92 = (short)(nGXsfl_92_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_89_512e( true) ;
         }
         else
         {
            wb_table3_89_512e( false) ;
         }
      }

      protected void wb_table2_8_512( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_512( true) ;
         }
         else
         {
            wb_table4_11_512( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_512e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_ContagemContagemItemWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_512( true) ;
         }
         else
         {
            wb_table5_21_512( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_512e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_512e( true) ;
         }
         else
         {
            wb_table2_8_512e( false) ;
         }
      }

      protected void wb_table5_21_512( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_26_512( true) ;
         }
         else
         {
            wb_table6_26_512( false) ;
         }
         return  ;
      }

      protected void wb_table6_26_512e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_512e( true) ;
         }
         else
         {
            wb_table5_21_512e( false) ;
         }
      }

      protected void wb_table6_26_512( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_ContagemContagemItemWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_35_512( true) ;
         }
         else
         {
            wb_table7_35_512( false) ;
         }
         return  ;
      }

      protected void wb_table7_35_512e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemContagemItemWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_ContagemContagemItemWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_55_512( true) ;
         }
         else
         {
            wb_table8_55_512( false) ;
         }
         return  ;
      }

      protected void wb_table8_55_512e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemContagemItemWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV26DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_ContagemContagemItemWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_75_512( true) ;
         }
         else
         {
            wb_table9_75_512( false) ;
         }
         return  ;
      }

      protected void wb_table9_75_512e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_26_512e( true) ;
         }
         else
         {
            wb_table6_26_512e( false) ;
         }
      }

      protected void wb_table9_75_512( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "", true, "HLP_ContagemContagemItemWC.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_fabricasoftwarecod3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65Contagem_FabricaSoftwareCod3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV65Contagem_FabricaSoftwareCod3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,80);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_fabricasoftwarecod3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_fabricasoftwarecod3_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemContagemItemWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome3_Internalname, AV29FuncaoAPF_Nome3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", 0, edtavFuncaoapf_nome3_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ContagemContagemItemWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_fsreferenciatecnicanom3_Internalname, StringUtil.RTrim( AV66ContagemItem_FSReferenciaTecnicaNom3), StringUtil.RTrim( context.localUtil.Format( AV66ContagemItem_FSReferenciaTecnicaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_fsreferenciatecnicanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_fsreferenciatecnicanom3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemContagemItemWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_fmreferenciatecnicanom3_Internalname, StringUtil.RTrim( AV67ContagemItem_FMReferenciaTecnicaNom3), StringUtil.RTrim( context.localUtil.Format( AV67ContagemItem_FMReferenciaTecnicaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,83);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_fmreferenciatecnicanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_fmreferenciatecnicanom3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_75_512e( true) ;
         }
         else
         {
            wb_table9_75_512e( false) ;
         }
      }

      protected void wb_table8_55_512( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_ContagemContagemItemWC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_fabricasoftwarecod2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62Contagem_FabricaSoftwareCod2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV62Contagem_FabricaSoftwareCod2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,60);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_fabricasoftwarecod2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_fabricasoftwarecod2_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemContagemItemWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome2_Internalname, AV24FuncaoAPF_Nome2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", 0, edtavFuncaoapf_nome2_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ContagemContagemItemWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_fsreferenciatecnicanom2_Internalname, StringUtil.RTrim( AV63ContagemItem_FSReferenciaTecnicaNom2), StringUtil.RTrim( context.localUtil.Format( AV63ContagemItem_FSReferenciaTecnicaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,62);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_fsreferenciatecnicanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_fsreferenciatecnicanom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemContagemItemWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_fmreferenciatecnicanom2_Internalname, StringUtil.RTrim( AV64ContagemItem_FMReferenciaTecnicaNom2), StringUtil.RTrim( context.localUtil.Format( AV64ContagemItem_FMReferenciaTecnicaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,63);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_fmreferenciatecnicanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_fmreferenciatecnicanom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_55_512e( true) ;
         }
         else
         {
            wb_table8_55_512e( false) ;
         }
      }

      protected void wb_table7_35_512( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_ContagemContagemItemWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagem_fabricasoftwarecod1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59Contagem_FabricaSoftwareCod1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV59Contagem_FabricaSoftwareCod1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagem_fabricasoftwarecod1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagem_fabricasoftwarecod1_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemContagemItemWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome1_Internalname, AV19FuncaoAPF_Nome1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", 0, edtavFuncaoapf_nome1_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ContagemContagemItemWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_fsreferenciatecnicanom1_Internalname, StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom1), StringUtil.RTrim( context.localUtil.Format( AV60ContagemItem_FSReferenciaTecnicaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_fsreferenciatecnicanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_fsreferenciatecnicanom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemContagemItemWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemitem_fmreferenciatecnicanom1_Internalname, StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom1), StringUtil.RTrim( context.localUtil.Format( AV61ContagemItem_FMReferenciaTecnicaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemitem_fmreferenciatecnicanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemitem_fmreferenciatecnicanom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_35_512e( true) ;
         }
         else
         {
            wb_table7_35_512e( false) ;
         }
      }

      protected void wb_table4_11_512( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemContagemItemWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_512e( true) ;
         }
         else
         {
            wb_table4_11_512e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Contagem_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contagem_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA512( ) ;
         WS512( ) ;
         WE512( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Contagem_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA512( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemcontagemitemwc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA512( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Contagem_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contagem_Codigo), 6, 0)));
         }
         wcpOAV7Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contagem_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Contagem_Codigo != wcpOAV7Contagem_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Contagem_Codigo = AV7Contagem_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Contagem_Codigo = cgiGet( sPrefix+"AV7Contagem_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Contagem_Codigo) > 0 )
         {
            AV7Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Contagem_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contagem_Codigo), 6, 0)));
         }
         else
         {
            AV7Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Contagem_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA512( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS512( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS512( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contagem_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contagem_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Contagem_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contagem_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Contagem_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE512( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812491014");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemcontagemitemwc.js", "?202051812491015");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_922( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_92_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_92_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_92_idx;
         edtContagemItem_Lancamento_Internalname = sPrefix+"CONTAGEMITEM_LANCAMENTO_"+sGXsfl_92_idx;
         edtContagem_FabricaSoftwareCod_Internalname = sPrefix+"CONTAGEM_FABRICASOFTWARECOD_"+sGXsfl_92_idx;
         edtContagem_FabricaSoftwarePessoaCod_Internalname = sPrefix+"CONTAGEM_FABRICASOFTWAREPESSOACOD_"+sGXsfl_92_idx;
         edtContagem_FabricaSoftwarePessoaNom_Internalname = sPrefix+"CONTAGEM_FABRICASOFTWAREPESSOANOM_"+sGXsfl_92_idx;
         edtContagem_UsuarioAuditorCod_Internalname = sPrefix+"CONTAGEM_USUARIOAUDITORCOD_"+sGXsfl_92_idx;
         edtContagem_UsuarioAuditorPessoaCod_Internalname = sPrefix+"CONTAGEM_USUARIOAUDITORPESSOACOD_"+sGXsfl_92_idx;
         edtContagem_UsuarioAuditorPessoaNom_Internalname = sPrefix+"CONTAGEM_USUARIOAUDITORPESSOANOM_"+sGXsfl_92_idx;
         edtContagemItem_Sequencial_Internalname = sPrefix+"CONTAGEMITEM_SEQUENCIAL_"+sGXsfl_92_idx;
         edtContagemItem_Requisito_Internalname = sPrefix+"CONTAGEMITEM_REQUISITO_"+sGXsfl_92_idx;
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO_"+sGXsfl_92_idx;
         edtContagemItem_PFB_Internalname = sPrefix+"CONTAGEMITEM_PFB_"+sGXsfl_92_idx;
         edtContagemItem_PFL_Internalname = sPrefix+"CONTAGEMITEM_PFL_"+sGXsfl_92_idx;
         edtFuncaoAPF_Nome_Internalname = sPrefix+"FUNCAOAPF_NOME_"+sGXsfl_92_idx;
         cmbFuncaoAPF_Tipo_Internalname = sPrefix+"FUNCAOAPF_TIPO_"+sGXsfl_92_idx;
         cmbContagemItem_TipoUnidade_Internalname = sPrefix+"CONTAGEMITEM_TIPOUNIDADE_"+sGXsfl_92_idx;
         edtContagemItem_Evidencias_Internalname = sPrefix+"CONTAGEMITEM_EVIDENCIAS_"+sGXsfl_92_idx;
         edtContagemItem_QtdINM_Internalname = sPrefix+"CONTAGEMITEM_QTDINM_"+sGXsfl_92_idx;
         edtContagemItem_CP_Internalname = sPrefix+"CONTAGEMITEM_CP_"+sGXsfl_92_idx;
         edtContagemItem_RA_Internalname = sPrefix+"CONTAGEMITEM_RA_"+sGXsfl_92_idx;
         edtContagemItem_DER_Internalname = sPrefix+"CONTAGEMITEM_DER_"+sGXsfl_92_idx;
         edtContagemItem_Funcao_Internalname = sPrefix+"CONTAGEMITEM_FUNCAO_"+sGXsfl_92_idx;
         cmbFuncaoAPF_Complexidade_Internalname = sPrefix+"FUNCAOAPF_COMPLEXIDADE_"+sGXsfl_92_idx;
         cmbContagemItem_FSTipoUnidade_Internalname = sPrefix+"CONTAGEMITEM_FSTIPOUNIDADE_"+sGXsfl_92_idx;
         cmbContagemItem_FSValidacao_Internalname = sPrefix+"CONTAGEMITEM_FSVALIDACAO_"+sGXsfl_92_idx;
         edtContagemItem_FSReferenciaTecnicaCod_Internalname = sPrefix+"CONTAGEMITEM_FSREFERENCIATECNICACOD_"+sGXsfl_92_idx;
      }

      protected void SubsflControlProps_fel_922( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_92_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_92_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_92_fel_idx;
         edtContagemItem_Lancamento_Internalname = sPrefix+"CONTAGEMITEM_LANCAMENTO_"+sGXsfl_92_fel_idx;
         edtContagem_FabricaSoftwareCod_Internalname = sPrefix+"CONTAGEM_FABRICASOFTWARECOD_"+sGXsfl_92_fel_idx;
         edtContagem_FabricaSoftwarePessoaCod_Internalname = sPrefix+"CONTAGEM_FABRICASOFTWAREPESSOACOD_"+sGXsfl_92_fel_idx;
         edtContagem_FabricaSoftwarePessoaNom_Internalname = sPrefix+"CONTAGEM_FABRICASOFTWAREPESSOANOM_"+sGXsfl_92_fel_idx;
         edtContagem_UsuarioAuditorCod_Internalname = sPrefix+"CONTAGEM_USUARIOAUDITORCOD_"+sGXsfl_92_fel_idx;
         edtContagem_UsuarioAuditorPessoaCod_Internalname = sPrefix+"CONTAGEM_USUARIOAUDITORPESSOACOD_"+sGXsfl_92_fel_idx;
         edtContagem_UsuarioAuditorPessoaNom_Internalname = sPrefix+"CONTAGEM_USUARIOAUDITORPESSOANOM_"+sGXsfl_92_fel_idx;
         edtContagemItem_Sequencial_Internalname = sPrefix+"CONTAGEMITEM_SEQUENCIAL_"+sGXsfl_92_fel_idx;
         edtContagemItem_Requisito_Internalname = sPrefix+"CONTAGEMITEM_REQUISITO_"+sGXsfl_92_fel_idx;
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO_"+sGXsfl_92_fel_idx;
         edtContagemItem_PFB_Internalname = sPrefix+"CONTAGEMITEM_PFB_"+sGXsfl_92_fel_idx;
         edtContagemItem_PFL_Internalname = sPrefix+"CONTAGEMITEM_PFL_"+sGXsfl_92_fel_idx;
         edtFuncaoAPF_Nome_Internalname = sPrefix+"FUNCAOAPF_NOME_"+sGXsfl_92_fel_idx;
         cmbFuncaoAPF_Tipo_Internalname = sPrefix+"FUNCAOAPF_TIPO_"+sGXsfl_92_fel_idx;
         cmbContagemItem_TipoUnidade_Internalname = sPrefix+"CONTAGEMITEM_TIPOUNIDADE_"+sGXsfl_92_fel_idx;
         edtContagemItem_Evidencias_Internalname = sPrefix+"CONTAGEMITEM_EVIDENCIAS_"+sGXsfl_92_fel_idx;
         edtContagemItem_QtdINM_Internalname = sPrefix+"CONTAGEMITEM_QTDINM_"+sGXsfl_92_fel_idx;
         edtContagemItem_CP_Internalname = sPrefix+"CONTAGEMITEM_CP_"+sGXsfl_92_fel_idx;
         edtContagemItem_RA_Internalname = sPrefix+"CONTAGEMITEM_RA_"+sGXsfl_92_fel_idx;
         edtContagemItem_DER_Internalname = sPrefix+"CONTAGEMITEM_DER_"+sGXsfl_92_fel_idx;
         edtContagemItem_Funcao_Internalname = sPrefix+"CONTAGEMITEM_FUNCAO_"+sGXsfl_92_fel_idx;
         cmbFuncaoAPF_Complexidade_Internalname = sPrefix+"FUNCAOAPF_COMPLEXIDADE_"+sGXsfl_92_fel_idx;
         cmbContagemItem_FSTipoUnidade_Internalname = sPrefix+"CONTAGEMITEM_FSTIPOUNIDADE_"+sGXsfl_92_fel_idx;
         cmbContagemItem_FSValidacao_Internalname = sPrefix+"CONTAGEMITEM_FSVALIDACAO_"+sGXsfl_92_fel_idx;
         edtContagemItem_FSReferenciaTecnicaCod_Internalname = sPrefix+"CONTAGEMITEM_FSREFERENCIATECNICACOD_"+sGXsfl_92_fel_idx;
      }

      protected void sendrow_922( )
      {
         SubsflControlProps_922( ) ;
         WB510( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_92_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_92_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_92_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV33Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV33Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV257Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)) ? AV257Update_GXI : context.PathToRelativeUrl( AV33Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV33Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV258Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV258Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV38Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV38Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV259Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV38Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV38Display)) ? AV259Display_GXI : context.PathToRelativeUrl( AV38Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV38Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Lancamento_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Lancamento_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_FabricaSoftwareCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A207Contagem_FabricaSoftwareCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContagem_FabricaSoftwareCod_Link,(String)"",(String)"",(String)"",(String)edtContagem_FabricaSoftwareCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_FabricaSoftwarePessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_FabricaSoftwarePessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_FabricaSoftwarePessoaNom_Internalname,StringUtil.RTrim( A209Contagem_FabricaSoftwarePessoaNom),StringUtil.RTrim( context.localUtil.Format( A209Contagem_FabricaSoftwarePessoaNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_FabricaSoftwarePessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_UsuarioAuditorCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A216Contagem_UsuarioAuditorCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_UsuarioAuditorCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_UsuarioAuditorPessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_UsuarioAuditorPessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_UsuarioAuditorPessoaNom_Internalname,StringUtil.RTrim( A218Contagem_UsuarioAuditorPessoaNom),StringUtil.RTrim( context.localUtil.Format( A218Contagem_UsuarioAuditorPessoaNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_UsuarioAuditorPessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Sequencial_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A225ContagemItem_Sequencial), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Sequencial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Sequencial",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Requisito_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A229ContagemItem_Requisito), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A229ContagemItem_Requisito), "ZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Requisito_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Requisito",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_PFB_Internalname,StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ",", "")),context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_PFB_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_PFL_Internalname,StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ",", "")),context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_PFL_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Nome_Internalname,(String)A166FuncaoAPF_Nome,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtFuncaoAPF_Nome_Link,(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "FUNCAOAPF_TIPO_" + sGXsfl_92_idx;
            cmbFuncaoAPF_Tipo.Name = GXCCtl;
            cmbFuncaoAPF_Tipo.WebTags = "";
            cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
            cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
            cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
            cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
            if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
            {
               A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Tipo,(String)cmbFuncaoAPF_Tipo_Internalname,StringUtil.RTrim( A184FuncaoAPF_Tipo),(short)1,(String)cmbFuncaoAPF_Tipo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Tipo.CurrentValue = StringUtil.RTrim( A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Tipo_Internalname, "Values", (String)(cmbFuncaoAPF_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_92_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMITEM_TIPOUNIDADE_" + sGXsfl_92_idx;
               cmbContagemItem_TipoUnidade.Name = GXCCtl;
               cmbContagemItem_TipoUnidade.WebTags = "";
               if ( cmbContagemItem_TipoUnidade.ItemCount > 0 )
               {
                  A952ContagemItem_TipoUnidade = cmbContagemItem_TipoUnidade.getValidValue(A952ContagemItem_TipoUnidade);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemItem_TipoUnidade,(String)cmbContagemItem_TipoUnidade_Internalname,StringUtil.RTrim( A952ContagemItem_TipoUnidade),(short)1,(String)cmbContagemItem_TipoUnidade_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"svchar",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemItem_TipoUnidade.CurrentValue = StringUtil.RTrim( A952ContagemItem_TipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemItem_TipoUnidade_Internalname, "Values", (String)(cmbContagemItem_TipoUnidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Evidencias_Internalname,(String)A953ContagemItem_Evidencias,(String)A953ContagemItem_Evidencias,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Evidencias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5000,(short)0,(short)0,(short)92,(short)1,(short)0,(short)-1,(bool)true,(String)"Evidencias",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_QtdINM_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A954ContagemItem_QtdINM), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A954ContagemItem_QtdINM), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_QtdINM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_CP_Internalname,StringUtil.RTrim( A955ContagemItem_CP),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_CP_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)-1,(bool)true,(String)"CP",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_RA_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A956ContagemItem_RA), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_RA_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Quantidade",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_DER_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A957ContagemItem_DER), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_DER_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Quantidade",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Funcao_Internalname,(String)A958ContagemItem_Funcao,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Funcao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "FUNCAOAPF_COMPLEXIDADE_" + sGXsfl_92_idx;
            cmbFuncaoAPF_Complexidade.Name = GXCCtl;
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Complexidade,(String)cmbFuncaoAPF_Complexidade_Internalname,StringUtil.RTrim( A185FuncaoAPF_Complexidade),(short)1,(String)cmbFuncaoAPF_Complexidade_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Complexidade.CurrentValue = StringUtil.RTrim( A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Complexidade_Internalname, "Values", (String)(cmbFuncaoAPF_Complexidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_92_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMITEM_FSTIPOUNIDADE_" + sGXsfl_92_idx;
               cmbContagemItem_FSTipoUnidade.Name = GXCCtl;
               cmbContagemItem_FSTipoUnidade.WebTags = "";
               cmbContagemItem_FSTipoUnidade.addItem("EE", "Entrada Externa - EE", 0);
               cmbContagemItem_FSTipoUnidade.addItem("CE", "Consulta Externa - CE", 0);
               cmbContagemItem_FSTipoUnidade.addItem("SE", "Sa�da Externa - SE", 0);
               cmbContagemItem_FSTipoUnidade.addItem("ALI", "Arquivo L�gico Interno - ALI", 0);
               cmbContagemItem_FSTipoUnidade.addItem("aie", "Arquivo de Interface Externa - AIE", 0);
               cmbContagemItem_FSTipoUnidade.addItem("DC", "Dados de C�digo - DC", 0);
               if ( cmbContagemItem_FSTipoUnidade.ItemCount > 0 )
               {
                  A228ContagemItem_FSTipoUnidade = cmbContagemItem_FSTipoUnidade.getValidValue(A228ContagemItem_FSTipoUnidade);
                  n228ContagemItem_FSTipoUnidade = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemItem_FSTipoUnidade,(String)cmbContagemItem_FSTipoUnidade_Internalname,StringUtil.RTrim( A228ContagemItem_FSTipoUnidade),(short)1,(String)cmbContagemItem_FSTipoUnidade_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"svchar",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemItem_FSTipoUnidade.CurrentValue = StringUtil.RTrim( A228ContagemItem_FSTipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemItem_FSTipoUnidade_Internalname, "Values", (String)(cmbContagemItem_FSTipoUnidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_92_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMITEM_FSVALIDACAO_" + sGXsfl_92_idx;
               cmbContagemItem_FSValidacao.Name = GXCCtl;
               cmbContagemItem_FSValidacao.WebTags = "";
               cmbContagemItem_FSValidacao.addItem("1", "Aprovado", 0);
               cmbContagemItem_FSValidacao.addItem("2", "N�o Aprovado", 0);
               if ( cmbContagemItem_FSValidacao.ItemCount > 0 )
               {
                  A267ContagemItem_FSValidacao = (short)(NumberUtil.Val( cmbContagemItem_FSValidacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0))), "."));
                  n267ContagemItem_FSValidacao = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemItem_FSValidacao,(String)cmbContagemItem_FSValidacao_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0)),(short)1,(String)cmbContagemItem_FSValidacao_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemItem_FSValidacao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemItem_FSValidacao_Internalname, "Values", (String)(cmbContagemItem_FSValidacao.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_FSReferenciaTecnicaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_FSReferenciaTecnicaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_LANCAMENTO"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_FABRICASOFTWARECOD"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A207Contagem_FabricaSoftwareCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_FABRICASOFTWAREPESSOACOD"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_FABRICASOFTWAREPESSOANOM"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A209Contagem_FabricaSoftwarePessoaNom, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_USUARIOAUDITORCOD"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A216Contagem_UsuarioAuditorCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_USUARIOAUDITORPESSOACOD"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_USUARIOAUDITORPESSOANOM"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A218Contagem_UsuarioAuditorPessoaNom, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_SEQUENCIAL"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_REQUISITO"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A229ContagemItem_Requisito), "ZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_CODIGO"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_PFB"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_PFL"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_TIPOUNIDADE"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_EVIDENCIAS"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, A953ContagemItem_Evidencias));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_QTDINM"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A954ContagemItem_QtdINM), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_CP"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A955ContagemItem_CP, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_RA"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_DER"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FUNCAO"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A958ContagemItem_Funcao, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_COMPLEXIDADE"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSTIPOUNIDADE"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A228ContagemItem_FSTipoUnidade, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSVALIDACAO"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A267ContagemItem_FSValidacao), "9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSREFERENCIATECNICACOD"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sPrefix+sGXsfl_92_idx, context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_92_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_92_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_92_idx+1));
            sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
            SubsflControlProps_922( ) ;
         }
         /* End function sendrow_922 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavContagem_fabricasoftwarecod1_Internalname = sPrefix+"vCONTAGEM_FABRICASOFTWARECOD1";
         edtavFuncaoapf_nome1_Internalname = sPrefix+"vFUNCAOAPF_NOME1";
         edtavContagemitem_fsreferenciatecnicanom1_Internalname = sPrefix+"vCONTAGEMITEM_FSREFERENCIATECNICANOM1";
         edtavContagemitem_fmreferenciatecnicanom1_Internalname = sPrefix+"vCONTAGEMITEM_FMREFERENCIATECNICANOM1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavContagem_fabricasoftwarecod2_Internalname = sPrefix+"vCONTAGEM_FABRICASOFTWARECOD2";
         edtavFuncaoapf_nome2_Internalname = sPrefix+"vFUNCAOAPF_NOME2";
         edtavContagemitem_fsreferenciatecnicanom2_Internalname = sPrefix+"vCONTAGEMITEM_FSREFERENCIATECNICANOM2";
         edtavContagemitem_fmreferenciatecnicanom2_Internalname = sPrefix+"vCONTAGEMITEM_FMREFERENCIATECNICANOM2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR3";
         edtavContagem_fabricasoftwarecod3_Internalname = sPrefix+"vCONTAGEM_FABRICASOFTWARECOD3";
         edtavFuncaoapf_nome3_Internalname = sPrefix+"vFUNCAOAPF_NOME3";
         edtavContagemitem_fsreferenciatecnicanom3_Internalname = sPrefix+"vCONTAGEMITEM_FSREFERENCIATECNICANOM3";
         edtavContagemitem_fmreferenciatecnicanom3_Internalname = sPrefix+"vCONTAGEMITEM_FMREFERENCIATECNICANOM3";
         tblTablemergeddynamicfilters3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtContagemItem_Lancamento_Internalname = sPrefix+"CONTAGEMITEM_LANCAMENTO";
         edtContagem_FabricaSoftwareCod_Internalname = sPrefix+"CONTAGEM_FABRICASOFTWARECOD";
         edtContagem_FabricaSoftwarePessoaCod_Internalname = sPrefix+"CONTAGEM_FABRICASOFTWAREPESSOACOD";
         edtContagem_FabricaSoftwarePessoaNom_Internalname = sPrefix+"CONTAGEM_FABRICASOFTWAREPESSOANOM";
         edtContagem_UsuarioAuditorCod_Internalname = sPrefix+"CONTAGEM_USUARIOAUDITORCOD";
         edtContagem_UsuarioAuditorPessoaCod_Internalname = sPrefix+"CONTAGEM_USUARIOAUDITORPESSOACOD";
         edtContagem_UsuarioAuditorPessoaNom_Internalname = sPrefix+"CONTAGEM_USUARIOAUDITORPESSOANOM";
         edtContagemItem_Sequencial_Internalname = sPrefix+"CONTAGEMITEM_SEQUENCIAL";
         edtContagemItem_Requisito_Internalname = sPrefix+"CONTAGEMITEM_REQUISITO";
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO";
         edtContagemItem_PFB_Internalname = sPrefix+"CONTAGEMITEM_PFB";
         edtContagemItem_PFL_Internalname = sPrefix+"CONTAGEMITEM_PFL";
         edtFuncaoAPF_Nome_Internalname = sPrefix+"FUNCAOAPF_NOME";
         cmbFuncaoAPF_Tipo_Internalname = sPrefix+"FUNCAOAPF_TIPO";
         cmbContagemItem_TipoUnidade_Internalname = sPrefix+"CONTAGEMITEM_TIPOUNIDADE";
         edtContagemItem_Evidencias_Internalname = sPrefix+"CONTAGEMITEM_EVIDENCIAS";
         edtContagemItem_QtdINM_Internalname = sPrefix+"CONTAGEMITEM_QTDINM";
         edtContagemItem_CP_Internalname = sPrefix+"CONTAGEMITEM_CP";
         edtContagemItem_RA_Internalname = sPrefix+"CONTAGEMITEM_RA";
         edtContagemItem_DER_Internalname = sPrefix+"CONTAGEMITEM_DER";
         edtContagemItem_Funcao_Internalname = sPrefix+"CONTAGEMITEM_FUNCAO";
         cmbFuncaoAPF_Complexidade_Internalname = sPrefix+"FUNCAOAPF_COMPLEXIDADE";
         cmbContagemItem_FSTipoUnidade_Internalname = sPrefix+"CONTAGEMITEM_FSTIPOUNIDADE";
         cmbContagemItem_FSValidacao_Internalname = sPrefix+"CONTAGEMITEM_FSVALIDACAO";
         edtContagemItem_FSReferenciaTecnicaCod_Internalname = sPrefix+"CONTAGEMITEM_FSREFERENCIATECNICACOD";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContagem_Codigo_Internalname = sPrefix+"CONTAGEM_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContagemItem_FSReferenciaTecnicaCod_Jsonclick = "";
         cmbContagemItem_FSValidacao_Jsonclick = "";
         cmbContagemItem_FSTipoUnidade_Jsonclick = "";
         cmbFuncaoAPF_Complexidade_Jsonclick = "";
         edtContagemItem_Funcao_Jsonclick = "";
         edtContagemItem_DER_Jsonclick = "";
         edtContagemItem_RA_Jsonclick = "";
         edtContagemItem_CP_Jsonclick = "";
         edtContagemItem_QtdINM_Jsonclick = "";
         edtContagemItem_Evidencias_Jsonclick = "";
         cmbContagemItem_TipoUnidade_Jsonclick = "";
         cmbFuncaoAPF_Tipo_Jsonclick = "";
         edtFuncaoAPF_Nome_Jsonclick = "";
         edtContagemItem_PFL_Jsonclick = "";
         edtContagemItem_PFB_Jsonclick = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         edtContagemItem_Requisito_Jsonclick = "";
         edtContagemItem_Sequencial_Jsonclick = "";
         edtContagem_UsuarioAuditorPessoaNom_Jsonclick = "";
         edtContagem_UsuarioAuditorPessoaCod_Jsonclick = "";
         edtContagem_UsuarioAuditorCod_Jsonclick = "";
         edtContagem_FabricaSoftwarePessoaNom_Jsonclick = "";
         edtContagem_FabricaSoftwarePessoaCod_Jsonclick = "";
         edtContagem_FabricaSoftwareCod_Jsonclick = "";
         edtContagemItem_Lancamento_Jsonclick = "";
         edtavContagemitem_fmreferenciatecnicanom1_Jsonclick = "";
         edtavContagemitem_fsreferenciatecnicanom1_Jsonclick = "";
         edtavContagem_fabricasoftwarecod1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContagemitem_fmreferenciatecnicanom2_Jsonclick = "";
         edtavContagemitem_fsreferenciatecnicanom2_Jsonclick = "";
         edtavContagem_fabricasoftwarecod2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContagemitem_fmreferenciatecnicanom3_Jsonclick = "";
         edtavContagemitem_fsreferenciatecnicanom3_Jsonclick = "";
         edtavContagem_fabricasoftwarecod3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtFuncaoAPF_Nome_Link = "";
         edtContagem_FabricaSoftwareCod_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtContagemItem_FSReferenciaTecnicaCod_Titleformat = 0;
         cmbContagemItem_FSValidacao_Titleformat = 0;
         cmbContagemItem_FSTipoUnidade_Titleformat = 0;
         edtContagemItem_Funcao_Titleformat = 0;
         edtContagemItem_DER_Titleformat = 0;
         edtContagemItem_RA_Titleformat = 0;
         edtContagemItem_CP_Titleformat = 0;
         edtContagemItem_QtdINM_Titleformat = 0;
         edtContagemItem_Evidencias_Titleformat = 0;
         cmbContagemItem_TipoUnidade_Titleformat = 0;
         cmbFuncaoAPF_Tipo_Titleformat = 0;
         edtFuncaoAPF_Nome_Titleformat = 0;
         edtContagemItem_PFL_Titleformat = 0;
         edtContagemItem_PFB_Titleformat = 0;
         edtFuncaoAPF_Codigo_Titleformat = 0;
         edtContagemItem_Requisito_Titleformat = 0;
         edtContagemItem_Sequencial_Titleformat = 0;
         edtContagem_UsuarioAuditorPessoaNom_Titleformat = 0;
         edtContagem_UsuarioAuditorPessoaCod_Titleformat = 0;
         edtContagem_UsuarioAuditorCod_Titleformat = 0;
         edtContagem_FabricaSoftwarePessoaNom_Titleformat = 0;
         edtContagem_FabricaSoftwarePessoaCod_Titleformat = 0;
         edtContagem_FabricaSoftwareCod_Titleformat = 0;
         edtContagemItem_Lancamento_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContagemitem_fmreferenciatecnicanom3_Visible = 1;
         edtavContagemitem_fsreferenciatecnicanom3_Visible = 1;
         edtavFuncaoapf_nome3_Visible = 1;
         edtavContagem_fabricasoftwarecod3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContagemitem_fmreferenciatecnicanom2_Visible = 1;
         edtavContagemitem_fsreferenciatecnicanom2_Visible = 1;
         edtavFuncaoapf_nome2_Visible = 1;
         edtavContagem_fabricasoftwarecod2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContagemitem_fmreferenciatecnicanom1_Visible = 1;
         edtavContagemitem_fsreferenciatecnicanom1_Visible = 1;
         edtavFuncaoapf_nome1_Visible = 1;
         edtavContagem_fabricasoftwarecod1_Visible = 1;
         edtContagemItem_FSReferenciaTecnicaCod_Title = "FS Cod.";
         cmbContagemItem_FSValidacao.Title.Text = "de Software";
         cmbContagemItem_FSTipoUnidade.Title.Text = "F�b. Software)";
         edtContagemItem_Funcao_Title = "de Dados";
         edtContagemItem_DER_Title = "DER";
         edtContagemItem_RA_Title = "A";
         edtContagemItem_CP_Title = "CP";
         edtContagemItem_QtdINM_Title = "INM";
         edtContagemItem_Evidencias_Title = "Evidencias";
         cmbContagemItem_TipoUnidade.Title.Text = "Tipo";
         cmbFuncaoAPF_Tipo.Title.Text = "Tipo";
         edtFuncaoAPF_Nome_Title = "Fun��o de Transa��o";
         edtContagemItem_PFL_Title = "PFL";
         edtContagemItem_PFB_Title = "PFB";
         edtFuncaoAPF_Codigo_Title = "Fun��o de Transa��o";
         edtContagemItem_Requisito_Title = "de Software)";
         edtContagemItem_Sequencial_Title = "Sequencial";
         edtContagem_UsuarioAuditorPessoaNom_Title = "do Auditor";
         edtContagem_UsuarioAuditorPessoaCod_Title = "Pessoa Auditor";
         edtContagem_UsuarioAuditorCod_Title = "Usu�rio Auditor";
         edtContagem_FabricaSoftwarePessoaNom_Title = "F�brica )";
         edtContagem_FabricaSoftwarePessoaCod_Title = "Pessoa Contratada";
         edtContagem_FabricaSoftwareCod_Title = "Contratada F�brica";
         edtContagemItem_Lancamento_Title = "Item do Lan�amento";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtContagem_Codigo_Jsonclick = "";
         edtContagem_Codigo_Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV260Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtContagemItem_Lancamento_Titleformat',ctrl:'CONTAGEMITEM_LANCAMENTO',prop:'Titleformat'},{av:'edtContagemItem_Lancamento_Title',ctrl:'CONTAGEMITEM_LANCAMENTO',prop:'Title'},{av:'edtContagem_FabricaSoftwareCod_Titleformat',ctrl:'CONTAGEM_FABRICASOFTWARECOD',prop:'Titleformat'},{av:'edtContagem_FabricaSoftwareCod_Title',ctrl:'CONTAGEM_FABRICASOFTWARECOD',prop:'Title'},{av:'edtContagem_FabricaSoftwarePessoaCod_Titleformat',ctrl:'CONTAGEM_FABRICASOFTWAREPESSOACOD',prop:'Titleformat'},{av:'edtContagem_FabricaSoftwarePessoaCod_Title',ctrl:'CONTAGEM_FABRICASOFTWAREPESSOACOD',prop:'Title'},{av:'edtContagem_FabricaSoftwarePessoaNom_Titleformat',ctrl:'CONTAGEM_FABRICASOFTWAREPESSOANOM',prop:'Titleformat'},{av:'edtContagem_FabricaSoftwarePessoaNom_Title',ctrl:'CONTAGEM_FABRICASOFTWAREPESSOANOM',prop:'Title'},{av:'edtContagem_UsuarioAuditorCod_Titleformat',ctrl:'CONTAGEM_USUARIOAUDITORCOD',prop:'Titleformat'},{av:'edtContagem_UsuarioAuditorCod_Title',ctrl:'CONTAGEM_USUARIOAUDITORCOD',prop:'Title'},{av:'edtContagem_UsuarioAuditorPessoaCod_Titleformat',ctrl:'CONTAGEM_USUARIOAUDITORPESSOACOD',prop:'Titleformat'},{av:'edtContagem_UsuarioAuditorPessoaCod_Title',ctrl:'CONTAGEM_USUARIOAUDITORPESSOACOD',prop:'Title'},{av:'edtContagem_UsuarioAuditorPessoaNom_Titleformat',ctrl:'CONTAGEM_USUARIOAUDITORPESSOANOM',prop:'Titleformat'},{av:'edtContagem_UsuarioAuditorPessoaNom_Title',ctrl:'CONTAGEM_USUARIOAUDITORPESSOANOM',prop:'Title'},{av:'edtContagemItem_Sequencial_Titleformat',ctrl:'CONTAGEMITEM_SEQUENCIAL',prop:'Titleformat'},{av:'edtContagemItem_Sequencial_Title',ctrl:'CONTAGEMITEM_SEQUENCIAL',prop:'Title'},{av:'edtContagemItem_Requisito_Titleformat',ctrl:'CONTAGEMITEM_REQUISITO',prop:'Titleformat'},{av:'edtContagemItem_Requisito_Title',ctrl:'CONTAGEMITEM_REQUISITO',prop:'Title'},{av:'edtFuncaoAPF_Codigo_Titleformat',ctrl:'FUNCAOAPF_CODIGO',prop:'Titleformat'},{av:'edtFuncaoAPF_Codigo_Title',ctrl:'FUNCAOAPF_CODIGO',prop:'Title'},{av:'edtContagemItem_PFB_Titleformat',ctrl:'CONTAGEMITEM_PFB',prop:'Titleformat'},{av:'edtContagemItem_PFB_Title',ctrl:'CONTAGEMITEM_PFB',prop:'Title'},{av:'edtContagemItem_PFL_Titleformat',ctrl:'CONTAGEMITEM_PFL',prop:'Titleformat'},{av:'edtContagemItem_PFL_Title',ctrl:'CONTAGEMITEM_PFL',prop:'Title'},{av:'edtFuncaoAPF_Nome_Titleformat',ctrl:'FUNCAOAPF_NOME',prop:'Titleformat'},{av:'edtFuncaoAPF_Nome_Title',ctrl:'FUNCAOAPF_NOME',prop:'Title'},{av:'cmbFuncaoAPF_Tipo'},{av:'cmbContagemItem_TipoUnidade'},{av:'edtContagemItem_Evidencias_Titleformat',ctrl:'CONTAGEMITEM_EVIDENCIAS',prop:'Titleformat'},{av:'edtContagemItem_Evidencias_Title',ctrl:'CONTAGEMITEM_EVIDENCIAS',prop:'Title'},{av:'edtContagemItem_QtdINM_Titleformat',ctrl:'CONTAGEMITEM_QTDINM',prop:'Titleformat'},{av:'edtContagemItem_QtdINM_Title',ctrl:'CONTAGEMITEM_QTDINM',prop:'Title'},{av:'edtContagemItem_CP_Titleformat',ctrl:'CONTAGEMITEM_CP',prop:'Titleformat'},{av:'edtContagemItem_CP_Title',ctrl:'CONTAGEMITEM_CP',prop:'Title'},{av:'edtContagemItem_RA_Titleformat',ctrl:'CONTAGEMITEM_RA',prop:'Titleformat'},{av:'edtContagemItem_RA_Title',ctrl:'CONTAGEMITEM_RA',prop:'Title'},{av:'edtContagemItem_DER_Titleformat',ctrl:'CONTAGEMITEM_DER',prop:'Titleformat'},{av:'edtContagemItem_DER_Title',ctrl:'CONTAGEMITEM_DER',prop:'Title'},{av:'edtContagemItem_Funcao_Titleformat',ctrl:'CONTAGEMITEM_FUNCAO',prop:'Titleformat'},{av:'edtContagemItem_Funcao_Title',ctrl:'CONTAGEMITEM_FUNCAO',prop:'Title'},{av:'cmbContagemItem_FSTipoUnidade'},{av:'cmbContagemItem_FSValidacao'},{av:'edtContagemItem_FSReferenciaTecnicaCod_Titleformat',ctrl:'CONTAGEMITEM_FSREFERENCIATECNICACOD',prop:'Titleformat'},{av:'edtContagemItem_FSReferenciaTecnicaCod_Title',ctrl:'CONTAGEMITEM_FSREFERENCIATECNICACOD',prop:'Title'},{av:'AV253GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV254GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11512',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV260Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E25512',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV33Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV38Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtContagem_FabricaSoftwareCod_Link',ctrl:'CONTAGEM_FABRICASOFTWARECOD',prop:'Link'},{av:'edtFuncaoAPF_Nome_Link',ctrl:'FUNCAOAPF_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E12512',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV260Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E18512',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV260Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13512',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV260Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'edtavContagem_fabricasoftwarecod2_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD2',prop:'Visible'},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagem_fabricasoftwarecod3_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD3',prop:'Visible'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagem_fabricasoftwarecod1_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD1',prop:'Visible'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E19512',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavContagem_fabricasoftwarecod1_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD1',prop:'Visible'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E20512',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV260Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14512',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV260Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'edtavContagem_fabricasoftwarecod2_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD2',prop:'Visible'},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagem_fabricasoftwarecod3_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD3',prop:'Visible'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagem_fabricasoftwarecod1_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD1',prop:'Visible'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E21512',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavContagem_fabricasoftwarecod2_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD2',prop:'Visible'},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15512',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV260Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'edtavContagem_fabricasoftwarecod2_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD2',prop:'Visible'},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagem_fabricasoftwarecod3_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD3',prop:'Visible'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagem_fabricasoftwarecod1_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD1',prop:'Visible'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E22512',iparms:[{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavContagem_fabricasoftwarecod3_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD3',prop:'Visible'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E16512',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV260Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV68FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV59Contagem_FabricaSoftwareCod1',fld:'vCONTAGEM_FABRICASOFTWARECOD1',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContagem_fabricasoftwarecod1_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD1',prop:'Visible'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom1_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV62Contagem_FabricaSoftwareCod2',fld:'vCONTAGEM_FABRICASOFTWARECOD2',pic:'ZZZZZ9',nv:0},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV65Contagem_FabricaSoftwareCod3',fld:'vCONTAGEM_FABRICASOFTWARECOD3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60ContagemItem_FSReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'AV61ContagemItem_FMReferenciaTecnicaNom1',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63ContagemItem_FSReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV64ContagemItem_FMReferenciaTecnicaNom2',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',pic:'@!',nv:''},{av:'AV29FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66ContagemItem_FSReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'AV67ContagemItem_FMReferenciaTecnicaNom3',fld:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',pic:'@!',nv:''},{av:'edtavContagem_fabricasoftwarecod2_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD2',prop:'Visible'},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM2',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom2_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagem_fabricasoftwarecod3_Visible',ctrl:'vCONTAGEM_FABRICASOFTWARECOD3',prop:'Visible'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavContagemitem_fsreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FSREFERENCIATECNICANOM3',prop:'Visible'},{av:'edtavContagemitem_fmreferenciatecnicanom3_Visible',ctrl:'vCONTAGEMITEM_FMREFERENCIATECNICANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E17512',iparms:[{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV19FuncaoAPF_Nome1 = "";
         AV60ContagemItem_FSReferenciaTecnicaNom1 = "";
         AV61ContagemItem_FMReferenciaTecnicaNom1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV24FuncaoAPF_Nome2 = "";
         AV63ContagemItem_FSReferenciaTecnicaNom2 = "";
         AV64ContagemItem_FMReferenciaTecnicaNom2 = "";
         AV26DynamicFiltersSelector3 = "";
         AV29FuncaoAPF_Nome3 = "";
         AV66ContagemItem_FSReferenciaTecnicaNom3 = "";
         AV67ContagemItem_FMReferenciaTecnicaNom3 = "";
         AV260Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV33Update = "";
         AV257Update_GXI = "";
         AV32Delete = "";
         AV258Delete_GXI = "";
         AV38Display = "";
         AV259Display_GXI = "";
         A209Contagem_FabricaSoftwarePessoaNom = "";
         A218Contagem_UsuarioAuditorPessoaNom = "";
         A166FuncaoAPF_Nome = "";
         A184FuncaoAPF_Tipo = "";
         A952ContagemItem_TipoUnidade = "";
         A953ContagemItem_Evidencias = "";
         A955ContagemItem_CP = "";
         A958ContagemItem_Funcao = "";
         A185FuncaoAPF_Complexidade = "";
         A228ContagemItem_FSTipoUnidade = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV19FuncaoAPF_Nome1 = "";
         lV60ContagemItem_FSReferenciaTecnicaNom1 = "";
         lV61ContagemItem_FMReferenciaTecnicaNom1 = "";
         lV24FuncaoAPF_Nome2 = "";
         lV63ContagemItem_FSReferenciaTecnicaNom2 = "";
         lV64ContagemItem_FMReferenciaTecnicaNom2 = "";
         lV29FuncaoAPF_Nome3 = "";
         lV66ContagemItem_FSReferenciaTecnicaNom3 = "";
         lV67ContagemItem_FMReferenciaTecnicaNom3 = "";
         A269ContagemItem_FSReferenciaTecnicaNom = "";
         A257ContagemItem_FMReferenciaTecnicaNom = "";
         H00512_A255ContagemItem_FMReferenciaTecnicaCod = new int[1] ;
         H00512_n255ContagemItem_FMReferenciaTecnicaCod = new bool[] {false} ;
         H00512_A257ContagemItem_FMReferenciaTecnicaNom = new String[] {""} ;
         H00512_n257ContagemItem_FMReferenciaTecnicaNom = new bool[] {false} ;
         H00512_A269ContagemItem_FSReferenciaTecnicaNom = new String[] {""} ;
         H00512_n269ContagemItem_FSReferenciaTecnicaNom = new bool[] {false} ;
         H00512_A192Contagem_Codigo = new int[1] ;
         H00512_A226ContagemItem_FSReferenciaTecnicaCod = new int[1] ;
         H00512_n226ContagemItem_FSReferenciaTecnicaCod = new bool[] {false} ;
         H00512_A267ContagemItem_FSValidacao = new short[1] ;
         H00512_n267ContagemItem_FSValidacao = new bool[] {false} ;
         H00512_A228ContagemItem_FSTipoUnidade = new String[] {""} ;
         H00512_n228ContagemItem_FSTipoUnidade = new bool[] {false} ;
         H00512_A958ContagemItem_Funcao = new String[] {""} ;
         H00512_n958ContagemItem_Funcao = new bool[] {false} ;
         H00512_A957ContagemItem_DER = new short[1] ;
         H00512_n957ContagemItem_DER = new bool[] {false} ;
         H00512_A956ContagemItem_RA = new short[1] ;
         H00512_n956ContagemItem_RA = new bool[] {false} ;
         H00512_A955ContagemItem_CP = new String[] {""} ;
         H00512_n955ContagemItem_CP = new bool[] {false} ;
         H00512_A954ContagemItem_QtdINM = new short[1] ;
         H00512_n954ContagemItem_QtdINM = new bool[] {false} ;
         H00512_A953ContagemItem_Evidencias = new String[] {""} ;
         H00512_n953ContagemItem_Evidencias = new bool[] {false} ;
         H00512_A952ContagemItem_TipoUnidade = new String[] {""} ;
         H00512_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00512_A166FuncaoAPF_Nome = new String[] {""} ;
         H00512_A951ContagemItem_PFL = new decimal[1] ;
         H00512_n951ContagemItem_PFL = new bool[] {false} ;
         H00512_A950ContagemItem_PFB = new decimal[1] ;
         H00512_n950ContagemItem_PFB = new bool[] {false} ;
         H00512_A229ContagemItem_Requisito = new short[1] ;
         H00512_n229ContagemItem_Requisito = new bool[] {false} ;
         H00512_A225ContagemItem_Sequencial = new short[1] ;
         H00512_n225ContagemItem_Sequencial = new bool[] {false} ;
         H00512_A218Contagem_UsuarioAuditorPessoaNom = new String[] {""} ;
         H00512_A217Contagem_UsuarioAuditorPessoaCod = new int[1] ;
         H00512_A216Contagem_UsuarioAuditorCod = new int[1] ;
         H00512_A209Contagem_FabricaSoftwarePessoaNom = new String[] {""} ;
         H00512_A208Contagem_FabricaSoftwarePessoaCod = new int[1] ;
         H00512_A207Contagem_FabricaSoftwareCod = new int[1] ;
         H00512_A224ContagemItem_Lancamento = new int[1] ;
         H00512_A165FuncaoAPF_Codigo = new int[1] ;
         H00512_n165FuncaoAPF_Codigo = new bool[] {false} ;
         GXt_char1 = "";
         H00513_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GridRow = new GXWebRow();
         AV34Session = context.GetSession();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Contagem_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemcontagemitemwc__default(),
            new Object[][] {
                new Object[] {
               H00512_A255ContagemItem_FMReferenciaTecnicaCod, H00512_n255ContagemItem_FMReferenciaTecnicaCod, H00512_A257ContagemItem_FMReferenciaTecnicaNom, H00512_n257ContagemItem_FMReferenciaTecnicaNom, H00512_A269ContagemItem_FSReferenciaTecnicaNom, H00512_n269ContagemItem_FSReferenciaTecnicaNom, H00512_A192Contagem_Codigo, H00512_A226ContagemItem_FSReferenciaTecnicaCod, H00512_n226ContagemItem_FSReferenciaTecnicaCod, H00512_A267ContagemItem_FSValidacao,
               H00512_n267ContagemItem_FSValidacao, H00512_A228ContagemItem_FSTipoUnidade, H00512_n228ContagemItem_FSTipoUnidade, H00512_A958ContagemItem_Funcao, H00512_n958ContagemItem_Funcao, H00512_A957ContagemItem_DER, H00512_n957ContagemItem_DER, H00512_A956ContagemItem_RA, H00512_n956ContagemItem_RA, H00512_A955ContagemItem_CP,
               H00512_n955ContagemItem_CP, H00512_A954ContagemItem_QtdINM, H00512_n954ContagemItem_QtdINM, H00512_A953ContagemItem_Evidencias, H00512_n953ContagemItem_Evidencias, H00512_A952ContagemItem_TipoUnidade, H00512_A184FuncaoAPF_Tipo, H00512_A166FuncaoAPF_Nome, H00512_A951ContagemItem_PFL, H00512_n951ContagemItem_PFL,
               H00512_A950ContagemItem_PFB, H00512_n950ContagemItem_PFB, H00512_A229ContagemItem_Requisito, H00512_n229ContagemItem_Requisito, H00512_A225ContagemItem_Sequencial, H00512_n225ContagemItem_Sequencial, H00512_A218Contagem_UsuarioAuditorPessoaNom, H00512_A217Contagem_UsuarioAuditorPessoaCod, H00512_A216Contagem_UsuarioAuditorCod, H00512_A209Contagem_FabricaSoftwarePessoaNom,
               H00512_A208Contagem_FabricaSoftwarePessoaCod, H00512_A207Contagem_FabricaSoftwareCod, H00512_A224ContagemItem_Lancamento, H00512_A165FuncaoAPF_Codigo, H00512_n165FuncaoAPF_Codigo
               }
               , new Object[] {
               H00513_AGRID_nRecordCount
               }
            }
         );
         AV260Pgmname = "ContagemContagemItemWC";
         /* GeneXus formulas. */
         AV260Pgmname = "ContagemContagemItemWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_92 ;
      private short nGXsfl_92_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV27DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A225ContagemItem_Sequencial ;
      private short A229ContagemItem_Requisito ;
      private short A954ContagemItem_QtdINM ;
      private short A956ContagemItem_RA ;
      private short A957ContagemItem_DER ;
      private short A267ContagemItem_FSValidacao ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_92_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemItem_Lancamento_Titleformat ;
      private short edtContagem_FabricaSoftwareCod_Titleformat ;
      private short edtContagem_FabricaSoftwarePessoaCod_Titleformat ;
      private short edtContagem_FabricaSoftwarePessoaNom_Titleformat ;
      private short edtContagem_UsuarioAuditorCod_Titleformat ;
      private short edtContagem_UsuarioAuditorPessoaCod_Titleformat ;
      private short edtContagem_UsuarioAuditorPessoaNom_Titleformat ;
      private short edtContagemItem_Sequencial_Titleformat ;
      private short edtContagemItem_Requisito_Titleformat ;
      private short edtFuncaoAPF_Codigo_Titleformat ;
      private short edtContagemItem_PFB_Titleformat ;
      private short edtContagemItem_PFL_Titleformat ;
      private short edtFuncaoAPF_Nome_Titleformat ;
      private short cmbFuncaoAPF_Tipo_Titleformat ;
      private short cmbContagemItem_TipoUnidade_Titleformat ;
      private short edtContagemItem_Evidencias_Titleformat ;
      private short edtContagemItem_QtdINM_Titleformat ;
      private short edtContagemItem_CP_Titleformat ;
      private short edtContagemItem_RA_Titleformat ;
      private short edtContagemItem_DER_Titleformat ;
      private short edtContagemItem_Funcao_Titleformat ;
      private short cmbContagemItem_FSTipoUnidade_Titleformat ;
      private short cmbContagemItem_FSValidacao_Titleformat ;
      private short edtContagemItem_FSReferenciaTecnicaCod_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Contagem_Codigo ;
      private int wcpOAV7Contagem_Codigo ;
      private int subGrid_Rows ;
      private int AV59Contagem_FabricaSoftwareCod1 ;
      private int AV62Contagem_FabricaSoftwareCod2 ;
      private int AV65Contagem_FabricaSoftwareCod3 ;
      private int A224ContagemItem_Lancamento ;
      private int A192Contagem_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int AV68FuncaoAPF_SistemaCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtContagem_Codigo_Visible ;
      private int A207Contagem_FabricaSoftwareCod ;
      private int A208Contagem_FabricaSoftwarePessoaCod ;
      private int A216Contagem_UsuarioAuditorCod ;
      private int A217Contagem_UsuarioAuditorPessoaCod ;
      private int A226ContagemItem_FSReferenciaTecnicaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A255ContagemItem_FMReferenciaTecnicaCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV252PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContagem_fabricasoftwarecod1_Visible ;
      private int edtavFuncaoapf_nome1_Visible ;
      private int edtavContagemitem_fsreferenciatecnicanom1_Visible ;
      private int edtavContagemitem_fmreferenciatecnicanom1_Visible ;
      private int edtavContagem_fabricasoftwarecod2_Visible ;
      private int edtavFuncaoapf_nome2_Visible ;
      private int edtavContagemitem_fsreferenciatecnicanom2_Visible ;
      private int edtavContagemitem_fmreferenciatecnicanom2_Visible ;
      private int edtavContagem_fabricasoftwarecod3_Visible ;
      private int edtavFuncaoapf_nome3_Visible ;
      private int edtavContagemitem_fsreferenciatecnicanom3_Visible ;
      private int edtavContagemitem_fmreferenciatecnicanom3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV253GridCurrentPage ;
      private long AV254GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A950ContagemItem_PFB ;
      private decimal A951ContagemItem_PFL ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_92_idx="0001" ;
      private String AV60ContagemItem_FSReferenciaTecnicaNom1 ;
      private String AV61ContagemItem_FMReferenciaTecnicaNom1 ;
      private String AV63ContagemItem_FSReferenciaTecnicaNom2 ;
      private String AV64ContagemItem_FMReferenciaTecnicaNom2 ;
      private String AV66ContagemItem_FSReferenciaTecnicaNom3 ;
      private String AV67ContagemItem_FMReferenciaTecnicaNom3 ;
      private String AV260Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String edtContagem_Codigo_Internalname ;
      private String edtContagem_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtContagemItem_Lancamento_Internalname ;
      private String edtContagem_FabricaSoftwareCod_Internalname ;
      private String edtContagem_FabricaSoftwarePessoaCod_Internalname ;
      private String A209Contagem_FabricaSoftwarePessoaNom ;
      private String edtContagem_FabricaSoftwarePessoaNom_Internalname ;
      private String edtContagem_UsuarioAuditorCod_Internalname ;
      private String edtContagem_UsuarioAuditorPessoaCod_Internalname ;
      private String A218Contagem_UsuarioAuditorPessoaNom ;
      private String edtContagem_UsuarioAuditorPessoaNom_Internalname ;
      private String edtContagemItem_Sequencial_Internalname ;
      private String edtContagemItem_Requisito_Internalname ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtContagemItem_PFB_Internalname ;
      private String edtContagemItem_PFL_Internalname ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String cmbFuncaoAPF_Tipo_Internalname ;
      private String A184FuncaoAPF_Tipo ;
      private String cmbContagemItem_TipoUnidade_Internalname ;
      private String edtContagemItem_Evidencias_Internalname ;
      private String edtContagemItem_QtdINM_Internalname ;
      private String A955ContagemItem_CP ;
      private String edtContagemItem_CP_Internalname ;
      private String edtContagemItem_RA_Internalname ;
      private String edtContagemItem_DER_Internalname ;
      private String edtContagemItem_Funcao_Internalname ;
      private String cmbFuncaoAPF_Complexidade_Internalname ;
      private String A185FuncaoAPF_Complexidade ;
      private String cmbContagemItem_FSTipoUnidade_Internalname ;
      private String cmbContagemItem_FSValidacao_Internalname ;
      private String edtContagemItem_FSReferenciaTecnicaCod_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV60ContagemItem_FSReferenciaTecnicaNom1 ;
      private String lV61ContagemItem_FMReferenciaTecnicaNom1 ;
      private String lV63ContagemItem_FSReferenciaTecnicaNom2 ;
      private String lV64ContagemItem_FMReferenciaTecnicaNom2 ;
      private String lV66ContagemItem_FSReferenciaTecnicaNom3 ;
      private String lV67ContagemItem_FMReferenciaTecnicaNom3 ;
      private String A269ContagemItem_FSReferenciaTecnicaNom ;
      private String A257ContagemItem_FMReferenciaTecnicaNom ;
      private String GXt_char1 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContagem_fabricasoftwarecod1_Internalname ;
      private String edtavFuncaoapf_nome1_Internalname ;
      private String edtavContagemitem_fsreferenciatecnicanom1_Internalname ;
      private String edtavContagemitem_fmreferenciatecnicanom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContagem_fabricasoftwarecod2_Internalname ;
      private String edtavFuncaoapf_nome2_Internalname ;
      private String edtavContagemitem_fsreferenciatecnicanom2_Internalname ;
      private String edtavContagemitem_fmreferenciatecnicanom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContagem_fabricasoftwarecod3_Internalname ;
      private String edtavFuncaoapf_nome3_Internalname ;
      private String edtavContagemitem_fsreferenciatecnicanom3_Internalname ;
      private String edtavContagemitem_fmreferenciatecnicanom3_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContagemItem_Lancamento_Title ;
      private String edtContagem_FabricaSoftwareCod_Title ;
      private String edtContagem_FabricaSoftwarePessoaCod_Title ;
      private String edtContagem_FabricaSoftwarePessoaNom_Title ;
      private String edtContagem_UsuarioAuditorCod_Title ;
      private String edtContagem_UsuarioAuditorPessoaCod_Title ;
      private String edtContagem_UsuarioAuditorPessoaNom_Title ;
      private String edtContagemItem_Sequencial_Title ;
      private String edtContagemItem_Requisito_Title ;
      private String edtFuncaoAPF_Codigo_Title ;
      private String edtContagemItem_PFB_Title ;
      private String edtContagemItem_PFL_Title ;
      private String edtFuncaoAPF_Nome_Title ;
      private String edtContagemItem_Evidencias_Title ;
      private String edtContagemItem_QtdINM_Title ;
      private String edtContagemItem_CP_Title ;
      private String edtContagemItem_RA_Title ;
      private String edtContagemItem_DER_Title ;
      private String edtContagemItem_Funcao_Title ;
      private String edtContagemItem_FSReferenciaTecnicaCod_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtContagem_FabricaSoftwareCod_Link ;
      private String edtFuncaoAPF_Nome_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContagem_fabricasoftwarecod3_Jsonclick ;
      private String edtavContagemitem_fsreferenciatecnicanom3_Jsonclick ;
      private String edtavContagemitem_fmreferenciatecnicanom3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContagem_fabricasoftwarecod2_Jsonclick ;
      private String edtavContagemitem_fsreferenciatecnicanom2_Jsonclick ;
      private String edtavContagemitem_fmreferenciatecnicanom2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContagem_fabricasoftwarecod1_Jsonclick ;
      private String edtavContagemitem_fsreferenciatecnicanom1_Jsonclick ;
      private String edtavContagemitem_fmreferenciatecnicanom1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7Contagem_Codigo ;
      private String sGXsfl_92_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagemItem_Lancamento_Jsonclick ;
      private String edtContagem_FabricaSoftwareCod_Jsonclick ;
      private String edtContagem_FabricaSoftwarePessoaCod_Jsonclick ;
      private String edtContagem_FabricaSoftwarePessoaNom_Jsonclick ;
      private String edtContagem_UsuarioAuditorCod_Jsonclick ;
      private String edtContagem_UsuarioAuditorPessoaCod_Jsonclick ;
      private String edtContagem_UsuarioAuditorPessoaNom_Jsonclick ;
      private String edtContagemItem_Sequencial_Jsonclick ;
      private String edtContagemItem_Requisito_Jsonclick ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String edtContagemItem_PFB_Jsonclick ;
      private String edtContagemItem_PFL_Jsonclick ;
      private String edtFuncaoAPF_Nome_Jsonclick ;
      private String cmbFuncaoAPF_Tipo_Jsonclick ;
      private String cmbContagemItem_TipoUnidade_Jsonclick ;
      private String edtContagemItem_Evidencias_Jsonclick ;
      private String edtContagemItem_QtdINM_Jsonclick ;
      private String edtContagemItem_CP_Jsonclick ;
      private String edtContagemItem_RA_Jsonclick ;
      private String edtContagemItem_DER_Jsonclick ;
      private String edtContagemItem_Funcao_Jsonclick ;
      private String cmbFuncaoAPF_Complexidade_Jsonclick ;
      private String cmbContagemItem_FSTipoUnidade_Jsonclick ;
      private String cmbContagemItem_FSValidacao_Jsonclick ;
      private String edtContagemItem_FSReferenciaTecnicaCod_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV25DynamicFiltersEnabled3 ;
      private bool AV31DynamicFiltersIgnoreFirst ;
      private bool AV30DynamicFiltersRemoving ;
      private bool n165FuncaoAPF_Codigo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n225ContagemItem_Sequencial ;
      private bool n229ContagemItem_Requisito ;
      private bool n950ContagemItem_PFB ;
      private bool n951ContagemItem_PFL ;
      private bool n953ContagemItem_Evidencias ;
      private bool n954ContagemItem_QtdINM ;
      private bool n955ContagemItem_CP ;
      private bool n956ContagemItem_RA ;
      private bool n957ContagemItem_DER ;
      private bool n958ContagemItem_Funcao ;
      private bool n228ContagemItem_FSTipoUnidade ;
      private bool n267ContagemItem_FSValidacao ;
      private bool n226ContagemItem_FSReferenciaTecnicaCod ;
      private bool n255ContagemItem_FMReferenciaTecnicaCod ;
      private bool n257ContagemItem_FMReferenciaTecnicaNom ;
      private bool n269ContagemItem_FSReferenciaTecnicaNom ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV33Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private bool AV38Display_IsBlob ;
      private String A953ContagemItem_Evidencias ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV19FuncaoAPF_Nome1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV24FuncaoAPF_Nome2 ;
      private String AV26DynamicFiltersSelector3 ;
      private String AV29FuncaoAPF_Nome3 ;
      private String AV257Update_GXI ;
      private String AV258Delete_GXI ;
      private String AV259Display_GXI ;
      private String A166FuncaoAPF_Nome ;
      private String A952ContagemItem_TipoUnidade ;
      private String A958ContagemItem_Funcao ;
      private String A228ContagemItem_FSTipoUnidade ;
      private String lV19FuncaoAPF_Nome1 ;
      private String lV24FuncaoAPF_Nome2 ;
      private String lV29FuncaoAPF_Nome3 ;
      private String AV33Update ;
      private String AV32Delete ;
      private String AV38Display ;
      private IGxSession AV34Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbFuncaoAPF_Tipo ;
      private GXCombobox cmbContagemItem_TipoUnidade ;
      private GXCombobox cmbFuncaoAPF_Complexidade ;
      private GXCombobox cmbContagemItem_FSTipoUnidade ;
      private GXCombobox cmbContagemItem_FSValidacao ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00512_A255ContagemItem_FMReferenciaTecnicaCod ;
      private bool[] H00512_n255ContagemItem_FMReferenciaTecnicaCod ;
      private String[] H00512_A257ContagemItem_FMReferenciaTecnicaNom ;
      private bool[] H00512_n257ContagemItem_FMReferenciaTecnicaNom ;
      private String[] H00512_A269ContagemItem_FSReferenciaTecnicaNom ;
      private bool[] H00512_n269ContagemItem_FSReferenciaTecnicaNom ;
      private int[] H00512_A192Contagem_Codigo ;
      private int[] H00512_A226ContagemItem_FSReferenciaTecnicaCod ;
      private bool[] H00512_n226ContagemItem_FSReferenciaTecnicaCod ;
      private short[] H00512_A267ContagemItem_FSValidacao ;
      private bool[] H00512_n267ContagemItem_FSValidacao ;
      private String[] H00512_A228ContagemItem_FSTipoUnidade ;
      private bool[] H00512_n228ContagemItem_FSTipoUnidade ;
      private String[] H00512_A958ContagemItem_Funcao ;
      private bool[] H00512_n958ContagemItem_Funcao ;
      private short[] H00512_A957ContagemItem_DER ;
      private bool[] H00512_n957ContagemItem_DER ;
      private short[] H00512_A956ContagemItem_RA ;
      private bool[] H00512_n956ContagemItem_RA ;
      private String[] H00512_A955ContagemItem_CP ;
      private bool[] H00512_n955ContagemItem_CP ;
      private short[] H00512_A954ContagemItem_QtdINM ;
      private bool[] H00512_n954ContagemItem_QtdINM ;
      private String[] H00512_A953ContagemItem_Evidencias ;
      private bool[] H00512_n953ContagemItem_Evidencias ;
      private String[] H00512_A952ContagemItem_TipoUnidade ;
      private String[] H00512_A184FuncaoAPF_Tipo ;
      private String[] H00512_A166FuncaoAPF_Nome ;
      private decimal[] H00512_A951ContagemItem_PFL ;
      private bool[] H00512_n951ContagemItem_PFL ;
      private decimal[] H00512_A950ContagemItem_PFB ;
      private bool[] H00512_n950ContagemItem_PFB ;
      private short[] H00512_A229ContagemItem_Requisito ;
      private bool[] H00512_n229ContagemItem_Requisito ;
      private short[] H00512_A225ContagemItem_Sequencial ;
      private bool[] H00512_n225ContagemItem_Sequencial ;
      private String[] H00512_A218Contagem_UsuarioAuditorPessoaNom ;
      private int[] H00512_A217Contagem_UsuarioAuditorPessoaCod ;
      private int[] H00512_A216Contagem_UsuarioAuditorCod ;
      private String[] H00512_A209Contagem_FabricaSoftwarePessoaNom ;
      private int[] H00512_A208Contagem_FabricaSoftwarePessoaCod ;
      private int[] H00512_A207Contagem_FabricaSoftwareCod ;
      private int[] H00512_A224ContagemItem_Lancamento ;
      private int[] H00512_A165FuncaoAPF_Codigo ;
      private bool[] H00512_n165FuncaoAPF_Codigo ;
      private long[] H00513_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contagemcontagemitemwc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00512( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             int AV59Contagem_FabricaSoftwareCod1 ,
                                             String AV19FuncaoAPF_Nome1 ,
                                             String AV60ContagemItem_FSReferenciaTecnicaNom1 ,
                                             String AV61ContagemItem_FMReferenciaTecnicaNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             int AV62Contagem_FabricaSoftwareCod2 ,
                                             String AV24FuncaoAPF_Nome2 ,
                                             String AV63ContagemItem_FSReferenciaTecnicaNom2 ,
                                             String AV64ContagemItem_FMReferenciaTecnicaNom2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             short AV27DynamicFiltersOperator3 ,
                                             int AV65Contagem_FabricaSoftwareCod3 ,
                                             String AV29FuncaoAPF_Nome3 ,
                                             String AV66ContagemItem_FSReferenciaTecnicaNom3 ,
                                             String AV67ContagemItem_FMReferenciaTecnicaNom3 ,
                                             int A207Contagem_FabricaSoftwareCod ,
                                             String A166FuncaoAPF_Nome ,
                                             String A269ContagemItem_FSReferenciaTecnicaNom ,
                                             String A257ContagemItem_FMReferenciaTecnicaNom ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A192Contagem_Codigo ,
                                             int AV7Contagem_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [33] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContagemItem_FMReferenciaTecnicaCod] AS ContagemItem_FMReferenciaTecni, T2.[ReferenciaTecnica_Nome] AS ContagemItem_FMReferenciaTecni, T3.[ReferenciaTecnica_Nome] AS ContagemItem_FSReferenciaTecnicaNom, T1.[Contagem_Codigo], T1.[ContagemItem_FSReferenciaTecnicaCod] AS ContagemItem_FSReferenciaTecnicaCod, T1.[ContagemItem_FSValidacao], T1.[ContagemItem_FSTipoUnidade], T1.[ContagemItem_Funcao], T1.[ContagemItem_DER], T1.[ContagemItem_RA], T1.[ContagemItem_CP], T1.[ContagemItem_QtdINM], T1.[ContagemItem_Evidencias], T1.[ContagemItem_TipoUnidade], T4.[FuncaoAPF_Tipo], T4.[FuncaoAPF_Nome], T1.[ContagemItem_PFL], T1.[ContagemItem_PFB], T1.[ContagemItem_Requisito], T1.[ContagemItem_Sequencial], T1.[Contagem_UsuarioAuditorPessoaNom], T1.[Contagem_UsuarioAuditorPessoaCod], T1.[Contagem_UsuarioAuditorCod], T1.[Contagem_FabricaSoftwarePessoaNom], T1.[Contagem_FabricaSoftwarePessoaCod], T1.[Contagem_FabricaSoftwareCod], T1.[ContagemItem_Lancamento], T1.[FuncaoAPF_Codigo]";
         sFromString = " FROM ((([ContagemItem] T1 WITH (NOLOCK) LEFT JOIN [ReferenciaTecnica] T2 WITH (NOLOCK) ON T2.[ReferenciaTecnica_Codigo] = T1.[ContagemItem_FMReferenciaTecnicaCod]) LEFT JOIN [ReferenciaTecnica] T3 WITH (NOLOCK) ON T3.[ReferenciaTecnica_Codigo] = T1.[ContagemItem_FSReferenciaTecnicaCod]) LEFT JOIN [FuncoesAPF] T4 WITH (NOLOCK) ON T4.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Contagem_Codigo] = @AV7Contagem_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 0 ) || ( AV17DynamicFiltersOperator1 == 3 ) ) && ( ! (0==AV59Contagem_FabricaSoftwareCod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] < @AV59Contagem_FabricaSoftwareCod1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 1 ) || ( AV17DynamicFiltersOperator1 == 4 ) ) && ( ! (0==AV59Contagem_FabricaSoftwareCod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] = @AV59Contagem_FabricaSoftwareCod1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 2 ) || ( AV17DynamicFiltersOperator1 == 5 ) ) && ( ! (0==AV59Contagem_FabricaSoftwareCod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] > @AV59Contagem_FabricaSoftwareCod1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 0 ) || ( AV17DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPF_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV19FuncaoAPF_Nome1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 1 ) || ( AV17DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPF_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV19FuncaoAPF_Nome1)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 0 ) || ( AV17DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like @lV60ContagemItem_FSReferenciaTecnicaNom1)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 1 ) || ( AV17DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like '%' + @lV60ContagemItem_FSReferenciaTecnicaNom1)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 0 ) || ( AV17DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaTecnica_Nome] like @lV61ContagemItem_FMReferenciaTecnicaNom1)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 1 ) || ( AV17DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaTecnica_Nome] like '%' + @lV61ContagemItem_FMReferenciaTecnicaNom1)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! (0==AV62Contagem_FabricaSoftwareCod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] < @AV62Contagem_FabricaSoftwareCod2)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 4 ) ) && ( ! (0==AV62Contagem_FabricaSoftwareCod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] = @AV62Contagem_FabricaSoftwareCod2)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 2 ) || ( AV22DynamicFiltersOperator2 == 5 ) ) && ( ! (0==AV62Contagem_FabricaSoftwareCod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] > @AV62Contagem_FabricaSoftwareCod2)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPF_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV24FuncaoAPF_Nome2)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPF_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV24FuncaoAPF_Nome2)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemItem_FSReferenciaTecnicaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like @lV63ContagemItem_FSReferenciaTecnicaNom2)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemItem_FSReferenciaTecnicaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like '%' + @lV63ContagemItem_FSReferenciaTecnicaNom2)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemItem_FMReferenciaTecnicaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaTecnica_Nome] like @lV64ContagemItem_FMReferenciaTecnicaNom2)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemItem_FMReferenciaTecnicaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaTecnica_Nome] like '%' + @lV64ContagemItem_FMReferenciaTecnicaNom2)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 0 ) || ( AV27DynamicFiltersOperator3 == 3 ) ) && ( ! (0==AV65Contagem_FabricaSoftwareCod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] < @AV65Contagem_FabricaSoftwareCod3)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 1 ) || ( AV27DynamicFiltersOperator3 == 4 ) ) && ( ! (0==AV65Contagem_FabricaSoftwareCod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] = @AV65Contagem_FabricaSoftwareCod3)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 2 ) || ( AV27DynamicFiltersOperator3 == 5 ) ) && ( ! (0==AV65Contagem_FabricaSoftwareCod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] > @AV65Contagem_FabricaSoftwareCod3)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 0 ) || ( AV27DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29FuncaoAPF_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV29FuncaoAPF_Nome3)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 1 ) || ( AV27DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29FuncaoAPF_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV29FuncaoAPF_Nome3)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 0 ) || ( AV27DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66ContagemItem_FSReferenciaTecnicaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like @lV66ContagemItem_FSReferenciaTecnicaNom3)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 1 ) || ( AV27DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66ContagemItem_FSReferenciaTecnicaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like '%' + @lV66ContagemItem_FSReferenciaTecnicaNom3)";
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 0 ) || ( AV27DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67ContagemItem_FMReferenciaTecnicaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaTecnica_Nome] like @lV67ContagemItem_FMReferenciaTecnicaNom3)";
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 1 ) || ( AV27DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67ContagemItem_FMReferenciaTecnicaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaTecnica_Nome] like '%' + @lV67ContagemItem_FMReferenciaTecnicaNom3)";
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[Contagem_FabricaSoftwareCod]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[Contagem_FabricaSoftwareCod] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_Lancamento]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_Lancamento] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[Contagem_FabricaSoftwarePessoaCod]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[Contagem_FabricaSoftwarePessoaCod] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[Contagem_FabricaSoftwarePessoaNom]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[Contagem_FabricaSoftwarePessoaNom] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[Contagem_UsuarioAuditorCod]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[Contagem_UsuarioAuditorCod] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[Contagem_UsuarioAuditorPessoaCod]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[Contagem_UsuarioAuditorPessoaCod] DESC";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[Contagem_UsuarioAuditorPessoaNom]";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[Contagem_UsuarioAuditorPessoaNom] DESC";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_Sequencial]";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_Sequencial] DESC";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_Requisito]";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_Requisito] DESC";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[FuncaoAPF_Codigo]";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[FuncaoAPF_Codigo] DESC";
         }
         else if ( ( AV14OrderedBy == 11 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_PFB]";
         }
         else if ( ( AV14OrderedBy == 11 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_PFB] DESC";
         }
         else if ( ( AV14OrderedBy == 12 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_PFL]";
         }
         else if ( ( AV14OrderedBy == 12 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_PFL] DESC";
         }
         else if ( ( AV14OrderedBy == 13 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T4.[FuncaoAPF_Nome]";
         }
         else if ( ( AV14OrderedBy == 13 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T4.[FuncaoAPF_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 14 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T4.[FuncaoAPF_Tipo]";
         }
         else if ( ( AV14OrderedBy == 14 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T4.[FuncaoAPF_Tipo] DESC";
         }
         else if ( ( AV14OrderedBy == 15 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_TipoUnidade]";
         }
         else if ( ( AV14OrderedBy == 15 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_TipoUnidade] DESC";
         }
         else if ( ( AV14OrderedBy == 16 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_Evidencias]";
         }
         else if ( ( AV14OrderedBy == 16 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_Evidencias] DESC";
         }
         else if ( ( AV14OrderedBy == 17 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_QtdINM]";
         }
         else if ( ( AV14OrderedBy == 17 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_QtdINM] DESC";
         }
         else if ( ( AV14OrderedBy == 18 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_CP]";
         }
         else if ( ( AV14OrderedBy == 18 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_CP] DESC";
         }
         else if ( ( AV14OrderedBy == 19 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_RA]";
         }
         else if ( ( AV14OrderedBy == 19 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_RA] DESC";
         }
         else if ( ( AV14OrderedBy == 20 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_DER]";
         }
         else if ( ( AV14OrderedBy == 20 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_DER] DESC";
         }
         else if ( ( AV14OrderedBy == 21 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_Funcao]";
         }
         else if ( ( AV14OrderedBy == 21 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_Funcao] DESC";
         }
         else if ( ( AV14OrderedBy == 22 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_FSTipoUnidade]";
         }
         else if ( ( AV14OrderedBy == 22 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_FSTipoUnidade] DESC";
         }
         else if ( ( AV14OrderedBy == 23 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_FSValidacao]";
         }
         else if ( ( AV14OrderedBy == 23 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_FSValidacao] DESC";
         }
         else if ( ( AV14OrderedBy == 24 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo], T1.[ContagemItem_FSReferenciaTecnicaCod]";
         }
         else if ( ( AV14OrderedBy == 24 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contagem_Codigo] DESC, T1.[ContagemItem_FSReferenciaTecnicaCod] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemItem_Lancamento]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00513( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             int AV59Contagem_FabricaSoftwareCod1 ,
                                             String AV19FuncaoAPF_Nome1 ,
                                             String AV60ContagemItem_FSReferenciaTecnicaNom1 ,
                                             String AV61ContagemItem_FMReferenciaTecnicaNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             int AV62Contagem_FabricaSoftwareCod2 ,
                                             String AV24FuncaoAPF_Nome2 ,
                                             String AV63ContagemItem_FSReferenciaTecnicaNom2 ,
                                             String AV64ContagemItem_FMReferenciaTecnicaNom2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             short AV27DynamicFiltersOperator3 ,
                                             int AV65Contagem_FabricaSoftwareCod3 ,
                                             String AV29FuncaoAPF_Nome3 ,
                                             String AV66ContagemItem_FSReferenciaTecnicaNom3 ,
                                             String AV67ContagemItem_FMReferenciaTecnicaNom3 ,
                                             int A207Contagem_FabricaSoftwareCod ,
                                             String A166FuncaoAPF_Nome ,
                                             String A269ContagemItem_FSReferenciaTecnicaNom ,
                                             String A257ContagemItem_FMReferenciaTecnicaNom ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A192Contagem_Codigo ,
                                             int AV7Contagem_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [28] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContagemItem] T1 WITH (NOLOCK) LEFT JOIN [ReferenciaTecnica] T4 WITH (NOLOCK) ON T4.[ReferenciaTecnica_Codigo] = T1.[ContagemItem_FMReferenciaTecnicaCod]) LEFT JOIN [ReferenciaTecnica] T3 WITH (NOLOCK) ON T3.[ReferenciaTecnica_Codigo] = T1.[ContagemItem_FSReferenciaTecnicaCod]) LEFT JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contagem_Codigo] = @AV7Contagem_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 0 ) || ( AV17DynamicFiltersOperator1 == 3 ) ) && ( ! (0==AV59Contagem_FabricaSoftwareCod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] < @AV59Contagem_FabricaSoftwareCod1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 1 ) || ( AV17DynamicFiltersOperator1 == 4 ) ) && ( ! (0==AV59Contagem_FabricaSoftwareCod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] = @AV59Contagem_FabricaSoftwareCod1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 2 ) || ( AV17DynamicFiltersOperator1 == 5 ) ) && ( ! (0==AV59Contagem_FabricaSoftwareCod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] > @AV59Contagem_FabricaSoftwareCod1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 0 ) || ( AV17DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPF_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV19FuncaoAPF_Nome1)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 1 ) || ( AV17DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPF_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV19FuncaoAPF_Nome1)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 0 ) || ( AV17DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like @lV60ContagemItem_FSReferenciaTecnicaNom1)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 1 ) || ( AV17DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemItem_FSReferenciaTecnicaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like '%' + @lV60ContagemItem_FSReferenciaTecnicaNom1)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 0 ) || ( AV17DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ReferenciaTecnica_Nome] like @lV61ContagemItem_FMReferenciaTecnicaNom1)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV17DynamicFiltersOperator1 == 1 ) || ( AV17DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemItem_FMReferenciaTecnicaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ReferenciaTecnica_Nome] like '%' + @lV61ContagemItem_FMReferenciaTecnicaNom1)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! (0==AV62Contagem_FabricaSoftwareCod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] < @AV62Contagem_FabricaSoftwareCod2)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 4 ) ) && ( ! (0==AV62Contagem_FabricaSoftwareCod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] = @AV62Contagem_FabricaSoftwareCod2)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 2 ) || ( AV22DynamicFiltersOperator2 == 5 ) ) && ( ! (0==AV62Contagem_FabricaSoftwareCod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] > @AV62Contagem_FabricaSoftwareCod2)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPF_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV24FuncaoAPF_Nome2)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPF_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV24FuncaoAPF_Nome2)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemItem_FSReferenciaTecnicaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like @lV63ContagemItem_FSReferenciaTecnicaNom2)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemItem_FSReferenciaTecnicaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like '%' + @lV63ContagemItem_FSReferenciaTecnicaNom2)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 0 ) || ( AV22DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemItem_FMReferenciaTecnicaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ReferenciaTecnica_Nome] like @lV64ContagemItem_FMReferenciaTecnicaNom2)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV22DynamicFiltersOperator2 == 1 ) || ( AV22DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemItem_FMReferenciaTecnicaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ReferenciaTecnica_Nome] like '%' + @lV64ContagemItem_FMReferenciaTecnicaNom2)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 0 ) || ( AV27DynamicFiltersOperator3 == 3 ) ) && ( ! (0==AV65Contagem_FabricaSoftwareCod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] < @AV65Contagem_FabricaSoftwareCod3)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 1 ) || ( AV27DynamicFiltersOperator3 == 4 ) ) && ( ! (0==AV65Contagem_FabricaSoftwareCod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] = @AV65Contagem_FabricaSoftwareCod3)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEM_FABRICASOFTWARECOD") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 2 ) || ( AV27DynamicFiltersOperator3 == 5 ) ) && ( ! (0==AV65Contagem_FabricaSoftwareCod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contagem_FabricaSoftwareCod] > @AV65Contagem_FabricaSoftwareCod3)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 0 ) || ( AV27DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29FuncaoAPF_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV29FuncaoAPF_Nome3)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 1 ) || ( AV27DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29FuncaoAPF_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV29FuncaoAPF_Nome3)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 0 ) || ( AV27DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66ContagemItem_FSReferenciaTecnicaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like @lV66ContagemItem_FSReferenciaTecnicaNom3)";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FSREFERENCIATECNICANOM") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 1 ) || ( AV27DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66ContagemItem_FSReferenciaTecnicaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ReferenciaTecnica_Nome] like '%' + @lV66ContagemItem_FSReferenciaTecnicaNom3)";
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 0 ) || ( AV27DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67ContagemItem_FMReferenciaTecnicaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ReferenciaTecnica_Nome] like @lV67ContagemItem_FMReferenciaTecnicaNom3)";
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTAGEMITEM_FMREFERENCIATECNICANOM") == 0 ) && ( ( AV27DynamicFiltersOperator3 == 1 ) || ( AV27DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67ContagemItem_FMReferenciaTecnicaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ReferenciaTecnica_Nome] like '%' + @lV67ContagemItem_FMReferenciaTecnicaNom3)";
         }
         else
         {
            GXv_int4[27] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 11 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 11 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 12 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 12 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 13 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 13 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 14 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 14 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 15 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 15 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 16 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 16 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 17 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 17 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 18 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 18 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 19 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 19 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 20 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 20 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 21 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 21 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 22 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 22 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 23 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 23 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 24 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 24 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00512(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] );
               case 1 :
                     return conditional_H00513(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00512 ;
          prmH00512 = new Object[] {
          new Object[] {"@AV7Contagem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV59Contagem_FabricaSoftwareCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV59Contagem_FabricaSoftwareCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV59Contagem_FabricaSoftwareCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV19FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV19FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV60ContagemItem_FSReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemItem_FSReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61ContagemItem_FMReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61ContagemItem_FMReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV62Contagem_FabricaSoftwareCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV62Contagem_FabricaSoftwareCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV62Contagem_FabricaSoftwareCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV24FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV24FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV63ContagemItem_FSReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63ContagemItem_FSReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64ContagemItem_FMReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64ContagemItem_FMReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV65Contagem_FabricaSoftwareCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65Contagem_FabricaSoftwareCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65Contagem_FabricaSoftwareCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV29FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV29FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV66ContagemItem_FSReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66ContagemItem_FSReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67ContagemItem_FMReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67ContagemItem_FMReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00513 ;
          prmH00513 = new Object[] {
          new Object[] {"@AV7Contagem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV59Contagem_FabricaSoftwareCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV59Contagem_FabricaSoftwareCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV59Contagem_FabricaSoftwareCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV19FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV19FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV60ContagemItem_FSReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemItem_FSReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61ContagemItem_FMReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61ContagemItem_FMReferenciaTecnicaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV62Contagem_FabricaSoftwareCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV62Contagem_FabricaSoftwareCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV62Contagem_FabricaSoftwareCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV24FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV24FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV63ContagemItem_FSReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63ContagemItem_FSReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64ContagemItem_FMReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64ContagemItem_FMReferenciaTecnicaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV65Contagem_FabricaSoftwareCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65Contagem_FabricaSoftwareCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65Contagem_FabricaSoftwareCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV29FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV29FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV66ContagemItem_FSReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66ContagemItem_FSReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67ContagemItem_FMReferenciaTecnicaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67ContagemItem_FMReferenciaTecnicaNom3",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00512", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00512,11,0,true,false )
             ,new CursorDef("H00513", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00513,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((short[]) buf[21])[0] = rslt.getShort(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((String[]) buf[23])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((String[]) buf[25])[0] = rslt.getVarchar(14) ;
                ((String[]) buf[26])[0] = rslt.getString(15, 3) ;
                ((String[]) buf[27])[0] = rslt.getVarchar(16) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((decimal[]) buf[30])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((short[]) buf[32])[0] = rslt.getShort(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((short[]) buf[34])[0] = rslt.getShort(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((String[]) buf[36])[0] = rslt.getString(21, 50) ;
                ((int[]) buf[37])[0] = rslt.getInt(22) ;
                ((int[]) buf[38])[0] = rslt.getInt(23) ;
                ((String[]) buf[39])[0] = rslt.getString(24, 50) ;
                ((int[]) buf[40])[0] = rslt.getInt(25) ;
                ((int[]) buf[41])[0] = rslt.getInt(26) ;
                ((int[]) buf[42])[0] = rslt.getInt(27) ;
                ((int[]) buf[43])[0] = rslt.getInt(28) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(28);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[62]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[63]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                return;
       }
    }

 }

}
