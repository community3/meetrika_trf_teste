/*
               File: type_SdtProjeto_Equipe
        Description: Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:51.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Projeto.Equipe" )]
   [XmlType(TypeName =  "Projeto.Equipe" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtProjeto_Equipe : GxSilentTrnSdt, IGxSilentTrnGridItem
   {
      public SdtProjeto_Equipe( )
      {
         /* Constructor for serialization */
         gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa = "";
         gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome = "";
         gxTv_SdtProjeto_Equipe_Mode = "";
         gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_Z = "";
         gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_Z = "";
      }

      public SdtProjeto_Equipe( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Equipe");
         metadata.Set("BT", "ProjetoEquipe");
         metadata.Set("PK", "[ \"ProjetoEquipe_UsuarioCodigo\" ]");
         metadata.Set("PKAssigned", "[ \"ProjetoEquipe_UsuarioCodigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"FuncaoUsuario_Codigo\" ],\"FKMap\":[ \"ProjetoEquipe_FuncaoUsuarioCod-FuncaoUsuario_Codigo\" ] },{ \"FK\":[ \"Projeto_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ProjetoEquipe_UsuarioCodigo-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modified" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projetoequipe_usuariocodigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projetoequipe_usuariopessoa_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projetoequipe_funcaousuariocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projetoequipe_funcaousuarionome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projetoequipe_funcaousuarionome_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtProjeto_Equipe deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtProjeto_Equipe)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtProjeto_Equipe obj ;
         obj = this;
         obj.gxTpr_Projetoequipe_usuariocodigo = deserialized.gxTpr_Projetoequipe_usuariocodigo;
         obj.gxTpr_Projetoequipe_usuariopessoa = deserialized.gxTpr_Projetoequipe_usuariopessoa;
         obj.gxTpr_Projetoequipe_funcaousuariocod = deserialized.gxTpr_Projetoequipe_funcaousuariocod;
         obj.gxTpr_Projetoequipe_funcaousuarionome = deserialized.gxTpr_Projetoequipe_funcaousuarionome;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Modified = deserialized.gxTpr_Modified;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Projetoequipe_usuariocodigo_Z = deserialized.gxTpr_Projetoequipe_usuariocodigo_Z;
         obj.gxTpr_Projetoequipe_usuariopessoa_Z = deserialized.gxTpr_Projetoequipe_usuariopessoa_Z;
         obj.gxTpr_Projetoequipe_funcaousuariocod_Z = deserialized.gxTpr_Projetoequipe_funcaousuariocod_Z;
         obj.gxTpr_Projetoequipe_funcaousuarionome_Z = deserialized.gxTpr_Projetoequipe_funcaousuarionome_Z;
         obj.gxTpr_Projetoequipe_funcaousuarionome_N = deserialized.gxTpr_Projetoequipe_funcaousuarionome_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ProjetoEquipe_UsuarioCodigo") )
               {
                  gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ProjetoEquipe_UsuarioPessoa") )
               {
                  gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ProjetoEquipe_FuncaoUsuarioCod") )
               {
                  gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ProjetoEquipe_FuncaoUsuarioNome") )
               {
                  gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtProjeto_Equipe_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modified") )
               {
                  gxTv_SdtProjeto_Equipe_Modified = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtProjeto_Equipe_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ProjetoEquipe_UsuarioCodigo_Z") )
               {
                  gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ProjetoEquipe_UsuarioPessoa_Z") )
               {
                  gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ProjetoEquipe_FuncaoUsuarioCod_Z") )
               {
                  gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ProjetoEquipe_FuncaoUsuarioNome_Z") )
               {
                  gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ProjetoEquipe_FuncaoUsuarioNome_N") )
               {
                  gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Projeto.Equipe";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ProjetoEquipe_UsuarioCodigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ProjetoEquipe_UsuarioPessoa", StringUtil.RTrim( gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ProjetoEquipe_FuncaoUsuarioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ProjetoEquipe_FuncaoUsuarioNome", StringUtil.RTrim( gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtProjeto_Equipe_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Modified", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Equipe_Modified), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Equipe_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ProjetoEquipe_UsuarioCodigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ProjetoEquipe_UsuarioPessoa_Z", StringUtil.RTrim( gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ProjetoEquipe_FuncaoUsuarioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ProjetoEquipe_FuncaoUsuarioNome_Z", StringUtil.RTrim( gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ProjetoEquipe_FuncaoUsuarioNome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ProjetoEquipe_UsuarioCodigo", gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo, false);
         AddObjectProperty("ProjetoEquipe_UsuarioPessoa", gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa, false);
         AddObjectProperty("ProjetoEquipe_FuncaoUsuarioCod", gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod, false);
         AddObjectProperty("ProjetoEquipe_FuncaoUsuarioNome", gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtProjeto_Equipe_Mode, false);
            AddObjectProperty("Modified", gxTv_SdtProjeto_Equipe_Modified, false);
            AddObjectProperty("Initialized", gxTv_SdtProjeto_Equipe_Initialized, false);
            AddObjectProperty("ProjetoEquipe_UsuarioCodigo_Z", gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo_Z, false);
            AddObjectProperty("ProjetoEquipe_UsuarioPessoa_Z", gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_Z, false);
            AddObjectProperty("ProjetoEquipe_FuncaoUsuarioCod_Z", gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod_Z, false);
            AddObjectProperty("ProjetoEquipe_FuncaoUsuarioNome_Z", gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_Z, false);
            AddObjectProperty("ProjetoEquipe_FuncaoUsuarioNome_N", gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ProjetoEquipe_UsuarioCodigo" )]
      [  XmlElement( ElementName = "ProjetoEquipe_UsuarioCodigo"   )]
      public int gxTpr_Projetoequipe_usuariocodigo
      {
         get {
            return gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo ;
         }

         set {
            gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo = (int)(value);
            gxTv_SdtProjeto_Equipe_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "ProjetoEquipe_UsuarioPessoa" )]
      [  XmlElement( ElementName = "ProjetoEquipe_UsuarioPessoa"   )]
      public String gxTpr_Projetoequipe_usuariopessoa
      {
         get {
            return gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa ;
         }

         set {
            gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa = (String)(value);
            gxTv_SdtProjeto_Equipe_Modified = 1;
         }

      }

      public void gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_SetNull( )
      {
         gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ProjetoEquipe_FuncaoUsuarioCod" )]
      [  XmlElement( ElementName = "ProjetoEquipe_FuncaoUsuarioCod"   )]
      public int gxTpr_Projetoequipe_funcaousuariocod
      {
         get {
            return gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod ;
         }

         set {
            gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod = (int)(value);
            gxTv_SdtProjeto_Equipe_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "ProjetoEquipe_FuncaoUsuarioNome" )]
      [  XmlElement( ElementName = "ProjetoEquipe_FuncaoUsuarioNome"   )]
      public String gxTpr_Projetoequipe_funcaousuarionome
      {
         get {
            return gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome ;
         }

         set {
            gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_N = 0;
            gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome = (String)(value);
            gxTv_SdtProjeto_Equipe_Modified = 1;
         }

      }

      public void gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_SetNull( )
      {
         gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_N = 1;
         gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtProjeto_Equipe_Mode ;
         }

         set {
            gxTv_SdtProjeto_Equipe_Mode = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Equipe_Mode_SetNull( )
      {
         gxTv_SdtProjeto_Equipe_Mode = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Equipe_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modified" )]
      [  XmlElement( ElementName = "Modified"   )]
      public short gxTpr_Modified
      {
         get {
            return gxTv_SdtProjeto_Equipe_Modified ;
         }

         set {
            gxTv_SdtProjeto_Equipe_Modified = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Equipe_Modified_SetNull( )
      {
         gxTv_SdtProjeto_Equipe_Modified = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Equipe_Modified_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtProjeto_Equipe_Initialized ;
         }

         set {
            gxTv_SdtProjeto_Equipe_Initialized = (short)(value);
            gxTv_SdtProjeto_Equipe_Modified = 1;
         }

      }

      public void gxTv_SdtProjeto_Equipe_Initialized_SetNull( )
      {
         gxTv_SdtProjeto_Equipe_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Equipe_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ProjetoEquipe_UsuarioCodigo_Z" )]
      [  XmlElement( ElementName = "ProjetoEquipe_UsuarioCodigo_Z"   )]
      public int gxTpr_Projetoequipe_usuariocodigo_Z
      {
         get {
            return gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo_Z ;
         }

         set {
            gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo_Z = (int)(value);
            gxTv_SdtProjeto_Equipe_Modified = 1;
         }

      }

      public void gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ProjetoEquipe_UsuarioPessoa_Z" )]
      [  XmlElement( ElementName = "ProjetoEquipe_UsuarioPessoa_Z"   )]
      public String gxTpr_Projetoequipe_usuariopessoa_Z
      {
         get {
            return gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_Z ;
         }

         set {
            gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_Z = (String)(value);
            gxTv_SdtProjeto_Equipe_Modified = 1;
         }

      }

      public void gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_Z_SetNull( )
      {
         gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ProjetoEquipe_FuncaoUsuarioCod_Z" )]
      [  XmlElement( ElementName = "ProjetoEquipe_FuncaoUsuarioCod_Z"   )]
      public int gxTpr_Projetoequipe_funcaousuariocod_Z
      {
         get {
            return gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod_Z ;
         }

         set {
            gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod_Z = (int)(value);
            gxTv_SdtProjeto_Equipe_Modified = 1;
         }

      }

      public void gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod_Z_SetNull( )
      {
         gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ProjetoEquipe_FuncaoUsuarioNome_Z" )]
      [  XmlElement( ElementName = "ProjetoEquipe_FuncaoUsuarioNome_Z"   )]
      public String gxTpr_Projetoequipe_funcaousuarionome_Z
      {
         get {
            return gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_Z ;
         }

         set {
            gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_Z = (String)(value);
            gxTv_SdtProjeto_Equipe_Modified = 1;
         }

      }

      public void gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_Z_SetNull( )
      {
         gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ProjetoEquipe_FuncaoUsuarioNome_N" )]
      [  XmlElement( ElementName = "ProjetoEquipe_FuncaoUsuarioNome_N"   )]
      public short gxTpr_Projetoequipe_funcaousuarionome_N
      {
         get {
            return gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_N ;
         }

         set {
            gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_N = (short)(value);
            gxTv_SdtProjeto_Equipe_Modified = 1;
         }

      }

      public void gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_N_SetNull( )
      {
         gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa = "";
         gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome = "";
         gxTv_SdtProjeto_Equipe_Mode = "";
         gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_Z = "";
         gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_Z = "";
         sTagName = "";
         return  ;
      }

      private short gxTv_SdtProjeto_Equipe_Modified ;
      private short gxTv_SdtProjeto_Equipe_Initialized ;
      private short gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo ;
      private int gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod ;
      private int gxTv_SdtProjeto_Equipe_Projetoequipe_usuariocodigo_Z ;
      private int gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuariocod_Z ;
      private String gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa ;
      private String gxTv_SdtProjeto_Equipe_Mode ;
      private String gxTv_SdtProjeto_Equipe_Projetoequipe_usuariopessoa_Z ;
      private String sTagName ;
      private String gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome ;
      private String gxTv_SdtProjeto_Equipe_Projetoequipe_funcaousuarionome_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Projeto.Equipe", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtProjeto_Equipe_RESTInterface : GxGenericCollectionItem<SdtProjeto_Equipe>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtProjeto_Equipe_RESTInterface( ) : base()
      {
      }

      public SdtProjeto_Equipe_RESTInterface( SdtProjeto_Equipe psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ProjetoEquipe_UsuarioCodigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projetoequipe_usuariocodigo
      {
         get {
            return sdt.gxTpr_Projetoequipe_usuariocodigo ;
         }

         set {
            sdt.gxTpr_Projetoequipe_usuariocodigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ProjetoEquipe_UsuarioPessoa" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Projetoequipe_usuariopessoa
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projetoequipe_usuariopessoa) ;
         }

         set {
            sdt.gxTpr_Projetoequipe_usuariopessoa = (String)(value);
         }

      }

      [DataMember( Name = "ProjetoEquipe_FuncaoUsuarioCod" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projetoequipe_funcaousuariocod
      {
         get {
            return sdt.gxTpr_Projetoequipe_funcaousuariocod ;
         }

         set {
            sdt.gxTpr_Projetoequipe_funcaousuariocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ProjetoEquipe_FuncaoUsuarioNome" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Projetoequipe_funcaousuarionome
      {
         get {
            return sdt.gxTpr_Projetoequipe_funcaousuarionome ;
         }

         set {
            sdt.gxTpr_Projetoequipe_funcaousuarionome = (String)(value);
         }

      }

      public SdtProjeto_Equipe sdt
      {
         get {
            return (SdtProjeto_Equipe)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtProjeto_Equipe() ;
         }
      }

   }

}
