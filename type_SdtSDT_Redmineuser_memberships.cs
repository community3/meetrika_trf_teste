/*
               File: type_SdtSDT_Redmineuser_memberships
        Description: SDT_Redmineuser
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:6.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Redmineuser.memberships" )]
   [XmlType(TypeName =  "SDT_Redmineuser.memberships" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_Redmineuser_memberships_membership ))]
   [Serializable]
   public class SdtSDT_Redmineuser_memberships : GxUserType
   {
      public SdtSDT_Redmineuser_memberships( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Redmineuser_memberships_Type = "";
      }

      public SdtSDT_Redmineuser_memberships( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Redmineuser_memberships deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Redmineuser_memberships)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Redmineuser_memberships obj ;
         obj = this;
         obj.gxTpr_Type = deserialized.gxTpr_Type;
         obj.gxTpr_Memberships = deserialized.gxTpr_Memberships;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("type") == 1 )
         {
            gxTv_SdtSDT_Redmineuser_memberships_Type = oReader.GetAttributeByName("type");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            if ( gxTv_SdtSDT_Redmineuser_memberships_Memberships != null )
            {
               gxTv_SdtSDT_Redmineuser_memberships_Memberships.ClearCollection();
            }
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp(oReader.LocalName, "membership") == 0 )
               {
                  if ( gxTv_SdtSDT_Redmineuser_memberships_Memberships == null )
                  {
                     gxTv_SdtSDT_Redmineuser_memberships_Memberships = new GxObjectCollection( context, "SDT_Redmineuser.memberships.membership", "", "SdtSDT_Redmineuser_memberships_membership", "GeneXus.Programs");
                  }
                  GXSoapError = gxTv_SdtSDT_Redmineuser_memberships_Memberships.readxmlcollection(oReader, "", "membership");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Redmineuser.memberships";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("type", StringUtil.RTrim( gxTv_SdtSDT_Redmineuser_memberships_Type));
         if ( gxTv_SdtSDT_Redmineuser_memberships_Memberships != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_Redmineuser_memberships_Memberships.writexmlcollection(oWriter, "", sNameSpace1, "membership", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("type", gxTv_SdtSDT_Redmineuser_memberships_Type, false);
         if ( gxTv_SdtSDT_Redmineuser_memberships_Memberships != null )
         {
            AddObjectProperty("memberships", gxTv_SdtSDT_Redmineuser_memberships_Memberships, false);
         }
         return  ;
      }

      [SoapAttribute( AttributeName = "type" )]
      [XmlAttribute( AttributeName = "type" )]
      public String gxTpr_Type
      {
         get {
            return gxTv_SdtSDT_Redmineuser_memberships_Type ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_memberships_Type = (String)(value);
         }

      }

      public class gxTv_SdtSDT_Redmineuser_memberships_Memberships_SdtSDT_Redmineuser_memberships_membership_80compatibility:SdtSDT_Redmineuser_memberships_membership {}
      [  SoapElement( ElementName = "membership" )]
      [  XmlElement( ElementName = "membership" , Namespace = "" , Type= typeof( SdtSDT_Redmineuser_memberships_membership ))]
      public GxObjectCollection gxTpr_Memberships_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_Redmineuser_memberships_Memberships == null )
            {
               gxTv_SdtSDT_Redmineuser_memberships_Memberships = new GxObjectCollection( context, "SDT_Redmineuser.memberships.membership", "", "SdtSDT_Redmineuser_memberships_membership", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_Redmineuser_memberships_Memberships ;
         }

         set {
            if ( gxTv_SdtSDT_Redmineuser_memberships_Memberships == null )
            {
               gxTv_SdtSDT_Redmineuser_memberships_Memberships = new GxObjectCollection( context, "SDT_Redmineuser.memberships.membership", "", "SdtSDT_Redmineuser_memberships_membership", "GeneXus.Programs");
            }
            gxTv_SdtSDT_Redmineuser_memberships_Memberships = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Memberships
      {
         get {
            if ( gxTv_SdtSDT_Redmineuser_memberships_Memberships == null )
            {
               gxTv_SdtSDT_Redmineuser_memberships_Memberships = new GxObjectCollection( context, "SDT_Redmineuser.memberships.membership", "", "SdtSDT_Redmineuser_memberships_membership", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_Redmineuser_memberships_Memberships ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_memberships_Memberships = value;
         }

      }

      public void gxTv_SdtSDT_Redmineuser_memberships_Memberships_SetNull( )
      {
         gxTv_SdtSDT_Redmineuser_memberships_Memberships = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Redmineuser_memberships_Memberships_IsNull( )
      {
         if ( gxTv_SdtSDT_Redmineuser_memberships_Memberships == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_Redmineuser_memberships_Type = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_Redmineuser_memberships_Type ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Redmineuser_memberships_membership ))]
      protected IGxCollection gxTv_SdtSDT_Redmineuser_memberships_Memberships=null ;
   }

   [DataContract(Name = @"SDT_Redmineuser.memberships", Namespace = "")]
   public class SdtSDT_Redmineuser_memberships_RESTInterface : GxGenericCollectionItem<SdtSDT_Redmineuser_memberships>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Redmineuser_memberships_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Redmineuser_memberships_RESTInterface( SdtSDT_Redmineuser_memberships psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "type" , Order = 0 )]
      public String gxTpr_Type
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Type) ;
         }

         set {
            sdt.gxTpr_Type = (String)(value);
         }

      }

      [DataMember( Name = "memberships" , Order = 1 )]
      public GxGenericCollection<SdtSDT_Redmineuser_memberships_membership_RESTInterface> gxTpr_Memberships
      {
         get {
            return new GxGenericCollection<SdtSDT_Redmineuser_memberships_membership_RESTInterface>(sdt.gxTpr_Memberships) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Memberships);
         }

      }

      public SdtSDT_Redmineuser_memberships sdt
      {
         get {
            return (SdtSDT_Redmineuser_memberships)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Redmineuser_memberships() ;
         }
      }

   }

}
