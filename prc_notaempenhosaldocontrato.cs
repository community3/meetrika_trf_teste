/*
               File: PRC_NotaEmpenhoSaldoContrato
        Description: PRC_Nota Empenho Saldo Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:35.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_notaempenhosaldocontrato : GXProcedure
   {
      public prc_notaempenhosaldocontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_notaempenhosaldocontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_SaldoContrato_Codigo ,
                           String aP1_HistoricoCodigo ,
                           int aP2_NotaEmpenho_Codigo ,
                           decimal aP3_AntNotaEmpenho_Valor ,
                           decimal aP4_NotaEmpenho_Valor )
      {
         this.AV8SaldoContrato_Codigo = aP0_SaldoContrato_Codigo;
         this.AV14HistoricoCodigo = aP1_HistoricoCodigo;
         this.AV12NotaEmpenho_Codigo = aP2_NotaEmpenho_Codigo;
         this.AV15AntNotaEmpenho_Valor = aP3_AntNotaEmpenho_Valor;
         this.AV9NotaEmpenho_Valor = aP4_NotaEmpenho_Valor;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_SaldoContrato_Codigo ,
                                 String aP1_HistoricoCodigo ,
                                 int aP2_NotaEmpenho_Codigo ,
                                 decimal aP3_AntNotaEmpenho_Valor ,
                                 decimal aP4_NotaEmpenho_Valor )
      {
         prc_notaempenhosaldocontrato objprc_notaempenhosaldocontrato;
         objprc_notaempenhosaldocontrato = new prc_notaempenhosaldocontrato();
         objprc_notaempenhosaldocontrato.AV8SaldoContrato_Codigo = aP0_SaldoContrato_Codigo;
         objprc_notaempenhosaldocontrato.AV14HistoricoCodigo = aP1_HistoricoCodigo;
         objprc_notaempenhosaldocontrato.AV12NotaEmpenho_Codigo = aP2_NotaEmpenho_Codigo;
         objprc_notaempenhosaldocontrato.AV15AntNotaEmpenho_Valor = aP3_AntNotaEmpenho_Valor;
         objprc_notaempenhosaldocontrato.AV9NotaEmpenho_Valor = aP4_NotaEmpenho_Valor;
         objprc_notaempenhosaldocontrato.context.SetSubmitInitialConfig(context);
         objprc_notaempenhosaldocontrato.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_notaempenhosaldocontrato);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_notaempenhosaldocontrato)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00BH2 */
         pr_default.execute(0, new Object[] {AV8SaldoContrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1561SaldoContrato_Codigo = P00BH2_A1561SaldoContrato_Codigo[0];
            A1576SaldoContrato_Saldo = P00BH2_A1576SaldoContrato_Saldo[0];
            A74Contrato_Codigo = P00BH2_A74Contrato_Codigo[0];
            if ( StringUtil.StrCmp(AV14HistoricoCodigo, "CRI") == 0 )
            {
               A1576SaldoContrato_Saldo = (decimal)(A1576SaldoContrato_Saldo+AV9NotaEmpenho_Valor);
               new prc_historicoconsumo(context ).execute(  AV8SaldoContrato_Codigo,  A74Contrato_Codigo,  AV12NotaEmpenho_Codigo,  0,  0,  AV9NotaEmpenho_Valor,  "CRE") ;
            }
            if ( StringUtil.StrCmp(AV14HistoricoCodigo, "ALT") == 0 )
            {
               A1576SaldoContrato_Saldo = (decimal)(A1576SaldoContrato_Saldo-AV15AntNotaEmpenho_Valor);
               new prc_historicoconsumo(context ).execute(  AV8SaldoContrato_Codigo,  A74Contrato_Codigo,  AV12NotaEmpenho_Codigo,  0,  0,  AV15AntNotaEmpenho_Valor,  "DEB") ;
               A1576SaldoContrato_Saldo = (decimal)(A1576SaldoContrato_Saldo+AV9NotaEmpenho_Valor);
               new prc_historicoconsumo(context ).execute(  AV8SaldoContrato_Codigo,  A74Contrato_Codigo,  AV12NotaEmpenho_Codigo,  0,  0,  AV9NotaEmpenho_Valor,  "CRE") ;
            }
            /* Using cursor P00BH3 */
            pr_default.execute(1, new Object[] {A1576SaldoContrato_Saldo, A1561SaldoContrato_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("SaldoContrato") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_NotaEmpenhoSaldoContrato");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BH2_A1561SaldoContrato_Codigo = new int[1] ;
         P00BH2_A1576SaldoContrato_Saldo = new decimal[1] ;
         P00BH2_A74Contrato_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_notaempenhosaldocontrato__default(),
            new Object[][] {
                new Object[] {
               P00BH2_A1561SaldoContrato_Codigo, P00BH2_A1576SaldoContrato_Saldo, P00BH2_A74Contrato_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8SaldoContrato_Codigo ;
      private int AV12NotaEmpenho_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private int A74Contrato_Codigo ;
      private decimal AV15AntNotaEmpenho_Valor ;
      private decimal AV9NotaEmpenho_Valor ;
      private decimal A1576SaldoContrato_Saldo ;
      private String AV14HistoricoCodigo ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00BH2_A1561SaldoContrato_Codigo ;
      private decimal[] P00BH2_A1576SaldoContrato_Saldo ;
      private int[] P00BH2_A74Contrato_Codigo ;
   }

   public class prc_notaempenhosaldocontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BH2 ;
          prmP00BH2 = new Object[] {
          new Object[] {"@AV8SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BH3 ;
          prmP00BH3 = new Object[] {
          new Object[] {"@SaldoContrato_Saldo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BH2", "SELECT [SaldoContrato_Codigo], [SaldoContrato_Saldo], [Contrato_Codigo] FROM [SaldoContrato] WITH (UPDLOCK) WHERE [SaldoContrato_Codigo] = @AV8SaldoContrato_Codigo ORDER BY [SaldoContrato_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BH2,1,0,true,true )
             ,new CursorDef("P00BH3", "UPDATE [SaldoContrato] SET [SaldoContrato_Saldo]=@SaldoContrato_Saldo  WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BH3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
