/*
               File: WP_CheckListAnalise
        Description: Check List do An�lise
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:23:43.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_checklistanalise : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_checklistanalise( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_checklistanalise( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           short aP1_Etapa )
      {
         this.AV5ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10Etapa = aP1_Etapa;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavCtlcumpre = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_19 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_19_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_19_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               A1868ContagemResultadoChckLst_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1868ContagemResultadoChckLst_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1868ContagemResultadoChckLst_OSCod), 6, 0)));
               AV5ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5ContagemResultado_Codigo), "ZZZZZ9")));
               A758CheckList_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV14SDT_Item);
               A763CheckList_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A763CheckList_Descricao", A763CheckList_Descricao);
               A762ContagemResultadoChckLst_Cumpre = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A762ContagemResultadoChckLst_Cumpre", A762ContagemResultadoChckLst_Cumpre);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV12SDT_CheckList1);
               A1230CheckList_De = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n1230CheckList_De = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1230CheckList_De", StringUtil.LTrim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0)));
               A1151CheckList_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1151CheckList_Ativo", A1151CheckList_Ativo);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( A1868ContagemResultadoChckLst_OSCod, AV5ContagemResultado_Codigo, A758CheckList_Codigo, AV14SDT_Item, A763CheckList_Descricao, A762ContagemResultadoChckLst_Cumpre, AV12SDT_CheckList1, A1230CheckList_De, A1151CheckList_Ativo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV5ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5ContagemResultado_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV10Etapa = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Etapa", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Etapa), 4, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vETAPA", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10Etapa), "ZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAJ12( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTJ12( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216234324");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_checklistanalise.aspx") + "?" + UrlEncode("" +AV5ContagemResultado_Codigo) + "," + UrlEncode("" +AV10Etapa)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_checklist1", AV12SDT_CheckList1);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_checklist1", AV12SDT_CheckList1);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_19", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_19), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOCHCKLST_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1868ContagemResultadoChckLst_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CHECKLIST_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ITEM", AV14SDT_Item);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ITEM", AV14SDT_Item);
         }
         GxWebStd.gx_hidden_field( context, "CHECKLIST_DESCRICAO", A763CheckList_Descricao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOCHCKLST_CUMPRE", StringUtil.RTrim( A762ContagemResultadoChckLst_Cumpre));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_CHECKLIST1", AV12SDT_CheckList1);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_CHECKLIST1", AV12SDT_CheckList1);
         }
         GxWebStd.gx_hidden_field( context, "CHECKLIST_DE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1230CheckList_De), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CHECKLIST_ATIVO", A1151CheckList_Ativo);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV18WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV18WWPContext);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vREFRESH", AV29Refresh);
         GxWebStd.gx_hidden_field( context, "vNOME", StringUtil.RTrim( AV23Nome));
         GxWebStd.gx_boolean_hidden_field( context, "vOKFLAG", AV11OkFlag);
         GxWebStd.gx_boolean_hidden_field( context, "vNAOCUMPRE", AV26NaoCumpre);
         GxWebStd.gx_hidden_field( context, "vGXV2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39GXV2), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vETAPA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10Etapa), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vETAPA", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10Etapa), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vETAPA", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10Etapa), "ZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEJ12( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTJ12( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_checklistanalise.aspx") + "?" + UrlEncode("" +AV5ContagemResultado_Codigo) + "," + UrlEncode("" +AV10Etapa) ;
      }

      public override String GetPgmname( )
      {
         return "WP_CheckListAnalise" ;
      }

      public override String GetPgmdesc( )
      {
         return "Check List do An�lise" ;
      }

      protected void WBJ10( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "<p>") ;
            wb_table1_4_J12( true) ;
         }
         else
         {
            wb_table1_4_J12( false) ;
         }
         return  ;
      }

      protected void wb_table1_4_J12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</p>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_CheckListAnalise.htm");
         }
         wbLoad = true;
      }

      protected void STARTJ12( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Check List do An�lise", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPJ10( ) ;
      }

      protected void WSJ12( )
      {
         STARTJ12( ) ;
         EVTJ12( ) ;
      }

      protected void EVTJ12( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E11J12 */
                                    E11J12 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ENCAMINHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12J12 */
                              E12J12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_19_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
                              SubsflControlProps_192( ) ;
                              AV32GXV1 = nGXsfl_19_idx;
                              if ( ( AV12SDT_CheckList1.Count >= AV32GXV1 ) && ( AV32GXV1 > 0 ) )
                              {
                                 AV12SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13J12 */
                                    E13J12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14J12 */
                                    E14J12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15J12 */
                                    E15J12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEJ12( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAJ12( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            GXCCtl = "CTLCUMPRE_" + sGXsfl_19_idx;
            cmbavCtlcumpre.Name = GXCCtl;
            cmbavCtlcumpre.WebTags = "";
            cmbavCtlcumpre.addItem("", "---", 0);
            cmbavCtlcumpre.addItem("S", "Sim", 0);
            cmbavCtlcumpre.addItem("N", "N�o", 0);
            cmbavCtlcumpre.addItem("X", "N/A", 0);
            if ( cmbavCtlcumpre.ItemCount > 0 )
            {
               if ( ( AV32GXV1 > 0 ) && ( AV12SDT_CheckList1.Count >= AV32GXV1 ) && String.IsNullOrEmpty(StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Cumpre)) )
               {
                  ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Cumpre = cmbavCtlcumpre.getValidValue(((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Cumpre);
               }
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_192( ) ;
         while ( nGXsfl_19_idx <= nRC_GXsfl_19 )
         {
            sendrow_192( ) ;
            nGXsfl_19_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1));
            sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
            SubsflControlProps_192( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( int A1868ContagemResultadoChckLst_OSCod ,
                                        int AV5ContagemResultado_Codigo ,
                                        int A758CheckList_Codigo ,
                                        SdtSDT_CheckList_Item AV14SDT_Item ,
                                        String A763CheckList_Descricao ,
                                        String A762ContagemResultadoChckLst_Cumpre ,
                                        IGxCollection AV12SDT_CheckList1 ,
                                        short A1230CheckList_De ,
                                        bool A1151CheckList_Ativo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RFJ12( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJ12( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcodigo_Enabled), 5, 0)));
         edtavCtldescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldescricao_Enabled), 5, 0)));
      }

      protected void RFJ12( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 19;
         /* Execute user event: E14J12 */
         E14J12 ();
         nGXsfl_19_idx = 1;
         sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
         SubsflControlProps_192( ) ;
         nGXsfl_19_Refreshing = 1;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "Grid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_192( ) ;
            /* Execute user event: E15J12 */
            E15J12 ();
            wbEnd = 19;
            WBJ10( ) ;
         }
         nGXsfl_19_Refreshing = 0;
      }

      protected int subGrid1_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPJ10( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcodigo_Enabled), 5, 0)));
         edtavCtldescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtldescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtldescricao_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13J12 */
         E13J12 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_checklist1"), AV12SDT_CheckList1);
            ajax_req_read_hidden_sdt(cgiGet( "vSDT_CHECKLIST1"), AV12SDT_CheckList1);
            ajax_req_read_hidden_sdt(cgiGet( "vSDT_ITEM"), AV14SDT_Item);
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_19 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_19"), ",", "."));
            AV11OkFlag = StringUtil.StrToBool( cgiGet( "vOKFLAG"));
            AV26NaoCumpre = StringUtil.StrToBool( cgiGet( "vNAOCUMPRE"));
            AV39GXV2 = (int)(context.localUtil.CToN( cgiGet( "vGXV2"), ",", "."));
            AV29Refresh = StringUtil.StrToBool( cgiGet( "vREFRESH"));
            nRC_GXsfl_19 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_19"), ",", "."));
            nGXsfl_19_fel_idx = 0;
            while ( nGXsfl_19_fel_idx < nRC_GXsfl_19 )
            {
               nGXsfl_19_fel_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_19_fel_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_19_fel_idx+1));
               sGXsfl_19_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_192( ) ;
               AV32GXV1 = nGXsfl_19_fel_idx;
               if ( ( AV12SDT_CheckList1.Count >= AV32GXV1 ) && ( AV32GXV1 > 0 ) )
               {
                  AV12SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1));
               }
            }
            if ( nGXsfl_19_fel_idx == 0 )
            {
               nGXsfl_19_idx = 1;
               sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
               SubsflControlProps_192( ) ;
            }
            nGXsfl_19_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13J12 */
         E13J12 ();
         if (returnInSub) return;
      }

      protected void E13J12( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV18WWPContext) ;
         AV25Codigos.Add(AV5ContagemResultado_Codigo, 0);
         AV17WebSession.Set("Codigos", AV25Codigos.ToXml(false, true, "Collection", ""));
         /* Using cursor H00J13 */
         pr_default.execute(0, new Object[] {AV5ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00J13_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00J13_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = H00J13_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00J13_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = H00J13_A456ContagemResultado_Codigo[0];
            A490ContagemResultado_ContratadaCod = H00J13_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00J13_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = H00J13_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00J13_n601ContagemResultado_Servico[0];
            A771ContagemResultado_StatusDmnVnc = H00J13_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = H00J13_n771ContagemResultado_StatusDmnVnc[0];
            A457ContagemResultado_Demanda = H00J13_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00J13_n457ContagemResultado_Demanda[0];
            A682ContagemResultado_PFBFMUltima = H00J13_A682ContagemResultado_PFBFMUltima[0];
            n682ContagemResultado_PFBFMUltima = H00J13_n682ContagemResultado_PFBFMUltima[0];
            A601ContagemResultado_Servico = H00J13_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00J13_n601ContagemResultado_Servico[0];
            A771ContagemResultado_StatusDmnVnc = H00J13_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = H00J13_n771ContagemResultado_StatusDmnVnc[0];
            A682ContagemResultado_PFBFMUltima = H00J13_A682ContagemResultado_PFBFMUltima[0];
            n682ContagemResultado_PFBFMUltima = H00J13_n682ContagemResultado_PFBFMUltima[0];
            AV9Contratada_Codigo = A490ContagemResultado_ContratadaCod;
            AV15Servico_Codigo = A601ContagemResultado_Servico;
            AV24Unidades = A682ContagemResultado_PFBFMUltima;
            AV16StatusDmnPai = A771ContagemResultado_StatusDmnVnc;
            lblSubtitle_Caption = "AN�LISE DA OS "+A457ContagemResultado_Demanda;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblSubtitle_Internalname, "Caption", lblSubtitle_Caption);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         cmbavCtlcumpre.Visible = (((AV10Etapa>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlcumpre_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlcumpre.Visible), 5, 0)));
         bttBtnenter_Visible = (((AV10Etapa>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         bttBtntramitar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtntramitar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtntramitar_Visible), 5, 0)));
         bttBtntramitar_Caption = "Encaminhar para o "+AV18WWPContext.gxTpr_Areatrabalho_descricao;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtntramitar_Internalname, "Caption", bttBtntramitar_Caption);
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         bttBtnenter_Visible = (AV18WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         bttBtntramitar_Visible = (AV18WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtntramitar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtntramitar_Visible), 5, 0)));
      }

      protected void E14J12( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV37GXLvl31 = 0;
         /* Using cursor H00J14 */
         pr_default.execute(1, new Object[] {AV5ContagemResultado_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1868ContagemResultadoChckLst_OSCod = H00J14_A1868ContagemResultadoChckLst_OSCod[0];
            A758CheckList_Codigo = H00J14_A758CheckList_Codigo[0];
            A763CheckList_Descricao = H00J14_A763CheckList_Descricao[0];
            A762ContagemResultadoChckLst_Cumpre = H00J14_A762ContagemResultadoChckLst_Cumpre[0];
            A763CheckList_Descricao = H00J14_A763CheckList_Descricao[0];
            AV37GXLvl31 = 1;
            AV14SDT_Item.gxTpr_Codigo = A758CheckList_Codigo;
            AV14SDT_Item.gxTpr_Descricao = A763CheckList_Descricao;
            AV14SDT_Item.gxTpr_Cumpre = A762ContagemResultadoChckLst_Cumpre;
            AV12SDT_CheckList1.Add(AV14SDT_Item, 0);
            gx_BV19 = true;
            AV14SDT_Item = new SdtSDT_CheckList_Item(context);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV37GXLvl31 == 0 )
         {
            /* Using cursor H00J15 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1151CheckList_Ativo = H00J15_A1151CheckList_Ativo[0];
               A1230CheckList_De = H00J15_A1230CheckList_De[0];
               n1230CheckList_De = H00J15_n1230CheckList_De[0];
               A758CheckList_Codigo = H00J15_A758CheckList_Codigo[0];
               A763CheckList_Descricao = H00J15_A763CheckList_Descricao[0];
               AV14SDT_Item.gxTpr_Codigo = A758CheckList_Codigo;
               AV14SDT_Item.gxTpr_Descricao = A763CheckList_Descricao;
               AV12SDT_CheckList1.Add(AV14SDT_Item, 0);
               gx_BV19 = true;
               AV14SDT_Item = new SdtSDT_CheckList_Item(context);
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         /* Execute user subroutine: 'BTNENTERCAPTION' */
         S112 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14SDT_Item", AV14SDT_Item);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12SDT_CheckList1", AV12SDT_CheckList1);
      }

      private void E15J12( )
      {
         /* Load Routine */
         AV32GXV1 = 1;
         while ( AV32GXV1 <= AV12SDT_CheckList1.Count )
         {
            AV12SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1));
            cmbavCtlcumpre.removeItem("X");
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 19;
            }
            sendrow_192( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_19_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(19, Grid1Row);
            }
            AV32GXV1 = (short)(AV32GXV1+1);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E11J12 */
         E11J12 ();
         if (returnInSub) return;
      }

      protected void E11J12( )
      {
         AV32GXV1 = nGXsfl_19_idx;
         if ( AV12SDT_CheckList1.Count >= AV32GXV1 )
         {
            AV12SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1));
         }
         /* Enter Routine */
         AV29Refresh = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Refresh", AV29Refresh);
         /* Execute user subroutine: 'GRAVARCHECKLIST' */
         S122 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'DOFECHAR' */
         S132 ();
         if (returnInSub) return;
      }

      protected void S132( )
      {
         /* 'DOFECHAR' Routine */
         if ( AV29Refresh )
         {
            lblTbjava_Caption = "<script language=\"text/javascript\"> opener.location.reload(true); self.close();</script>";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         }
         else
         {
            lblTbjava_Caption = "<script language=\"text/javascript\"> self.close();</script>";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         }
      }

      protected void E12J12( )
      {
         AV32GXV1 = nGXsfl_19_idx;
         if ( AV12SDT_CheckList1.Count >= AV32GXV1 )
         {
            AV12SDT_CheckList1.CurrentItem = ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1));
         }
         /* 'Encaminhar' Routine */
         AV29Refresh = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Refresh", AV29Refresh);
         /* Execute user subroutine: 'GRAVARCHECKLIST' */
         S122 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'TRAMITACAO' */
         S142 ();
         if (returnInSub) return;
      }

      protected void S142( )
      {
         /* 'TRAMITACAO' Routine */
         context.PopUp(formatLink("wp_tramitacao.aspx") + "?" + UrlEncode("" +AV5ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(false)) + "," + UrlEncode(StringUtil.BoolToStr(true)) + "," + UrlEncode(StringUtil.RTrim(AV23Nome)), new Object[] {"AV23Nome"});
      }

      protected void S112( )
      {
         /* 'BTNENTERCAPTION' Routine */
         bttBtnenter_Caption = "Continuar an�lise";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         AV11OkFlag = true;
         AV26NaoCumpre = false;
         AV39GXV2 = 1;
         while ( AV39GXV2 <= AV12SDT_CheckList1.Count )
         {
            AV14SDT_Item = ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV39GXV2));
            if ( ( StringUtil.StrCmp(AV14SDT_Item.gxTpr_Cumpre, "N") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( AV14SDT_Item.gxTpr_Cumpre)) )
            {
               AV11OkFlag = false;
               AV26NaoCumpre = (bool)(AV26NaoCumpre||(StringUtil.StrCmp(AV14SDT_Item.gxTpr_Cumpre, "N")==0));
            }
            AV39GXV2 = (int)(AV39GXV2+1);
         }
         if ( AV11OkFlag )
         {
            bttBtnenter_Caption = "Finalizar an�lise";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         }
         bttBtntramitar_Visible = (AV26NaoCumpre ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtntramitar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtntramitar_Visible), 5, 0)));
      }

      protected void S122( )
      {
         /* 'GRAVARCHECKLIST' Routine */
         AV17WebSession.Set("CheckList", AV12SDT_CheckList1.ToXml(false, true, "SDT_CheckList", "GxEv3Up14_MeetrikaVs3"));
         new prc_updchecklistanalise(context ).execute(  AV5ContagemResultado_Codigo,  AV18WWPContext.gxTpr_Userid) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5ContagemResultado_Codigo), "ZZZZZ9")));
         context.CommitDataStores( "WP_CheckListAnalise");
      }

      protected void wb_table1_4_J12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\">") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblSubtitle_Internalname, lblSubtitle_Caption, "", "", lblSubtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_CheckListAnalise.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_13_J12( true) ;
         }
         else
         {
            wb_table2_13_J12( false) ;
         }
         return  ;
      }

      protected void wb_table2_13_J12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(19), 2, 0)+","+"null"+");", bttBtnenter_Caption, bttBtnenter_Jsonclick, 5, "Continuar an�lise", "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CheckListAnalise.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtntramitar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(19), 2, 0)+","+"null"+");", bttBtntramitar_Caption, bttBtntramitar_Jsonclick, 5, "Encaminhar", "", StyleString, ClassString, bttBtntramitar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ENCAMINHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CheckListAnalise.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(19), 2, 0)+","+"null"+");", "Fechar", bttBtnfechar_Jsonclick, 7, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e16j11_client"+"'", TempTags, "", 2, "HLP_WP_CheckListAnalise.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_4_J12e( true) ;
         }
         else
         {
            wb_table1_4_J12e( false) ;
         }
      }

      protected void wb_table2_13_J12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTbletapa1_Internalname, tblTbletapa1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle3_Internalname, "ITENS DE VERIFICA��O", "", "", lblViewtitle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_CheckListAnalise.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"19\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Descri��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(69), 4, 0))+"px"+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((cmbavCtlcumpre.Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Adequa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "Grid");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlcodigo_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtldescricao_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavCtlcumpre.Visible), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 19 )
         {
            wbEnd = 0;
            nRC_GXsfl_19 = (short)(nGXsfl_19_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV32GXV1 = nGXsfl_19_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_13_J12e( true) ;
         }
         else
         {
            wb_table2_13_J12e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV5ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5ContagemResultado_Codigo), "ZZZZZ9")));
         AV10Etapa = Convert.ToInt16(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Etapa", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Etapa), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vETAPA", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10Etapa), "ZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJ12( ) ;
         WSJ12( ) ;
         WEJ12( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216234364");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_checklistanalise.js", "?20206216234364");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_192( )
      {
         edtavCtlcodigo_Internalname = "CTLCODIGO_"+sGXsfl_19_idx;
         edtavCtldescricao_Internalname = "CTLDESCRICAO_"+sGXsfl_19_idx;
         cmbavCtlcumpre_Internalname = "CTLCUMPRE_"+sGXsfl_19_idx;
      }

      protected void SubsflControlProps_fel_192( )
      {
         edtavCtlcodigo_Internalname = "CTLCODIGO_"+sGXsfl_19_fel_idx;
         edtavCtldescricao_Internalname = "CTLDESCRICAO_"+sGXsfl_19_fel_idx;
         cmbavCtlcumpre_Internalname = "CTLCUMPRE_"+sGXsfl_19_fel_idx;
      }

      protected void sendrow_192( )
      {
         SubsflControlProps_192( ) ;
         WBJ10( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0x0);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_19_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_19_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlcodigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Codigo), 6, 0, ",", "")),((edtavCtlcodigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Codigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlcodigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlcodigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldescricao_Internalname,((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Descricao,((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtldescricao_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtldescricao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)19,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((cmbavCtlcumpre.Visible==0) ? "display:none;" : "")+"\">") ;
         }
         TempTags = " " + ((cmbavCtlcumpre.Enabled!=0)&&(cmbavCtlcumpre.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 22,'',false,'"+sGXsfl_19_idx+"',19)\"" : " ");
         if ( ( nGXsfl_19_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "CTLCUMPRE_" + sGXsfl_19_idx;
            cmbavCtlcumpre.Name = GXCCtl;
            cmbavCtlcumpre.WebTags = "";
            cmbavCtlcumpre.addItem("", "---", 0);
            cmbavCtlcumpre.addItem("S", "Sim", 0);
            cmbavCtlcumpre.addItem("N", "N�o", 0);
            cmbavCtlcumpre.addItem("X", "N/A", 0);
            if ( cmbavCtlcumpre.ItemCount > 0 )
            {
               if ( ( AV32GXV1 > 0 ) && ( AV12SDT_CheckList1.Count >= AV32GXV1 ) && String.IsNullOrEmpty(StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Cumpre)) )
               {
                  ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Cumpre = cmbavCtlcumpre.getValidValue(((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Cumpre);
               }
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavCtlcumpre,(String)cmbavCtlcumpre_Internalname,StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Cumpre),(short)1,(String)cmbavCtlcumpre_Jsonclick,(short)7,(String)"'"+""+"'"+",false,"+"'"+"e17j12_client"+"'",(String)"char",(String)"",cmbavCtlcumpre.Visible,(short)1,(short)0,(short)0,(short)69,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavCtlcumpre.Enabled!=0)&&(cmbavCtlcumpre.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavCtlcumpre.Enabled!=0)&&(cmbavCtlcumpre.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,22);\"" : " "),(String)"",(bool)true});
         cmbavCtlcumpre.CurrentValue = StringUtil.RTrim( ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Cumpre);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlcumpre_Internalname, "Values", (String)(cmbavCtlcumpre.ToJavascriptSource()));
         GxWebStd.gx_hidden_field( context, "gxhash_CTLCODIGO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sGXsfl_19_idx, context.localUtil.Format( (decimal)(((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_CTLDESCRICAO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sGXsfl_19_idx, ((SdtSDT_CheckList_Item)AV12SDT_CheckList1.Item(AV32GXV1)).gxTpr_Descricao));
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_19_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1));
         sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
         SubsflControlProps_192( ) ;
         /* End function sendrow_192 */
      }

      protected void init_default_properties( )
      {
         lblSubtitle_Internalname = "SUBTITLE";
         lblViewtitle3_Internalname = "VIEWTITLE3";
         edtavCtlcodigo_Internalname = "CTLCODIGO";
         edtavCtldescricao_Internalname = "CTLDESCRICAO";
         cmbavCtlcumpre_Internalname = "CTLCUMPRE";
         tblTbletapa1_Internalname = "TBLETAPA1";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtntramitar_Internalname = "BTNTRAMITAR";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblTable1_Internalname = "TABLE1";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavCtlcumpre_Jsonclick = "";
         cmbavCtlcumpre.Enabled = 1;
         edtavCtldescricao_Jsonclick = "";
         edtavCtlcodigo_Jsonclick = "";
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtavCtldescricao_Enabled = 0;
         edtavCtlcodigo_Enabled = 0;
         cmbavCtlcumpre.Visible = -1;
         subGrid1_Class = "Grid";
         bttBtntramitar_Visible = 1;
         bttBtnenter_Visible = 1;
         bttBtnenter_Caption = "Continuar an�lise";
         bttBtntramitar_Caption = "Encaminhar";
         cmbavCtlcumpre.Visible = -1;
         lblSubtitle_Caption = "AN�LISE";
         subGrid1_Backcolorstyle = 0;
         edtavCtldescricao_Enabled = -1;
         edtavCtlcodigo_Enabled = -1;
         lblTbjava_Caption = "Java";
         lblTbjava_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Check List do An�lise";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'A1868ContagemResultadoChckLst_OSCod',fld:'CONTAGEMRESULTADOCHCKLST_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14SDT_Item',fld:'vSDT_ITEM',pic:'',nv:null},{av:'A763CheckList_Descricao',fld:'CHECKLIST_DESCRICAO',pic:'',nv:''},{av:'A762ContagemResultadoChckLst_Cumpre',fld:'CONTAGEMRESULTADOCHCKLST_CUMPRE',pic:'',nv:''},{av:'AV12SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:19,pic:'',nv:null},{av:'A1230CheckList_De',fld:'CHECKLIST_DE',pic:'ZZZ9',nv:0},{av:'A1151CheckList_Ativo',fld:'CHECKLIST_ATIVO',pic:'',nv:false}],oparms:[{av:'AV14SDT_Item',fld:'vSDT_ITEM',pic:'',nv:null},{av:'AV12SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:19,pic:'',nv:null},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNTRAMITAR',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E11J12',iparms:[{av:'AV12SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:19,pic:'',nv:null},{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV29Refresh',fld:'vREFRESH',pic:'',nv:false}],oparms:[{av:'AV29Refresh',fld:'vREFRESH',pic:'',nv:false},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E16J11',iparms:[{av:'AV29Refresh',fld:'vREFRESH',pic:'',nv:false}],oparms:[{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         setEventMetadata("CTLCUMPRE.CLICK","{handler:'E17J12',iparms:[{av:'AV12SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:19,pic:'',nv:null}],oparms:[{ctrl:'BTNENTER',prop:'Caption'},{av:'AV14SDT_Item',fld:'vSDT_ITEM',pic:'',nv:null},{ctrl:'BTNTRAMITAR',prop:'Visible'}]}");
         setEventMetadata("'ENCAMINHAR'","{handler:'E12J12',iparms:[{av:'AV12SDT_CheckList1',fld:'vSDT_CHECKLIST1',grid:19,pic:'',nv:null},{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV23Nome',fld:'vNOME',pic:'@!',nv:''}],oparms:[{av:'AV29Refresh',fld:'vREFRESH',pic:'',nv:false},{av:'AV23Nome',fld:'vNOME',pic:'@!',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV14SDT_Item = new SdtSDT_CheckList_Item(context);
         A763CheckList_Descricao = "";
         A762ContagemResultadoChckLst_Cumpre = "";
         AV12SDT_CheckList1 = new GxObjectCollection( context, "SDT_CheckList.Item", "GxEv3Up14_MeetrikaVs3", "SdtSDT_CheckList_Item", "GeneXus.Programs");
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV18WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV23Nome = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTbjava_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         Grid1Container = new GXWebGrid( context);
         AV25Codigos = new GxSimpleCollection();
         AV17WebSession = context.GetSession();
         scmdbuf = "";
         H00J13_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00J13_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00J13_A602ContagemResultado_OSVinculada = new int[1] ;
         H00J13_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00J13_A456ContagemResultado_Codigo = new int[1] ;
         H00J13_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00J13_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00J13_A601ContagemResultado_Servico = new int[1] ;
         H00J13_n601ContagemResultado_Servico = new bool[] {false} ;
         H00J13_A771ContagemResultado_StatusDmnVnc = new String[] {""} ;
         H00J13_n771ContagemResultado_StatusDmnVnc = new bool[] {false} ;
         H00J13_A457ContagemResultado_Demanda = new String[] {""} ;
         H00J13_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00J13_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00J13_n682ContagemResultado_PFBFMUltima = new bool[] {false} ;
         A771ContagemResultado_StatusDmnVnc = "";
         A457ContagemResultado_Demanda = "";
         AV16StatusDmnPai = "";
         H00J14_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         H00J14_A1868ContagemResultadoChckLst_OSCod = new int[1] ;
         H00J14_A758CheckList_Codigo = new int[1] ;
         H00J14_A763CheckList_Descricao = new String[] {""} ;
         H00J14_A762ContagemResultadoChckLst_Cumpre = new String[] {""} ;
         H00J15_A1151CheckList_Ativo = new bool[] {false} ;
         H00J15_A1230CheckList_De = new short[1] ;
         H00J15_n1230CheckList_De = new bool[] {false} ;
         H00J15_A758CheckList_Codigo = new int[1] ;
         H00J15_A763CheckList_Descricao = new String[] {""} ;
         Grid1Row = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblSubtitle_Jsonclick = "";
         TempTags = "";
         bttBtnenter_Jsonclick = "";
         bttBtntramitar_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblViewtitle3_Jsonclick = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_checklistanalise__default(),
            new Object[][] {
                new Object[] {
               H00J13_A1553ContagemResultado_CntSrvCod, H00J13_n1553ContagemResultado_CntSrvCod, H00J13_A602ContagemResultado_OSVinculada, H00J13_n602ContagemResultado_OSVinculada, H00J13_A456ContagemResultado_Codigo, H00J13_A490ContagemResultado_ContratadaCod, H00J13_n490ContagemResultado_ContratadaCod, H00J13_A601ContagemResultado_Servico, H00J13_n601ContagemResultado_Servico, H00J13_A771ContagemResultado_StatusDmnVnc,
               H00J13_n771ContagemResultado_StatusDmnVnc, H00J13_A457ContagemResultado_Demanda, H00J13_n457ContagemResultado_Demanda, H00J13_A682ContagemResultado_PFBFMUltima, H00J13_n682ContagemResultado_PFBFMUltima
               }
               , new Object[] {
               H00J14_A761ContagemResultadoChckLst_Codigo, H00J14_A1868ContagemResultadoChckLst_OSCod, H00J14_A758CheckList_Codigo, H00J14_A763CheckList_Descricao, H00J14_A762ContagemResultadoChckLst_Cumpre
               }
               , new Object[] {
               H00J15_A1151CheckList_Ativo, H00J15_A1230CheckList_De, H00J15_n1230CheckList_De, H00J15_A758CheckList_Codigo, H00J15_A763CheckList_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlcodigo_Enabled = 0;
         edtavCtldescricao_Enabled = 0;
      }

      private short AV10Etapa ;
      private short wcpOAV10Etapa ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_19 ;
      private short nGXsfl_19_idx=1 ;
      private short A1230CheckList_De ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short AV32GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_19_Refreshing=0 ;
      private short subGrid1_Backcolorstyle ;
      private short nGXsfl_19_fel_idx=1 ;
      private short AV37GXLvl31 ;
      private short GRID1_nEOF ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int AV5ContagemResultado_Codigo ;
      private int wcpOAV5ContagemResultado_Codigo ;
      private int A1868ContagemResultadoChckLst_OSCod ;
      private int A758CheckList_Codigo ;
      private int AV39GXV2 ;
      private int lblTbjava_Visible ;
      private int subGrid1_Islastpage ;
      private int edtavCtlcodigo_Enabled ;
      private int edtavCtldescricao_Enabled ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int AV9Contratada_Codigo ;
      private int AV15Servico_Codigo ;
      private int bttBtnenter_Visible ;
      private int bttBtntramitar_Visible ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nFirstRecordOnPage ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal AV24Unidades ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_19_idx="0001" ;
      private String A762ContagemResultadoChckLst_Cumpre ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV23Nome ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String edtavCtlcodigo_Internalname ;
      private String edtavCtldescricao_Internalname ;
      private String sGXsfl_19_fel_idx="0001" ;
      private String scmdbuf ;
      private String A771ContagemResultado_StatusDmnVnc ;
      private String AV16StatusDmnPai ;
      private String lblSubtitle_Caption ;
      private String lblSubtitle_Internalname ;
      private String cmbavCtlcumpre_Internalname ;
      private String bttBtnenter_Internalname ;
      private String bttBtntramitar_Internalname ;
      private String bttBtntramitar_Caption ;
      private String bttBtnenter_Caption ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblSubtitle_Jsonclick ;
      private String TempTags ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtntramitar_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTbletapa1_Internalname ;
      private String lblViewtitle3_Internalname ;
      private String lblViewtitle3_Jsonclick ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String ROClassString ;
      private String edtavCtlcodigo_Jsonclick ;
      private String edtavCtldescricao_Jsonclick ;
      private String cmbavCtlcumpre_Jsonclick ;
      private bool entryPointCalled ;
      private bool n1230CheckList_De ;
      private bool A1151CheckList_Ativo ;
      private bool toggleJsOutput ;
      private bool AV29Refresh ;
      private bool AV11OkFlag ;
      private bool AV26NaoCumpre ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n771ContagemResultado_StatusDmnVnc ;
      private bool n457ContagemResultado_Demanda ;
      private bool n682ContagemResultado_PFBFMUltima ;
      private bool gx_refresh_fired ;
      private bool gx_BV19 ;
      private String A763CheckList_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavCtlcumpre ;
      private IDataStoreProvider pr_default ;
      private int[] H00J13_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00J13_n1553ContagemResultado_CntSrvCod ;
      private int[] H00J13_A602ContagemResultado_OSVinculada ;
      private bool[] H00J13_n602ContagemResultado_OSVinculada ;
      private int[] H00J13_A456ContagemResultado_Codigo ;
      private int[] H00J13_A490ContagemResultado_ContratadaCod ;
      private bool[] H00J13_n490ContagemResultado_ContratadaCod ;
      private int[] H00J13_A601ContagemResultado_Servico ;
      private bool[] H00J13_n601ContagemResultado_Servico ;
      private String[] H00J13_A771ContagemResultado_StatusDmnVnc ;
      private bool[] H00J13_n771ContagemResultado_StatusDmnVnc ;
      private String[] H00J13_A457ContagemResultado_Demanda ;
      private bool[] H00J13_n457ContagemResultado_Demanda ;
      private decimal[] H00J13_A682ContagemResultado_PFBFMUltima ;
      private bool[] H00J13_n682ContagemResultado_PFBFMUltima ;
      private short[] H00J14_A761ContagemResultadoChckLst_Codigo ;
      private int[] H00J14_A1868ContagemResultadoChckLst_OSCod ;
      private int[] H00J14_A758CheckList_Codigo ;
      private String[] H00J14_A763CheckList_Descricao ;
      private String[] H00J14_A762ContagemResultadoChckLst_Cumpre ;
      private bool[] H00J15_A1151CheckList_Ativo ;
      private short[] H00J15_A1230CheckList_De ;
      private bool[] H00J15_n1230CheckList_De ;
      private int[] H00J15_A758CheckList_Codigo ;
      private String[] H00J15_A763CheckList_Descricao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV17WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV25Codigos ;
      [ObjectCollection(ItemType=typeof( SdtSDT_CheckList_Item ))]
      private IGxCollection AV12SDT_CheckList1 ;
      private GXWebForm Form ;
      private SdtSDT_CheckList_Item AV14SDT_Item ;
      private wwpbaseobjects.SdtWWPContext AV18WWPContext ;
   }

   public class wp_checklistanalise__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00J13 ;
          prmH00J13 = new Object[] {
          new Object[] {"@AV5ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00J14 ;
          prmH00J14 = new Object[] {
          new Object[] {"@AV5ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00J15 ;
          prmH00J15 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00J13", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaCod], T2.[Servico_Codigo] AS ContagemResultado_Servico, T3.[ContagemResultado_StatusDmn] AS ContagemResultado_StatusDmnVnc, T1.[ContagemResultado_Demanda], COALESCE( T4.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV5ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00J13,1,0,false,true )
             ,new CursorDef("H00J14", "SELECT T1.[ContagemResultadoChckLst_Codigo], T1.[ContagemResultadoChckLst_OSCod], T1.[CheckList_Codigo], T2.[CheckList_Descricao], T1.[ContagemResultadoChckLst_Cumpre] FROM ([ContagemResultadoChckLst] T1 WITH (NOLOCK) INNER JOIN [CheckList] T2 WITH (NOLOCK) ON T2.[CheckList_Codigo] = T1.[CheckList_Codigo]) WHERE T1.[ContagemResultadoChckLst_OSCod] = @AV5ContagemResultado_Codigo ORDER BY T1.[ContagemResultadoChckLst_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00J14,100,0,false,false )
             ,new CursorDef("H00J15", "SELECT [CheckList_Ativo], [CheckList_De], [CheckList_Codigo], [CheckList_Descricao] FROM [CheckList] WITH (NOLOCK) WHERE ([CheckList_Ativo] = 1) AND ([CheckList_De] = 2) ORDER BY [CheckList_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00J15,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 1) ;
                return;
             case 2 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
