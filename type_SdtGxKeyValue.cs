/*
               File: type_SdtGxKeyValue
        Description: GxKeyValue
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:56.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GxKeyValue" )]
   [XmlType(TypeName =  "GxKeyValue" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtGxKeyValue : GxUserType
   {
      public SdtGxKeyValue( )
      {
         /* Constructor for serialization */
         gxTv_SdtGxKeyValue_Key = "";
         gxTv_SdtGxKeyValue_Value = "";
      }

      public SdtGxKeyValue( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtGxKeyValue deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtGxKeyValue)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtGxKeyValue obj ;
         obj = this;
         obj.gxTpr_Key = deserialized.gxTpr_Key;
         obj.gxTpr_Value = deserialized.gxTpr_Value;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Key") )
               {
                  gxTv_SdtGxKeyValue_Key = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Value") )
               {
                  gxTv_SdtGxKeyValue_Value = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "GxKeyValue";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Key", StringUtil.RTrim( gxTv_SdtGxKeyValue_Key));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Value", StringUtil.RTrim( gxTv_SdtGxKeyValue_Value));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Key", gxTv_SdtGxKeyValue_Key, false);
         AddObjectProperty("Value", gxTv_SdtGxKeyValue_Value, false);
         return  ;
      }

      [  SoapElement( ElementName = "Key" )]
      [  XmlElement( ElementName = "Key"   )]
      public String gxTpr_Key
      {
         get {
            return gxTv_SdtGxKeyValue_Key ;
         }

         set {
            gxTv_SdtGxKeyValue_Key = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Value" )]
      [  XmlElement( ElementName = "Value"   )]
      public String gxTpr_Value
      {
         get {
            return gxTv_SdtGxKeyValue_Value ;
         }

         set {
            gxTv_SdtGxKeyValue_Value = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtGxKeyValue_Key = "";
         gxTv_SdtGxKeyValue_Value = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtGxKeyValue_Key ;
      protected String gxTv_SdtGxKeyValue_Value ;
   }

   [DataContract(Name = @"GxKeyValue", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtGxKeyValue_RESTInterface : GxGenericCollectionItem<SdtGxKeyValue>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGxKeyValue_RESTInterface( ) : base()
      {
      }

      public SdtGxKeyValue_RESTInterface( SdtGxKeyValue psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Key" , Order = 0 )]
      public String gxTpr_Key
      {
         get {
            return sdt.gxTpr_Key ;
         }

         set {
            sdt.gxTpr_Key = (String)(value);
         }

      }

      [DataMember( Name = "Value" , Order = 1 )]
      public String gxTpr_Value
      {
         get {
            return sdt.gxTpr_Value ;
         }

         set {
            sdt.gxTpr_Value = (String)(value);
         }

      }

      public SdtGxKeyValue sdt
      {
         get {
            return (SdtGxKeyValue)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtGxKeyValue() ;
         }
      }

   }

}
