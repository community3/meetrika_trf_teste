/*
               File: PRC_DesvincularOS
        Description: Desvincular OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:6.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_desvincularos : GXProcedure
   {
      public prc_desvincularos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_desvincularos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo )
      {
         this.AV10ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo )
      {
         prc_desvincularos objprc_desvincularos;
         objprc_desvincularos = new prc_desvincularos();
         objprc_desvincularos.AV10ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_desvincularos.context.SetSubmitInitialConfig(context);
         objprc_desvincularos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_desvincularos);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_desvincularos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00BX2 */
         pr_default.execute(0, new Object[] {AV10ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P00BX2_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P00BX2_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00BX2_n602ContagemResultado_OSVinculada[0];
            A457ContagemResultado_Demanda = P00BX2_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00BX2_n457ContagemResultado_Demanda[0];
            A602ContagemResultado_OSVinculada = 0;
            n602ContagemResultado_OSVinculada = false;
            n602ContagemResultado_OSVinculada = true;
            A457ContagemResultado_Demanda = "";
            n457ContagemResultado_Demanda = false;
            n457ContagemResultado_Demanda = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00BX3 */
            pr_default.execute(1, new Object[] {n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00BX4 */
            pr_default.execute(2, new Object[] {n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Optimized DELETE. */
         /* Using cursor P00BX5 */
         pr_default.execute(3, new Object[] {AV10ContagemResultado_Codigo});
         pr_default.close(3);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoRequisito") ;
         /* End optimized DELETE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_DesvincularOS");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BX2_A456ContagemResultado_Codigo = new int[1] ;
         P00BX2_A602ContagemResultado_OSVinculada = new int[1] ;
         P00BX2_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00BX2_A457ContagemResultado_Demanda = new String[] {""} ;
         P00BX2_n457ContagemResultado_Demanda = new bool[] {false} ;
         A457ContagemResultado_Demanda = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_desvincularos__default(),
            new Object[][] {
                new Object[] {
               P00BX2_A456ContagemResultado_Codigo, P00BX2_A602ContagemResultado_OSVinculada, P00BX2_n602ContagemResultado_OSVinculada, P00BX2_A457ContagemResultado_Demanda, P00BX2_n457ContagemResultado_Demanda
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV10ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private String scmdbuf ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n457ContagemResultado_Demanda ;
      private String A457ContagemResultado_Demanda ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00BX2_A456ContagemResultado_Codigo ;
      private int[] P00BX2_A602ContagemResultado_OSVinculada ;
      private bool[] P00BX2_n602ContagemResultado_OSVinculada ;
      private String[] P00BX2_A457ContagemResultado_Demanda ;
      private bool[] P00BX2_n457ContagemResultado_Demanda ;
   }

   public class prc_desvincularos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BX2 ;
          prmP00BX2 = new Object[] {
          new Object[] {"@AV10ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BX3 ;
          prmP00BX3 = new Object[] {
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BX4 ;
          prmP00BX4 = new Object[] {
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BX5 ;
          prmP00BX5 = new Object[] {
          new Object[] {"@AV10ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BX2", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_OSVinculada], [ContagemResultado_Demanda] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV10ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BX2,1,0,true,true )
             ,new CursorDef("P00BX3", "UPDATE [ContagemResultado] SET [ContagemResultado_OSVinculada]=@ContagemResultado_OSVinculada, [ContagemResultado_Demanda]=@ContagemResultado_Demanda  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BX3)
             ,new CursorDef("P00BX4", "UPDATE [ContagemResultado] SET [ContagemResultado_OSVinculada]=@ContagemResultado_OSVinculada, [ContagemResultado_Demanda]=@ContagemResultado_Demanda  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BX4)
             ,new CursorDef("P00BX5", "DELETE FROM [ContagemResultadoRequisito]  WHERE ([ContagemResultadoRequisito_OSCod] = @AV10ContagemResultado_Codigo) AND (Not [ContagemResultadoRequisito_Owner] = 1)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BX5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
