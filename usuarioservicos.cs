/*
               File: UsuarioServicos
        Description: Usuario Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:30:8.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuarioservicos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A828UsuarioServicos_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A828UsuarioServicos_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A829UsuarioServicos_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A829UsuarioServicos_ServicoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynUsuarioServicos_ServicoCod.Name = "USUARIOSERVICOS_SERVICOCOD";
         dynUsuarioServicos_ServicoCod.WebTags = "";
         dynUsuarioServicos_ServicoCod.removeAllItems();
         /* Using cursor T002M6 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            dynUsuarioServicos_ServicoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T002M6_A155Servico_Codigo[0]), 6, 0)), T002M6_A608Servico_Nome[0], 0);
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( dynUsuarioServicos_ServicoCod.ItemCount > 0 )
         {
            A829UsuarioServicos_ServicoCod = (int)(NumberUtil.Val( dynUsuarioServicos_ServicoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
         }
         cmbUsuarioServicos_ServicoAtivo.Name = "USUARIOSERVICOS_SERVICOATIVO";
         cmbUsuarioServicos_ServicoAtivo.WebTags = "";
         cmbUsuarioServicos_ServicoAtivo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbUsuarioServicos_ServicoAtivo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbUsuarioServicos_ServicoAtivo.ItemCount > 0 )
         {
            A832UsuarioServicos_ServicoAtivo = StringUtil.StrToBool( cmbUsuarioServicos_ServicoAtivo.getValidValue(StringUtil.BoolToStr( A832UsuarioServicos_ServicoAtivo)));
            n832UsuarioServicos_ServicoAtivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A832UsuarioServicos_ServicoAtivo", A832UsuarioServicos_ServicoAtivo);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Usuario Servicos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public usuarioservicos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public usuarioservicos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynUsuarioServicos_ServicoCod = new GXCombobox();
         cmbUsuarioServicos_ServicoAtivo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynUsuarioServicos_ServicoCod.ItemCount > 0 )
         {
            A829UsuarioServicos_ServicoCod = (int)(NumberUtil.Val( dynUsuarioServicos_ServicoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
         }
         if ( cmbUsuarioServicos_ServicoAtivo.ItemCount > 0 )
         {
            A832UsuarioServicos_ServicoAtivo = StringUtil.StrToBool( cmbUsuarioServicos_ServicoAtivo.getValidValue(StringUtil.BoolToStr( A832UsuarioServicos_ServicoAtivo)));
            n832UsuarioServicos_ServicoAtivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A832UsuarioServicos_ServicoAtivo", A832UsuarioServicos_ServicoAtivo);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2M102( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2M102e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2M102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2M102( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2M102e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Usuario Servicos", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_UsuarioServicos.htm");
            wb_table3_28_2M102( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_2M102e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2M102e( true) ;
         }
         else
         {
            wb_table1_2_2M102e( false) ;
         }
      }

      protected void wb_table3_28_2M102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_2M102( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_2M102e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioServicos.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioServicos.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_2M102e( true) ;
         }
         else
         {
            wb_table3_28_2M102e( false) ;
         }
      }

      protected void wb_table4_34_2M102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuarioservicos_usuariocod_Internalname, "Usu�rio", "", "", lblTextblockusuarioservicos_usuariocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUsuarioServicos_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0, ",", "")), ((edtUsuarioServicos_UsuarioCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A828UsuarioServicos_UsuarioCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A828UsuarioServicos_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuarioServicos_UsuarioCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtUsuarioServicos_UsuarioCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuarioservicos_servicocod_Internalname, "Nome", "", "", lblTextblockusuarioservicos_servicocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynUsuarioServicos_ServicoCod, dynUsuarioServicos_ServicoCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)), 1, dynUsuarioServicos_ServicoCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynUsuarioServicos_ServicoCod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_UsuarioServicos.htm");
            dynUsuarioServicos_ServicoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUsuarioServicos_ServicoCod_Internalname, "Values", (String)(dynUsuarioServicos_ServicoCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuarioservicos_servicosigla_Internalname, "Sigla", "", "", lblTextblockusuarioservicos_servicosigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuarioServicos_ServicoSigla_Internalname, StringUtil.RTrim( A831UsuarioServicos_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( A831UsuarioServicos_ServicoSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuarioServicos_ServicoSigla_Jsonclick, 0, "Attribute", "", "", "", 1, edtUsuarioServicos_ServicoSigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuarioservicos_servicoativo_Internalname, "Ativo", "", "", lblTextblockusuarioservicos_servicoativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbUsuarioServicos_ServicoAtivo, cmbUsuarioServicos_ServicoAtivo_Internalname, StringUtil.BoolToStr( A832UsuarioServicos_ServicoAtivo), 1, cmbUsuarioServicos_ServicoAtivo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbUsuarioServicos_ServicoAtivo.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", "", true, "HLP_UsuarioServicos.htm");
            cmbUsuarioServicos_ServicoAtivo.CurrentValue = StringUtil.BoolToStr( A832UsuarioServicos_ServicoAtivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbUsuarioServicos_ServicoAtivo_Internalname, "Values", (String)(cmbUsuarioServicos_ServicoAtivo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuarioservicos_tmpestanl_Internalname, "de an�lise", "", "", lblTextblockusuarioservicos_tmpestanl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUsuarioServicos_TmpEstAnl_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1517UsuarioServicos_TmpEstAnl), 8, 0, ",", "")), ((edtUsuarioServicos_TmpEstAnl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1517UsuarioServicos_TmpEstAnl), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1517UsuarioServicos_TmpEstAnl), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuarioServicos_TmpEstAnl_Jsonclick, 0, "Attribute", "", "", "", 1, edtUsuarioServicos_TmpEstAnl_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuarioservicos_tmpestexc_Internalname, "de execu��o", "", "", lblTextblockusuarioservicos_tmpestexc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUsuarioServicos_TmpEstExc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1513UsuarioServicos_TmpEstExc), 8, 0, ",", "")), ((edtUsuarioServicos_TmpEstExc_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1513UsuarioServicos_TmpEstExc), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1513UsuarioServicos_TmpEstExc), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuarioServicos_TmpEstExc_Jsonclick, 0, "Attribute", "", "", "", 1, edtUsuarioServicos_TmpEstExc_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuarioservicos_tmpestcrr_Internalname, "de corre��o", "", "", lblTextblockusuarioservicos_tmpestcrr_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUsuarioServicos_TmpEstCrr_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1514UsuarioServicos_TmpEstCrr), 8, 0, ",", "")), ((edtUsuarioServicos_TmpEstCrr_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1514UsuarioServicos_TmpEstCrr), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1514UsuarioServicos_TmpEstCrr), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuarioServicos_TmpEstCrr_Jsonclick, 0, "Attribute", "", "", "", 1, edtUsuarioServicos_TmpEstCrr_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_UsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_2M102e( true) ;
         }
         else
         {
            wb_table4_34_2M102e( false) ;
         }
      }

      protected void wb_table2_5_2M102( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_UsuarioServicos.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2M102e( true) ;
         }
         else
         {
            wb_table2_5_2M102e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtUsuarioServicos_UsuarioCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtUsuarioServicos_UsuarioCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "USUARIOSERVICOS_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A828UsuarioServicos_UsuarioCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
               }
               else
               {
                  A828UsuarioServicos_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtUsuarioServicos_UsuarioCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
               }
               dynUsuarioServicos_ServicoCod.CurrentValue = cgiGet( dynUsuarioServicos_ServicoCod_Internalname);
               A829UsuarioServicos_ServicoCod = (int)(NumberUtil.Val( cgiGet( dynUsuarioServicos_ServicoCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
               A831UsuarioServicos_ServicoSigla = StringUtil.Upper( cgiGet( edtUsuarioServicos_ServicoSigla_Internalname));
               n831UsuarioServicos_ServicoSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A831UsuarioServicos_ServicoSigla", A831UsuarioServicos_ServicoSigla);
               cmbUsuarioServicos_ServicoAtivo.CurrentValue = cgiGet( cmbUsuarioServicos_ServicoAtivo_Internalname);
               A832UsuarioServicos_ServicoAtivo = StringUtil.StrToBool( cgiGet( cmbUsuarioServicos_ServicoAtivo_Internalname));
               n832UsuarioServicos_ServicoAtivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A832UsuarioServicos_ServicoAtivo", A832UsuarioServicos_ServicoAtivo);
               if ( ( ( context.localUtil.CToN( cgiGet( edtUsuarioServicos_TmpEstAnl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtUsuarioServicos_TmpEstAnl_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "USUARIOSERVICOS_TMPESTANL");
                  AnyError = 1;
                  GX_FocusControl = edtUsuarioServicos_TmpEstAnl_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1517UsuarioServicos_TmpEstAnl = 0;
                  n1517UsuarioServicos_TmpEstAnl = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1517UsuarioServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1517UsuarioServicos_TmpEstAnl), 8, 0)));
               }
               else
               {
                  A1517UsuarioServicos_TmpEstAnl = (int)(context.localUtil.CToN( cgiGet( edtUsuarioServicos_TmpEstAnl_Internalname), ",", "."));
                  n1517UsuarioServicos_TmpEstAnl = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1517UsuarioServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1517UsuarioServicos_TmpEstAnl), 8, 0)));
               }
               n1517UsuarioServicos_TmpEstAnl = ((0==A1517UsuarioServicos_TmpEstAnl) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtUsuarioServicos_TmpEstExc_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtUsuarioServicos_TmpEstExc_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "USUARIOSERVICOS_TMPESTEXC");
                  AnyError = 1;
                  GX_FocusControl = edtUsuarioServicos_TmpEstExc_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1513UsuarioServicos_TmpEstExc = 0;
                  n1513UsuarioServicos_TmpEstExc = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1513UsuarioServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1513UsuarioServicos_TmpEstExc), 8, 0)));
               }
               else
               {
                  A1513UsuarioServicos_TmpEstExc = (int)(context.localUtil.CToN( cgiGet( edtUsuarioServicos_TmpEstExc_Internalname), ",", "."));
                  n1513UsuarioServicos_TmpEstExc = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1513UsuarioServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1513UsuarioServicos_TmpEstExc), 8, 0)));
               }
               n1513UsuarioServicos_TmpEstExc = ((0==A1513UsuarioServicos_TmpEstExc) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtUsuarioServicos_TmpEstCrr_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtUsuarioServicos_TmpEstCrr_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "USUARIOSERVICOS_TMPESTCRR");
                  AnyError = 1;
                  GX_FocusControl = edtUsuarioServicos_TmpEstCrr_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1514UsuarioServicos_TmpEstCrr = 0;
                  n1514UsuarioServicos_TmpEstCrr = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1514UsuarioServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1514UsuarioServicos_TmpEstCrr), 8, 0)));
               }
               else
               {
                  A1514UsuarioServicos_TmpEstCrr = (int)(context.localUtil.CToN( cgiGet( edtUsuarioServicos_TmpEstCrr_Internalname), ",", "."));
                  n1514UsuarioServicos_TmpEstCrr = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1514UsuarioServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1514UsuarioServicos_TmpEstCrr), 8, 0)));
               }
               n1514UsuarioServicos_TmpEstCrr = ((0==A1514UsuarioServicos_TmpEstCrr) ? true : false);
               /* Read saved values. */
               Z828UsuarioServicos_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "Z828UsuarioServicos_UsuarioCod"), ",", "."));
               Z829UsuarioServicos_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "Z829UsuarioServicos_ServicoCod"), ",", "."));
               Z1517UsuarioServicos_TmpEstAnl = (int)(context.localUtil.CToN( cgiGet( "Z1517UsuarioServicos_TmpEstAnl"), ",", "."));
               n1517UsuarioServicos_TmpEstAnl = ((0==A1517UsuarioServicos_TmpEstAnl) ? true : false);
               Z1513UsuarioServicos_TmpEstExc = (int)(context.localUtil.CToN( cgiGet( "Z1513UsuarioServicos_TmpEstExc"), ",", "."));
               n1513UsuarioServicos_TmpEstExc = ((0==A1513UsuarioServicos_TmpEstExc) ? true : false);
               Z1514UsuarioServicos_TmpEstCrr = (int)(context.localUtil.CToN( cgiGet( "Z1514UsuarioServicos_TmpEstCrr"), ",", "."));
               n1514UsuarioServicos_TmpEstCrr = ((0==A1514UsuarioServicos_TmpEstCrr) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A828UsuarioServicos_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
                  A829UsuarioServicos_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2M102( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes2M102( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption2M0( )
      {
      }

      protected void ZM2M102( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1517UsuarioServicos_TmpEstAnl = T002M3_A1517UsuarioServicos_TmpEstAnl[0];
               Z1513UsuarioServicos_TmpEstExc = T002M3_A1513UsuarioServicos_TmpEstExc[0];
               Z1514UsuarioServicos_TmpEstCrr = T002M3_A1514UsuarioServicos_TmpEstCrr[0];
            }
            else
            {
               Z1517UsuarioServicos_TmpEstAnl = A1517UsuarioServicos_TmpEstAnl;
               Z1513UsuarioServicos_TmpEstExc = A1513UsuarioServicos_TmpEstExc;
               Z1514UsuarioServicos_TmpEstCrr = A1514UsuarioServicos_TmpEstCrr;
            }
         }
         if ( GX_JID == -1 )
         {
            Z1517UsuarioServicos_TmpEstAnl = A1517UsuarioServicos_TmpEstAnl;
            Z1513UsuarioServicos_TmpEstExc = A1513UsuarioServicos_TmpEstExc;
            Z1514UsuarioServicos_TmpEstCrr = A1514UsuarioServicos_TmpEstCrr;
            Z828UsuarioServicos_UsuarioCod = A828UsuarioServicos_UsuarioCod;
            Z829UsuarioServicos_ServicoCod = A829UsuarioServicos_ServicoCod;
            Z831UsuarioServicos_ServicoSigla = A831UsuarioServicos_ServicoSigla;
            Z832UsuarioServicos_ServicoAtivo = A832UsuarioServicos_ServicoAtivo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load2M102( )
      {
         /* Using cursor T002M7 */
         pr_default.execute(5, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound102 = 1;
            A831UsuarioServicos_ServicoSigla = T002M7_A831UsuarioServicos_ServicoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A831UsuarioServicos_ServicoSigla", A831UsuarioServicos_ServicoSigla);
            n831UsuarioServicos_ServicoSigla = T002M7_n831UsuarioServicos_ServicoSigla[0];
            A832UsuarioServicos_ServicoAtivo = T002M7_A832UsuarioServicos_ServicoAtivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A832UsuarioServicos_ServicoAtivo", A832UsuarioServicos_ServicoAtivo);
            n832UsuarioServicos_ServicoAtivo = T002M7_n832UsuarioServicos_ServicoAtivo[0];
            A1517UsuarioServicos_TmpEstAnl = T002M7_A1517UsuarioServicos_TmpEstAnl[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1517UsuarioServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1517UsuarioServicos_TmpEstAnl), 8, 0)));
            n1517UsuarioServicos_TmpEstAnl = T002M7_n1517UsuarioServicos_TmpEstAnl[0];
            A1513UsuarioServicos_TmpEstExc = T002M7_A1513UsuarioServicos_TmpEstExc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1513UsuarioServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1513UsuarioServicos_TmpEstExc), 8, 0)));
            n1513UsuarioServicos_TmpEstExc = T002M7_n1513UsuarioServicos_TmpEstExc[0];
            A1514UsuarioServicos_TmpEstCrr = T002M7_A1514UsuarioServicos_TmpEstCrr[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1514UsuarioServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1514UsuarioServicos_TmpEstCrr), 8, 0)));
            n1514UsuarioServicos_TmpEstCrr = T002M7_n1514UsuarioServicos_TmpEstCrr[0];
            ZM2M102( -1) ;
         }
         pr_default.close(5);
         OnLoadActions2M102( ) ;
      }

      protected void OnLoadActions2M102( )
      {
      }

      protected void CheckExtendedTable2M102( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T002M4 */
         pr_default.execute(2, new Object[] {A828UsuarioServicos_UsuarioCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T002M5 */
         pr_default.execute(3, new Object[] {A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario Servicos_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_SERVICOCOD");
            AnyError = 1;
            GX_FocusControl = dynUsuarioServicos_ServicoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A831UsuarioServicos_ServicoSigla = T002M5_A831UsuarioServicos_ServicoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A831UsuarioServicos_ServicoSigla", A831UsuarioServicos_ServicoSigla);
         n831UsuarioServicos_ServicoSigla = T002M5_n831UsuarioServicos_ServicoSigla[0];
         A832UsuarioServicos_ServicoAtivo = T002M5_A832UsuarioServicos_ServicoAtivo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A832UsuarioServicos_ServicoAtivo", A832UsuarioServicos_ServicoAtivo);
         n832UsuarioServicos_ServicoAtivo = T002M5_n832UsuarioServicos_ServicoAtivo[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors2M102( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A828UsuarioServicos_UsuarioCod )
      {
         /* Using cursor T002M8 */
         pr_default.execute(6, new Object[] {A828UsuarioServicos_UsuarioCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_3( int A829UsuarioServicos_ServicoCod )
      {
         /* Using cursor T002M9 */
         pr_default.execute(7, new Object[] {A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario Servicos_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_SERVICOCOD");
            AnyError = 1;
            GX_FocusControl = dynUsuarioServicos_ServicoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A831UsuarioServicos_ServicoSigla = T002M9_A831UsuarioServicos_ServicoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A831UsuarioServicos_ServicoSigla", A831UsuarioServicos_ServicoSigla);
         n831UsuarioServicos_ServicoSigla = T002M9_n831UsuarioServicos_ServicoSigla[0];
         A832UsuarioServicos_ServicoAtivo = T002M9_A832UsuarioServicos_ServicoAtivo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A832UsuarioServicos_ServicoAtivo", A832UsuarioServicos_ServicoAtivo);
         n832UsuarioServicos_ServicoAtivo = T002M9_n832UsuarioServicos_ServicoAtivo[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A831UsuarioServicos_ServicoSigla))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A832UsuarioServicos_ServicoAtivo))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void GetKey2M102( )
      {
         /* Using cursor T002M10 */
         pr_default.execute(8, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound102 = 1;
         }
         else
         {
            RcdFound102 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002M3 */
         pr_default.execute(1, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2M102( 1) ;
            RcdFound102 = 1;
            A1517UsuarioServicos_TmpEstAnl = T002M3_A1517UsuarioServicos_TmpEstAnl[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1517UsuarioServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1517UsuarioServicos_TmpEstAnl), 8, 0)));
            n1517UsuarioServicos_TmpEstAnl = T002M3_n1517UsuarioServicos_TmpEstAnl[0];
            A1513UsuarioServicos_TmpEstExc = T002M3_A1513UsuarioServicos_TmpEstExc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1513UsuarioServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1513UsuarioServicos_TmpEstExc), 8, 0)));
            n1513UsuarioServicos_TmpEstExc = T002M3_n1513UsuarioServicos_TmpEstExc[0];
            A1514UsuarioServicos_TmpEstCrr = T002M3_A1514UsuarioServicos_TmpEstCrr[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1514UsuarioServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1514UsuarioServicos_TmpEstCrr), 8, 0)));
            n1514UsuarioServicos_TmpEstCrr = T002M3_n1514UsuarioServicos_TmpEstCrr[0];
            A828UsuarioServicos_UsuarioCod = T002M3_A828UsuarioServicos_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
            A829UsuarioServicos_ServicoCod = T002M3_A829UsuarioServicos_ServicoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
            Z828UsuarioServicos_UsuarioCod = A828UsuarioServicos_UsuarioCod;
            Z829UsuarioServicos_ServicoCod = A829UsuarioServicos_ServicoCod;
            sMode102 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load2M102( ) ;
            if ( AnyError == 1 )
            {
               RcdFound102 = 0;
               InitializeNonKey2M102( ) ;
            }
            Gx_mode = sMode102;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound102 = 0;
            InitializeNonKey2M102( ) ;
            sMode102 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode102;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2M102( ) ;
         if ( RcdFound102 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound102 = 0;
         /* Using cursor T002M11 */
         pr_default.execute(9, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T002M11_A828UsuarioServicos_UsuarioCod[0] < A828UsuarioServicos_UsuarioCod ) || ( T002M11_A828UsuarioServicos_UsuarioCod[0] == A828UsuarioServicos_UsuarioCod ) && ( T002M11_A829UsuarioServicos_ServicoCod[0] < A829UsuarioServicos_ServicoCod ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T002M11_A828UsuarioServicos_UsuarioCod[0] > A828UsuarioServicos_UsuarioCod ) || ( T002M11_A828UsuarioServicos_UsuarioCod[0] == A828UsuarioServicos_UsuarioCod ) && ( T002M11_A829UsuarioServicos_ServicoCod[0] > A829UsuarioServicos_ServicoCod ) ) )
            {
               A828UsuarioServicos_UsuarioCod = T002M11_A828UsuarioServicos_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
               A829UsuarioServicos_ServicoCod = T002M11_A829UsuarioServicos_ServicoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
               RcdFound102 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void move_previous( )
      {
         RcdFound102 = 0;
         /* Using cursor T002M12 */
         pr_default.execute(10, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T002M12_A828UsuarioServicos_UsuarioCod[0] > A828UsuarioServicos_UsuarioCod ) || ( T002M12_A828UsuarioServicos_UsuarioCod[0] == A828UsuarioServicos_UsuarioCod ) && ( T002M12_A829UsuarioServicos_ServicoCod[0] > A829UsuarioServicos_ServicoCod ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T002M12_A828UsuarioServicos_UsuarioCod[0] < A828UsuarioServicos_UsuarioCod ) || ( T002M12_A828UsuarioServicos_UsuarioCod[0] == A828UsuarioServicos_UsuarioCod ) && ( T002M12_A829UsuarioServicos_ServicoCod[0] < A829UsuarioServicos_ServicoCod ) ) )
            {
               A828UsuarioServicos_UsuarioCod = T002M12_A828UsuarioServicos_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
               A829UsuarioServicos_ServicoCod = T002M12_A829UsuarioServicos_ServicoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
               RcdFound102 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2M102( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2M102( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound102 == 1 )
            {
               if ( ( A828UsuarioServicos_UsuarioCod != Z828UsuarioServicos_UsuarioCod ) || ( A829UsuarioServicos_ServicoCod != Z829UsuarioServicos_ServicoCod ) )
               {
                  A828UsuarioServicos_UsuarioCod = Z828UsuarioServicos_UsuarioCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
                  A829UsuarioServicos_ServicoCod = Z829UsuarioServicos_ServicoCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "USUARIOSERVICOS_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update2M102( ) ;
                  GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A828UsuarioServicos_UsuarioCod != Z828UsuarioServicos_UsuarioCod ) || ( A829UsuarioServicos_ServicoCod != Z829UsuarioServicos_ServicoCod ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2M102( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "USUARIOSERVICOS_USUARIOCOD");
                     AnyError = 1;
                     GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2M102( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A828UsuarioServicos_UsuarioCod != Z828UsuarioServicos_UsuarioCod ) || ( A829UsuarioServicos_ServicoCod != Z829UsuarioServicos_ServicoCod ) )
         {
            A828UsuarioServicos_UsuarioCod = Z828UsuarioServicos_UsuarioCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
            A829UsuarioServicos_ServicoCod = Z829UsuarioServicos_ServicoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "USUARIOSERVICOS_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound102 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "USUARIOSERVICOS_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtUsuarioServicos_TmpEstAnl_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2M102( ) ;
         if ( RcdFound102 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtUsuarioServicos_TmpEstAnl_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2M102( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound102 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtUsuarioServicos_TmpEstAnl_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound102 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtUsuarioServicos_TmpEstAnl_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2M102( ) ;
         if ( RcdFound102 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound102 != 0 )
            {
               ScanNext2M102( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtUsuarioServicos_TmpEstAnl_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2M102( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency2M102( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002M2 */
            pr_default.execute(0, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"UsuarioServicos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1517UsuarioServicos_TmpEstAnl != T002M2_A1517UsuarioServicos_TmpEstAnl[0] ) || ( Z1513UsuarioServicos_TmpEstExc != T002M2_A1513UsuarioServicos_TmpEstExc[0] ) || ( Z1514UsuarioServicos_TmpEstCrr != T002M2_A1514UsuarioServicos_TmpEstCrr[0] ) )
            {
               if ( Z1517UsuarioServicos_TmpEstAnl != T002M2_A1517UsuarioServicos_TmpEstAnl[0] )
               {
                  GXUtil.WriteLog("usuarioservicos:[seudo value changed for attri]"+"UsuarioServicos_TmpEstAnl");
                  GXUtil.WriteLogRaw("Old: ",Z1517UsuarioServicos_TmpEstAnl);
                  GXUtil.WriteLogRaw("Current: ",T002M2_A1517UsuarioServicos_TmpEstAnl[0]);
               }
               if ( Z1513UsuarioServicos_TmpEstExc != T002M2_A1513UsuarioServicos_TmpEstExc[0] )
               {
                  GXUtil.WriteLog("usuarioservicos:[seudo value changed for attri]"+"UsuarioServicos_TmpEstExc");
                  GXUtil.WriteLogRaw("Old: ",Z1513UsuarioServicos_TmpEstExc);
                  GXUtil.WriteLogRaw("Current: ",T002M2_A1513UsuarioServicos_TmpEstExc[0]);
               }
               if ( Z1514UsuarioServicos_TmpEstCrr != T002M2_A1514UsuarioServicos_TmpEstCrr[0] )
               {
                  GXUtil.WriteLog("usuarioservicos:[seudo value changed for attri]"+"UsuarioServicos_TmpEstCrr");
                  GXUtil.WriteLogRaw("Old: ",Z1514UsuarioServicos_TmpEstCrr);
                  GXUtil.WriteLogRaw("Current: ",T002M2_A1514UsuarioServicos_TmpEstCrr[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"UsuarioServicos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2M102( )
      {
         BeforeValidate2M102( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2M102( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2M102( 0) ;
            CheckOptimisticConcurrency2M102( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2M102( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2M102( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002M13 */
                     pr_default.execute(11, new Object[] {n1517UsuarioServicos_TmpEstAnl, A1517UsuarioServicos_TmpEstAnl, n1513UsuarioServicos_TmpEstExc, A1513UsuarioServicos_TmpEstExc, n1514UsuarioServicos_TmpEstCrr, A1514UsuarioServicos_TmpEstCrr, A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
                     if ( (pr_default.getStatus(11) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2M0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2M102( ) ;
            }
            EndLevel2M102( ) ;
         }
         CloseExtendedTableCursors2M102( ) ;
      }

      protected void Update2M102( )
      {
         BeforeValidate2M102( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2M102( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2M102( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2M102( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2M102( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002M14 */
                     pr_default.execute(12, new Object[] {n1517UsuarioServicos_TmpEstAnl, A1517UsuarioServicos_TmpEstAnl, n1513UsuarioServicos_TmpEstExc, A1513UsuarioServicos_TmpEstExc, n1514UsuarioServicos_TmpEstCrr, A1514UsuarioServicos_TmpEstCrr, A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
                     if ( (pr_default.getStatus(12) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"UsuarioServicos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2M102( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption2M0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2M102( ) ;
         }
         CloseExtendedTableCursors2M102( ) ;
      }

      protected void DeferredUpdate2M102( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate2M102( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2M102( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2M102( ) ;
            AfterConfirm2M102( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2M102( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002M15 */
                  pr_default.execute(13, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound102 == 0 )
                        {
                           InitAll2M102( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption2M0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode102 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel2M102( ) ;
         Gx_mode = sMode102;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls2M102( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002M16 */
            pr_default.execute(14, new Object[] {A829UsuarioServicos_ServicoCod});
            A831UsuarioServicos_ServicoSigla = T002M16_A831UsuarioServicos_ServicoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A831UsuarioServicos_ServicoSigla", A831UsuarioServicos_ServicoSigla);
            n831UsuarioServicos_ServicoSigla = T002M16_n831UsuarioServicos_ServicoSigla[0];
            A832UsuarioServicos_ServicoAtivo = T002M16_A832UsuarioServicos_ServicoAtivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A832UsuarioServicos_ServicoAtivo", A832UsuarioServicos_ServicoAtivo);
            n832UsuarioServicos_ServicoAtivo = T002M16_n832UsuarioServicos_ServicoAtivo[0];
            pr_default.close(14);
         }
      }

      protected void EndLevel2M102( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2M102( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(14);
            context.CommitDataStores( "UsuarioServicos");
            if ( AnyError == 0 )
            {
               ConfirmValues2M0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(14);
            context.RollbackDataStores( "UsuarioServicos");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2M102( )
      {
         /* Using cursor T002M17 */
         pr_default.execute(15);
         RcdFound102 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound102 = 1;
            A828UsuarioServicos_UsuarioCod = T002M17_A828UsuarioServicos_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
            A829UsuarioServicos_ServicoCod = T002M17_A829UsuarioServicos_ServicoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2M102( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound102 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound102 = 1;
            A828UsuarioServicos_UsuarioCod = T002M17_A828UsuarioServicos_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
            A829UsuarioServicos_ServicoCod = T002M17_A829UsuarioServicos_ServicoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
         }
      }

      protected void ScanEnd2M102( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm2M102( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2M102( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2M102( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2M102( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2M102( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2M102( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2M102( )
      {
         edtUsuarioServicos_UsuarioCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioServicos_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioServicos_UsuarioCod_Enabled), 5, 0)));
         dynUsuarioServicos_ServicoCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynUsuarioServicos_ServicoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynUsuarioServicos_ServicoCod.Enabled), 5, 0)));
         edtUsuarioServicos_ServicoSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioServicos_ServicoSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioServicos_ServicoSigla_Enabled), 5, 0)));
         cmbUsuarioServicos_ServicoAtivo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbUsuarioServicos_ServicoAtivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbUsuarioServicos_ServicoAtivo.Enabled), 5, 0)));
         edtUsuarioServicos_TmpEstAnl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioServicos_TmpEstAnl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioServicos_TmpEstAnl_Enabled), 5, 0)));
         edtUsuarioServicos_TmpEstExc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioServicos_TmpEstExc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioServicos_TmpEstExc_Enabled), 5, 0)));
         edtUsuarioServicos_TmpEstCrr_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioServicos_TmpEstCrr_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioServicos_TmpEstCrr_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2M0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299301042");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("usuarioservicos.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z828UsuarioServicos_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z829UsuarioServicos_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1517UsuarioServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1517UsuarioServicos_TmpEstAnl), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1513UsuarioServicos_TmpEstExc", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1513UsuarioServicos_TmpEstExc), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1514UsuarioServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1514UsuarioServicos_TmpEstCrr), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("usuarioservicos.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "UsuarioServicos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Usuario Servicos" ;
      }

      protected void InitializeNonKey2M102( )
      {
         A831UsuarioServicos_ServicoSigla = "";
         n831UsuarioServicos_ServicoSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A831UsuarioServicos_ServicoSigla", A831UsuarioServicos_ServicoSigla);
         A832UsuarioServicos_ServicoAtivo = false;
         n832UsuarioServicos_ServicoAtivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A832UsuarioServicos_ServicoAtivo", A832UsuarioServicos_ServicoAtivo);
         A1517UsuarioServicos_TmpEstAnl = 0;
         n1517UsuarioServicos_TmpEstAnl = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1517UsuarioServicos_TmpEstAnl", StringUtil.LTrim( StringUtil.Str( (decimal)(A1517UsuarioServicos_TmpEstAnl), 8, 0)));
         n1517UsuarioServicos_TmpEstAnl = ((0==A1517UsuarioServicos_TmpEstAnl) ? true : false);
         A1513UsuarioServicos_TmpEstExc = 0;
         n1513UsuarioServicos_TmpEstExc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1513UsuarioServicos_TmpEstExc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1513UsuarioServicos_TmpEstExc), 8, 0)));
         n1513UsuarioServicos_TmpEstExc = ((0==A1513UsuarioServicos_TmpEstExc) ? true : false);
         A1514UsuarioServicos_TmpEstCrr = 0;
         n1514UsuarioServicos_TmpEstCrr = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1514UsuarioServicos_TmpEstCrr", StringUtil.LTrim( StringUtil.Str( (decimal)(A1514UsuarioServicos_TmpEstCrr), 8, 0)));
         n1514UsuarioServicos_TmpEstCrr = ((0==A1514UsuarioServicos_TmpEstCrr) ? true : false);
         Z1517UsuarioServicos_TmpEstAnl = 0;
         Z1513UsuarioServicos_TmpEstExc = 0;
         Z1514UsuarioServicos_TmpEstCrr = 0;
      }

      protected void InitAll2M102( )
      {
         A828UsuarioServicos_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A828UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0)));
         A829UsuarioServicos_ServicoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A829UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0)));
         InitializeNonKey2M102( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299301048");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("usuarioservicos.js", "?20205299301048");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockusuarioservicos_usuariocod_Internalname = "TEXTBLOCKUSUARIOSERVICOS_USUARIOCOD";
         edtUsuarioServicos_UsuarioCod_Internalname = "USUARIOSERVICOS_USUARIOCOD";
         lblTextblockusuarioservicos_servicocod_Internalname = "TEXTBLOCKUSUARIOSERVICOS_SERVICOCOD";
         dynUsuarioServicos_ServicoCod_Internalname = "USUARIOSERVICOS_SERVICOCOD";
         lblTextblockusuarioservicos_servicosigla_Internalname = "TEXTBLOCKUSUARIOSERVICOS_SERVICOSIGLA";
         edtUsuarioServicos_ServicoSigla_Internalname = "USUARIOSERVICOS_SERVICOSIGLA";
         lblTextblockusuarioservicos_servicoativo_Internalname = "TEXTBLOCKUSUARIOSERVICOS_SERVICOATIVO";
         cmbUsuarioServicos_ServicoAtivo_Internalname = "USUARIOSERVICOS_SERVICOATIVO";
         lblTextblockusuarioservicos_tmpestanl_Internalname = "TEXTBLOCKUSUARIOSERVICOS_TMPESTANL";
         edtUsuarioServicos_TmpEstAnl_Internalname = "USUARIOSERVICOS_TMPESTANL";
         lblTextblockusuarioservicos_tmpestexc_Internalname = "TEXTBLOCKUSUARIOSERVICOS_TMPESTEXC";
         edtUsuarioServicos_TmpEstExc_Internalname = "USUARIOSERVICOS_TMPESTEXC";
         lblTextblockusuarioservicos_tmpestcrr_Internalname = "TEXTBLOCKUSUARIOSERVICOS_TMPESTCRR";
         edtUsuarioServicos_TmpEstCrr_Internalname = "USUARIOSERVICOS_TMPESTCRR";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Usuario Servicos";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtUsuarioServicos_TmpEstCrr_Jsonclick = "";
         edtUsuarioServicos_TmpEstCrr_Enabled = 1;
         edtUsuarioServicos_TmpEstExc_Jsonclick = "";
         edtUsuarioServicos_TmpEstExc_Enabled = 1;
         edtUsuarioServicos_TmpEstAnl_Jsonclick = "";
         edtUsuarioServicos_TmpEstAnl_Enabled = 1;
         cmbUsuarioServicos_ServicoAtivo_Jsonclick = "";
         cmbUsuarioServicos_ServicoAtivo.Enabled = 0;
         edtUsuarioServicos_ServicoSigla_Jsonclick = "";
         edtUsuarioServicos_ServicoSigla_Enabled = 0;
         dynUsuarioServicos_ServicoCod_Jsonclick = "";
         dynUsuarioServicos_ServicoCod.Enabled = 1;
         edtUsuarioServicos_UsuarioCod_Jsonclick = "";
         edtUsuarioServicos_UsuarioCod_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAUSUARIOSERVICOS_SERVICOCOD2M1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAUSUARIOSERVICOS_SERVICOCOD_data2M1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAUSUARIOSERVICOS_SERVICOCOD_html2M1( )
      {
         int gxdynajaxvalue ;
         GXDLAUSUARIOSERVICOS_SERVICOCOD_data2M1( ) ;
         gxdynajaxindex = 1;
         dynUsuarioServicos_ServicoCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynUsuarioServicos_ServicoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAUSUARIOSERVICOS_SERVICOCOD_data2M1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T002M18 */
         pr_default.execute(16);
         while ( (pr_default.getStatus(16) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002M18_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002M18_A608Servico_Nome[0]));
            pr_default.readNext(16);
         }
         pr_default.close(16);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T002M19 */
         pr_default.execute(17, new Object[] {A828UsuarioServicos_UsuarioCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(17);
         /* Using cursor T002M16 */
         pr_default.execute(14, new Object[] {A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario Servicos_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_SERVICOCOD");
            AnyError = 1;
            GX_FocusControl = dynUsuarioServicos_ServicoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A831UsuarioServicos_ServicoSigla = T002M16_A831UsuarioServicos_ServicoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A831UsuarioServicos_ServicoSigla", A831UsuarioServicos_ServicoSigla);
         n831UsuarioServicos_ServicoSigla = T002M16_n831UsuarioServicos_ServicoSigla[0];
         A832UsuarioServicos_ServicoAtivo = T002M16_A832UsuarioServicos_ServicoAtivo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A832UsuarioServicos_ServicoAtivo", A832UsuarioServicos_ServicoAtivo);
         n832UsuarioServicos_ServicoAtivo = T002M16_n832UsuarioServicos_ServicoAtivo[0];
         pr_default.close(14);
         GX_FocusControl = edtUsuarioServicos_TmpEstAnl_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Usuarioservicos_usuariocod( int GX_Parm1 )
      {
         A828UsuarioServicos_UsuarioCod = GX_Parm1;
         /* Using cursor T002M19 */
         pr_default.execute(17, new Object[] {A828UsuarioServicos_UsuarioCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtUsuarioServicos_UsuarioCod_Internalname;
         }
         pr_default.close(17);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Usuarioservicos_servicocod( int GX_Parm1 ,
                                                    GXCombobox dynGX_Parm2 ,
                                                    int GX_Parm3 ,
                                                    int GX_Parm4 ,
                                                    int GX_Parm5 ,
                                                    String GX_Parm6 ,
                                                    GXCombobox cmbGX_Parm7 )
      {
         A828UsuarioServicos_UsuarioCod = GX_Parm1;
         dynUsuarioServicos_ServicoCod = dynGX_Parm2;
         A829UsuarioServicos_ServicoCod = (int)(NumberUtil.Val( dynUsuarioServicos_ServicoCod.CurrentValue, "."));
         A1517UsuarioServicos_TmpEstAnl = GX_Parm3;
         n1517UsuarioServicos_TmpEstAnl = false;
         A1513UsuarioServicos_TmpEstExc = GX_Parm4;
         n1513UsuarioServicos_TmpEstExc = false;
         A1514UsuarioServicos_TmpEstCrr = GX_Parm5;
         n1514UsuarioServicos_TmpEstCrr = false;
         A831UsuarioServicos_ServicoSigla = GX_Parm6;
         n831UsuarioServicos_ServicoSigla = false;
         cmbUsuarioServicos_ServicoAtivo = cmbGX_Parm7;
         A832UsuarioServicos_ServicoAtivo = StringUtil.StrToBool( cmbUsuarioServicos_ServicoAtivo.CurrentValue);
         n832UsuarioServicos_ServicoAtivo = false;
         cmbUsuarioServicos_ServicoAtivo.CurrentValue = StringUtil.BoolToStr( A832UsuarioServicos_ServicoAtivo);
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T002M16 */
         pr_default.execute(14, new Object[] {A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario Servicos_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_SERVICOCOD");
            AnyError = 1;
            GX_FocusControl = dynUsuarioServicos_ServicoCod_Internalname;
         }
         A831UsuarioServicos_ServicoSigla = T002M16_A831UsuarioServicos_ServicoSigla[0];
         n831UsuarioServicos_ServicoSigla = T002M16_n831UsuarioServicos_ServicoSigla[0];
         A832UsuarioServicos_ServicoAtivo = T002M16_A832UsuarioServicos_ServicoAtivo[0];
         cmbUsuarioServicos_ServicoAtivo.CurrentValue = StringUtil.BoolToStr( A832UsuarioServicos_ServicoAtivo);
         n832UsuarioServicos_ServicoAtivo = T002M16_n832UsuarioServicos_ServicoAtivo[0];
         pr_default.close(14);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A831UsuarioServicos_ServicoSigla = "";
            n831UsuarioServicos_ServicoSigla = false;
            A832UsuarioServicos_ServicoAtivo = false;
            n832UsuarioServicos_ServicoAtivo = false;
            cmbUsuarioServicos_ServicoAtivo.CurrentValue = StringUtil.BoolToStr( A832UsuarioServicos_ServicoAtivo);
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1517UsuarioServicos_TmpEstAnl), 8, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1513UsuarioServicos_TmpEstExc), 8, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1514UsuarioServicos_TmpEstCrr), 8, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A831UsuarioServicos_ServicoSigla));
         cmbUsuarioServicos_ServicoAtivo.CurrentValue = StringUtil.BoolToStr( A832UsuarioServicos_ServicoAtivo);
         isValidOutput.Add(cmbUsuarioServicos_ServicoAtivo);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z828UsuarioServicos_UsuarioCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z829UsuarioServicos_ServicoCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1517UsuarioServicos_TmpEstAnl), 8, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1513UsuarioServicos_TmpEstExc), 8, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1514UsuarioServicos_TmpEstCrr), 8, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z831UsuarioServicos_ServicoSigla));
         isValidOutput.Add(Z832UsuarioServicos_ServicoAtivo);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(17);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         T002M6_A155Servico_Codigo = new int[1] ;
         T002M6_A608Servico_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockusuarioservicos_usuariocod_Jsonclick = "";
         lblTextblockusuarioservicos_servicocod_Jsonclick = "";
         lblTextblockusuarioservicos_servicosigla_Jsonclick = "";
         A831UsuarioServicos_ServicoSigla = "";
         lblTextblockusuarioservicos_servicoativo_Jsonclick = "";
         lblTextblockusuarioservicos_tmpestanl_Jsonclick = "";
         lblTextblockusuarioservicos_tmpestexc_Jsonclick = "";
         lblTextblockusuarioservicos_tmpestcrr_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z831UsuarioServicos_ServicoSigla = "";
         T002M7_A831UsuarioServicos_ServicoSigla = new String[] {""} ;
         T002M7_n831UsuarioServicos_ServicoSigla = new bool[] {false} ;
         T002M7_A832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         T002M7_n832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         T002M7_A1517UsuarioServicos_TmpEstAnl = new int[1] ;
         T002M7_n1517UsuarioServicos_TmpEstAnl = new bool[] {false} ;
         T002M7_A1513UsuarioServicos_TmpEstExc = new int[1] ;
         T002M7_n1513UsuarioServicos_TmpEstExc = new bool[] {false} ;
         T002M7_A1514UsuarioServicos_TmpEstCrr = new int[1] ;
         T002M7_n1514UsuarioServicos_TmpEstCrr = new bool[] {false} ;
         T002M7_A828UsuarioServicos_UsuarioCod = new int[1] ;
         T002M7_A829UsuarioServicos_ServicoCod = new int[1] ;
         T002M4_A828UsuarioServicos_UsuarioCod = new int[1] ;
         T002M5_A831UsuarioServicos_ServicoSigla = new String[] {""} ;
         T002M5_n831UsuarioServicos_ServicoSigla = new bool[] {false} ;
         T002M5_A832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         T002M5_n832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         T002M8_A828UsuarioServicos_UsuarioCod = new int[1] ;
         T002M9_A831UsuarioServicos_ServicoSigla = new String[] {""} ;
         T002M9_n831UsuarioServicos_ServicoSigla = new bool[] {false} ;
         T002M9_A832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         T002M9_n832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         T002M10_A828UsuarioServicos_UsuarioCod = new int[1] ;
         T002M10_A829UsuarioServicos_ServicoCod = new int[1] ;
         T002M3_A1517UsuarioServicos_TmpEstAnl = new int[1] ;
         T002M3_n1517UsuarioServicos_TmpEstAnl = new bool[] {false} ;
         T002M3_A1513UsuarioServicos_TmpEstExc = new int[1] ;
         T002M3_n1513UsuarioServicos_TmpEstExc = new bool[] {false} ;
         T002M3_A1514UsuarioServicos_TmpEstCrr = new int[1] ;
         T002M3_n1514UsuarioServicos_TmpEstCrr = new bool[] {false} ;
         T002M3_A828UsuarioServicos_UsuarioCod = new int[1] ;
         T002M3_A829UsuarioServicos_ServicoCod = new int[1] ;
         sMode102 = "";
         T002M11_A828UsuarioServicos_UsuarioCod = new int[1] ;
         T002M11_A829UsuarioServicos_ServicoCod = new int[1] ;
         T002M12_A828UsuarioServicos_UsuarioCod = new int[1] ;
         T002M12_A829UsuarioServicos_ServicoCod = new int[1] ;
         T002M2_A1517UsuarioServicos_TmpEstAnl = new int[1] ;
         T002M2_n1517UsuarioServicos_TmpEstAnl = new bool[] {false} ;
         T002M2_A1513UsuarioServicos_TmpEstExc = new int[1] ;
         T002M2_n1513UsuarioServicos_TmpEstExc = new bool[] {false} ;
         T002M2_A1514UsuarioServicos_TmpEstCrr = new int[1] ;
         T002M2_n1514UsuarioServicos_TmpEstCrr = new bool[] {false} ;
         T002M2_A828UsuarioServicos_UsuarioCod = new int[1] ;
         T002M2_A829UsuarioServicos_ServicoCod = new int[1] ;
         T002M16_A831UsuarioServicos_ServicoSigla = new String[] {""} ;
         T002M16_n831UsuarioServicos_ServicoSigla = new bool[] {false} ;
         T002M16_A832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         T002M16_n832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         T002M17_A828UsuarioServicos_UsuarioCod = new int[1] ;
         T002M17_A829UsuarioServicos_ServicoCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002M18_A155Servico_Codigo = new int[1] ;
         T002M18_A608Servico_Nome = new String[] {""} ;
         T002M19_A828UsuarioServicos_UsuarioCod = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuarioservicos__default(),
            new Object[][] {
                new Object[] {
               T002M2_A1517UsuarioServicos_TmpEstAnl, T002M2_n1517UsuarioServicos_TmpEstAnl, T002M2_A1513UsuarioServicos_TmpEstExc, T002M2_n1513UsuarioServicos_TmpEstExc, T002M2_A1514UsuarioServicos_TmpEstCrr, T002M2_n1514UsuarioServicos_TmpEstCrr, T002M2_A828UsuarioServicos_UsuarioCod, T002M2_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               T002M3_A1517UsuarioServicos_TmpEstAnl, T002M3_n1517UsuarioServicos_TmpEstAnl, T002M3_A1513UsuarioServicos_TmpEstExc, T002M3_n1513UsuarioServicos_TmpEstExc, T002M3_A1514UsuarioServicos_TmpEstCrr, T002M3_n1514UsuarioServicos_TmpEstCrr, T002M3_A828UsuarioServicos_UsuarioCod, T002M3_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               T002M4_A828UsuarioServicos_UsuarioCod
               }
               , new Object[] {
               T002M5_A831UsuarioServicos_ServicoSigla, T002M5_n831UsuarioServicos_ServicoSigla, T002M5_A832UsuarioServicos_ServicoAtivo, T002M5_n832UsuarioServicos_ServicoAtivo
               }
               , new Object[] {
               T002M6_A155Servico_Codigo, T002M6_A608Servico_Nome
               }
               , new Object[] {
               T002M7_A831UsuarioServicos_ServicoSigla, T002M7_n831UsuarioServicos_ServicoSigla, T002M7_A832UsuarioServicos_ServicoAtivo, T002M7_n832UsuarioServicos_ServicoAtivo, T002M7_A1517UsuarioServicos_TmpEstAnl, T002M7_n1517UsuarioServicos_TmpEstAnl, T002M7_A1513UsuarioServicos_TmpEstExc, T002M7_n1513UsuarioServicos_TmpEstExc, T002M7_A1514UsuarioServicos_TmpEstCrr, T002M7_n1514UsuarioServicos_TmpEstCrr,
               T002M7_A828UsuarioServicos_UsuarioCod, T002M7_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               T002M8_A828UsuarioServicos_UsuarioCod
               }
               , new Object[] {
               T002M9_A831UsuarioServicos_ServicoSigla, T002M9_n831UsuarioServicos_ServicoSigla, T002M9_A832UsuarioServicos_ServicoAtivo, T002M9_n832UsuarioServicos_ServicoAtivo
               }
               , new Object[] {
               T002M10_A828UsuarioServicos_UsuarioCod, T002M10_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               T002M11_A828UsuarioServicos_UsuarioCod, T002M11_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               T002M12_A828UsuarioServicos_UsuarioCod, T002M12_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002M16_A831UsuarioServicos_ServicoSigla, T002M16_n831UsuarioServicos_ServicoSigla, T002M16_A832UsuarioServicos_ServicoAtivo, T002M16_n832UsuarioServicos_ServicoAtivo
               }
               , new Object[] {
               T002M17_A828UsuarioServicos_UsuarioCod, T002M17_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               T002M18_A155Servico_Codigo, T002M18_A608Servico_Nome
               }
               , new Object[] {
               T002M19_A828UsuarioServicos_UsuarioCod
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound102 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z828UsuarioServicos_UsuarioCod ;
      private int Z829UsuarioServicos_ServicoCod ;
      private int Z1517UsuarioServicos_TmpEstAnl ;
      private int Z1513UsuarioServicos_TmpEstExc ;
      private int Z1514UsuarioServicos_TmpEstCrr ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int A829UsuarioServicos_ServicoCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtUsuarioServicos_UsuarioCod_Enabled ;
      private int edtUsuarioServicos_ServicoSigla_Enabled ;
      private int A1517UsuarioServicos_TmpEstAnl ;
      private int edtUsuarioServicos_TmpEstAnl_Enabled ;
      private int A1513UsuarioServicos_TmpEstExc ;
      private int edtUsuarioServicos_TmpEstExc_Enabled ;
      private int A1514UsuarioServicos_TmpEstCrr ;
      private int edtUsuarioServicos_TmpEstCrr_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtUsuarioServicos_UsuarioCod_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockusuarioservicos_usuariocod_Internalname ;
      private String lblTextblockusuarioservicos_usuariocod_Jsonclick ;
      private String edtUsuarioServicos_UsuarioCod_Jsonclick ;
      private String lblTextblockusuarioservicos_servicocod_Internalname ;
      private String lblTextblockusuarioservicos_servicocod_Jsonclick ;
      private String dynUsuarioServicos_ServicoCod_Internalname ;
      private String dynUsuarioServicos_ServicoCod_Jsonclick ;
      private String lblTextblockusuarioservicos_servicosigla_Internalname ;
      private String lblTextblockusuarioservicos_servicosigla_Jsonclick ;
      private String edtUsuarioServicos_ServicoSigla_Internalname ;
      private String A831UsuarioServicos_ServicoSigla ;
      private String edtUsuarioServicos_ServicoSigla_Jsonclick ;
      private String lblTextblockusuarioservicos_servicoativo_Internalname ;
      private String lblTextblockusuarioservicos_servicoativo_Jsonclick ;
      private String cmbUsuarioServicos_ServicoAtivo_Internalname ;
      private String cmbUsuarioServicos_ServicoAtivo_Jsonclick ;
      private String lblTextblockusuarioservicos_tmpestanl_Internalname ;
      private String lblTextblockusuarioservicos_tmpestanl_Jsonclick ;
      private String edtUsuarioServicos_TmpEstAnl_Internalname ;
      private String edtUsuarioServicos_TmpEstAnl_Jsonclick ;
      private String lblTextblockusuarioservicos_tmpestexc_Internalname ;
      private String lblTextblockusuarioservicos_tmpestexc_Jsonclick ;
      private String edtUsuarioServicos_TmpEstExc_Internalname ;
      private String edtUsuarioServicos_TmpEstExc_Jsonclick ;
      private String lblTextblockusuarioservicos_tmpestcrr_Internalname ;
      private String lblTextblockusuarioservicos_tmpestcrr_Jsonclick ;
      private String edtUsuarioServicos_TmpEstCrr_Internalname ;
      private String edtUsuarioServicos_TmpEstCrr_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z831UsuarioServicos_ServicoSigla ;
      private String sMode102 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A832UsuarioServicos_ServicoAtivo ;
      private bool n832UsuarioServicos_ServicoAtivo ;
      private bool wbErr ;
      private bool n831UsuarioServicos_ServicoSigla ;
      private bool n1517UsuarioServicos_TmpEstAnl ;
      private bool n1513UsuarioServicos_TmpEstExc ;
      private bool n1514UsuarioServicos_TmpEstCrr ;
      private bool Z832UsuarioServicos_ServicoAtivo ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T002M6_A155Servico_Codigo ;
      private String[] T002M6_A608Servico_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynUsuarioServicos_ServicoCod ;
      private GXCombobox cmbUsuarioServicos_ServicoAtivo ;
      private String[] T002M7_A831UsuarioServicos_ServicoSigla ;
      private bool[] T002M7_n831UsuarioServicos_ServicoSigla ;
      private bool[] T002M7_A832UsuarioServicos_ServicoAtivo ;
      private bool[] T002M7_n832UsuarioServicos_ServicoAtivo ;
      private int[] T002M7_A1517UsuarioServicos_TmpEstAnl ;
      private bool[] T002M7_n1517UsuarioServicos_TmpEstAnl ;
      private int[] T002M7_A1513UsuarioServicos_TmpEstExc ;
      private bool[] T002M7_n1513UsuarioServicos_TmpEstExc ;
      private int[] T002M7_A1514UsuarioServicos_TmpEstCrr ;
      private bool[] T002M7_n1514UsuarioServicos_TmpEstCrr ;
      private int[] T002M7_A828UsuarioServicos_UsuarioCod ;
      private int[] T002M7_A829UsuarioServicos_ServicoCod ;
      private int[] T002M4_A828UsuarioServicos_UsuarioCod ;
      private String[] T002M5_A831UsuarioServicos_ServicoSigla ;
      private bool[] T002M5_n831UsuarioServicos_ServicoSigla ;
      private bool[] T002M5_A832UsuarioServicos_ServicoAtivo ;
      private bool[] T002M5_n832UsuarioServicos_ServicoAtivo ;
      private int[] T002M8_A828UsuarioServicos_UsuarioCod ;
      private String[] T002M9_A831UsuarioServicos_ServicoSigla ;
      private bool[] T002M9_n831UsuarioServicos_ServicoSigla ;
      private bool[] T002M9_A832UsuarioServicos_ServicoAtivo ;
      private bool[] T002M9_n832UsuarioServicos_ServicoAtivo ;
      private int[] T002M10_A828UsuarioServicos_UsuarioCod ;
      private int[] T002M10_A829UsuarioServicos_ServicoCod ;
      private int[] T002M3_A1517UsuarioServicos_TmpEstAnl ;
      private bool[] T002M3_n1517UsuarioServicos_TmpEstAnl ;
      private int[] T002M3_A1513UsuarioServicos_TmpEstExc ;
      private bool[] T002M3_n1513UsuarioServicos_TmpEstExc ;
      private int[] T002M3_A1514UsuarioServicos_TmpEstCrr ;
      private bool[] T002M3_n1514UsuarioServicos_TmpEstCrr ;
      private int[] T002M3_A828UsuarioServicos_UsuarioCod ;
      private int[] T002M3_A829UsuarioServicos_ServicoCod ;
      private int[] T002M11_A828UsuarioServicos_UsuarioCod ;
      private int[] T002M11_A829UsuarioServicos_ServicoCod ;
      private int[] T002M12_A828UsuarioServicos_UsuarioCod ;
      private int[] T002M12_A829UsuarioServicos_ServicoCod ;
      private int[] T002M2_A1517UsuarioServicos_TmpEstAnl ;
      private bool[] T002M2_n1517UsuarioServicos_TmpEstAnl ;
      private int[] T002M2_A1513UsuarioServicos_TmpEstExc ;
      private bool[] T002M2_n1513UsuarioServicos_TmpEstExc ;
      private int[] T002M2_A1514UsuarioServicos_TmpEstCrr ;
      private bool[] T002M2_n1514UsuarioServicos_TmpEstCrr ;
      private int[] T002M2_A828UsuarioServicos_UsuarioCod ;
      private int[] T002M2_A829UsuarioServicos_ServicoCod ;
      private String[] T002M16_A831UsuarioServicos_ServicoSigla ;
      private bool[] T002M16_n831UsuarioServicos_ServicoSigla ;
      private bool[] T002M16_A832UsuarioServicos_ServicoAtivo ;
      private bool[] T002M16_n832UsuarioServicos_ServicoAtivo ;
      private int[] T002M17_A828UsuarioServicos_UsuarioCod ;
      private int[] T002M17_A829UsuarioServicos_ServicoCod ;
      private int[] T002M18_A155Servico_Codigo ;
      private String[] T002M18_A608Servico_Nome ;
      private int[] T002M19_A828UsuarioServicos_UsuarioCod ;
      private GXWebForm Form ;
   }

   public class usuarioservicos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002M6 ;
          prmT002M6 = new Object[] {
          } ;
          Object[] prmT002M7 ;
          prmT002M7 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M4 ;
          prmT002M4 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M5 ;
          prmT002M5 = new Object[] {
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M8 ;
          prmT002M8 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M9 ;
          prmT002M9 = new Object[] {
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M10 ;
          prmT002M10 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M3 ;
          prmT002M3 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M11 ;
          prmT002M11 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M12 ;
          prmT002M12 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M2 ;
          prmT002M2 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M13 ;
          prmT002M13 = new Object[] {
          new Object[] {"@UsuarioServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M14 ;
          prmT002M14 = new Object[] {
          new Object[] {"@UsuarioServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M15 ;
          prmT002M15 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M17 ;
          prmT002M17 = new Object[] {
          } ;
          Object[] prmT002M18 ;
          prmT002M18 = new Object[] {
          } ;
          Object[] prmT002M19 ;
          prmT002M19 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002M16 ;
          prmT002M16 = new Object[] {
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002M2", "SELECT [UsuarioServicos_TmpEstAnl], [UsuarioServicos_TmpEstExc], [UsuarioServicos_TmpEstCrr], [UsuarioServicos_UsuarioCod] AS UsuarioServicos_UsuarioCod, [UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod FROM [UsuarioServicos] WITH (UPDLOCK) WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002M2,1,0,true,false )
             ,new CursorDef("T002M3", "SELECT [UsuarioServicos_TmpEstAnl], [UsuarioServicos_TmpEstExc], [UsuarioServicos_TmpEstCrr], [UsuarioServicos_UsuarioCod] AS UsuarioServicos_UsuarioCod, [UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002M3,1,0,true,false )
             ,new CursorDef("T002M4", "SELECT [Usuario_Codigo] AS UsuarioServicos_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @UsuarioServicos_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002M4,1,0,true,false )
             ,new CursorDef("T002M5", "SELECT [Servico_Sigla] AS UsuarioServicos_ServicoSigla, [Servico_Ativo] AS UsuarioServicos_ServicoAtivo FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @UsuarioServicos_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002M5,1,0,true,false )
             ,new CursorDef("T002M6", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002M6,0,0,true,false )
             ,new CursorDef("T002M7", "SELECT T2.[Servico_Sigla] AS UsuarioServicos_ServicoSigla, T2.[Servico_Ativo] AS UsuarioServicos_ServicoAtivo, TM1.[UsuarioServicos_TmpEstAnl], TM1.[UsuarioServicos_TmpEstExc], TM1.[UsuarioServicos_TmpEstCrr], TM1.[UsuarioServicos_UsuarioCod] AS UsuarioServicos_UsuarioCod, TM1.[UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod FROM ([UsuarioServicos] TM1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[UsuarioServicos_ServicoCod]) WHERE TM1.[UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod and TM1.[UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod ORDER BY TM1.[UsuarioServicos_UsuarioCod], TM1.[UsuarioServicos_ServicoCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002M7,100,0,true,false )
             ,new CursorDef("T002M8", "SELECT [Usuario_Codigo] AS UsuarioServicos_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @UsuarioServicos_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002M8,1,0,true,false )
             ,new CursorDef("T002M9", "SELECT [Servico_Sigla] AS UsuarioServicos_ServicoSigla, [Servico_Ativo] AS UsuarioServicos_ServicoAtivo FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @UsuarioServicos_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002M9,1,0,true,false )
             ,new CursorDef("T002M10", "SELECT [UsuarioServicos_UsuarioCod] AS UsuarioServicos_UsuarioCod, [UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002M10,1,0,true,false )
             ,new CursorDef("T002M11", "SELECT TOP 1 [UsuarioServicos_UsuarioCod] AS UsuarioServicos_UsuarioCod, [UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod FROM [UsuarioServicos] WITH (NOLOCK) WHERE ( [UsuarioServicos_UsuarioCod] > @UsuarioServicos_UsuarioCod or [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod and [UsuarioServicos_ServicoCod] > @UsuarioServicos_ServicoCod) ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002M11,1,0,true,true )
             ,new CursorDef("T002M12", "SELECT TOP 1 [UsuarioServicos_UsuarioCod] AS UsuarioServicos_UsuarioCod, [UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod FROM [UsuarioServicos] WITH (NOLOCK) WHERE ( [UsuarioServicos_UsuarioCod] < @UsuarioServicos_UsuarioCod or [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod and [UsuarioServicos_ServicoCod] < @UsuarioServicos_ServicoCod) ORDER BY [UsuarioServicos_UsuarioCod] DESC, [UsuarioServicos_ServicoCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002M12,1,0,true,true )
             ,new CursorDef("T002M13", "INSERT INTO [UsuarioServicos]([UsuarioServicos_TmpEstAnl], [UsuarioServicos_TmpEstExc], [UsuarioServicos_TmpEstCrr], [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod]) VALUES(@UsuarioServicos_TmpEstAnl, @UsuarioServicos_TmpEstExc, @UsuarioServicos_TmpEstCrr, @UsuarioServicos_UsuarioCod, @UsuarioServicos_ServicoCod)", GxErrorMask.GX_NOMASK,prmT002M13)
             ,new CursorDef("T002M14", "UPDATE [UsuarioServicos] SET [UsuarioServicos_TmpEstAnl]=@UsuarioServicos_TmpEstAnl, [UsuarioServicos_TmpEstExc]=@UsuarioServicos_TmpEstExc, [UsuarioServicos_TmpEstCrr]=@UsuarioServicos_TmpEstCrr  WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod", GxErrorMask.GX_NOMASK,prmT002M14)
             ,new CursorDef("T002M15", "DELETE FROM [UsuarioServicos]  WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod", GxErrorMask.GX_NOMASK,prmT002M15)
             ,new CursorDef("T002M16", "SELECT [Servico_Sigla] AS UsuarioServicos_ServicoSigla, [Servico_Ativo] AS UsuarioServicos_ServicoAtivo FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @UsuarioServicos_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002M16,1,0,true,false )
             ,new CursorDef("T002M17", "SELECT [UsuarioServicos_UsuarioCod] AS UsuarioServicos_UsuarioCod, [UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod FROM [UsuarioServicos] WITH (NOLOCK) ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002M17,100,0,true,false )
             ,new CursorDef("T002M18", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002M18,0,0,true,false )
             ,new CursorDef("T002M19", "SELECT [Usuario_Codigo] AS UsuarioServicos_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @UsuarioServicos_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002M19,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                stmt.SetParameter(5, (int)parms[7]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                stmt.SetParameter(5, (int)parms[7]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
